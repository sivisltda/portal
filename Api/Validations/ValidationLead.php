<?php
class ValidationLead extends Validation {

  public static function validation($data) {
    $regras = self::regras($data);
    return self::validacao($regras, $data);
  } 

  public static function validationFormLead($data) {
    $regras = self::regrasLead();
    return self::validacao($regras, $data);
  }

  private static function regrasLead() {
    return [
      'nome' => [
        'rule'  => 'required|min:3'
      ],
      'celular'   => [
        'rule' => 'required',
      ],
      'procedencia'   => [
          'rule' => 'required|number',
      ]
    ];
  }

  private static function regras($data) {
    return [
      'nome' => [
          'rule'  => 'required|min:3'
      ],
      'celular' => [
        'rule'  => 'required'
        .($data['tipo'] === 'indicar' ? '|unique:telefone_contato,sf_lead' : '')
      ],
      'email' => [
        'rule'  => ($data['tipo'] === 'cadastrar' ? 'required|' : '').
        'email|unique:email_contato,sf_lead'
      ]
    ];
  }
}