<?php

class ValidationDependente extends Validation {

    public static function validation($data) {
        $regras = self::regras();
        return self::validacao($regras, $data);
    }

    private static function regras() {
        return [
            'nome' => [
                'rule'  => 'required|min:3'
            ],
            'data_nascimento' => [
                'rule'  => 'required|date'
            ],
            'cnpj'=> [
                'rule'  => 'cnpj_cpf|unique:cnpj,sf_fornecedores_despesas,id,id_fornecedores_despesas' 
            ],
            'email' => [
                'rule'  => 'email|unique:conteudo_contato,sf_fornecedores_despesas_contatos,id,fornecedores_despesas'
            ],
            'sexo' => [
                'rule'  => 'required'
            ],
            'estado_civil' => [
                'rule'  => 'required'
            ]
        ];
    }

}