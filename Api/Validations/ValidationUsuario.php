<?php

class ValidationUsuario extends Validation {

    public static function validation($data) {
        $regras = [];
        $modulos = getModulos();
        if ($modulos['seg']) {
            $regras = self::regrasSeg($data);
        } else {
            $regras = self::regras($data);
        }
        return self::validacao($regras, $data);
    }

    public static function validationUpdate($data) {
        $regras = [];
        $modulos = getModulos();
        if ($modulos['seg']) {
            $regras = self::regrasUpdateSeg($data);
        } else {
            $regras = self::regrasUpdate();
        }
        return self::validacao($regras, $data);
    }

    public static function ValidationPerfil($data) {
        $regras = self::regrasPerfil();
        return self::validacao($regras, $data);
    }

    public static function validationProspect($data) {
        $regras = self::regrasProspect();
        return self::validacao($regras, $data);
    }

    private static function regras($data) {
        $usuarioRepository = new UsuarioRepository();
        $isUsuario = false;
        if (isset($data['id'])) {
            $isUsuario = $usuarioRepository->isVerificaEmail($data['email'], $data['id']);
        }
        $dados =  [
            'email' => [
                'rule' => 'required|email'.(!$isUsuario ? '|unique:conteudo_contato,sf_fornecedores_despesas_contatos,id,fornecedores_despesas' : ''  ),
                'message' => [
                    'required' => 'O email é obrigatório'
                ]
            ],
            'cnpj' => [
                'rule' => (isset($data['id']) ? '' : 'required|').'cnpj_cpf|unique:cnpj,sf_fornecedores_despesas,id,id_fornecedores_despesas'
            ],
            'razao_social' => [
                'rule' => 'required'
            ],
            'celular'   => [
                'rule' => 'required',
            ],
            'procedencia'   => [
                'rule' => 'required|number',
            ],
            'cep' => [
                'rule'  => 'required|min:9|cep'
            ],
            'endereco' => [
                'rule'  => 'required|min:3'
            ],
            'numero' => [
                'rule'  => 'required'
            ],
            'bairro' => [
                'rule' => 'required|min:3'
            ],
            'cidade' => [
                'rule' => 'required|number'
            ],
            'estado' => [
                'rule' => 'required|number'
            ]
        ];
        if (!isset($data['juridico_tipo']) || $data['juridico_tipo'] === 'F') {
            $dados = array_merge($dados, [
                'data_nascimento' => [
                    'rule' => 'required|date'
                ],
                'estado_civil' => [
                    'rule'  => 'required'
                ],
            ]);
        } else {
            $dados = array_merge($dados,  [
                'regime_tributario' => [
                    'rule' => 'required|number'
                ],
                'contato' => [
                    'rule' => 'required'
                ],
                'telefone' => [
                    'rule' => 'required'
                ],
            ]);
        }
        return $dados;
    }

    private static function regrasUpdate() {
        return [];
    }


    private static function regrasSeg($data) {
        $usuarioRepository = new UsuarioRepository();
        $veiculoRepository = new VeiculoRepository();
        $isUsuario =  isset($data['id']) ? 
            $usuarioRepository->isVerificaEmail($data['email'], $data['id']) : false; 
        $isVeiculo = isset($data['id']) && isset($data['veiculo']['id']) ?
            $veiculoRepository->isVerificaPlaca([
                'id' => $data['veiculo']['id'],
                'id_usuario' => $data['id'],
                'placa' => $data['veiculo']['placa']
            ]) : false;
            return [
            'email' => [
                'rule' => 'required|email'.(!$isUsuario ? '|unique:conteudo_contato,sf_fornecedores_despesas_contatos,id,fornecedores_despesas' : ''),
                'message' => [
                    'required' => 'O email é obrigatório'
                ]
            ],
            'razao_social' => [
                'rule' => 'required'
            ],
            'celular'   => [
                'rule' => 'required',
            ],
            'cep' => [
                'rule'  => 'required|cep'
            ],
            'veiculo' => [
                'placa' => [
                    'rule'  => ($data['veiculo']['consulta_fipe'] == "2" ? "" : 
                    'required|placa' .(!$isVeiculo ? '|unique:placa,sf_fornecedores_despesas_veiculo,id,id' : ''))                     
                ],
                'marca' => [
                    'rule'  => 'required'
                ],
                'ano_modelo' => [
                    'rule'  => 'required'
                ],
                'modelo' => [
                    'rule'  => 'required'
                ]
            ]
        ];
    }

    private static function regrasProspect() {
        return [
            'razao_social' => [
                'rule' => 'required'
            ],
            'email' => [
                'rule' => 'required|email|unique:conteudo_contato,sf_fornecedores_despesas_contatos',
                'message' => [
                    'required' => 'O email é obrigatório'
                ]
            ],
            'celular'   => [
                'rule' => 'required',
            ],
        ];
    }


    private static function regrasUpdateSeg($data) {
        return [
            'razao_social' => [
                'rule' => 'required'
            ],
            'cnpj' => [
                'rule' => 'required|cnpj_cpf|unique:cnpj,sf_fornecedores_despesas,id,id_fornecedores_despesas'
            ],
            'inscricao_estadual' => [
                'rule'  => 'required'
            ],
            'data_nascimento' => [
                'rule'  => 'required|date'
            ],
            'sexo' => [
                'rule'  => 'required'
            ],
            'estado_civil'  => [
                'rule'  => 'required'
            ],
            'celular' => [
                'rule'  =>  'required'
            ],
            'cep'   => [
                'rule'  => 'required|min:9|cep'
            ],
            'estado' => [
                'rule'  => 'required|number'
            ],
            'cidade' => [
                'rule'  => 'required|number'
            ],
            'bairro' => [
                'rule'  => 'required'
            ],
            'numero' => [
                'rule'  => 'required'
            ],
            'veiculo' => [
                'id' => [
                    'rule'  => 'required|number'
                ],
                'placa' => [
                    'rule'  => ($data['veiculo']['consulta_fipe'] == "2" ? "" : 'required|placa')  
                ],
                'marca' => [
                    'rule'  => 'required'
                ],
                'ano_modelo' => [
                    'rule'  => 'required'
                ],
                'modelo' => [
                    'rule'  => 'required'
                ],
                'renavam' => [
                    'rule'  => 'required'
                ],
                'chassi' => [
                    'rule'  => 'required'
                ],
                'cor' => [
                    'rule'  => 'required|number'
                ],
                'combustivel' => [
                    'rule'  => 'required'
                ]
            ]
        ];
    }

    private static function regrasPerfil() {
        return [
            'estado_civil' => [
                'rule' => 'required'
            ],
            'celular' => [
                'rule' => 'required'
            ],
            'cep' => [
                'rule'  => 'required|min:9|cep'
            ],
            'endereco' => [
                'rule'  => 'required|min:3'
            ],
            'numero' => [
                'rule'  => 'required'
            ],
            'bairro' => [
                'rule' => 'required|min:3'
            ],
            'cidade' => [
                'rule' => 'required|number'
            ],
            'estado' => [
                'rule' => 'required|number'
            ]
        ];
    }
}