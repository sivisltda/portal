<?php 

abstract class Validation {

    public static function validacao($regras, $data) {
        $erros = [];
        foreach($regras as $campo => $regra) {
            $valor = isset($data[$campo]) ?  $data[$campo] : '';
            if (is_array($regra) && is_array($valor)) {
                $validador = self::validacao($regra, $valor);
                if ($validador) {
                    $erros[$campo] = $validador;
                }
            } else {
                $r          = is_array($regra) ? $regra['rule'] : $regra;
                $mensagem   = (is_array($regra) && isset($regra['message'])) ? $regra['message'] : [];
                $validador  = self::regrasValidacao($r, $valor, $mensagem, $data);
                if($validador) {
                    $erros[$campo] = $validador;            
                }            
            }                      
        }
        return $erros;
    }

    private static function regrasValidacao($regra, $valor, $mensagem, $data) {
        $regras = preg_match('/|/', $regra) ? explode('|', $regra) : [$regra];
        $mensagemRetorno = function($campo, $mensagem, $padrao) {
            return isset($mensagem[$campo]) ? $mensagem[$campo] : $padrao;
        };
        foreach($regras as $r) {
            switch(true) {
                case $r === 'required':
                    if($valor === "") return $mensagemRetorno($r, $mensagem, 'Campo obrigatório');
                break;
                case $r === 'email':
                    if($valor && !self::validaEmail($valor)) return $mensagemRetorno($r, $mensagem, 'Email inválido');
                break;
                case $r === 'cnpj_cpf':
                    if($valor && !self::validaCpf($valor)) return $mensagemRetorno($r, $mensagem, 'CPF/CNPJ inválido');
                break;
                case $r === 'number':
                    if($valor && !is_numeric($valor)) return $mensagemRetorno($r, $mensagem, 'Número inválido');
                break;
                case $r === 'cep':
                    if($valor && !preg_match('/^\d{5}-?\d{3}$/', $valor)) return $mensagemRetorno($r, $mensagem, 'Cep inválido');
                break;
                case $r === 'date':
                    $patternDate = '/^(((0[1-9]|[12][0-9]|3[01])([-.\/])(0[13578]|10|12)([-.\/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([-.\/])(0[469]|11)([-.\/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([-.\/])(02)([-.\/])(\d{4}))|((29)(\.|-|\/)(02)([-.\/])([02468][048]00))|((29)([-.\/])(02)([-.\/])([13579][26]00))|((29)([-.\/])(02)([-.\/])([0-9][0-9][0][48]))|((29)([-.\/])(02)([-.\/])([0-9][0-9][2468][048]))|((29)([-.\/])(02)([-.\/])([0-9][0-9][13579][26])))$/';
                    if ($valor && !preg_match($patternDate, $valor)) return $mensagemRetorno($r, $mensagem, 'Data inválida');
                break;
                case $r === 'placa':
                    if ($valor && !(self::validaPlaca($valor))) return $mensagemRetorno($r, $mensagem, 'Placa inválida');
                break;
                case preg_match('/^unique:/', $r):
                    $unique = str_replace('unique:', '', $r);
                    $validacao = preg_match('/,/', $unique) ? explode(',', $unique) : [$unique, null];
                    $campo = $validacao[0];
                    $tabela = $validacao[1];
                    $campoExcecao = isset($validacao[2]) ?  $validacao[2] : null;
                    $campoBancoExcecao = isset($validacao[3]) ? $validacao[3] : null;
                    $excecao = $campoExcecao && isset($data[$campoExcecao]) ? $data[$campoExcecao] : null;
                    if($valor && self::unique($tabela, $campo, $valor, $excecao, $campoBancoExcecao)) return $mensagemRetorno($r, $mensagem,'Dado Existente');
                break;
                case preg_match('/^requiredIf:/', $r):
                    $requireIf = str_replace('requiredIf:', '', $r);
                    list($campo, $condicao) = preg_match('/&/', $requireIf) ? explode('&', $requireIf) : [$requireIf, null];
                    $funcaoValidacao = function ($campo, $valorRegra) {
                      return !$valorRegra ? $campo == '' : $campo == $valorRegra;  
                    };
                    if($valor && $funcaoValidacao($data[$campo], $condicao)) return $mensagemRetorno($r, $mensagem,  'Campo obrigatório');
                break;
                case preg_match('/^min:/', $r):
                    $min = str_replace('min:', '', $r);
                    if($valor && strlen($valor) < $min) return $mensagemRetorno($r, $mensagem, "Minimo $min caractere(s)");
                break;
                case preg_match('/^max:/', $r):
                    $max = str_replace('max:', '', $r);
                    if($valor && strlen($valor) > $max) return $mensagemRetorno($r, $mensagem, "Máximo $max caractere(s)");
                break;
            }
        }
    }

    public static function validaEmail($valor)
    {        
        if(filter_var($valor, FILTER_VALIDATE_EMAIL)){
            $retorno = true;
        }else{
            $retorno = false;
        }        
        return ($retorno);
    }

    public static function validaCelular($phone){
        if(!preg_match("/\(?\d{2}\)?\s?\d{5}\-?\d{4}/", $phone)) {
            return false;
        } 
        return true;
    }
    
    public static function validaCpf($valor){

        if(!preg_match("/([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})/", $valor)) {
            return false;
        }

        $valor = str_replace(['.','-','/'], "", $valor);
        $cpf = str_pad(preg_replace('[^0-9]', '', $valor), 11, '0', STR_PAD_LEFT);

        //validacao de cpf
        if( strlen($cpf) == 11 ){
            
            if ( $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999'):
                return false;
            else: 
                for ($t = 9; $t < 11; $t++):
                    for ($d = 0, $c = 0; $c < $t; $c++) :
                        $d += $cpf{$c} * (($t + 1) - $c);
                    endfor;
                    $d = ((10 * $d) % 11) % 10;
                    if ($cpf{$c} != $d):
                        return false;
                    endif;
                endfor;
                return true;
            endif;

        }

        //validacao de cnpj
        else{

            $cpf = (string) $cpf;

            if ( strlen($cpf) != 14 )
                return false;
            // Valida primeiro dígito verificador
            for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
            {
                $soma += $cpf[$i] * $j;
                $j = ($j == 2) ? 9 : $j - 1;
            }
            $resto = $soma % 11;

            if ( $cpf[12] != ($resto < 2 ? 0 : 11 - $resto) )
                return false;
            // Valida segundo dígito verificador
            for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
            {
                $soma += $cpf[$i] * $j;
                $j = ($j == 2) ? 9 : $j - 1;
            }
            $resto = $soma % 11;
            //return true;
            $retorno =  $cpf[13];
            
            return $cpf[13] == ($resto < 2 ? 0 : 11 - $resto);

        }        
        
    } 
    
    public static function validaPlaca($placa) {
        $placaNormal = preg_match('/^[a-zA-Z]{3}-?[0-9]{4}$/', $placa);
        $placaMercosul = preg_match('/^[a-zA-Z]{3}-?[0-9]{1}[a-zA-Z]{1}[0-9]{2}$/', $placa);
        return $placaNormal || $placaMercosul;
    }

    public static function unique($entidade, $campo, $valor, $id, $campoPrimario = 'id') {
        try {
            $where = $id ? " AND ".$campoPrimario." != ".$id : "";
            $whereCondicional = $entidade === 'sf_fornecedores_despesas' ? " where fornecedores_status not in ('Excluido') and tipo in ('C', 'P') and"
            : ($entidade === 'sf_fornecedores_despesas_contatos' ? "inner join sf_fornecedores_despesas fd 
                on fd.id_fornecedores_despesas = fornecedores_despesas and fornecedores_status not in ('Excluido') and tipo in ('C', 'P') WHERE" : 
            ($entidade === 'sf_fornecedores_despesas_veiculo' ? "inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas 
            = sf_fornecedores_despesas_veiculo.id_fornecedores_despesas and fornecedores_status not in ('Excluido') and tipo in ('C', 'P') WHERE" :"WHERE"));
            $sql = "select top 1 $campo from $entidade $whereCondicional $campo = ".valoresTexto2($valor).$where;
            $res = Conexao::conect($sql);
            return odbc_result($res, 1);
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}