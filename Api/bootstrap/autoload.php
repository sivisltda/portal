<?php

Class Autoload {

    public function __construct(){
        requireOnce(__DIR__.'/../config/config.php');
        spl_autoload_register([$this, 'folders']);
    }

    private function folders($files){
        $this->archives = [
            __DIR__.'/../utils/Connections/'.$files.'.php',
            __DIR__.'/../Repository/'.$files.'.php',
            __DIR__.'/../Service/'.$files.'.php',
            __DIR__.'/../Validations/'.$files.'.php'
        ];
        foreach ($this->archives as $archives) :
            if (file_exists(replaceBarraDirectory($archives)))  {
                requireOnce($archives);
            }
        endforeach;
    }

}
new Autoload;