<?php
requireOnce(__DIR__.'/../utils/Connections/ConexaoDefault.php');

function getPartesUrl() {
    if ($_SERVER['SERVER_NAME'] == 'localhost') { 
        $request_uri = strtolower(filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL));
        $fullUrl = preg_replace("/\/".DOMINIO."\//", "", $request_uri, 1);
        $isGet = strpos($fullUrl, '?');
        $fullUrl = ($isGet && $isGet >= 0) ? substr($fullUrl, 0, $isGet) : $fullUrl;
    } else {
        $fullUrl = strtolower($_REQUEST['url']);
    }
    
   return array_filter(explode('/', trim($fullUrl)));
}

function isExistsTag($nome, $partes) {
    if(in_array($nome, $partes) || isset($_GET[$nome])) {
        $key = array_search($nome, $partes);
        $isKey = $key > -1 ? array_key_exists($key + 1, $partes) : false;
        return $isKey ? $partes[$key + 1] : filter_input(INPUT_GET, $nome);
    }
    return false;
}

function verificaLoja($contrato) {
    try {
        $query = ConexaoDefault::conect("select *, (select top 1 conteudo_contato from sf_fornecedores_despesas_contatos where tipo_contato = 2 and fornecedores_despesas = id_fornecedores_despesas) email
        from ca_contratos
        left join sf_fornecedores_despesas on id_fornecedores_despesas = id_cliente 
        left join tb_estados on estado_codigo = estado
        left join tb_cidades on cidade_codigo = cidade 
        where inativa = 0 and numero_contrato = " . valoresTexto2($contrato).";");
        $data = [];
        while ($RFP = odbc_fetch_array($query)) {
            $data['id']             = $RFP['id'];
            $data['cedente']        = escreverTexto($RFP['razao_social']);
            $data['nome_fantasia']  = escreverTexto($RFP['nome_fantasia']);
            $data['cpf_cnpj']       = escreverTexto($RFP['cnpj']);
            $data['endereco']       = escreverTexto($RFP['endereco']) . " n°: " . escreverTexto($RFP['numero']) . ", " . escreverTexto($RFP['bairro']);
            $data['cidade_uf']      = escreverTexto($RFP['cidade_nome']) . " - " . escreverTexto($RFP['estado_sigla']);
            $data['email']          = escreverTexto($RFP['email']);
            $data['site']           = escreverTexto($RFP['contato']);
            $data['contrato']       = escreverTexto($RFP['numero_contrato']);
            $data['hostname']       = escreverTexto($RFP['local']);
            $data['username']       = escreverTexto($RFP['login']);
            $data['password']       = escreverTexto($RFP['senha']);
            $data['database']       = escreverTexto($RFP['banco']);
            $data['contrato_hash']  = encrypt($RFP['numero_contrato']."|".KEY_CRIPT, KEY_SIVIS, true);
            $data['mdl_clb']        = !!$RFP['mdl_clb'];
            $data['mdl_mil']        = !!$RFP['mdl_mil'];
            $data['mdl_seg']        = !!$RFP['mdl_seg'];
            $data['mdl_sau']        = !!$RFP['mdl_sau'];
            $data['mdl_wha']        = !!$RFP['mdl_wha'];
            $data['grupos']         = getGrupoContrato($RFP['id']);
        }
        return $data;
    } catch(Exception $e) {
        throw new Exception($e);
    }
}

function verificaIp($id_usuario, $contrato) {
    try {        
        $white_total_ = 0;
        $white_count_ = 0;
        $black_count_ = 0;           
        $query = Conexao::conect("select 
        (select COUNT(*) total from sf_login_white) white_total,
        (select COUNT(*) total from sf_login_white where ip = '" . $_SERVER['REMOTE_ADDR'] . "' and (id_usuario is null or id_usuario = " . $id_usuario . ")) white_count,
        (select COUNT(*) total from sf_login_black where ip = '" . $_SERVER['REMOTE_ADDR'] . "') black_count;");
        while ($RFP = odbc_fetch_array($query)) {
            $white_total_ = $RFP['white_total'];
            $white_count_ = $RFP['white_count'];
            $black_count_ = $RFP['black_count'];
        }        
        if ($white_total_ > 0 && $white_count_ == 0 && $id_usuario > 1 && $black_count_ == 0) {    
            $link_go = encrypt(($_SERVER['REMOTE_ADDR'] . "|" . $contrato . "|L|" . $id_usuario . "|" . date("d/m/Y H:i")), "VipService123", true);    
            $query_insert = "insert into sf_whatsapp (telefone, mensagem, data_envio, fornecedor_despesas, sys_login, status)
            select (select top 1 replace(replace(replace(replace(isnull(conteudo_contato,''), '(',''), ')',''), ' ',''), '-','') from sf_fornecedores_despesas_contatos where tipo_contato = 1 and fornecedores_despesas = c.id_fornecedores_despesas) celular," .
            utf8_decode("'Olá *' + login_user + '*, uma tentativa de acesso foi feita ao seu usuário foi feita pelo ip: *" . $_SERVER['REMOTE_ADDR'] . "* dia *" . date("d/m/Y H:i") . "* %0a
            Caso esteja tentando fazer esse acesso, acesse o link abaixo para liberar%0a Se não o acesso continuará bloqueado.%0a%0a ") .
            "https://sivisweb.com.br/Modulos/Sivis/ws_security.php?link=" . $link_go . "', 
            getdate(), c.id_fornecedores_despesas, login_user, 0 from sf_usuarios u 
            inner join sf_fornecedores_despesas c on u.funcionario = c.id_fornecedores_despesas
            where u.id_usuario = " . $id_usuario . " and id_fornecedores_despesas not in (select fornecedor_despesas from sf_whatsapp where dateadd(minute,5,data_envio) > getdate());";
            Conexao::conect($query_insert);
        }
        if (($white_total_ > 0 && $white_count_ == 0) || ($black_count_ > 0)) {    
            return true;
        } else {
            return false;
        }
    } catch (Exception $ex) {
        throw new Exception($e);
    }
}

function getGrupoContrato($idContrato) {
    try {
        $rs = ConexaoDefault::conect("select top 1 numero_contrato, nome_fantasia as razao_social, local, login, senha, banco from ca_contratos c 
        inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = c.id_cliente
        where id = ".valoresSelect2($idContrato)."
        union all select case when de_contrato = ".valoresSelect2($idContrato)." then c2.numero_contrato else c1.numero_contrato end numero_contrato,
        case when de_contrato = ".valoresSelect2($idContrato)." then f2.nome_fantasia else f1.nome_fantasia end razao_social,
        case when de_contrato = ".valoresSelect2($idContrato)." then c2.local else c1.local end local,
        case when de_contrato = ".valoresSelect2($idContrato)." then c2.login else c1.login end login,
        case when de_contrato = ".valoresSelect2($idContrato)." then c2.senha else c1.senha end senha,
        case when de_contrato = ".valoresSelect2($idContrato)." then c2.banco else c1.local end banco
        from ca_contratos_dependente c
        inner join ca_contratos c1 on c1.id = c.de_contrato 
        inner join sf_fornecedores_despesas f1 on f1.id_fornecedores_despesas = c1.id_cliente
        inner join ca_contratos c2 on c2.id = c.para_contrato
        inner join sf_fornecedores_despesas f2 on f2.id_fornecedores_despesas = c2.id_cliente
        where (de_contrato = ".valoresSelect2($idContrato)." or para_contrato = ".valoresSelect2($idContrato).");");
        $data = [];
        while ($RFP = odbc_fetch_array($rs)) {
            $data[] = [
                'numero_contrato'   => $RFP['numero_contrato'],
                'razao_social'      => escreverTexto($RFP['razao_social']),
                'local'             => $RFP['local'],
                'login'             => $RFP['login'],
                'senha'             => $RFP['senha'],
                'banco'             => $RFP['banco']  
            ];
        }
        return $data;
    } catch (Exception $e) {
        throw new Exception($e);
    }
}

function getConfig($con) {
    try {
        $query = odbc_exec($con, 'select cor_primaria, cor_secundaria, cor_destaque, adm_dependente_parentesco, aca_adesao_dep, 
          adm_plano_unico, adm_terminal_venda, adm_venda_online, ACA_TIPO_VENCIMENTO, ACA_DIA_VENCIMENTO, ACA_EX_PJ, ACA_EX_VEN_PR,
          seg_cota_defaut, seg_ano_veiculo_piso, com_forma, wa_padrao_portal, adm_agrupar_plan_item, adm_plan_mens_plan_contrat, adm_nao_valor_print,
          adm_msg_boas_vindas, adm_msg_cotacao, adm_associado, adm_msg_contratacao, adm_msg_taxa_adesao, adm_desc_isencao_ie, portal_contrato,
          adm_msg_termos_email, adm_msg_termos_whats, adm_msg_link_pag, adm_nao_alterar_tp_pag, adm_nao_proc_pag_online, aca_add_dep,
          (select top 1 telefone from sf_filiais) as telefone, (select top 1 site from sf_filiais) as site, aca_pes_cot_tip,
          (SELECT count(*) as agenda FROM sf_agenda where inativo = 0 and terminal = 1) as agenda, adm_desconto_range, seg_vei_cotac_person
          from sf_configuracao;');
        if (!$query) {
            throw new Exception(odbc_errormsg($con));
        }
        $dados = [];
        while ($RFP = odbc_fetch_array($query)) {
            $dados = $RFP;
            $dados['adm_msg_boas_vindas'] = escreverTexto($dados['adm_msg_boas_vindas']);
            $dados['adm_msg_cotacao'] = escreverTexto($dados['adm_msg_cotacao']);
            $dados['adm_associado'] = escreverTexto($dados['adm_associado']);
            $dados['adm_msg_contratacao'] = escreverTexto($dados['adm_msg_contratacao']);
            $dados['adm_msg_taxa_adesao'] = escreverTexto($dados['adm_msg_taxa_adesao']);
            $dados['adm_msg_termos_email'] = escreverTexto($dados['adm_msg_termos_email']);
            $dados['adm_msg_termos_whats'] = escreverTexto($dados['adm_msg_termos_whats']);
            $dados['adm_msg_link_pag'] = escreverTexto($dados['adm_msg_link_pag']);
            $dados['seg_cota_defaut'] = floatval($dados['seg_cota_defaut']);
            $dados['agenda'] = $RFP['agenda'] > 0 ? 1 : 0;
            $dados['nao_pag_online'] = $RFP['adm_nao_proc_pag_online'] > 0 ? 1 : 0;
        }
        return $dados;
    }catch (Exception $e) {
        throw new Exception($e);
    }
} 

function getImagesContratacao($contrato) {
    try {
        $dados = getImages($contrato, 'Empresa/Contratacao');
        if ($dados && count($dados)) {
            $image = current($dados);
            return $image['url'];
        }
        return BASE_URL.'App/assets/images/conclusao.jpg';        
    } catch (Exception $e) {
        throw new Exception($e);
    }
}

function getImagesLogo($contrato) {
    try {
        $dados = getImages($contrato, 'Empresa');
        if ($dados && count($dados)) {
            $image = current($dados);
            return $image['url'];
        }
        return BASE_URL.'App/assets/img/logo.png';        
    } catch (Exception $e) {
        throw new Exception($e);
    }
}

function getImageWhatsApp($contrato) {
    try {
        $dados = getImages($contrato, 'Empresa/Whatsapp');
        if ($dados && count($dados)) {
            $image = current($dados);
            return $image['url'];
        }
        return false;        
    } catch (Exception $e) {
        throw new Exception($e);
    }
}



function isExistsConsultor($partes) {
    $consultor = isExistsTag('consultor', $partes);
    if ($consultor && is_numeric($consultor)) {
        $funcionarioRepositorory = new FuncionarioRepository();
        $dados = $funcionarioRepositorory->getNomeConsultorPorCodigo($consultor);
        if ($dados) {
            $dados['ind_chave'] = encrypt(numPad($consultor), KEY_SIVIS, true);
            return $dados;
        }
    }
    return [];
}


function isExistsProduto($partes) {
    $produto = isExistsTag('produto', $partes);
    if ($produto && (is_numeric($produto) || is_string($produto))) {
        $produtoRepository = new ProdutoRepository();
        $condicao = 'conta_produto = '.valoresSelect2($produto);
        if ($dados = $produtoRepository->getProduto($condicao, 0)) {
            return $dados;
        }
    }
    return [];
}
