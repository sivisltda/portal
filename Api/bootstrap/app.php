<?php

requireOnce(__DIR__ . '/functions.php');

$partes = getPartesUrl();
$contrato = isExistsTag('loja', $partes) ? isExistsTag('loja', $partes) : null;
$headers = getRequestHeaders();
$requestJson = isJson($headers) || array_search('api', $partes);

try {
    if ($contrato && is_numeric($contrato)) {
        $dadosLoja = verificaLoja($contrato);
        if (!$dadosLoja) {
            throw new Exception('Contrato inválido', 406);
        }
        if (isset($_SESSION['loja-' . $contrato]) && $requestJson) {
            $dadosLoja = json_decode(encrypt($_SESSION['loja-' . $contrato]['config'], KEY_CRIPT, false), 1);
        } else {
            $con = odbc_connect("DRIVER={SQL Server}; SERVER=" . $dadosLoja['hostname'] . "; DATABASE=" . $dadosLoja['database'] . ";", $dadosLoja['username'], $dadosLoja['password']);
            $dadosLoja = array_merge($dadosLoja, getConfig($con));
            $dadosLoja['imagem_logo'] = getImagesLogo($dadosLoja['contrato']);
            $dadosLoja["imagem_whats"] = getImageWhatsApp($dadosLoja['contrato']);
            $dadosLoja["imagem_conclusao"] = getImagesContratacao($dadosLoja['contrato']);
            $_SESSION["loja-" . $dadosLoja['contrato']]['config'] = encrypt(json_encode($dadosLoja), KEY_CRIPT, true);
        }
        $_SESSION['contrato'] = $dadosLoja['contrato'];
        requireOnce(__DIR__ . '/autoload.php');
        $tipo = isExistsTag($contrato, $partes);      
        
        if (isset($_SESSION[CHAVE_CONTRATO]['indicador']) && isset($_SESSION[CHAVE_CONTRATO]['indicador']['id_usuario'])) {
            $dadosIp = verificaIp($_SESSION[CHAVE_CONTRATO]['indicador']['id_usuario'], $contrato);
            if ($dadosIp) {
                throw new Exception('Banco corrompido!', 406);
            }
        }
        
        if ($tipo === 'api') {
            $processo = isExistsTag('api', $partes);
            $recurso = isExistsTag($processo, $partes);
            if ($processo && $recurso) {
                $arquivo = __DIR__ . "/../routes/" . $processo . '/' . $recurso . ".php";
                if (file_exists(replaceBarraDirectory($arquivo))) {
                    requireOnce($arquivo);
                }
            }
            throw new Exception('Rota Inválida', 404);
        } else {
            requireOnce(__DIR__ . "/../proccess/proccess.php", compact('tipo', 'partes', 'dadosLoja'));
        }
    }
} catch (Exception $e) {
    $message = $e->getMessage();
    $code = $e->getCode() ? $e->getCode() : 500;
    if ($requestJson) {
        if ($code === 422) {
            $message = json_decode($message, 1);
        }
        responseJSON(['erro' => $message], $code);
    } else {
        if ($code === 406) {
            requireOnce(__DIR__ . '/../../App/View/errors/contract-invalid.php', ['exception' => $e]);
        } else if ($code === 404) {
            requireOnce(__DIR__ . '/../../App/View/errors/not-found.php', ['exception' => $e]);
        } else if ($code === 403) {
            requireOnce(__DIR__ . '/../../App/View/errors/forbbiden.php', ['exception' => $e]);
        } else if ($code === 500) {
            requireOnce(__DIR__ . '/../../App/View/errors/error-internal.php', ['exception' => $e]);
        } else if ($code === 401) {
            header('Location:' . BASE_URL . 'loja/' . $contrato . '/login');
        }
    }
    exit;
}