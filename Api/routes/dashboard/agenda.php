<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 23/06/2020
 * Time: 13:49
 */

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

if(empty($_SESSION[CHAVE_CONTRATO]['restricted_id']) ){
    throw new Exception('Usuário não autenticado', 401);
}

$agendaRepository = new AgendaRepository();
$agendaService = new AgendaService();

if ($request_body["functionPage"] === "ListaAgenda") {
    try {
        $dados = $agendaService->getListAgenda($request_body['numero_contrato']);
        responseJSON($dados);
    }catch(Exception $e) {
       throw new Exception($e->getMessage(), $e->getCode());
    }
}


if ($request_body['functionPage'] === 'ListaHorariosLivres') {
    try {
        $dados = $agendaService->getListHorarioAgenda($request_body);
        responseJSON($dados);
    }catch(Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if ($request_body['functionPage'] === 'ListaHorariosDisponiveis') {
    try {
        $dados = $agendaService->getListHorarioAgenda($request_body);
        responseJSON($dados);
    }catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if ($request_body['functionPage'] === 'ListaDias') {
    try {
        $agenda = $agendaService->getAgendaPorId($request_body);
        $dias   = ($agenda['dias'] > 0 ? $agenda['dias'] : 7) + 1;
        $dados  = $agendaService->make($dias.' days', convertDataIso($request_body['data_inicio']));
        responseJSON($dados);
    } catch(Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'SalvarAgendamento') {
    try {
        $request_body['id_usuario'] = (int) $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $agendamento = $agendaService->salvarAgendamento($request_body);
        responseJSON($agendamento, 201);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if ($request_body['functionPage'] === 'ListarAgendamentos') {
    try {
        $request_body['id_usuario'] = (int) $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $agendamentos = $agendaService->getListaAgendamento($request_body);
        responseJSON($agendamentos);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if ($request_body['functionPage'] === 'CancelaAgendamento') {
    try {
        $request_body['id_usuario'] = (int) $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $agendaService->cancelarAgendamento($request_body);
        responseJSON([], 204);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if ($request_body['functionPage'] === 'VerificaUsuarioAtivo') {
    try {
        if (!$agendaRepository->verificaPlanoAtivo($_SESSION[CHAVE_CONTRATO]['restricted_id'])) {
            throw new Exception('Não foi possível efetuar o agendamento. Favor entrar em contato com o SAC', 403);
        }
        responseJSON('ok');
    }catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}