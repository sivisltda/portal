<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 23/06/2020
 * Time: 13:47
 */

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

if (empty($_SESSION[CHAVE_CONTRATO]['restricted_id']) ){
    throw new Exception('Usuário não autenticado', 401);
}

$planoRepository = new PlanoRepository();
$mensalidadeRepository = new MensalidadeRepository();
$pagamentoRepository = new PagamentoRepository();

if($request_body["functionPage"] === 'filtrosMensalidades') {
    try {
        $data = [];
        $id = (int) $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $veiculoRepository = new VeiculoRepository();
        $data['veiculos'] = $veiculoRepository->getListaVeiculos('v.id_fornecedores_despesas = '.$id);
        $data['planos'] = $planoRepository->getPlanos('ve.id in ('.implode(', ', array_keys($data['veiculos'])).')');
        $data['status'] = ['aberto' => 'aberto', 'pago' => 'pago', 'vencido' => 'vencido'];
        $data['tipos_pagamentos'] = ['dcc' => 'DCC', 'boleto' => 'Boleto', 'mensal' => 'Mensal'];
        responseJSON($data);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if($request_body["functionPage"] == "listaMensalidades"){
    try {
        $sLimit = 0;
        $sQtd = 20;
        $iTotal = 0;
        $sWhere = "";

        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = $_GET['iDisplayStart'];
            $sQtd = $_GET['iDisplayLength'];
        }
        if (isset($request_body["inicio"]) && isset($request_body["fim"])) {
            $inicio = valoresData2(escreverData($request_body["inicio"]));
            $fim = valoresDataHora2(escreverData($request_body["fim"]), '23:59:59');
            $sWhere.= " and (dt_inicio_mens BETWEEN $inicio and $fim)";
        }

        if(isset($request_body['veiculo']) && $request_body['veiculo']) {
            $sWhere.= " and id_veiculo in (".filtroWhereIn($request_body['veiculo']).")";
        }

        if(isset($request_body['tipo_plano']) && $request_body['tipo_plano']) {
            $sWhere.= " and conta_produto in (".filtroWhereIn($request_body['tipo_plano']).")";
        }

        if(isset($request_body['tipo_pagamento']) && $request_body['tipo_pagamento']) {
            $sWhere.= " and tipo_pagamento in (".filtroWhereIn($request_body['tipo_pagamento']).")";
        }

        if(isset($request_body['status']) && $request_body['status']) {
            $sWhere.= " and status_mensalidade in (".filtroWhereIn($request_body['status']).")";
        }
        $id = (int) $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $dados = [
            "sEcho" => intval(filter_input(INPUT_GET, 'sEcho')),
            "aaData" => []
        ];
        $condicoes = "(favorecido = ".valoresSelect2($id)." 
            or (select top 1 tipo from sf_fornecedores_despesas 
            where id_fornecedores_despesas = ".valoresSelect2($id).") = 'E')".$sWhere;
        $dataMensalidade = $mensalidadeRepository->listaMensalidadesPlano(null, ['limit' => $sLimit, 'qtd' => $sQtd]);
        $dados['aaData'] = $dataMensalidade['aaData'];
        $dados['iTotalRecords'] = $dados['total'];
        $dados['iTotalDisplayRecords'] = $dados['total'];
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}