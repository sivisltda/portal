<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 23/06/2020
 * Time: 13:57
 */

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

if (empty($_SESSION[CHAVE_CONTRATO]['restricted_id']) ){
    throw new Exception('Usuário não autenticado', 401);
}

$pagamentoService       = new PagamentoService();        
$pagamentoRepository    = new PagamentoRepository();
$planoService           = new PlanoService();

if ($request_body['functionPage'] === 'processarPagamento') {
    try {
        $data = $request_body;
        if (!isset($data['usuario_id'])) {
            $data['usuario_id'] = isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['restricted_id']) ? $_SESSION[CHAVE_CONTRATO]['restricted_id'] : null;
        }
        if (!$data['usuario_id']) {
            throw new Exception('Usuário não logado', 401);
        }
        $dados = $pagamentoService->processarPagamento($data);
        responseJSON($dados);
    } catch(Exception $e) {
       throw new Exception($e->getMessage(), $e->getCode());
    }

}

if($request_body['functionPage'] === 'ListaMensalidadesPorPlano') {
    try {
        $mensalidades = $pagamentoService->getListaMensalidadesPorPlano($request_body['plano_id']);
        responseJSON($mensalidades);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if($request_body['functionPage'] === 'inicioFormaPagamento') {
    try {
        $id_usuario = $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $dados = $pagamentoService->getInfoInicial($id_usuario);
        responseJSON($dados);
    }catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'verificaMulta') {
    try {
        $id_mens = $request_body['id_mens'];
        if (!($id_mens && is_numeric($id_mens))) {
            throw new Exception('Código de Mensalidade inválido', 400);
        }
        $valor = $pagamentoService->calculaMultaMensalidade($id_mens);
        responseJSON(compact('valor'));
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if($request_body['functionPage'] === 'alterarFormaPagamento') {
    try {
        $request_body['usuario_id'] = $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $pagamentoService->atualizarFormaPagamento($request_body);
        responseJSON('OK');
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'alterarCartao') {
    try {
        $id_usuario = $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $planoRepository = new PlanoRepository();
        $planoRepository->criarCartaoUsuario($id_usuario, $request_body);
        $planoRepository->atualizaMensalidadesDCC($id_usuario);
        responseJSON('ok');
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if($request_body['functionPage'] === 'listaPlanos') {
    try {
        $modulos = getModulos();
        $id = $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $tipo = $request_body['tipo_veiculo'];
        $campoTipo = $modulos['seg'] ? 'tipo_veiculo' : 'conta_movimento';
        $where = "dt_cancelamento is null and favorecido = ".valoresSelect2($id).($tipo ? ' and '.$campoTipo.' = '.valoresTexto2($tipo) : '');
        $planos = $planoService->listaPlanos($where);
        responseJSON($planos);
    } catch(Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if($request_body['functionPage'] === 'getTipoPagamentos') {
    try {
        $condicao = ' WHERE cartao_dcc > 0';
        $dados =  $pagamentoRepository->getTiposPagamentos($condicao);
        $dados = array_map(function($data) {
            $data['descricao'] = $data['cartao_dcc'] == 1 ? 'Cartão de Crédito' : 'Cartão de Débito';
            return $data;
        }, $dados);
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if($request_body['functionPage'] === 'listaTransacoes') {
    try {
        $sDate = "";
        $iTotal = 0;
        $sWhere = " WHERE mkr_login = ".valoresTexto2($_SESSION[CHAVE_CONTRATO]['indicador']['login_usuario']);
        $pagination = [];

        if (isset($request_body['iDisplayStart']) && $request_body['iDisplayLength'] != '-1') {
            $pagination['limit']    = $request_body['iDisplayStart'];
            $pagination['qtd']      = $request_body['iDisplayLength'];
        }

        if(isset($request_body['txtBusca']) && $request_body['txtBusca']) {
            $sWhere.= " and razao_social like '".$request_body['txtBusca']."%' collate Latin1_General_CI_AI";
        }

        if (isset($request_body["inicio"]) && isset($request_body["fim"])) {
            $inicio = valoresData2(escreverData($request_body["inicio"]));
            $fim = valoresDataHora2(escreverData($request_body["fim"]), '23:59:59');
            $sWhere.= " and transactionTimestamp BETWEEN $inicio and $fim ";
        }
        if(isset($request_body['tipo_pagamento']) && $request_body['tipo_pagamento']) {
            $sWhere.= " and id_tipo_documento in (".filtroWhereIn($request_body['tipo_pagamento']).")";
        }

        if(isset($request_body['status']) && $request_body['status']) {
            $sWhere.= " and status in (".filtroWhereIn($request_body['status']).")";
        }

        $data = $pagamentoRepository->getListaTransacoesDCC($sWhere, $pagination);
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $output = [
                "sEcho"                 => intval(filter_input(INPUT_GET, 'sEcho')),
                "aaData"                => $data['aaData'],
                "iTotalRecords"         => $data['total'],
                "iTotalDisplayRecords"  => $data['total']
            ];
            responseJSON($output);
        } else {
            responseJSON($data);
        }
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if($request_body['functionPage'] === 'listaClientes') {
    try {
        $funcionarioId = isset($_SESSION[CHAVE_CONTRATO]['indicador']) ? $_SESSION[CHAVE_CONTRATO]['indicador']['id_fornecedores_despesas'] : null;
        if(!$funcionarioId) {
            http_response_code(403);
            echo json_encode('Você não pode acessar esse recurso');
        }
        $nome = isset($request_body['q']) ? utf8_decode($request_body['q']) : '';
        $usuarioRepository = new UsuarioRepository();
        $condicao = " WHERE id_user_resp = (select top 1 id_usuario from sf_usuarios where funcionario = "
            .valoresSelect2($funcionarioId).") AND tipo in ('C', 'P') AND razao_social like '".$nome."%'";
        $dados = $usuarioRepository->listaUsuarios($condicao);
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}