<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 18/09/2020
 * Time: 15:53
 */

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

$dependenteService = new DependenteService();

if (empty($_SESSION[CHAVE_CONTRATO]['restricted_id']) ){
    throw new Exception('Usuário não autenticado', 401);
}

if ($request_body['functionPage'] === 'getListaDependentes') {
    try {
        $id_usuario = $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $dados = $dependenteService->getInicioDashboard($id_usuario);
        responseJSON($dados);
    } catch (Exception $e) {
       throw new Exception($e->getMessage(), $e->getCode());
    }
}

