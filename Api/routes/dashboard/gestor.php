<?php
$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

$gestorService = new GestorService();

if ($request_body['functionPage'] === 'getClientes') {
  try {
    $sLimit = 20;
    $sQtd = 0;
    if (isset($request_body['iDisplayStart']) && $request_body['iDisplayLength'] != '-1') {
      $sLimit = $request_body['iDisplayStart'];
      $sQtd = $request_body['iDisplayLength'];
    }
    $dados = $gestorService->getClientes($request_body, $sLimit, $sQtd);
    responseJSON($dados);
  } catch (Exception $e) {
    throw new Exception($e->getMessage(), $e->getCode());
  }
}

if ($request_body['functionPage'] === 'getCliente') {
  try {
    $dados = $gestorService->getCliente($request_body);
    responseJSON($dados);
  } catch (Exception $e) {
    throw new Exception($e->getMessage(), $e->getCode());
  }
}