<?php

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

if(empty($_SESSION[CHAVE_CONTRATO]['restricted_id']) ){
   throw new Exception('Usuário não autenticado', 401);
}

$funcionarioService = new FuncionarioService();

if($request_body['functionPage'] == "GetComissaoFuncionario") {
    try {
        $sLimit = 20;
        $sQtd = 0;
        if (isset($request_body['iDisplayStart']) && $request_body['iDisplayLength'] != '-1') {
            $sLimit = $request_body['iDisplayStart'];
            $sQtd = $request_body['iDisplayLength'];
        }
        $dados = $funcionarioService->getComissaoFuncionario($request_body, $sLimit, $sQtd);       
        responseJSON($dados);
    } catch(Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'getProspectLead') {
    try {
        $sLimit = 20;
        $sQtd = 0;
        if (isset($request_body['iDisplayStart']) && $request_body['iDisplayLength'] != '-1') {
            $sLimit = $request_body['iDisplayStart'];
            $sQtd = $request_body['iDisplayLength'];
        }
        $dados = $funcionarioService->getListaProspectsLeads($request_body, $sLimit, $sQtd);       
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'convertLeadProspect') {
    try {
        $formInvalido = ValidationUsuario::validationProspect($request_body);
        if ($formInvalido) {
            throw new Exception(json_encode($formInvalido), 422);
        } 
        $id_usuario = $funcionarioService->convertLeadProspect($request_body);
        responseJSON($id_usuario, 201);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'verificaEmail') {
    try {
        $usuarioRepository = new UsuarioRepository();
        $dados = $usuarioRepository->getUsuarioPorEmail($request_body['email']);
        if ($dados) {
            throw new Exception('Email já cadastrado', 400);
        }
        responseJSON('ok');
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'criarSessaoConsultor') {
    try {
        if (in_array($_SESSION[CHAVE_CONTRATO]['login_ativo'], ['funcionario', 'gestor'])) {
            $funcionarioService->criarSessaoConsultor($request_body);
            responseJSON(['ok']);
        }
        throw new Exception('Não permitido', 403);
    } catch(Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'inativarUsuario') {
    try {
        $funcionarioService->inativarUsuario($request_body);
        responseJSON(['Ok']);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}