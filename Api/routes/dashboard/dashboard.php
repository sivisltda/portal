<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 23/06/2020
 * Time: 13:43
 */

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

if (empty($_SESSION[CHAVE_CONTRATO]['restricted_id']) ){
    throw new Exception('Usuário não autenticado', 401);
}

$usuarioRepository      = new UsuarioRepository();
$veiculoRepository      = new VeiculoRepository();
$planoRepository        = new PlanoRepository();
$mensalidadeRepository  = new MensalidadeRepository();

if ($request_body["functionPage"] == "GetVeiculos"){
    try {
        $id = $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $pagination = [];

        if (isset($request_body['iDisplayStart']) && $request_body['iDisplayLength'] != '-1') {
            $pagination['limit']    = $request_body['iDisplayStart'];
            $pagination['qtd']      = $request_body['iDisplayLength'];
        }
        $data = $veiculoRepository->getVeiculosPorDados("v.id_fornecedores_despesas = ".valoresSelect2($id).
            " AND id_plano is not null AND id_plano is not null", $pagination);
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $output = [
                "sEcho"                 => intval(filter_input(INPUT_GET, 'sEcho')),
                "aaData"                => $data['aaData'],
                "iTotalRecords"         => $data['total'],
                "iTotalDisplayRecords"  => $data['total']
            ];
            responseJSON($output);
        } else {
            responseJSON($data);
        }
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
    exit;
}

if ($request_body["functionPage"] == "getCarteiras") {
    try {
        $request_body['id_usuario'] =  $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $carteiraService = new CarteiraService();
        $dados = $carteiraService->getDadosCarteira($request_body);
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage());
    }
}

if ($request_body["functionPage"] == "GetPlanos") {
    try {
        $id = $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $pagination = [];
        $where = "(favorecido = ".valoresSelect2($id)." 
            OR favorecido IN (SELECT id_titular FROM sf_fornecedores_despesas_dependentes WHERE id_dependente = ".valoresSelect2($id).")) and
            ((DATEADD(day,(select ACA_PLN_DIAS_RENOVACAO from sf_configuracao),A.dt_fim) > GETDATE() and B.parcelar = 0) or
            (A.dt_fim >= GETDATE() and B.parcelar = 1) or ((select COUNT(*) from sf_vendas_planos_mensalidade C where C.id_plano_mens = A.id_plano 
            and dt_pagamento_mens is null) > 0))";

        if (isset($request_body['iDisplayStart']) && $request_body['iDisplayLength'] != '-1') {
            $pagination['limit']    = $request_body['iDisplayStart'];
            $pagination['qtd']      = $request_body['iDisplayLength'];
        }
        $data = $planoRepository->getPlanos($where, $pagination);
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $output = [
                "sEcho"                 => intval(filter_input(INPUT_GET, 'sEcho')),
                "aaData"                => $data['aaData'],
                "iTotalRecords"         => $data['total'],
                "iTotalDisplayRecords"  => $data['total']
            ];
            responseJSON($output);
        } else {
            responseJSON($data);
        }
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if($request_body['functionPage'] === 'GetMensalidades') {
    try {
        $data = [];
        if($plano_id = $request_body['plano_id']) {
            $sWhere = "id_plano_mens = ".valoresSelect2($plano_id);
            $pagination = [];
            if (isset($request_body['iDisplayStart']) && $request_body['iDisplayLength'] != '-1') {
                $pagination['limit']    = $request_body['iDisplayStart'];
                $pagination['qtd']      = $request_body['iDisplayLength'];
            }
            if($status = $request_body['status']) {
                $arrayStatus = array_map(function($s) {
                    return valoresTexto2(trim($s));
                }, explode(',', $status));
                $sWhere.= " and status_mens in (".implode(',', $arrayStatus).")";
            }
            $data   = $mensalidadeRepository->listaMensalidadesPlano($sWhere, $pagination);
            $dados  = $mensalidadeRepository->totalStatusMensalidadePlano("id_plano_mens = ".valoresSelect2($plano_id));
        } else {
            $data = [];
            $total = 0;
        }
        $output = [
            "sEcho" => intval(filter_input(INPUT_GET, 'sEcho')),
            "aaData" => $data['aaData'],
            "totais" => [
                "vencido"   => isset($dados) && isset($dados['vencido']) ? $dados['vencido'] : 0,
                "aberto"    => isset($dados) && isset($dados['aberto']) ? $dados['aberto'] : 0,
                "pago"      => isset($dados) && isset($dados['pago']) ? $dados['pago'] : 0
            ],
            "iTotalRecords" => $data['total'],
            "iTotalDisplayRecords" => $data['total']
        ];
        responseJSON($output);
    } catch(Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}



if($request_body['functionPage'] === 'GetDashboard') {
    try {
        $dashboardService = new DashboardService();
        $dados = $dashboardService->getDashboard();
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if($request_body['functionPage'] === 'getMenu') {
    try {
        $menuService = new MenuService();
        $id = $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $dados = $menuService->makeMenu($id);
       responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'logout') {
    try {
        if ($_SESSION && isset($_SESSION[CHAVE_CONTRATO]['restricted_id'])) {
            unset($_SESSION[CHAVE_CONTRATO]['restricted_id']);       
            unset($_SESSION[CHAVE_CONTRATO]['login_ativo']);       
            unset($_SESSION[CHAVE_CONTRATO]['indicador']);       
            unset($_SESSION[CHAVE_CONTRATO]['nome']);       
          }
          responseJSON([], 204);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}