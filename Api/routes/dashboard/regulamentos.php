<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 23/06/2020
 * Time: 13:49
 */

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

if (empty($_SESSION[CHAVE_CONTRATO]['restricted_id'])) {
    throw new Exception('Usuário não autenticado', 401);
}

$regulamentoService = new RegulamentoService();

if ($request_body['functionPage'] === 'getRegulamentos') {
    try {
        $dados = $regulamentoService->getRegulamentos();
        responseJSON($dados);
    } catch (Exception $e) {
       throw new Exception($e->getMessage(), $e->getCode());
    }
}