<?php

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

$planoService = new PlanoRepository();
$pendenciaService = new PendenciaService();
$veiculoRepository = new VeiculoRepository();
$usuarioRepository = new UsuarioRepository();
$produtoRepository = new ProdutoRepository();
$funcionarioService = new FuncionarioService();

if ($request_body['functionPage'] === 'getListaPendencias') {
    try {
        $dados = $pendenciaService->getListaPendencia($request_body);
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'getListaAcessorios') {
    try {
        $dados = $pendenciaService->getListaAcessorios($request_body);
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'saveComprovante') {
    try {
        $dados = $pendenciaService->saveUploadComprovante($request_body);
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'salvarConvenios') {
    try {
        $pendenciaService->salvarConvenios($request_body);
        responseJSON(['message' => 'convenio criado com sucesso'], 201);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'finalizarAdesao') {
    try {
        $pendenciaService->finalizarAdesao($request_body);
        responseJSON(['message' => 'Pendencia finalizada criado com sucesso']);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'cancelarAdesao') {
    try {
        $pendenciaService->cancelarAdesao($request_body);
        responseJSON(['message' => 'Pendencia cancelada com sucesso']);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'removerConvenio') {
    try {
        $pendenciaService->removerConvenio($request_body);
        responseJSON(['message' => 'Convenio removido com sucesso']);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'enviarContratoWhatsapp') {
    try {
        $pendenciaService->enviarContratoWhatsapp($request_body);
        responseJSON(['message' => 'Contrato enviado com sucesso']);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'enviarVistoriaWhatsapp') {
    try {
        $pendenciaService->enviarVistoriaWhatsapp($request_body);
        responseJSON(['message' => 'Vistoria enviada com sucesso']);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'removerContrato') {
    try {
        $dados = $pendenciaService->removerContrato($request_body);
        responseJSON(['message' => 'Contrato removido com sucesso']);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if($request_body['functionPage'] === 'listaClientes') {
    try {
        $funcionarioId = isset($_SESSION[CHAVE_CONTRATO]['indicador']) ? $_SESSION[CHAVE_CONTRATO]['indicador']['id_fornecedores_despesas'] : null;
        if(!$funcionarioId) {
            http_response_code(403);
            echo json_encode('Você não pode acessar esse recurso');
        }
        $nome = isset($request_body['q']) ? utf8_decode($request_body['q']) : '';
        $usuarioRepository = new UsuarioRepository();
        $condicao = " WHERE id_user_resp = (select top 1 id_usuario from sf_usuarios where funcionario = "
            .valoresSelect2($funcionarioId).") AND tipo in ('C', 'P') AND razao_social like '".$nome."%'";
        $dados = $usuarioRepository->listaUsuarios($condicao);
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'imprimir') {
    $toPrint = [];
    $ids = explode(",", $request_body['id']);
    foreach ($ids as $id) {
        $toPrint[] = imprimir($id);
    }
    makePDF($toPrint);
}

if ($request_body['functionPage'] === 'imprimirRapida') {
    $toPrint = [];
    $produto = $produtoRepository->getProduto("p.conta_produto = " . valoresNumericos2($request_body['id']), 0);
    $franquia = ($produto["cota_defaut_tp"] == "1" ? $produto["cota_defaut"] : ((valoresNumericos2($request_body["valor"]) * $produto["cota_defaut"]) / 100));
    $consultor = $_SESSION[CHAVE_CONTRATO]['consultor'];
    $cupom = $planoService->getCupomById(valoresNumericos2($request_body['id_convenio']), valoresNumericos2($request_body['id']));
    //--------------------------------------------------------------------------
    $totalAdd = 0;
    $totalAdd_acessorios = 0;
    $totalAdd_adicionais = 0;
    $html_acessorios = "";
    $html_adicionais = "";
    //-----------------------adicionais-----------------------------------------    
    $acessorios = $veiculoRepository->getAcessoriosPorIds(explode(",", $request_body['acessorios']));
    foreach ($acessorios as $item) {
        $totalAdd += ($item["tp_preco"] == 0 ? valoresNumericos2($item["preco_venda"]) :
        ($item["tp_preco"] == 1 ? (($produto["segMax"] * valoresNumericos2($item["preco_venda"])) / 100) :
        ((valoresNumericos2($request_body["valor"]) * valoresNumericos2($item["preco_venda"])) / 100)));
    }
    //------------------------parcelas------------------------------------------
    for ($i = 0; count($produto["parcelas"]) > $i; $i++) {
        $valorPlano = (valoresNumericos2($produto["parcelas"][$i]["valor_unico"]) + $totalAdd);
        if (isset($cupom["tp_valor"])) {
            $valorPlanoDesc = escreverNumero(($valorPlano - ($cupom["tp_valor"] == "D" ? valoresNumericos2($cupom["valor"]) : (($valorPlano * valoresNumericos2($cupom["valor"])) / 100))), 1);
        } else {
            $valorPlanoDesc = escreverNumero($valorPlano, 1);
        }
        $produto["parcelas"][$i]["valor_desc_calc"] = $valorPlanoDesc;
    }
    //-------------------------acessorios---------------------------------------
    foreach ($produto["acessorios"] as $item) {
        $verificar = true;
        foreach ($acessorios as $item_check) {
            if ($item["cobertura"] == $item_check["cobertura"] && $item["id"] != $item_check["id"] && $verificar) {
                $verificar = false;
            }
        }
        if ($verificar) {
            $html_acessorios .= '<tr><td> * ' . $item["nome"] . '</td><td style="text-align:right;">' . (NAO_VALOR_PRINT > 0 ? "" : $item["valor"]) . '</td></tr>';
            $totalAdd_acessorios += valoresNumericos2($item["valor"]);
        }
    }
    //--------------------------adicionais--------------------------------------
    foreach ($acessorios as $item) {
        $verificar = true;
        foreach ($produto["acessorios"] as $item_check) {
            if ($item_check["id"] == $item["id"] && $verificar) {
                $verificar = false;
            }
        }
        if ($verificar) {
            $preco_venda = ($item["tp_preco"] == 0 ? valoresNumericos2($item["preco_venda"]) :
                    ($item["tp_preco"] == 1 ? (($produto["segMax"] * valoresNumericos2($item["preco_venda"])) / 100) :
                    ((valoresNumericos2($request_body["valor"]) * valoresNumericos2($item["preco_venda"])) / 100))
                    );
            $html_adicionais .= '<tr><td> * ' . $item["descricao"] . '</td><td style="text-align:right;">' .
                    (NAO_VALOR_PRINT > 0 ? "" : escreverNumero($preco_venda, 0)) . '</td></tr>';
            $totalAdd_adicionais += $preco_venda;
        }
    }
    //--------------------------------------------------------------------------    
    $html = '<html><head></head><body>
    <div style="text-align: center;">
    <span>Valor Médio Mensal:</span><br>';
    if (escreverNumero($valorPlano, 1) != $valorPlanoDesc) {
        $html .= '<span style="text-decoration: line-through;">De ' . escreverNumero($valorPlano, 1) . ' / mês</span><br>';
    }
    $html .= '<span style="font-size: 38px;font-weight: bold;">' . $valorPlanoDesc . '</span><span>/ mês</span><br><br>
        <span>' . MENSAGEM_COTACAO . '</span><br>
        <span>' . MENSAGEM_ADESAO . ': ' . escreverNumero($produto["taxa_adesao"], 1) . '</span><br>
        <span>Cota de Participação: ' . escreverNumero(($produto["seg_piso_franquia"] > $franquia ? $produto["seg_piso_franquia"] : $franquia), 1) . '</span>
    </div><br>
    <table border="0" cellpadding="2" cellspacing="1" style="font-size: 11px; border-top: 1px solid black;">
    <tr><td width="85%">Forma de Pagamento:</td><td width="15%" style="text-align:right;"></td></tr>';
    for ($i = 0; count($produto["parcelas"]) > $i; $i++) {
        $html .= '<tr><td> O ' . getTipoDoc($produto["parcelas"][$i]["parcela"]) . '</td><td style="text-align:right;">' . $produto["parcelas"][$i]["valor_desc_calc"] . '</td></tr>';
    }
    $html .= '</table>
    <div></div>
    <table border="0" cellpadding="1" cellspacing="1" style="font-size: 11px; border-top: 1px solid black;">
        <tr><td width="15%">Placa:</td><td width="45%">' . $request_body["placa"] . '</td><td style="text-align: center;font-weight: bold;" width="40%">' . $request_body["valor"] . '</td></tr> 
        <tr><td width="15%">Marca:</td><td width="45%">' . $request_body["marca"] . '</td><td style="font-size: 8px;text-align: center;" width="40%">de acordo com a tabela FIPE código ' . $request_body["codigo_fipe"] . '</td></tr> 
        <tr><td width="15%">Modelo:</td><td width="45%">' . $request_body["modelo"] . '</td><td style="font-size: 8px;text-align: center;" width="40%">' . $request_body["mes_referencia"] . '.</td></tr> 
        <tr><td width="15%">Ano:</td><td width="45%">' . ($request_body["ano_modelo"] == "32000" ? "Zero km" : $request_body["ano_modelo"]) . '</td><td width="40%"></td></tr> 
    </table> 
    <div></div>   
    <table border="0" cellpadding="1" cellspacing="1" style="font-size: 11px; border-top: 1px solid black;">
    <tr>
        <td width="85%">Coberturas:</td>
        <td width="15%" style="text-align:right;"></td>
    </tr>' . (strlen($produto["descricao"]) > 0 ? '<tr><td>' . $produto["descricao"] . '</td><td></td></tr>' : '') . $html_acessorios . 
    '<tr>
        <td width="85%"><b>Subtotal:</b></td>
        <td width="15%" style="text-align:right;"><b>' . escreverNumero(($valorPlano - $totalAdd_adicionais), 1) . '</b></td>
    </tr></table>
    <table border="0" cellpadding="1" cellspacing="1" style="font-size: 11px; border-top: 1px solid black; border-bottom: 1px solid black;">
    <tr>
        <td width="85%">Adicionais:</td>
        <td width="15%" style="text-align:right;"></td></tr>' . $html_adicionais . 
    '<tr>
        <td width="85%"><b>Subtotal:</b></td>
        <td width="15%" style="text-align:right;"><b>' . escreverNumero($totalAdd_adicionais, 1) . '</b></td></tr></table>            
    <div></div>
    <table border="0" cellpadding="1" cellspacing="1" style="font-size: 12px;">
        <tr>
            <td width="85%"><b>Total Geral:</b></td>
            <td width="15%" style="text-align:right;"><b>' . $valorPlanoDesc . '</b></td>
        </tr>
    </table>
    <div></div>    
    <span style="font-size: 10px;">Observações:</span>
    <br><br><br><br>
    <table cellpadding="1" cellspacing="1" style="text-align: center;font-size: 11px;">
        <tr><td width="45%">_________________________________</td><td width="10%">
        </td><td width="45%">_________________________________</td></tr> 
        <tr><td width="45%"><b>' . MENSAGEM_ASSOCIADO . '</b></td><td width="10%"></td><td width="45%"><b>' . $consultor["nome"] . '</b></td></tr> 
        <tr><td width="45%"></td><td width="10%"></td><td width="45%">' . $consultor["celular"] . '</td></tr> 
    </table>
    </body></html>';
    $toPrint[] = $html;
    makePDF($toPrint);
}

if ($request_body['functionPage'] === 'imprimirLote') {
    $toPrint = [];
    $usuario = $usuarioRepository->getUsuarioPorId(valoresNumericos2($request_body['id']));
    $veiculos = $veiculoRepository->getVeiculosPorDados("v.status not in ('Reprovado') and v.id_fornecedores_despesas = " . valoresNumericos2($request_body['id']), [], true);
    if (in_array($_SESSION[CHAVE_CONTRATO]['login_ativo'], ['funcionario', 'gestor'])) {
        $funcionarioService->criarSessaoConsultor($request_body);
    }
    $consultor = $_SESSION[CHAVE_CONTRATO]['consultor'];
    //echo json_encode($veiculos); exit;
    //-------------------------------------------------------------------------- 
    $valorPlano = 0;
    $html_veiculos = '';
    foreach ($veiculos as $veiculo) {
        //echo json_encode($veiculo); exit;
        $valorPlanoDesc = 0;
        $valor_acessorio = 0;
        $valor_plano = valoresNumericos2($veiculo["valor_plano_item"]);
        foreach ($veiculo["acessorios"] as $acessorio) {
            $valor_acessorio += valoresNumericos2($acessorio["preco_venda"]);
        }
        if (isset($veiculo["cupom"]["tp_valor"])) {
            $valorPlanoDesc = (($valor_plano + $valor_acessorio) - ($veiculo["cupom"]["tp_valor"] == "D" ?
                    valoresNumericos2($veiculo["cupom"]["valor"]) : ((($valor_plano + $valor_acessorio) * valoresNumericos2($veiculo["cupom"]["valor"])) / 100)));
        } else {
            $valorPlanoDesc = ($valor_plano + $valor_acessorio);
        }
        $html_veiculos .= '<tr>
            <td>' . $veiculo["placa"] . '</td>
            <td>' . $veiculo["marca"] . '</td>
            <td>' . $veiculo["modelo"] . '</td>
            <td>' . ($veiculo["ano_modelo"] == "32000" ? "Zero km" : $veiculo["ano_modelo"]) . '</td>
            <td style="text-align: right;">' . escreverNumero($valorPlanoDesc, 1) . '</td>
        </tr>';
        $valorPlano += $valorPlanoDesc;
    }
    //--------------------------------------------------------------------------     
    $html = '<html><head></head><body>
    <div style="text-align: center;">
    <span>Valor mensal da sua cotação:</span><br>';
    $html .= '<span style="font-size: 38px;font-weight: bold;">' . escreverNumero($valorPlano, 1) . '</span><span>/ mês</span><br><br>
        <span>' . MENSAGEM_COTACAO . '</span><br>
    </div>
    <table border="1" cellpadding="4" style="font-size: 11px; width: 100%;">
        <tr><td width="25%" style="font-weight: bold;">Nome:</td><td width="75%">' . $usuario["razao_social"] . '</td></tr> 
        <tr><td style="font-weight: bold;">CNPJ:</td><td>' . $usuario["cnpj"] . '</td></tr> 
    </table> 
    <div></div>
    <table border="1" cellpadding="4" style="font-size: 11px; width: 100%; margin-top: 15px;">
        <tr>
            <td width="15%"><b>Placa</b></td>
            <td width="20%"><b>Marca</b></td>
            <td width="40%"><b>Modelo</b></td>
            <td width="10%"><b>Ano</b></td>
            <td width="15%"><b>Valor</b></td>
        </tr>';
    $html .= $html_veiculos;
    $html .= '<tr>
            <td colspan="4">Valor Total Mensal</td>
            <td style="text-align: right;">' . escreverNumero($valorPlano, 1) . '</td>
        </tr>
        <tr>
            <td colspan="4">Valor Total Anual</td>
            <td style="text-align: right;"><b>' . escreverNumero((12 * $valorPlano), 1) . '</b></td>
        </tr>
    </table> 
    <div></div>
    <span style="font-size: 10px;font-weight: bold;">Observações:</span><br><br><br><br>
    <table cellpadding="1" cellspacing="1" style="text-align: center;font-size: 11px;">
        <tr><td width="45%">_________________________________</td><td width="10%">
        </td><td width="45%">_________________________________</td></tr> 
        <tr><td width="45%"><b>' . MENSAGEM_ASSOCIADO . '</b></td><td width="10%"></td><td width="45%"><b>' . $consultor["nome"] . '</b></td></tr> 
        <tr><td width="45%">' . $usuario["razao_social"] . '</td><td width="10%"></td><td width="45%">' . $consultor["celular"] . '</td></tr> 
    </table>
    </body></html>';
    $toPrint[] = $html;
    makePDF($toPrint);    
}

function imprimir($id) {
    $veiculoRepository = new VeiculoRepository();
    $usuarioRepository = new UsuarioRepository();
    $produtoRepository = new ProdutoRepository();        
    $veiculo = $veiculoRepository->getVeiculosPorDados('v.id = ' . valoresNumericos2($id), [], true)[0];
    $usuario = $usuarioRepository->getUsuarioPorId($veiculo["id_fornecedores_despesas"]);
    $produto = $produtoRepository->getProdutosSeguro($veiculo, " and p.conta_produto = " . $veiculo["produto_id"])[0];
    $consultor = $_SESSION[CHAVE_CONTRATO]['consultor'];
    //--------------------------------------------------------------------------
    $totalAdd = 0;
    $totalAdd_acessorios = 0;
    $totalAdd_adicionais = 0;
    $html_acessorios = "";
    $html_adicionais = "";
    //-----------------------adicionais-----------------------------------------        
    foreach ($veiculo["acessorios"] as $item) {
        $totalAdd += valoresNumericos2($item["preco_venda"]);
    }
    //------------------------parcelas------------------------------------------
    for ($i = 0; count($produto["parcelas"]) > $i; $i++) {
        $valorPlano = (valoresNumericos2($produto["parcelas"][$i]["valor_unico"]) + $totalAdd);
        if (isset($veiculo["cupom"]["tp_valor"])) {
            $valorPlanoDesc = escreverNumero(($valorPlano - ($veiculo["cupom"]["tp_valor"] == "D" ? valoresNumericos2($veiculo["cupom"]["valor"]) : (($valorPlano * valoresNumericos2($veiculo["cupom"]["valor"])) / 100))), 1);
        } else {
            $valorPlanoDesc = escreverNumero($valorPlano, 1);
        }
        $produto["parcelas"][$i]["valor_desc_calc"] = $valorPlanoDesc;
    }
    //-------------------------acessorios---------------------------------------    
    foreach ($produto["acessorios"] as $item) {
        $verificar = true;
        foreach ($veiculo["acessorios"] as $item_check) {
            if ($item["cobertura"] == $item_check["parent"] && $item["id"] != $item_check["id_servico_acessorio"] && $verificar) {
                $verificar = false;
            }
        }
        if ($verificar) {
            $html_acessorios .= '<tr><td> * ' . $item["nome"] . '</td><td style="text-align:right;">' .
                    (NAO_VALOR_PRINT > 0 ? "" : $item["valor"]) . '</td></tr>';
            $totalAdd_acessorios += valoresNumericos2($item["valor"]);
        }
    }
    //--------------------------adicionais--------------------------------------
    foreach ($veiculo["acessorios"] as $item) {
        $verificar = true;
        foreach ($produto["acessorios"] as $item_check) {
            if ($item_check["id"] == $item["id_servico_acessorio"] && $verificar) {
                $verificar = false;
            }
        }
        if ($verificar) {
            $html_adicionais .= '<tr><td> * ' . $item["descricao"] . '</td><td style="text-align:right;">' .
                    (NAO_VALOR_PRINT > 0 ? "" : $item["preco_venda"]) . '</td></tr>';
            $totalAdd_adicionais += valoresNumericos2($item["preco_venda"]);
        }
    }
    //--------------------------------------------------------------------------    
    $html = '<html><head></head><body>
    <div style="text-align: center;">
    <span>Valor Médio Mensal:</span><br>';
    if (escreverNumero($valorPlano, 1) != $valorPlanoDesc) {
        $html .= '<span style="text-decoration: line-through;">De ' . escreverNumero($valorPlano, 1) . ' / mês</span><br>';
    }
    $html .= '<span style="font-size: 38px;font-weight: bold;">' . $valorPlanoDesc . '</span><span>/ mês</span><br><br><span>' . MENSAGEM_COTACAO . '</span><br>';
    if (valoresNumericos2($veiculo["valor_desc_adesao"]) > 0) {
        $html .= '<span style="text-decoration: line-through;">' . MENSAGEM_ADESAO . ': ' . $produto["taxa_adesao"] . '</span><br>';    
    }
    if ((valoresNumericos2($produto["taxa_adesao"]) - valoresNumericos2($veiculo["valor_desc_adesao"])) > 0) {
        $html .= '<span>' . MENSAGEM_ADESAO . ': ' . escreverNumero((valoresNumericos2($produto["taxa_adesao"]) - valoresNumericos2($veiculo["valor_desc_adesao"])), 1) . '</span><br>';    
    }
    $html .= '<span>Cota de Participação: ' . escreverNumero(($veiculo["franquia_tp"] == "1" ? $veiculo["franquia"] : ((valoresNumericos2($veiculo["valor"]) * $veiculo["franquia"]) / 100)), 1) . '</span></div><br>       
    <table border="0" cellpadding="2" cellspacing="1" style="font-size: 11px; border-top: 1px solid black;">
    <tr><td width="85%">Forma de Pagamento:</td><td width="15%" style="text-align:right;"></td></tr>';
    for ($i = 0; count($produto["parcelas"]) > $i; $i++) {
        $html .= '<tr><td> O ' . getTipoDoc($produto["parcelas"][$i]["parcela"]) . '</td><td style="text-align:right;">' . $produto["parcelas"][$i]["valor_desc_calc"] . '</td></tr>';
    }
    $html .= '</table>
    <div></div>    
    <table border="0" cellpadding="1" cellspacing="1" style="font-size: 11px; border-top: 1px solid black;">
        <tr><td width="15%">Nome:</td><td width="45%">' . $usuario["razao_social"] . '</td><td width="40%"></td></tr> 
        <tr><td width="15%">CPF:</td><td width="45%">' . $usuario["cnpj"] . '</td><td style="text-align: center;font-weight: bold;" width="40%">' . $veiculo["valor"] . '</td></tr>             
        <tr><td width="15%">Placa:</td><td width="45%">' . $veiculo["placa"] . '</td><td style="font-size: 8px;text-align: center;" width="40%">de acordo com a tabela FIPE código ' . $veiculo["codigo_fipe"] . '</td></tr> 
        <tr><td width="15%">Marca:</td><td width="45%">' . $veiculo["marca"] . '</td><td style="font-size: 8px;text-align: center;" width="40%">' . $veiculo["mes_referencia"] . '.</td></tr> 
        <tr><td width="15%">Modelo:</td><td width="45%">' . $veiculo["modelo"] . '</td><td style="font-size: 8px;text-align: center;" width="40%"></td></tr> 
        <tr><td width="15%">Ano:</td><td width="45%">' . ($veiculo["ano_modelo"] == "32000" ? "Zero km" : $veiculo["ano_modelo"]) . '</td><td width="40%"></td></tr> 
    </table>
    <div></div>    
    <table border="0" cellpadding="1" cellspacing="1" style="font-size: 11px; border-top: 1px solid black;">
    <tr>
        <td width="85%">Coberturas:</td>
        <td width="15%" style="text-align:right;"></td>
    </tr>' . (strlen($produto["descricao"]) > 0 ? '<tr><td>' . $produto["descricao"] . '</td><td></td></tr>' : '') . $html_acessorios . 
    '<tr>
        <td width="85%"><b>Subtotal:</b></td>
        <td width="15%" style="text-align:right;"><b>' . escreverNumero(($valorPlano - $totalAdd_adicionais), 1) . '</b></td>
    </tr></table>
    <table border="0" cellpadding="1" cellspacing="1" style="font-size: 11px; border-top: 1px solid black; border-bottom: 1px solid black;">
    <tr>
        <td width="85%">Adicionais:</td>
        <td width="15%" style="text-align:right;"><b></b></td></tr>' . $html_adicionais . 
    '<tr>
        <td width="85%"><b>Subtotal:</b></td>
        <td width="15%" style="text-align:right;"><b>' . escreverNumero($totalAdd_adicionais, 1) . '</b></td>
    </tr></table>
    <div></div> 
    <table border="0" cellpadding="1" cellspacing="1" style="font-size: 12px;">
        <tr>
            <td width="85%"><b>Total Geral:</b></td>
            <td width="15%" style="text-align:right;"><b>' . $valorPlanoDesc . '</b></td>
        </tr>
    </table>
    <div></div>            
    <span style="font-size: 10px;">Observações:</span><br><br><br><br>
    <table cellpadding="1" cellspacing="1" style="text-align: center;font-size: 11px;">
        <tr><td width="45%">_________________________________</td><td width="10%">
        </td><td width="45%">_________________________________</td></tr> 
        <tr><td width="45%"><b>' . MENSAGEM_ASSOCIADO . '</b></td><td width="10%"></td><td width="45%"><b>' . $consultor["nome"] . '</b></td></tr> 
        <tr><td width="45%">' . $usuario["razao_social"] . '</td><td width="10%"></td><td width="45%">' . $consultor["celular"] . '</td></tr> 
    </table>
    </body></html>';
    return $html;
}

function getTipoDoc($tipo) {
    switch ($tipo) {
        case 1:
            return "Mensal";
        case 0:
            return "Cartão de Crédito";
        case -1:
            return "Boleto Recorrente";
        case -2:
            return "Pix Recorrente";
        default:
            return $tipo . " Meses";
    }
}

function makePDF($htmls) {        
    require_once(__DIR__ . './../../utils/Print/examples/tcpdf_include.php');
    require_once(__DIR__ . './../../utils/Print/examples/fpdi2/src/autoload.php');
    
    if (!file_exists(__DIR__ . '/tmp/' . CONTRATO)) {
        mkdir(__DIR__ . '/tmp/' . CONTRATO, 0777, true);
    }  
    
    class MYPDF extends \setasign\Fpdi\Tcpdf\Fpdi {
        public function Header() {                                  
            $image = @file_get_contents(LOGO_URL);
            if ($image != '') {
                file_put_contents(__DIR__ . '/tmp/' . CONTRATO . '/' . CONTRATO . '.png', $image);
            }
            $this->Image(__DIR__ . '/tmp/' . CONTRATO . '/' . CONTRATO . '.png', 10, 10, 50, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            $this->SetFont('helvetica', 'B', 12);
            $this->Cell(0, 15, 'PROPOSTA DE ADESÃO', 0, false, 'R', 0, '', 0, false, 'M', 'M');
            $this->Ln(5);
            $this->SetFont('', '', 8);
            $this->Cell(0, 15, date("d/m/Y H:i"), 0, false, 'R', 0, '', 0, false, 'M', 'M');
        }
    }
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->setCreator(PDF_CREATOR);
    $pdf->setTitle('Imprimir');
    $pdf->setSubject('Imprimir');
    $pdf->setHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->setDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->setMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->setAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $pdf->setFontSubsetting(true);
    $pdf->SetFont('', '', 10);
    addPdfPage($pdf, 'anterior.pdf');    
    foreach ($htmls as $html) {
        $pdf->SetPrintHeader();
        $pdf->AddPage();
        addPdfImage($pdf, 'marca-d-agua.png');    
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);        
    }    
    addPdfPage($pdf, 'posterior.pdf');
    $pdf->Output('imprimir.pdf', 'I');
}

function addPdfPage($pdf, $file) {
    $pdf_file = @file_get_contents(ERP_URL . "Pessoas/" . CONTRATO . "/Cotacao/" . $file);
    if ($pdf_file != '') {
        file_put_contents(__DIR__ . '/tmp/' . CONTRATO . '/' . $file, $pdf_file);
        $pdf->SetPrintHeader(false);
        $pages = $pdf->setSourceFile(__DIR__ . '/tmp/' . CONTRATO . '/' . $file);
        for ($i = 0; $i < $pages; $i++) {
            $pdf->AddPage();
            $tplIdx = $pdf->importPage($i + 1);
            $pdf->useTemplate($tplIdx, 0, 0, 210);
        }
    }
    return $pdf;
}

function addPdfImage($pdf, $file) {
    $pdf_img = @file_get_contents(ERP_URL . "Pessoas/" . CONTRATO . "/Cotacao/" . $file);
    if ($pdf_img != '') {    
        file_put_contents(__DIR__ . '/tmp/' . CONTRATO . '/' . $file, $pdf_img);
        $bMargin = $pdf->getBreakMargin();
        $auto_page_break = $pdf->getAutoPageBreak();
        $pdf->SetAutoPageBreak(false, 0);
        $pdf->Image(__DIR__ . '/tmp/' . CONTRATO . '/' . $file, 0, 30, 210, 270, '', '', '', false, 300, '', false, false, 0);
        $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
        $pdf->setPageMark();
    }
    return $pdf;
}