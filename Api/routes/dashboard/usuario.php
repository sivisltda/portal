<?php

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

if (empty($_SESSION[CHAVE_CONTRATO]['restricted_id']) ){
    throw new Exception('Usuário não autenticado', 401);
}

$usuarioRepository = new UsuarioRepository();

if($request_body["functionPage"] === "getUsuario") {
    try {
        $id = $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $dados = $usuarioRepository->getUsuarioPorId($id);
        $dados['cnpj'] = $dados['cnpj'] ? substr($dados['cnpj'], 0,1).'xx.xxx.xxx-'.substr($dados['cnpj'], -2) : '';
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body["functionPage"] == "atualizarUsuario"){
    try {
        $id = (int) $_SESSION[CHAVE_CONTRATO]['restricted_id'];
        $request_body['id'] = $id;
        $formInvalido = ValidationUsuario::ValidationPerfil($request_body);
        if ($formInvalido) {
            throw new Exception(json_encode($formInvalido), 422);
        } 
        $usuarioRepository->salvarUsuario($request_body, $id);
        responseJSON('ok');
    } catch(Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === "trocarSessao") {
    try {
        if($request_body['tipo'] === 'funcionario') {
            $_SESSION[CHAVE_CONTRATO]['login_ativo'] = 'funcionario';
        } else {
            $_SESSION[CHAVE_CONTRATO]['login_ativo'] = 'cliente';
        }
        responseJSON('Ok');
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}
