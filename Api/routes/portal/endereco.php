<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 23/06/2020
 * Time: 14:21
 */

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

$enderecoRepository = new EnderecoRepository();

if ($request_body["functionPage"] === "CarregaEstado") {
    $listaEstados = $enderecoRepository->getEstados();
    responseJSON($listaEstados);
}

if ($request_body["functionPage"] === "carregaCidade") {
    $listaCidades = $enderecoRepository->getCidades($request_body["estado"]);
    responseJSON($listaCidades);
}