<?php
  $request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
  if (!$request_body) {
      $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
  }

  $campanhaService = new CampanhaService();

  if ($request_body['functionPage'] === 'verificaLead') {
    try {
      $condicao = "";
      if (isset($request_body['email'])) {
        $condicao = "c.tipo_contato = 2 and conteudo_contato = ".valoresTexto2($request_body['email']);
      } else if (isset($request_body['celular'])) {
        $condicao = "c.tipo_contato = 1 and conteudo_contato = ".valoresTexto2($request_body['celular']);
      }
      $dados = $campanhaService->verifyIndicador($condicao);
      responseJSON(['id' => $dados['id_fornecedores_despesas'], 'razao_social' => $dados['razao_social']]); 
    } catch (Exception $e) {
      throw new Exception($e->getMessage(), $e->getCode());
    }
  } else if ($request_body['functionPage'] === 'salvarLead') {
    try {
      $formInvalido = ValidationLead::validation($request_body);
      if ($formInvalido) {
        throw new Exception(json_encode($formInvalido), 422);
      } 
      $id = $campanhaService->salvarLead($request_body);
      responseJSON($id, 201);
    } catch (Exception $e) {
      throw new Exception($e->getMessage(), $e->getCode());
    }
  } else if ($request_body['functionPage'] === 'getDadosLead') {
    try {
        $dados = $campanhaService->getDadosLeads($request_body);
        responseJSON($dados);
    } catch (Exception $e) {
      throw new Exception($e->getMessage(), $e->getCode());
    }
  }