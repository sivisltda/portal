<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 23/06/2020
 * Time: 14:15
 */

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

$veiculoRepository = new VeiculoRepository();
$veiculoService = new VeiculoService();

if ($request_body['functionPage'] === 'BuscaPlaca') {
    try {
        $usuario_id = isset($_SESSION[CHAVE_CONTRATO]['usuario']) ? $_SESSION[CHAVE_CONTRATO]['usuario'] : null;
        $dados = $veiculoRepository->verificaPlaca($request_body['placa'], $usuario_id);
        if($dados['status'] && count($dados['data'])) {
            responseJSON($dados);
        }else if($dados['status']) {
            $dados['data'] = $veiculoService->getInformacoesVeiculoPlaca($request_body['placa']);
            if(isset($dados['data']['placa'])) {
                responseJSON($dados);
            } else {
                throw new Exception(json_encode($dados), 404);
            }
        } else {
            throw new Exception('Veiculo já cadastrado', 403);
        }
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}
