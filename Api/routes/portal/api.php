<?php

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

$usuarioRepository  = new UsuarioRepository();
$produtoRepository  = new ProdutoRepository();
$planoRepository    = new PlanoRepository();
$planoService       = new PlanoService();
$pagamentoService   = new PagamentoService();

if ($request_body['functionPage'] === 'buscarUsuario') {
    try {
        $dados = [];
        if ($usuario = $usuarioRepository->getUsuarioPorEmail($request_body['email'])) {
            $dados['usuario']  = $usuario;
            $dados['planos']   = $planoRepository->getPlanos('favorecido = '.valoresSelect2($usuario['id_fornecedores_despesas']));
        }
        if (isset($request_body['produtos']) && $request_body['produtos']) {
            $dados['produtos'] = $produtoRepository->listaProdutos("P.tipo in ('P', 'L', 'C') and P.inativa = 0 and conta_produto > 0", 0);
        }
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'buscarProdutos') {
    try {
        $dados = [
            'produtos' => $produtoRepository->listaProdutos("P.tipo in ('P', 'L', 'C') and P.inativa = 0 and conta_produto > 0", 0)
        ];
        responseJSON($dados);
    } catch(Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'salvarUsuario') {
    try {
        $data = $request_body;
        $data['id_usuario'] = $usuarioRepository->salvarUsuario($data);      
        if (isset($data['id_produto']) && $data['id_produto']) {
            $dados = $planoService->criarPlano($data);
            responseJSON(array_merge($dados, ['id_fornecedor_despesa' => $data['id_usuario']]));
        } else {
            responseJSON(['id_fornecedor_despesa' => $data['id_usuario']]);
        }
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if($request_body['functionPage'] === 'salvarPlano') {
    try {
        $data = $request_body;
        $data['id_usuario'] = $request_body['id_usuario'] ? $request_body['id_usuario'] :
            (isset($_SESSION[CHAVE_CONTRATO]['restricted_id']) ? $_SESSION[CHAVE_CONTRATO]['restricted_id'] : null);
        if (isset($data['id_produto']) && $data['id_produto'] && $data['id_usuario']) {
            $dados = $planoService->criarPlano($data);
            responseJSON($dados);
        } else {
            throw new Exception('Paramêtro inválido', 400);
        }
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if($request_body['functionPage'] === 'processarPagamento') {
    try {
        $data = $request_body;
        $dados = $pagamentoService->processarPagamento($data);
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if($request_body['functionPage'] === 'criarCredencial') {
    try {
        $data = $request_body;
        $credencialService = new CredencialService();
        $dados = $credencialService->verificaCriaCredencial($data);
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if ($request_body['functionPage'] === 'cancelarCredencial') {
    try {
        $data = $request_body;
        $credencialService = new CredencialService();
        $credencialService->cancelaCredencial($data);
        responseJSON('ok');
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if ($request_body['functionPage'] === 'cancelarPlano') {
    try {
        $request_body['id_usuario'] = $request_body['id_usuario'] ? $request_body['id_usuario'] :
        (isset($_SESSION[CHAVE_CONTRATO]['restricted_id']) ? $_SESSION[CHAVE_CONTRATO]['restricted_id'] : null); 
        if (!$request_body['id_usuario']) {
            throw new Exception('Usuario não informado', 400);
        }
        $planoService->cancelarPlano($request_body);
        responseJSON('ok');
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}