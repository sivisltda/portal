<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 18/09/2020
 * Time: 15:53
 */

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}


$dependenteRepository = new DependenteRepository();
$dependenteService = new DependenteService();

if ($request_body['functionPage'] === 'getListaDependentes') {
    try {
        $id_titular = ($request_body['dashboard'] && isset($_SESSION[CHAVE_CONTRATO]['restricted_id'])) ? $_SESSION[CHAVE_CONTRATO]['restricted_id'] 
        :(isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario']) ? $_SESSION[CHAVE_CONTRATO]['usuario'] : null);
        if (!$id_titular) {
           throw new Exception('Usuário não logado', 401);
        }
        $dados = $dependenteService->getListaDependentes($id_titular);
        responseJSON($dados);
    } catch (Exception $e) {
       throw new Exception($e->getMessage(), $e->getCode());
    }

}

if ($request_body['functionPage'] === 'cadastrarDependentes') {
    try {
        $dados = $request_body;
        $dados['id_titular'] = ($request_body['dashboard'] && isset($_SESSION[CHAVE_CONTRATO]['restricted_id'])) ? $_SESSION[CHAVE_CONTRATO]['restricted_id'] 
        :(isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario']) ? $_SESSION[CHAVE_CONTRATO]['usuario'] : null);
        if($dados['id_titular']) {
            $formInvalido = ValidationDependente::validation($request_body);
            if ($formInvalido) {
              throw new Exception(json_encode($formInvalido), 422);
            } 
            $id_dependente = $dependenteService->criarDependente($dados);
            responseJSON($id_dependente);
        }
        throw new Exception('Usuário não logado', 401);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'verificaDadosDependente') {
    try {
        $dados = $dependenteService->verificaDadosDependente($request_body);
        echo json_encode($dados);
    } catch (Exception $e) {
        http_response_code($e->getCode());
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
}

if ($request_body['functionPage'] === 'removeDependente') {
    try {
        $dados = $request_body;
        $dados['id_titular'] = ($request_body['dashboard'] && isset($_SESSION[CHAVE_CONTRATO]['restricted_id'])) ? $_SESSION[CHAVE_CONTRATO]['restricted_id'] 
        :(isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario']) ? $_SESSION[CHAVE_CONTRATO]['usuario'] : null);
        $dependenteService->removeDependente($dados);
        responseJSON([], 204);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}