<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 23/06/2020
 * Time: 14:20
 */

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

$planoRepository = new PlanoRepository();
$planoService = new PlanoService();

if ($request_body['functionPage'] === 'salvarPlano') {
    try {
        if (isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario'])) {
            $request_body['id_usuario'] = $_SESSION[CHAVE_CONTRATO]['usuario'];
            $planoService = new PlanoService();
            $mensalidade  = $planoService->criarPlano($request_body);
            $usuarioRepository = new UsuarioRepository();
            foreach (['email_marketing', 'celular_marketing'] as $item) {
                if (isset($request_body[$item])) {
                    $usuarioRepository->atualizaMarketing($item,  $request_body[$item], $request_body['id_usuario']);
                }
            }
            responseJSON($mensalidade); 
        }
        throw new Exception('Usuário não logado', 401);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body["functionPage"] === 'cancelarCupom') {
    try {
        $request_body['id_usuario'] = isset($request_body['id_usuario']) ? $request_body['id_usuario'] :
        (isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario']) ? $_SESSION[CHAVE_CONTRATO]['usuario'] : null); 
        if (!$request_body['id_usuario']) {
            throw new Exception('Usuario não informado', 400);
        }
        $planoService->cancelarCupom($request_body);
        responseJSON('ok');
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body["functionPage"] === 'verificarCupom') {
    try {
        /*$request_body['id_usuario'] = isset($request_body['id_usuario']) ? $request_body['id_usuario'] :
        (isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario']) ? $_SESSION[CHAVE_CONTRATO]['usuario'] : null); 
        if (!$request_body['id_usuario']) {
            throw new Exception('Usuario não informado', 400);
        }*/
        $cupom = $planoService->verificarCupom($request_body);
        responseJSON($cupom);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'salvarAcessorios') {
    try {
        if (!(isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario']))) {
            throw new Exception('Usuário não logado', 401);
        }
        $acessorios = $planoRepository->criarPlanoAccessorios($request_body);
        responseJSON($acessorios);
    } catch(Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body["functionPage"] === 'salvarCartao') {
    try {
        if (isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario'])) {
            $planoRepository->criarCartaoUsuario($_SESSION[CHAVE_CONTRATO]['usuario'], $request_body);
            responseJSON(true);
        }
        throw new Exception('Usuário não logado', 401);
    } catch(Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body["functionPage"] === 'CancelarPlano') {
    try {
        $request_body['id_usuario'] = isset($request_body['id_usuario']) ? $request_body['id_usuario'] :
        (isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario']) ? $_SESSION[CHAVE_CONTRATO]['usuario'] : null); 
        if (!$request_body['id_usuario']) {
            throw new Exception('Usuario não informado', 400);
        }
        $planoService->cancelarPlano($request_body);
        responseJSON('ok');
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}