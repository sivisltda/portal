<?php

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

$usuarioService = new UsuarioService();
$usuarioRepository = new UsuarioRepository();

if ($request_body['functionPage'] === 'buscarUsuario') {
    try {
        if ($dados = $usuarioService->buscarUsuario($request_body)) {
            if (isset($dados['id_fornecedores_despesas']) || isset($dados['id'])) {
                $_SESSION[CHAVE_CONTRATO]['usuario'] = isset($dados['id_fornecedores_despesas']) ? $dados['id_fornecedores_despesas'] : $dados['id'];
            }
            responseJSON($dados);
        }
        unset($_SESSION[CHAVE_CONTRATO]['usuario']);
        throw new Exception('Usuário não encontrado', 404);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'buscarDadosUsuario') {
    try {
        if (!(isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario']))) {
          throw new Exception('Usuário não logado', 403);
        }
        $request_body['id_usuario'] = $_SESSION[CHAVE_CONTRATO]['usuario'];
        $dados = $usuarioService->buscarDadosUsuario($request_body);
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'salvarUsuario') {
    try {
        if (!isset($request_body['id']) && isset($_SESSION[CHAVE_CONTRATO]['usuario'])) {
            $request_body['id'] =  $_SESSION[CHAVE_CONTRATO]['usuario'];
        }
       $formInvalido = ValidationUsuario::validation($request_body);
       if ($formInvalido) {
           throw new Exception(json_encode($formInvalido), 422);
       } 
       if ($dados = $usuarioService->salvarUsuario($request_body)) {
            responseJSON($dados);
       }
       throw new Exception('Ocorreu um erro ao salvar os dados do usuário');
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if ($request_body['functionPage'] === 'atualizarUsuario') {
    try {
        // var_dump($request_body); exit;
        if ((isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario']))) {
            if (!(isset($request_body['id']) && $request_body['id'])) {
                $request_body['id'] = $_SESSION[CHAVE_CONTRATO]['usuario'];
            }
            $formInvalido = ValidationUsuario::validationUpdate($request_body);
            if ($formInvalido) {
                throw new Exception(json_encode($formInvalido), 422);
            } 
            if ($dados = $usuarioService->salvarUsuario($request_body)) {
                responseJson($dados);
            }
        }
        throw new Exception('Usuário não logado', 403);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body["functionPage"] === "restrictedArea") {
    try {
        $formInvalido = [];
        $err = [];
        $senha = strrev($request_body["password"]);
        $formInvalido["email"] = Validation::validaEmail($request_body["email"]);
        $formInvalido["password"] = Validation::validaCpf($senha);
        foreach ($formInvalido as $key => $value) {
            if (!$value) {
                $err[$key] = $value;
            }
        }
        if ($err) {
            throw new Exception(json_encode($err), 422);
        } else {
            if ($usuarioService->login($request_body['email'], $senha)) {
                responseJSON(true);
            }
            throw new Exception('Credenciais inválidas', 401);
        }
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'ConsultarUsuario') {
    try {
        $email = isset($request_body['email']) ? $request_body['email'] : null;
        $cnpj  = isset($request_body['cnpj']) ? $request_body['cnpj'] : null;
        if ($email && !Validation::validaEmail($email)) {
            throw new Exception('E-mail inválido', 400);
        }
        if ($cnpj && !Validation::validaCpf($cnpj)) {
            throw new Exception('CPF/CNPJ inválido', 400);
        }
        $id_usuario = (isset($request_body['usuario']) && isset($_SESSION[CHAVE_CONTRATO]) 
        && isset($_SESSION[CHAVE_CONTRATO]['usuario'])) 
        ? $_SESSION[CHAVE_CONTRATO]['usuario'] : null;
        if ($usuarioRepository->selecionaUsuarioPorEmailOuCPF($email, $cnpj, $id_usuario)) {
            throw new Exception('Já utilizado', 400);
        }
        responseJSON(['msg' => 'ok']);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'ConsultaUsuarioValidador') {
    try {
        $dados = $usuarioService->getConsultaDadosValidador($request_body);
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if($request_body['functionPage'] === 'listaClientes') {
    try {
        $dados = $usuarioService->consultaClientes($request_body);
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}