<?php

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

$produtoService = new ProdutoService();

if ($request_body["functionPage"] === "listaProduto") {
    try {
        $dados = $produtoService->getListaProduto($request_body);
        responseJSON($dados);
    } catch(Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if ($request_body['functionPage'] === "itemProduto") {
    try {
        $dados = $produtoService->getItemProduto($request_body);
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if ($request_body['functionPage'] === 'buscarServicos') {
    try {
        $request_body['usuario'] = (isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario'])) 
        ? $_SESSION[CHAVE_CONTRATO]['usuario'] : null;
        $lista = $produtoService->getProdutoServicoSeguro($request_body); 
        responseJSON($lista);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'buscarCoberturas') {
    try {
        $lista = $produtoService->getCoberturas($request_body);
        responseJSON($lista);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if($request_body['functionPage'] === 'buscarCores') {
    try {
        $produtoRepository = new ProdutoRepository();
        $dados = $produtoRepository->getCores();
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if($request_body['functionPage'] === 'buscarCambio') {
    try {        
        $dados = [];
            $row = [];
            $row["id"] = "1";
            $row["nome"] = "Manual";
        $dados[] = $row;         
            $row = [];
            $row["id"] = "2";
            $row["nome"] = "Automático";
        $dados[] = $row;         
            $row = [];
            $row["id"] = "3";
            $row["nome"] = "Semi-Automático";
        $dados[] = $row;         
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}