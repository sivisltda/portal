<?php

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

$crmService = new CRMService();
$whatsAppService = new WhatsAppService();

if ($request_body["functionPage"] === "salvarLead") {
    try {
        $formInvalido = ValidationLead::validationFormLead($request_body);
        if ($formInvalido) {
            throw new Exception(json_encode($formInvalido), 422);
        } 
        $id = $crmService->salvarLead($request_body);
        responseJSON(['id' => $id]); 
    } catch(Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body["functionPage"] === 'getModeloWhats') {
    try {
        $modelo = $whatsAppService->getModelo();
        responseJSON(['data' => $modelo]);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body["functionPage"] === "enviaWhatsApp") {
    try {
        $id = $whatsAppService->salvarWhatsApp($request_body);
        responseJSON(['id' => $id]);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

