<?php

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

$portalService = new PortalService();

if ($request_body['functionPage'] === 'getInfo') {
    try {
        $data = $portalService->getInfo();
        $data['isLogged'] = $_SESSION && (isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario']));
        responseJSON($data);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}

if ($request_body['functionPage'] === 'getProcedencia') {
    try {
        $configRepository = new ConfigRepository();
        $dados = $configRepository->getProcedencia();
        responseJSON($dados);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if ($request_body['functionPage'] === 'logout') {
    try {
        if ($_SESSION && (isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario']))) {
          unset($_SESSION[CHAVE_CONTRATO]['usuario']);       
        }
        responseJSON([], 204);
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}


if ($request_body['functionPage'] === 'verificaSessao') {
    try {    
        $portalService->verificaSessao($request_body);
        responseJSON('ok');
    } catch (Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }
}