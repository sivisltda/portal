<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 23/06/2020
 * Time: 14:20
 */

$request_body = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$request_body) {
    $request_body = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

$pagamentoService = new PagamentoService();

if ($request_body['functionPage'] === 'processarPagamento') {
    try {
        $data = $request_body;
        $data['usuario_id'] = isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario']) ? $_SESSION[CHAVE_CONTRATO]['usuario'] : null;
        if ($data['usuario_id']) {
            $dados = $pagamentoService->processarPagamento($data);
            responseJSON($dados);
        }
        throw new Exception('Usuário não logado', 401);
    } catch(Exception $e) {
        throw new Exception($e->getMessage(), $e->getCode());
    }

}