<?php

class LeadRepository {

    public function salvarLead($dados) {
        try {
            $id = null;
            if (!isset($dados['id'])) {
                $sql = "insert into sf_lead(nome_contato, razao_social, telefone_contato, email_contato, assunto, procedencia, dt_cadastro, 
                indicador_cliente, indicador, empresa) values (" . valoresTexto2($dados['nome']) . "," . valoresTexto2($dados['nome']) . "," .
                valoresTexto2($dados['celular']) . "," . verificaValorTextoCampo($dados, 'email') . "," . verificaValorTextoCampo($dados, 'assunto') . "," . verificaValorCampo($dados, 'procedencia') . ",getdate()," .                                                                        
                verificaValorCampo($dados, 'indicador_lead') . "," . verificaValorCampo($dados, 'consultor') . "," .                        
                "(select isnull(max(empresa), '1') from sf_fornecedores_despesas where id_fornecedores_despesas = " . verificaValorCampo($dados, 'consultor') . "));" . 
                "SELECT SCOPE_IDENTITY() id;";
                $result = Conexao::conect($sql);
                odbc_next_result($result);
                $id = odbc_result($result, 1);
            } else {
                $sql = "update sf_lead set nome_contato = " . valoresTexto2($dados['nome']) . "," .
                "razao_social = " . valoresTexto2($dados['nome']) . "," .
                "telefone_contato = " . valoresTexto2($dados['celular']) . "," .
                "assunto = " . verificaValorTextoCampo($dados, 'assunto') . " " .
                "where id_lead = " . valoresSelect2($dados['id']) . ";";
                Conexao::conect($sql);
                $id = $dados['id'];
            }
            if (!is_numeric($id)) {
                throw new Exception("Erro ao inserir o lead => " . $sql);
            }
            return $id;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function updateProspectLead($id_usuario, $id_lead) {
        try {
            $sql = "update sf_lead set prospect =" . valoresSelect2($id_usuario) . ", dt_convert = getdate() where id_lead = " . valoresSelect2($id_lead) . ";";
            Conexao::conect($sql);
            $sql2 = "update sf_fornecedores_despesas set prof_resp = (select max(indicador_cliente) from sf_lead where id_lead = " . valoresSelect2($id_lead) . ") where prof_resp is null and id_fornecedores_despesas = " . valoresSelect2($id_usuario) . ";";
            Conexao::conect($sql2);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function inativaLead($condicao) {
        try {
            $sql = "update sf_lead set inativo = 1 where " . $condicao;
            Conexao::conect($sql);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getLeads($condicao, $paginacao = []) {
        try {
            $where = $condicao ? ' WHERE ' . $condicao : '';
            $sql = "select " . $this->getSQLBase() . $where;
            $sqlTotal = "";
            if (count($paginacao) > 0) {
                $limit = $paginacao['limit'];
                $qtd = $paginacao['qtd'];
                $sqlTotal = "select count(*) as total from (" . $sql . ") as a";
                $sql = "SELECT * FROM (SELECT ROW_NUMBER() OVER (order by dt_cadastro asc) as row, * 
                FROM (" . $sql . ") as x) as a where row BETWEEN " . ($limit + 1) . "AND " . ($limit + $qtd) . " order by dt_cadastro asc;";
            }
            $query = Conexao::conect($sql);
            $data = [];
            $dados = [];
            while ($rows = odbc_fetch_array($query)) {
                $data[] = $this->getResponse($rows);
            }
            if (count($paginacao) > 0) {
                $resTotal = Conexao::conect($sqlTotal);
                $dados['aaData'] = $data;
                $dados['total'] = odbc_result($resTotal, 'total');
            } else {
                $dados = $data;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getLead($condicao) {
        try {
            $sql = "select top 1 " . $this->getSQLBase() . " where " . $condicao;
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados = $this->getResponse($row);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function getResponse($row) {
        $row['nome_contato'] = escreverTexto($row['nome_contato']);
        $row['razao_social'] = escreverTexto($row['razao_social']);
        $row['assunto'] = escreverTexto($row['assunto']);
        $row['nome_procedencia'] = escreverTexto($row['nome_procedencia']);
        return $row;
    }

    private function getSQLBase() {
        return "*, nome_procedencia from sf_lead l left join sf_procedencia p on l.procedencia = p.id_procedencia";
    }

}
