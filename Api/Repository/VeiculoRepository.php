<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 29/10/2019
 * Time: 11:38
 */

class VeiculoRepository
{
    /**
     * Pega a lista de Veículos por campo
     * @param string $condicoes
     * @param array $paginacao
     * @param bool $cupom
     * @return array
     * @throws Exception
     */
    public function getVeiculosPorDados($condicoes = "", $paginacao = [], $cupom = false) {
        try {
            $where = $condicoes ? 'WHERE '.$condicoes : '';
            $sql = $this->queryBase().$where;
            if($cupom) {
                $sql = $this->queryBase(null, null, "convenio.*,
                isnull((select top 1 id_prod_convenio from sf_convenios_planos  where id_convenio_planos = convenio.id_convenio and id_prod_convenio = p.conta_produto_adesao), 0) as desconto_adesao,
                isnull((select top 1 cr2.valor valor_desc_adesao from sf_convenios c2 inner join sf_convenios_planos cp2 on cp2.id_convenio_planos = c2.id_convenio
                inner join sf_fornecedores_despesas_convenios fc2 on fc2.convenio = c2.id_convenio inner join sf_convenios_regras cr2 on cr2.convenio = c2.id_convenio
                where c2.id_conv_plano = vp.id_plano and cp2.id_prod_convenio = p.conta_produto_adesao and fc2.id_fornecedor = fd.id_fornecedores_despesas and getdate() between fc2.dt_inicio and fc2.dt_fim), 0) as valor_desc_adesao") .
                "LEFT JOIN (select cupom, id_fornecedor, id_convenio, convenio_especifico, cr.*, descricao as descricao_cupom,
                cupom_meses,id_conv_plano from sf_convenios co inner join sf_convenios_regras cr on cr.convenio = co.id_convenio
                inner join sf_fornecedores_despesas_convenios fdc on fdc.convenio = co.id_convenio
                where co.ativo = 0 and getdate() between fdc.dt_inicio and fdc.dt_fim
                ) as convenio on convenio.id_conv_plano = vp.id_plano and convenio.id_fornecedor = fd.id_fornecedores_despesas 
                and convenio.id_convenio in (select id_convenio_planos from sf_convenios_planos where id_prod_convenio = p.conta_produto)
                WHERE ".$condicoes;
            }
            $sqlTotal = "";
            $dados = [];
            if(count($paginacao) > 0) {
                $limit = $paginacao['limit'];
                $qtd = $paginacao['qtd'];
                $sqlTotal = "select count(*) as total from (".$sql.") as a";
                $sql = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY id desc) as row, *, dbo.VALOR_REAL_MENSALIDADE(x.prim_mens) valor_plano 
                        from (".$sql.") as x) as a where row BETWEEN " . ($limit + 1) . "AND " . ($limit + $qtd)." order by id desc;";
            }
            $data = [];            
            //echo $sql; exit;
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $veiculo = $this->responseVeiculo($row);
                if($cupom && $row['descricao_cupom']) {
                    $veiculo['cupom'] = [
                        "convenios_meses_site"  => $row['cupom_meses'],
                        "cupom"                 => escreverTexto($row['descricao_cupom']),
                        "desconto_adesao"       => is_numeric($row['desconto_adesao']),
                        "id_convenio"           => $row['id_convenio'],
                        "id_turno"              => null,
                        "tp_valor"              => $row['tp_valor'],
                        "valor"                 => escreverNumero($row['valor']),
                        "cupom_texto"           => escreverTexto($row['cupom'])
                    ];
                } else {
                    $veiculo['cupom'] = null;
                }
                if ($veiculo['plano_id']) {
                    $veiculo["link_contrato"] = $this->getLinkContrato($veiculo);
                }
                $data[] = $veiculo ;
            }
            if(count($paginacao) > 0) {
                $resTotal = Conexao::conect($sqlTotal);
                $dados['aaData'] = $data;
                $dados['total'] = odbc_result($resTotal, 'total');
            } else {
                $dados = $data;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    /**
     * Pega os contratos de veiculos
     * @return array $id_plano
     * @throws Exception
     */
    public function getContratoVeiculos($id_plano) {
        try {
            $contrato = 0;
            $sql = "select id from sf_vendas_planos
            inner join sf_produtos on id_prod_plano = conta_produto
            inner join sf_contratos_grupos cg on cg.id_grupo = conta_movimento            
            inner join sf_contratos on cg.id_contrato = sf_contratos.id
            where inativo = 0 and sf_produtos.tipo = 'C' and sf_contratos.tipo = 0 
            and portal = 1 and id_plano = " . valoresNumericos2($id_plano);
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $contrato = $row['id'];
            }
            return $contrato;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }    

    /**
     * Verifica se o contrato existe
     * @param string $veiculo
     * @return array|int
     * @throws Exception
     */
    public function getLinkContrato($veiculo) {
        $url = "";
        $contrato = $this->getContratoVeiculos($veiculo['plano_id']);
        if ($contrato > 0) {
            $url = ERP_URL . "/Pessoas/" . CONTRATO . "/Documentos/" . $veiculo["id_fornecedores_despesas"] . "/" . $veiculo["plano_id"] . "_" . $contrato . ".pdf";
            $file_headers = get_headers($url);        
            if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                return ERP_URL . "/Modulos/Seguro/FormAssinatura.php?id=" . $veiculo["plano_id"] . "&idContrato=" . $contrato . "&crt=" . CONTRATO . "&idCliente=" . $veiculo["id_fornecedores_despesas"];
            }
        }
        return $url;        
    }

    /**
     * Pega o total de veículos por dados
     * @param string $condicoes
     * @return array|int
     * @throws Exception
     */
    public function getTotalVeiculosPorDados($condicoes = "") {
        try {
            $sql = $this->queryBase(null, true)." WHERE ". $condicoes;
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados = intval($row['total']);
            }
            return $dados;
        }catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Pega os tipos de veiculos
     * @return array $dados
     * @throws Exception
     */
    public function getTiposVeiculos() {
        try {
            $sql = "select * from sf_veiculo_tipos where v_inativo = 0 ORDER BY v_ordem asc";
            $res = Conexao::conect($sql);
            $dados = [];
            while($row = odbc_fetch_array($res)) {
                $dados[] = [
                    'id'        => $row['id'],
                    'descricao' => escreverTexto($row['descricao']),
                    'tipo'      => $row['v_tipo'],
                ];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Pega o veículo por campo
     * @param string $campo
     * @param $valor
     * @return array
     * @throws Exception
     */
    public function getVeiculo($condicao) {
        try {
            $sql = $this->queryBase('top 1')."  WHERE $condicao";
            $res = Conexao::conect($sql);
            $dados = [];
            while($row = odbc_fetch_array($res)) {
                $dados = $this->responseVeiculo($row);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Pega o veículo por campo
     * @param string $campo
     * @param $valor
     * @return array
     * @throws Exception
     */
    public function getVeiculoPorDados($campo = 'id', $valor) {
        try {
            $sql = $this->queryBase('top 1')."  WHERE $campo = ".valoresTexto2($valor);
            $res = Conexao::conect($sql);
            $dados = [];
            while($row = odbc_fetch_array($res)) {
                $dados = $this->responseVeiculo($row);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Verifica a placa
     * @param $placa
     * @param $usuario_id
     * @return array
     * @throws Exception
     */
    public function verificaPlaca($placa, $usuario_id) {
        try {
            $sql = $this->queryBase('top 1').
                " WHERE (v.placa = ".valoresTexto2($placa)." or v.placa = ".valoresTexto2($this->trocarPlaca($placa)).")";
            $res = Conexao::conect($sql);
            $dados = [
                'status' => true,
                'data'  => []
            ];
            while($row = odbc_fetch_array($res)) {
                if ($row['id_fornecedores_despesas'] == $usuario_id || isset($_SESSION[CHAVE_CONTRATO]['rapido'])) {
                    $dados = [
                        'status' => true,
                        'data' => $this->responseVeiculo($row)
                    ];
                } else {
                    $dados = [
                        'status' => false,
                        'data' => []
                    ];
                }
            }
            return $dados;

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Salva os dados do veiculo
     * @param $data
     * @param $id
     * @return int $id
     * @throws Exception
     */
    public function salvarVeiculo($data, $id) {
        try {
            Conexao::initTransaction();
            $franquia = $this->setFranquia($data['valor'], $data['tipo_veiculo']);
            $zero_km = $data['ano_modelo'] == '32000' ?  1 : 0;
            $data['ano_fab'] = $data['ano_modelo'] == '32000' ? date('Y') : $data['ano_modelo'];
            $placa = valoresTexto2(strtoupper($data['placa']));
            if (!$id && strlen($data['placa']) > 0) {
                $sqlVerificaPlaca = "select top 1 id from sf_fornecedores_despesas_veiculo 
                                     where placa = ".$placa." 
                                     and id_fornecedores_despesas = ".valoresSelect2($data['id_fornecedores_despesas']);
                $resultVerificaPlaca = Conexao::conect($sqlVerificaPlaca);
                $id = odbc_result($resultVerificaPlaca, 1);
            }
            if (!$id) {
                $sql = "insert into sf_fornecedores_despesas_veiculo(id_fornecedores_despesas, valor, marca, modelo, ano_modelo, ano_fab, combustivel, codigo_fipe, mes_referencia, tipo_veiculo,
                        placa, renavam, data_cadastro, status, franquia, franquia_tp, zero_km, porcentagem, cilindrada, cambio, consulta_fipe) values(".valoresSelect2($data['id_fornecedores_despesas']).", ".valoresNumericos2($data['valor']).",
                        ".valoresTexto2($data['marca']).",".valoresTexto2($data['modelo']).", ".valoresTexto2($data['ano_modelo']).", ".valoresTexto2($data['ano_fab']).", ".valoresTexto2($data['combustivel']).", ".valoresTexto2($data['codigo_fipe']).",
                        ".valoresTexto2($data['mes_referencia']).", ".valoresTexto2($data['tipo_veiculo']) . ", " . $placa. ",'', getdate(), 'Pendente', " . $franquia[0] . ", " . $franquia[1] . ", ".valoresSelect2($zero_km).", 100,
                        ".valoresTexto2($data['cilindrada']).", ". valoresSelect2($data['cambio']).", ".valoresNumericos2($data['consulta_fipe']) . ");
                        SELECT SCOPE_IDENTITY() id;";
                $result = Conexao::conect($sql);
                odbc_next_result($result);
                $id = odbc_result($result, 1);
                salvarLog(['tabela' => 'sf_fornecedores_despesas_veiculo', 'id_item' => $id,
                'usuario' => getLoginUser(), 'acao' => 'I', 
                'descricao' => 'INCLUSAO VEICULO ('.$data['placa'].')',
                'id_fornecedores_despesas' => $data['id_fornecedores_despesas']]);
                if ($this->getVistoria()) {
                    $this->criarVistoria($id);
                }
            } else {
                $sql = " update sf_fornecedores_despesas_veiculo set valor = ".valoresNumericos2($data['valor']).", marca = ".valoresTexto2($data['marca']).", modelo = ".valoresTexto2($data['modelo']).",
                        ano_modelo = ".valoresTexto2($data['ano_modelo']). ", ano_fab = ".valoresTexto2($data['ano_fab']). ",  combustivel = ".valoresTexto2($data['combustivel']).", codigo_fipe = ".valoresTexto2($data['codigo_fipe']).", mes_referencia = ".valoresTexto2($data['mes_referencia']).",
                        tipo_veiculo = ".valoresTexto2($data['tipo_veiculo']).",  placa = ".$placa. ", renavam = ".verificaValorTextoCampo($data, 'renavam').", cor = ".verificaValorCampo($data, 'cor').", 
                        chassi = UPPER(".verificaValorTextoCampo($data, 'chassi')."), zero_km = ".valoresSelect2($zero_km). ", cilindrada = ".valoresTexto2($data['cilindrada']). ", cambio = ". valoresSelect2($data['cambio']). ",
                        consulta_fipe = ". valoresNumericos2($data['consulta_fipe']). " WHERE id = " . valoresSelect2($id) . ";";
                Conexao::conect($sql);
            }
            Conexao::commit();
            return $id;
        } catch (Exception $e) {
            Conexao::rollback();
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Lista de veículos
     * @param $condicao
     * @return array
     * @throws Exception
     */
    public function getListaVeiculos($condicao) {
        try {
            $sql = "select id, upper(concat(placa, ' - ', modelo)) as descricao 
            FROM sf_fornecedores_despesas_veiculo v
            INNER JOIN sf_vendas_planos vp ON vp.id_veiculo = v.id 
            AND (vp.planos_status != 'cancelado' or vp.planos_status is null)
            WHERE ".$condicao;
            $result = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($result)) {
                $dados[$row['id']] = escreverTexto($row['descricao']);
            }
            return $dados;
        }catch (Exception $e) {
            throw  new Exception($e->getMessage());
        }
    }


    /**
     * Função retorna os Acessorios pelo plano
     * @param $id_plano
     * @return array
     * @throws Exception
     */
    public function getAcessorios($id_plano) {
        try {
            $dados = [];
            $sql = "select id_servico_acessorio, p.descricao, 
            case when p.tp_preco = 1 then (pl.segMax * p.preco_venda)/100 
                 when p.tp_preco = 2 then (v.valor * p.preco_venda)/100
                 else p.preco_venda end as preco_venda, p.conta_movimento
            from sf_vendas_planos_acessorios pa 
            inner join sf_vendas_planos vp on vp.id_plano = pa.id_venda_plano
            inner join sf_fornecedores_despesas_veiculo v on v.id = vp.id_veiculo
            inner join sf_produtos p ON p.conta_produto = pa.id_servico_acessorio
            inner join sf_produtos pl ON pl.conta_produto = vp.id_prod_plano and pl.tipo = 'C'
            where p.tipo = 'A' and p.inativa = 0 AND id_venda_plano = " . valoresSelect2($id_plano);
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $dados[] = [
                    'parent'                => $row['conta_movimento'],
                    'id_servico_acessorio'  => $row['id_servico_acessorio'],
                    'descricao'             => escreverTexto($row['descricao']),
                    'preco_venda'           => escreverNumero($row['preco_venda'], 1)
                ];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getAcessoriosPorIds($idsAcessorios) {
        try {
            $dados = [];
            if (is_array($idsAcessorios) && strlen(implode(",", $idsAcessorios)) > 0) {
                $sql = "select * from sf_produtos where tipo = 'A' and inativa = 0 
                and conta_produto in (".implode(",", $idsAcessorios).");";
                $res = Conexao::conect($sql);
                while ($row = odbc_fetch_array($res)) {
                    $dados[] = [
                        'id'            => $row['conta_produto'],
                        'descricao'     => escreverTexto($row['descricao']),
                        'cobertura'     => $row['conta_movimento'],
                        'tp_preco'      => $row['tp_preco'],
                        'preco_venda'   => escreverNumero($row['preco_venda'], 0, 4)
                    ];
                }
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Troca a Placa
     * @param string $placa
     * @return string $placa
     */
    private function trocarPlaca($placa) {
        $placa2 = "";
        $algarismos = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
        if(is_numeric($placa[5])) {
            $placa2 = substr_replace($placa, $algarismos[$placa[5]],5, 1 );
        }else if(is_string($placa[5])) {
            $chave = array_search($placa[5], $algarismos);
            $placa2 = substr_replace($placa, $chave, 5, 1 );
        }
        return $placa2;
    }

    /**
     * Pega a lista de grupo de fabricante por uma condição
     * @param string $where
     * @return array $dados
     * @throws Exception
     */
    public function getGrupoFabricante($where) {
        try {
            $dados = [];
            $sql = "select * from sf_veiculo_grupo_fabricante ".$where.";";
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $row['marca'] = escreverTexto($row['marca']);
                $dados[] = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Cria Vistoria
     * @param int $veiculo_id
     * @throws Exception
     */
    private function criarVistoria($veiculo_id) {
        try {            
            $sql = "insert into sf_vistorias(id_veiculo, data, sys_login, tipo, obs) values (".valoresSelect2($veiculo_id).", getdate(), 
            'SISTEMA', (select top 1 id from sf_vistorias_tipo where (id in (select id_vistoria from sf_vistorias_veiculo_tipos where 
            id_tipo in (select max(tipo_veiculo) from sf_fornecedores_despesas_veiculo where id = ".valoresSelect2($veiculo_id).")) or 
            (select count(id_tipo) from sf_vistorias_veiculo_tipos where id_vistoria = sf_vistorias_tipo.id) = 0) and inativo = 0 order by id), '');";
            Conexao::conect($sql);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Verifica se existe tipos de vistorias
     * @return bool
     * @throws Exception
     */
    public function getVistoria() {
        try {
            $sql = "select * from sf_vistorias_tipo";
            $res = Conexao::conect($sql);
            return odbc_num_rows($res) > 0;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Base Query
     * @param string $top
     * @param int $total
     * @param string $campo
     * @return string
     */
    private function queryBase($top = "", $total = null, $campo = "") {
        $campo = $campo ? ', '.$campo : '';
        $select = $total ? "count(v.id) as total" :
            "$top v.*, vt.descricao as descricao_tipo_veiculo, v.valor as valor_veiculo, p.descricao, p.mensagem_site as produto_descricao,
            pp.valor_unico as valor_plano_item, vp.id_prod_plano as plano, vp.id_plano, pc.nome_produto_cor,
            (SELECT top 1 id_mens FROM sf_vendas_planos_mensalidade WHERE id_plano_mens = vp.id_plano ORDER BY dt_inicio_mens) AS prim_mens,
            case when (CAST(dateadd(day, (select top 1 aca_dias_tolerancia_status from sf_configuracao), (
            select top 1 dt_inicio_mens from sf_vendas_planos_mensalidade where id_plano_mens = vp.id_plano and dt_inicio_mens < cast(getdate() as date) and dt_pagamento_mens is null
            )) as date) > cast(getdate() as date) and dt_cancelamento is null)
            then 'Ativo' else vp.planos_status end as plano_status $campo";
        return "SELECT DISTINCT $select 
                FROM sf_fornecedores_despesas_veiculo v
                INNER JOIN sf_fornecedores_despesas fd ON fd.id_fornecedores_despesas = v.id_fornecedores_despesas
                AND fornecedores_status not in ('Excluido')
                INNER JOIN sf_veiculo_tipos vt on vt.id = v.tipo_veiculo
                LEFT JOIN sf_vendas_planos vp ON vp.id_veiculo = v.id AND (vp.planos_status != 'cancelado' or vp.planos_status is null)
                LEFT JOIN sf_produtos p ON p.conta_produto = vp.id_prod_plano
                LEFT JOIN sf_produtos_parcelas pp ON pp.id_parcela = (
                    select top 1 id_parc_prod_mens from sf_vendas_planos_mensalidade
                    where id_plano_mens = vp.id_plano order by dt_inicio_mens desc
                ) and id_produto = p.conta_produto
                LEFT JOIN sf_produtos_cor pc ON pc.id_produto_cor = v.cor
                ";
    }

    /**
     * Response de Veículo
     * @param $row
     * @return array
     */
    public function responseVeiculo($row) {
        return [
            'id'                        => $row['id'],
            'id_fornecedores_despesas'  => $row['id_fornecedores_despesas'],
            'marca'                     => escreverTexto($row['marca']),
            'modelo'                    => escreverTexto($row['modelo']),
            'valor'                     => escreverNumero($row['valor_veiculo'], 1),
            'placa'                     => $row['placa'],
            'ano_modelo'                => $row['ano_modelo'],
            'combustivel'               => escreverTexto($row['combustivel']),
            'renavam'                   => escreverTexto($row['renavam']),
            'codigo_fipe'               => $row['codigo_fipe'],
            'mes_referencia'            => escreverTexto($row['mes_referencia']),
            'tipo_veiculo'              => escreverTexto($row['tipo_veiculo']),
            'consulta_fipe'             => escreverTexto($row['consulta_fipe']),
            'descricao_tipo_veiculo'    => escreverTexto($row['descricao_tipo_veiculo']),
            'produto_id'                => $row['plano'],
            'plano_id'                  => $row['id_plano'],
            'prim_mens'                 => $row['prim_mens'],
            'produto'                   => escreverTexto($row['descricao']),
            'produto_descricao'         => escreverTexto($row['produto_descricao']),
            'franquia'                  => $row['franquia'],
            'franquia_tp'               => $row['franquia_tp'],
            'cilindrada'                => $row['cilindrada'],
            'cambio'                    => $row['cambio'],
            'status_veiculo'            => escreverTexto($row['status']),
            'chassi'                    => escreverTexto(strtoupper($row['chassi'])),
            'cor'                       => escreverTexto($row['cor']),
            'nome_cor'                  => escreverTexto($row['nome_produto_cor']),
            'valor_plano'               => escreverNumero($row['valor_plano'], 1),
            'valor_plano_item'          => escreverNumero($row['valor_plano_item'], 1),
            'data_cadastro'             => escreverDataHora($row['data_cadastro']),
            'acessorios'                => $row['id_plano'] ? $this->getAcessorios($row["id_plano"]) : [],
            'valor_desc_adesao'         => escreverNumero($row['valor_desc_adesao'])
        ];
    }

    /**
     * Retorna a porcentagem da franquia via valor da FIPE
     * @param string $valorFipe
     * @return double 
     * @throws Exception
     */
    private function setFranquia($valorFipe, $tipo) {
        try {
            $cota_defaut = FRANQUIA_DEFAULT;
            $cota_defaut_tp = "0";
            $valorF = valoresNumericos2($valorFipe);            
            $sql = "select top 1 cota_defaut, cota_defaut_tp from sf_produtos p1 where p1.tipo = 'C' 
            and " . $valorF . " between p1.segMin and p1.segMax
            and (conta_produto in (select id_produto from sf_produtos_veiculo_tipos where id_tipo = " . $tipo . ") or
            conta_produto not in (select id_produto from sf_produtos_veiculo_tipos))
            and p1.inativa = 0 and p1.favorito = 1 order by cota_defaut desc;";
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $cota_defaut = $row['cota_defaut'];
                $cota_defaut_tp = $row['cota_defaut_tp'];
            }            
            $rs_fm = Conexao::conect("select top 1 seg_piso_franquia from sf_configuracao");
            $piso_franquia = floatval(odbc_result($rs_fm, 1));            
            $valor = ($cota_defaut_tp == "1" ? $cota_defaut : ($valorF * ($cota_defaut / 100)));            
            if ($piso_franquia == 0 || $piso_franquia <= $valor) {
                return [$cota_defaut, $cota_defaut_tp];
            }
            return [ceil(($piso_franquia * 100) / $valorF), "0"];
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function isVerificaPlaca($data) {
        try {
            $placa = str_replace("-",'', $data['placa']);
            $sql = "select id from sf_fornecedores_despesas_veiculo 
            where id = ".valoresSelect2($data['id'])."
            and replace(placa, '-', '') = ".valoresTexto2($placa)."
            and id_fornecedores_despesas = ".valoresSelect2($data['id_usuario']).";";
            $res = Conexao::conect($sql);
            $id = odbc_result($res, 1);
            if(!is_numeric($id)) {
                return false;
            }
            return $id;
        } catch (Exception $e) {
            throw new Exception($e->getMessage()); 
        }
    }

    public function verificaPlanoVeiculo($condicao) {
        try {
            $sql = "select top 1 pp.id_parcela from sf_fornecedores_despesas_veiculo ve 
            inner join sf_vendas_planos vp on vp.id_veiculo = ve.id and planos_status not in ('Inativo', 'Cancelado')
            inner join sf_produtos_parcelas pp on pp.id_parcela = (select top 1 id_parc_prod_mens from sf_vendas_planos_mensalidade where id_plano_mens = vp.id_plano)
            inner join sf_produtos p on p.conta_produto = pp.id_produto and tipo = 'C' and inativa = 0 
            where ".$condicao.";";
            $res = Conexao::conect($sql);
            $id = odbc_result($res, 1);
            if(!is_numeric($id)) {
                return false;
            }
            return $id;
        } catch (Exception $e) {
            throw new Exception($e->getMessage()); 
        }
    }

}