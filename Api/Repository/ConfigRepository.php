<?php 

class ConfigRepository {

    /**
     * Lista as configurações 
     * @return array
     * @throws Exception
     */
    public function getConfig() {
        try {
            $sql = "select top 1 * from sf_configuracao where id = 1;";
            $result = Conexao::conect($sql);
            $dados = [];
            while($row = odbc_fetch_array($result)) {
                $dados['credencial']            = 0;
                $dados['login_usuario']         = 'SISTEMA';
                $dados['dcc_empresa']           = $row['DCC_EMPRESA'];
                $dados['loja']                  = $row['DCC_LOJA'];
                $dados['chave']                 = $row['DCC_CHAVE'];
                $dados['tipo']                  = $row['DCC_TIPO'] == 1 ? "LIVE" : "TEST";
                $dados['operadora']             = $row['DCC_OPERADORA'];
                $dados['dcc_forma_pagamento']   = $row['DCC_FORMA_PAGAMENTO'];
                $dados['dcc_empresa2']          = $row['DCC_EMPRESA2'];
                $dados['loja2']                 = $row['DCC_LOJA2'];
                $dados['chave2']                = $row['DCC_CHAVE2'];
                $dados['tipo2']                 = $row['DCC_TIPO2'] == 1 ? "LIVE" : "TEST";
                $dados['operadora2']            = $row['DCC_OPERADORA2'];
                $dados['dcc_forma_pagamento2']  = $row['DCC_FORMA_PAGAMENTO2'];
            }
            return $dados;
        }catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Lista de Procedencias
     * @return array
     * @throws Exception
     */
    public function getProcedencia($excetoCampanha = 0, $portal = 0) {
        try {
            $whereCampanha = $excetoCampanha ? " AND id_procedencia not in (select id_procedencia from sf_lead_campanha)" : "";
            $wherePortal = $portal ? " AND tipo_portal = 1" : "";
            $sql = "select * from sf_procedencia WHERE tipo_procedencia = 0".$whereCampanha.$wherePortal.";";
            $result = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($result)) {
                $dados[] = [
                    'id_procedencia'    => $row['id_procedencia'],
                    'nome_procedencia'  => escreverTexto($row['nome_procedencia']),
                    'tipo_procedencia'  => $row['tipo_procedencia']
                ];
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Lista de Vencimentos
     * @return array
     * @throws Exception
     */
    public function getDiasVencimentos() {
        try {
            $sql = "select * from sf_configuracao_vencimentos where de <= datepart(day, getdate()) and ate >= datepart(day, getdate()) order by dia asc;";
            $result = Conexao::conect($sql);
            $dados = [];
            while($row = odbc_fetch_array($result)) {
                $dados[] = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Lista de campos Livres
     * @param string $condicao
     * @return array
     * @throws Exception
     */
    public function getCampoExtra($condicao) {
        try {
            $sql = "select * from sf_configuracao_campos_livres where ".$condicao;
            $result = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($result)) {
                $row['descricao_campo'] = escreverTexto($row['descricao_campo']);
                $dados[] = $row; 
            }
            return $dados;

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    /**
     * Lista de Filiais
     * @return array
     * @throws Exception
     */
    public function getFiliais() {
        try {
            $sql = "select * from sf_filiais";
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados[] = [
                    'numero_filial' => $row['numero_filial'],
                    'descricao'     => escreverTexto($row['Descricao']),
                    'endereco'      => escreverTexto($row['endereco']),
                    'numero'        => $row['numero'],
                    'bairro'        => escreverTexto($row['bairro']),
                    'cidade'        => escreverTexto($row['cidade_nome']),
                    'estado'        => escreverTexto($row['estado']),
                    'cep'           => $row['cep'],
                    'cnpj'          => $row['cnpj'],
                    'telefone'      => $row['telefone'],
                    'site'          => $row['site'],
                    'razao_social'  => escreverTexto($row['razao_social_contrato']),
                    'nome_fantasia' => escreverTexto($row['nome_fantasia_contrato'])
                ];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    public function getConfigMultaPorData($dtCobranca) {
        try {
            $sql = "set dateformat dmy; select ACA_PLN_DIAS_TOLERANCIA, aca_tipo_multa, aca_forma_multa, aca_taxa_multa, aca_taxa_multa_dias,
            aca_mora_multa, aca_tolerancia_multa, datediff(day, " . valoresData2($dtCobranca) . ", GETDATE()) dias 
            from sf_configuracao where id = 1";
            $dados = [];
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getCalculaDiaUtil($dtCobranca) {
        try {
            $sql = "set dateformat dmy; select dbo.CALC_DIA_UTIL(" . valoresData2($dtCobranca) . ") total;";
            $res = Conexao::conect($sql);
            $total = 0;
            while($row = odbc_fetch_array($res)) {
                $total = $row['total'];
            }
            return $total;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}