<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 15/09/2020
 * Time: 15:14
 */

class MensalidadeRepository
{

    /**
     * Lista as mensalidades do plano atraves da condicao
     * @param $condicao
     * @param $paginacao
     * @return array $dados;
     * @throws Exception
     */
    public function listaMensalidadesPlano($condicao, $paginacao = []) {
        try {
            $where = $condicao ? " WHERE $condicao" : "";
            $sql = "SELECT * FROM (".$this->queryBaseMensalidades().") as b ".$where;
            $sqlTotal = "";
            if (count($paginacao) > 0) {
                $limit = $paginacao['limit'];
                $qtd = $paginacao['qtd'];
                $sqlTotal = "set dateformat dmy; select count(*) as total from (".$sql.") as a";
                $sql = "set dateformat dmy; SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY dt_inicio_mens desc) as row, * 
                        from (".$sql.") as x) as a where row BETWEEN " . ($limit + 1) . "AND " . ($limit + $qtd)
                    ." ORDER BY dt_inicio_mens DESC;";
            }
            //echo $sql; exit;
            $rs = Conexao::conect($sql);
            $dados = [];
            $data = [];
            while ($row = odbc_fetch_array($rs)) {
                $data[] = $this->getResponseMensalidade($row);
            }
            if (count($paginacao) > 0) {
                $resTotal = Conexao::conect($sqlTotal);
                $dados['aaData'] = $data;
                $dados['total'] = odbc_result($resTotal, 'total');
            } else {
                $dados = $data;
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Busca o plano através condição
     * @param string $condicao
     * @return array $dados;
     * @throws Exception
     */
    public function getMensalidadePlano($condicao) {
        try {
            $where = $condicao ? " WHERE $condicao" : "";
            $sql = "SELECT TOP 1 * FROM (".$this->queryBaseMensalidades().") as b ".$where;
            $rs = Conexao::conect($sql);
            $dados = [];
            while($row = odbc_fetch_array($rs)) {
                $dados = $this->getResponseMensalidade($row);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Pega o idParcela por uma condição
     * @param string $where
     * @return strinf $id_parcela
     * @throws Exception
     */
    public function getIdParcelaPorMensalidade($where) {
        try {
            $sql = "select top 1 id_parc_prod_mens from sf_vendas_planos vp
            inner join sf_vendas_planos_mensalidade pm on pm.id_plano_mens = vp.id_plano
            where ".$where." order by dt_inicio_mens asc;";
            $res = Conexao::conect($sql);
            $id_parcela = odbc_result($res, 'id_parc_prod_mens');
            return $id_parcela;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getInfoMensalidade($id_mens) {
        try {
            $sql = "select top 1 dbo.VALOR_REAL_MENSALIDADE(id_mens) as valor, 
            dt_inicio_mens as data 
            from sf_vendas_planos_mensalidade where id_mens = ".valoresSelect2($id_mens).";";
            $res = Conexao::conect($sql);
            $dados = [];
            while($row = odbc_fetch_array($res)) {
                $row['valor']   = escreverNumero($row['valor']);
                $row['data']    = escreverData($row['data']);
                $dados = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Atualiza Mensalidade
     * @param array $data
     * @param string $condicao
     * @throws Exception 
     */
    public function atualizaMensalidade($data, $condicao) {
        try {
            $sqlData =  array_map(function($key, $value) {
                return $key." = ".$value;
            }, array_keys($data), array_values($data));
            $sql = " update sf_vendas_planos_mensalidade set ".(implode(', ', $sqlData))." WHERE ".$condicao;
            Conexao::conect($sql);
            salvarLog(['tabela' => 'sf_vendas_planos_mensalidade', 'id_item' => $data['id_plano'],
            'usuario' => getLoginUser(), 'acao' => 'A', 'descricao' => 'ALTERACAO MENS.:(atualizar) ' . $data['id_mens'] . " PLANO: ".$data['id_plano'],
            'id_fornecedores_despesas' => $data['usuario_id']]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Atualizar a parcela de mensalidades
     * @param $id_parcela
     * @param $id_plano
     * @param $usuario_id
     * @throws Exception
     */
    public function atualizarParcMens($data) {
        try {
            $sqlUpdateMensalidades = "update sf_vendas_planos_mensalidade set id_parc_prod_mens = " . valoresSelect2($data['id_parcela']) . " where id_mens = " . valoresSelect2($data['id_mens']);
            //echo $sqlUpdateMensalidades; exit;
            Conexao::conect($sqlUpdateMensalidades);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    /**
     * Atualizar a parcela de mensalidades
     * @param $id_parcela
     * @param $id_plano
     * @param $usuario_id
     * @throws Exception
     */
    public function atualizarParcelaMensalidades($data) {
        try {
            $sqlUpdateMensalidades = "update sf_vendas_planos_mensalidade 
            set id_parc_prod_mens = pad.id_parcela, valor_mens = pad.valor_unico,
            dt_fim_mens = (case when p.parcelar = 1 or (pad.parcela <= 1) then DATEADD(DAY, -1, (DATEADD(MONTH, 1, pm.dt_inicio_mens)))
            else DATEADD(DAY, -1, (DATEADD(MONTH, pad.parcela, pm.dt_inicio_mens))) end)
            from sf_vendas_planos_mensalidade pm
            inner join sf_produtos_parcelas pp on pp.id_parcela = pm.id_parc_prod_mens
            cross apply (select top 1 id_parcela, valor_unico, parcela, id_produto from sf_produtos_parcelas
            where id_parcela = ".valoresSelect2($data['id_parcela'])." and id_produto = pp.id_produto) as pad
            inner join sf_produtos p on p.conta_produto = pad.id_produto
            inner join sf_vendas_planos vp on vp.id_plano = pm.id_plano_mens
            where pm.dt_pagamento_mens is null and pm.id_plano_mens = ".valoresSelect2($data['id_plano'])
            . " and vp.favorecido = ".valoresSelect2($data['usuario_id']);
            //echo $sqlUpdateMensalidades; exit;
            Conexao::conect($sqlUpdateMensalidades);
            salvarLog(['tabela' => 'sf_vendas_planos_mensalidade', 'id_item' => $data['id_plano'],
            'usuario' => getLoginUser(), 'acao' => 'A', 'descricao' => 'ALTERACAO MENS. (Parcela) ' . $data['id_parcela'] . " PLANO: ".$data['id_plano'],
            'id_fornecedores_despesas' => $data['usuario_id']]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getMensalidades($condicao) {
        try {
            $sql = "select * from sf_vendas_planos_mensalidade where ".$condicao.";";
            $res = Conexao::conect($sql);
            $dados = [];
            while($row = odbc_fetch_array($res)) {
                $row['dt_inicio_mens'] = escreverData($row['dt_inicio_mens']);
                $row['dt_fim_mens'] = escreverData($row['dt_fim_mens']);
                $row['dt_pagamento_mens'] = escreverData($row['dt_pagamento_mens']);
                $row['dt_cadastro'] = escreverData($row['dt_cadastro']);
                $row['valor_mens'] = escreverNumero($row['valor_mens']);
                $dados[] = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());

        }
    }

    /**
     * Salva Mensalidades
     * @param array $data
     * @return int 
     * @throws Exception
     */
    public function salvarMensalidades($data, $atualiza = 1) {
        try {                                
            //Conexao::initTransaction();
            $produto = $data['produto'];
            $mensalidades = $this->getMensalidades('id_plano_mens = '.valoresSelect2($data['id_plano']) . ' and
            month(dt_inicio_mens) = month(getdate()) and year(dt_inicio_mens) = year(getdate()) 
            order by dt_inicio_mens, id_mens asc');
            
            $parcelar = $produto['gerar_parc'];
            $produto['valor_taxa_adesao'] = valoresNumericos2($produto['valor_taxa_adesao']);
            $produto['adesao_soma'] = valoresNumericos2($produto['adesao_soma']);
            $produto['valor_unico'] = valoresNumericos2($produto['valor_unico']);
            
            $condicaoAdesao = ($produto['valor_taxa_adesao'] > 0) && !$produto['adesao_soma']; 
            $parcela = ($condicaoAdesao || $parcelar) ? 1 : $produto['num_parcela'];
            
            $valor_mens = $parcelar ? valoresNumericos2(($produto['valor_unico'] / $produto['num_parcela'])) : $produto['valor_unico'];
            $data_inicio = isset($data['data_vencimento']) ? valoresData2($this->atualizaDataVencimento($data['data_vencimento'], $parcela)) : "dateadd(month,+".$parcela.",getdate())"; 
            $dt_fim  =  "dateadd(day,-1,".$data_inicio.")";
            if (count($mensalidades) > 0) {
                $atualizaParcela = $atualiza ? "id_parc_prod_mens = ".valoresSelect2($produto['parcela_id']).", " : "";
                $data['id_mens'] = $mensalidades[0]['id_mens'];
                $sql = "set dateformat dmy; update sf_vendas_planos_mensalidade set dt_inicio_mens = getdate(), dt_fim_mens = ".$dt_fim.", ".$atualizaParcela."
                valor_mens = " . $valor_mens . " where id_mens = " . valoresSelect2($data['id_mens']) . " and dt_pagamento_mens is null";
                Conexao::conect($sql);
                salvarLog(['tabela' => 'sf_vendas_planos_mensalidade', 'id_item' => $data['id_plano'],
                'usuario' => getLoginUser(), 'acao' => 'A', 'descricao' => 'ALTERACAO MENS.: (salvar) ' . $data['id_mens'] . " PLANO: " . $data['id_plano'],
                'id_fornecedores_despesas' => $data['id_usuario']]);
            } else {                
                $sql = "set dateformat dmy; insert sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens) 
                values(" . valoresSelect2($data['id_plano']) . ", GETDATE(), ".$dt_fim."," . valoresSelect2($produto['parcela_id']) . 
                ", getdate()," . $valor_mens . "); SELECT SCOPE_IDENTITY() id;";
                $res = Conexao::conect($sql);
                odbc_next_result($res);
                $data['id_mens'] = odbc_result($res, 1);
                salvarLog(['tabela' => 'sf_vendas_planos_mensalidade', 'id_item' => $data['id_plano'],
                'usuario' => getLoginUser(), 'acao' => 'I', 'descricao' => 'INCLUSAO MENS.: (salvar) ' . $data['id_mens'] . " PLANO: " . $data['id_plano'],
                'id_fornecedores_despesas' => $data['id_usuario']]);                                
            }
            //-----------------------------------------------------------------------------------------------------------------------------------                             
            if (isset($data['data_vencimento']) && TIPO_VENCIMENTO == 2) {
                $cur = Conexao::conect("select dia, minimo, pro_rata from sf_configuracao_vencimentos where de <= datepart(day, getdate()) and ate >= datepart(day, getdate())
                and dia = " . $data['data_vencimento'] . " order by dia asc;");
                while ($row = odbc_fetch_array($cur)) {
                    $dia = $row["dia"];
                    $minimo = $row["minimo"];
                    $pro_rata = $row["pro_rata"];
                }
                $sql = "EXEC dbo.SP_MK_MENS_DIA_COND @id_mens = " . $data['id_mens'] . ", @dt_ini = '" . date("Y-m-d") . "', @id_plano = " . $data['id_plano'] . ",
                @id_parcela = " . $produto['parcela_id'] . ", @dia_padrao = " . $dia . ", @minimo = " . $minimo . ", @pro_rata = " . $pro_rata . ";";
                Conexao::conect($sql);   
            } else if (TIPO_VENCIMENTO == 0) {
                $cur = Conexao::conect("delete from sf_vendas_planos_mensalidade where id_plano_mens = " . $data['id_plano'] . 
                " and id_mens not in (" . $data['id_mens'] . ") and id_item_venda_mens is null;");
            }
            //-----------------------------------------------------------------------------------------------------------------------------------                 
            $sql = "EXEC dbo.SP_MK_MENS_ANT_APOS @id_mens = " . valoresNumericos2($data['id_mens']) . ";";
            $res = Conexao::conect($sql);
            $tot_mens = odbc_result($res, 1);
            //-----------------------------------------------------------------------------------------------------------------------------------            
            return $data['id_mens'];
        } catch(Exception $e) {
            Conexao::rollback();
            throw new Exception($e->getMessage());
        }
    }

    public function calculoProrata($data2, $parcela) {
        try {
            $data = [];            
            $id_mens = $data2['id_mens'];
            $sql = "select top 1 * from sf_vendas_planos_mensalidade where id_mens = ".valoresSelect2($id_mens);
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $data = $row;
            }
            if (isset($data['dt_inicio_mens']) && isset($data['dt_fim_mens'])) {
                $dt_inicio_mens = escreverData($data['dt_inicio_mens']);
                $dt_fim_mens    = escreverData($data['dt_fim_mens']);
                $dt_inicio_real = escreverDataSoma($dt_fim_mens, "-".$parcela." month", "Y-m-d");
                $difNovaDt      = dataDiff($data['dt_inicio_mens'], $dt_inicio_real, 'd') + 1;
                if ($difNovaDt < 0) {
                    $sql = " update sf_vendas_planos_mensalidade set dt_inicio_mens = dateadd(month,-".$parcela.", dateadd(day, +1,".valoresTexto2($dt_fim_mens)."))
                     where id_mens = ".valoresSelect2($id_mens);
                    Conexao::conect($sql);
                } else {
                    $dt_fim_real    = escreverDataSoma($dt_inicio_mens, " +".$parcela." month", "Y-m-d");
                    $difDias        = dataDiff($data['dt_inicio_mens'], $dt_fim_real, 'd');
                    $valorTotal     = floatval($data['valor_mens']) + $this->acrescimo($data['id_plano_mens']);
                    $valorProrata   = floatval($data['valor_mens']) + (($valorTotal / $difDias) * $difNovaDt);
                    $sql = "update sf_vendas_planos_mensalidade set valor_mens = ".$valorProrata." where id_mens = ".valoresSelect2($id_mens);
                    Conexao::conect($sql);
                }
                salvarLog(['tabela' => 'sf_vendas_planos_mensalidade', 'id_item' => $data2['id_plano'],
                'usuario' => getLoginUser(), 'acao' => 'A', 'descricao' => 'ALTERACAO MENS.: (ProRata) ' . $data2['id_mens'] . " PLANO: ". $data2['id_plano'],
                'id_fornecedores_despesas' => $data2['usuario_id']]);
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function acrescimo($id_plano) {
        try {
            $sql = "select ISNULL(sum(x.qtd_total * valor), 0) from (
                select id, count(id) qtd_total from sf_vendas_planos
                inner join sf_fornecedores_despesas_dependentes on favorecido = id_titular
                inner join sf_fornecedores_despesas on id_fornecedores_despesas = id_dependente
                left join sf_produtos_acrescimo on id_produto = id_prod_plano and
                ((datediff(year,data_nascimento,getdate()) between idade_min and idade_max_m and sexo = 'M') or
                (datediff(year,data_nascimento,getdate()) between idade_min and idade_max_f and sexo = 'F'))
                and (id_parentesco is null or id_parentesco = parentesco)
                where compartilhado = 1 and id_plano = ".valoresSelect2($id_plano)." group by sf_produtos_acrescimo.id) as x 
                inner join sf_produtos_acrescimo on sf_produtos_acrescimo.id = x.id 
                and x.qtd_total between qtd_min and qtd_max;";
            $res = Conexao::conect($sql);
            $valor = floatval(odbc_result($res, 1));
            $sqlAcessorios = "select top 1 dbo.FU_VALOR_ACESSORIOS(".$id_plano.")";
            $res = Conexao::conect($sqlAcessorios);
            $valorAcessorios = floatval(odbc_result($res, 1));
            return $valor + $valorAcessorios;    
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Dia do Vencimento da mensalidade
     * @param int $id_mens
     * @return string 
     * @throws Exception
     */
    public function getDiaVencimentoMensalidade($id_mens) {
        try {
            $sql = "select top 1 dia from sf_configuracao_vencimentos where dia >= 
            (select top 1 case when dt_fim_mens = EOMONTH(dt_fim_mens) then 1 
            else datepart(DAY, dt_fim_mens) end as dia from sf_vendas_planos_mensalidade where id_mens = ".valoresSelect2($id_mens).");";
            $res = Conexao::conect($sql);
            return odbc_result($res, 1);

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    

    /**
     * Adiciona Info na Mensalidades
     * @param string $valor_mens
     * @param int $id_parcela
     * @param int $parcela
     * @param string $dt_inicio
     * @param int $i
     * @param int $id_plano
     * @throws Exception
     *  
     */
    private function adicionaInfoMensalidade($valor_mens, $id_parcela, $parcela, $dt_inicio, $i, $id_plano, $data) {
        try {
            $sql = " insert into sf_vendas_planos_mensalidade (id_plano_mens, dt_inicio_mens, dt_fim_mens, id_parc_prod_mens, dt_cadastro, valor_mens) 
            values (".valoresSelect2($id_plano).", dateadd(month," . $i . "," .$dt_inicio. "), dateadd(day,-1,dateadd(month," . ($i + $parcela) . "," .$dt_inicio. ")),
            ".valoresSelect2($id_parcela).", getdate(), $valor_mens )";
            Conexao::conect($sql);
            salvarLog(['tabela' => 'sf_vendas_planos_mensalidade', 'id_item' => $data['id_plano'],
            'usuario' => getLoginUser(), 'acao' => 'I', 'descricao' => 'INCLUSAO MENS.: (Info) ' . $data['id_mens'] . " PLANO: ".$data['id_plano'],
            'id_fornecedores_despesas' => $data['usuario_id']]);
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Atualiza Info na Mensalidades
     * @param int $data
     * @param string $valor_mens
     * @param int $id_parcela
     * @param int $parcela
     * @param string $dt_inicio
     * @param int $i
     * @throws Exception
     *  
     */
    private function atualizaInfoMensalidade($data, $valor_mens, $id_parcela, $parcela, $dt_inicio, $i) {
        try {
            $indice = $i === 1 ? 0 : $i;
            $sql = " update sf_vendas_planos_mensalidade set valor_mens = ".$valor_mens.",
            dt_inicio_mens = dateadd(month," . $indice . "," .$dt_inicio. "), 
            dt_fim_mens = dateadd(day,-1,dateadd(month," . ($indice + $parcela) . "," .$dt_inicio. ")),
            id_parc_prod_mens = ".valoresSelect2($id_parcela)." 
            where id_mens = ".valoresSelect2($data['id_mens'])." and dt_pagamento_mens is null";
            Conexao::conect($sql);
            salvarLog(['tabela' => 'sf_vendas_planos_mensalidade', 'id_item' => $data['id_plano'],
            'usuario' => getLoginUser(), 'acao' => 'A', 'descricao' => 'ALTERACAO MENS.: (Info) ' . $data['id_mens'] . " PLANO: ".$data['id_plano'],
            'id_fornecedores_despesas' => $data['usuario_id']]);
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Atualiza a Data da Mensalidade
     * @param string $data_inicio
     * @param int $data
     * @param int $parcela
     * @throws Exception
     * 
     */
    public function atualizaDataMensalidade($data_inicio, $data, $parcela = 1) {
        try {
            $sql = "set dateformat dmy; update sf_vendas_planos_mensalidade set dt_inicio_mens = ".valoresData2($data_inicio).",
            dt_fim_mens = dateadd(day,-1,dateadd(month,+".$parcela.", ".valoresData2($data_inicio).")) WHERE id_mens = ".valoresSelect2($data['id_mens']);
            Conexao::conect($sql);
            salvarLog(['tabela' => 'sf_vendas_planos_mensalidade', 'id_item' => $data['id_plano'],
            'usuario' => getLoginUser(), 'acao' => 'A', 'descricao' => 'ALTERAR PARCELA: (Data)'.$data['id_mens']."PLANO: ".$data['id_plano'],
            'id_fornecedores_despesas' => $data['id_usuario']]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Atualiza Data de Vencimento
     * @param string $dia
     * @param int $parcela
     * 
     */
    public function atualizaDataVencimento($dia, $parcela) {
        $agora = new DateTime();
        $mesVenc = $agora->format('Y-m');
        $diaVenc = new DateTime($mesVenc.'-'.$dia);
        $diffVenc = $agora->diff($diaVenc);
        $dias = intval($diffVenc->format('%R%a'));
        $dataVenc = $dias < 0 ? $diaVenc->modify('next month') : $diaVenc;
        $diffDataVenc = intval($agora->diff($dataVenc)->format('%R%a'));
        if (intval($agora->format('d')) <= intval($dia) ||  $diffDataVenc + 1 < 20) {
            $resposta = $dataVenc->modify('+'.$parcela.' month');
        } else {
            $resposta = (new DateTime($mesVenc.'-'.$dia))->modify('+'.$parcela.' month');
        } 
        return $resposta->format('d/m/Y');
    }


    /**
     * Remove a Mensalidade atraves do Id
     * @param int $id_mens
     * @throws Exception
     */
    private function removeMensalidade($id_mens, $id_plano, $usuario_id) {
        try {
            $sql = "delete sf_vendas_planos_mensalidade where id_mens = ".valoresSelect2($id_mens)." and dt_pagamento_mens is null";
            Conexao::conect($sql);
            salvarLog(['tabela' => 'sf_vendas_planos_mensalidade', 'id_item' => $id_plano,
            'usuario' => getLoginUser(), 'acao' => 'R', 'descricao' => 'EXCLUSAO MENS. ' . $id_mens, 
            'id_fornecedores_despesas' => $usuario_id]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Busca a lista os items da mensalidade
     * @param $id_mens
     * @return array
     * @throws Exception
     */
    public function getItemsMensalidade($id_mens) {
        try {
            $sql = "select conta_produto, descricao, tipo, quantidade, sum(valor_multa) as valor_multa, sum(valor_desconto) as valor_desconto, sum(preco_venda) as preco_venda, indice
            from (select distinct p.conta_produto, p.descricao, p.tipo, vi.valor_multa, 
            case when vi.id_venda is not null then ((vi.valor_bruto + vi.valor_multa) - vi.valor_total) else NULL end as valor_desconto,
            case when configuracao.aca_adesao_dep = 1 and id_venda is null and p.conta_produto = vp.conta_produto_adesao then dependentes.adesao_dependente
            when dependentes.acrescimo > 0 and id_venda is null and p.conta_produto = vp.conta_produto then dependentes.acrescimo
            when id_venda is not null then vi.valor_total else ((select case when vp.parcela < 1 then 1 else vp.parcela end as num_parcela) * p.preco_venda) end as preco_venda,
            case when vi.id_venda is not null then vi.quantidade else 1 end as quantidade,
            case when p.tipo = 'C' then 0 else 1 end as indice
            from sf_vendas_planos_mensalidade vpm 
             cross apply (select top 1 aca_dias_tolerancia_status, ACA_DIA_VENCIMENTO, ACA_TIPO_VENCIMENTO, aca_adesao_dep, dcc_dias_boleto from sf_configuracao) as configuracao
            cross apply (select *, DATEDIFF(day,(select case when x.parcela = 0 or x.id_prim_mens = x.id_mens then x.dt_inicio_mens 
            else DATEADD(day,-configuracao.dcc_dias_boleto, x.dt_inicio_mens) end as data_prazo), x.prev_mens) as prorata,
            case when x.parcela = 0 or x.id_prim_mens = x.id_mens then x.dt_inicio_mens else DATEADD(day,-configuracao.dcc_dias_boleto, x.dt_inicio_mens) end as data_prazo
            from (select top 1 case when (pp.parcela = 0) then 'DCC' when (pp.parcela = -1) then 'BOLETO'
            when (pp.parcela = -2) then 'PIX' else 'MENSAL' end as tipo_pagamento,
            (select top 1 id_mens from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano order by dt_inicio_mens asc) as id_prim_mens,
            (SELECT TOP 1 s.preco_venda FROM sf_produtos s WHERE s.tipo = 'S' AND s.conta_produto = B.conta_produto_adesao) as taxa_adesao,
            isnull((select max(dt_inicio_mens) from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano and id_mens < vpm.id_mens), dateadd(month, -(select case when pp.parcela < 1 then 1 else pp.parcela end), dt_inicio_mens)) as prev_mens,
            id_mens, A.favorecido, A.id_plano, A.id_prod_plano, A.planos_status, B.conta_produto, B.conta_produto_adesao, pp.id_parcela, pp.parcela, vpm.dt_inicio_mens
            from sf_vendas_planos A inner join sf_produtos B on B.conta_produto = A.id_prod_plano
            inner join sf_produtos_parcelas pp ON pp.id_parcela = vpm.id_parc_prod_mens
            where  A.id_plano = id_plano_mens) as x) as vp
            left join sf_vendas_planos_acessorios vpa on vpm.id_plano_mens = vpa.id_venda_plano
            left join sf_vendas_itens vi on vi.id_venda = (select top 1 id_venda from sf_vendas_itens where id_item_venda = vpm.id_item_venda_mens)
            inner join sf_produtos p on (p.conta_produto = vi.produto and id_venda is not null)
            or ((p.conta_produto = vpa.id_servico_acessorio or p.conta_produto = vp.conta_produto) and id_venda is null)
            or (p.conta_produto = vp.conta_produto_adesao and vp.id_mens = vp.id_prim_mens and id_venda is null)
            outer apply (select * from (select top 1 cupom_meses as convenios_meses_site, id_convenio, valor, tp_valor, co.cupom, co.descricao as convenio_descricao,
            isnull((select top 1 id_prod_convenio from sf_convenios_planos where id_convenio_planos = p.conta_produto and id_prod_convenio = p.conta_produto_adesao), 0) as desconto_adesao 
            from sf_fornecedores_despesas_convenios fdc inner join sf_convenios co on co.id_convenio = fdc.convenio
            inner join sf_convenios_regras cr on cr.convenio = co.id_convenio left join sf_produtos p on co.id_convenio = p.convenios_site
            where ((co.convenio_especifico = 1 and (conta_produto = p.conta_produto or co.id_convenio in (select id_convenio_planos from sf_convenios_planos
            where id_prod_convenio = p.conta_produto))) or co.convenio_especifico = 0) and co.ativo = 0) as x) as convenio 
            outer apply (select sum(valor * dependentes) as acrescimo,  novo, dependentes, valor as valor_dependente, 
            sum((case when config_adesao = 1 and (convenio_desconto = 0 or convenio_desconto is null) then taxa_adesao 
            when config_adesao = 1 and convenio_desconto = 1 and tipo_convenio = 'D' then taxa_adesao - convenio_valor  
            when config_adesao = 1 and convenio_desconto = 1 and tipo_convenio = 'P' then  taxa_adesao - (valor * convenio_valor) / 100 else 0 end) * novo) as adesao_dependente
            from (select valor, taxa_adesao, count(case when cast(data_inclusao as date) <= x.dt_prazo then id_dependente else null end) as dependentes, config_adesao, convenio_desconto, tipo_convenio, convenio_valor,
            count(case when (cast(data_inclusao as date) <= x.dt_prazo and cast(data_inclusao as date) > DATEADD(day, x.prorata, x.dt_prazo)) then id_dependente else null end) as novo 
            from (select id, id_dependente, valor, data_inclusao, vp.data_prazo as dt_prazo, vp.prorata as prorata, vp.taxa_adesao as taxa_adesao, 
            convenio.tp_valor as tipo_convenio, convenio.valor as convenio_valor, convenio.desconto_adesao as convenio_desconto, configuracao.aca_adesao_dep as config_adesao
            from sf_vendas_planos inner join sf_fornecedores_despesas_dependentes fdd on fdd.id_titular = favorecido
            inner join sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = fdd.id_dependente and fd.inativo = 0
            left join sf_produtos_acrescimo on id_produto = id_prod_plano and ((datediff(year,data_nascimento,getdate()) between idade_min and idade_max_m and sexo = 'M') or
            (datediff(year,data_nascimento,getdate()) between idade_min and idade_max_f and sexo = 'F')) and (id_parentesco is null or id_parentesco = parentesco)
            where id_plano = vp.id_plano) as x group by valor, taxa_adesao, config_adesao, convenio_desconto, tipo_convenio, convenio_valor) as y group by valor, dependentes, novo) as dependentes
            where p.preco_venda > 0 and vpm.id_mens = ".valoresSelect2($id_mens).") as a group by tipo, quantidade, conta_produto, descricao, indice order by indice asc;";
            $dados = [];
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados[] = [
                    'id_produto'        => $row['conta_produto'],
                    'descricao'         => escreverTexto($row['descricao']),
                    'tipo'              => escreverTexto($row['tipo']),
                    'preco_venda'       => escreverNumero($row['preco_venda'], 1),
                    'valor_multa'       => escreverNumero($row['valor_multa'], 1),
                    'valor_desconto'    => escreverNumero($row['valor_desconto'], 1),
                    'quantidade'        => intval($row['quantidade'])
                ];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Query Base
     * @return string
     */
    private function queryBaseMensalidades() {
        return "SELECT distinct vpm.*, vp.id_prim_mens, dbo.VALOR_REAL_MENSALIDADE(vpm.id_mens) 
        + isnull(dependentes.adesao_dependente,0) + (case when vpm.valor_mens > 0 and id_prim_mens = vpm.id_mens 
        then vp.taxa_adesao else 0 end) as valor_real_mens, dependentes.*, case when adesao_dependente is not null 
        then adesao_dependente when id_prim_mens = vpm.id_mens then vp.taxa_adesao else null end as valor_adesao, 
        vp.data_prazo, vp.prorata, vi.id_venda, vps.valor_parcela, tp.abreviacao, p.descricao as plano, vp.parcela, 
        vp.id_parcela, vp.taxa_adesao, vp.tipo_pagamento, parcelar_site, b.id_boleto, b.exp_remessa, 
        case when dcc_numero_cartao is not null then dcc_numero_cartao else (select top 1 legenda_cartao 
        from sf_fornecedores_despesas_cartao where id_fornecedores_despesas = favorecido) end as legenda_cartao, 
        case when dt_pagamento_mens is null then ( select case when dateadd(day, 1, vpm.dt_inicio_mens) > GETDATE() 
        then 'Aberto' else 'Vencido' end) else 'Pago' end as status_mens from sf_vendas_planos_mensalidade vpm 
        cross apply (select top 1 aca_dias_tolerancia_status, ACA_DIA_VENCIMENTO, ACA_TIPO_VENCIMENTO, aca_adesao_dep, 
        dcc_dias_boleto from sf_configuracao) as configuracao 
        cross apply (select *, DATEDIFF(day,(select case when x.parcela = 0 or x.id_prim_mens = x.id_mens 
        then x.dt_inicio_mens else DATEADD(day,-configuracao.dcc_dias_boleto, x.dt_inicio_mens) end as data_prazo), x.prev_mens) as prorata, 
        case when x.parcela = 0 or x.id_prim_mens = x.id_mens then x.dt_inicio_mens else DATEADD(day,-configuracao.dcc_dias_boleto, 
        x.dt_inicio_mens) end as data_prazo from (select top 1 case when (pp.parcela = -1) then 'BOLETO RECORRENTE'
        when (pp.parcela = -2) then 'PIX' when (pp.parcela = 0) then 'DCC' when (pp.parcela = 1) then 'BOLETO' when (pp.parcela > 1) then 'MENSAL' else null end as tipo_pagamento, 
        (select top 1 id_mens from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano order by dt_inicio_mens asc) as id_prim_mens, 
        (SELECT TOP 1 s.preco_venda FROM sf_produtos s WHERE s.tipo = 'S' AND s.conta_produto = B.conta_produto_adesao) as taxa_adesao, 
        isnull((select max(dt_inicio_mens) from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano and id_mens < vpm.id_mens), 
        dateadd(month, -(select case when pp.parcela < 1 then 1 else pp.parcela end), dt_inicio_mens)) as prev_mens, id_mens, A.favorecido, 
        A.id_plano, A.id_prod_plano, A.planos_status, B.conta_produto_adesao, pp.id_parcela, pp.parcela, vpm.dt_inicio_mens 
        from sf_vendas_planos A inner join sf_produtos B on B.conta_produto = A.id_prod_plano inner join sf_produtos_parcelas pp 
        ON pp.id_parcela = vpm.id_parc_prod_mens where A.id_plano = id_plano_mens) as x) as vp 
        outer apply (select * from (select top 1 cupom_meses as convenios_meses_site, id_convenio, valor, tp_valor, co.cupom, 
        co.descricao as convenio_descricao, isnull((select top 1 id_prod_convenio from sf_convenios_planos 
        where id_convenio_planos = p.conta_produto and id_prod_convenio = p.conta_produto_adesao), 0) as desconto_adesao 
        from sf_fornecedores_despesas_convenios fdc inner join sf_convenios co on co.id_convenio = fdc.convenio 
        inner join sf_convenios_regras cr on cr.convenio = co.id_convenio left join sf_produtos p on co.id_convenio = p.convenios_site 
        where ((co.convenio_especifico = 1 and (conta_produto = p.conta_produto or co.id_convenio in (select id_convenio_planos 
        from sf_convenios_planos where id_prod_convenio = p.conta_produto))) or co.convenio_especifico = 0) and co.ativo = 0) as x) as convenio 
        outer apply (select sum(valor * dependentes) as acrescimo, novo, dependentes, valor as valor_dependente, sum((case when valor = 0 then 0 when config_adesao = 1 and (convenio_desconto = 0 or convenio_desconto is null) 
        then taxa_adesao when config_adesao = 1 and convenio_desconto = 1 and tipo_convenio = 'D' then taxa_adesao - convenio_valor
        when config_adesao = 1 and convenio_desconto = 1 and tipo_convenio = 'P' then taxa_adesao - (valor * convenio_valor) / 100 else 0 end) * novo) as adesao_dependente 
        from (select valor, taxa_adesao, count(case when cast(data_inclusao as date) <= x.dt_prazo then id_dependente else null end)
        as dependentes, config_adesao, convenio_desconto, tipo_convenio, convenio_valor, count(case when (cast(data_inclusao as date) <= x.dt_prazo 
        and cast(data_inclusao as date) > DATEADD(day, x.prorata, x.dt_prazo)) then id_dependente else null end) as novo 
        from (select id_dependente, valor, data_inclusao, vp.data_prazo as dt_prazo, vp.prorata as prorata, vp.taxa_adesao as taxa_adesao, 
        convenio.tp_valor as tipo_convenio, convenio.valor as convenio_valor, convenio.desconto_adesao as convenio_desconto, 
        configuracao.aca_adesao_dep as config_adesao from sf_vendas_planos 
        inner join sf_fornecedores_despesas_dependentes fdd on fdd.id_titular = favorecido 
        inner join sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = fdd.id_dependente and fd.inativo = 0 
        left join sf_produtos_acrescimo on id_produto = id_prod_plano 
        and ((datediff(year,data_nascimento,getdate()) between idade_min and idade_max_m and sexo = 'M') 
        or (datediff(year,data_nascimento,getdate()) between idade_min and idade_max_f and sexo = 'F')) 
        and (id_parentesco is null or id_parentesco = parentesco) where id_plano = vp.id_plano) as x 
        group by valor, taxa_adesao, config_adesao, convenio_desconto, tipo_convenio, convenio_valor) as y 
        group by valor, dependentes, novo) as dependentes INNER JOIN sf_produtos p ON p.conta_produto = vp.id_prod_plano 
        LEFT JOIN sf_vendas_itens vi ON vi.id_item_venda = vpm.id_item_venda_mens LEFT JOIN sf_boleto b ON b.id_referencia = vpm.id_mens 
        AND b.tp_referencia = 'M' and b.inativo = 0 LEFT JOIN sf_vendas_itens_dcc vic ON vic.id_plano_item = vpm.id_mens 
        and status_transacao = 'A' 
        outer apply (select tipo_documento, sum(valor_parcela) as valor_parcela from sf_venda_parcelas where venda = vi.id_venda 
        group by tipo_documento) as vps LEFT JOIN sf_tipo_documento tp ON tp.id_tipo_documento = vps.tipo_documento";
    }


    /**
     * Retorna os totais dos status das Mensalidade de acordo com a condição
     * @param string $condicao
     * @return array $dados
     * @throws Exception
     */
    public function totalStatusMensalidadePlano($condicao) {
        try {
            $sql = "SELECT count(case when status_mens = 'Pago' then id_mens else null end) as pago, 
            count(case when status_mens = 'Aberto' then id_mens else null end)  as aberto, 
            count(case when status_mens = 'Vencido' then id_mens else null end)  as vencido
            FROM (".$this->queryBaseMensalidades().") as a  WHERE ".$condicao;
            $res = Conexao::conect($sql);
            $dados = [];
            while($row = odbc_fetch_array($res)) {
                $dados = array_map(function($i) {
                    return intval($i);
                }, $row);
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Retorna o valor real da Mensalidade
     * @param int $id_mens
     * @return string 
     * @throws Exception
     */
    public function getValorRealMensalidade($id_mens) {
        try {
            $res = Conexao::conect("select top 1 case when (vp.id_prim_mens = vpm.id_mens and vp.taxa_adesao > 0 and vp.adesao_soma = 0) then vp.taxa_adesao
            when (vp.id_prim_mens = vpm.id_mens and vp.taxa_adesao > 0) then dbo.VALOR_REAL_MENSALIDADE(vpm.id_mens) + 
            (case when adesao_dependente > 0 then adesao_dependente else vp.taxa_adesao end) 
            else dbo.VALOR_REAL_MENSALIDADE(vpm.id_mens) end as valor_pagar, adesao_soma, 
            (select top 1 id_tipo_documento from sf_tipo_documento where abreviacao = 'CRE') as id_tipo_documento,
            case when configuracao.aca_adesao_dep is not null and vp.id_prim_mens = vpm.id_mens and adesao_dependente > 0 then dependentes.adesao_dependente + vp.taxa_adesao
            when adesao_dependente > 0 then adesao_dependente
            when vp.id_prim_mens = vpm.id_mens then vp.taxa_adesao 
            else 0 end taxa_adesao, vp.id_prim_mens
            from  sf_vendas_planos_mensalidade vpm 
             cross apply (select top 1 aca_dias_tolerancia_status, ACA_DIA_VENCIMENTO, ACA_TIPO_VENCIMENTO, aca_adesao_dep, dcc_dias_boleto from sf_configuracao) as configuracao
            cross apply (select *, DATEDIFF(day,(select case when x.parcela = 0 or x.id_prim_mens = x.id_mens then x.dt_inicio_mens 
            else DATEADD(day,-configuracao.dcc_dias_boleto, x.dt_inicio_mens) end as data_prazo), x.prev_mens) as prorata,
            case when x.parcela = 0 or x.id_prim_mens = x.id_mens then x.dt_inicio_mens else DATEADD(day,-configuracao.dcc_dias_boleto, x.dt_inicio_mens) end as data_prazo
            from (select top 1 case when (pp.parcela = 0) then 'DCC' when (pp.parcela = -1) then 'BOLETO'
            when (pp.parcela = -2) then 'PIX' else 'MENSAL' end as tipo_pagamento,
            (select top 1 id_mens from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano order by dt_inicio_mens asc) as id_prim_mens,
            (SELECT TOP 1 s.preco_venda FROM sf_produtos s WHERE s.tipo = 'S' AND s.conta_produto = B.conta_produto_adesao) as taxa_adesao,
            isnull((select max(dt_inicio_mens) from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano and id_mens < vpm.id_mens), dateadd(month, -(select case when pp.parcela < 1 then 1 else pp.parcela end), dt_inicio_mens)) as prev_mens,
            id_mens, A.favorecido, A.id_plano, A.id_prod_plano, A.planos_status, B.conta_produto_adesao, B.adesao_soma, pp.id_parcela, pp.parcela, vpm.dt_inicio_mens
            from sf_vendas_planos A inner join sf_produtos B on B.conta_produto = A.id_prod_plano
            inner join sf_produtos_parcelas pp ON pp.id_parcela = vpm.id_parc_prod_mens
            where  A.id_plano = id_plano_mens) as x) as vp  
            outer apply (select * from (select top 1 cupom_meses as convenios_meses_site, id_convenio, valor, tp_valor, co.cupom, co.descricao as convenio_descricao,
            isnull((select top 1 id_prod_convenio from sf_convenios_planos where id_convenio_planos = p.conta_produto and id_prod_convenio = p.conta_produto_adesao), 0) as desconto_adesao 
            from sf_fornecedores_despesas_convenios fdc inner join sf_convenios co on co.id_convenio = fdc.convenio
            inner join sf_convenios_regras cr on cr.convenio = co.id_convenio left join sf_produtos p on co.id_convenio = p.convenios_site
            where ((co.convenio_especifico = 1 and (conta_produto = p.conta_produto or co.id_convenio in (select id_convenio_planos from sf_convenios_planos
            where id_prod_convenio = p.conta_produto))) or co.convenio_especifico = 0) and co.ativo = 0) as x) as convenio 
            outer apply (select sum(valor * dependentes) as acrescimo,  novo, dependentes, valor as valor_dependente, 
            sum((case when id is null or valor = 0 then 0
            when config_adesao = 1 and (convenio_desconto = 0 or convenio_desconto is null) then taxa_adesao 
            when config_adesao = 1 and convenio_desconto = 1 and tipo_convenio = 'D' then taxa_adesao - convenio_valor  
            when config_adesao = 1 and convenio_desconto = 1 and tipo_convenio = 'P' 
            then taxa_adesao - (valor * convenio_valor) / 100 else 0 end) * novo) as adesao_dependente
            from (select id, valor, taxa_adesao, count(case when cast(data_inclusao as date) <= x.dt_prazo then id_dependente else null end) as dependentes, config_adesao, convenio_desconto, tipo_convenio, convenio_valor,
            count(case when (cast(data_inclusao as date) <= x.dt_prazo and cast(data_inclusao as date) > DATEADD(day, x.prorata, x.dt_prazo)) then id_dependente else null end) as novo 
            from (select id, id_dependente, valor, data_inclusao, vp.data_prazo as dt_prazo, vp.prorata as prorata, vp.taxa_adesao as taxa_adesao, 
            convenio.tp_valor as tipo_convenio, convenio.valor as convenio_valor, convenio.desconto_adesao as convenio_desconto, configuracao.aca_adesao_dep as config_adesao
            from sf_vendas_planos inner join sf_fornecedores_despesas_dependentes fdd on fdd.id_titular = favorecido
            inner join sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = fdd.id_dependente and fd.inativo = 0
            left join sf_produtos_acrescimo on id_produto = id_prod_plano and ((datediff(year,data_nascimento,getdate()) between idade_min and idade_max_m and sexo = 'M') or
            (datediff(year,data_nascimento,getdate()) between idade_min and idade_max_f and sexo = 'F')) and (id_parentesco is null or id_parentesco = parentesco)
            where id_plano = vp.id_plano) as x group by id, valor, taxa_adesao, config_adesao, convenio_desconto, tipo_convenio, convenio_valor) as y group by id, valor, dependentes, novo) as dependentes
            where vpm.id_mens = ".valoresSelect2($id_mens));
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $row['valor_pagar'] = escreverNumero($row['valor_pagar']);
                $row['taxa_adesao'] = escreverNumero($row['taxa_adesao']);
                $dados = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Retorna um array com as informações da Mensalidade
     * @param array $row
     * @return array 
     */
    private function getResponseMensalidade($row) {
        return [
            'id_venda'          => $row['id_venda'],
            'id_plano'          => $row['id_plano_mens'],
            'tipo_pagamento'    => escreverTexto($row['abreviacao'] ? $row['abreviacao'] : $row['tipo_pagamento']),
            'acrescimo'         => escreverNumero($row['acrescimo'],1),
            'parcela'           => $row['parcela'],
            'parcelar_site'     => boolval($row['parcelar_site']),
            'id_parcela'        => $row['id_parcela'],
            'url_boleto'        => ($row['id_boleto'] && !$row['dt_pagamento_mens']) ? ERP_URL.'Boletos/Boleto.php?id=M-'.$row['id_mens'].'&crt='.CONTRATO : null,
            'dt_fim_mens'       => escreverData($row['dt_fim_mens']),
            'dt_inicio_mens'    => escreverData($row['dt_inicio_mens']),
            'valor_mens'        => escreverNumero($row['valor_real_mens'],1,2),
            'valor_adesao'      => escreverNumero($row['valor_adesao'], 1),
            'valor_pago'        => $row['valor_parcela'] ? escreverNumero($row['valor_parcela'],1,2) : '---',
            'valor_unit'       => escreverNumero($row['valor_mens'], 1),
            'legenda_cartao'    => $row['legenda_cartao'],
            'dependentes'       => $row['dependentes'],
            'novo_dependente'   => $row['novo'],
            'taxa_adesao'       => escreverNumero($row['taxa_adesao'], 1),
            'dt_pagamento_mens' => $row['dt_pagamento_mens'] ? escreverData($row['dt_pagamento_mens']) : '---',
            'id_mens'           => $row['id_mens'],
            'id_prim_mens'      => $row['id_prim_mens'],
            'status'            => escreverTexto($row['status_mens'])
        ];
    }
}