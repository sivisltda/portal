<?php

/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 28/10/2019
 * Time: 12:07
 */
class UsuarioRepository {

    /**
     * Função que retorna o id e o nome do usuário via email
     * @param string $email
     * @return array
     * @throws Exception
     */
    public function getUsuarioPorEmail($email) {
        try {
            $dados = [];
            $sql = "SELECT TOP 1 ".$this->getQueryUsuario()." 
            INNER JOIN sf_fornecedores_despesas_contatos fdc ON fdc.fornecedores_despesas = fd.id_fornecedores_despesas
            WHERE conteudo_contato = " . valoresTexto2($email) . " AND tipo_contato = 2 
            AND(fornecedores_status != 'Excluido' or fornecedores_status is null) AND tipo not in ('I', 'E')";
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                if ($row['cnpj']) {
                    $dados['id_fornecedores_despesas'] = $row['id_fornecedores_despesas'];
                    $dados['razao_social'] = escreverTexto($row['razao_social']);
                    $dados['hasCnpj'] = !!$row['cnpj'];
                } else {
                    $dados = $this->responseUsuario($row);
                }
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Função que retorna o id e o nome do usuário via email
     * @param $email
     * @return array
     * @throws Exception
     */
    public function getUsuarioPorEmailParaApi($email) {
        try {
            $dados = [];
            $sql = "SELECT TOP 1 id_fornecedores_despesas, razao_social FROM sf_fornecedores_despesas fd
            INNER JOIN sf_fornecedores_despesas_contatos fdc ON fdc.fornecedores_despesas = fd.id_fornecedores_despesas
            WHERE conteudo_contato = " . valoresTexto2($email) . " AND tipo_contato = 2 
            AND(fornecedores_status != 'Excluido' or fornecedores_status is null) AND tipo not in ('I', 'E')";
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $dados['id_fornecedores_despesas'] = $row['id_fornecedores_despesas'];
                $dados['razao_social'] = escreverTexto($row['razao_social']);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Retorna uma lista de usuário de acorod com a condição passada
     * @param $condicao
     * @return array
     * @throws Exception
     */
    public function listaUsuarios($condicao) {
        try {
            $dados = [];
            $sql = "select ".$this->getQueryUsuario()." $condicao";
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $dados[] = $this->responseUsuario($row);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function listaUsuariosPesquisa($condicao) {
        try {
            $dados = [];
            $sql = "select * from sf_fornecedores_despesas WHERE (fornecedores_status != 'Excluido' and inativo = 0) AND $condicao";
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $dados[] = [
                    'id'                    => $row['id_fornecedores_despesas'],
                    'razao_social'          => escreverTexto($row['razao_social']),
                    'tipo'                  => $row['tipo']
                ];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Retorna o usuario pelo ID
     * @param $usuario_id
     * @return array
     * @throws Exception
     */
    public function getUsuarioPorId($usuario_id) {
        try {
            $dados = [];
            $sql = "SELECT TOP 1 ".$this->getQueryUsuario()." 
            WHERE id_fornecedores_despesas = " . valoresSelect2($usuario_id) ." AND(fornecedores_status != 'Excluido' 
            or fornecedores_status is null)";
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $dados = $this->responseUsuario($row);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Função retorna o usuário pelo CPF
     * @param $cnpj
     * @param $usuario_id
     * @return array
     * @throws Exception
     */
    public function getUsuarioPorCNPJ($cnpj, $usuario_id) {
        try {
            $dados = [];
            $cnpj = str_replace(".",'', str_replace("-",'',$cnpj));
            $sql = "SELECT TOP 1 ".$this->getQueryUsuario()." WHERE SUBSTRING(REPLACE(REPLACE(cnpj, '.', '') , '-', ''), 1, 3) = " . valoresTexto2($cnpj) . " 
            AND id_fornecedores_despesas = " . valoresSelect2($usuario_id)." AND (fornecedores_status != 'Excluido' 
            or fornecedores_status is null) AND tipo not in ('I', 'E')";
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $dados = $this->responseUsuario($row);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function verificaClientePlanoAtivo($usuario_id) {
        try {
            $sql = "select id_fornecedores_despesas from sf_vendas_planos vp
            inner join sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = vp.favorecido
            where (id_fornecedores_despesas = ".valoresSelect2($usuario_id)." or id_fornecedores_despesas 
            in (select id_titular from sf_fornecedores_despesas_dependentes where id_dependente = ".valoresSelect2($usuario_id)." and compartilhado = 1))
            and fornecedores_status not in ('Excluido', 'Inativo') and tipo != 'P';";
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados[] = $row;
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Função que salva o Usuário
     * @param $dados
     * @param null $id
     * @return int $usuario_id
     * @throws Exception
     */
    public function salvarUsuario($dados, $id = null) {
        try {
            Conexao::initTransaction();
            if(!isset($id) && ($dados['email'] || (isset($dados['cnpj']) && $dados['cnpj']))) {
                $cnpj = isset($dados['cnpj']) ? $dados['cnpj'] : '';
                $dados = array_merge($this->selecionaUsuarioPorEmailOuCPF($dados['email'], $cnpj), $dados);
                $id = isset($dados['id']) ? $dados['id'] : null;
            } else {
                $dados = array_merge($this->getUsuarioPorId($id), $dados);
            }           
            if (!$id) {
                $dados['tipo'] = isset($dados['tipo']) ? $dados['tipo'] : 'P';
                $dados['procedencia'] = (isset($dados['procedencia']) && $dados['procedencia']) ? $dados['procedencia'] : 0;
                $dados['termometro'] = 2;
                $dados['juridico_tipo'] = strlen($cnpj) == 18 ? 'J' : 'F';
                $dados['empresa'] = isset($dados['empresa']) ? $dados['empresa'] : 1;
                $usuario_id = $this->inserirUsuario($dados);
                $this->atualizaUsuarioResponsavel(array_merge($dados, ['id' => $usuario_id]));
                salvarLog(['tabela' => 'sf_fornecedores_despesas', 'id_item' => $usuario_id,
                'usuario' => getLoginUser(), 'acao' => 'I', 'descricao' => 'INCLUSAO PROSPECT',
                'id_fornecedores_despesas' => $usuario_id]);
            } else {
                $usuario_id = $this->atualizaUsuario($dados, $id);
            }            
            $this->atualizarStatus("id_fornecedores_despesas = ".valoresSelect2($usuario_id)." AND fornecedores_status not in ('Excluido', 'Dependente')");            
            if (isset($dados['email']) && $dados['email']) {
                $this->verificaSalvaContatos($dados['email'], $usuario_id, 2);
            }
            if (isset($dados['telefone']) && $dados['telefone']) {
                $this->verificaSalvaContatos($dados['telefone'], $usuario_id, 0);
            }
            if (isset($dados['celular']) && $dados['celular']) {
                $this->verificaSalvaContatos($dados['celular'], $usuario_id, 1);
            }
            $this->inserirDescIsencaoIe($dados['nfe_iss'], $usuario_id);
            Conexao::commit();
            return $usuario_id;
        } catch (Exception $e) {
            Conexao::rollback();
            throw new Exception($e->getMessage());
        }
    }

    public function inserirDescIsencaoIe($nfe_iss, $usuario_id) {
        $dt_inicio = "";        
        if ($nfe_iss == 1 && is_numeric(DESC_ISENCAO_IE) && is_numeric($usuario_id)) {
            $query_verify = "select dt_inicio from sf_fornecedores_despesas_convenios
            where id_fornecedor = " . $usuario_id . " and convenio = " . DESC_ISENCAO_IE;
            $curV = Conexao::conect($query_verify);
            while ($rows = odbc_fetch_array($curV)) {
                $dt_inicio = $rows['dt_inicio'];
            }      
            if (strlen($dt_inicio) == 0) {
                $query = "insert into sf_fornecedores_despesas_convenios (id_fornecedor, convenio, dt_inicio, dt_fim) 
                values (" . $usuario_id . "," . DESC_ISENCAO_IE . ", cast(GETDATE() as date), cast(dateadd(year, 1, GETDATE()) as date))";
                Conexao::conect($query);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Verifica e salva o contato caso não estiver registro
     * @param string $contato
     * @param int $usuario_id
     * @param int $tipo
     * @throws Exception
     */
    public function verificaSalvaContatos($contato, $usuario_id, $tipo) {
        try {
            $cadastra_contato = 1;
            $contato_id = null;

            if (isset($contato)) {
                $query_verify_contato = " SELECT TOP 1 id_contatos, conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE tipo_contato = " . $tipo . " AND fornecedores_despesas = ".valoresSelect2($usuario_id);
                $curV = Conexao::conect($query_verify_contato);
                while ($rows = odbc_fetch_array($curV)) {
                    $contato_id = $rows['id_contatos'];
                    $cadastra_contato = 0;
                }
                if ($cadastra_contato) {
                    $sQuery3 = "INSERT INTO sf_fornecedores_despesas_contatos(fornecedores_despesas, tipo_contato, conteudo_contato) VALUES (" . $usuario_id . "," . $tipo . "," . valoresTexto2($contato) . ")";
                    Conexao::conect($sQuery3);
                } else {
                    $sQuery2 = "UPDATE sf_fornecedores_despesas_contatos SET conteudo_contato = " . valoresTexto2($contato) . " WHERE id_contatos = $contato_id";
                    Conexao::conect($sQuery2);
                }
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Pega os dados do usuário por credencial
     * @param array $data
     * @return array $dados
     * @throws Exception
     */
    public function getUsuarioPorCredencial($data) {
        try {
            $query = "SELECT id_fornecedores_despesas, case when (d.tipo_departamento in (1,2)) then 'G' else tipo end as tipo,
             u.login_user, u.id_usuario, razao_social FROM sf_fornecedores_despesas f
            INNER JOIN sf_fornecedores_despesas_contatos c ON c.fornecedores_despesas = id_fornecedores_despesas
            LEFT JOIN sf_usuarios u ON u.funcionario = id_fornecedores_despesas and u.inativo = 0 and tipo in ('I', 'E')
            LEFT JOIN sf_departamentos d ON d.id_departamento = u.departamento
            WHERE conteudo_contato = ".valoresTexto2($data['email'])." AND REPLACE(REPLACE(REPLACE(cnpj, '.', '') , '-', ''), '/', '') = ".valoresTexto2($data['senha'])."
            AND tipo in ('C', 'E', 'I') AND f.inativo = 0 AND (fornecedores_status != 'Excluido' or fornecedores_status is null);";
            //echo $query; exit;
            $dados = [];
            $res = Conexao::conect($query);
            while ($row = odbc_fetch_array($res)) {
                $dados[] = [
                 'id_fornecedores_despesas' => $row['id_fornecedores_despesas'],
                 'tipo'                     => $row['tipo'],
                 'login_user'               => escreverTexto($row['login_user']),
                 'id_usuario'               => $row['id_usuario'],
                 'razao_social'             => escreverTexto($row['razao_social'])
                ];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    /**
     * Cadastrar logs dos usuários
     * @param string $data
     * @throws Exception
     */
    public function setUsuarioLogin($data) {
        try {
            $query = "INSERT INTO sf_login_logs(data_acao,sys_login,ip) VALUES
            (GETDATE()," . valoresTexto2((strlen($data['login_user']) > 0 ? $data['login_user'] . "*" : $data['email'])) . "," . 
            valoresTexto2($_SERVER['REMOTE_ADDR']) . ")";
            Conexao::conect($query);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Response do Usuário
     * @param $row
     * @return array
     */
    public function responseUsuario($row) {
        return [
            'id'                    => $row['id_fornecedores_despesas'],
            'razao_social'          => escreverTexto($row['razao_social']),
            'tipo'                  => $row['tipo'],
            'cep'                   => $row['cep'],
            'endereco'              => escreverTexto($row['endereco']),
            'numero'                => $row['numero'],
            'bairro'                => escreverTexto($row['bairro']),
            'complemento'           => escreverTexto($row['complemento']),
            'cidade'                => escreverTexto($row['cidade']),
            'cidade_nome'           => escreverTexto($row['cidade_nome']),
            'estado'                => escreverTexto($row['estado']),
            'estado_nome'           => escreverTexto($row['estado_nome']),
            'estado_sigla'          => escreverTexto($row['estado_sigla']),
            'email_marketing'       => $row['email_marketing'],
            'celular_marketing'     => $row['celular_marketing'],
            'indicador'             => $row['prof_resp'],
            'cnpj'                  => $row['cnpj'],
            'inscricao_estadual'    => $row['inscricao_estadual'],
            'regime_tributario'     => $row['regime_tributario'],
            'nfe_iss'               => $row['nfe_iss'],
            'contato'               => escreverTexto($row['contato']),
            'sexo'                  => $row['sexo'],
            'data_nascimento'       => escreverData($row['data_nascimento']),
            'estado_civil'          => escreverTexto($row['estado_civil']),
            'email'                 => escreverTexto($row['email']),
            'telefone'              => $row['telefone'],
            'celular'               => $row['celular'],
            'inativo'               => $row['inativo'],
            'procedencia'           => escreverTexto($row['procedencia']),
            'fornecedores_status'   => escreverTexto($row['fornecedores_status']),
            'juridico_tipo'         => $row['juridico_tipo'],
            'data_cadastro'         => escreverData($row['dt_cadastro'])
        ];
    }

    /**
     * Inserir o chamado
     * @param $id_usuario
     * @param $usuario_resp
     * @return int
     * @throws Exception
     * $id_usuario, $usuario_resp, $mensagem, $obs, $procedencia
     */
    public function inserirChamado($data) {
        try {
            $sql = " insert into sf_telemarketing(de_pessoa, para_pessoa, data_tele, data_tele_fechamento, pendente, proximo_contato, pessoa, 
            especificacao, procedencia, relato, dt_inclusao, obs, obs_fechamento, acompanhar, origem, atendimento_servico) values(".valoresSelect2($data['usuario_resp']).", 
            ".valoresSelect2($data['usuario_resp']).", getdate(), getdate(), 1, getdate(), ".valoresSelect2($data['id_usuario']).", 'R', ".verificaValorCampo($data, 'procedencia').", 
            ".valoresTexto2($data['mensagem']).", getdate(), ".valoresTexto2($data['obs']).", ".valoresTexto2($data['obs']).", 0, 0, 0); SELECT SCOPE_IDENTITY() id;";
            $result = Conexao::conect($sql);
            odbc_next_result($result);
            $id = odbc_result($result, 1);
            if (!is_numeric($id)) {
                throw new Exception("Erro ao inserir o chamado => ".$sql);
            }
            return $id;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Atualiza o status do Prospect
     * @param string $condicao
     * @throws Exception
     */
    public function atualizarStatus($condicao) {
        try {
            $sql = "update sf_fornecedores_despesas set fornecedores_status = (
            case when fornecedores_status = 'Dependente' then 'Dependente'
            when tipo = 'P' then dbo.FU_STATUS_PRO(id_fornecedores_despesas) 
            else dbo.FU_STATUS_CLI(id_fornecedores_despesas) end)
            where ".$condicao;
            Conexao::conect($sql);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function isPropect($idUsuario) {
        try {
            $sql = "select top 1 id_fornecedores_despesas from sf_fornecedores_despesas 
            where id_fornecedores_despesas = ".valoresSelect2($idUsuario)." and dt_convert_cliente is null 
            and tipo = 'P' and (id_user_resp is null and prof_resp is null);";
            $res = Conexao::conect($sql);
            $id = odbc_result($res, 1);
            return $id;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function listaUsuariosParaChamado() {
        try {
            $sql = "select u.id_usuario from sf_usuarios u
            inner join sf_departamentos d on d.id_departamento = u.departamento
            where d.id_departamento in (select adm_dep_online from sf_configuracao) and u.inativo = 0;";
            $dados = [];
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados[] = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function inativarUsuario($id_usuario) {
        try {
            $sql = "update sf_fornecedores_despesas set fornecedores_status = 'Inativo' where id_fornecedores_despesas = ".valoresSelect2($id_usuario);
            Conexao::conect($sql);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Atualiza os dados da Procedência
     * @param int $procedencia
     * @param int $usuario_id
     * @throws Exception
     */
    public function atualizarProcedencia($procedencia, $usuario_id) {
        try {
            if($procedencia) {
                $sql_query_proc = "UPDATE sf_fornecedores_despesas SET procedencia = $procedencia  WHERE id_fornecedores_despesas = $usuario_id";
            } else {
                $sql_query_proc = "update sf_fornecedores_despesas set procedencia = 
               (select top 1 procedencia from sf_fornecedores_despesas where  procedencia is not null and (id_fornecedores_despesas = ".valoresSelect2($usuario_id)."
               or id_fornecedores_despesas = (select top 1 id_titular from sf_fornecedores_despesas_dependentes where id_dependente = ".valoresSelect2($usuario_id).")))
                where id_fornecedores_despesas = ".valoresSelect2($usuario_id);
            }
            Conexao::conect($sql_query_proc);
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }

    }

    /**
     * Atualiza a autorização de marketing
     * @param $campo
     * @param $valor
     * @param $usuario_id
     * @throws Exception
     */
    public function atualizaMarketing($campo, $valor, $usuario_id) {
        try {
            $sql_query_proc = "UPDATE sf_fornecedores_despesas SET $campo = ".valoresSelect2($valor)." WHERE id_fornecedores_despesas = $usuario_id";
            Conexao::conect($sql_query_proc);
        }catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Converte Prospect para Cliente
     * @param int $usuario_id
     * @throws Exception
     */
    public function converteProspectCliente($usuario_id) {
        try {
            $sql = "update sf_fornecedores_despesas set tipo = 'C', dt_convert_cliente = getdate() where tipo = 'P' 
            and (id_fornecedores_despesas = " .valoresSelect2($usuario_id).
            " OR id_fornecedores_despesas in (SELECT id_dependente from sf_fornecedores_despesas_dependentes where id_titular = " .valoresSelect2($usuario_id). "));";
            Conexao::conect($sql);
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Query do Usuário
     * @return string
     */
    private function getQueryUsuario() {
        return "fd.*, ci.cidade_nome, es.estado_nome, es.estado_sigla,
            (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = id_fornecedores_despesas AND tipo_contato = 2) as email,
            (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = id_fornecedores_despesas AND tipo_contato = 0) as telefone,
            (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = id_fornecedores_despesas AND tipo_contato = 1) as celular
            FROM sf_fornecedores_despesas fd
            LEFT JOIN tb_cidades ci on ci.cidade_codigo = fd.cidade
            LEFT JOIN tb_estados es on es.estado_codigo = fd.estado";
    }


    /**
     * Inserir o Usuário
     * @param array $data
     * @throws Exception
     * @return int $id
     */
    private function inserirUsuario($data) {
        try {
            $status = (isset($data['fornecedores_status']) && $data['fornecedores_status'] === 'Dependente' ? valoresTexto2('Dependente') : valoresTexto2('Aguardando'));
            $id_user = (isset($data['id_user_resp']) ? $data['id_user_resp'] : null);
            $indicador = (isset($data['indicador']) ? $data['indicador'] : (is_numeric($id_user) ? "(select max(f1.prof_resp) from sf_usuarios u1 inner join sf_fornecedores_despesas f1 on u1.funcionario = f1.id_fornecedores_despesas where u1.id_usuario = " . $id_user . ")" : null));
            $sql = "set dateformat dmy; insert into sf_fornecedores_despesas(empresa, razao_social,cnpj, endereco, numero, comiss_prod, comiss_gere, comiss_serv,
            complemento, bairro, cidade, estado, cep, inativo, tipo, juridico_tipo, data_nascimento, pri_cnh, sexo, procedencia, 
            dt_cadastro, dt_aprov_prospect, email_marketing, dt_convert_cliente, grupo_pessoa, estado_civil, id_user_resp, fornecedores_status, 
            socio, titular, titularidade, tipo_socio, departamento, termometro,
            recebe_comissao, tbcomiss_prod, tbcomiss_serv, tbcomiss_gere, tbcomiss_plano, comiss_plano, comiss_planoPri, comiss_gerePri, tbcomiss_prod_tipo,
            tbcomiss_ser_tipo, trancar, trabalha_empresa, contato, regime_tributario, inscricao_estadual, prof_resp, nfe_iss) values (
            (select isnull(max(empresa), '1') from sf_usuarios u1 inner join sf_fornecedores_despesas f1 on u1.funcionario = f1.id_fornecedores_despesas where u1.id_usuario = " . valoresSelect2($id_user) . "),
            ".valoresTexto2($data['razao_social']).",".verificaValorTextoCampo($data, 'cnpj').",".verificaValorTextoCampo($data, 'endereco').", 
            ".verificaValorTextoCampo($data, 'numero').", 0, 0, 0, ".verificaValorTextoCampo($data, 'complemento').", 
            ".verificaValorTextoCampo($data, 'bairro').", ".verificaValorCampo($data, 'cidade').", ".verificaValorCampo($data, 'estado').", ".verificaValorTextoCampo($data, 'cep').",
            0, ".valoresTexto2($data['tipo']).", ".valoresTexto2($data['juridico_tipo']).", " . 
            valoresData2($data['data_nascimento']) . ", ". valoresData2($data['pri_cnh']) . ", ". verificaValorTextoCampo($data, 'sexo').",".verificaValorCampo($data, 'procedencia').", 
            getdate(), getdate(), 1, " . ($data['tipo'] == "C" ? verificaValorTextoCampo($data, 'dt_convert_cliente') : "null") . ", null, ".verificaValorTextoCampo($data, 'estado_civil').", ". 
            valoresSelect2($id_user).", ".$status.",". valoresNumericos2($data['socio']).",". valoresNumericos2($data['titular']).", null, null, null, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,".verificaValorTextoCampo($data, 'nome_empresa').",".
            verificaValorTextoCampo($data, 'contato'). "," . (isset($data['regime_tributario']) ? valoresSelect2($data['regime_tributario']) : 9).",".valoresTexto2($data['inscricao_estadual']).",". 
            valoresSelect2($indicador) . "," . valoresCheck2($data['nfe_iss']) . "); SELECT SCOPE_IDENTITY() id;";
            //echo $sql; exit;
            $result = Conexao::conect($sql);
            odbc_next_result($result);
            $id = odbc_result($result, 1);
            if(!is_numeric($id)) {
                throw new Exception("Erro ao inserir o usuário => ".$sql);
            }
            return $id;
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }


    /**
     * Atualiza o Usuário
     * @param array $data
     * @param int $id
     * @throws Exception
     * @return int $id
     */
    private function atualizaUsuario($data, $id) {
        try {
            $sql = "set dateformat dmy; UPDATE sf_fornecedores_despesas set razao_social = ".valoresTexto2($data['razao_social']).", cnpj = ".verificaValorTextoCampo($data, 'cnpj').",
            endereco = ".valoresTexto2($data['endereco']).", numero = ".valoresTexto2($data['numero']).", complemento =".valoresTexto2($data['complemento']).",
            bairro = ".valoresTexto2($data['bairro']).", cidade = ".verificaValorCampo($data,'cidade').", estado = ".verificaValorCampo($data, 'estado').",
            cep = ".valoresTexto2($data['cep']).", inscricao_estadual = ".valoresTexto2($data['inscricao_estadual']).", procedencia = ".valoresSelect2($data['procedencia']).",
            estado_civil = ".valoresTexto2($data['estado_civil']).", sexo = ".valoresTexto2($data['sexo']). ", data_nascimento = ".valoresData2($data['data_nascimento']). ",
            pri_cnh = ".valoresData2($data['pri_cnh']).", regime_tributario = ".(isset($data['regime_tributario']) ? valoresSelect2($data['regime_tributario']) : 9).",  
            contato = ".verificaValorTextoCampo($data, 'contato').", fornecedores_status = dbo.FU_STATUS_PRO(id_fornecedores_despesas), inativo = 0,
            nfe_iss = ". valoresCheck2($data['nfe_iss'])." where id_fornecedores_despesas = ".valoresSelect2($id);
            Conexao::conect($sql);
            return $id;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    

    /**
     * Seleciona usuário por email ou CPF
     * @param string $email
     * @param string $cpf
     * @param int|null $id_usuario
     * @throws Exception
     * @return array $dados
     *  
     */
    public function selecionaUsuarioPorEmailOuCPF($email, $cpf = null, $id_usuario = null) {
        try {
            $dados = [];
            if($cpf) {
                $where = "fd.cnpj = ".valoresTexto2($cpf);
            } else if($email) {
                $where = "conteudo_contato = ".valoresTexto2($email);
            }
            if($id_usuario) {
                $where.= " and fd.id_fornecedores_despesas != ".valoresSelect2($id_usuario);
            }
            $sql = "SELECT TOP 1 ".$this->getQueryUsuario()."
            LEFT JOIN sf_fornecedores_despesas_contatos b on b.fornecedores_despesas = fd.id_fornecedores_despesas
            where fd.tipo in ('P', 'C')  and (fd.fornecedores_status != 'Excluido' and fd.inativo = 0) and ".$where.";";
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados = $this->responseUsuario($row);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getUsuarios($condicao, $paginacao = []) {
        try {
            $where = $condicao ? ' AND '.$condicao : '';
            $sql = "select ".$this->getQueryUsuario().$where;
            $sqlTotal = "";
            if(count($paginacao) > 0) {
                $limit = $paginacao['limit'];
                $qtd = $paginacao['qtd'];
                $sqlTotal = "select count(*) as total from (".$sql.") as a";
                $sql = "SELECT * FROM (SELECT ROW_NUMBER() OVER (order by dt_cadastro asc) as row, * 
                        FROM (".$sql.") as x) as a where row BETWEEN " . ($limit + 1) . "AND " . ($limit + $qtd)." order by dt_cadastro asc;";
            }
            $query = Conexao::conect($sql);
            $data = [];
            $dados = [];
            while($rows = odbc_fetch_array($query)) {
                $data[] = $this->responseUsuario($rows);
            }
            if(count($paginacao) > 0) {
                $resTotal = Conexao::conect($sqlTotal);
                $dados['aaData'] = $data;
                $dados['total'] = odbc_result($resTotal, 'total');
            } else {
                $dados = $data;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());  
        }
    }

    /**
     * Pega os dados do usuário para o validador
     * @param string $condicao
     * @return array $dados
     * @throws Exception
     */
    public function getConsultaDadosValidador($condicao) {
        try {
            $sql = "select top 1 f.id_fornecedores_despesas, razao_social, p.descricao as nome_plano,
            case when f.tipo = 'C' then dbo.FU_STATUS_CLI(favorecido) else 'Prospect' end as status,
            isnull((select login_user from sf_usuarios where id_usuario = id_user_resp), '') usuario_atual,
            v.placa, v.modelo, v.ano_modelo, v.marca
            from sf_fornecedores_despesas f 
            left join sf_fornecedores_despesas_veiculo v on v.id_fornecedores_despesas = f.id_fornecedores_despesas
            left join sf_fornecedores_despesas_contatos fc on fc.fornecedores_despesas = f.id_fornecedores_despesas
            left join sf_fornecedores_despesas_campos_livres fcl on fcl.fornecedores_campos = f.id_fornecedores_despesas 
            and campos_campos = (select top 1 id_campo from sf_configuracao_campos_livres where busca_campo = 1 and ativo_campo = 1)
            left join sf_vendas_planos vp on favorecido = f.id_fornecedores_despesas 
            or favorecido = (select top 1 id_titular from sf_fornecedores_despesas_dependentes where id_dependente = f.id_fornecedores_despesas) 
            and planos_status not in ('Cancelado', 'Inativo')
            left join sf_produtos p on p.conta_produto = vp.id_prod_plano 
            where f.tipo in ('P', 'C') and fornecedores_status not in ('Excluido') and inativo = 0 and ".$condicao;
            //echo $sql; exit;
            $res = Conexao::conect($sql);
            $modulos = getModulos();
            $dados = [];
            while($row = odbc_fetch_array($res)) {
                $row['razao_social'] = escreverTexto($row['razao_social']);
                $row['nome_plano'] = escreverTexto($row['nome_plano']);
                $row['status'] = escreverTexto($row['status']);
                $row['usuario_atual'] = escreverTexto($row['usuario_atual']);
                if ($modulos['seg']) {
                    $row['placa']  = escreverTexto($row['placa']);
                    $row['modelo'] = escreverTexto($row['modelo']);
                    $row['marca']  = escreverTexto($row['marca']);
                }
                $dados = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function isVerificaEmail($email, $idUsuario) {
        try {
            $sql = "select top 1 id_fornecedores_despesas from sf_fornecedores_despesas fd
            inner join sf_fornecedores_despesas_contatos fc on fc.fornecedores_despesas = fd.id_fornecedores_despesas
            and fc.tipo_contato = 2 where fd.tipo in ('C', 'P') and fornecedores_status not in ('Excluído') and inativo = 0
            and conteudo_contato like ".valoresTexto2($email)." and id_fornecedores_despesas = ".valoresSelect2($idUsuario).";";
            $res = Conexao::conect($sql);
            $id = odbc_result($res, 1);
            if(!is_numeric($id)) {
                return false;
            }
            return $id;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function atualizaUsuarioResponsavel($data) {
        try {
            if (!(isset($data['id_user_resp']) && $data['id_user_resp'])) {
                Conexao::conect("update sf_configuracao set id_ultimo_usuario = isnull((select top 1 id from sf_configuracao_usuarios where id > 
                (select isnull(id_ultimo_usuario, 0) from sf_configuracao) order by id), (select min(id) from sf_configuracao_usuarios));");
                Conexao::conect("update sf_fornecedores_despesas set id_user_resp = (select u.id_usuario from sf_configuracao c 
                inner join sf_configuracao_usuarios u on c.id_ultimo_usuario = u.id) where id_fornecedores_despesas = " .valoresSelect2($data['id']));
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getStatus() {
        $sql= "select distinct fornecedores_status from sf_fornecedores_despesas where tipo = 'P' and fornecedores_status != 'Excluido';";
        $res = Conexao::conect($sql);
        $dados = [];
        while($row = odbc_fetch_array($res)) {
            $dados[] = escreverTexto($row['fornecedores_status']);
        }
        return $dados;
    }
}
