<?php
class FaqRepository {

    /**
     * Lista as Faqs
     * @param string|null $condicao
     * @return array $dados
     * @throws Exception 
     */
    public function listaFaq($condicao = "") {
        try {
            $sql = "select * from sf_faq".$condicao;
            $res = Conexao::conect($sql);
            $dados = [];
            while($row = odbc_fetch_array($res)) {
                $dados[] = [
                  'id'          => $row['id_faq'],
                  'data_pub'    => escreverDataHora($row['data_pub']),
                  'titulo'      => escreverTexto($row['titulo']),
                  'conteudo'    => escreverTexto($row['conteudo'])
                ];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}