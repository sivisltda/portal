<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 02/04/2020
 * Time: 10:13
 */

class PagamentoRepository {   
    /**
     * Lista as mensalidades para o carousel de mensalidades
     * @param $plano_id
     * @return array
     * @throws Exception
     */
    public  function getListaMensalidadesPorPlano($plano_id) {
        try {
            $sql = "SELECT ROW_NUMBER() OVER (ORDER BY dt_inicio_mens desc, id_mens desc) as row,
                ROW_NUMBER() OVER (ORDER BY dt_inicio_mens asc) as num_mens,
                DATEPART(MONTH, dt_inicio_mens) as num_mes, DATEPART(YEAR, dt_inicio_mens) as num_ano,
                 * FROM (".$this->queryBase().") as a               
                where id_plano_mens = ".valoresSelect2($plano_id)." order by num_mens asc;";
                //echo $sql; exit;
            $dados = [];
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados[] = [
                    'row'                   => $row['row'],
                    'num_mens'              => $row['num_mens'],
                    'id_parcela'            => $row['id_parcela'],
                    'parcela'               => $row['parcela'],
                    'id_parcela_boleto'     => $row['id_parcela_boleto'],
                    'id_parcela_cartao'     => $row['id_parcela_cartao'],
                    'exp_remessa'           => $row['exp_remessa'],
                    'id_boleto'             => $row['id_boleto'],
                    'label_mens'            => labelMensalidade($row),
                    'id_mens'               => $row['id_mens'],
                    'dt_inicio_mens'        => escreverData($row['dt_inicio_mens']),
                    'dt_fim_mens'           => escreverData($row['dt_fim_mens']),
                    'dt_pagamento'          => escreverData($row['dt_pagamento_mens']),
                    'ord_pag'               => $row['ord_pag'],
                    'tipo_pag_abrev'        => escreverTexto($row['tipo_pagamento']),
                    'descricao_pagamento'   => escreverTexto($row['descricao_pagamento']),
                    'valor_transacao'       => escreverNumero($row['valor_pagar'], 1),
                    'valor_pago'            => escreverNumero($row['valor_total'], 1),
                    'id_venda'              => $row['id_venda'],
                    'url_boleto'            => linkBoleto($row),
                    'numero_cartao'         => $row['numero_cartao'],
                    'bandeira'              => verificar_bandeira($row['numero_cartao']),
                    'data_transacao'        => escreverData($row['data_transacao']),
                    'status'                => escreverTexto($row['status']),
                    'transacoes'            => $this->listaTransacoes($row['id_mens'])
                ];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Retorna a lista de transações do DCC
     * @param $condicao
     * @param array|null $pagination
     * @return array
     * @throws Exception
     */
    public function getListaTransacoesDCC($condicao, $pagination = []) {
        try {
            $dados = [];
            $sqlTotal = "";
            $sqlBase = "select * from (select fd.razao_social, vic.*, case when tp.cartao_dcc = 1 then 'Cartão de Crédito' 
			when tp.cartao_dcc = 2 then 'Cartão de Débito' else tp.descricao end as tipo_documento,
			case when vic.status_transacao = 'D' then 'Negada' when vic.status_transacao = 'I' then 'Inválida'
			when vic.status_transacao = 'E' then 'Estornado' when vic.status_transacao = 'A' then 'Aprovado'
			else '' end as status
			from sf_vendas_itens_dcc vic
			inner join sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = vic.id_fornecedores_despesas
			inner join sf_tipo_documento tp on tp.id_tipo_documento = vic.id_tipo_documento) as x ".$condicao;
            Conexao::conect('');
            if (count($pagination)) {
                $sql = "set dateformat dmy; SELECT * FROM (SELECT ROW_NUMBER() OVER (order by id_transacao desc, dt_modalidade desc) as row,  a.* FROM ("
                        .$sqlBase.") as a ) as b where row BETWEEN " . $pagination['limit'] . " AND " . ($pagination['limit'] + $pagination['qtd']);
                $sqlTotal = "set dateformat dmy; SELECT count(id_transacao) as total FROM (".$sqlBase.") as a";
            } else {
                $sql = "set dateformat dmy; ".$sqlBase;
            }
            $res = Conexao::conect($sql);
            $data = [];
            while($row = odbc_fetch_array($res)) {
                $data[] = [
                    'razao_social'      => escreverTexto($row['razao_social']),
                    'id_transacao'      => $row['id_transacao'],
                    'dcc_numero_cartao' => escreverTexto($row['dcc_numero_cartao']),
                    'tipo_documento'    => $row['tipo_documento'],
                    'valor_transacao'   => escreverNumero($row['valor_transacao'], 1),
                    'status'            => $row['status'],
                    'error_msg'         => $row['errorMessage'] ? escreverTexto($row['errorMessage']): '-------',
                    'data_transacao'    => escreverDataHora($row['transactionTimestamp'])
                ];
            }
            if (count($pagination)) {
                $resTotal = Conexao::conect($sqlTotal);
                $dados['aaData'] = $data;
                $dados['total'] = odbc_result($resTotal, 'total');
            } else {
                $dados = $data;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Lista de Tipos de Pagamentos
     * @param $condicao
     * @return array
     * @throws Exception
     */
    public function getTiposPagamentos($condicao) {
        try {
            $dados = [];
            $sql = "select * from sf_tipo_documento ".$condicao;
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados[] = [
                    'id_tipo_documento' => $row['id_tipo_documento'],
                    'descricao'         => valoresTexto2($row['descricao']),
                    'abreviacao'        => valoresTexto2($row['abreviacao']),
                    'cartao_dcc'        => $row['cartao_dcc']
                ];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Lista os planos
     * @param $usuario_id
     * @return array
     * @throws Exception
     */
    public function listaPlanos($usuario_id, $tipo = null) {
        try {
            $modulos = getModulos();
            $tipos_veiculos = [1, 2, 3];
            $count_rows = 0;
            $res = null;
            $i = 0;
            do {
                $tipo_veiculo = is_numeric($tipo) ? $tipo : $tipos_veiculos[$i];
                $sql = $this->selectPlanos($usuario_id, $tipo_veiculo);
                $res = Conexao::conect($sql);
                $count_rows = odbc_num_rows($res);
                $i++;
            } while(!is_numeric($tipo) && ($count_rows == 0 || count($tipos_veiculos) == $i));
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $item = [
                    'id_plano'          => $row['id_plano'],
                    'dt_inicio'         => escreverData($row['dt_inicio']),
                    'dt_fim'            => escreverData($row['dt_fim']),
                    'dt_cadastro'       => escreverData($row['dt_cadastro']),
                    'descricao'         => escreverTexto($row['descricao']),
                    'planos_status'     => escreverTexto($row['planos_status']),
                    'valor_plano'       => escreverNumero($row['valor_plano'], 1),
                    'tipo_pagamento'    => escreverTexto($row['tipo_pagamento'])
                    ];
                if($modulos['seg']) {
                    $item = array_merge($item, [
                        'id_veiculo'        => $row['id_veiculo'],
                        'placa'             => escreverTexto($row['placa']),
                        'marca'             => escreverTexto($row['marca']),
                        'modelo'            => escreverTexto($row['modelo']),
                        'ano_modelo'        => $row['ano_modelo'],
                        'tipo_veiculo'      => $row['tipo_veiculo']
                    ]);
                }
                $dados[] = $item;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * lista de transações
     * @param $id_mens
     * @return array
     * @throws Exception
     */
    public function listaTransacoes($id_mens) {
        try {
            $dados = [];
            $sql = "select *  from (
                select 'BOL' as tipo_pagamento, 'BOLETO' as descricao_pagamento, id_boleto as id, id_referencia as id_mens, tp_referencia as tipo, bol_valor as valor_transacao,
                null as numero_cartao, mundi_id as id_transacao, id_venda, 'Pendente' as status, bol_data_criacao as data_criacao
                from sf_boleto b inner join sf_vendas_planos_mensalidade pm on pm.id_mens = b.id_referencia	where b.inativo = 0 and b.id_venda is null
                union select 'DCC' as tipo_pagamento, 'PAGAMENTO RECORRENTE' as descricao_pagamento, id_transacao as id, id_plano_item as id_mens, 'M' as tipo, valor_transacao, 
                SUBSTRING(ltrim(dcc_numero_cartao),0,5)+'....'+SUBSTRING(rtrim(dcc_numero_cartao), len(rtrim(dcc_numero_cartao)) -3,4) as numero_cartao,
                transactionID as id_transacao, id_venda_transacao as id_venda, 
                (select case when vic.status_transacao = 'E' then 'Estornado' 
                when vic.status_transacao = 'A' then 'Aprovado'
                else 'Reprovado' end as status), transactionTimestamp as data_criacao
                from sf_vendas_itens_dcc vic inner join sf_vendas_planos_mensalidade pm on pm.id_mens = vic.id_plano_item where id_venda_transacao is null
                union select abreviacao as tipo_pagamento, tp.descricao as descricao_pagamento, vi.id_venda as id, id_mens, 'M' as tipo_referencia, vi.valor_total as valor_transacao,
                case when vic.id_transacao is not null then SUBSTRING(ltrim(dcc_numero_cartao),0,5)+'....'+SUBSTRING(rtrim(dcc_numero_cartao), len(rtrim(dcc_numero_cartao)) -3,4) else null end as numero_cartao,
                vic.transactionID as id_transacao, vi.id_venda, (select top 1 status from sf_vendas where id_venda = vi.id_venda) as status, dt_cadastro as data_criacao 
                from sf_vendas_itens vi
                inner join sf_vendas_planos_mensalidade pm on pm.id_item_venda_mens = vi.id_item_venda
                left join sf_vendas_itens_dcc vic on vic.id_plano_item = pm.id_mens and vic.id_venda_transacao = vi.id_venda
                inner join sf_venda_parcelas vp on vp.venda = vi.id_venda
                inner join sf_tipo_documento tp on tp.id_tipo_documento = vp.tipo_documento
            ) as transacoes
            where transacoes.id_mens = ".valoresSelect2($id_mens)." and transacoes.tipo = 'M' order by status asc";
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $dados[] = [
                    'tipo_pagamento'        => escreverTexto($row['tipo_pagamento']),
                    'descricao_pagamento'   => escreverTexto($row['descricao_pagamento']),
                    'valor_transacao'       => escreverNumero($row['valor_transacao'], 1),
                    'id_transacao'          => $row['id'],
                    'numero_cartao'         => $row['numero_cartao'],
                    'status'                => $row['status'],
                    'data_criacao'          => escreverDataHora($row['data_criacao']),
                    'bandeira'              => verificar_bandeira($row['numero_cartao'])
                ];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param $usuario_id
     * @param $tipo_veiculo
     * @return string
     */
    private function selectPlanos($usuario_id, $tipo_veiculo) {
        $modulos = getModulos();
        $whereVeiculo = $modulos['seg'] ? " and tipo_veiculo = ".$tipo_veiculo : "";
        $sql = "select id_plano, dt_inicio, dt_fim, dt_cadastro, p.descricao, planos_status,
            id_veiculo, placa, marca, modelo, ano_modelo, mensalidades.tipo_pagamento,
            mensalidades.valor_plano, ve.tipo_veiculo from sf_vendas_planos vp
            cross apply (select top 1 case when (pp.parcela = 0) then 'DCC'
            else 'BOLETO' end as tipo_pagamento, 
             dbo.VALOR_REAL_MENSALIDADE(id_mens) as valor_plano
            from sf_vendas_planos_mensalidade pm
            inner join sf_produtos_parcelas pp on pp.id_parcela = pm.id_parc_prod_mens
            where id_plano_mens = vp.id_plano order by dt_inicio_mens desc) as mensalidades
            inner join sf_produtos p on p.conta_produto = vp.id_prod_plano
            left join sf_fornecedores_despesas_veiculo ve on ve.id = vp.id_veiculo
            where favorecido = ".valoresSelect2($usuario_id).$whereVeiculo.";";
        return $sql;
    }

    /**
     * Verifica o Link
     * @param string $condicao
     * @return arry $dados
     * @throws Exception
     */
    public function verificaLink($condicao) {
        try {
            $sql = "select top 1 id_mens, dt_inicio_mens, data_parcela, valor,
            case when (vp.id_prim_mens = id_mens and vp.taxa_adesao > 0 and vp.adesao_soma = 0) then vp.taxa_adesao
            when (vp.id_prim_mens = id_mens and vp.taxa_adesao > 0) then dbo.VALOR_REAL_MENSALIDADE(id_mens) + vp.taxa_adesao
            else dbo.VALOR_REAL_MENSALIDADE(id_mens) end as valor_mens, id_link,
            cast(case when (vp.id_prim_mens = id_mens) then dateadd(day, 3, dt_inicio_mens) 
            when (cast(dt_inicio_mens as date) < cast(getdate() as date)) then getdate() 
            else dt_inicio_mens end as date) dt_vencimento, id_plano, id_prim_mens
            from sf_vendas_planos_mensalidade pm inner join(select vp.*, p.adesao_soma,
            (SELECT TOP 1 id_mens FROM sf_vendas_planos_mensalidade where id_plano_mens = vp.id_plano order by dt_inicio_mens asc) as id_prim_mens,
            isnull((SELECT TOP 1 s.preco_venda FROM sf_produtos s WHERE s.tipo = 'S' AND p.conta_produto_adesao = s.conta_produto), 0) as taxa_adesao
            from sf_vendas_planos vp inner join sf_produtos p on p.conta_produto = vp.id_prod_plano) as vp on id_plano_mens = id_plano
            inner join sf_produtos_parcelas pp on pp.id_parcela = pm.id_parc_prod_mens
            left join sf_link_online on id_pessoa = favorecido and inativo = 0 and id_link in (select id_link from sf_link_online_itens where id_mens = pm.id_mens)
            where ".$condicao;
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $row['dt_inicio_mens'] = escreverData($row['dt_inicio_mens']);
                $row['data_parcela'] = escreverDataHora($row['data_parcela']);
                $row['valor'] = escreverNumero($row['valor']);
                $row['valor_mens'] = escreverNumero($row['valor_mens']);
                $row['dt_vencimento'] = escreverData($row['dt_vencimento']);
                $dados = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }    
    
    public function inativaLink($condicao) {
        try {
            $queryInativa = "update sf_link_online set inativo = 1 where ".$condicao;
            Conexao::conect($queryInativa);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    public function inserirLink($data, $parcela) {        
        try {
            $config = $this->getInfoLinkPadrao($data['id_mens']);
            $query = "set dateformat dmy;
            insert into sf_link_online (id_pessoa, data_criacao, syslogin, tipo_documento, data_parcela, max_parcela, inativo)
            values (" . valoresNumericos2($data['usuario_id']) . ", getdate(), " . valoresTexto2(getLoginUser()) . "," . 
            valoresSelect2($config['link_forma_pagamento']) . ", cast(getdate() as date)," . valoresNumericos2($config['link_max_parcelas']) . ",2);
            SELECT SCOPE_IDENTITY() ID;";                
            $result = Conexao::conect($query);
            odbc_next_result($result);
            $link_id = odbc_result($result, 1);            
                        
            if ($config["id_mens"] == $config["id_prim_mens"] && is_numeric($config["conta_produto_adesao"])) {
                $queryItem = "insert into sf_link_online_itens (id_link, id_mens, id_produto, quantidade, valor_total, valor_bruto, valor_multa)
                values (" . $link_id . ", null, " . valoresSelect2($config["conta_produto_adesao"]) . ", 1.00," . 
                $config["preco_adesao_bruto"] . "," . $config["preco_adesao"] . ", 0.00);";
                Conexao::conect($queryItem);
            }
            
            $queryItem = "insert into sf_link_online_itens (id_link, id_mens, id_produto, quantidade, valor_total, valor_bruto, valor_multa)
            values (" . $link_id . "," . valoresSelect2($config["id_mens"]) . "," . valoresSelect2($config["conta_produto"]) . ", 1.00," . 
            $config["valor_mens_bruto"] . "," . $config["valor_mens"] . ", 0.00);";
            Conexao::conect($queryItem);
            
            $queryUpdate = "update sf_link_online set valor = (select sum(valor_bruto) from sf_link_online_itens bb where bb.id_link = sf_link_online.id_link),
            data_parcela = (SELECT cast(case when min(dt_inicio_mens) > dateadd(day,2, getdate()) then min(dt_inicio_mens) else dateadd(day,2, getdate()) end as date) bol_vencimento 
            FROM sf_link_online_itens bb inner join sf_vendas_planos_mensalidade mm on bb.id_mens = mm.id_mens where bb.id_link = sf_link_online.id_link), inativo = 0 
            where id_link = " . $link_id;
            Conexao::conect($queryUpdate);
            
            $query_log = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
            select 'sf_link_online', id_link, " . valoresTexto2(getLoginUser()) . ", 'I',
            'CADASTRO LINK: ' + cast(id_link as varchar), GETDATE(), id_pessoa from sf_link_online where id_link = " . $link_id;
            Conexao::conect($query_log);
            
            return $link_id;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }    

    /**
     * Verifica o boleto
     * @param string $condicao
     * @return arry $dados
     * @throws Exception
     */
    public function verificaBoleto($condicao) {
        try {
            $sql = "select top 1 id_mens, dt_inicio_mens, bol_data_parcela, bol_valor,
            case when (vp.id_prim_mens = id_mens and vp.taxa_adesao > 0 and vp.adesao_soma = 0) then vp.taxa_adesao
            when (vp.id_prim_mens = id_mens and vp.taxa_adesao > 0) then dbo.VALOR_REAL_MENSALIDADE(id_mens) + vp.taxa_adesao
            else dbo.VALOR_REAL_MENSALIDADE(id_mens) end as valor_mens, id_boleto,
            cast(case when (vp.id_prim_mens = id_mens) then dateadd(day, 3, dt_inicio_mens) 
            when (cast(dt_inicio_mens as date) < cast(getdate() as date)) then getdate() 
            else dt_inicio_mens end as date) dt_vencimento, id_plano, id_prim_mens
            from sf_vendas_planos_mensalidade pm inner join(select vp.*, p.adesao_soma,
            (SELECT TOP 1 id_mens FROM sf_vendas_planos_mensalidade where id_plano_mens = vp.id_plano order by dt_inicio_mens asc) as id_prim_mens,
            isnull((SELECT TOP 1 s.preco_venda FROM sf_produtos s WHERE s.tipo = 'S' AND p.conta_produto_adesao = s.conta_produto), 0) as taxa_adesao
            from sf_vendas_planos vp inner join sf_produtos p on p.conta_produto = vp.id_prod_plano) as vp on id_plano_mens = id_plano
            inner join sf_produtos_parcelas pp on pp.id_parcela = pm.id_parc_prod_mens
            left join sf_boleto on sf_boleto.id_referencia = pm.id_mens and tp_referencia = 'M' and inativo = 0            
            where ".$condicao;
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $row['dt_inicio_mens'] = escreverData($row['dt_inicio_mens']);
                $row['bol_data_parcela'] = escreverData($row['bol_data_parcela']);
                $row['bol_valor'] = escreverNumero($row['bol_valor']);
                $row['valor_mens'] = escreverNumero($row['valor_mens']);
                $row['dt_vencimento'] = escreverData($row['dt_vencimento']);
                $dados = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function inativaBoleto($condicao) {
        try {
            $queryInativa = "update sf_boleto set inativo = 1 where ".$condicao;
            Conexao::conect($queryInativa);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function inserirBoleto($data, $parcela) {
        try {
            $banco = $this->getInfoBancoPadrao();
            $produto = $parcela["produto"];
            $query = "set dateformat dmy; INSERT INTO sf_boleto (id_referencia,tp_referencia,bol_id_banco,carteira_id,bol_data_criacao,bol_data_parcela,bol_valor,bol_juros,
            bol_multa,bol_nosso_numero,bol_descricao,bol_desconto,tipo_documento,sa_descricao,exp_remessa,inativo,syslogin)
            VALUES (" . valoresSelect2($data['id_mens']) . ",'M', " . valoresSelect2($banco['id_bancos']) . ", " . valoresSelect2($banco['sf_carteiras_id']) .
            ", getdate() , dateadd(day,2,". valoresData2($data['dt_inicio_mens']). "), " .
            valoresNumericos2(($produto["adesao_soma"] == 0 ? $data['valor_adesao'] : $data['valor_mens'])) . ", 0.00, 0.00, " .
            $this->getNossoNumero($banco['id_bancos']) . ",'',0.00,10,'',0,0," . valoresTexto2(getLoginUser()) . ");SELECT SCOPE_IDENTITY() ID;";
            $result = Conexao::conect($query);
            odbc_next_result($result);
            $boleto_id = odbc_result($result, 1);
            return $boleto_id;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Pega o Nosso Número
     * @param int $banco
     * @return string $nossoNumero
     * @throws Exception
     */
    public function getNossoNumero($banco) {
        try {
            $nossoNumero = "1";
            $cur = Conexao::conect("select isnull(MAX(x.bol_nosso_numero),0) vm, isnull(nosso_numero,0) nn from (
                        select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_boleto p on p.bol_id_banco = b.id_bancos
                        where id_bancos = '" . $banco . "' union all
                        select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_solicitacao_autorizacao_parcelas p on p.bol_id_banco = b.id_bancos
                        where id_bancos = '" . $banco . "' union all
                        select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_venda_parcelas p on p.bol_id_banco = b.id_bancos
                        where id_bancos = '" . $banco . "' union all
                        select bol_nosso_numero,nosso_numero from sf_bancos b left join sf_lancamento_movimento_parcelas l on l.bol_id_banco = b.id_bancos
                        where id_bancos = '" . $banco . "') as x group by x.nosso_numero");
            while ($RFP = odbc_fetch_array($cur)) {
                if ($RFP['nn'] > $RFP['vm']) {
                    $nossoNumero = $RFP['nn'];
                } else {
                    $nossoNumero = bcadd($RFP['vm'], 1);
                }
            }
            return $nossoNumero;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Pega Info do Banco Padrao
     * @return array $dados
     * @throws Exception 
     */
    public function getInfoBancoPadrao() {
        try {
            $dados = [];
            $sql_banco = "select top 1 id_bancos, sf_carteiras_id from sf_bancos b inner join sf_bancos_carteiras bc on bc.sf_carteiras_bancos = b.id_bancos
                    where sf_carteiras_id = (select top 1 bol_cart_padrao from sf_configuracao);";
            $res = Conexao::conect($sql_banco);
            while($row = odbc_fetch_array($res)) {
                $dados = $row;
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Pega Info do Link Padrao
     * @return array $dados
     * @throws Exception 
     */
    public function getInfoLinkPadrao($id_mens) {
        try {
            $dados = [];
            $sql_banco = "select id_mens, (select top 1 id_mens from sf_vendas_planos_mensalidade where id_plano_mens = pl.id_plano order by dt_inicio_mens asc) as id_prim_mens,
            p.conta_produto, dbo.VALOR_REAL_MENSALIDADE(id_mens) valor_mens, pp.valor_unico + dbo.FU_VALOR_ACESSORIOS(id_plano) valor_mens_bruto,
            adesao_soma, conta_produto_adesao, ((select preco_venda from sf_produtos where conta_produto = p.conta_produto_adesao) - 
            isnull((select case when tp_valor = 'D' then valor else (((select preco_venda from sf_produtos where conta_produto = p.conta_produto_adesao) * valor)/100)
            end from sf_fornecedores_despesas_convenios fc
            inner join sf_convenios_planos cp on fc.convenio = cp.id_convenio_planos
            inner join sf_convenios_regras cr on fc.convenio = cr.convenio
            where id_fornecedor = pl.favorecido and id_prod_convenio = p.conta_produto_adesao
            and getdate() between dt_inicio and dt_fim),0)) preco_adesao,
            (select preco_venda from sf_produtos where conta_produto = p.conta_produto_adesao) preco_adesao_bruto,
            (select link_forma_pagamento from sf_configuracao) link_forma_pagamento,
            (select link_max_parcelas from sf_configuracao) link_max_parcelas
            from sf_vendas_planos_mensalidade m
            inner join sf_vendas_planos pl on m.id_plano_mens = pl.id_plano
            inner join sf_produtos p on p.conta_produto = pl.id_prod_plano
            inner join sf_produtos_parcelas pp on pp.id_parcela = m.id_parc_prod_mens
            where id_mens = " . $id_mens;
            $res = Conexao::conect($sql_banco);
            while($row = odbc_fetch_array($res)) {               
                $dados = $row;
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Query Base
     * @return string
     */
    private function queryBase() {
        return "select distinct transacoes.*, case when adesao_soma = 0 and id_prim_mens = id_mens and dependentes.adesao_dependente is null 
        then taxa_adesao else  (dbo.VALOR_REAL_MENSALIDADE(id_mens) + (case when dependentes.adesao_dependente is not null and valor_mens > 0
        and dependentes.acrescimo > 0 and id_prim_mens = id_mens and taxa_adesao is not null 
        then dependentes.dependentes * taxa_adesao when valor_mens > 0 and id_prim_mens = id_mens and taxa_adesao
        is not null then taxa_adesao else 0 end)) end as valor_pagar
        FROM (select distinct pm.*, configuracao.*, p.conta_produto,  p.adesao_soma, p.conta_produto_adesao, vp.id_veiculo, 
        isnull(b.exp_remessa, 1) exp_remessa,
        DATEDIFF(day,(select case when pm.parcela = 0 or pm.id_prim_mens = pm.id_mens then pm.dt_inicio_mens 
        else DATEADD(day,-configuracao.dcc_dias_boleto, pm.dt_inicio_mens) end as data_prazo), pm.prev_mens) as prorata,
        (select top 1 id_parcela from sf_produtos_parcelas where id_produto = p.conta_produto and (parcela = -1 or parcela > 0) order by parcela asc) as id_parcela_boleto,
        (select top 1 id_parcela from sf_produtos_parcelas where id_produto = p.conta_produto and (parcela = 0 or parcela > 0) order by parcela asc) as id_parcela_cartao,
        case when pm.parcela = 0 or pm.id_prim_mens = pm.id_mens then pm.dt_inicio_mens else DATEADD(day,-configuracao.dcc_dias_boleto, pm.dt_inicio_mens) end as data_prazo,
        (SELECT TOP 1 s.preco_venda FROM sf_produtos s WHERE s.tipo = 'S' AND s.conta_produto = p.conta_produto_adesao) as taxa_adesao,
        case when (td.id_tipo_documento is null and pm.parcela = 0) then 'DCC' 
        when (td.id_tipo_documento is null and pm.parcela = 12) then 'ANUAL' 
        when (td.id_tipo_documento is null and pm.parcela = -1) then 'BOL' 
        when (td.id_tipo_documento is null and pm.parcela = -2) then 'PIX' 
        when (td.id_tipo_documento is null and pm.parcela > 0) then 'MENSAL'
        else td.abreviacao end as tipo_pagamento, 
        case when (td.id_tipo_documento is null and pm.parcela = 0) then 'DCC' 
        when (td.id_tipo_documento is null and pm.parcela = 12) then 'ANUAL' 
        when (td.id_tipo_documento is null and pm.parcela = -1) then 'BOLETO' 
        when (td.id_tipo_documento is null and pm.parcela = -2) then 'PIX' 
        when (td.id_tipo_documento is null and pm.parcela > 0) then 'MENSAL'
        else td.descricao end as descricao_pagamento,
        case when vic.id_transacao is not null then SUBSTRING(ltrim(dcc_numero_cartao),0,5)+'....'+SUBSTRING(rtrim(dcc_numero_cartao), len(rtrim(dcc_numero_cartao)) -3,4) 
        when pm.parcela = 0 then (select top 1 legenda_cartao from sf_fornecedores_despesas_cartao where id_fornecedores_despesas = vp.favorecido) else null end as numero_cartao, 
        case 
        when (vi.id_venda is null and bb.id_boleto is not null) then bb.bol_data_criacao        
        when (vi.id_venda is null and b.id_boleto is not null) then b.bol_data_criacao 
        when (vi.id_venda is null and vic.id_transacao is not null) then vic.transactionTimestamp 
        when (vi.id_venda is null and b.id_boleto is null and vic.id_transacao is null) then pm.dt_inicio_mens 
        else vpc.data_cadastro end as data_transacao, 
        case when v.id_venda is null and dt_inicio_mens < cast(getdate() as date) then 'Vencido' 
        when v.id_venda is null and dt_inicio_mens >= cast(getdate() as date) then 'Pendente' 
        else status end as status,
        case when dt_pagamento_mens is null then 0 else 1 end as ord_pag,
        isnull(bb.id_boleto, b.id_boleto) id_boleto,
        isnull(bb.bol_valor, b.bol_valor) bol_valor,
        b.bol_descricao, bb.bol_link,
        vic.valor_transacao, vpc.valor_parcela, vp.favorecido, vi.id_venda, 
        (select sum(valor_total) from sf_vendas_itens where id_venda = vi.id_venda) as valor_total 
        from (select pm.*, pp.*,
        (SELECT TOP 1 id_mens FROM sf_vendas_planos_mensalidade where id_plano_mens = pm.id_plano_mens order by dt_inicio_mens asc) as id_prim_mens, 
        isnull((select max(dt_inicio_mens) from sf_vendas_planos_mensalidade where id_plano_mens = pm.id_plano_mens and id_mens < pm.id_mens), dateadd(month, -(select case when pp.parcela < 1 then 1 else pp.parcela end), dt_inicio_mens)) as prev_mens
        from sf_vendas_planos_mensalidade pm 
        inner join sf_produtos_parcelas pp on pp.id_parcela = pm.id_parc_prod_mens) as pm
        inner join sf_vendas_planos vp on vp.id_plano = pm.id_plano_mens
        cross apply (select top 1 aca_dias_tolerancia_status, ACA_DIA_VENCIMENTO, ACA_TIPO_VENCIMENTO, aca_adesao_dep, dcc_dias_boleto from sf_configuracao) as configuracao 
        left join sf_vendas_itens vi on vi.id_item_venda = pm.id_item_venda_mens 
        left join sf_boleto b on b.id_referencia = pm.id_mens and b.tp_referencia = 'M' and b.inativo = 0 
        left join sf_boleto_online_itens bbi on bbi.id_mens = pm.id_mens left join sf_boleto_online bb on bb.id_boleto = bbi.id_boleto and bb.inativo = 0
        outer apply (select top 1 * from sf_vendas_itens_dcc where id_plano_item = pm.id_mens order by id_transacao desc) as vic 
        left join sf_vendas v on v.id_venda = vi.id_venda 
        left join sf_venda_parcelas vpc on vpc.venda = vi.id_venda 
        left join sf_tipo_documento td on td.id_tipo_documento = v.tipo_documento 
        inner join sf_produtos p on p.conta_produto = vi.produto or p.conta_produto = pm.id_produto ) as transacoes 
        left join sf_fornecedores_despesas_veiculo ve on ve.id = transacoes.id_veiculo
        outer apply (select top 1 cupom_meses as convenios_meses_site, id_convenio, valor, tp_valor, co.cupom, co.descricao as convenio_descricao,
        isnull((select top 1 id_prod_convenio from sf_convenios_planos where id_convenio_planos = transacoes.conta_produto and id_prod_convenio = p.conta_produto_adesao), 0) as desconto_adesao 
        from sf_fornecedores_despesas_convenios fdc inner join sf_convenios co on co.id_convenio = fdc.convenio
        inner join sf_convenios_regras cr on cr.convenio = co.id_convenio left join sf_produtos p on co.id_convenio = p.convenios_site
        where ((co.convenio_especifico = 1 and (conta_produto = transacoes.conta_produto or co.id_convenio in (select id_convenio_planos from sf_convenios_planos
        where id_prod_convenio = transacoes.conta_produto))) or co.convenio_especifico = 0) and co.ativo = 0) as convenio 
        outer apply (select sum(valor * dependentes) as acrescimo,  novo, dependentes, valor as valor_dependente, 
        sum((case when valor > 0 and config_adesao = 1 and (convenio_desconto = 0 or convenio_desconto is null) then taxa_adesao 
        when valor > 0 and config_adesao = 1 and convenio_desconto = 1 and tipo_convenio = 'D' then taxa_adesao - convenio_valor  
        when valor > 0 and config_adesao = 1 and convenio_desconto = 1 and tipo_convenio = 'P' then  taxa_adesao - (valor * convenio_valor) / 100 else 0 end) * novo) as adesao_dependente
        from (select valor, taxa_adesao, count(case when cast(data_inclusao as date) <= x.dt_prazo then id_dependente else null end) as dependentes, config_adesao, convenio_desconto, tipo_convenio, convenio_valor,
        count(case when (cast(data_inclusao as date) <= x.dt_prazo and cast(data_inclusao as date) > DATEADD(day, x.prorata, x.dt_prazo)) then id_dependente else null end) as novo 
        from (select id, id_dependente, valor, data_inclusao, transacoes.data_prazo as dt_prazo, transacoes.prorata as prorata, transacoes.taxa_adesao as taxa_adesao, 
        convenio.tp_valor as tipo_convenio, convenio.valor as convenio_valor, convenio.desconto_adesao as convenio_desconto, transacoes.aca_adesao_dep as config_adesao
        from sf_vendas_planos inner join sf_fornecedores_despesas_dependentes fdd on fdd.id_titular = favorecido
        inner join sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = fdd.id_dependente and fd.inativo = 0
        left join sf_produtos_acrescimo on id_produto = id_prod_plano and ((datediff(year,data_nascimento,getdate()) between idade_min and idade_max_m and sexo = 'M') or
        (datediff(year,data_nascimento,getdate()) between idade_min and idade_max_f and sexo = 'F')) and (id_parentesco is null or id_parentesco = parentesco)
        where id_plano = transacoes.id_plano_mens) as x group by valor, taxa_adesao, config_adesao, convenio_desconto, tipo_convenio, convenio_valor) as y group by valor, dependentes, novo) as dependentes";
    }
}