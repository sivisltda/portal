<?php

class WhatsAppRepository {

  public function insert($dados) {
    try {
      $sql = "insert into sf_whatsapp(telefone, mensagem, data_envio, 
      fornecedor_despesas, sys_login, status) values(
        ".valoresTexto2($dados['telefone']).", ".valoresTexto2($dados['mensagem']).",
        getdate(), ".valoresSelect2($dados['id_usuario'])."," . valoresTexto2(getLoginUser()) . ", 0);
        SELECT SCOPE_IDENTITY() id;";
        $result = Conexao::conect($sql);
        odbc_next_result($result);
        $id = odbc_result($result, 1);
        if(!is_numeric($id)) {
            throw new Exception("Erro ao inserir o mensagem pra whataspp => ".$sql);
        }
        return $id;
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  public function getModelo($condicao) {
    try {
      $sql = "select top 1 * from sf_emails where tipo = 2 and $condicao";
      $res = Conexao::conect($sql);
      $dados = [];
      while($row = odbc_fetch_array($res)) {
        $row['mensagem'] = escreverTexto($row['mensagem']);
        $dados = $row;
      }
      return $dados;
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }
}