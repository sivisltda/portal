<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 14/09/2020
 * Time: 13:51
 */

class ProdutoRepository {

    /**
     * Busca o Produto
     * @param $condicao
     * @return array $plan
     * @throws Exception
     */
    public function getProduto($condicao, $valorFipe) {
        try {
            $sql = "SELECT TOP 1 ".$this->getSqlBase()." AND ".$condicao;
            $query = Conexao::conect($sql);
            $data = [];
            while($rows = odbc_fetch_array($query)) {
                $data = $this->getResponse($rows, $valorFipe);
            }
            return $data;
        }catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Lista os planos
     * @return array $plans
     * @throws Exception
     */
    public function listaProdutos($condicao, $valorFipe) {
        try {
            $sql = "SELECT ".$this->getSqlBase()." AND ".$condicao;
            $rs = Conexao::conect($sql);
            $data = [];
            while($rows = odbc_fetch_array($rs)) {
                $data[] = $this->getResponse($rows, $valorFipe);
            }
            return $data;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getListaProdutosChunck($condicao) {
        try {
            $dados = [];
            $sql = "select * from sf_produtos where ".$condicao;
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados[] = [
                    'id' => $row['conta_produto'],
                    'nome' => escreverTexto($row['descricao'])
                ];
            }
            return $dados;

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getIdsMateriaPrimaParcela($condicao) {
        try {
            $dados = [];
            $sql = "select distinct id_materiaprima from sf_produtos_materiaprima
            inner join sf_produtos on sf_produtos.conta_produto = id_materiaprima where tipo = 'A' 
            and id_produtomp in (".$condicao.")";
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados[] = $row['id_materiaprima'];
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function getListaTipoPagamento($condicao) {
        try {
            $sql = "select distinct parcela as id, 
            case when parcela = -1 then 'Boleto Recorrente'
            when parcela = -2 then 'Pix'
            when parcela = 0 then 'DCC Recorrente'
            when parcela = 1 then 'Boleto'
            when parcela > 1 then 'Mensal'
            else 'Outro' end as descricao from sf_produtos_parcelas pp
            inner join sf_produtos p on p.conta_produto = pp.id_produto
            where ".$condicao;
            $res = Conexao::conect($sql);
            $dados = [];
            while($row = odbc_fetch_array($res)) {
                $row['descricao'] = escreverTexto($row['descricao']);
                $dados[] = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Lista de Produtos Parcelas
     * @param string $condicao
     * @throws Exception
     * @return array $dados
     */
    public function getListaProdutosParcelas($condicao) {
        try {
            $dados = [];
            $sql = "SELECT *, CONVERT(DECIMAL(10,2), (valor - (valor * valor_desconto / 100))) as valor_final,
            CONVERT(DECIMAL(10,2), (valor * valor_desconto / 100)) as desconto
            FROM (select pp.*, (case when pp.parcela = -1 then 1 when pp.parcela = 1 then 2 when pp.parcela = -2 then 3 when pp.parcela = 0 then 4 else 5 end) as ord_parcela, 
            case when pp.valor_unico = 0 then p.preco_venda * (case when pp.parcela < 1 then 1 else pp.parcela end) else pp.valor_unico end as valor,
            case when pp.valor_unico = 0 then 1 else 0 end as valor_zerado  
            from sf_produtos_parcelas pp inner join sf_produtos p on p.conta_produto = pp.id_produto WHERE parcela <= 1 ".$condicao.") as x order by ord_parcela asc";
            $rs = Conexao::conect($sql);
            $isBolRec = false;
            while ($row = odbc_fetch_array($rs)) {
                if (!$isBolRec) $isBolRec = $row['parcela'] == -1;
                if (!($isBolRec && $row['parcela'] == 1)) {
                    $dados[] =  [
                       'valor_unico'        => escreverNumero($row['valor_final'], 1),
                       'valor_desconto'     => escreverNumero($row['desconto'], 1),
                       'desconto'           => escreverNumero($row['valor_desconto']),
                       'valor_comissao'     => escreverNumero($row['valor_comissao'], 1),
                       'valor_zerado'       => intval($row['valor_zerado']),
                       'parcela'            => $row['parcela'],
                       'id_parcela'         => $row['id_parcela']
                    ];
                }
            }
           return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Pega o Produto Parcela
     * @param string $condicao
     * @throws Exception
     * @return array $dados
     */
    public function getProdutoParcela($condicao) {
        try {
            $sql = "select top 1 * from (SELECT (case when pp.parcela = -1 then 1 when pp.parcela = 1 then 2 when pp.parcela = 0 then 3 else 4 end) as ord_parcela, 
            id_parcela, CONVERT(DECIMAL(10,2), (pp.valor_unico - (pp.valor_unico * pp.valor_desconto / 100))) as valor_unico,
            p.parcelar gerar_parc, adesao_soma, parcela,
            isnull((SELECT TOP 1 s.preco_venda FROM sf_produtos s WHERE s.tipo = 'S' AND p.conta_produto_adesao = s.conta_produto), 0) as taxa_adesao
            FROM sf_produtos_parcelas pp INNER JOIN sf_produtos p on p.conta_produto = pp.id_produto
            where ".$condicao.") as x order by ord_parcela asc;";
            //echo $sql; exit;
            $cur = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($cur)) {
                $dados['parcela_id']        = $row["id_parcela"];
                $dados['valor_unico']       = escreverNumero($row["valor_unico"], 1);
                $dados['valor_taxa_adesao'] = escreverNumero($row['taxa_adesao'], 1);
                $dados['adesao_soma']       = $row["adesao_soma"];
                $dados['parcela']           = $row['parcela'];
                $dados['num_parcela']       = $row['parcela'] > 0 ? $row['parcela'] : 1;
                $dados['gerar_parc']        = intval($row['gerar_parc']);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Lista de Grupos de Produtos
     * @param $condicao
     * @return array
     * @throws Exception
     */
    public function listaGruposProdutos($condicao) {
        try {
            $sql = "SELECT * FROM sf_contas_movimento WHERE ".$condicao;
            $rs = Conexao::conect($sql);
            $data = [];
            while($rows = odbc_fetch_array($rs)) {
                $data[] = [
                    'id'            => $rows['id_contas_movimento'],
                    'nome'          => escreverTexto($rows['descricao']),
                    'tipo'          => $rows['tipo'],
                    'acessorios'    => $rows['acessorios'],
                    'pes_jur'       => $rows['pes_jur'],
                    'agrupa_item'   => $rows['direcao_texto']
                ];
            }
            return $data;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Pega a lista de produtos seguro
     * @param $data
     * @return array lista
     * @throws Exception
     */
    public function getProdutosSeguro($data, $where) {
        try {
            $dados = [];
            $sql = "select conta_produto, p.descricao, p.descricao_site, favorito, cota_defaut, cota_defaut_tp,
            p.mensagem_site, preco_venda, convenio.id_convenio, adesao_soma,
            convenio.descricao as desc_convenio, convenio.valor as valor_convenio, p.convenios_meses_site, parcelar_site,
            conta_movimento, cm.acessorios as editavel, (select top 1 seg_piso_franquia from sf_configuracao where id = 1) as seg_piso_franquia,
            (select top 1 preco_venda from sf_produtos where tipo = 'S' and conta_produto = p.conta_produto_adesao) as taxa_adesao,
            STUFF((SELECT distinct ',' + cast(id_materiaprima as varchar) FROM sf_produtos_materiaprima 
            WHERE id_produtomp = conta_produto FOR XML PATH('')), 1, 1, '') as servicos 
            from sf_produtos p 
            left join (select * from sf_convenios co left join sf_convenios_regras cr on co.id_convenio = cr.convenio
            where tp_valor = 'P') convenio on convenio.id_convenio = p.convenios_site
            left join sf_contas_movimento cm on p.conta_movimento = cm.id_contas_movimento and cm.tipo = 'L'
            where p.tipo = 'C' and p.inativa = 0 and conta_produto > 0  and mostrar_site = 1 
            and (conta_produto in (select id_produto from sf_produtos_veiculo_tipos where id_tipo = " . valoresSelect2($data['tipo_veiculo']) . ") or
            conta_produto not in (select id_produto from sf_produtos_veiculo_tipos))" . $where. " and (((select count(id) from sf_veiculo_grupo inner join sf_veiculo_grupo_fabricante on id = id_grupo where inativo = 0 and id_marca = " . valoresNumericos2($data['marca']) . ") = 0 and grupo_fabricantes is null) or 
            grupo_fabricantes in (select id from sf_veiculo_grupo inner join sf_veiculo_grupo_fabricante on id = id_grupo where inativo = 0 and id_marca = " . valoresNumericos2($data['marca']) . "))
            and (datepart(Year, getdate()) - ".$data['ano_modelo'].") < case when p.ano_piso is not null then p.ano_piso 
            else (select top 1 seg_ano_veiculo_piso from sf_configuracao) end and " . valoresNumericos2($data['valor']) . "between segMin and segMax 
            ORDER BY p.tempo_producao asc, preco_venda;";
            //echo $sql; exit;
            $result = Conexao::conect($sql);
            while ($rows = odbc_fetch_array($result)) {
                $row = [];
                $row["id"]                  = escreverTexto($rows["conta_produto"]);
                $row["nome"]                = escreverTexto($rows["descricao"]);
                $row["descricao"]           = escreverTexto($rows['descricao_site']);
                $row["mensagem"]            = escreverTexto($rows['mensagem_site']);
                $row["id_grupo"]            = $rows['conta_movimento'];
                $row["recomendado"]         = boolval($rows["favorito"]);
                $row["adesao_soma"]         = boolval($rows["adesao_soma"]);
                $row["valor_mensal"]        = escreverNumero($rows["preco_venda"], 1);
                $row["servicos"]            = explode(",", $rows["servicos"]);
                $row["taxa_adesao"]         = escreverNumero($rows['taxa_adesao'], 1);
                $row["desc_convenio"]       = escreverTexto($rows['desc_convenio']);
                $row['valor_convenio']      = escreverNumero($rows['valor_convenio'], 1);
                $row['piso_franquia']       = escreverNumero($rows['seg_piso_franquia']);
                $row['validade_convenio']   = $rows['convenios_meses_site'];
                $row['cota_defaut_prod']    = $rows['cota_defaut'];
                $row['cota_defaut_prod_tp'] = $rows['cota_defaut_tp'];
                $row['editavel']            = boolval($rows['editavel']);
                $row['parcelar_site']       = boolval($rows['parcelar_site']);
                $row['parcelas']            = $this->getListaProdutosParcelas('and id_produto = '.valoresSelect2($rows['conta_produto']));
                $row['acessorios']          = $this->getServicosSeguro('p1.conta_produto = '.valoresSelect2($rows['conta_produto']), $data['valor']);
                $dados[] = $row;
            }
           
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getServicosSeguro($where, $valorFipe) {
        try {
            $dados = [];
            $sql = "select * from (select distinct p2.conta_produto, p2.descricao, 
            case when p2.tp_preco = 1 then (p1.segMax * p2.preco_venda)/100 
                 when p2.tp_preco = 2 then (".valoresNumericos2($valorFipe)." * p2.preco_venda) / 100
                 else p2.preco_venda end as preco_venda,
            p2.desc_prod, cm.id_contas_movimento as cobertura,
            case when p2.tipo = 'A' then 1 else 0 end padrao, nivel
            from sf_produtos_materiaprima mp
            inner join sf_produtos p1 on p1.conta_produto = mp.id_produtomp and p1.inativa = 0 and p1.conta_produto > 0 and p1.tipo = 'C'
            inner join sf_produtos p2 on p2.conta_produto = mp.id_materiaprima and p2.inativa = 0 and p2.conta_produto > 0 and p2.tipo in ('S', 'A')
            left join sf_contas_movimento cm on cm.id_contas_movimento = p2.conta_movimento and p2.tipo = 'A'
            where ".$where." ) as x order by nivel asc"; 
            //echo $sql; exit;
            $result = Conexao::conect($sql);
            while ($rows = odbc_fetch_array($result)) {
                $row = [];
                $row["id"]          = escreverTexto($rows["conta_produto"]);
                $row["nome"]        = escreverTexto($rows["descricao"]);
                $row["valor"]       = escreverNumero($rows["preco_venda"], 1);
                $row["descricao"]   = strip_tags($rows["desc_prod"]);
                $row["cobertura"]   = $rows['cobertura'];
                $row['padrao']      = intval($rows['padrao']);
                $dados[] = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getServicosCobertura($condicao) {
        $sql = "SELECT conta_produto, descricao FROM sf_produtos_materiaprima pp
        INNER JOIN sf_produtos p on p.conta_produto = pp.id_materiaprima WHERE ".$condicao;
        $res = Conexao::conect($sql);
        $dados = [];
        while($row = odbc_fetch_array($res)) {
            $row['descricao'] = escreverTexto($row['descricao']);
            $dados[] = $row;
        }
        return $dados;
    }

    /**
     * Pega a lista de coberturas
     * @param string $where
     * @return array $lista
     * @throws Exception
     */
    public function getCoberturas($where) {
        try {
            $toReturn = [];
            $sql = "select id_contas_movimento, descricao, terminal, grupo_observacao, direcao_texto, so_padrao,
            STUFF((SELECT distinct ',' + cast(id_produto as varchar) FROM sf_produtos_grupos 
            WHERE id_grupo = c.id_contas_movimento FOR XML PATH('')), 1, 1, '') as dependencias 
            from sf_contas_movimento c
            inner join sf_contas_movimento_contas cmc on cmc.id_conta = c.id_contas_movimento
            where ".$where." order by nivel asc";
            //echo $sql;exit;
            $result = Conexao::conect($sql);
            while ($rows = odbc_fetch_array($result)) {
                $row = [];
                $row["id"]           = escreverTexto($rows["id_contas_movimento"]);
                $row["questao"]      = escreverTexto($rows["descricao"]);
                $row["descricao"]    = escreverTexto($rows["grupo_observacao"]);
                $row["tipo"]         = ($rows["terminal"] == "1" ? "multiple" : "single");
                $row['agrupar_item'] = intval($rows['direcao_texto']);
                $row['so_padrao']    = intval($rows['so_padrao']);
                $row['dependencias'] = $rows['dependencias'] ? explode(',', $rows['dependencias']) : [];
                $toReturn[] = $row;
            }
            return $toReturn;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Pega a lista de coberturas itens
     * @param $id
     * @return array $toReturn
     * @throws Exception
     */
    public function getCoberturasItens($produto_id, $id, $where, $valorFipe) {
        try {
            $toReturn = [];
            $sql = "select p.*,so_padrao, 
                case when p.tp_preco = 1 then (p2.segMax * p.preco_venda) / 100
                     when p.tp_preco = 2 then (".valoresNumericos2($valorFipe)." * p.preco_venda) / 100
                else p.preco_venda end as preco_item,
            case when p2.id_materiaprima is not null then 1 else 0 end as padrao
            from sf_produtos p 
            inner join sf_contas_movimento cm on cm.id_contas_movimento = p.conta_movimento
            cross apply (select p3.segMax, pm.id_materiaprima from sf_produtos p3
            left join sf_produtos_materiaprima pm on pm.id_produtomp = p3.conta_produto and pm.id_materiaprima = p.conta_produto
            where p3.tipo = 'C' and p3.conta_produto = ".valoresSelect2($produto_id).") as p2
            where ".$where." and p.conta_movimento = " . valoresNumericos2($id)." order by p.tempo_producao asc";
            //echo $sql; exit;                 
            $result = Conexao::conect($sql);
            while ($rows = odbc_fetch_array($result)) {
                $row = [];
                $row["id"]              = escreverTexto($rows["conta_produto"]);
                $row["nome"]            = escreverTexto($rows["descricao"]);
                $row["valor"]           = escreverNumero($rows["preco_item"], 1);
                $row["descricao"]       = strip_tags($rows["desc_prod"]);
                $row["padrao"]          = intval($rows['padrao']);
                $row['so_padrao']       = intval($rows['so_padrao']);
                $toReturn[] = $row;
            }
            return $toReturn;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Buscar as cores
     * @return array
     * @throws Exception
     */
    public function getCores() {
        try {
            $sql = "SELECT * FROM sf_produtos_cor ORDER BY id_produto_cor ASC";
            $res = Conexao::conect($sql);
            $dados = [];
            while($rows = odbc_fetch_array($res)) {
                $dados[$rows['id_produto_cor']] = escreverTexto($rows['nome_produto_cor']);
            }
            return $dados;
        }catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Buscar os Termos
     * @param string|null $condicao
     * @throws Exception
     * @return array $data
     */
    public function getTermos($condicao = null) {
        try {
            $where = $condicao ? " WHERE ".$condicao : ""; 
            $sql = "select * from sf_configuracao_termos".$where;
            $rs = Conexao::conect($sql);
            $data = [];
            while($rows = odbc_fetch_array($rs)) {
                $data[] = [
                    'id'            => $rows['id'],
                    'termo'         => escreverTexto($rows['termo']),
                    'grupo'         => $rows['grupo'],
                    'inativo'       => $rows['inativo'],
                    'obrigatorio'   => $rows['obrigatorio']
                ];
            }
            return $data;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Query Base
     * @return string
     */
    private function getSqlBase() {
        return "P.*, CM.descricao as grupo, CM.pes_jur,
            (SELECT TOP 1 s.preco_venda FROM sf_produtos s WHERE S.tipo = 'S' AND P.conta_produto_adesao = s.conta_produto) as taxa_adesao,
            (SELECT TOP 1 seg_piso_franquia FROM sf_configuracao WHERE id = 1) as seg_piso_franquia
            FROM sf_produtos P
            LEFT JOIN sf_contas_movimento CM ON CM.id_contas_movimento = P.conta_movimento 
            WHERE conta_produto > 0";
    }

    /**
     * Response
     * @param array $rows
     * @return array
     */
    private function getResponse($rows, $valorFipe) {
        return [
            'id'                  => escreverTexto($rows['conta_produto']),
            'nome'                => escreverTexto($rows['descricao']),
            'id_grupo'            => escreverTexto($rows['conta_movimento']),
            'grupo'               => escreverTexto($rows['grupo']),
            'pes_jur'             => intval($rows['pes_jur']),
            'parcelar'            => intval($rows['parcelar']),
            'adesao_soma'         => intval($rows['adesao_soma']),
            'valor'               => escreverNumero($rows['preco_venda']),
            'segMax'              => $rows['segMax'],
            'descricao'           => escreverTexto($rows['descricao_site']),
            'mensagem'            => escreverTexto($rows['mensagem_site']),
            'taxa_adesao'         => escreverNumero($rows['taxa_adesao']),
            'cota_defaut'         => $rows['cota_defaut'],
            'cota_defaut_tp'      => $rows['cota_defaut_tp'],
            'seg_piso_franquia'   => $rows['seg_piso_franquia'],            
                'parcelas'            => $this->getListaProdutosParcelas('and id_produto = '.valoresSelect2($rows['conta_produto'])),
            'acessorios'          => $this->getServicosSeguro('p1.conta_produto = '.valoresSelect2($rows['conta_produto']), $valorFipe)
        ];
    }

}