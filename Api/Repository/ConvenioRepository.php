<?php
class ConvenioRepository {

 public function inserirConvenio($data) {
    try {
      $query = "insert into sf_convenios (descricao, status, tipo, convenio_especifico, ativo, ate_vencimento, id_conv_plano) 
      values (".valoresTexto2($data['descricao']).", 0, 1, ".valoresSelect2($data['convenio_especifico']).",
       0, (select top 1 adm_desc_vencimento from sf_configuracao), " . valoresSelect2($data['id_conv_plano']) . "); SELECT SCOPE_IDENTITY() id;";
       $res = Conexao::conect($query);
       odbc_next_result($res);
       $id = odbc_result($res, 1);
       if (!is_numeric($id)) {
           throw new Exception("Erro ao inserir o convênio => ".$query);
       }
       return $id;
    } catch (Exception $e) {
     throw new Exception($e->getMessage());
    }
  }

  public function removeConvenio($id_convenio) {
    try {
      $query = "delete sf_convenios where id_convenio = ".valoresSelect2($id_convenio);
      Conexao::conect($query);
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  public function inserirConvenioRegra($data) {
    try {
      $query = "insert into sf_convenios_regras (convenio, valor, tp_valor, 
      numero_minimo) values (".valoresSelect2($data['id_convenio']).", "
      .valoresNumericos2($data['valor']).", ".valoresTexto2($data['tp_valor']).", 0);
      SELECT SCOPE_IDENTITY() id;";
      $res = Conexao::conect($query);
      odbc_next_result($res);
      $id = odbc_result($res, 1);
      if (!is_numeric($id)) {
        throw new Exception("Erro ao inserir a regra do convênio => ".$query);
      }
      return $id;
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  public function atualizaConvenioRegra($data) {
    try {
      $query = "update sf_convenios_regras set valor = "
      .valoresNumericos2($data['valor']).", tp_valor = "
      .valoresTexto2($data['tp_valor'])." where convenio = "
      .valoresSelect2($data['id_convenio']).";";
      Conexao::conect($query);
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  public function removeConvenioRegra($id_convenio) {
    try {
      $query = "delete sf_convenios_regras where convenio = ".valoresSelect2($id_convenio);
      Conexao::conect($query);
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  public function inserirConvenioProduto($data) {
    try {
      $query = "insert into sf_convenios_planos (id_convenio_planos, id_prod_convenio) 
      values (".valoresSelect2($data['id_convenio']).", "
      .valoresSelect2($data['id_produto']).");";
      Conexao::conect($query);
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  public function removeConvenioProduto($data) {
    try {
      $query = "delete sf_convenios_planos where id_convenio_planos = "
      .valoresSelect2($data['id_convenio'])." and id_prod_convenio = "
      .valoresSelect2($data['id_produto']);
      Conexao::conect($query);
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  public function inserirConvenioUsuario($data) {
    try {
      $query = "set dateformat dmy; insert into sf_fornecedores_despesas_convenios (id_fornecedor, 
      convenio, dt_inicio, dt_fim) values (".valoresSelect2($data['id_fornecedor']).", 
      ".valoresSelect2($data['id_convenio']).", ".valoresData2($data['dt_inicio']).",
      ".valoresData2($data['dt_fim']).");";
      Conexao::conect($query);
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  public function atualizarConvenioUsuario($data) {
    try {
      $query = "set dateformat dmy; update sf_fornecedores_despesas_convenios set dt_inicio = "
      .valoresData2($data['dt_inicio']).", dt_fim = ".valoresData2($data['dt_fim'])
      ." WHERE id_fornecedor = ".valoresSelect2($data['id_fornecedor']). " AND 
      convenio = ".valoresSelect2($data['id_convenio']).";";
      Conexao::conect($query);
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  public function removeConvenioUsuario($data) {
    try {
      $query = "delete sf_fornecedores_despesas_convenios where convenio = "
      .valoresSelect2($data['id_convenio'])." and id_fornecedor = "
      .valoresSelect2($data['id_fornecedor']);
      Conexao::conect($query);
    } catch (Exception $e) {
      throw new Exception($e->getMessage());
    }
  }
}