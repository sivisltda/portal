<?php

class PlanoRepository {

    /**
     * Pega o plano
     * @param $condicoes
     * @return array
     * @throws Exception
     */
    public function getPlano($condicoes) {
        try {
            $data = [];
            $where = $condicoes ? ' AND '.$condicoes : '';
            $sql = "select top 1 ".$this->getSQLBasePlano().$where;
            $rs = Conexao::conect($sql);
            while($rows = odbc_fetch_array($rs)) {
                $data = $this->getResponse($rows);
            }
            return $data;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Lista os planos dos usuários
     * @param $condicoes
     * @param $paginacao
     * @return array $plans
     * @throws Exception
     * 
     */
    public function getPlanos($condicoes, $paginacao = []) {
        try {
            $where = $condicoes ? ' AND '.$condicoes : '';
            $sql = "select ".$this->getSQLBasePlano().$where;
            $sqlTotal = "";
            if(count($paginacao) > 0) {
                $limit = $paginacao['limit'];
                $qtd = $paginacao['qtd'];
                $sqlTotal = "select count(*) as total from (".$sql.") as a";
                $sql = "SELECT * FROM (SELECT ROW_NUMBER() OVER (order by dt_cadastro asc) as row, * 
                FROM (".$sql.") as x) as a where row BETWEEN " . ($limit + 1) . "AND " . ($limit + $qtd)." order by dt_cadastro asc;";
            }
            //echo $sql;exit;
            $query = Conexao::conect($sql);
            $data = [];
            $dados = [];
            while($rows = odbc_fetch_array($query)) {
                $data[] = $this->getResponse($rows);
            }
            if(count($paginacao) > 0) {
                $resTotal = Conexao::conect($sqlTotal);
                $dados['aaData'] = $data;
                $dados['total'] = odbc_result($resTotal, 'total');
            } else {
                $dados = $data;
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }   

    public function getDadosPlano($condicao) {
        try {
            $sql = "select top 1 vp.dt_cadastro, vp.dt_inicio, vp.dt_fim, p.descricao, v.placa, v.marca, 
            v.modelo, v.ano_modelo, f.id_fornecedores_despesas, f.razao_social, f.procedencia,
            (select top 1 id_mens from sf_vendas_planos_mensalidade where id_plano_mens = vp.id_plano order by dt_inicio_mens asc) as id_prim_mens 
            from sf_vendas_planos vp inner join sf_produtos p on p.conta_produto = vp.id_prod_plano
            inner join sf_fornecedores_despesas f on f.id_fornecedores_despesas = vp.favorecido
            left join sf_fornecedores_despesas_veiculo v on v.id = vp.id_veiculo 
            and v.id_fornecedores_despesas = f.id_fornecedores_despesas where ".$condicao;
            $res = Conexao::conect($sql);
            $dados = [];
            while($row = odbc_fetch_array($res)) {
                $row['razao_social'] = escreverTexto($row['razao_social']);
                $row['dt_cadastro']  = escreverData($row['dt_cadastro']);
                $row['dt_inicio']    = escreverData($row['dt_inicio']);
                $row['dt_fim']       = escreverData($row['dt_fim']);
                $row['descricao']    = escreverTexto($row['descricao']);
                $row['marca']        = escreverTexto($row['marca']);
                $row['modelo']       = escreverTexto($row['modelo']);
                $dados = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Cria ou atualiza o cartão de crédito do usuario
     * @param int $usuario_id
     * @param array $data
     * @throws Exception
     */
    public function criarCartaoUsuario($usuario_id, $data) {
        try {
            $num_cartao = str_replace(' ', '', $data["cartao_numero"]);
            $id_bandeira = $data["cartao_bandeira"];
            $id_cartao = null;
            $credencial = null;
            $legendaAnt = '';
            $cur = Conexao::conect("select operadora_dcc_site from sf_configuracao");
            while ($aRow = odbc_fetch_array($cur)) {
                $credencial = $aRow["operadora_dcc_site"];
            }
            $res_cartao = Conexao::conect('select TOP 1 id_cartao, legenda_cartao from sf_fornecedores_despesas_cartao where id_fornecedores_despesas = '.valoresSelect2($usuario_id));
            while ($row = odbc_fetch_array($res_cartao)) {
                $legendaAnt = $row['legenda_cartao'];
                $id_cartao = $row['id_cartao'];
            }
            if (!$id_cartao) {
                $legCartao = substr($num_cartao, 0, 4) . "..." . substr($num_cartao, -4, 4);
                $numCartao = base64_encode(str_replace(" ", "", $num_cartao));
                $query = "set dateformat dmy; insert into sf_fornecedores_despesas_cartao
                (id_fornecedores_despesas,nome_cartao, num_cartao, legenda_cartao, dv_cartao, validade_cartao, credencial, id_bandeira) values
                (" . valoresSelect2($usuario_id) . ", UPPER(" . valoresTexto2($data["nome_cartao"]) . ")," . valoresTexto2($numCartao) . "," . valoresTexto2($legCartao) . "," .
                valoresTexto2($data["cvv"]) . "," . valoresData2('01/' . str_replace(" ", "", $data["data_expiracao"])) . "," . 
                valoresNumericos2($credencial) . "," . valoresNumericos2($id_bandeira) . "); 
                SELECT SCOPE_IDENTITY() id;";
                $result = Conexao::conect($query);
                odbc_next_result($result);
                $id_cartao = odbc_result($result, 1);
                salvarLog(['tabela' => 'sf_fornecedores_despesas_cartao', 'id_item' => $id_cartao,
                'usuario' => getLoginUser(), 'acao' => 'I', 
                'descricao' => 'INCLUSAO CARTAO '.$legCartao." (".$data["cvv"].") - ".$data['nome_cartao'],
                'id_fornecedores_despesas' => $usuario_id]);
            } else {
                $legCartao = substr($num_cartao, 0, 4) . "..." . substr($num_cartao, -4, 4);
                $numCartao = base64_encode(str_replace(" ", "", $num_cartao));
                $query = "set dateformat dmy; update top (1) sf_fornecedores_despesas_cartao
                set nome_cartao = UPPER(" . valoresTexto2($data["nome_cartao"]) . ")" .
                    ",num_cartao = " . valoresTexto2($numCartao) .
                    ",legenda_cartao = " . valoresTexto2($legCartao) .
                    ",dv_cartao = " . valoresTexto2($data["cvv"]) .
                    ",validade_cartao = " . valoresData2('01/' . str_replace(" ", "", $data["data_expiracao"])) .
                    ",credencial = " . valoresNumericos2($credencial) .
                    ",id_bandeira = " . valoresNumericos2($id_bandeira) .
                    "where id_fornecedores_despesas = " . valoresSelect2($usuario_id);
                Conexao::conect($query);
                salvarLog(['tabela' => 'sf_fornecedores_despesas_cartao', 'id_item' => $id_cartao,
                'usuario' => getLoginUser(), 'acao' => 'A', 
                'descricao' => 'ALTERACAO CARTAO '.$legendaAnt." -> ".$legCartao." (".$data["cvv"].") - ".$data['nome_cartao'],
                'id_fornecedores_despesas' => $usuario_id]);
            }
        }catch(Exception $e) {
            throw new Exception($e->getMessage());
        }

    }
    
    /**
     * Atualiza Mensalidades DCC
     * @param array $usuario_id
     * @return array
     * @throws Exception
     */
    public function atualizaMensalidadesDCC($usuario_id) {
        $query = "update sf_vendas_planos_mensalidade set id_parc_prod_mens = isnull(pp.id_parcela, m.id_parc_prod_mens) from
        sf_vendas_planos_mensalidade m inner join sf_vendas_planos vp on m.id_plano_mens = vp.id_plano
        left join sf_produtos_parcelas pp on vp.id_prod_plano = pp.id_produto and parcela = 0
        where favorecido = " . valoresSelect2($usuario_id) . " and id_item_venda_mens is null";
        Conexao::conect($query);
    }
    
    /**
     * Vincular o usuario ao convênio se o produto pertencer ao mesmo
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function criarConvenio($data) {
        try {
            $dados = $this->getValidaCupomPlano($data);
            $cupomId = (isset($data['cupom_chave']) && strlen($data['cupom_chave']) > 0 ? $this->getValidaCupom($data) : 0);
            if ($dados && is_numeric($dados['id_convenio']) && DESCONTO_RANGE == 0) {
                return $dados;    
            } else if (isset($data['cupom_chave']) && strlen($data['cupom_chave']) > 0 && $cupomId > 0 && DESCONTO_RANGE == 0) {                
                $query = "insert into sf_convenios (descricao, status, tipo, convenio_especifico, ativo, ate_vencimento, cupom, cupom_meses, id_conv_plano)
                select descricao, status, tipo, convenio_especifico, ativo, ate_vencimento, cupom, cupom_meses, " . $data['id_plano'] . " 
                from sf_convenios where id_convenio = " . $cupomId . "; 
                SELECT SCOPE_IDENTITY() id;";
                $result = Conexao::conect($query);
                odbc_next_result($result);
                $id_convenio = odbc_result($result, 1);                
                $query = "insert into sf_convenios_planos (id_convenio_planos, id_prod_convenio) 
                values (" . $id_convenio . ", " . $data['id_produto'] . ");
                insert into sf_convenios_regras (convenio, valor, tp_valor, numero_minimo)
                select " . $id_convenio . ", valor, tp_valor, numero_minimo from sf_convenios_regras where convenio = " . $cupomId . ";                    
                insert into sf_fornecedores_despesas_convenios (convenio, id_fornecedor, dt_inicio, dt_fim) 
                values (" . $id_convenio . ", " . $data['id_usuario'] . ",GETDATE(),
                (select dateadd(month,(select isnull(cupom_meses, 0) from sf_convenios where id_convenio = " . $cupomId . "), getdate())));";
                Conexao::conect($query);                
                salvarLog(['tabela' => 'sf_fornecedores_despesas_convenios', 'id_item' => $dados['id_convenio'],
                'usuario' => getLoginUser(), 'acao' => 'I', 
                'descricao' => 'INCLUSAO CONVENIO '. $id_convenio . ' - ' . $data['cupom_chave'] . ' - ' . date('d/m/Y'),
                'id_fornecedores_despesas' => $data['id_usuario']]);                
                return $this->getValidaCupomPlano($data);                 
            } else if (isset($data['id_range']) && is_numeric($data['id_range']) && DESCONTO_RANGE == 1) {
                $dadosRange = $this->getValidaCupomPlanoRange($data);
                $perc_desc = ($dadosRange['valor_comiss'] - $data['id_range']);
                if (!is_numeric($dadosRange['id_convenio']) && $data['id_range'] < $dadosRange['valor_comiss'] && $perc_desc <= $dadosRange['max_desc'] && $perc_desc > 0) {
                    //Insert convenio
                    $query = "insert into sf_convenios (descricao, status, tipo, convenio_especifico, ativo, ate_vencimento, id_conv_plano)
                    values('" . $dadosRange['placa'] . "', 0, 1, 1, 0, (select top 1 adm_desc_vencimento from sf_configuracao), " . $data['id_plano'] . "); 
                    SELECT SCOPE_IDENTITY() id;";
                    $result = Conexao::conect($query);
                    odbc_next_result($result);
                    $id_convenio = odbc_result($result, 1);
                    $query = "insert into sf_convenios_planos (id_convenio_planos, id_prod_convenio) 
                    values (" . $id_convenio . ", " . $data['id_produto'] . ");
                    insert into sf_convenios_regras (convenio, valor, tp_valor, numero_minimo)
                    values (" . $id_convenio . ", " . $perc_desc . ",'P', 0);                    
                    insert into sf_fornecedores_despesas_convenios (convenio, id_fornecedor, dt_inicio, dt_fim) 
                    values (" . $id_convenio . ", " . $data['id_usuario'] . ",GETDATE(), dateadd(month, 1, GETDATE()));";
                    Conexao::conect($query);
                    salvarLog(['tabela' => 'sf_fornecedores_despesas_convenios', 'id_item' => $id_convenio,
                    'usuario' => getLoginUser(), 'acao' => 'I', 
                    'descricao' => 'INCLUSAO CONVENIO ' . $id_convenio . ' - ' . $perc_desc . "% " . date('d/m/Y'),
                    'id_fornecedores_despesas' => $data['id_usuario']]);                    
                    return $this->getCupomById($id_convenio, $data['id_produto']);
                } else if (!is_numeric($dadosRange['id_convenio']) && $data['id_range'] == $dadosRange['valor_comiss']) {
                    return null;
                } else if (is_numeric($dadosRange['id_convenio']) && $dadosRange['valor'] <> $perc_desc && $perc_desc <= $dadosRange['max_desc'] && $perc_desc > 0) {
                    //Update convenio
                    $this->atualizaIndiceCupom($dadosRange['id_convenio'], $perc_desc, $data['id_usuario']);                    
                    return $this->getCupomById($dadosRange['id_convenio'], $data['id_produto']);                    
                } else if (is_numeric($dadosRange['id_convenio']) && $dadosRange['valor'] == $perc_desc && $perc_desc <= $dadosRange['max_desc'] && $perc_desc > 0) {
                    //Sem alteração
                    return $this->getCupomById($dadosRange['id_convenio'], $data['id_produto']);                    
                } else if (is_numeric($dadosRange['id_convenio']) && $data['id_range'] == $dadosRange['valor_comiss']) {
                    //Delete convenio
                    $this->removeCupom($dadosRange['id_convenio'], $data['id_usuario']);                    
                    return null;
                } else {
                    throw new Exception("Indice Invalido!" . $dadosRange['id_convenio'] . "|" . $data['id_range'] . "|" . $dadosRange['valor_comiss'] . "|" . $perc_desc  . "|" . $dadosRange['max_desc'] . "|" . $perc_desc);
                }
            } else if (isset($data['cupom_chave']) && strlen($data['cupom_chave']) > 0 && $cupomId == 0 && DESCONTO_RANGE == 0) {
                throw new Exception("Cupom Invalido!");                
            } else {
                return null;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Pega o valida o Cupom e valor de desconto do convênio caso houver
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function getValidaCupomPlano($data) {
        try {
            $dados = [];            
            $sql = "select top 1 id_turno, cupom_meses as convenios_meses_site, id_convenio, valor, tp_valor, co.cupom, co.descricao,
            isnull((select top 1 id_prod_convenio from sf_convenios_planos where id_convenio_planos = " . valoresSelect2($data['id_produto']) . " and id_prod_convenio = p.conta_produto_adesao), 0) as desconto_adesao,
            case when 'C' in (select tipo from sf_convenios_planos inner join sf_produtos on id_prod_convenio = conta_produto where id_convenio_planos = co.id_convenio) then 1 else 0 end tipo
            from sf_convenios co 
            inner join sf_convenios_regras cr on cr.convenio = co.id_convenio
            left join sf_produtos p on co.id_convenio = p.convenios_site
            where id_conv_plano = " . valoresSelect2($data['id_plano']) . " order by tipo desc ";
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados = $row;
                $dados['cupom']             = escreverTexto($row['descricao']);
                $dados['descricao']         = escreverTexto($row['descricao']);
                $dados['desconto_adesao']   = $row['desconto_adesao'] > 0;
                $dados['valor']             = escreverNumero($row['valor']);                
                $dados['cupom_texto']       = escreverTexto($row['cupom']);
            }
            return $dados;
        } catch (Exception $e) {
            throw  new Exception($e->getMessage());
        }
    }
    
    /**
     * Pega o valida o Cupom e valor de desconto do convênio caso houver
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function getValidaCupomPlanoRange($data) {
        try {
            $dados = [];            
            $sql = "select id_fornecedores_despesas, id_user_resp, id_convenio, valor,
            (select max(valor) from sf_comissao_cliente_padrao_item where id_comissao_padrao = u.id_comissao_padrao and tipo = 0) valor_comiss,
            (select max(fin_desc_plano_tot) from sf_usuarios_permissoes where id_permissoes = u.master and fin_desc_plano = 1) max_desc,
            (select max(placa) from sf_fornecedores_despesas_veiculo where id = id_veiculo) placa
            from sf_vendas_planos p
            inner join sf_fornecedores_despesas c on p.favorecido = c.id_fornecedores_despesas
            inner join sf_usuarios u on u.id_usuario = c.id_user_resp
            left join sf_convenios co on co.id_conv_plano = p.id_plano
            left join sf_convenios_regras r on co.id_convenio = r.convenio
            where id_plano = " . valoresSelect2($data['id_plano']);
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw  new Exception($e->getMessage());
        }
    }
 
    /**
     * Pega o valida o Cupom e valor de desconto do convênio caso houver
     * @param array $data
     * @return array
     * @throws Exception
     */   
    public function getValidaCupom($data) {
        try {
            $toReturn = 0;
            $sql = "select top 1 id_convenio from sf_convenios co 
            inner join sf_convenios_regras cr on cr.convenio = co.id_convenio
            where co.id_conv_plano is null and ((co.convenio_especifico = 1 
            and (co.id_convenio in (select id_convenio_planos from sf_convenios_planos where id_prod_convenio = " . 
            valoresSelect2($data['id_produto']) . "))) or co.convenio_especifico = 0) and cupom = " . 
            valoresTexto2($data['cupom_chave']) . " and len(isnull(cupom, '')) > 0 and co.status = 0;";
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $toReturn = $row['id_convenio'];
            }
            return $toReturn;
        } catch (Exception $e) {
            throw  new Exception($e->getMessage());
        }
    }

    /**
     * Vincular os acessórios no plano do usuario
     * @param array $data
     * @return float 
     * @throws Exception
     */
    public function criarPlanoAccessorios($data) {
        try {
            $notIn = "0";
            $acessorios = isset($data['acessorios']) ? $data['acessorios'] : [];
            foreach ($acessorios as $servico) {
                if ($servico > 0) {
                    $notIn .= "," . $servico;
                }
            }
            $query = "delete from sf_vendas_planos_acessorios where id_venda_plano = " . valoresSelect2($data['id_plano']) . " and id_servico_acessorio not in (" . $notIn . ")";
            Conexao::conect($query);
            foreach ($acessorios as $servico) {
                if ($servico > 0) {
                    $aQuery = "IF NOT EXISTS(SELECT 1 FROM sf_vendas_planos_acessorios WHERE id_venda_plano = " . valoresSelect2($data['id_plano']) . " and id_servico_acessorio = " . valoresSelect2($servico) . ")
                    INSERT INTO sf_vendas_planos_acessorios(id_venda_plano,id_servico_acessorio) VALUES(" . valoresSelect2($data['id_plano']) . ", " . valoresSelect2($servico) . ")";
                    Conexao::conect($aQuery);
                }
            }
            $insertPadrao = "insert into sf_vendas_planos_acessorios (id_venda_plano, id_servico_acessorio)
            select " . $data['id_plano'] . ", id_materiaprima from sf_produtos_materiaprima
            inner join sf_produtos on sf_produtos.conta_produto = id_materiaprima where tipo = 'A' 
            and id_produtomp in (select id_produto from sf_produtos_parcelas where id_parcela = " . valoresSelect2($data['produto']['parcela_id']) . ")
            and conta_movimento not in (select id_contas_movimento from sf_vendas_planos_acessorios
            inner join sf_produtos on conta_produto = id_servico_acessorio
            inner join sf_contas_movimento on conta_movimento = id_contas_movimento
            where id_venda_plano = " . valoresSelect2($data['id_plano']) . " and terminal = 0)";
            Conexao::conect($insertPadrao);
            $result = Conexao::conect("select dbo.FU_VALOR_ACESSORIOS(".$data['id_plano'].")");
            return escreverNumero(odbc_result($result, 1), 1);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Pega o dias de férias
     * @param string $meses
     * @return int $dias_ferias
     * @throws Exception 
     */
    public function getDiaFerias($meses) {
        try {
            $dias_ferias = 0;
            $cur = Conexao::conect("select top 1 dias from sf_ferias where meses = ".$meses);
            while ($row = odbc_fetch_array($cur)) {
                $dias_ferias = $row["dias"];
            }
            return $dias_ferias;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Pega a data do primeiro vencimento
     * @param array $data
     * @return string $dt_fim
     * @throws Exception
     */
    public function getDataPrimVencimento($data) {
        try {
            $dt_fim = "";
            $parcela = $data['produto']['num_parcela'];
            if (in_array(TIPO_VENCIMENTO, ["1","2"])) {
                $dia = isset($data['data_vencimento']) ? $data['data_vencimento'] : DIA_VENCIMENTO; 
                $dt_fim = valoresData2($this->atualizaDataVencimento($dia, $parcela));
            } else {
                $dt_fim = "dateadd(day,-1,dateadd(month,".$parcela.",getdate()))";
            }
            return $dt_fim;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Verifica se o Usuário já possui o plano
     * @param array $data
     * @return array $dados
     * @throws Exception 
     */
    public function verificaPlanoUsuario($data) {
        try {
            $modulos = getModulos();
            $dados = [];
            $sql = $modulos['seg'] ? "select top 1 id, id_prod_plano as produto, id_plano, p.descricao 
            from sf_fornecedores_despesas_veiculo 
            left join sf_vendas_planos vp on id_veiculo = id and dt_cancelamento is null and (planos_status = 'Novo' or planos_status is null)
            left join sf_produtos p on p.conta_produto = vp.id_prod_plano or p.conta_produto = ".valoresSelect2($data['id_produto'])."
            where id = " . valoresSelect2($data['id_veiculo'])." and id_fornecedores_despesas = ".valoresSelect2($data['id_usuario']).";"
            : (PLANO_UNICO ? "select top 1 id_plano, conta_produto as produto, p.descricao
            from sf_produtos p
            left join sf_vendas_planos vp on favorecido = ".valoresSelect2($data['id_usuario'])." and dt_cancelamento is null and planos_status not in ('Cancelado')
            WHERE (vp.id_plano is null and p.conta_produto = ".valoresSelect2($data['id_produto']).") or (vp.id_plano is not null and p.conta_produto = vp.id_prod_plano);"
            : "select top 1 id_plano, conta_produto as produto, p.descricao
            from sf_produtos p
            left join sf_vendas_planos vp on vp.id_prod_plano = p.conta_produto and dt_cancelamento is null and (planos_status = 'Novo' or planos_status is null)
            and favorecido = ".valoresSelect2($data['id_usuario'])."
            where p.conta_produto = ".valoresSelect2($data['id_produto']).";");
            $cur = Conexao::conect($sql);
            while ($row = odbc_fetch_array($cur)) {
                $dados['descricao'] = escreverTexto($row['descricao']);
                $dados['id_plano'] = $row["id_plano"];
                $dados['produto'] = $row["produto"];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Salva o plano do Usuário
     * @param array $data
     * @return int
     * @throws Exception
     */
    public function salvaPlanoUsuario($data) {
        try {
            $modulos = getModulos();
            $dados = $this->verificaPlanoUsuario($data);
            if (!$dados['id_plano']) {
                $sQuery = "set dateformat dmy; insert into sf_vendas_planos(id_turno, dt_inicio, dt_fim, favorecido,dias_ferias,dt_cadastro,usuario_resp,id_prod_plano, planos_status, id_veiculo)
                values(".valoresSelect2($data['horario_id']) . ", GETDATE(), ". $data['dt_fim']."," . valoresSelect2($data['id_usuario']) . "," .
                valoresNumericos2($data['dias_ferias']) . ", getdate(), 1," . valoresSelect2($data['id_produto']) . ", 'Novo', "
                .((isset($data['id_veiculo']) && $data['id_veiculo']) ? valoresSelect2($data['id_veiculo']): "null")."); SELECT SCOPE_IDENTITY() id;";
                $res = Conexao::conect($sQuery);
                odbc_next_result($res);
                $dados['id_plano'] = odbc_result($res, 1);
                salvarLog(['tabela' => 'sf_vendas_planos', 'id_item' => $dados['id_plano'],
                'usuario' => getLoginUser(), 'acao' => 'I', 
                'descricao' => 'INCLUSAO PLANO ('.$dados['descricao'].')',
                'id_fornecedores_despesas' => $data['id_usuario']]);
            } else if ($modulos['seg'] && (isset($dados['produto']) && $data['id_produto'] != $dados['produto'])) {
                $sql = "set dateformat dmy; UPDATE sf_vendas_planos SET id_prod_plano = " .valoresSelect2($data['id_produto']) . ",
                dt_inicio = getdate(), dt_fim = ".$data['dt_fim']." 
                WHERE id_plano = " .valoresSelect2($dados['id_plano'])." and (planos_status = 'Novo' or planos_status is null)";
                Conexao::conect($sql);
            } else {
                $sqlUpdate = "set dateformat dmy; update sf_vendas_planos set dt_inicio = getdate(), dt_fim = ".$data['dt_fim']."
                WHERE id_plano = ".valoresSelect2($dados['id_plano'])." and (planos_status = 'Novo' or planos_status is null)";
                Conexao::conect($sqlUpdate);
            }
            if (isset($data['id_veiculo']) && is_numeric($data['id_veiculo'])) {
                $sql = "update sf_fornecedores_despesas_veiculo set 
                franquia = case when (select top 1 seg_piso_franquia from sf_configuracao) >= (case when p.cota_defaut_tp = 1 then p.cota_defaut else (valor * p.cota_defaut)/100 end)
                then (select top 1 seg_piso_franquia from sf_configuracao)
                else p.cota_defaut end,
                franquia_tp = case when (select top 1 seg_piso_franquia from sf_configuracao) >= (case when p.cota_defaut_tp = 1 then p.cota_defaut else (valor * p.cota_defaut)/100 end)
                then 1 else p.cota_defaut_tp end
                from sf_fornecedores_despesas_veiculo v
                inner join sf_vendas_planos vp on v.id = vp.id_veiculo
                inner join sf_produtos p on p.conta_produto = vp.id_prod_plano
                where id = " . valoresSelect2($data['id_veiculo']);              
                Conexao::conect($sql);
            }
            return $dados;

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getTotalAcessorios($condicao) {
        try {
            $sql = "select count(id_servico_acessorio) as total from sf_vendas_planos_acessorios vpa 
            inner join sf_vendas_planos vp on vp.id_plano = vpa.id_venda_plano
            inner join sf_vendas_planos_mensalidade vm on vm.id_plano_mens = vp.id_plano 
            inner join sf_produtos_parcelas pp on pp.id_parcela = vm.id_parc_prod_mens
            and id_servico_acessorio not in (select distinct id_materiaprima from sf_produtos_materiaprima
            inner join sf_produtos on sf_produtos.conta_produto = id_materiaprima where tipo = 'A' 
            and id_produtomp = pp.id_produto) and ".$condicao;
            $res = Conexao::conect($sql);
            return intval(odbc_result($res, 1));
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Base Query
     * @return string
     */
    private function getSQLBasePlano() {
        return " A.*, B.conta_produto, B.descricao, B.descricao_site, B.parcelar, CM.descricao AS grupo, dependentes.*, mensalidades.taxa_adesao,
        isnull((select top 1 cr2.valor valor_desc_adesao from sf_convenios c2 inner join sf_convenios_planos cp2 on cp2.id_convenio_planos = c2.id_convenio
        inner join sf_fornecedores_despesas_convenios fc2 on fc2.convenio = c2.id_convenio inner join sf_convenios_regras cr2 on cr2.convenio = c2.id_convenio
        where c2.id_conv_plano = A.id_plano and cp2.id_prod_convenio = B.conta_produto_adesao and fc2.id_fornecedor = A.favorecido and getdate() between fc2.dt_inicio and fc2.dt_fim), 0) as valor_desc_adesao,            
        case when adesao_dependente is not null then adesao_dependente when id_prim_mens = id_mens then mensalidades.taxa_adesao else null end as valor_adesao, 
        placa, marca, modelo, ano_modelo, mensalidades.tipo_pagamento, convenio.*, 
        (dbo.FU_VALOR_ACESSORIOS(A.id_plano) + mensalidades.valor_unico + isnull(dependentes.acrescimo,0)) as valor_plano, ve.tipo_veiculo, id_mens, mensalidades.parcela, mensalidades.id_parcela, dt_inicio_mens as prox_venc,
        mensalidades.id_prim_mens, mensalidades.prev_mens, case when configuracao.ACA_TIPO_VENCIMENTO in (1,2) then 
        isnull((select top 1 dia from sf_configuracao_vencimentos where dia >= (select top 1 case when mensalidades.dt_fim_mens = EOMONTH(mensalidades.dt_fim_mens) 
        then 1 else datepart(DAY, mensalidades.dt_fim_mens) end as dia)), configuracao.ACA_DIA_VENCIMENTO) 
        else DATEPART(day, mensalidades.dt_fim_mens) end as dia_vencimento,
        case when (CAST(dateadd(day, configuracao.aca_dias_tolerancia_status, (
        select top 1 dt_inicio_mens from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano 
        and dt_inicio_mens < cast(getdate() as date) and dt_pagamento_mens is null
        )) as date) > cast(getdate() as date) and dt_cancelamento is null and A.planos_status in ('Suspenso'))
        then 'Ativo' else A.planos_status end as plano_status, B.parcelar gera_parc,
        case when mensalidades.parcela = 0 then (select top 1 legenda_cartao from sf_fornecedores_despesas_cartao where id_fornecedores_despesas = A.favorecido)
        else null end as legenda_cartao
        from sf_vendas_planos A inner join sf_produtos B on A.id_prod_plano = B.conta_produto
        cross apply (select top 1 aca_dias_tolerancia_status, ACA_DIA_VENCIMENTO, ACA_TIPO_VENCIMENTO, aca_adesao_dep, dcc_dias_boleto from sf_configuracao) as configuracao
        cross apply (select *, DATEDIFF(day,(select case when x.parcela = 0 or x.id_prim_mens = x.id_mens then x.dt_inicio_mens 
        else DATEADD(day,-configuracao.dcc_dias_boleto, x.dt_inicio_mens) end as data_prazo), x.prev_mens) as prorata,
        case when x.parcela = 0 or x.id_prim_mens = x.id_mens then x.dt_inicio_mens else DATEADD(day,-configuracao.dcc_dias_boleto, x.dt_inicio_mens) end as data_prazo
        from (select top 1 case when (pp.parcela = 0) then 'DCC' when (pp.parcela = -1) then 'BOLETO' when (pp.parcela = -2) then 'PIX' else 'MENSAL' end as tipo_pagamento,
        (select top 1 id_mens from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano and CAST(getdate() as date) between dt_inicio_mens and dt_fim_mens order by dt_inicio_mens asc) as id_prim_mens,
        (SELECT TOP 1 s.preco_venda FROM sf_produtos s WHERE s.tipo = 'S' AND s.conta_produto = B.conta_produto_adesao) as taxa_adesao,
        isnull((select max(dt_inicio_mens) from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano and id_mens < pm.id_mens), dateadd(month, -(select case when pp.parcela < 1 then 1 else pp.parcela end), dt_inicio_mens)) as prev_mens,
        id_mens, dt_inicio_mens, dt_fim_mens, pp.parcela, pp.id_parcela, (pp.valor_unico - (pp.valor_unico * pp.valor_desconto / 100)) as valor_unico 
        from sf_vendas_planos_mensalidade pm inner join sf_produtos_parcelas pp on pp.id_parcela = pm.id_parc_prod_mens
        where id_plano_mens = A.id_plano and id_mens = (select top 1 case when pm.dt_pagamento_mens is null then (select top 1 id_mens from sf_vendas_planos_mensalidade 
        where id_plano_mens = A.id_plano and dt_pagamento_mens is null and CAST(getdate() as date) between dt_inicio_mens and dt_fim_mens order by dt_inicio_mens asc
        ) else (select top 1 id_mens from sf_vendas_planos_mensalidade where id_plano_mens = A.id_plano and CAST(getdate() as date) between dt_inicio_mens and dt_fim_mens order by dt_inicio_mens desc) end id_mens) ) as x) as mensalidades
        LEFT JOIN sf_contas_movimento CM ON CM.id_contas_movimento = B.conta_movimento
        left join sf_fornecedores_despesas_veiculo ve on ve.id = A.id_veiculo
        outer apply (select top 1 cupom_meses as convenios_meses_site, id_convenio, valor, tp_valor, co.cupom, co.descricao as convenio_descricao,
        isnull((select top 1 id_prod_convenio from sf_convenios_planos where id_convenio_planos = B.conta_produto and id_prod_convenio = p.conta_produto_adesao), 0) as desconto_adesao 
        from sf_fornecedores_despesas_convenios fdc inner join sf_convenios co on co.id_convenio = fdc.convenio
        inner join sf_convenios_regras cr on cr.convenio = co.id_convenio left join sf_produtos p on co.id_convenio = p.convenios_site
        where ((co.convenio_especifico = 1 and (conta_produto = B.conta_produto or co.id_convenio in (select id_convenio_planos from sf_convenios_planos
        where id_prod_convenio = B.conta_produto))) or co.convenio_especifico = 0) and co.ativo = 0 and fdc.id_fornecedor = A.favorecido) as convenio 
        outer apply (select sum(valor * dependentes) as acrescimo,  novo, dependentes, valor as valor_dependente, 
        sum((case when config_adesao = 1 and (convenio_desconto = 0 or convenio_desconto is null) then taxa_adesao 
        when config_adesao = 1 and convenio_desconto = 1 and tipo_convenio = 'D' then taxa_adesao - convenio_valor  
        when config_adesao = 1 and convenio_desconto = 1 and tipo_convenio = 'P' then  taxa_adesao - (valor * convenio_valor) / 100 else 0 end) * novo) as adesao_dependente
        from (select valor, taxa_adesao, count(case when cast(data_inclusao as date) <= x.dt_prazo then id_dependente else null end) as dependentes, config_adesao, convenio_desconto, tipo_convenio, convenio_valor,
        count(case when (cast(data_inclusao as date) <= x.dt_prazo and cast(data_inclusao as date) > DATEADD(day, x.prorata, x.dt_prazo)) then id_dependente else null end) as novo 
        from (select id, id_dependente, valor, data_inclusao, mensalidades.data_prazo as dt_prazo, mensalidades.prorata as prorata, mensalidades.taxa_adesao as taxa_adesao, 
        convenio.tp_valor as tipo_convenio, convenio.valor as convenio_valor, convenio.desconto_adesao as convenio_desconto, configuracao.aca_adesao_dep as config_adesao
        from sf_vendas_planos inner join sf_fornecedores_despesas_dependentes fdd on fdd.id_titular = favorecido
        inner join sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = fdd.id_dependente and fd.inativo = 0
        left join sf_produtos_acrescimo on id_produto = id_prod_plano and ((datediff(year,data_nascimento,getdate()) between idade_min and idade_max_m and sexo = 'M') or
        (datediff(year,data_nascimento,getdate()) between idade_min and idade_max_f and sexo = 'F')) and (id_parentesco is null or id_parentesco = parentesco)
        where id_plano = A.id_plano) as x group by valor, taxa_adesao, config_adesao, convenio_desconto, tipo_convenio, convenio_valor) as y group by valor, dependentes, novo) as dependentes WHERE 1 = 1 ";
    }

    /**
     * Response da Repositorio
     * @param array $rows
     * @return array $item     * 
     */
    private function getResponse($rows) {
        $item = [
            'id'                => $rows['id_plano'],
            'favorecido'        => $rows['favorecido'],
            'nome'              => escreverTexto($rows['descricao']),
            'descricao'         => escreverTexto($rows['descricao_site']),
            'id_produto'        => $rows['id_prod_plano'],
            'id_mens'           => $rows['id_mens'],
            'id_prim_mens'      => $rows['id_prim_mens'],
            'prox_venc'         => escreverData($rows['prox_venc']),
            'grupo'             => escreverTexto($rows['grupo']),
            'dt_inicio'         => escreverData($rows['dt_inicio']),
            'dt_fim'            => escreverData($rows['dt_fim']),
            'dia_vencimento'    => $rows['dia_vencimento'],
            'status'            => escreverTexto($rows['plano_status']),
            'convenio_meses'    => $rows['convenios_meses_site'],
            'convenio_valor'    => escreverNumero($rows['valor']),
            'convenio_tipo'     => $rows['tp_valor'],
            'cupom'             => $rows['cupom'],
            'convenio_descricao'=> escreverTexto ($rows['convenio_descricao']),
            'taxa_adesao'       => escreverNumero($rows['taxa_adesao'], 1),
            'valor_desc_adesao' => escreverNumero($rows['valor_desc_adesao'], 1),
            'valor_adesao'      => escreverNumero($rows['valor_adesao'], 1),
            'desconto_adesao'   => intval($rows['desconto_adesao']),   
            'valor_plano'       => escreverNumero($rows['valor_plano'], 1),
            'gerar_parc'        => intval($rows['parcelar']),
            'id_parcela'        => $rows['id_parcela'],
            'parcela'           => $rows['parcela'],
            'dependentes'       => $rows['dependentes'],
            'novo_dependentes'  => $rows['novo'],
            'id_veiculo'        => $rows['id_veiculo'],
            'placa'             => $rows['placa'],
            'tipo_veiculo'      => $rows['tipo_veiculo'],
            'modelo'            => escreverTexto($rows['modelo']),
            'marca'             => escreverTexto($rows['marca']),
            'ano_modelo'        => $rows['ano_modelo'],
            'tipo_pagamento'    => escreverTexto($rows['tipo_pagamento']),
            'dt_cadastro'       => escreverData($rows['dt_cadastro'])
        ];
        if ($rows['legenda_cartao']) {
            $item['cartao'] = [
                'legenda_cartao'    => $rows['legenda_cartao'],
                'bandeira'          => verificar_bandeira($rows['legenda_cartao'])
            ];
        }
        return $item;
    }

    /**
     * Autaliza a data de vencimento
     * @param string $dia
     * @param int $parcela
     * @return string
     */
    public function atualizaDataVencimento($dia, $parcela) {
        $agora = new DateTime();
        $mesAtual = $agora->format('Y-m');
        $dataVencimento = (new DateTime($mesAtual.'-'.$dia))->modify('-1 day');
        $intervalo = $agora->diff($dataVencimento); 
        $resposta =  $intervalo->days < 0 ? $dataVencimento->modify('+'.$parcela.' month') : $dataVencimento;
        return $resposta->format('d/m/Y');
    }

    /**
     * Atualiza o Status do Plano
     * @param int $id_plano
     * @throws Exception
     */
    public function atualizaStatusPlano($id_plano) {
        try {
            $sql = "update sf_vendas_planos set planos_status = dbo.FU_STATUS_PLANO_CLI(id_plano),
            dt_inicio = dbo.FU_PLANO_INI(id_plano), dt_fim = dbo.FU_PLANO_FIM(id_plano) 
            WHERE id_plano = ".valoresSelect2($id_plano);
            Conexao::conect($sql);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Cancela o Plano 
     * @param int $id_plano
     * @throws Exception 
     */
    public function cancelaPlano($id_plano) {
        try {   
            $sql = "update sf_vendas_planos set dt_cancelamento = getdate(), usuario_canc = 1 
            where id_plano = ".valoresSelect2($id_plano);
            Conexao::conect($sql);
           $this->atualizaStatusPlano($id_plano);
        } catch (Exception $e) {
            throw new Exception($e->getMessage()); 
        }
    }

    /**
     * Remove o Plano
     * @param int $id_plano
     * @throws Exception
     */
    public function removePlano($id_plano) {
        try {
            Conexao::initTransaction();
            $sqlMensalidades = "delete from sf_vendas_planos_mensalidade where id_plano_mens = ".valoresSelect2($id_plano);
            Conexao::conect($sqlMensalidades);
            $sql = "delete from sf_vendas_planos where id_plano = ".valoresSelect2($id_plano);
            Conexao::conect($sql);
            Conexao::commit();
        } catch (Exception $e) {
            Conexao::rollback();
            throw new Exception($e->getMessage()); 
        }
    }

    /**
     * Atualiza Dia Vencimento
     * @param int $id_veiculo
     * @param int $vencimento
     * @throws Exception
     */
    public function atualizaDiaVencimento($id_veiculo, $vencimento) {
        try {
            if (is_numeric($id_veiculo) && is_numeric($vencimento)) {
                $cur = Conexao::conect("select top 1 id_mens, dt_inicio_mens, id_plano, id_parc_prod_mens from sf_vendas_planos
                inner join sf_vendas_planos_mensalidade on id_plano = id_plano_mens
                where dt_cancelamento is null and dt_pagamento_mens is null and id_veiculo = " . valoresSelect2($id_veiculo) . " order by dt_inicio_mens");
                while ($row = odbc_fetch_array($cur)) {
                    $id_mens = $row["id_mens"];                    
                    $dt_inicio_mens = $row["dt_inicio_mens"];
                    $id_plano = $row["id_plano"];
                    $id_parc_prod_mens = $row["id_parc_prod_mens"];          
                }
                $cur = Conexao::conect("select dia, minimo, pro_rata from sf_configuracao_vencimentos where id = " . valoresSelect2($vencimento));
                while ($row = odbc_fetch_array($cur)) {
                    $dia = $row["dia"];
                    $minimo = $row["minimo"];
                    $pro_rata = $row["pro_rata"];
                }
                $sql = "EXEC dbo.SP_MK_MENS_DIA_COND @id_mens = " . $id_mens . ", @dt_ini = '" . $dt_inicio_mens . "', @id_plano = " . $id_plano . ",
                @id_parcela = " . $id_parc_prod_mens . ", @dia_padrao = " . $dia . ", @minimo = " . $minimo . ", @pro_rata = " . $pro_rata . ";";
                Conexao::conect($sql);
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage()); 
        }  
    }
    
    /**
     * Atualiza o Indice do Cupom
     * @param int $id_cupom
     * @param double $indice
     * @throws Exception
     */
    public function atualizaIndiceCupom($id_cupom, $indice, $id_usuario) {
        try {
            $sql = "update sf_convenios_regras set valor = " . valoresNumericos2($indice) . " where convenio = ".valoresSelect2($id_cupom);
            Conexao::conect($sql);            
            salvarLog(['tabela' => 'sf_fornecedores_despesas_convenios', 'id_item' => $id_cupom,
            'usuario' => getLoginUser(), 'acao' => 'E', 
            'descricao' => 'ALTERACAO CONVENIO '. $id_cupom . ' - ' . $indice . "% " . date('d/m/Y'),
            'id_fornecedores_despesas' => $id_usuario]);
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Remove o Cupom
     * @param int $id_cupom
     * @throws Exception
     */
    public function removeCupom($id_cupom, $id_usuario) {
        try {
            $sql = "delete from sf_fornecedores_despesas_convenios where convenio in (select id_convenio from sf_convenios where id_conv_plano in 
            (select id_conv_plano from sf_convenios where id_conv_plano is not null and id_convenio = " . valoresSelect2($id_cupom) . "));
            delete from sf_convenios_regras where convenio in (select id_convenio from sf_convenios where id_conv_plano in 
            (select id_conv_plano from sf_convenios where id_conv_plano is not null and id_convenio = " . valoresSelect2($id_cupom) . "));
            delete from sf_convenios_planos where id_convenio_planos in (select id_convenio from sf_convenios where id_conv_plano in 
            (select id_conv_plano from sf_convenios where id_conv_plano is not null and id_convenio = " . valoresSelect2($id_cupom) . "));
            delete from sf_convenios where id_convenio in (select id_convenio from sf_convenios where id_conv_plano in 
            (select id_conv_plano from sf_convenios where id_conv_plano is not null and id_convenio = " . valoresSelect2($id_cupom) . "));";
            Conexao::conect($sql);            
            
            $sql = "delete from sf_fornecedores_despesas_convenios where convenio = " . valoresSelect2($id_cupom) . ";
            delete from sf_convenios_regras where convenio = " . valoresSelect2($id_cupom) . ";
            delete from sf_convenios_planos where id_convenio_planos = " . valoresSelect2($id_cupom) . ";
            delete from sf_convenios where id_convenio = " . valoresSelect2($id_cupom) . ";";
            Conexao::conect($sql);  
            
            salvarLog(['tabela' => 'sf_fornecedores_despesas_convenios', 'id_item' => $id_cupom,
            'usuario' => getLoginUser(), 'acao' => 'R', 
            'descricao' => 'EXCLUSAO CONVENIO '. $id_cupom . ' - ' . date('d/m/Y'),
            'id_fornecedores_despesas' => $id_usuario]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage()); 
        }
    }
        
    /**
     * Remove o Acessorios
     * @param int $id_plano
     * @throws Exception
     */
    public function removeAcessorios($id_plano) {
        try {
            $sql = "delete from sf_vendas_planos_acessorios where id_venda_plano = " . valoresSelect2($id_plano);
            Conexao::conect($sql);
        } catch (Exception $e) {
            throw new Exception($e->getMessage()); 
        }  
    }
    
    /**
     * Verificar o convênio se o produto pertencer ao mesmo
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function verificarCupom($data) {
        try {
            $cupomId = (isset($data['cupom_chave']) && strlen($data['cupom_chave']) > 0 ? $this->getValidaCupom($data) : 0);
            if ($cupomId > 0) {
                return $this->getCupomById($cupomId, $data['id_produto']);  
            } else if (isset($data['cupom_chave']) && strlen($data['cupom_chave']) > 0 && $cupomId == 0) {
                throw new Exception("Cupom Invalido!");                
            } else {
                return null;
            }                
        } catch (Exception $e) {
            throw new Exception($e->getMessage()); 
        }  
    }     
    
    /**
     * Pega o Cupom por Id
     * @param array $id, $id_produto
     * @return array
     * @throws Exception
     */
    public function getCupomById($id, $id_produto) {
        try {
            $dados = [];            
            $sql = "select top 1 id_turno, cupom_meses as convenios_meses_site, id_convenio, valor, tp_valor, co.cupom, co.descricao,
            isnull((select top 1 id_prod_convenio from sf_convenios_planos where id_convenio_planos = " . valoresSelect2($id_produto) . " and id_prod_convenio = p.conta_produto_adesao), 0) as desconto_adesao 
            from sf_convenios co 
            inner join sf_convenios_regras cr on cr.convenio = co.id_convenio
            left join sf_produtos p on co.id_convenio = p.convenios_site
            where id_convenio = " . valoresSelect2($id);
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados = $row;
                $dados['cupom']             = escreverTexto($row['descricao']);
                $dados['descricao']         = escreverTexto($row['descricao']);
                $dados['desconto_adesao']   = $row['desconto_adesao'] > 0;
                $dados['valor']             = escreverNumero($row['valor']);                
                $dados['cupom_texto']       = escreverTexto($row['cupom']);
            }
            return $dados;
        } catch (Exception $e) {
            throw  new Exception($e->getMessage());
        }
    }
}