<?php

class PessoaRepository {

    public function salvarPessoa($dados) {
        try {
            $modulos = getModulos();            
            $sql = "insert into sf_fornecedores_despesas (empresa, razao_social, cnpj, inativo, tipo, dt_cadastro, id_user_resp, fornecedores_status, procedencia, 
            juridico_tipo, regime_tributario, dt_aprov_prospect, socio, titular, termometro, nfe_iss) values (
            (select isnull(max(empresa), '1') from sf_fornecedores_despesas where id_fornecedores_despesas = " . verificaValorCampo($dados, 'consultor') . ")," .
            valoresTexto2($dados['nome']) . "," . valoresTexto2($dados['cpf']) . ", 0, 'P', getdate(), " .
            "(select top 1 id_usuario from sf_usuarios where funcionario = " . verificaValorCampo($dados, 'consultor') . ")," .
            "'Aguardando', " . valoresTexto2($dados['procedencia']) . ", 'F', 9, getdate(), " . ($modulos['seg'] ? 0 : 1) . ", " . ($modulos['seg'] ? 0 : 1) . ", 2," . valoresCheck2($dados['nfe_iss']) . ");
            SELECT SCOPE_IDENTITY() id;";
            $result = Conexao::conect($sql);
            odbc_next_result($result);
            $id = odbc_result($result, 1);
            if (!is_numeric($id)) {
                throw new Exception("Erro ao inserir o Pessoa => " . $sql);
            } else {
                Conexao::conect("insert into sf_fornecedores_despesas_contatos (fornecedores_despesas, tipo_contato, conteudo_contato) values (" . $id . ", 2, " . valoresTexto2($dados['email']) . ")");
                Conexao::conect("insert into sf_fornecedores_despesas_contatos (fornecedores_despesas, tipo_contato, conteudo_contato) values (" . $id . ", 1, " . valoresTexto2($dados['celular']) . ")");
            }
            return $id;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function salvarUsuario($id) {
        try {
            $id_usuario = "";
            $razao_social = "";
            $id_empresa = "";
            $res = Conexao::conect("select top 1 razao_social, id_usuario, isnull(empresa, 1) empresa 
            from sf_fornecedores_despesas left join sf_usuarios on funcionario = id_fornecedores_despesas
            where id_fornecedores_despesas = " . $id);
            while ($row = odbc_fetch_array($res)) {
                $id_usuario = $row['id_usuario'];
                $razao_social = escreverTexto($row['razao_social']);
                $id_empresa = $row['empresa'];
            }
            if (!is_numeric($id_usuario)) {
                $sql = "insert into sf_usuarios (login_user, senha, nome, master, email, imagem, funcionario, email_desc,
                email_host, email_porta, email_login, email_senha, email_ckb, 
                copyMe, filial, assinatura, email_ssl, data_cadastro, email_smtp, adm_mu) 
                values ('" . str_replace(" ", ".", $this->nameClean($this->nameCompact(strtolower($razao_social)))) . "." . $id . "', 'Z1tDZ1BE', " .
                valoresTexto2(strtoupper($razao_social)) . ",(select adm_func_usuario from sf_configuracao where id = 1), '', '', " .
                $id . ", '','', '', '', '', 'false', 0, 1, '', 0, getdate(), 0, 0);
                SELECT SCOPE_IDENTITY() id;";
                $result = Conexao::conect($sql);
                odbc_next_result($result);
                $id_user = odbc_result($result, 1);
                if (!is_numeric($id_user)) {
                    throw new Exception("Erro ao inserir o Usuario => " . $sql);
                } else {
                    Conexao::conect("insert into sf_usuarios_filiais (id_usuario_f, id_filial_f) values (" . $id_user . "," . $id_empresa . ")");
                }
                return $id_user;
            } else {
                return $id_usuario;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getPessoa($condicao) {
        try {
            $sql = "select top 1 " . $this->getSQLBase() . " where " . $condicao;
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados = $this->getResponse($row);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function getResponse($row) {
        $row['id_fornecedores_despesas'] = $row['id_fornecedores_despesas'];
        $row['razao_social'] = escreverTexto($row['razao_social']);
        $row['consultor'] = $row['consultor'];
        return $row;
    }

    private function getSQLBase() {
        return "p.*, (select top 1 funcionario from sf_usuarios where id_usuario = p.id_user_resp) consultor
        from sf_fornecedores_despesas_contatos c
        inner join sf_fornecedores_despesas p on p.id_fornecedores_despesas = c.fornecedores_despesas";
    }

    private function nameCompact($name) {
        $CompleteName = explode(" ", $name);
        $nomeUserLogin = $CompleteName[0];
        if (count($CompleteName) > 1) {
            $nomeUserLogin .= " " . $CompleteName[count($CompleteName) - 1];
        }
        return $nomeUserLogin;
    }

    private function nameClean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
