<?php

class AgendaRepository {

    /**
     * Lista a agenda 
     * @return array
     * @throws Exception
     */
    public function getListAgenda() {
        try {
            $dados = [];
            $sql = "SELECT * FROM sf_agenda where inativo = 0 and terminal = 1 order by descricao_agenda asc";
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados[] = $this->responseAgenda($row);
            }
            return $dados;
        }catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Pega a agenda pelo Id
     * @param $id_agenda
     * @return array
     * @throws Exception
     */
    public function getAgendaPorId($id_agenda) {
        try {
            $dados = null;
            $sql = "SELECT TOP 1 * FROM sf_agenda where inativo = 0 and terminal = 1 AND id_agenda = ".valoresSelect2($id_agenda)." order by descricao_agenda asc";
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados = $this->responseAgenda($row);
            }
            return $dados;
        }catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Busca o Agendamento por Id e pelo usuário
     * @param array $data
     * @param bool $externo
     * @return array|null
     * @throws Exception
     */
    public function getAgendamentoPorId($data, $externo) {
        try {
            $dados = null;
            $where = isset($data['id_usuario']) && $externo ? " AND id_pessoa_externo = ".valoresSelect2($data['id_usuario']) 
            :(isset($data['id_usuario']) ? ' AND id_fornecedores = '.valoresSelect2($data['id_usuario']) : "");
            if ($externo && isset($data['loja'])) {
                $where.= " AND loja_externo = ".$data['loja'];
            }
            $sql = "SELECT TOP 1 *,
              CASE WHEN ((data_inicio < GETDATE() AND data_fim >= GETDATE()) OR (data_inicio >= GETDATE())) THEN 'ativo' ELSE 'inativo' END as status
              FROM sf_agendamento WHERE id_agendamento = ".valoresSelect2($data['id_agendamento']).$where;
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $dados = $row;
                $dados['data_inicio']       = escreverDataHora($row['data_inicio']);
                $dados['data_fim']          = escreverDataHora($row['data_fim']);
                $dados['obs']               = escreverTexto($row['obs']);
                $dados['assunto']           = escreverTexto($row['assunto']);
                $dados['data_agendamento']  = escreverDataHora($row['data_agendamento']);
                $dados['agendado_por']      = escreverTexto($row['agendado_por']);
                $dados['status']            = escreverTexto($row['status']);
            }
            return $dados;
        }catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Lista os agendamentos
     * @param $id_agenda int
     * @param $data string
     * @throws Exception
     * @return array
     */
    public function getListaAgendamentos($id_agenda, $data) {
        try {
            $dados = [];
            $sql = "set dateformat dmy; select  count(id_agendamento) as quant_agendamento, data_inicio, data_fim 
            from sf_agendamento WHERE sf_agendamento.inativo = 0 AND id_agenda = " .valoresSelect2($id_agenda). " 
            AND ". valoresData2($data). " between cast(data_inicio as date) and cast(data_fim as date)
            group by data_inicio, data_fim";
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados[] = [
                    strtotime(str_replace("/", "-", escreverDataHora($row["data_inicio"]))),
                    strtotime(str_replace("/", "-", escreverDataHora($row["data_fim"]))),
                    $row['quant_agendamento']
                ];
            }
            return $dados;
        }catch(Exception $e) {
            throw new Exception($e->getMessage());
        }

    }

    /**
     * Lista os horários da Agenda
     * @param $id_agenda
     * @param $data
     * @return array
     * @throws Exception
     */
    public function getListHorarioAgenda($id_agenda, $data) {
        try {
            $dados      = $this->getConfigAgenda();
            $intervalo  = $dados['intervalo'];
            $horarios   = [];
            if (is_numeric($id_agenda) && valoresData2($data) != "null" && $intervalo > 0) {
                $periodos = [];
                $cur = Conexao::conect("set dateformat dmy; select DATEPART(weekday," . valoresData2($data) . ") dia_semana,
                    faixa1_ini, faixa1_fim, dia_semana1, faixa2_ini, faixa2_fim, dia_semana2, 
                    faixa3_ini, faixa3_fim, dia_semana3, faixa4_ini, faixa4_fim, dia_semana4, 
                    faixa5_ini, faixa5_fim, dia_semana5 from sf_agenda 
                    inner join sf_turnos on id_turno = cod_turno where id_agenda = " . $id_agenda);
                while ($RFP = odbc_fetch_array($cur)) {
                    if (valoresHora2($RFP["faixa1_ini"]) != "null" && valoresHora2($RFP["faixa1_fim"]) != "null" && $RFP["dia_semana1"] && substr($RFP["dia_semana1"], $RFP["dia_semana"] - 1, 1) == 1) {
                        $periodos[] = [$RFP["faixa1_ini"], $RFP["faixa1_fim"]];
                    }
                    if (valoresHora2($RFP["faixa2_ini"]) != "null" && valoresHora2($RFP["faixa2_fim"]) != "null" && $RFP["dia_semana2"] && substr($RFP["dia_semana2"], $RFP["dia_semana"] - 1, 1) == 1) {
                        $periodos[] = [$RFP["faixa2_ini"], $RFP["faixa2_fim"]];
                    }
                    if (valoresHora2($RFP["faixa3_ini"]) != "null" && valoresHora2($RFP["faixa3_fim"]) != "null" && $RFP["dia_semana3"] && substr($RFP["dia_semana3"], $RFP["dia_semana"] - 1, 1) == 1) {
                        $periodos[] = [$RFP["faixa3_ini"], $RFP["faixa3_fim"]];
                    }
                    if (valoresHora2($RFP["faixa4_ini"]) != "null" && valoresHora2($RFP["faixa4_fim"]) != "null" && $RFP["dia_semana4"] && substr($RFP["dia_semana4"], $RFP["dia_semana"] - 1, 1) == 1) {
                        $periodos[] = [$RFP["faixa4_ini"], $RFP["faixa4_fim"]];
                    }
                    if (valoresHora2($RFP["faixa5_ini"]) != "null" && valoresHora2($RFP["faixa5_fim"]) != "null" && $RFP["dia_semana5"] && substr($RFP["dia_semana5"], $RFP["dia_semana"] - 1, 1) == 1) {
                        $periodos[] = [$RFP["faixa5_ini"], $RFP["faixa5_fim"]];
                    }
                }
                $agendamentos = $this->getListaAgendamentos($id_agenda, $data);
                foreach ($periodos as $value) {
                    $dt_ini = strtotime(str_replace("/", "-", $data) . " " . $value[0]);
                    $total = ((strtotime(str_replace("/", "-", $data) . " " . $value[1]) - $dt_ini) / ($intervalo * 60));
                    for ($i = 0; $i < $total; $i++) {
                        $data_ini = ($dt_ini + ($intervalo * 60 * $i));
                        $data_fim = (($dt_ini - 1) + ($intervalo * 60 * ($i + 1)));
                        $now = (new DateTime('now', new DateTimeZone('America/Bahia')))->format('Y-m-d H:i:s');
                        if(strtotime($now) <= $data_ini ) {
                            $horarios[] = [
                                "hora_ini"  => date('H:i', $data_ini),
                                "hora_fim"  => date('H:i', $data_fim),
                                "inativo"   => $this->checkFreeDate($agendamentos, $dados, $data_ini, $data_fim)
                            ];
                        }
                    }
                }
            }
            return $horarios;
        }catch (Exception $e) {
            throw  new Exception($e->getMessage());
        }

    }

    /**
     * Pega a configuração da Agenda
     * @return array
     * @throws Exception
     */
    public function getConfigAgenda() {
        try {
            $dados = [];
            $cur = Conexao::conect( "select adm_agen_umamar, adm_agen_hmarc, adm_agen_semanal, 
                    adm_agen_intervalo, adm_num_agendamento from sf_configuracao");
            while ($RFP = odbc_fetch_array($cur)) {
                $dados['intervalo']             = $RFP["adm_agen_intervalo"] ? $RFP['adm_agen_intervalo'] : 0;
                $dados['adm_agen_umamar']       = $RFP['adm_agen_umamar'] ? $RFP['adm_agen_umamar'] : 0;
                $dados['adm_agen_semanal']      = $RFP['adm_agen_semanal'] ? $RFP['adm_agen_semanal'] : 0;
                $dados['adm_agen_hmarc']        = $RFP['adm_agen_hmarc'] ? $RFP['adm_agen_hmarc'] : 0;
                $dados['adm_num_agendamento']   = $RFP['adm_num_agendamento'] ? $RFP['adm_num_agendamento'] : 0;
            }
            return $dados;

        }catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Salvar Agendamento
     * @param $data
     * @return $this
     * @throws Exception
     */
    public function salvarAgendamento($data) {
        try {
            if ($data['id_agendamento']) {
                $sql = " UPDATE sf_agendamento SET assunto = " .valoresTexto2($data['assunto']).", 
                obs = ".valoresTexto2($data['obs']). " WHERE id_agendamento = " .valoresSelect2($data['id_agendamento']);
                Conexao::conect($sql);
                if ($data['id_usuario']) {
                    salvarLog(['tabela' => 'sf_agendamento', 'id_item' => $data['id_agendamento'],
                    'usuario' => getLoginUser(), 'acao' => 'A', 
                    'descricao' => 'ALTERACAO AGENDAMENTO '.$data['id_agendamento'],
                    'id_fornecedores_despesas' => $data['id_usuario']]);
                }
            } else {
                $sql = "set dateformat dmy; INSERT INTO sf_agendamento(assunto, obs, data_inicio, data_fim, id_fornecedores,
                agendado_por, id_agenda, data_agendamento, inativo, concluido, id_pessoa_externo, loja_externo) 
                VALUES (".valoresTexto2($data['assunto']).", ".valoresTexto2($data['obs']).",
                ".valoresDataHora2($data['data_inicio'], $data['hora_inicio']).", ".valoresDataHora2($data['data_fim'], $data['hora_fim']).",
                ".valoresNumericos2($data['id_usuario']).", " . valoresTexto2(getLoginUser()) . ", ".valoresNumericos2($data['id_agenda']).", getdate(), 0, 0, 
                ".verificaValorCampo($data, 'id_usuario_externo').", ".verificaValorCampo($data, 'loja')."); 
                SELECT SCOPE_IDENTITY() id;";
                $res = Conexao::conect($sql);
                odbc_next_result($res);
                $data['id_agendamento'] = odbc_result($res, 1);
                if ($data['id_usuario']) {
                    salvarLog(['tabela' => 'sf_agendamento', 'id_item' => $data['id_agendamento'],
                    'usuario' => getLoginUser(), 'acao' => 'I', 
                    'descricao' => 'INCLUSAO AGENDAMENTO de '.$data['data_inicio'].":".$data['hora_inicio']
                    ." - ".$data['data_fim'].":".$data['hora_fim'],
                    'id_fornecedores_despesas' => $data['id_usuario']]);
                }
            }
               return $data['id_agendamento'];
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }


    public function salvarAgendamentoExterno($data) {
        try {
            $query = "select top 1 * from sf_agendamento_externo where id_agendamento = ".valoresSelect2($data['id_agendamento']);
            $res = Conexao::conect($query);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados = $row;
            }
            if (!$dados) {
                $query = "insert into sf_agendamento_externo(id_pessoa, id_agendamento, loja) 
                values(".valoresSelect2($data['id_usuario_externo']).", "
                .valoresSelect2($data['id_agendamento']).", ".$data['numero_contrato']."); 
                SELECT SCOPE_IDENTITY() id;";
                $res = Conexao::conect($query);
                odbc_next_result($res);
                $data['id_agendamento_externo'] = odbc_result($res, 1);
                salvarLog(['tabela' => 'sf_agendamento_externo', 'id_item' => $data['id_agendamento_externo'],
                    'usuario' => getLoginUser(), 'acao' => 'I', 
                    'descricao' => 'INCLUSAO AGENDAMENTO EXTERNO ('.$data['numero_contrato'].') - '
                    .$data['id_agendamento'].' de '.$data['data_inicio'].":".$data['hora_inicio']
                    ." - ".$data['data_fim'].":".$data['hora_fim'],
                    'id_fornecedores_despesas' => $data['id_usuario']]);
            } 

        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
    
    /**
     * Salvar Link Agendamento
     * @param $data
     * @return $this
     * @throws Exception
     */
    public function salvarLinkAgendamento($data) {
        try {              
            $id_servico = "";            
            $id_agendamento = $data['id_agendamento'];
            $link = $this->getLinkAgendamento($id_agendamento);
            
            $query = "select a.id_servico from sf_agendamento ag
            inner join sf_agenda a on ag.id_agenda = a.id_agenda inner join sf_produtos p on p.conta_produto = a.id_servico 
            where id_agendamento = " . valoresSelect2($id_agendamento);
            $res = Conexao::conect($query);
            while ($row = odbc_fetch_array($res)) {
                $id_servico = $row['id_servico'];
            }
            
            if (is_numeric($id_agendamento) && is_numeric($id_servico) && count($link) == 0) {
                $sql = "insert into sf_link_online (id_pessoa, data_criacao, syslogin, tipo_documento, data_parcela, max_parcela, inativo)
                select id_fornecedores, getdate(), 'SISTEMA', (select link_forma_pagamento from sf_configuracao where id = 1), 
                cast(getdate() as date), (select link_max_parcelas from sf_configuracao where id = 1),2
                from sf_agendamento where id_agendamento = " . $id_agendamento . "; 
                SELECT SCOPE_IDENTITY() id;";
                $res = Conexao::conect($sql);
                odbc_next_result($res);
                $id_link = odbc_result($res, 1);
                
                $query = "insert into sf_link_online_itens (id_link, id_mens, id_produto, quantidade, valor_total, valor_bruto, valor_multa, id_agendamento)
                select " . $id_link . ", null, conta_produto, 1.000, preco_venda, preco_venda, 0.00, id_agendamento from sf_agendamento ag
                inner join sf_agenda a on ag.id_agenda = a.id_agenda inner join sf_produtos p on p.conta_produto = a.id_servico 
                where id_agendamento = " . $id_agendamento;
                Conexao::conect($query);
                
                $query = "update sf_link_online set valor = (select sum(valor_bruto) from sf_link_online_itens bb where bb.id_link = sf_link_online.id_link),
                data_parcela = (SELECT cast(case when min(dt_inicio_mens) > dateadd(day,2, getdate()) then min(dt_inicio_mens) else dateadd(day,2, getdate()) end as date) bol_vencimento 
                FROM sf_link_online_itens bb inner join sf_vendas_planos_mensalidade mm on bb.id_mens = mm.id_mens where bb.id_link = sf_link_online.id_link), inativo = 0 
                where id_link = " . $id_link;
                Conexao::conect($query);

                $query_log = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, id_fornecedores_despesas)
                select 'sf_link_online', id_link, 'SISTEMA', 'I', 'CADASTRO LINK: ' + cast(id_link as varchar), GETDATE(), id_pessoa 
                from sf_link_online where id_link = " . $id_link;
                Conexao::conect($query_log);
                
                $link = $this->getLinkAgendamento($id_agendamento);
            }
            return $link;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Verifica se a data esta livre
     * @param $agendamentos
     * @param $config
     * @param $dt_ini
     * @param $dt_fim
     * @return int
     */
    private function checkFreeDate($agendamentos, $config, $dt_ini, $dt_fim) {
        foreach ($agendamentos as $value) {
            if ((($dt_ini >= $value[0] && $dt_ini <= $value[1]) || ($dt_fim >= $value[0] && $dt_fim <= $value[1]))
                && $value[2] >= $config['adm_num_agendamento']) {
                return 1;
            }
        }
        return 0;
    }

    public function getIdsAgendamentoExternos($condicao) {
        try {
            $query = "SELECT STUFF((SELECT distinct ', ' +cast(id_agendamento as varchar) FROM sf_agendamento_externo WHERE ".$condicao
            ." FOR XML PATH('')), 1, 1, '') as ids_agendamento;";
            $res = Conexao::conect($query);
            $ids = "";
            while ($row = odbc_fetch_array($res)) {
                $ids = $row['ids_agendamento'];
            }
            return $ids;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    } 

    /**
     * Verifica o agendamento diário
     * @param $data
     * @return bool
     * @throws Exception
     */
    public function verificaAgendamentoDiario($data) {
        try {
            $retorno = false;
            $query = "set dateformat dmy; select count(id_agendamento) total from sf_agendamento where inativo = 0 
            and id_fornecedores = " . valoresSelect2($data['id_usuario']) .
            " and cast(data_inicio as date) = " . valoresData2($data['data_inicio']);
            if (isset($data['id_usuario_externo']) && isset($data['loja'])) {
                $query .= " and id_pessoa_externo = ".valoresSelect2($data['id_usuario_externo'])." and loja_externo = ".valoresSelect2($data['loja']);
            }
            if (isset($data['id_agendamento']) && is_numeric($data['id_agendamento'])) {
                $query .= " and id_agendamento not in (" . $data['id_agendamento'] . ")";
            }
            $cur = Conexao::conect($query);
            while ($RFP = odbc_fetch_array($cur)) {
                if ($RFP['total'] > 0) {
                    $retorno = true;
                }
            }
            return $retorno;
        }catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Verifica o agendamento por hora marcada
     * @param $data
     * @return bool
     * @throws Exception
     */
    public function verificaAgendamentoHoraMarcado($data) {
        try {
            $retorno = false;
            $query = "set dateformat dmy; select count(id_agendamento) total from sf_agendamento 
            where inativo = 0 and id_agenda = " . valoresNumericos2($data['id_agenda']) .
                " and ((" . valoresDataHora2($data['data_inicio'], $data['hora_inicio']) . " between data_inicio and data_fim) or
                (" . valoresDataHora2($data['data_fim'], $data['hora_fim']) . " between data_inicio and data_fim))";
            if (isset($data['id_usuario_externo']) && isset($data['loja'])) {
                $query .= " and id_pessoa_externo = ".valoresSelect2($data['id_usuario_externo'])." and loja_externo = ".valoresSelect2($data['loja']);
            }    
            if (isset($data['id_agendamento']) && is_numeric($data['id_agendamento'])) {
                $query .= " and id_agendamento not in (" .$data['id_agendamento'] . ")";
            }
            $query .= "";
            $cur = Conexao::conect($query);
            while ($RFP = odbc_fetch_array($cur)) {
                if ($RFP['total'] >= $data['adm_num_agendamento']) {
                   $retorno = true;
                }
            }
            return $retorno;

        } catch (Exception $e) {
            throw  new Exception($e->getMessage());
        }
    }


    /**
     * Verifica o cadastro semanal
     * @param $data
     * @return bool
     * @throws Exception
     */
    public function verificaCadastroSemanal($data) {
        try {
            $retorno = false;
            $query = "set dateformat dmy; select count(id_agendamento) total from sf_agendamento 
                where inativo = 0 and id_fornecedores = " . valoresNumericos2($data['id_usuario']) .
                " and DATEPART(week, data_inicio) = DATEPART(week, " . valoresData2($data['data_inicio']) . ")";
            if (isset($data['id_usuario_externo']) && isset($data['loja'])) {
                $query .= " and id_pessoa_externo = ".valoresSelect2($data['id_usuario_externo'])." and loja_externo = ".valoresSelect2($data['loja']);
            }    
            if (isset($data['id_agendamento']) && is_numeric($data['id_agendamento'])) {
                $query .= " and id_agendamento not in (" . $data['id_agendamento'] . ")";
            }
            $cur = Conexao::conect($query);
            while ($RFP = odbc_fetch_array($cur)) {
                if ($RFP['total'] >= $data['adm_agen_semanal']) {
                    $retorno = true;
                }
            }
            return $retorno;
        }catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

    }

    /**
     * Verifica plano ativo do usuário
     * @param $id_usuario
     * @return bool
     * @throws Exception
     */
    public function verificaPlanoAtivo($id_usuario) {
        try {
            $retorno = false;
            $sql = "select 
                     case when fdc.id_fornecedores_despesas_credencial is not null and (fdc.dt_cancelamento is null and getdate() between fdc.dt_inicio and fdc.dt_fim) then 10 
                     when vp.id_plano is not null and (vp.dt_cancelamento is null and (select count(id_mens) from sf_vendas_planos_mensalidade where dt_pagamento_mens is not null and id_plano_mens = id_plano) > 0) 
                     then datediff(day, getdate(), dateadd(day, (select ACA_PLN_DIAS_TOLERANCIA from sf_configuracao), max(dbo.FU_PLANO_FIM(id_plano))))
                     else 0 end as dias
                    from sf_fornecedores_despesas fd
                    left join sf_vendas_planos vp on vp.favorecido = fd.id_fornecedores_despesas 
                    left join sf_fornecedores_despesas_credenciais fdc on fdc.id_fornecedores_despesas = fd.id_fornecedores_despesas
                    where fd.id_fornecedores_despesas = ".valoresSelect2($id_usuario)." 
                    GROUP BY fdc.id_fornecedores_despesas_credencial, vp.id_plano, vp.dt_cancelamento, fdc.dt_cancelamento, fdc.dt_inicio, fdc.dt_fim";
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                if(!$retorno) {
                    if ($row['dias'] <= 0) {
                       $retorno = false;
                    }else {
                        $retorno = true;
                    }
                }
            }
            return $retorno;
        }catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Cancela o Agendamento
     * @param $id_agendamento
     * @return bool
     * @throws Exception
     */
    public function cancelarAgendamento($data) {
        try {
            $query = "" .
                "UPDATE sf_agendamento SET inativo = 1" .
                "WHERE id_agendamento = " . $data['id_agendamento'];
            Conexao::conect($query);
            if ($data['id_usuario']) {
                salvarLog(['tabela' => 'sf_agendamento', 'id_item' => $data['id_agendamento'],
                'usuario' => getLoginUser(), 'acao' => 'C', 'descricao' => 'CANCELAMENTO - AGENDAMENTO',
                'id_fornecedores_despesas' => $data['id_usuario']]);
            }
            return true;
        }catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Deleta o Agendamento
     * @param $id_agendamento
     * @return bool
     * @throws Exception
     */
    public function excluirAgendamento($data) {
        try {
            $query = "DELETE FROM sf_agendamento WHERE id_agendamento = " .valoresSelect2($data['id_agendamento']);
            Conexao::conect($query);
            salvarLog(['tabela' => 'sf_agendamento', 'id_item' => $data['id_agendamento'],
            'usuario' => getLoginUser(), 'acao' => 'C', 'descricao' => 'EXCLUIR - AGENDAMENTO',
            'id_fornecedores_despesas' => $data['id_usuario']]);
            return true;
        }catch (Exception $e) {
            throw  new Exception($e->getMessage());
        }
    }

    /**
     * Lista o Agendamento
     * @param $id_agenda
     * @param $data_inicio
     * @param $qtd
     * @param $limit
     * @param string $query
     * @return array
     * @throws Exception
     */
    public function getListaAgendamento($data, $qtd, $limit, $query = '') {
        try {
            $dados = [];
            $sql = "set dateformat dmy; select * FROM (SELECT ROW_NUMBER() OVER (ORDER BY data_inicio asc) as row,  
                    * from (".$this->queryBaseAgendamento($data).") as x ".$query.")
                    as a where row BETWEEN " . $limit . " AND " . ($limit + $qtd).";";
            //echo $sql; exit;  
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados[] = [
                    'id_agendamento'    => $row['id_agendamento'],
                    'data_inicio'       => escreverDataHora($row['data_inicio']),
                    'data_fim'          => escreverDataHora($row['data_fim']),
                    'assunto'           => escreverTexto($row['assunto']),
                    'obs'               => escreverTexto($row['obs']),
                    'data_agendamento'  => escreverDataHora($row['data_agendamento']),
                    'concluido'         => boolval($row['concluido']),
                    'inativo'           => boolval($row['inativo']),
                    'dia'               => dataParaDiaSemana($row['data_inicio']),
                    'status'            => escreverTexto($row['status'])
                ];
            }
            for ($i = 0; $i < count($dados); $i++) {
                $dados[$i]['link'] = $this->getLinkAgendamento($dados[$i]['id_agendamento']);
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    /**
     * Link o Agendamento
     * @param $id_agendamento
     * @return array
     * @throws Exception
     */
    public function getLinkAgendamento($id_agendamento) {
        try {
            $dados = [];
            $sql = "select l.id_link, id_venda, data_parcela,
            isnull(valor, (select sum(valor_bruto) total from sf_link_online_itens bi where bi.id_link = l.id_link)) bol_valor
            from sf_link_online_itens li inner join sf_link_online l on l.id_link = li.id_link
            where li.id_agendamento = " . valoresSelect2($id_agendamento) . " and l.inativo = 0 order by l.id_link desc";
            //echo $sql; exit;  
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $link = encrypt((escreverDataHora($row["data_parcela"]) . "|" . $row["id_link"] . "|" . CONTRATO . "|" . escreverNumero($row["bol_valor"])), "VipService123", true);
                $dados[] = [
                    'id_link'     => $row["id_link"],
                    'link_link'   => ERP_URL . "Modulos/Financeiro/frmLinkPagamento.php?link=" . $link,
                    'id_venda'    => $row['id_venda'],
                ];
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Lista os totais da lista de Agendamento
     * @param $id_agenda
     * @param $id_usuario
     * @param $data_inicio
     * @return array
     * @throws Exception
     */
    public function getTotalListaAgendamento($data, $status) {
        try {
            $dados = [];
            $sql = "SELECT COUNT(CASE WHEN status in (".implode(',', $status).") THEN id_agendamento ELSE null END) AS total,
                    COUNT(CASE WHEN status = 'ativo' THEN id_agendamento ELSE null END) AS ativo,
                    COUNT(CASE WHEN status = 'inativo' THEN id_agendamento ELSE null END) AS inativo
                    FROM (".$this->queryBaseAgendamento($data).") as x ";
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados = array_map(function($i) {
                    return intval($i);
                },$row);
            }
            return $dados;
        }catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Query Base para Agendamento
     * @param $id_agenda
     * @param $id_usuario
     * @param $data_inicio
     * @return string
     */
    private function queryBaseAgendamento($data) {
        $condicao = isset($data['id_usuario_externo']) && isset($data['loja']) 
        ? ' and id_pessoa_externo = '.valoresSelect2($data['id_usuario_externo']).' and loja_externo = '.valoresSelect2($data['loja'])  : '';
        return "select *,
            CASE WHEN (((data_inicio < GETDATE() AND data_fim >= GETDATE()) OR (data_inicio >= GETDATE())) AND inativo = 0) THEN 'ativo' ELSE 'inativo' END as status
            from sf_agendamento 
            where id_agenda = ".valoresSelect2($data['id_agenda'])."
            and id_fornecedores = ".valoresSelect2($data['id_usuario'])." 
            and inativo = 0 ".(isset($data['ids'])  ? ' and id_agendamento in ('.$data['ids'].') ': '')."
            and datepart(month, data_inicio) = datepart(month, ".valoresData2($data['data_inicio']).")
            and datepart(year, data_inicio) = datepart(year, ".valoresData2($data['data_inicio']).")".$condicao;
    }


    /**
     * Response de agenda 
     * @param array $row
     * @return array
     */
    private function responseAgenda($row) {
        return [
            'id_agenda'             => $row['id_agenda'],
            'descricao_agenda'      => escreverTexto($row['descricao_agenda']),
            'inativo'               => $row['inativo'],
            'user_resp'             => $row['user_resp'],
            'id_turno'              => $row['id_turno'],
            'terminal'              => $row['terminal'],
            'dias'                  => $row['dias']    
        ];
    }

}