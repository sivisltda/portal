<?php
class DependenteRepository {

    /**
     * Lista os dados dos dependentes
     * @param $condicao
     * @return array $dados
     * @throws Exception
     */
    public function listaDependentes($condicao) {
        try {
           $dados = [];
           $where = $condicao ? ' WHERE '.$condicao : '';
           $query = "SELECT ".$this->sqlBase($where);
           $rs = Conexao::conect($query);
           while ($row = odbc_fetch_array($rs)) {
                $dados[] = $this->responseDependente($row);
           }
           return $dados;           
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Traz o dependente com acordo com a condição
     * @param $condicao
     * @return array $dados
     * @throws Exception
     */
    public function getDependente($condicao) {
        try {
            $dados = [];
            $where = $condicao ? ' WHERE '.$condicao : '';
            $query = "SELECT TOP 1 ".$this->sqlBase($where);
            $rs = Conexao::conect($query);
            while ($row = odbc_fetch_array($rs)) {
                $dados = $this->responseDependente($row);
           }
           return $dados; 
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Verifica se existe dependente
     * @param string $id_usuario
     * @return bool
     * @throws Exception
     */
    public function getHasDependente($id_usuario) {
        try {
            $sql = "select count(*) as total from sf_vendas_planos vp
            inner join sf_produtos p on p.conta_produto = vp.id_prod_plano
            left join sf_produtos_acrescimo pa on pa.id_produto = p.conta_produto
            left join sf_fornecedores_despesas_dependentes dep on dep.id_titular = vp.favorecido
            where (pa.id is not null or dep.id_titular is not null) and favorecido = ".valoresSelect2($id_usuario);
            $rs = Conexao::conect($sql);
            $total = odbc_result($rs, 1);
            return $total > 0;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getDependenteIsPayment($id_usuario) {
        try {
            $sql = "select count(*) as total from sf_vendas_planos vp
            inner join sf_produtos p on p.conta_produto = vp.id_prod_plano
            inner join sf_produtos_acrescimo pa on pa.id_produto = p.conta_produto and pa.valor > 0
            inner join sf_fornecedores_despesas_dependentes dep on dep.id_titular = vp.favorecido 
            where (pa.id is not null or dep.id_titular is not null) and (select top 1 aca_adesao_dep from sf_configuracao) = 1
            and favorecido = ".valoresSelect2($id_usuario);
            $rs = Conexao::conect($sql);
            $total = odbc_result($rs, 1);
            return $total > 0;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Remove o dependente 
     * @param array $data
     * @throws Exception
     */
    public function removeDependente($data) {
        try {
            $sql = "delete sf_fornecedores_despesas_dependentes where id_titular = ".valoresSelect2($data['id_titular'])." and id_dependente = ".valoresSelect2($data['id_dependente']);
            Conexao::conect($sql);  
        }catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Verifica se a pessoa já é dependente
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function verificaDependente($data) {
        try {
            $sql = "SELECT TOP 1 * FROM sf_fornecedores_despesas_dependentes WHERE id_titular = ".valoresSelect2($data['id_titular'])
            ." AND id_dependente = ".valoresSelect2($data['id_dependente']);
            $rs = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($rs)) {
                $dados = $row;
            }
            return $dados;
        }catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Lista os dados de parentescos
     * @return array $dados
     * @throws Exception
     */
    public function listaParentescos() {
        try {
            $sql = "SELECT * FROM sf_parentesco ORDER BY nome_parentesco";
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados[$row['id_parentesco']] = escreverTexto($row['nome_parentesco']);
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Regras de dependencias referentes ao produto
     * @param string $condicao
     * @return array
     * @throws Exception
     */
    public function getRegrasDependentePlano($condicao) {
        try {
            $dados = [];
            $sql = "select *, (select top 1 nome_parentesco from sf_parentesco where id_parentesco = pc.id_parentesco)
             nome_parentesco from sf_produtos_acrescimo pc where ".$condicao.";";
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $data = $row;
                $data['valor']              = escreverNumero($row['valor'], 1);
                $data['nome_parentesco']    = escreverTexto($row['nome_parentesco']);
                $dados[] = $data;        
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }      
    }

    /**
     * Remove o usuário dependente do plano
     * @param array $data
     * @throws Exception
     */
    public function verificaRemoveUsuarioDependentePlano($data) {
        try {
            $sql = "DELETE FROM sf_fornecedores_despesas_dependentes
            WHERE id_titular = ".valoresSelect2($data['id_usuario'])."
            AND id_dependente in (select id_dependente from sf_fornecedores_despesas_dependentes dd
            inner join sf_vendas_planos vp on vp.favorecido = dd.id_titular
            where favorecido = ".valoresSelect2($data['id_usuario'])." and id_plano = ".valoresSelect2($data['id_plano'])." and planos_status in ('Novo'));";
            Conexao::conect($sql);               
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Query Base
     * @param string $where
     * @return string
     */
    private function sqlBase($where) {
        return "fdd.*, pt.nome_parentesco, fd.razao_social as nome, fd.cnpj, fd.inscricao_estadual, fd.endereco, fd.bairro, fd.cep, fd.complemento, fd.numero, fd.sexo, fd.estado_civil,
        fd.data_nascimento, fd.cidade, fd.estado, c.cidade_nome, e.estado_nome, fd.tipo, fd.pri_cnh,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = id_fornecedores_despesas AND tipo_contato = 2) as email,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = id_fornecedores_despesas AND tipo_contato = 1) as celular,
        (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = id_fornecedores_despesas AND tipo_contato = 0) as telefone
        FROM sf_fornecedores_despesas_dependentes fdd 
        INNER JOIN sf_fornecedores_despesas fd ON fd.id_fornecedores_despesas = fdd.id_dependente
        LEFT JOIN tb_cidades c ON c.cidade_codigo = fd.cidade
        LEFT JOIN tb_estados e ON  e.estado_codigo = fd.estado
        LEFT JOIN sf_parentesco pt ON pt.id_parentesco = fdd.parentesco ".$where;
    }

    /**
     * Salvar o dependente
     * @param array $data
     * @throws Exception
     */
    public function saveDependente($data) {
        try {
            if ($this->verificaDependente($data)) {
                $sql = "update sf_fornecedores_despesas_dependentes set parentesco = ".valoresSelect2(isset($data['parentesco']) ? $data['parentesco'] : '')."
                where id_titular = ".valoresSelect2($data['id_titular'])." AND id_dependente = ".valoresSelect2($data['id_dependente']);
            } else {
                $sql = "INSERT INTO sf_fornecedores_despesas_dependentes (id_titular,id_dependente,data_inclusao,compartilhado,parentesco) 
                values (".valoresSelect2($data['id_titular']).", ".valoresSelect2($data['id_dependente']).", getdate(), 1, "
                .valoresSelect2(isset($data['parentesco']) ? $data['parentesco'] : '').");";
                salvarLog(['tabela' => 'sf_fornecedores_despesas_dependentes', 'id_item' => $data['id_dependente'],
                'usuario' => getLoginUser(), 'acao' => 'I', 'descricao' => 'INCLUSAO DEPENDENTE:'.$data['id_dependente'],
                'id_fornecedores_despesas' => $data['id_titular']]);
            }
            Conexao::conect($sql);
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Response do dependente
     * @param array $row
     * @return array 
     */
    private function responseDependente($row) {
        $data                       = $row;
        $data['nome_parentesco']    = escreverTexto($row['nome_parentesco']);
        $data['nome']               = escreverTexto($row['nome']);
        $data['rg']                 = escreverTexto($row['inscricao_estadual']);
        $data['data_nascimento']    = escreverData($row['data_nascimento']);
        $data['data_inclusao']      = escreverData($row['data_inclusao']);
        $data['pri_cnh']            = escreverData($row['pri_cnh']);
        $data['estado_civil']       = escreverTexto($row['estado_civil']);
        $data['endereco']           = escreverTexto($row['endereco']);
        $data['bairro']             = escreverTexto($row['bairro']);
        $data['estado_nome']        = escreverTexto($row['estado_nome']);
        $data['cidade_nome']        = escreverTexto($row['cidade_nome']);
        $data['id_cidade']          = $row['cidade'];
        $data['id_estado']          = $row['estado'];
        $data['status']             = $row['tipo'] === 'P' ? 'Pendente' : 'Ativo';
        return $data;
    }

}
