<?php

class FuncionarioRepository {

    /**
     * Busca o nome do Funcionário
     * @param $codigo
     * @return array
     * @throws Exception
     */
    public function getNomeConsultorPorCodigo($codigo, $porUsuario = false) {
        try {
            $where = $porUsuario ? "id_usuario = " . valoresSelect2($codigo) : "id_fornecedores_despesas = " . valoresSelect2($codigo);
            $sql = "select top 1 id_usuario, razao_social, id_fornecedores_despesas, c.empresa as filial,
            (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = id_fornecedores_despesas AND tipo_contato = 1) as celular,
            (select max(isnull(fin_desc_plano_tot, 0)) from sf_usuarios_permissoes where fin_desc_plano = 1 and id_permissoes = master) max_desc_pri,
            (select max(isnull(valor, 0)) from sf_comissao_cliente_padrao_item i where tipo = 0 and id_usuario = 0 and tipo_valor = 0 and i.id_comissao_padrao = u.id_comissao_padrao) max_comis_pri
            from sf_fornecedores_despesas c
            left join sf_usuarios u on u.funcionario = c.id_fornecedores_despesas
            left join sf_usuarios_filiais uf on uf.id_usuario_f = u.id_usuario
            where tipo in ('I','E') AND (fornecedores_status != 'Excluido' or fornecedores_status is null) 
            and c.inativo = 0 and dt_demissao is null and " . $where;
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados['id'] = $row['id_fornecedores_despesas'];
                $dados['nome'] = escreverTexto($row['razao_social']);
                $dados['id_usuario'] = $row['id_usuario'];
                $dados['celular'] = escreverTexto($row['celular']);
                $dados['indicador'] = escreverTexto($row['id_fornecedores_despesas']);
                $dados['filial'] = $row['filial'];
                $dados['max_desc_pri'] = $row['max_desc_pri'];
                $dados['max_comis_pri'] = $row['max_comis_pri'];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Lista de Indicadores
     * @return array $dados
     * @throws Exception
     */
    public function getIndicadores() {
        try {
            $sql = "select id_fornecedores_despesas, empresa, razao_social, cnpj, 
            (select top 1 cidade_nome from tb_cidades where cidade_codigo = cidade) as cidade_nome,
            (select top 1 estado_nome from tb_estados where estado_codigo = estado) as estado_nome
            from sf_fornecedores_despesas where tipo = 'I' and fornecedores_status not in ('Excluido') order by razao_social asc;";
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados[] = [
                    'id' => $row['id_fornecedores_despesas'],
                    'razao_social' => escreverTexto($row['razao_social']),
                    'cnpj' => $row['cnpj'],
                    'empresa' => $row['empresa'],
                    'cnpj' => $row['cnpj'],
                    'cidade' => escreverTexto($row['cidade_nome']),
                    'estado' => escreverTexto($row['estado_nome'])
                ];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Comissao do Funcionário por tipo
     * @param array $dadosFuncionario
     * @param string $where
     * @return array $comissoes
     * @throws Exception
     * @internal param bool $mesAtual
     */
    /*public function comissaoFuncionarioPorTipo($whereComissao) {
        try {
            $condicao = $whereComissao ? " WHERE " . $whereComissao : "";
            $sql = " 
            select nome_plano, count(id_venda) as quantidade, 
            sum(case when comissao_plano_pri > 0 then comissao_plano_pri else 0 end) as comissao_plano_pri,
            sum(case when comissao_plano > 0 then comissao_plano else 0 end) as comissao_plano,
            sum(case when comissao_produto > 0 then comissao_produto else 0 end) as comissao_produto,
            sum(case when comissao_servico > 0 then comissao_servico else 0 end) as comissao_servico,
            sum(comissao) as comissao
            from #tempComissionamento" . $condicao . " group by nome_plano;";
            $comissoes = [];
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $data['descricao'] = escreverTexto($row['nome_plano']);
                $data['quantidade'] = $row['quantidade'];
                $data['comissao'] = escreverNumero($row['comissao'], 1);
                $data['comissao_plano_pri'] = escreverNumero($row['comissao_plano_pri'], 1);
                $data['comissao_plano'] = escreverNumero($row['comissao_plano'], 1);
                $data['comissao_produto'] = escreverNumero($row['comissao_produto'], 1);
                $data['comissao_servico'] = escreverNumero($row['comissao_servico'], 1);
                $comissoes[] = $data;
            }
            return $comissoes;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }*/

    /**
     * Total de Comissão
     * @param array $dadosFuncionario
     * @param string $where
     * @return array $dados
     * @throws Exception
     */
    /*public function totalComissao($whereComissao) {
        try {
            $condicao = $whereComissao ? " WHERE " . $whereComissao : "";
            $sql = " select sum(quantidade) as quantidade, sum(valor_total) as valor_total, 
            sum(pendente) as pendente, sum(pago) as pago, sum(reprovado) as reprovado 
            from (select count(id_venda) as quantidade,
            sum(comissao) as valor_total,
            case when status is null or status = 'Pendente' 
            then sum(comissao) else 0 end as pendente,
            case when status = 'Aprovado' then sum(comissao) else 0 end as pago,
            case when status = 'Reprovado' then sum(comissao) else 0 end as reprovado  
            from #tempComissionamento" . $condicao . " group by status) as x";
            $dados = [];
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $dados['quantidade'] = $row['quantidade'];
                $dados['valor_total'] = escreverNumero($row['valor_total'], 0, 2, '.', '');
                $dados['pendente'] = escreverNumero($row['pendente'], 0, 2, '.', '');
                $dados['pago'] = escreverNumero($row['pago'], 0, 2, '.', '');
                $dados['reprovado'] = escreverNumero($row['reprovado'], 0, 2, '.', '');
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }*/

    /**
     * Retorna uma lista de funcionários
     * @return array
     * @throws Exception
     */
    public function listaFuncionarios() {
        try {
            $sql = "select distinct id_fornecedores_despesas, razao_social from sf_fornecedores_despesas fd 
            left join sf_usuarios u on u.funcionario = fd.id_fornecedores_despesas
            left join sf_departamentos d ON d.id_departamento = u.departamento
            where tipo = 'E' AND fd.inativo = 0;";
            $dados = [];
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $dados[] = [
                    'id' => $row['id_fornecedores_despesas'],
                    'nome' => escreverTexto($row['razao_social'])
                ];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Lista dos planos por Funcionário
     * @param array $dadosFuncionario
     * @return array $dados
     * @throws Exception
     */
    /*public function listaPlanosPorFuncionarioTipo($limit, $qtd, $whereComissao) {
        try {
            $condicao = $whereComissao ? ' WHERE ' . $whereComissao : '';
            $sql = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY data_venda desc) as row, * from
            #tempComissionamento" . $condicao . ") as a where row > " . $limit . " and row <= " . ($limit + $qtd) . " order by data_venda desc;";
            //echo $sql; exit;
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados[] = [
                    'parcela' => ($row["prov"] + 1) . "/" . $row["parcela"],
                    'id_venda' => $row['id_venda'],
                    'data_venda' => escreverDataHora($row['data_venda']),
                    'preco_plano' => escreverNumero($row['preco_plano'], 1),
                    'preco_servico' => escreverNumero($row['preco_servico'], 1),
                    'preco_produto' => escreverNumero($row['preco_produto']),
                    'comissao_plano_pri' => escreverNumero($row['comissao_plano_pri'], 1),
                    'comissao_plano' => escreverNumero($row['comissao_plano'], 1),
                    'comissao_servico' => escreverNumero($row['comissao_servico'], 1),
                    'comissao_produto' => escreverNumero($row['comissao_produto'], 1),
                    'valor_comissao' => escreverNumero($row['comissao'], 1),
                    'nome' => escreverTexto($row['razao_social']),
                    'produto' => escreverTexto($row['nome_plano']),
                    'status' => escreverTexto($row['status'])
                ];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }*/

    /**
     * Lista de Clientes
     * @param $query
     * @param $limit
     * @param $qtd
     * @return array
     * @throws Exception
     */
    public function listaClientes($query, $limit, $qtd) {
        try {
            Conexao::conect("set dateformat dmy;");
            $condicao = $query ? 'WHERE ' . $query : '';
            $sql = "SELECT * FROM (
               SELECT ROW_NUMBER() OVER (ORDER BY razao_social asc) as row,  a.* FROM (" . $this->queryBaseClientes() . ") as a $condicao 
            ) as b where row > " . $limit . " and row <= " . ($limit + $qtd);
            //echo $sql; exit;
            $res = Conexao::conect($sql);
            $list = [];
            while ($row = odbc_fetch_array($res)) {
                $dados = $this->responseCliente($row);
                $list[] = $dados;
            }
            return $list;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getStatusProspectsLeads() {
        try {
            $sql = "select 'pendente' as id, count(id) as qtd, 'Pendente' as status 
            from #tempProspects where status = 'Pendente' 
            union all select 'aguardando' as id, count(id) as qtd, 'Aguardando' as status
            from #tempProspects where status = 'Aguardando'
            union all select 'analise' as id, count(id) as qtd, 'Análise' as status
            from #tempProspects where status = 'Analise'
            union all select 'negociacao' as id, count(id) as qtd, 'Em Negociação' as status
            from #tempProspects where status = 'Negociacao'
            union all select 'orcamento' as id, count(id) as qtd, 'Em Orçamento' as status
            from #tempProspects where status = 'orcamento'
            union all select 'inativo' as id, count(id) as qtd, 'Inativo' as status
            from #tempProspects where status = 'Inativo'
            union all select 'todos' as id, count(id) as qtd, 'Todos' as status 
            from #tempProspects;";
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados[] = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getListaProspectsLeads($condicao, $paginacao = []) {
        try {
            $where = $condicao ? ' WHERE ' . $condicao : '';
            Conexao::conect("drop table if exists #tempProspects;");
            $sqlProspect = "set dateformat dmy; select * into #tempProspects from (" . $this->queryBaseProspectLead() . ") as x" . $where;
            Conexao::conect($sqlProspect);
            $sql = "select * from #tempProspects";
            $sqlTotal = "";
            if (count($paginacao) > 0) {
                $limit = $paginacao['limit'];
                $qtd = $paginacao['qtd'];
                $sqlTotal = "select count(*) as total from (" . $sql . ") as a";
                $sql = "SELECT * FROM (SELECT ROW_NUMBER() OVER (order by dt_cadastro desc) as row, * 
                        FROM (" . $sql . ") as a) as b where row BETWEEN " . ($limit + 1) . "AND " . ($limit + $qtd) . " order by dt_cadastro desc;";
            }
            $query = Conexao::conect($sql);
            $data = [];
            $dados = [];
            while ($rows = odbc_fetch_array($query)) {
                $data[] = $this->getResponseProspectLead($rows);
            }
            if (count($paginacao) > 0) {
                $resTotal = Conexao::conect($sqlTotal);
                $dados['aaData'] = $data;
                $dados['total'] = odbc_result($resTotal, 'total');
                $dados['iTotalDisplayRecords'] = $dados['total'];
                $dados['iTotalRecords'] = $dados['total'];
            } else {
                $dados = $data;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Retorna a quantidade totais de clientes
     * @param $query
     * @return array
     * @throws Exception
     */
    public function totalCliente($query) {
        try {
            $condicao = $query ? 'WHERE ' . $query : '';
            $sql = "set dateformat dmy; select count(id_fornecedores_despesas) as total,
            count(case when tipo = 'C' and status = 'ativo' then id_fornecedores_despesas else null end) as ativo,
            count(case when tipo = 'C' and status = 'suspenso' then id_fornecedores_despesas else null end) as suspenso,
            count(case when tipo = 'C' and status = 'emaberto' then id_fornecedores_despesas else null end) as emaberto,
            count(case when tipo = 'C' and status = 'inativo' then id_fornecedores_despesas else null end) as inativo,
            count(case when tipo = 'P' then id_fornecedores_despesas else null end) as prospect, 
            count(id_dependente) as dependente
            from (" . $this->queryBaseClientes() . ") as a " . $condicao;
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados = array_map(function($i) {
                    return intval($i);
                }, $row);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getCliente($where) {
        try {
            $sql = $this->queryBaseClientes() . " WHERE " . $where;
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados = $this->responseCliente($row);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /*public function criaTabelaComissaoCliente($dadosFuncionario, $where) {
        try {
            Conexao::conect("drop table if exists #tempComissionamento;");
            $comissao_plano_pri = "((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio)) * isnull((select max(valor) 
            from sf_comissao_cliente where id_cliente = c.id_fornecedores_despesas and tipo = 0 and id_usuario = "
                    . valoresNumericos2($dadosFuncionario['id_usuario']) . "),0)/100) / case when pp.parcela < 1 then 1 else pp.parcela end";
            $comissao_plano = "((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio)) * isnull((select max(valor) 
            from sf_comissao_cliente where id_cliente = c.id_fornecedores_despesas and tipo = 1 and id_usuario = "
                    . valoresNumericos2($dadosFuncionario['id_usuario']) . "),0)/100) / case when pp.parcela < 1 then 1 else pp.parcela end";
            $comissao_servico = "((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio)) * isnull((select max(valor) 
            from sf_comissao_cliente where id_cliente = c.id_fornecedores_despesas and tipo = 2 and id_usuario = "
                    . valoresNumericos2($dadosFuncionario['id_usuario']) . "),0)/100)";
            $comissao_produto = "((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio)) * isnull((select max(valor) 
            from sf_comissao_cliente where id_cliente = c.id_fornecedores_despesas and tipo = 3 and id_usuario = "
                    . valoresNumericos2($dadosFuncionario['id_usuario']) . "),0)/100)";
            $whereTipo = " and c.id_fornecedores_despesas in (select id_cliente from sf_comissao_cliente where id_usuario = "
                    . valoresNumericos2($dadosFuncionario['id_usuario']) . ")";
            $sWhere = $whereTipo . $where;

            $sQuery = " ";
            $sQuery .= "select *, (comissao_plano + comissao_plano_pri + comissao_servico + comissao_produto) as comissao 
            into #tempComissionamento 
            from (select id_venda, data_venda, vendedor, razao_social, empresa, nome_plano,
            sum(preco_plano) preco_plano, sum(preco_servico) preco_servico, sum(preco_produto) preco_produto, 
            sum(CONVERT(DECIMAL(10,2), isnull(comissao_plano_pri, 0))) comissao_plano_pri, sum(CONVERT(DECIMAL(10,2), isnull(comissao_plano, 0))) comissao_plano, 
            sum(CONVERT(DECIMAL(10,2), isnull(comissao_servico, 0))) comissao_servico, sum(CONVERT(DECIMAL(10,2), isnull(comissao_produto, 0))) comissao_produto, 
            id_solicitacao_autorizacao, isnull(status_comissao, 'Pendente') status, prov, parcela 
            from (";
            $sQuery .= "select v.id_venda, v.data_venda, c.razao_social, c.empresa,   
            (select top 1 descricao from sf_produtos s where conta_produto = vp.id_prod_plano 
            or conta_produto = vi.produto) as nome_plano, 
            case when p.tipo = 'C' then vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_plano,
            case when p.tipo = 'S' then vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_servico, 
            case when p.tipo = 'P' then vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_produto,
            case when p.tipo = 'C' and dbo.FU_PRIMEIRO_PAGAMENTO(vi.id_item_venda) > 0 then $comissao_plano_pri end comissao_plano_pri, 
            case when p.tipo = 'C' and dbo.FU_PRIMEIRO_PAGAMENTO(vi.id_item_venda) = 0 then $comissao_plano end comissao_plano,   
            case when p.tipo = 'S' and p.valor_comissao_venda > 0 then $comissao_servico else 0 end comissao_servico,
            case when p.tipo = 'P' and p.valor_comissao_venda > 0 then $comissao_produto else 0 end comissao_produto,     
            v.vendedor, v.indicador, c.id_user_resp, c.prof_resp, vi.vendedor_comissao comissionado, 
            0 prov, 1 parcela, sa.id_solicitacao_autorizacao, sa.status as status_comissao
            from sf_vendas v inner join sf_vendas_itens vi on vi.id_venda = v.id_venda
            inner join sf_produtos p on p.conta_produto = vi.produto
            left join sf_vendas_planos_mensalidade m on m.id_item_venda_mens = vi.id_item_venda
            left join sf_vendas_planos vp on vp.id_plano = m.id_plano_mens 
            left join sf_produtos_parcelas pp on pp.id_parcela = m.id_parc_prod_mens
            inner join sf_fornecedores_despesas c on c.id_fornecedores_despesas = v.cliente_venda
            left join sf_filiais fi on fi.id_filial = c.empresa
            left join sf_comissao_historico ch on v.id_venda = ch.hist_venda and hist_parcela = 0 and hist_vendedor = " . valoresNumericos2($dadosFuncionario['id_usuario']) . "
            left join sf_solicitacao_autorizacao sa on sa.id_solicitacao_autorizacao = ch.hist_solicitacao_autorizacao and sa.fornecedor_despesa = " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas']) . "
            where v.cov = 'V' and v.status = 'Aprovado' and p.tipo in ('C', 'P', 'S') " . $sWhere;

            $sQuery .= " union all select distinct #tempProdutividade.*, sa.id_solicitacao_autorizacao, sa.status from #tempProdutividade 
            left join sf_comissao_historico ch on id_venda = ch.hist_venda and hist_parcela = prov and hist_vendedor = " . valoresNumericos2($dadosFuncionario['id_usuario']) . "
            left join sf_solicitacao_autorizacao sa on sa.id_solicitacao_autorizacao = ch.hist_solicitacao_autorizacao and sa.fornecedor_despesa = " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas']) . "    
            where prov > 0 " . $where;

            $sQuery .= ") as x group by id_venda, data_venda, vendedor, razao_social, empresa, nome_plano,
            id_solicitacao_autorizacao, prov, parcela, status_comissao
            having (sum(CONVERT(DECIMAL(10,2), isnull(comissao_produto, 0))) + sum(CONVERT(DECIMAL(10,2), isnull(comissao_servico, 0))) +
            sum(CONVERT(DECIMAL(10,2), isnull(comissao_plano, 0))) + sum(CONVERT(DECIMAL(10,2), isnull(comissao_plano_pri,0)))) > 0 ) as a";
            //echo $sQuery; exit;
            Conexao::conect($sQuery);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getlistaTipoComissao($whereComissao) {
        try {
            $dados = [];
            $where = $whereComissao ? ' WHERE ' . $whereComissao : '';
            $sql = "select 'comissao_plano_pri' as id, count(id_venda) as quantidade, isnull(sum(comissao_plano_pri), 0) as valor, '1ª Parcela' as status
            from #tempComissionamento where comissao_plano_pri > 0
            union all
            select 'comissao_plano' as id, count(id_venda) as quantidade, isnull(sum(comissao_plano), 0) as valor, 'Plano' as status
            from #tempComissionamento where comissao_plano > 0
            union all 
            select 'comissao_servico' as id, count(id_venda) as quantidade, isnull(sum(comissao_servico), 0) as valor, 'Serviço' as status
            from #tempComissionamento where comissao_servico > 0
            union all 
            select 'comissao_produto' as id, count(id_venda) as quantidade, isnull(sum(comissao_produto), 0) as valor, 'Produto' as status
            from #tempComissionamento where comissao_produto > 0
            union all
            select 'comissao' as id, count(id_venda) as quantidade, isnull(sum(comissao), 0) as valor, 'Total' as status
            from #tempComissionamento" . $where . ";";
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $row['valor'] = escreverNumero($row['valor'], 1);
                $dados[] = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function criaTabelaComissao($dadosFuncionario, $where) {
        try {
            Conexao::conect("drop table if exists #tempComissionamento;");

            $whereFuncionario = " and ((f.prof_resp is not null and (f.prof_resp = " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas'])
                    . " or f.prof_resp in (select funcionario from sf_usuarios u 
            inner join sf_usuarios_dependentes ud on ud.id_usuario = u.id_usuario 
            where ud.id_fornecedor_despesas = " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas']) . "))) 
            or (f.id_user_resp is not null and (f.id_user_resp = " . valoresSelect2($dadosFuncionario['id_usuario'])
                    . " or f.id_user_resp in (select ud.id_usuario from sf_usuarios_dependentes ud
            inner join sf_usuarios u on u.funcionario = ud.id_fornecedor_despesas
            where u.id_usuario = " . valoresSelect2($dadosFuncionario['id_usuario']) . "))))";

            $sQuery = "set dateformat dmy; ";
            $sQuery .= "select *, (comissao_plano + comissao_plano_pri + comissao_servico + comissao_produto) as comissao 
            into #tempComissionamento 
            from (select id_venda, data_venda, vendedor, razao_social, empresa, nome_plano,
            sum(preco_plano) preco_plano, sum(preco_servico) preco_servico, sum(preco_produto) preco_produto, 
            sum(CONVERT(DECIMAL(10,2), isnull(comissao_plano_pri, 0))) comissao_plano_pri, sum(CONVERT(DECIMAL(10,2), isnull(comissao_plano, 0))) comissao_plano, 
            sum(CONVERT(DECIMAL(10,2), isnull(comissao_servico, 0))) comissao_servico, sum(CONVERT(DECIMAL(10,2), isnull(comissao_produto, 0))) comissao_produto, 
            id_solicitacao_autorizacao, isnull(status_comissao, 'Pendente') status, prov, parcela 
            from (";
            $sQuery .= "select v.id_venda,v.data_venda, f.razao_social,f.empresa
            ,(select top 1 descricao from sf_vendas_planos vp inner join sf_produtos s on s.conta_produto = vp.id_prod_plano 
            inner join sf_vendas_planos_mensalidade vpm on vpm.id_plano_mens = vp.id_plano
            inner join sf_vendas_itens svi on svi.id_item_venda = vpm.id_item_venda_mens
            where svi.id_venda = v.id_venda) as nome_plano
            ,case when p.tipo = 'C' then  vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_plano
            ,case when p.tipo = 'S' then  vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_servico
            ,case when p.tipo = 'P' then  vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_produto
            ,case when p.tipo = 'C' and e.recebe_comissao = 1 and dbo.FU_PRIMEIRO_PAGAMENTO(vi.id_item_venda) > 0
            then (((valor_total - (valor_custo_medio * quantidade)) * case when " . $dadosFuncionario['tbcomiss_gere'] . " = 1 
            and e.id_fornecedores_despesas != " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas']) . " then valor_comissao_venda 
            when " . $dadosFuncionario['tbcomiss_gere'] . " = 2 and e.id_fornecedores_despesas != " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas'])
                    . " then " . $dadosFuncionario['comiss_gerePri'] . " when e.tbcomiss_plano = 1 
            then valor_comissao_venda when e.tbcomiss_plano = 2 then e.comiss_planoPri else 0 end)/100) 
            / case when pp.parcela < 1 then 1 else pp.parcela end else 0 end comissao_plano_pri
            ,case when p.tipo = 'C' and e.recebe_comissao = 1 and dbo.FU_PRIMEIRO_PAGAMENTO(vi.id_item_venda) = 0
            then (((valor_total - (valor_custo_medio * quantidade)) * case when " . $dadosFuncionario['tbcomiss_gere'] . " = 1 
            and e.id_fornecedores_despesas != " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas']) . " then valor_comissao_venda 
            when " . $dadosFuncionario['tbcomiss_gere'] . " = 2 and e.id_fornecedores_despesas != " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas'])
                    . " then " . $dadosFuncionario['comiss_gere'] . " when e.tbcomiss_plano = 1 
            then valor_comissao_venda when e.tbcomiss_plano = 2 then e.comiss_plano else 0 end)/100) 
            / case when pp.parcela < 1 then 1 else pp.parcela end else 0 end comissao_plano
            ,case when p.tipo = 'S' and e.recebe_comissao = 1 and e.id_fornecedores_despesas = " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas']) . "
            then ((valor_total - (valor_custo_medio * quantidade)) * case when e.tbcomiss_serv = 1 
            then valor_comissao_venda when e.tbcomiss_serv = 2 then e.comiss_serv else 0 end)/100 else 0 end comissao_servico
            ,case when p.tipo = 'P' and e.recebe_comissao = 1 and e.id_fornecedores_despesas = " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas']) . "
            then ((valor_total - (valor_custo_medio * quantidade)) * case when e.tbcomiss_prod = 1 
            then valor_comissao_venda when e.tbcomiss_prod = 2 then e.comiss_prod else 0 end)/100 else 0 end comissao_produto
            ,v.vendedor, v.indicador, f.id_user_resp, f.prof_resp, vi.vendedor_comissao comissionado, 0 prov, 1 parcela, sa.id_solicitacao_autorizacao, sa.status as status_comissao
            from sf_vendas v inner join sf_fornecedores_despesas f on v.cliente_venda = f.id_fornecedores_despesas AND fornecedores_status != 'Excluido'
            inner join sf_vendas_itens vi on vi.id_venda = v.id_venda
            left join sf_vendas_planos_mensalidade pm on vi.id_item_venda = pm.id_item_venda_mens
            left join sf_produtos_parcelas pp on pm.id_parc_prod_mens = pp.id_parcela
            left join sf_grupo_cliente g on g.id_grupo = f.grupo_pessoa    
            inner join sf_produtos p on p.conta_produto = vi.produto 
            inner join sf_fornecedores_despesas e on f.prof_resp = e.id_fornecedores_despesas or e.id_fornecedores_despesas = (select top 1 funcionario from sf_usuarios where id_usuario = f.id_user_resp)
            left join sf_comissao_historico ch on v.id_venda = ch.hist_venda and ((f.id_user_resp is not null and ch.hist_vendedor = " . valoresSelect2($dadosFuncionario['id_usuario']) . ")
            or (f.prof_resp is not null and ch.hist_vendedor = " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas']) . "))
            left join sf_solicitacao_autorizacao sa on sa.id_solicitacao_autorizacao = ch.hist_solicitacao_autorizacao
            where v.status = 'Aprovado' and p.tipo in ('C', 'P', 'S') " . $whereFuncionario . $where;

            $sQuery .= " union all select distinct #tempProdutividade.*, sa.id_solicitacao_autorizacao, sa.status as status_comissao from #tempProdutividade 
            left join sf_comissao_historico ch on id_venda = ch.hist_venda and hist_parcela = prov and hist_vendedor = " . valoresNumericos2($dadosFuncionario['id_usuario']) . "
            left join sf_solicitacao_autorizacao sa on sa.id_solicitacao_autorizacao = ch.hist_solicitacao_autorizacao and sa.fornecedor_despesa = " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas']) . "    
            where prov > 0 " . $where;

            $sQuery .= ") as x group by id_venda, data_venda, vendedor, razao_social, empresa, nome_plano,
            id_solicitacao_autorizacao, prov, parcela, status_comissao
            having (sum(CONVERT(DECIMAL(10,2), isnull(comissao_produto, 0))) + sum(CONVERT(DECIMAL(10,2), isnull(comissao_servico, 0))) +
            sum(CONVERT(DECIMAL(10,2), isnull(comissao_plano, 0))) + sum(CONVERT(DECIMAL(10,2), isnull(comissao_plano_pri,0)))) > 0 ) as a";
            //echo $sQuery; exit;
            Conexao::conect($sQuery);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }*/

    private function queryBaseProspectLead() {
        return "select id_lead as id, razao_social, telefone_contato collate Latin1_General_CI_AI as telefone, email_contato collate Latin1_General_CI_AI as email, 'lead' as tipo,
        indicador_lead, (select top 1 razao_social from sf_lead where id_lead = l.indicador_lead) as nome_indicador,
        case when inativo = 0 then 'Pendente' else 'Inativo' end as status, procedencia as id_procedencia, nome_procedencia, assunto, indicador as funcionario, dt_cadastro
        from sf_lead l
        left join sf_procedencia p on l.procedencia = p.id_procedencia
        where prospect is null
        union all 
        select id_fornecedores_despesas as id, f.razao_social, 
         (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = id_fornecedores_despesas AND tipo_contato = 1) as telefone,
         (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = id_fornecedores_despesas AND tipo_contato = 2) as email,
         'prospect' as tipo, indicador_lead,
         (select top 1 razao_social from sf_lead where id_lead = l.indicador_lead) as nome_indicador,
         fornecedores_status as status, f.procedencia as id_procedencia, nome_procedencia, null as assunto, 
         (select case when prof_resp is not null then prof_resp else (select top 1 funcionario from sf_usuarios where id_usuario = f.id_user_resp) end) as funcionario,
         f.dt_cadastro
        from sf_fornecedores_despesas f
        left join sf_lead l on l.prospect = f.id_fornecedores_despesas
        left join sf_procedencia p on f.procedencia = p.id_procedencia
        where f.inativo = 0 and tipo = 'P'";
    }

    private function queryBaseClientes() {
        return "SELECT x.*, p.descricao, p.conta_produto, cm.descricao as grupo,
        msn.parcela, msn.tipo_pagamento, vp.favorecido, vp.dt_cadastro as data_cadastro_plano, vp.planos_status,
        (select top 1 razao_social from sf_fornecedores_despesas where id_fornecedores_despesas = vp.favorecido) as nome_responsavel,
        case when x.tipo = 'P' then 'prospect'
        when fornecedores_status in ('AtivoEmAberto', 'DesligadoEmAberto') then 'EmAberto'
        when fornecedores_status in ('Ativo', 'AtivoAbono') or fornecedores_status like 'Ativo Ausente%' then 'Ativo'
        when fornecedores_status in ('Inativo', 'Desligado') then 'Inativo'
        else fornecedores_status end as status  
        from (
            SELECT fd.*, descricao_grupo as tipo_grupo_pessoa, fun.razao_social as nome_funcionario, fun.id_fornecedores_despesas as id_funcionario, ci.cidade_nome, es.estado_sigla,
            (SELECT TOP 1 id_plano from sf_vendas_planos vp WHERE vp.planos_status not in ('Cancelado') and (vp.favorecido = fd.id_fornecedores_despesas 
            OR vp.favorecido in (select id_titular from sf_fornecedores_despesas_dependentes where id_dependente = fd.id_fornecedores_despesas))) as id_plano,
            (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 2) as email,
            (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 0) as telefone,
            (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 1) as celular,
            (SELECT COUNT(a.id_dependente) FROM sf_fornecedores_despesas_dependentes a WHERE a.id_titular = fd.id_fornecedores_despesas) as dependentes,
            (SELECT TOP 1 id_dependente FROM sf_fornecedores_despesas_dependentes WHERE id_dependente = fd.id_fornecedores_despesas) as id_dependente 
            FROM sf_fornecedores_despesas fd  
            LEFT JOIN sf_fornecedores_despesas fun on 
            ((fd.prof_resp is not null and fun.id_fornecedores_despesas = fd.prof_resp) or
            (fd.prof_resp is null and fun.id_fornecedores_despesas = (select top 1 funcionario from sf_usuarios where id_usuario = fd.id_user_resp)))
            LEFT JOIN sf_grupo_cliente gc ON gc.id_grupo = fd.grupo_pessoa
            LEFT JOIN tb_cidades ci on ci.cidade_codigo = fd.cidade
            LEFT JOIN tb_estados es on es.estado_codigo = fd.estado
            WHERE fd.inativo = 0 and (fd.fornecedores_status != 'excluido' or fd.fornecedores_status is null) and fd.tipo in ('C', 'P')
        ) as x
        LEFT JOIN sf_vendas_planos vp on vp.id_plano = x.id_plano
        OUTER APPLY (SELECT TOP 1 parcela, 
        case when (pp.parcela = -1) then 'BOLETO RECORRENTE'
        when (pp.parcela = -2) then 'PIX'
        when (pp.parcela = 0) then 'DCC'
        when (pp.parcela = 1) then 'BOLETO'
        when (pp.parcela > 1) then 'MENSAL'
        else null end  as tipo_pagamento from sf_vendas_planos_mensalidade vpm 
        inner join sf_produtos_parcelas pp ON pp.id_parcela = vpm.id_parc_prod_mens
        where vpm.id_plano_mens = x.id_plano ORDER BY id_mens desc) as msn
        LEFT JOIN sf_produtos p ON p.conta_produto = vp.id_prod_plano
        LEFT JOIN sf_contas_movimento cm ON cm.id_contas_movimento = p.conta_movimento";
    }

    private function responseCliente($row) {
        $dados = [];
        $dados['id'] = $row['id_fornecedores_despesas'];
        $dados['nome'] = escreverTexto($row['razao_social']);
        $dados['cnpj'] = $this->ocultarCnpj($row['cnpj']);
        $dados['email'] = escreverTexto($row['email']);
        $dados['id_plano'] = $row['id_plano'];
        $dados['nome_plano'] = escreverTexto($row['descricao']);
        $dados['grupo'] = escreverTexto($row['grupo']);
        $dados['data_cadastro_plano'] = escreverDataHora($row['data_cadastro_plano']);
        $dados['data_cadastro'] = escreverDataHora($row['dt_cadastro']);
        $dados['status'] = escreverTexto($row['status']);
        $dados['situacao'] = escreverTexto(($row['fornecedores_status'] and $row['fornecedores_status'] !== 'Dependente') ? $row['fornecedores_status'] : ($row['tipo'] == 'P' ? 'Inativo' : 'Ativo'));
        $dados['dependentes'] = $row['dependentes'];
        $dados['tipo_pagamento'] = escreverTexto($row['tipo_pagamento']);
        $dados['celular'] = escreverTexto($row['celular']);
        $dados['cep'] = $row['cep'];
        $dados['endereco'] = escreverTexto($row['endereco']);
        $dados['numero'] = escreverTexto($row['endereco']);
        $dados['complemento'] = escreverTexto($row['complemento']);
        $dados['bairro'] = escreverTexto($row['bairro']);
        $dados['cidade'] = escreverTexto($row['cidade_nome']);
        $dados['estado'] = escreverTexto($row['estado_sigla']);
        $dados['sexo'] = $row['sexo'];
        $dados['data_nascimento'] = escreverData($row['data_nascimento']);
        $dados['estado_civil'] = escreverTexto($row['estado_civil']);
        $dados['tipo'] = escreverTexto($row['tipo']);
        $dados['titular'] = $row['favorecido'];
        $dados['juridico_tipo'] = $row['juridico_tipo'];
        $dados['nome_responsavel'] = escreverTexto($row['nome_responsavel']);
        $dados['nome_funcionario'] = escreverTexto($row['nome_funcionario']);
        if ($row['juridico_tipo'] === 'J') {
            $dados['cnae_fiscal'] = escreverTexto($row['cnae_fiscal']);
            $dados['inscricao_estadual'] = escreverTexto($row['inscricao_estadual']);
            $dados['regime_tributario'] = $row['regime_tributario'];
            $dados['nfe_iss'] = $row['nfe_iss'];
        }
        return $dados;
    }

    private function ocultarCnpj($cnpj) {
        if (strlen($cnpj) == 18) {
            return substr($cnpj, 0, 4) . "**.***/****-" . substr($cnpj, 16, 2);            
        } else if (strlen($cnpj) == 14) {
            return substr($cnpj, 0, 3) . ".***.***-" . substr($cnpj, 12, 2);            
        } else {
            return $cnpj;
        }
    }
    
    private function getResponseProspectLead($rows) {
        $rows['razao_social'] = escreverTexto($rows['razao_social']);
        $rows['nome_indicador'] = escreverTexto($rows['nome_indicador']);
        $rows['nome_procedencia'] = escreverTexto($rows['nome_procedencia']);
        $rows['assunto'] = escreverTexto($rows['assunto']);
        $rows['dt_cadastro'] = escreverDataHora($rows['dt_cadastro']);
        $rows['status'] = escreverTexto($rows['status']);
        $rows['dependentes'] = $this->getResponseProspectDependentes($rows['id']);
        return $rows;
    }

    public function getResponseProspectDependentes($id_fornecedores) {
        try {
            $sql = "select id_fornecedores_despesas, razao_social from sf_fornecedores_despesas_dependentes cd
            inner join sf_fornecedores_despesas c on c.id_fornecedores_despesas = cd.id_dependente
            where cd.id_titular = " . valoresSelect2($id_fornecedores);
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $row['razao_social'] = escreverTexto($row['razao_social']);
                $dados[] = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getUsuarioFuncionario($id_fornecedores) {
        try {
            $sql = "select top 1 id_fornecedores_despesas, login_user, id_usuario, tbcomiss_gere, comiss_gere, comiss_gerePri, tbcomiss_plano,
            comiss_plano, comiss_planoPri, tbcomiss_serv, tbcomiss_ser_tipo, tbcomiss_serv, comiss_serv, tbcomiss_prod, razao_social,
            tbcomiss_prod_tipo, comiss_prod
            from sf_fornecedores_despesas fd 
            left join sf_usuarios u on u.funcionario = fd.id_fornecedores_despesas
            where tipo in ('E', 'I') and id_fornecedores_despesas = " . valoresSelect2($id_fornecedores);
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados = $row;
                $dados['login_user'] = escreverTexto($row['login_user']);
                $dados['razao_social'] = escreverTexto($row['razao_social']);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getListaFuncionariosPorGestor($id_funcionario) {
        try {
            $query = "select fd.id_fornecedores_despesas, u.id_usuario, razao_social
            from sf_fornecedores_despesas fd
            inner join sf_usuarios u on u.funcionario = fd.id_fornecedores_despesas and tipo = 'E'
            where fd.id_fornecedores_despesas = " . valoresSelect2($id_funcionario) . ";";
            $dados = [];
            $res = Conexao::conect($query);
            while ($row = odbc_fetch_array($res)) {
                $row['razao_social'] = escreverTexto($row['razao_social']);
                $dados[] = $row;
                $dados = $this->getListaFuncionariosPorDependente($row['id_fornecedores_despesas'], $dados, "");
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getListaFuncionariosPorDependente($id_funcionario, $dados, $mark) {
        try {
            $query = "select p.id_fornecedores_despesas, u.id_usuario, razao_social 
            from sf_usuarios_dependentes ud
            inner join sf_usuarios u on u.id_usuario = ud.id_usuario
            inner join sf_fornecedores_despesas p on p.id_fornecedores_despesas = u.funcionario
            where id_fornecedor_despesas = " . valoresSelect2($id_funcionario) . "
            order by razao_social asc;";
            $res = Conexao::conect($query);
            while ($row = odbc_fetch_array($res)) {
                $row['razao_social'] = $mark . escreverTexto($row['razao_social']);
                $dados[] = $row;
                $dados = $this->getListaFuncionariosPorDependente($row['id_fornecedores_despesas'], $dados, "" . $mark);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /*private function regrasComissaoPlano($dadosFuncionario) {
        $comissao_plano = "0";
        if ($dadosFuncionario['tbcomiss_plano'] > 0) {
            $comissao_plano = " (vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) * " .
                    ($dadosFuncionario['tbcomiss_plano'] == 1 ? "p.valor_comissao_venda" : $dadosFuncionario['comiss_plano']) . "/100)";
        } else if ($dadosFuncionario['tbcomiss_gere'] > 0) {
            $comissao_plano = "(vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) * "
                    . ($dadosFuncionario['tbcomiss_gere'] == 1 ? "p.valor_comissao_venda" : $dadosFuncionario['comiss_gere']) . "/100)";
        }
        return $comissao_plano;
    }

    private function regrasComissaoPlanoPri($dadosFuncionario) {
        $comissao_plano_pri = "0";
        if ($dadosFuncionario['tbcomiss_plano'] > 0) {
            $comissao_plano_pri = " (vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) * "
                    . ($dadosFuncionario['tbcomiss_plano'] == 1 ? "p.valor_comissao_venda" : $dadosFuncionario['comiss_planoPri']) . "/100)";
        } else if ($dadosFuncionario['tbcomiss_gere'] > 0) {
            $comissao_plano_pri = "(vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) * "
                    . ($dadosFuncionario['tbcomiss_gere'] == 1 ? "p.valor_comissao_venda" : $dadosFuncionario['comiss_gerePri']) . "/100)";
        }
        return $comissao_plano_pri;
    }

    private function regrasServico($dadosFuncionario) {
        $regraServico = ($dadosFuncionario['tbcomiss_ser_tipo'] > 0 ? " and vi.vendedor_comissao = "
                . valoresNumericos2($dadosFuncionario['id_fornecedores_despesas']) : "");
        return $regraServico;
    }

    private function regrasComissaoServico($dadosFuncionario) {
        $comissao_servico = "((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio)) * "
                . ($dadosFuncionario['tbcomiss_serv'] == 1 ? "p.valor_comissao_venda" : $dadosFuncionario['comiss_serv']) . "/100)";
        return $comissao_servico;
    }

    private function regrasProduto($dadosFuncionario) {
        $regraProduto = ($dadosFuncionario['tbcomiss_prod_tipo'] > 0 ? " and vi.vendedor_comissao = "
                . valoresNumericos2($dadosFuncionario['id_fornecedores_despesas']) : "");
        return $regraProduto;
    }

    private function regrasComissaoProduto($dadosFuncionario) {
        $comissao_produto = "((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio)) * "
                . ($dadosFuncionario['tbcomiss_prod'] == 1 ? "p.valor_comissao_venda" : $dadosFuncionario['comiss_prod']) . "/100)";
        return $comissao_produto;
    }

    private function regrasTipoVendedores($dadosFuncionario) {
        $whereTipoVend = " and (v.vendedor in (" . valoresTexto2($dadosFuncionario['login_user']) . ")";
        $whereTipoVend .= " or v.indicador in (" . valoresNumericos2($dadosFuncionario['id_fornecedores_despesas']) . ")";
        $whereTipoVend .= " or (c.id_user_resp in (" . valoresNumericos2($dadosFuncionario['id_usuario'])
                . ") or vi.vendedor_comissao in (" . valoresNumericos2($dadosFuncionario['id_fornecedores_despesas']) . "))";
        $whereTipoVend .= " or (c.prof_resp in (" . valoresNumericos2($dadosFuncionario['id_fornecedores_despesas'])
                . ") or vi.vendedor_comissao in (" . valoresNumericos2($dadosFuncionario['id_fornecedores_despesas']) . "))";
        $whereTipoVend .= " or (c.id_user_resp in (select ud.id_usuario from sf_usuarios_dependentes ud
        inner join sf_usuarios u on u.id_usuario = ud.id_usuario
        INNER JOIN sf_fornecedores_despesas f ON u.funcionario = f.id_fornecedores_despesas  
        WHERE f.inativo = 0 AND dt_demissao IS NULL AND f.tipo = 'E' AND (tbcomiss_prod <> 0 or comiss_planoPri <> 0 or tbcomiss_serv <> 0 or tbcomiss_plano <> 0)
        and ud.id_fornecedor_despesas = " . valoresNumericos2($dadosFuncionario['id_fornecedores_despesas']) . ") or 
        vi.vendedor_comissao in (select f.id_fornecedores_despesas from sf_usuarios_dependentes ud
        inner join sf_usuarios u on u.id_usuario = ud.id_usuario
        INNER JOIN sf_fornecedores_despesas f ON u.funcionario = f.id_fornecedores_despesas  
        WHERE f.inativo = 0 AND dt_demissao IS NULL AND f.tipo = 'E' AND (tbcomiss_prod <> 0 or comiss_planoPri <> 0 or tbcomiss_serv <> 0 or tbcomiss_plano <> 0)
        and ud.id_fornecedor_despesas = " . valoresNumericos2($dadosFuncionario['id_fornecedores_despesas']) . ")))";
        return $whereTipoVend;
    }

    public function criaTabelaProvissionamento($dadosFuncionario, $data) {
        try {
            Conexao::conect("drop table if exists #tempProdutividade;");
            $condicaoData = "";
            if ($data['inicio']) {
                $condicaoData .= " and (" . valoresDataHoraUnico2(str_replace("_", "/", $data['inicio']) . " 00:00:00") . " <= DATEADD(month,pp.parcela,data_venda))";
            }
            if ($data['fim']) {
                $condicaoData .= " and (data_venda <= " . valoresDataHoraUnico2(str_replace("_", "/", $data['fim']) . " 23:59:59") . ")";
            }
            $where = " and ((c.prof_resp is not null and (c.prof_resp = " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas']) . " or c.prof_resp in (select funcionario from sf_usuarios u 
            inner join sf_usuarios_dependentes ud on ud.id_usuario = u.id_usuario 
            where ud.id_fornecedor_despesas = " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas']) . "))) 
            or (c.id_user_resp is not null and (c.id_user_resp = " . valoresSelect2($dadosFuncionario['id_usuario']) . " or c.id_user_resp in (select ud.id_usuario from sf_usuarios_dependentes ud
            inner join sf_usuarios u on u.funcionario = ud.id_fornecedor_despesas
            where u.id_usuario = " . valoresSelect2($dadosFuncionario['id_usuario']) . "))))";

            $sql = "set dateformat dmy; select v.id_venda, v.data_venda, c.razao_social, c.empresa, p.descricao as nome_plano, 
            case when p.tipo = 'C' then vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_plano,
            0 preco_servico, 0 preco_produto, 0 comissao_plano_pri, 
            case when p.tipo = 'C' and e.recebe_comissao = 1 and dbo.FU_PRIMEIRO_PAGAMENTO(vi.id_item_venda) = 0
            then (((valor_total - (valor_custo_medio * quantidade)) * case when " . $dadosFuncionario['tbcomiss_gere'] . " = 1 
            and e.id_fornecedores_despesas != " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas']) . " then valor_comissao_venda 
            when " . $dadosFuncionario['tbcomiss_gere'] . " = 2 and e.id_fornecedores_despesas != " . valoresSelect2($dadosFuncionario['id_fornecedores_despesas'])
                    . " then " . $dadosFuncionario['comiss_gere'] . " when e.tbcomiss_plano = 1 
            then valor_comissao_venda when e.tbcomiss_plano = 2 then e.comiss_plano else 0 end)/100) 
            / case when pp.parcela < 1 then 1 else pp.parcela end else 0 end comissao_plano, 
            0 comissao_servico, 0 comissao_produto, v.vendedor, v.indicador, c.id_user_resp, c.prof_resp, vi.vendedor_comissao comissionado, 0 prov, 
            pp.parcela into #tempProdutividade from sf_vendas v inner join sf_vendas_itens vi on vi.id_venda = v.id_venda
            inner join sf_produtos p on p.conta_produto = vi.produto 
            left join sf_vendas_planos_mensalidade m on m.id_item_venda_mens = vi.id_item_venda 
            left join sf_vendas_planos vp on vp.id_plano = m.id_plano_mens 
            left join sf_produtos_parcelas pp on pp.id_parcela = m.id_parc_prod_mens 
            inner join sf_fornecedores_despesas c on c.id_fornecedores_despesas = v.cliente_venda 
            inner join sf_fornecedores_despesas e on c.prof_resp = e.id_fornecedores_despesas or e.id_fornecedores_despesas = (select top 1 funcionario from sf_usuarios where id_usuario = c.id_user_resp)
            left join sf_filiais fi on fi.id_filial = c.empresa where pp.parcela > 1 and v.cov = 'V' and v.status = 'Aprovado' 
            and p.tipo in ('C') " . $where . $condicaoData . ";";
            //echo $sql; exit;
            Conexao::conect($sql);
            $this->adicionaLinhaTabelaProvissionamento();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function criaTabelaProvissionamentoCliente($dadosFuncionario, $data) {
        try {
            Conexao::conect("drop table if exists #tempProdutividade;");
            $condicaoData = "";
            if ($data['inicio']) {
                $condicaoData .= " and (" . valoresDataHoraUnico2(str_replace("_", "/", $data['inicio']) . " 00:00:00") . " <= DATEADD(month,pp.parcela,data_venda))";
            }
            if ($data['fim']) {
                $condicaoData .= " and (data_venda <= " . valoresDataHoraUnico2(str_replace("_", "/", $data['fim']) . " 23:59:59") . ")";
            }
            $where = " and c.id_fornecedores_despesas in (select id_cliente from sf_comissao_cliente where id_usuario = " . valoresSelect2($dadosFuncionario['id_usuario']) . ")";
            $sql = "set dateformat dmy; select v.id_venda, v.data_venda, c.razao_social, c.empresa, p.descricao as nome_plano,  
            case when p.tipo = 'C' then vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio) else 0 end preco_plano,
            0 preco_servico, 0 preco_produto, 0 comissao_plano_pri, 
            case when p.tipo = 'C' then ((vi.quantidade * ((vi.valor_total/vi.quantidade) - p.valor_custo_medio))
            * isnull((select max(valor) from sf_comissao_cliente where id_cliente = c.id_fornecedores_despesas and tipo = 1 and id_usuario = "
                    . valoresSelect2($dadosFuncionario['id_usuario']) . "),0)/100) / case when pp.parcela < 1 then 1 else pp.parcela end end
            comissao_plano, 0 comissao_servico,0 comissao_produto,   
            v.vendedor, v.indicador, c.id_user_resp, c.prof_resp, vi.vendedor_comissao comissionado, 0 prov, pp.parcela
            into #tempProdutividade from sf_vendas v 
            inner join sf_vendas_itens vi on vi.id_venda = v.id_venda
            inner join sf_produtos p on p.conta_produto = vi.produto
            left join sf_vendas_planos_mensalidade m on m.id_item_venda_mens = vi.id_item_venda
            left join sf_vendas_planos vp on vp.id_plano = m.id_plano_mens
            left join sf_produtos_parcelas pp on pp.id_parcela = m.id_parc_prod_mens
            inner join sf_fornecedores_despesas c on c.id_fornecedores_despesas = v.cliente_venda
            left join sf_filiais fi on fi.id_filial = c.empresa
            where pp.parcela > 1 and v.cov = 'V' and v.status = 'Aprovado' and p.tipo in ('C') " . $where . $condicaoData . ";";
            //echo $sql; exit;
            Conexao::conect($sql);
            $this->adicionaLinhaTabelaProvissionamento();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function adicionaLinhaTabelaProvissionamento() {
        try {
            $cur = Conexao::conect("select * from #tempProdutividade");
            if (odbc_num_rows($cur) > 0) {
                $query = "set dateformat dmy; insert into #tempProdutividade ";
                while ($RFP = odbc_fetch_array($cur)) {
                    for ($i = 1; $i < $RFP['parcela']; $i++) {
                        $query .= " select id_venda, dateadd(month," . $i . ",data_venda) data_venda, razao_social,empresa, 
                        nome_plano, preco_plano, preco_servico,preco_produto,comissao_plano_pri,comissao_plano,
                        comissao_servico,comissao_produto, vendedor,indicador,id_user_resp,prof_resp,comissionado," . $i . ", parcela
                        from #tempProdutividade where id_venda = " . $RFP['id_venda'] . " and cast(data_venda as date) = "
                                . valoresData2(escreverData($RFP['data_venda'])) . " union all";
                    }
                }
                //echo substr_replace($query, "", -9).";"; exit;
                Conexao::conect(substr_replace($query, "", -9));
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }*/
}
