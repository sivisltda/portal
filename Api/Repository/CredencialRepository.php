<?php
class CredencialRepository {
    
    /**
     * Credencial
     * @param int $id_credencial
     * @return array $dados
     * @throws Exception
     */
    public function getCredencial($id_credencial) {
        try {
            $dados = [];
            $sql = "select top 1 * from sf_credenciais WHERE id_credencial = ".valoresSelect2($id_credencial);
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
                $dados = $row;
                $dados['descricao'] = escreverTexto($row['descricao']);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Cria a credencial do Usuário
     * @param array $data
     * @return array | boolean
     * @throws Exception
     */
    public function criaCredencialUsuario($data) {
        try {
            $sql = " insert into sf_fornecedores_despesas_credenciais(id_fornecedores_despesas, id_credencial, dt_inicio, dt_fim, motivo, dt_cadastro, usuario_resp)
            values (".valoresSelect2($data['id_usuario']).", ".valoresSelect2($data['id_credencial'])."
            , getdate(), dateadd(day, ".$data['dias'].", getdate()), ".valoresTexto2($data['mensagem']).", getdate(), 1); SELECT SCOPE_IDENTITY() id;";
            $res = Conexao::conect($sql);
            odbc_next_result($res);
            $id_credencial = odbc_result($res, 1);            
            if($id_credencial) {
                salvarLog(['tabela' => 'sf_fornecedores_despesas_credenciais', 'id_item' => $id_credencial,
                'usuario' => getLoginUser(), 'acao' => 'I', 'descricao' => 'INCLUSAO CREDENCIAL:'.$data['id_credencial'],
                'id_fornecedores_despesas' => $data['id_usuario']]);
                return $this->getCredencialUsuario($id_credencial);
            }
            return false;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Verifica se a credencial do usuário
     * @param int $id_credencial
     * @return array $dados
     * @throws Exception
     */
    public function getCredencialUsuario($id_fornecedores_credencial) {
        try {
            $dados = [];
            $sql = "select top 1 * from sf_fornecedores_despesas_credenciais
             where id_fornecedores_despesas_credencial =".valoresSelect2($id_fornecedores_credencial);
             $res = Conexao::conect($sql);
             while($row = odbc_fetch_array($res)) {
                $dados = $this->responseUsuarioCredencial($row);
             }
             return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage()); 
        }
    }


    /**
     * Verifica se possui a credencial informada
     * @param int $id_credencial
     * @param int $usuario_id
     * @return array $dados
     * @throws Exception 
     */
    public function verificaCredencialUsuario($id_credencial, $usuario_id) {
        try {
            $dados = [];
            $sql = "select top 1 * from sf_fornecedores_despesas_credenciais 
            where id_credencial = ".valoresSelect2($id_credencial). " and id_fornecedores_despesas = ".valoresSelect2($usuario_id);
            $res = Conexao::conect($sql);
            while($row = odbc_fetch_array($res)) {
               $dados = $this->responseUsuarioCredencial($row);
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    /**
     * Cancela Credencial
     * @param $dados
     * @throws Exception
     */
    public function cancelaCredencial($dados) {
        try {
            $sql = "update sf_fornecedores_despesas_credenciais set dt_cancelamento = getdate(), usuario_canc = 1
            where id_credencial = ".valoresSelect2($dados['credencial_id']). " and id_fornecedores_despesas = ".valoresSelect2($dados['id_usuario']);
            salvarLog(['tabela' => 'sf_fornecedores_despesas_credenciais', 'id_item' => $dados['credencial_id'],
            'usuario' => getLoginUser(), 'acao' => 'C', 'descricao' => 'CANCELAMENTO CREDENCIAL:'.$dados['credencial_id'],
            'id_fornecedores_despesas' => $dados['id_usuario']]);
            Conexao::conect($sql);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    

    /**
     * Response Credencial
     * @param array $row
     * @return array $dados
     */
    private function responseUsuarioCredencial($row) {
        $dados =[];
        $dados = $row;
        $dados['motivo']            = escreverTexto($row['motivo']);
        $dados['dt_inicio']         = escreverData($row['dt_inicio']);
        $dados['dt_fim']            = escreverData($row['dt_fim']);
        $dados['dt_cancelamento']   = escreverData($row['dt_cancelamento']);
        $dados['dt_cadastro']       = escreverDataHora($row['dt_cadastro']);
        return $dados;
    }
}