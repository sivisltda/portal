<?php

class EnderecoRepository {
    
     /**
     * Pega a lista de estados
     * @return array
     * @throws Exception
     */
    public function getEstados() {
        try {
            $sql    = "select estado_codigo,estado_sigla, estado_nome from tb_estados where estado_codigo > 0 ORDER BY estado_nome";
            $result = Conexao::conect($sql);
            $dados  = [];
            while ($rows = odbc_fetch_array($result)) {
                $dados[] = [
                    'estado_codigo'     => $rows['estado_codigo'],
                    'estado_sigla'      => $rows['estado_sigla'],
                    'estado_nome'       => escreverTexto($rows['estado_nome'])    
                ];
            }
            return $dados;
        }catch(Exception $e) {
            throw new Exception($e->getMessage());
        }        
    }


    /**
     * Pega a lista de Cidades
     * @param string $estado
     * @return array 
     * @throws Exception
     */
    public function getCidades($estado) {
        try {
            $dados  = [];
            $sql    = "SELECT * from tb_cidades where cidade_codigo > 0 AND cidade_codigoEstado = " . valoresSelect2($estado) . " ORDER BY cidade_nome"; 
            $result = Conexao::conect($sql);
            while ($rows = odbc_fetch_array($result)) {
                $data = $rows;
                $data['cidade_nome'] = escreverTexto($rows['cidade_nome']);
                $dados[] = $data;
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }       
    }

    /**
     * Pega a lista de Regiões
     * @return array
     * @throws Exception
     */
    public function getRegioes() {
        try {
            $dados = [];
            $sql = "SELECT * from sf_regioes where inativo = 0";
            $result = Conexao::conect($sql);
            while ($rows = odbc_fetch_array($result)) {
                $rows['descricao'] = escreverTexto($rows['descricao']);
                $dados[] = $rows;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
    
    /**
     * Pega a lista de Cidade de Regiões
     * @param string $where
     * @return array
     * @throws Exception
     */
    public function getCidadeRegioes($where) {
        try {
            $dados = [];
            $sql = "select * from sf_regioes_cidades where ".$where;
            $result = Conexao::conect($sql);
            while($rows = odbc_fetch_array($result)) {
                $dados[] = $rows;
            }   
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

}