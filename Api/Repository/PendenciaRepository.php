<?php

class PendenciaRepository {

    public function getListPendencias($condicao) {
        try {
            $sql = "set dateformat dmy; select * from (select distinct ve.id as id_veiculo, mes_referencia, marca, modelo, ano_modelo, 
            ano_fab, placa, id_plano, cnt.id as contrato_id, vp.id_mens, vi.id id_vistoria,
            (select top 1 id_mens from sf_vendas_planos_mensalidade where id_plano_mens = vp.id_plano and dt_pagamento_mens is not null) pagamento_id,
            vp.descricao, vp.valor_unico, fd.id_fornecedores_despesas, fd.razao_social, vp.dt_cadastro, 
            (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 2) as email,
            (SELECT TOP 1 conteudo_contato FROM sf_fornecedores_despesas_contatos WHERE fornecedores_despesas = fd.id_fornecedores_despesas AND tipo_contato = 1) as celular, 
            case when ve.chassi is not null then 1 else 0 end as complete,
            (select case when adm_desconto_range = 1 then 2 else 1 end from sf_configuracao) as descontos, case when vi.data_aprov is null then 0 else 1 end as vistoria 
            from sf_fornecedores_despesas_veiculo ve 
            inner join sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = ve.id_fornecedores_despesas 
            and inativo = 0 and tipo in ('C', 'P') 
            inner join (select vp.*, vm.id_mens, p.conta_movimento, descricao, valor_unico
            from sf_vendas_planos vp 
            inner join sf_vendas_planos_mensalidade vm on vm.id_plano_mens = vp.id_plano and 
            id_mens = (select top 1 id_mens from sf_vendas_planos_mensalidade where id_plano_mens = vp.id_plano and dt_pagamento_mens is null)
            inner join sf_produtos_parcelas pp on pp.id_parcela = vm.id_parc_prod_mens
            inner join sf_produtos p on p.conta_produto = pp.id_produto
            where vp.dt_cancelamento is null) as vp on vp.id_veiculo = ve.id and vp.favorecido = fd.id_fornecedores_despesas
            inner join sf_contratos_grupos cg on cg.id_grupo = vp.conta_movimento      
            inner join sf_contratos cnt on cg.id_contrato = cnt.id and cnt.tipo = 0 and cnt.portal = 1
            left join sf_vistorias vi on vi.id_veiculo = ve.id and vi.id in (select MAX(vx.id) from sf_vistorias vx where vx.id_veiculo = ve.id)
            where ve.status = 'Pendente' and " . $condicao . " ) as a order by dt_cadastro desc";
            //echo $sql; exit;
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados[] = [
                    'razao_social' => escreverTexto($row['razao_social']),
                    'celular' => $row['celular'],
                    'email' => $row['email'],
                    'id_veiculo' => $row['id_veiculo'],
                    'id_plano' => $row['id_plano'],
                    'id_mens' => $row['id_mens'],
                    'id_fornecedores_despesas' => $row['id_fornecedores_despesas'],
                    'data_cadastro' => escreverDataHora($row['dt_cadastro']),
                    'ano_modelo' => $row['ano_modelo'],
                    'ano_fabricacao' => $row['ano_fab'],
                    'placa' => strtoupper($row['placa']),
                    'valor' => escreverNumero($row['valor_unico'], 1),
                    'plano' => escreverTexto($row['descricao']),
                    'veiculo' => escreverTexto($row['marca'] . ' - ' . $row['modelo']),
                    'contrato_id' => $row['contrato_id'],
                    'pagamento_id' => $row['pagamento_id'],
                    'complete' => intval($row['complete']),
                    'descontos' => intval($row['descontos']),
                    'vistoria' => intval($row['vistoria']),
                    'url_vistoria' => "https://vistoria.sivisweb.com.br/" . $row['id_vistoria'] . "/" . CONTRATO
                ];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getConveniosPorPlano($id_plano) {
        try {
            $sql = "select top 1 id_plano, conta_produto, conta_produto_adesao, valor_adesao, valor_total, valor_desconto_adesao,
            convenio_adesao, convenio_veiculo, id_veiculo, dt_inicio_mens, id_fornecedores_despesas, placa, 
            CONVERT(DECIMAL(10,2), case when tp_valor = 'P' then(valor_total * (valor / 100)) 
            when tp_valor = 'D' then valor else 0 end) as valor_desconto 
            from (select con_v.*,  vp.id_plano, ve.id_fornecedores_despesas, vpm.conta_produto, vpm.conta_produto_adesao, vpm.valor_adesao, vp.id_veiculo,
            ve.placa, vpm.valor_unico + dbo.FU_VALOR_ACESSORIOS(vp.id_plano) as valor_total, vpm.dt_inicio_mens,
            case when conv_ad.tp_valor = 'P' then (valor_adesao * (conv_ad.valor / 100))
            when conv_ad.tp_valor = 'D' then conv_ad.valor else 0 end 
            as valor_desconto_adesao, conv_ad.id_convenio as convenio_adesao, con_v.convenio as convenio_veiculo
            from sf_vendas_planos vp 
            inner join (select vpm.*,pp.valor_unico, p.conta_produto, p.descricao, p.conta_produto_adesao, s.preco_venda as valor_adesao 
            from sf_vendas_planos_mensalidade vpm 
            inner join sf_produtos_parcelas pp on pp.id_parcela = vpm.id_parc_prod_mens
            inner join sf_produtos p on p.conta_produto = pp.id_produto
            left join sf_produtos s on s.conta_produto = p.conta_produto_adesao) vpm 
            on vpm.id_mens = (select top 1 id_mens from sf_vendas_planos_mensalidade where id_plano_mens = vp.id_plano)
            inner join sf_fornecedores_despesas_veiculo ve on ve.id = vp.id_veiculo
            and ve.id_fornecedores_despesas = vp.favorecido      
            left join (select id_fornecedor, id_convenio, id_conv_plano, id_prod_convenio, valor, tp_valor, dt_inicio, dt_fim
            from sf_fornecedores_despesas_convenios fdc 
            inner join sf_convenios con on con.id_convenio = fdc.convenio
            inner join sf_convenios_regras cre on cre.convenio = con.id_convenio
            inner join sf_convenios_planos cp on cp.id_convenio_planos = con.id_convenio
            where cast(getdate() as date) between dt_inicio and dt_fim) conv_ad 
            on conv_ad.id_fornecedor = ve.id_fornecedores_despesas and conv_ad.id_conv_plano = vp.id_plano and conv_ad.id_prod_convenio = vpm.conta_produto_adesao      
            left join (select id_fornecedor,dt_inicio as dt_inicio_con, dt_fim as dt_fim_con, descricao, valor, tp_valor, fdc.convenio, id_conv_plano, id_prod_convenio
            from sf_fornecedores_despesas_convenios fdc
            inner join sf_convenios con on con.id_convenio = fdc.convenio
            inner join sf_convenios_regras cre on cre.convenio = con.id_convenio
            inner join sf_convenios_planos cp on cp.id_convenio_planos = con.id_convenio
            where cast(getdate() as date) between dt_inicio and dt_fim) con_v 
            on con_v.id_fornecedor = ve.id_fornecedores_despesas and con_v.id_conv_plano = vp.id_plano and con_v.id_prod_convenio = vp.id_prod_plano
            where vp.dt_cancelamento is null and vp.id_plano = " . valoresSelect2($id_plano) . ") as x;";
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $row['placa'] = escreverTexto(strtoupper($row['placa']));
                $row['valor_adesao'] = escreverNumero($row['valor_adesao']);
                $row['valor_total'] = escreverNumero($row['valor_total']);
                $row['valor_desconto_adesao'] = escreverNumero($row['valor_desconto_adesao']);
                $row['valor_desconto'] = escreverNumero($row['valor_desconto']);
                $row['dt_inicio_mens'] = escreverData($row['dt_inicio_mens']);
                $dados = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getValoresDesconto($id_usuario) {
        try {
            $sql = "select top 1 fin_desc_plano, fin_desc_plano_tot, fin_desc_serv, fin_desc_serv_tot 
            from sf_usuarios u inner join sf_usuarios_permissoes up on up.id_permissoes = u.master
            where u.id_usuario = " . valoresSelect2($id_usuario) . ";";
            $dados = [];
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $dados = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function atualizarStatusVeiculo($status, $condicao) {
        try {
            $query = "update sf_fornecedores_despesas_veiculo 
            set status = " . valoresTexto2($status) . " where " . $condicao . ";";
            Conexao::conect($query);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}
