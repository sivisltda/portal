<?php

class CarteiraRepository {

    public function getDadosCarteira() {
        try {
            $sql = "select * from sf_carteirinhas where foto_capa is not null 
            and foto_verso is not null";
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $row['descricao']   = escreverTexto($row['descricao']);
                $row['mensagem']    = escreverTexto($row['mensagem']);
                $row['foto_capa']   = escreverTexto($row['foto_capa']);
                $row['foto_verso']  = escreverTexto($row['foto_verso']);
                $dados[] = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getDadosUsuarioCarteira($idUsuario) {
        try {
            $sql = "select id_fornecedores_despesas, p.conta_movimento, vp.id_plano
            from sf_vendas_planos vp 
            inner join sf_produtos p on p.conta_produto = vp.id_prod_plano and p.tipo = 'C'
            inner join sf_fornecedores_despesas fd on fd.id_fornecedores_despesas = vp.favorecido and fd.tipo = 'C'
            where vp.dt_cancelamento is null and (vp.favorecido = (select top 1 id_titular from sf_fornecedores_despesas_dependentes where id_dependente = "
            .valoresSelect2($idUsuario).") or vp.favorecido = ".valoresSelect2($idUsuario).")
            and fd.fornecedores_status in ('Ativo','AtivoCred','AtivoAbono','Ativo Ausente 1','Ativo Ausente 2','Ativo Ausente 3','Ativo Ausente 4','Ativo Ausente 5');";
            $res = Conexao::conect($sql);
            $dados = [];
            while($row = odbc_fetch_array($res)) {
                $dados[] = $row;  
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}