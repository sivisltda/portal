<?php

class CampanhaRepository {

    public function getCampanha($condicao) {
        try {
            $sql = "select top 1 * from sf_lead_campanha lc
            inner join sf_procedencia p on p.id_procedencia = lc.id_procedencia
            where " . $condicao;
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados = $this->getResponse($row);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getCampanhas($condicao) {
        try {
            $sql = "select * from sf_lead_campanha lc
            inner join sf_procedencia p on p.id_procedencia = lc.id_procedencia
            where " . $condicao;
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $dados[] = $this->getResponse($row);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getLeads($condicao) {
        try {
            $sql = "select id_lead, l.razao_social, 
            case when prospect is not null and tipo = 'C' and fornecedores_status like 'Ativo%' then 'ativo'
            when (l.inativo = 1 or c.inativo = 1) then 'inativo'
            when prospect is not null and tipo in ('P', 'C') then 'prospect'
            else 'pendente' end as status
            from sf_lead l left join sf_fornecedores_despesas c on id_fornecedores_despesas = prospect
            WHERE " . $condicao;
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $row['razao_social'] = escreverTexto($row['razao_social']);
                $dados[] = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getFinanceiro($id) {
        try {
            $sql = "select v.data_venda, c.razao_social, ((vi.valor_total * cc.valor)/100) valor, 0 tipo
            from sf_fornecedores_despesas c
            inner join sf_usuarios u on u.funcionario = c.prof_resp
            inner join sf_comissao_cliente cc on cc.id_usuario = u.id_usuario and id_cliente = id_fornecedores_despesas and cc.tipo = 2
            inner join sf_vendas v on v.cliente_venda = c.id_fornecedores_despesas and status = 'Aprovado'
            inner join sf_vendas_itens vi on vi.id_venda = v.id_venda
            inner join sf_produtos p on vi.produto = p.conta_produto and p.tipo = 'S'
            where prof_resp = " . valoresSelect2($id) . "
            union all
            select s.data_lanc, 'PAGAMENTO', valor_pago, 1 from sf_solicitacao_autorizacao s
            inner join sf_solicitacao_autorizacao_parcelas sp on s.id_solicitacao_autorizacao = sp.solicitacao_autorizacao
            where s.status = 'Aprovado' and s.fornecedor_despesa = " . valoresSelect2($id) . "
            order by 1";
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $row['data_venda'] = escreverData($row['data_venda']);                
                $row['razao_social'] = escreverTexto($row['razao_social']);
                $dados[] = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getQtdCupons($id_campanha) {
        try {
            $sql = "select (max_cupom - qtd_cupom) as disponivel, * 
            from (select count(cupom) as qtd_cupom, (1 + max_range - min_range) as max_cupom 
            from sf_lead_campanha lc
            left join sf_lead_campanha_cupom lcu on lcu.id_campanha = lc.id 
            where lc.id = " . valoresSelect2($id_campanha) . " group by max_range, min_range) as x;";
            $dados = [];
            $res = Conexao::conect($sql);
            while ($row = odbc_fetch_array($res)) {
                $dados = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function criarCupom($id_campanha, $id_indicador, $id_lead) {
        try {
            $sql = "EXEC dbo.SP_LEAD_CUPOM @campanha = " . valoresSelect2($id_campanha) . "," . 
            "@lead = " . valoresSelect2($id_lead) . "," . 
            "@lead_origem = " . valoresSelect2($id_indicador) . ";";
            $res = Conexao::conect($sql);
            $id_cupom = odbc_result($res, 1);
            return $id_cupom;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getCupons($condicao) {
        try {
            $sql = "select REPLICATE('0', 10 - LEN(cupom)) + RTrim(cupom) as cupom,
            razao_social from sf_lead_campanha_cupom c
            inner join sf_lead l on l.id_lead = c.id_lead
            WHERE " . $condicao;
            $res = Conexao::conect($sql);
            $dados = [];
            while ($row = odbc_fetch_array($res)) {
                $row['razao_social'] = escreverTexto($row['razao_social']);
                $dados[] = $row;
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function getResponse($row) {
        $row['titulo'] = escreverTexto($row['titulo']);
        $row['descricao'] = escreverTexto($row['descricao']);
        $row['data_ini'] = escreverData($row['data_ini']);
        $row['data_fim'] = escreverData($row['data_fim']);
        $row['nome_procedencia'] = escreverTexto($row['nome_procedencia']);
        return $row;
    }
}
