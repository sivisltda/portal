<?php

class ConexaoDefault {
    private static $con;

    private function __construct()
    {
        
    }
    
    public static function getInstance() {
        try {
            if (!isset(self::$con)) {               
                $con = odbc_connect("DRIVER={SQL Server}; SERVER=" .HOSTNAME_DEFAULT.  "; DATABASE=" . DATABASE_DEFAULT, USERNAME_DEFAULT, PASSWORD); 
                if (!$con) {
                    throw new Exception(odbc_errormsg($con));
                } 
                self::$con = $con;
            }
            return self::$con;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public static function conect($query) {
        try {
            $con = self::getInstance();    
            odbc_exec($con, "set dateformat dmy;");               
            $cur = odbc_exec($con, $query);     
            if (!$cur) {
                gerarLog('erro'.uniqid(), odbc_errormsg($con));
                throw new Exception (odbc_errormsg($con));
            }
            return $cur;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public static function initTransaction() {
        $con = self::getInstance();
        odbc_autocommit($con, false);
    }

    public static function rollback() {
        $con = self::getInstance();
        odbc_rollback($con);
        odbc_autocommit($con, true);
    }

    public static function commit() {
        $con = self::getInstance();
        odbc_commit($con);
        odbc_autocommit($con, true);
    }
}