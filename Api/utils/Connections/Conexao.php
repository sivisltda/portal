<?php

class Conexao {
    private static $con;

    private function __construct()
    {
        
    }

    public static function getInstance() {
        try {
            if (!isset(self::$con)) {               
                self::defaultConnection();
            }
            return self::$con;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public static function toggleConnection($hostname, $database, $username, $password) {
        try {
            $con = odbc_connect("DRIVER={SQL Server}; SERVER=" .$hostname.  "; DATABASE=" . $database, $username, $password); 
            if ($con) {
                self::$con = $con;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public static function defaultConnection() {
        try {
            $con = odbc_connect("DRIVER={SQL Server}; SERVER=" .CONEXAO_HOSTNAME.  "; DATABASE=" . CONEXAO_DATABASE, CONEXAO_USERNAME, CONEXAO_PASSWORD); 
            if (!$con) {
                throw new Exception(odbc_errormsg($con));
            } 
            self::$con = $con;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public static function conect($query) {
        try {
            $con = self::getInstance(); 
            $cur = odbc_exec($con, $query);     
            if (!$cur) {
                gerarLog('erro'.uniqid(), odbc_errormsg($con));
                throw new Exception (odbc_errormsg($con).":".$query);
            }
            return $cur;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public static function initTransaction() {
        $con = self::getInstance();
        odbc_autocommit($con, false);
    }

    public static function rollback() {
        $con = self::getInstance();
        odbc_rollback($con);
        odbc_autocommit($con, true);
    }

    public static function commit() {
        $con = self::getInstance();
        odbc_commit($con);
        odbc_autocommit($con, true);
    }

}