<?php

function replaceBarraDirectory($path) {
    return str_replace(['/', '\\'], DR, $path);
}

function requireOnce($path, $dados = []) {
    $arquivo = replaceBarraDirectory($path);
    if (file_exists($arquivo)) {
        extract($dados);
        require_once $arquivo;
    } else {
        throw new Exception('Arquivo não encontrado', 404);
    }
}

function valoresTexto($nome) {
    return valoresTexto2($_POST[$nome]);
}

function valoresTextoUpper($nome) {
    return strtoupper(valoresTexto2($_POST[$nome]));
}

function valoresCheck($nome) {
    return valoresCheck2($_POST[$nome]);
}

function valoresSelect($nome) {
    return valoresSelect2($_POST[$nome]);
}

function valoresSelectTexto($nome) {
    return valoresSelectTexto2($_POST[$nome]);
}

function valoresNumericos($nome) {
    return valoresNumericos2($_POST[$nome]);
}

function valoresData($nome) {
    return valoresData2($_POST[$nome]);
}

function valoresHora($nome) {
    return valoresHora2($_POST[$nome]);
}

function valoresDataHora($data, $hora) {
    return valoresDataHora2($_POST[$data], $_POST[$hora]);
}

function valoresDataHoraUnico($datahora) {
    return valoresDataHoraUnico2($_REQUEST[$datahora]);
}

//------------------------------------------------Funções-Idiomas----------------------------------------
$languages = "pt";
require_once __DIR__ .DR."..".DR."Languages".DR.$languages.DR."lang.".$languages.".php";

//------------------------------------------------Funções-Processamento-----------------------------------------
function valoresTexto2($nome) {
    return "'" . utf8_decode(str_replace("'", "", $nome)) . "'";
}

function valoresCheck2($nome) {
    if ($nome == "1") {
        return 1;
    } else {
        return 0;
    }
}

function valoresSelect2($nome) {
    if ($nome != "") {
        return $nome;
    } else {
        return "null";
    }
}

function valoresSelectTexto2($nome) {
    if ($nome != "") {
        return "'" . $nome . "'";
    } else {
        return "null";
    }
}

function valoresHora2($nome) {
    $explode = explode(":", $nome);
    if (count($explode) == 2 || count($explode) == 3) {
        if (is_numeric($explode[0]) && $explode[0] >= 0 && $explode[0] < 24 &&
        is_numeric($explode[1]) && $explode[1] >= 0 && $explode[1] < 60 &&
        (count($explode) == 2 || (count($explode) == 3 && $explode[2] >= 0 && $explode[2] < 60))) {
            return "'" . $nome . "'";
        } else {
            return "null";
        }
    } else {
        return "null";
    }
}

function valoresDataHora2($data, $hora) {
    if (valoresData2($data) != "null" && valoresHora2($hora) != "null") {
        return str_replace("''", " ", valoresData2($data) . valoresHora2($hora));
    } elseif (valoresData2($data) != "null") {
        return valoresData2($data);
    } else {
        return "null";
    }
}

function valoresDataHoraUnico2($datahora) {
    $explode = explode(" ", str_replace("_", "/", $datahora));
    if (count($explode) == 2) {
        return valoresDataHora2($explode[0], $explode[1]);
    } else {
        return "null";
    }
}

function valoresDataFormat($nome, $mask = "d/m/Y") {
    $explode = explode('-', substr($nome, 0, 10));
    if(count($explode) === 3) {
        if(checkdate($explode[1], $explode[2], $explode[0])) {
            return "'" . date_format(date_create($nome), $mask) . "'";
        }
    }
    return "null";
}

function ValoresTextoMP($val) {
    return preg_replace('/[^A-Za-z0-9\- ]/', '', $val);
}

function valoresTextoPuro($val) {
    return preg_replace('/[^A-Za-z0-9\ ]/', '', $val);
}

function valorNumeroPuro($val) {
    return preg_replace('/[^0-9\ ]/', '', $val);
}

function ValoresNumericosMP($val) {
    $und = "00";
    $number = explode(',', $val);
    if (count($number) > 1) {
        $cent = $number[0];
        $und = $number[1] . "00";
    }
    return $cent . substr($und, 0, 2);
}

//------------------------------------------------Funções-Gerais-----------------------------------------
function returnPart($nome, $posicao) {
    if (is_numeric(substr($nome, $posicao, 1))) {
        return substr($nome, $posicao, 1);
    } else {
        return "1";
    }
}

function getModulos() {
    $dados = json_decode(encrypt($_SESSION[CHAVE_CONTRATO]['config'], KEY_CRIPT, false), true);
    return [
        'clb' => $dados['mdl_clb'],
        'mil' => $dados['mdl_mil'],
        'seg' => $dados['mdl_seg'],
        'sau' => $dados['mdl_sau']
    ];
}

function getLoginUser() {
    if(isset($_SESSION[CHAVE_CONTRATO]['indicador']['login_usuario']) && 
        strlen($_SESSION[CHAVE_CONTRATO]['indicador']['login_usuario']) > 0) {
        return $_SESSION[CHAVE_CONTRATO]['indicador']['login_usuario'];
    } else {        
        return 'PORTAL';
    }
}

function deleteDirectory($dir) {
    if (!file_exists($dir)) {
        return true;
    }
    if (!is_dir($dir)) {
        return unlink($dir);
    }
    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') {
            continue;
        }
        if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
            return false;
        }
    }
    return rmdir($dir);
}

function dataDiff($data1, $data2, $intervalo) {
    switch ($intervalo) {
        case 'y':
            $Q = 86400 * 365;
            break; //ano
        case 'm':
            $Q = 2592000;
            break; //mes
        case 'd':
            $Q = 86400;
            break; //dia
        case 'h':
            $Q = 3600;
            break; //hora
        case 'n':
            $Q = 60;
            break; //minuto
        default:
            $Q = 1;
            break; //segundo
    }
    return round((strtotime($data2) - strtotime($data1)) / $Q);
}

function prepareCollum($valor, $tipo) {
    if (strripos($valor, " ")) {
        $result = explode(" ", str_replace(" as ", " ", $valor));
        return $result[$tipo];
    } else {
        return $valor;
    }
}

function getCollor($i) {
    switch ($i) {
        case 0:
            return "#68AF27"; //verde
        case 1:
            return "#08c"; //Azul
        case 2:
            return "#333"; //preto
        case 3:
            return "#dc5039"; //vermelho
        case 4:
            return "#e09f4f"; //laranja
        case 5:
            return "#fdd400"; //verde claro
        case 6:
            return "#722672"; //roxo
        case 7:
            return "#996632"; //marrom
        case 8:
            return "#666666"; //cinza
        default:
            return "#333"; //preto                
    }
}

function getColorStatusAl($status) {
    if ($status == "Ativo" || $status == "Ativos") {
        return "#68AF27";
    }if ($status == "Inativo") {
        return "#dc5039";
    }if ($status == "Bolsistas" || $status == "AtivoCred") {
        return "#fdd400";
    }if ($status == "Serasa") {
        return "#333";
    } else {
        return "#e09f4f";
    }
}

function formatNameCompact($name) {
    $CompleteName = explode(" ", $name);
    $nomeUserLogin = $CompleteName[0];
    if (count($CompleteName) > 1) {
        $nomeUserLogin .= " " . $CompleteName[count($CompleteName) - 1];
    }
    return $nomeUserLogin;
}

function encrypt($frase, $chave, $crypt) {
    $retorno = "";
    if ($crypt) {
        $string = $frase;
        $i = strlen($string) - 1;
        $j = strlen($chave);
        do {
            $retorno .= ($string{$i} ^ $chave{$i % $j});
        } while ($i--);
        return base64_encode(strrev($retorno));
    } else {
        $string = base64_decode($frase);
        $i = strlen($string) - 1;
        $j = strlen($chave);
        do {
            $retorno .= ($string{$i} ^ $chave{$i % $j});
        } while ($i--);
        return strrev($retorno);
    }
}

function dd($arguments) {
    var_dump($arguments);
    die();
}

function numPad($number, $dec = 8) {
    return str_pad($number, $dec, 0, STR_PAD_LEFT);
}

function diaPrimeiroVencimento($dia_escolhido) {
    if($dia_escolhido <= date('d')) {
        $mes = date('m', strtotime("+1 month"));
        $ano = date('Y', strtotime("+1 month"));
    } else {
        $mes = date('m');
        $ano = date('Y');
    }
    return date('d/m/Y', strtotime($ano.'-'.$mes.'-'.$dia_escolhido.' -1 days'));      
}

function proRata($dia_vencimento, $valor) {
    list($dia, $mes, $ano) = explode('/', $dia_vencimento);
    $quant_dias = dataDiff(date('Y-m-d'), $ano.'-'.$mes.'-'.$dia, 'd');
    $valor = str_replace(",", ".", str_replace(".", "", $valor));
    return arredondar_dois_decimal(($valor / 30) * $quant_dias);
}

function arredondar_dois_decimal($valor) { 
    $float_arredondado=round($valor * 100) / 100; 
    return $float_arredondado; 
} 

function age($date){
    $today = strtotime('now');
    $birth = strtotime($date);
    $age = floor((((($today - $birth) /60) /60) /24) /365.25);
    return $age;
}

function limitText($value, $limit) {
    if ($value && strlen(strip_tags($value)) > $limit) {
        return substr($value, 0, $limit).'...';
    }
    return $value;
}

function convertValores($valores) {
    return array_map(function($v) {
        if(preg_match('/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/', $v)) {
            return valoresData2($v);
        } else if(is_string($v)) {
            return utf8_decode($v);
        }
        return $v;
    }, $valores);
}

function convertValoresParaTexto($valores) {
    return array_map(function($v) {
        if(is_string($v)) {
            return utf8_encode($v);
        }
        return $v;
    }, $valores);
}


/**
* Listas os dias dos vencimentos possíveis
* @return array $dias_escolhidos
*/
function diasVencimento() {
    $dias_possiveis = [1,10,20];
    $dia_atual = date('d');
    $dias_escolhidos = [];
    for($i=0; $i < $dias_possiveis; $i++) {
        $next =  $i >= count($dias_possiveis) ? count($dias_possiveis) - 1 :  $i+1;
        if($dia_atual >= $dias_possiveis[$i] && $dia_atual <= $dias_possiveis[$next]) {
            $dias_escolhidos[] = $dias_possiveis[$i];
            $dias_escolhidos[] = $dias_possiveis[$next];
        }
        if($dia_atual > $dias_possiveis[count($dias_possiveis) - 1] && count($dias_escolhidos) < 2) {
            $dias_escolhidos[] = $dias_possiveis[count($dias_possiveis) - 1];
            $dias_escolhidos[] = $dias_possiveis[0];
        }
        if(count($dias_escolhidos) === 2) {
            break;
        }
    }
    return $dias_escolhidos;
}

function gerarLog($nome, $content) {
    $dir = __DIR__.replaceBarraDirectory('/../../../Log/'.date('d/m/Y').'/Query/');
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true);
    }
    $myfile = fopen($dir.$nome.".txt", "w");
    fwrite($myfile, $content);
    fclose($myfile);
}

function days_of_week(){
    return [
        'Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'
    ];
}

function month(){
    return [
        'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'
    ];
}

function hours(){
    $dados = [];
    for ($hr = 0; $hr <= 23; $hr++) {
        $dados[] = str_pad($hr, 2, "0", STR_PAD_LEFT) . ":00:00";
    }
    return $dados;
}

function hours_interval($start, $end, $interval) {
    $times = [$start, $end];
    $interval = $interval * 60;
    $count_time = [];
    $i = 0;
    $intervals = [];
    foreach($times as $time) {
        list($h,$m,$s) = explode(':',$time);
        $seconds = ($h * 3600) + ($m * 60) + $s;
        $count_time[$i] = $seconds;
        $i++;
    }
    $is = 0;
    foreach(hours() as $hour){
        list($h, $m, $s) = explode(':', $hour);
        $seconds = ($h * 3600) + ($m * 60) + $s;
        if($seconds >= $count_time[0] && $seconds < $count_time[1]){
            if($is == 0){
                $is = $seconds;
            }
            while($is < $seconds + 3600){
                $intervals [] = second_time($is);
                $is+= $interval;
            }
        }
    }
    return $intervals;
}

function show_days(array $days){
    $array = [];
    foreach ($days as $day){
        $array[$day] = days_of_week()[$day];
    }
    return $array;
}

function second_time($seconds){
    $hours = str_pad(floor($seconds / 3600),2,0,STR_PAD_LEFT);
    $seconds -= $hours * 3600;
    $minutes = str_pad((floor($seconds / 60)), 2, '0', STR_PAD_LEFT);
    $seconds -= $minutes * 60;
    $seconds = str_pad($seconds, 2, '0', STR_PAD_LEFT);
    return  "$hours:$minutes:$seconds";
}

function dataParaDiaSemana($date) {
    list($year, $month, $day) = explode('-',date('Y-m-d', strtotime($date)));
    $dayMonth = gregoriantojd($month, $day, $year);
    $weekMonth = jddayofweek($dayMonth, 0);
    return days_of_week()[$weekMonth];
}

function paginationNext($total,$page, $qtd){
    $qtdPages = $total / ($qtd * $page);
    return $qtdPages > 1 ? $page + 1 : false;
}

function paginationPrev($total, $page, $qtd) {
    $qtdPages = $total / $qtd;
    return ($qtdPages > 1 && $page > 1) ? $page - 1 : false;
}

function paginationQtd($total, $qtd) {
    return ceil($total / $qtd);
}

function verificaValorCampo($data, $nome) {
    if(is_array($data) && isset($data[$nome]) && $data[$nome]) {
        return valoresSelect2($data[$nome]);
    }
    return "null";
}

function verificaValorTextoCampo($data, $nome) {
    if(is_array($data) && isset($data[$nome]) && $data[$nome]) {
        return valoresTexto2($data[$nome]);
    }
    return "null";
}

function filtroWhereIn($condicoes) {
    return join(',', array_map(function($item) {
        return valoresTexto2($item);
    }, explode(',', $condicoes)));
}

function verificaSessao($contrato) {
    $sessao = isset($_SESSION) && isset($_SESSION[$contrato]) ? $_SESSION[$contrato] : null;
    if ($sessao) {
        http_response_code(403);
        echo json_encode('Não autorizado');
        exit;
    }
}

function getRequestHeaders() {
    $headers = [];
    foreach($_SERVER as $key => $value) {
        if (substr($key, 0, 5) <> 'HTTP_') {
            continue;
        }
        $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
        $headers[$header] = $value;
    }
    return $headers;
}

function isJson($headers) {
    if (isset($headers['Accept']) && preg_match("/application\/json/i", $headers['Accept'])) {
        return true;
    }
    return false;
}

function contratoInvalido($headers, $mensagem, $code) {
    if (isJson($headers)) {
        http_response_code($code);
        echo json_encode($mensagem);
        exit;
    } else {        
        header("Location: ".SITE."not-found.php");
        exit;
    }
}

function responseJSON($message, $code = 200) {
    http_response_code($code);
    echo json_encode($message);
    exit;
}

function getGrupo() {
    $dados = json_decode(encrypt($_SESSION[CHAVE_CONTRATO]['config'], KEY_CRIPT, false), true);
    $grupo = $dados['grupos'];
    return $grupo;
}

function toggleConnection($numero_contrato) {
    $grupo = array_filter(getGrupo(), function($item) use($numero_contrato) {
        return $item['numero_contrato'] == $numero_contrato;
    });
    if(count($grupo)) {
        $grupo = current($grupo); 
        Conexao::toggleConnection($grupo['local'], $grupo['banco'], $grupo['login'], $grupo['senha']);
    }
}

function defaultConnection() {
    Conexao::defaultConnection();
}

function valoresNumericoPuro($val) {
    return preg_replace('/\s+/', '', preg_replace('/[^0-9\ ]/', '', $val));
}

function getImages($contrato, $tipo, $arquivo = null) {
    try {
        $getFields = [
            'functionPage' => 'buscarImages',
            'loja' => $contrato,
            'tipo' => $tipo,
        ];
        if($arquivo) $getFields['arquivo'] = $arquivo;     
        $dados = request(
            ERP_URL.'Modulos/Sivis/ws_api.php?'.http_build_query($getFields)
        );
        return $dados;
    } catch (Exception $e) {
        throw new Exception($e);
    }
}

function request($url, $method = "GET", $params = [], $headers = [], $file = false) {
    $ch = curl_init();
    if ($method === "GET" && $params) {
        $url = $url."?".http_build_query($params);
    }
    curl_setopt($ch, CURLOPT_URL, $url);
    if ($params && $method === 'POST') {
        $payload = $file ? $params : json_encode($params);
       /*  if ($file) {
            $arquivo = curl_file_create($file['path'], $file['mime']);
            $payload[$file['name_pos']] = $arquivo;
        } */
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $res = curl_exec($ch);
    if(curl_errno($ch)){
        throw new Exception(curl_error($ch));
    }     
    $dados = json_decode($res, 1);
    return $dados;     
}

function salvarLog($data) {
    $query = "insert into sf_logs (tabela, id_item, usuario, acao, descricao, data, 
    id_fornecedores_despesas) values (".valoresTexto2($data['tabela']).",
    ".valoresSelect2($data['id_item']).", ".valoresTexto2($data['usuario']).",
    ".valoresTexto2($data['acao']).",".valoresTexto2($data['descricao']).", 
    getdate(), ".valoresSelect2($data['id_fornecedores_despesas']).");";
    Conexao::conect($query);
}

function getExtensao($arquivo) {
    return substr($arquivo, strrpos($arquivo, '.')+1);
}

/**
 * Label para mensalidade
 * @param $row
 * @return string
 */
function labelMensalidade($row) {
    $meses = ['', 'JANEIRO', 'FEVEREIRO', 'MARÇO', 'ABRIL', 'MAIO', 'JUNHO', 'JULHO', 'AGOSTO', 'SETEMBRO', 'OUTUBRO', 'NOVEMBRO', 'DEZEMBRO'];
    return $meses[$row['num_mes']].' / '.$row['num_ano'];
}

/**
 * Link para o boleto
 * @param $row
 * @return string
 */
function linkBoleto($row) {
    $link = '';
    if(strlen($row['bol_descricao']) > 0) {
        $link = $row['bol_descricao'];
    } else if(strlen($row['bol_link']) > 0) {
        $link = $row['bol_link'];
    } else if(!$row['dt_pagamento_mens'] && ($row['tipo_pagamento'] === 'BOL' || ($row['tipo_pagamento'] === 'MENSAL' && $row['id_boleto']))) {
        $link = ERP_URL.'Boletos/Boleto.php?id=M-'.$row['id_mens'].'&crt='.CONTRATO;
    }
    return $link;
}

/**
 * Verifica qual é a bandeira
 * @param $cc_num
 * @return string
 */
function verificar_bandeira($cc_num) {
    if($cc_num) {
        if (preg_match("/^3[47]/", $cc_num)) {
            return "amex"; //"amex -59";
        } elseif (preg_match("/^30[0-5]/", $cc_num)) {
            return "diners_club_carte_blanche"; //" -87";
        } elseif (preg_match("/^36/", $cc_num)) {
            return "diners_club_international"; //"-87";
        } elseif (preg_match("/^35(2[89]|[3-8][0-9])/", $cc_num)) {
            return "jcb"; //"-114";
        } elseif (preg_match("/^(6304|670[69]|6771)/", $cc_num)) {
            return "laser"; //"-114";
        } elseif (preg_match("/^(4026|417500|4508|4844|491(3|7))/", $cc_num)) {
            return "visa_electron"; //"-186";
        } elseif (preg_match("/^4/", $cc_num)) {
            return "visa"; //"-150";
        } elseif (preg_match("/^5[1-5]/", $cc_num)) {
            return "mastercard"; //"-223";
        } elseif (preg_match("/^(5018|5020|5038|6304|6759|676[1-3])/", $cc_num)) {
            return "maestro"; //"-259";
        } elseif (preg_match("/^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/", $cc_num)) {
            return "discover"; //"-295";
        } elseif (preg_match("/^(636368|438935|504175|451416|636297|5067|4576|4011)\d+$/", $cc_num)) {
            return "elo"; //"-30";
        } elseif (preg_match("/^50[0-9]/", $cc_num)) {
            return "aura"; //"-1";
        } elseif (preg_match("/^(38|60)\d{11}(?:\d{3})?(?:\d{3})?$/", $cc_num)) {
            return "hiper"; //"-324";
        } else {
            return "default"; //-114;
        }
    }
    return "";
}

function getExtension($arquivo) {
    $arquivoExt = explode('.', $arquivo);
    return end($arquivoExt);
}