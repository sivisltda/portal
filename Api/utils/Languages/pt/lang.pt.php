<?php

$lang['dmy'] = "d/m/Y";
$lang['prefix'] = "R$ ";

function valoresNumericos2($nome) {
    $xTeste = str_replace(",", ".", str_replace(".", "", str_replace("%", "", str_replace("R$ ", "", $nome))));
    if (!is_numeric($xTeste)) {
        return 0;
    } else {
        return $xTeste;
    }
}

function valoresData2($nome) {
    $explode = explode("/", str_replace("-", "/", $nome));
    if (count($explode) == 3) {
        if (checkdate($explode[1], $explode[0], $explode[2])) {
            return "'" . str_replace("-", "/", $nome) . "'";
        } else {
            return "null";
        }
    } else {
        return "null";
    }
}
function escreverTexto($nome){
    return utf8_encode($nome);
}

function escreverNumero($nome, $ident = 0, $decimal = 2, $dec_point = ",", $thousands_sep = ".") {
    return ($ident == 1 ? "R$ " : "") . number_format($nome, $decimal, $dec_point, $thousands_sep);
}

function escreverData($data, $mask = "d/m/Y") {    
    return ($data == "" ? "" : date_format(date_create($data), $mask));
}

function escreverDataHora($data) {
    return ($data == "" ? "" : date_format(date_create($data), 'd/m/Y H:i'));
}

function getData($type, $tag = "") {
    if ($type == "B") {
        return $tag == "" ? date("01/m/Y") : date("01/m/Y", strtotime($tag));
    } elseif ($type == "T") {
        return $tag == "" ? date("d/m/Y") : date("d/m/Y", strtotime($tag));
    } elseif ($type == "E") {
        return $tag == "" ? date("t/m/Y") : date("t/m/Y", strtotime($tag));
    }
}

function geraTimestamp($data) {
    $partes = explode('/', $data);
    return mktime(0, 0, 0, $partes[1], $partes[0], $partes[2]);
}

function escreverDataSoma($data, $tag, $mask = "d/m/Y") {
    $partes = explode('/', str_replace("'", "", $data));
    $date = date_create($partes[2] . "-" . $partes[1] . "-" . $partes[0]);
    date_add($date, date_interval_create_from_date_string($tag));
    return date_format($date, $mask);
}

function convertDataIso($date) {
    if($date !== "" || $date !== null) {
        $partials = explode('/', $date);
        if(count($partials) === 3) {
            if (checkdate($partials[1], $partials[0], $partials[2])) {
                return $partials[2].'-'.$partials[1].'-'.$partials[0];
            }
        }
    }
    return null;
}
