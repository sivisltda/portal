<?php

if (basename($_SERVER["PHP_SELF"]) == "config.php") {
    die("Este arquivo não pode ser acessado diretamente.");
}

$contrato = isset($_SESSION['contrato']) ? $_SESSION['contrato'] : null;
if (!$contrato) {
    contratoInvalido($headers, 'Contrato Inválido', 403);
}

if(!isset($_SESSION["loja-".$contrato]['config'])) {   
    contratoInvalido($headers, 'Sessão inválida', 401);
}

$dados = json_decode(encrypt($_SESSION["loja-".$contrato]['config'], KEY_CRIPT, false), true);
$endereco =  $dados['endereco'].', '.$dados['cidade_uf'];
$cnpj = $dados['cpf_cnpj'];
define('SITE_NAME', $dados['cedente']);
define('NOME', $dados['nome_fantasia']);
define('CONTRATO', $dados['contrato'] );
define('CHAVE_CONTRATO', "loja-".$dados['contrato']);
define('CNPJ', $cnpj);
define('HASH_CONTRATO', $dados['contrato_hash']);
define('LOGO_URL', $dados['imagem_logo']);
define('LOGO_WHATS', $dados['imagem_whats']);
define('LOGO_CONCLUSAO', $dados['imagem_conclusao']);
define('COR_PRIMARIA', $dados['cor_primaria']);
define('COR_SECUNDARIA', $dados['cor_secundaria']);
define('COR_DESTAQUE', $dados['cor_destaque']);
define('ENDERECO', $endereco);
define('AGENDA', $dados['agenda']);
define('PLANO_UNICO', $dados['adm_plano_unico']);
define('ACA_EX_PJ', $dados['ACA_EX_PJ']);
define('ACA_EX_VEN_PR', $dados['ACA_EX_VEN_PR']);
define('TERMINAL_VENDA', $dados['adm_terminal_venda']);
define('VENDA_ONLINE', $dados['adm_venda_online']);
define('DEPENDENTE_PARENTESCO', $dados['adm_dependente_parentesco']);
define('TAXA_ADESAO_DEPENDENTE', $dados['aca_adesao_dep']);
define('ADD_DEPENDENTE', $dados['aca_add_dep']);
define('TIPO_VENCIMENTO', $dados['ACA_TIPO_VENCIMENTO']);
define('DIA_VENCIMENTO', $dados['ACA_DIA_VENCIMENTO']);
define('IMAGE', $dados['imagem_logo']);
define('RAPIDO', isset($_SESSION["loja-".$contrato]['rapido']));
define('TELEFONE', $dados['telefone']);
define('SITE_CLI', $dados['site']);
define('FRANQUIA_DEFAULT', isset($dados['seg_cota_defaut']) ? $dados['seg_cota_defaut'] : 10);
define('ANO_LIMITE', isset($dados['seg_ano_veiculo_piso']) ? $dados['seg_ano_veiculo_piso'] : 25);
define('TIPO_COMISSAO', $dados['com_forma']);
define('MOD_WHAT', $dados['mdl_wha']);
define('WHAT_PADRAO', $dados['wa_padrao_portal']);
define('AGRUPAR_PLANO', intval($dados['adm_agrupar_plan_item']));
define('MENS_PLANO_CONTRACT', intval($dados['adm_plan_mens_plan_contrat']));
define('MENSAGEM_BOA_VINDAS', $dados['adm_msg_boas_vindas']);
define('ACA_PES_COT_TIP', $dados['aca_pes_cot_tip']);
define('DESC_ISENCAO_IE', $dados['adm_desc_isencao_ie']);
define('MENSAGEM_COTACAO', $dados['adm_msg_cotacao']);
define('MENSAGEM_ASSOCIADO', $dados['adm_associado']);
define('MENSAGEM_CONTRATACAO', $dados['adm_msg_contratacao']);
define('MENSAGEM_ADESAO', $dados['adm_msg_taxa_adesao']);
define('MENSAGEM_TERMO_EMAIL', $dados['adm_msg_termos_email']);
define('MENSAGEM_TERMO_WHATS', $dados['adm_msg_termos_whats']);
define('MENSAGEM_LINK_PAG', $dados['adm_msg_link_pag']);
define('NAO_ALT_PAGAMENTO', intval($dados['adm_nao_alterar_tp_pag']));
define('DESCONTO_RANGE', intval($dados['adm_desconto_range']));
define('SEG_VEI_COTAC_PERSON', intval($dados['seg_vei_cotac_person']));
define('NAO_PAG_ONLINE', intval($dados['nao_pag_online']));
define('NAO_VALOR_PRINT', intval($dados['adm_nao_valor_print']));
define('PORTAL_CONTRATO', intval($dados['portal_contrato']));
define('API', BASE_URL."loja/".CONTRATO."/api/");
define('VIEW', SITE.'View/');
define('CONEXAO_HOSTNAME', $dados['hostname']);
define('CONEXAO_DATABASE', $dados['database']);
define('CONEXAO_USERNAME', $dados['username']);
define('CONEXAO_PASSWORD', $dados['password']);
