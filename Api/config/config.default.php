<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 04/09/2020
 * Time: 11:25
 */

if (basename($_SERVER["PHP_SELF"]) == "config.default.php") {
    die("Este arquivo não pode ser acessado diretamente.");
}

define('HOSTNAME_DEFAULT', '148.72.177.101,6000');
define('USERNAME_DEFAULT', 'erp003');
define('DATABASE_DEFAULT', 'erp003');
define('PASSWORD', '!Password123');
define('KEY_CRIPT', "VipService");
define('KEY_SIVIS', "VipService123");

$host =  (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http')."://".$_SERVER['SERVER_NAME'].'/';

if($_SERVER['SERVER_NAME'] === 'localhost') {
    define('DOMINIO', 'portal');
    define('BASE_URL', $host.DOMINIO.'/');
    define("ERP_URL", "https://sivisweb.com.br/");
} else if ($_SERVER['SERVER_NAME'] === 'portal.sivisweb.com.br'){
    define('DOMINIO', '');
    define('BASE_URL', $host);
    define("ERP_URL", "https://sivisweb.com.br/");
} else if ($_SERVER['SERVER_NAME'] === 'portal2.sivisweb.com.br'){
    define('DOMINIO', '');
    define('BASE_URL', $host);
    define("ERP_URL", "https://teste.sivisweb.com.br/");
} else {
    define('DOMINIO', '');
    define('BASE_URL', $host);
    define("ERP_URL", "https://sivisweb.com.br/");
}
define('SITE', BASE_URL."App/");
