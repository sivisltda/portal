<?php
//definir as rotas publicas
//index,login,quem-indica

$auth         = isset($_SESSION[CHAVE_CONTRATO]['restricted_id']);
$cliente      = $auth && $_SESSION[CHAVE_CONTRATO]['login_ativo'] === "cliente";
$funcionario  = $auth && $_SESSION[CHAVE_CONTRATO]['login_ativo'] === "funcionario";
$gestor       = $auth && $_SESSION[CHAVE_CONTRATO]['login_ativo'] === "gestor";

$modulos = getModulos();

$guard = [
  'rules' => [
    'auth' => $auth,
    'cliente' => $cliente, 
    'funcionario' => $funcionario,
    'gestor' => $gestor
  ],
  'auths' => [
    'auth' => [
      'rotas' => ['dashboard', 'perfil'],
      'rules' => 'auth'
    ],
    'cliente' => [
      'rotas' => ['agenda', 'duvidas', 'forma-pagamentos',
      'meus-dependentes', 'minha-agenda', 'regulamentos', 'veiculos'],
      'rules' => 'cliente'
    ],
    'funcionario' => [
      'rotas' => array_merge(['comissao', 'meus-clientes', 'cliente', 'ordem'],
     $modulos['seg'] ? ['pendencias', 'validador'] : []),
      'rules' => 'funcionario'
    ],
    'gestor' => [
      'rotas' => array_merge(['clientes', 'comissao', 'meus-clientes', 'cliente', 'ordem'],
      $modulos['seg'] ? ['pendencias', 'validador'] : []),
      'rules' => 'gestor'
    ]
  ]
];

$pagesAuth = array_filter($guard['auths'], function($v) use($tipo) {
  return in_array($tipo, $v['rotas']);
});

$auths = array_filter($guard['rules'], function($v) {
  return $v;
});

$isAuth = array_filter($pagesAuth, function($v) use($auths) {
  return in_array($v['rules'], array_keys($auths));
});

if (count($pagesAuth)) {
  if (!count($auths)) {
    throw new Exception('Não autenticado', 401);
  } else if (count($auths) && !count($isAuth)) {
    throw new Exception('Não autorizado', 403);
  }
}

 