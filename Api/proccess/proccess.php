<?php
  requireOnce(__DIR__."/guard.php", compact('tipo'));
  if ($tipo !== 'rapido') {
    $arquivo = __DIR__."/../../App/View/pages/".$tipo.".php";
  } else {
    $tipo = 'index';
  }
  
  function getDadosMenu() {
    $menuService = new MenuService();
    $menu = isset($_SESSION[CHAVE_CONTRATO]['restricted_id']) ? $menuService->makeMenu($_SESSION[CHAVE_CONTRATO]['restricted_id']) : [];
    $menuImage = $menuService->getImageMenu();
    return [
      'menu'  => $menu,
      'image' => $menuImage
    ];
  }


  switch($tipo) {
    case 'quem-indica': 
      if (file_exists(replaceBarraDirectory($arquivo))) {
        $id = isExistsTag($tipo, $partes);
        if (is_numeric($id)) {
          $campanhaService = new CampanhaService();
          $dados = $campanhaService->getCampanhaPorId($id);
          if ($dados) {
            $dados['url'] = BASE_URL.'loja/'.CONTRATO.'/quem-indica/'.$dados['id'];
            $_SESSION[CHAVE_CONTRATO]['campanha'] = [
              'id' => $dados['id'],
              'id_procedencia' => $dados['id_procedencia']
            ];
            if ($consultor = isExistsConsultor($partes)) {
              $dados['consultor'] = $consultor['id'];
              $dados['url'].='/consultor/'.$dados['consultor'];
            }
            requireOnce($arquivo, compact('dados'));
            exit;
          }
        }
        throw new Exception('Campanha inválida', 404);
      }
    break;
    case 'login':
      if (file_exists(replaceBarraDirectory($arquivo))) {
        requireOnce($arquivo);
        exit;
      }
    break;
    case 'validador':
      $portalService = new PortalService();
      $campoExtra = $portalService->getCampoExtraBusca();
      $modulos = getModulos();
      if (file_exists(replaceBarraDirectory($arquivo))) {
        requireOnce($arquivo, compact('campoExtra', 'modulos'));
        exit;
      }
    break;  
    case 'cliente':
      if (file_exists(replaceBarraDirectory($arquivo))) {
        $prospect = isExistsTag('prospect', $partes);
        if (is_numeric($prospect)) {
          $_SESSION[CHAVE_CONTRATO]['prospect'] = $prospect;
        } 
        requireOnce($arquivo, getDadosMenu());
        exit;
      }
    break;  
    case 'meus-clientes':
      $crmService = new CRMService();
      $procedencias = $crmService->getProcedencias();
      $status = $crmService->getStatus();
      requireOnce($arquivo, array_merge(getDadosMenu(), ['procedencias' => $procedencias, 'status' => $status]));
      exit;
    break;
    case 'clientes': 
      $gestorService = new GestorService();
      $data = array_merge(getDadosMenu(), [
        'produtos' => $gestorService->getListaProdutosChunck(),
        'tipos_pagamentos' => $gestorService->getTiposPagamentos(),
        'funcionarios' => $gestorService->getListaFuncionarios()
      ]);
      requireOnce($arquivo, $data);
      exit;
    break;  
    case 'regulamentos':
      $regulamentoService = new RegulamentoService();
      $regulamentos = $regulamentoService->getRegulamentos(); 
      requireOnce($arquivo, array_merge(getDadosMenu(), ['regulamentos' => $regulamentos])); 
    break;
    case 'minha-agenda':
      $grupo = getGrupo();
      requireOnce($arquivo, array_merge(getDadosMenu(), ['grupo' => $grupo])); 
    break;
    case 'agenda':
      $grupo = getGrupo();
      requireOnce($arquivo, array_merge(getDadosMenu(), ['grupo' => $grupo])); 
    break;
    case 'pendencias':
      $funcionarios = (new GestorService())->getListaFuncionariosPorGestor();
      requireOnce($arquivo, array_merge(getDadosMenu(), compact('funcionarios'))); 
    break;
    /* case 'ordem':
      $numero = isExistsTag($tipo, $partes);
      requireOnce($arquivo, array_merge(getDadosMenu(), ['numero' => $numero])); 
    break; */
    case end($partes):
      if (file_exists(replaceBarraDirectory($arquivo))) {
        requireOnce($arquivo, getDadosMenu());
        exit;
      } else {
        throw new Exception('Página não encontrada', 404);
      }
    break;
    default:
      $login = array_search('login', $partes);
      if (!$dadosLoja['adm_venda_online'] && !$login && !$tipo) {
          throw new Exception('Não Autorizado', 401);
      }
      if (isset($_SESSION[CHAVE_CONTRATO]['rapido'])) unset($_SESSION[CHAVE_CONTRATO]['rapido']);
      $rapido = in_array('rapido', $partes);
      if ($rapido && isset($_SESSION[CHAVE_CONTRATO]['login_ativo']) 
      && $_SESSION[CHAVE_CONTRATO]['login_ativo'] === 'funcionario') {
          $_SESSION[CHAVE_CONTRATO]['rapido'] = true;
      }
      if (isset($_SESSION[CHAVE_CONTRATO]['consultor'])) unset($_SESSION[CHAVE_CONTRATO]['consultor']);
      $consultor = isExistsConsultor($partes);
      if ($consultor) {
        $_SESSION[CHAVE_CONTRATO]['consultor']  = $consultor;       
      } 
      $prospect = isExistsTag('prospect', $partes);
      if (is_numeric($prospect)) {
        $_SESSION[CHAVE_CONTRATO]['prospect'] = $prospect;
      }
      if (isset($_SESSION[CHAVE_CONTRATO]['produto'])) unset($_SESSION[CHAVE_CONTRATO]['produto']);
      $produto = isExistsProduto($partes);
      if ($produto) {   
        $_SESSION[CHAVE_CONTRATO]['produto'] = $produto['id'];     
      }
      requireOnce(__DIR__."/../../APP/View/pages/index.php");
      exit;
    break;
  }