<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 18/09/2020
 * Time: 16:07
 */

class DependenteService
{
    private $dependenteRepository;
    private $planoRepository;
    private $usuarioRepository;
    private $produtoRepository;

    public function __construct(){
        $this->dependenteRepository = new DependenteRepository();
        $this->planoRepository = new PlanoRepository();
        $this->usuarioRepository = new UsuarioRepository();
        $this->produtoRepository = new ProdutoRepository();
    }

    public function getInicioDashboard($id_usuario) {
        try {
            //se for plano único verificar as regras de dependentes se tiver habilitado
            //trazer as informações dos dependentes
            $modulos = getModulos();
            $hasDependente = $this->dependenteRepository->getHasDependente($id_usuario);
            if ($hasDependente) {
                $dados = $this->getListaDependentes($id_usuario);   
                $dados['adicionar'] = $modulos['sau'] && ADD_DEPENDENTE == "0";
                return $dados;
            }
            throw new Exception('Recurso não permitido', 403);
        } catch (Exception $e) {
            $code = $e->getCode() ? $e->getCode() : 500;
            throw new Exception($e->getMessage(), $code);
        }
    }

    /**
     * Criar os dependentes
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function criarDependente($data) {
        try {
            $retorno = null;
            Conexao::initTransaction(); 
            $usuario = $this->usuarioRepository->getUsuarioPorId($data['id_titular']);
            if ($this->dependenteRepository->getDependenteIsPayment($data['id_titular'])) {
                $data['tipo'] = 'P';
            } else {
                $data['tipo'] = $usuario['tipo'];
            }
            if ($data['tipo'] == 'C') {
                $data['dt_convert_cliente'] = escreverDataHora(date('Y-m-d H:i:s'));
            }
            if (isset($data['id_dependente']) && $data['id_dependente']) {
                $data['razao_social'] = $data['nome'];
                $this->usuarioRepository->salvarUsuario($data, $data['id_dependente']);
                $this->dependenteRepository->saveDependente($data);
                $retorno = $data['id_dependente'];              
            } else {
                $data = array_merge($data, [
                    'juridico_tipo' => "F", 'razao_social' => $data['nome'], 'data_nascimento' => $data['data_nascimento'],
                    'fornecedores_status' => 'Dependente', 'socio' => 1, 'titular' => 0, 'tipo_socio' => 4
                ]);
                $userId = $this->usuarioRepository->salvarUsuario($data);
                $dataDependente = ['id_titular' => $data['id_titular'], 'id_dependente' => $userId, 'compartilhado' => 1, 'data_inclusao' => date('d-m-Y H:i:s')];
                if(isset($data['parentesco'])) {
                    $dataDependente['parentesco'] = $data['parentesco'];
                }
                $this->dependenteRepository->saveDependente($dataDependente);
                $retorno = $dataDependente['id_dependente'];
            }
            Conexao::commit();
            return $this->dependenteRepository->getDependente('id_titular = '.valoresSelect2($data['id_titular']). ' and id_dependente = '.valoresSelect2($retorno));
        }catch (Exception $e) {
            Conexao::rollback();
            throw new Exception($e);
        }
    }


    public function getListaDependentes($id_usuario) {
        try {
            $dados = [];
            $condicao = 'id_titular = '.valoresSelect2($id_usuario);
            $dados['lista'] = array_map(function($data) {
                $data['cnpj'] = $data['cnpj'] ? substr($data['cnpj'], 0,1).'xx.xxx.xxx-'.substr($data['cnpj'], -2) : '';
                return $data;
            }, $this->dependenteRepository->listaDependentes($condicao));
            $dados['parentesco'] = $this->dependenteRepository->listaParentescos();           
            if (DEPENDENTE_PARENTESCO) {
                $plano = $this->planoRepository->getPlano('dt_cancelamento is null and favorecido = '.valoresSelect2($id_usuario));
                $produto = $this->produtoRepository->getProduto('conta_produto = '.valoresSelect2($plano['id_produto']), 0);
                $dados['regra'] = $this->dependenteRepository->getRegrasDependentePlano("id_produto = ".valoresSelect2($plano['id_produto']));
                $dados['pes_jur'] = $produto['pes_jur'];
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function verificaDadosDependente($data) {
        try {
            $dadosUsuario = $this->usuarioRepository->selecionaUsuarioPorEmailOuCPF($data['email'], $data['cnpj']);
            if ($dadosUsuario) {
                $id_usuario = isset($data['id_dependente']) ? $data['id_dependente'] : null; 
                if (PLANO_UNICO) {
                    $condicao  = 'favorecido = '.valoresSelect2($dadosUsuario['id'])
                    .' or favorecido = (select top 1 id_titular from sf_fornecedores_despesas_dependentes 
                    where id_dependente = '.valoresSelect2($dadosUsuario['id']).')';
                    $planos = $this->planoRepository->getPlanos($condicao, [], 0);
                    if (count($planos) > 0 && $dadosUsuario['id'] != $id_usuario) {
                        throw new Exception('O dependente já possui plano', 403);
                    }
                }
                return $id_usuario === $dadosUsuario['id'] ? [] : $dadosUsuario;
            } 
            throw new Exception('Usuário não encontrado', 404);            
        } catch(Exception $e) {
            $code = $e->getCode() ? $e->getCode() : 500;
            throw new Exception($e->getMessage(), $code);
        }
    }

    public function removeDependente($data) {
        try {
            if ($this->dependenteRepository->verificaDependente($data)) {
                $this->dependenteRepository->removeDependente($data);
                salvarLog(['tabela' => 'sf_fornecedores_despesas_dependentes', 'id_item' => $data['id_dependente'],
                'usuario' => getLoginUser(), 'acao' => 'R', 'descricao' => 'EXCLUSAO DEPENDENTE:'.$data['id_dependente'],
                'id_fornecedores_despesas' => $data['id_titular']]);
                $this->usuarioRepository->atualizarStatus('id_fornecedores_despesas = '.valoresSelect2($data['id_dependente']));
                return true;
            }
            throw new Exception('Dependente não encontrado', 404);
        } catch (Exception $e) {
            $code = $e->getCode() ? $e->getCode() : 500;
            throw new Exception($e->getMessage(), $code);
        }
    }
}