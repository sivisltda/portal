<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 11/09/2020
 * Time: 09:50
 */

class DashboardService
{
    private $usuarioRepository;
    private $planoRepository;
    private $carteiraService;
    private $campanhaRepository;

    public function __construct() {
        $this->usuarioRepository = new UsuarioRepository();
        $this->planoRepository = new PlanoRepository();
        $this->carteiraService = new CarteiraService();
        $this->campanhaRepository = new CampanhaRepository();
    }

    public function getDashboard() {
        try {
            $modulos = getModulos();
            $dados = [];
            $id_usuario = $_SESSION[CHAVE_CONTRATO]['restricted_id'];
            $dados['usuario'] = $this->usuarioRepository->getUsuarioPorId($id_usuario);

            if($_SESSION[CHAVE_CONTRATO]['login_ativo'] === 'cliente') {
                $where = "dt_cancelamento is null and (favorecido = ".valoresSelect2($id_usuario)
                    ." or favorecido in (select id_titular from sf_fornecedores_despesas_dependentes where id_dependente = "
                    .valoresSelect2($id_usuario)."))";
                if ($modulos['mil']) {
                    unset($dados['usuario']['email']);
                    unset($dados['usuario']['cnpj']);
                    unset($dados['usuario']['celular']);
                }
                $dados['plano'] = [
                    'tipo'      => PLANO_UNICO ? 'unico' : 'multiplo',
                    'modelo'    => $modulos['seg'] ? 'seguro' : 'clube',
                    'data'      => PLANO_UNICO ?
                     current($this->planoRepository->getPlanos($where)) : []                ];
                $dados['carteiras'] = $this->carteiraService->getDadosCarteira(['id_usuario' => $id_usuario]);   
            } else {
                $campanhas = $this->campanhaRepository->getCampanhas('cast(getdate() as date) between data_ini 
                and data_fim and inativo = 0');
                $dados['links'] = array_map(function($item) {
                    return [
                     'titulo' => $item['nome_procedencia'],
                     'url' =>  BASE_URL."loja/".CONTRATO."/quem-indica/".$item['id']
                     ."/consultor/".$_SESSION[CHAVE_CONTRATO]['indicador']['id_fornecedores_despesas']
                    ];
                }, $campanhas);
            }
            return $dados;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }

    }
}