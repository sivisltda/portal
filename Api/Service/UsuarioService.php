<?php

class UsuarioService {

    protected $usuarioRepository;
    protected $planoRepository;
    protected $veiculoRepository;

    public function __construct() {
        $this->usuarioRepository = new UsuarioRepository();
        $this->planoRepository = new PlanoRepository();
        $this->veiculoRepository = new VeiculoRepository();
    }

    public function buscarUsuario($data) {
        try {
            $dados = [];
            $modulos = getModulos();         
            if ($dados = $this->usuarioRepository->getUsuarioPorEmail($data['email'])) {
                $idUsuario = isset($dados['id_fornecedores_despesas']) ? $dados['id_fornecedores_despesas'] : $dados['id'];
                if (!isset($dados['hasCnpj']) && $modulos['seg']) {
                    $dados['veiculos'] =  $this->veiculoRepository->getVeiculosPorDados('v.id_fornecedores_despesas = '.valoresSelect2($idUsuario).
                    " AND (planos_status is null OR planos_status = 'novo')", [], true);
                } 
            }
            return $dados;
        } catch (Exception $e) {
            $code = $e->getCode() ? $e->getCode() : 500;
            throw new Exception($e->getMessage(), $code);
        }
    }

    public function buscarDadosUsuario($data) {
        try {
            $dados = [];
            $modulos = getModulos();
            if ($dados = $this->usuarioRepository->getUsuarioPorCNPJ($data['cnpj'], $data['id_usuario'])) {
                if (PLANO_UNICO) {
                    $dadosPlano = $this->usuarioRepository->verificaClientePlanoAtivo($dados['id']);
                    if (count($dadosPlano)) {
                        throw new Exception('Plano já ativado', 403);
                    }
                }
                if ($modulos['seg']) {
                    $dados['veiculos'] = $this->veiculoRepository->getVeiculosPorDados('v.id_fornecedores_despesas = ' .valoresSelect2($data['id_usuario']).
                        " AND (planos_status is null OR planos_status = 'novo')", [], true);
                } else {
                    unset($dados['cnpj']);
                    $dados['planos'] = $this->planoRepository->getPlanos("(favorecido = ".valoresSelect2($data['id_usuario'])
                    ." or favorecido in (select id_titular from sf_fornecedores_despesas_dependentes where id_dependente = ".valoresSelect2($data['id_usuario'])
                    .")) and A.planos_status not in ('Inativo', 'Cancelado') and B.mostrar_site = 1");
                }              
                return $dados;
            } 
            throw new Exception('CPF informado não corresponde a essa conta', 401);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function salvarUsuario($data) {
        try {
            $dados = [];
            $modulos = getModulos();            
            $usuario_id = isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario']) ? $_SESSION[CHAVE_CONTRATO]['usuario'] : null;
            if (!$usuario_id) {
                if (isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['consultor'])) {
                    $data['id_user_resp'] = $_SESSION[CHAVE_CONTRATO]['consultor']['id_usuario'];
                    $data['empresa'] = $_SESSION[CHAVE_CONTRATO]['consultor']['filial'];
                    $data['prof_resp'] = $_SESSION[CHAVE_CONTRATO]['consultor']['id'];
                }
                $data['socio'] = ($modulos['seg'] ? 0 : 1);
                $data['titular'] = ($modulos['seg'] ? 0 : 1);
            } else if (PLANO_UNICO) {
                $dadosPlano = $this->usuarioRepository->verificaClientePlanoAtivo($usuario_id);
                if (count($dadosPlano)) {
                    throw new Exception('Plano já ativado', 403);
                }
            }
            if ($usuario_id = $this->usuarioRepository->salvarUsuario($data, $usuario_id)) {
                $_SESSION[CHAVE_CONTRATO]['usuario'] = $usuario_id;
                $data['id_fornecedores_despesas'] = $usuario_id;
                $dados['id_fornedores_despesas'] = $usuario_id;
                if ($modulos['seg'] && $data['veiculo']) {
                    $veiculo = $data['veiculo'];
                    $veiculo['id_fornecedores_despesas'] = isset($veiculo['id_fornecedores_despesas']) ? $veiculo['id_fornecedores_despesas'] : $usuario_id;
                    $id_veiculo = isset($veiculo['id']) ? $veiculo['id'] : null;
                    $dados['id_veiculo'] = $this->veiculoRepository->salvarVeiculo($veiculo, $id_veiculo);
                    $this->planoRepository->atualizaDiaVencimento($dados['id_veiculo'], $data['vencimento']);
                }
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

     /**
     * Faz o login do usuário
     * @param string $email
     * @param string $senha
     * @return bool
     * @throws Exception
     */
    public function login($email, $senha) {
        try {
            if(!empty($senha)){
                $senha = str_replace(".",'', $senha);
                $senha = str_replace("-",'', $senha);
                $senha = str_replace("/", '', $senha);
            }
            $dados = $this->usuarioRepository->getUsuarioPorCredencial(['email' => $email, 'senha' => $senha]);  
            //var_dump($dados); exit;      
            $accept = 0;
            $fornecedores_despesas = null;
            $nome =  '';
            $tipo = [];
            foreach($dados as $row) {
                if(!$fornecedores_despesas) {
                    $tipo[] = $row['tipo'];
                    $fornecedores_despesas = $row['id_fornecedores_despesas'];
                    $nome = $row['razao_social'];
                }
                if($row['tipo'] === 'E' || $row['tipo'] === 'I' || $row['tipo'] === 'G') {
                    $tipo[] = $row['tipo'];
                    $_SESSION[CHAVE_CONTRATO]['indicador'] = [
                        'id_fornecedores_despesas' => $row['id_fornecedores_despesas'],
                        'login_usuario'            => $row['login_user'],
                        'id_usuario'               => $row['id_usuario']
                    ];
                }
                if ($accept == 0) {
                    $this->usuarioRepository->setUsuarioLogin(['email' => $email, 'login_user' => $row['login_user']]);
                }
                $accept = 1;               
            }
            if ($accept == 1) {
                $_SESSION[CHAVE_CONTRATO]['login_ativo'] = in_array('E', $tipo) || in_array('I', $tipo) ? 'funcionario' : (in_array('G', $tipo) ? 'gestor' : 'cliente');
                $_SESSION[CHAVE_CONTRATO]['restricted_id'] = $fornecedores_despesas ? $fornecedores_despesas : $_SESSION[CONTRATO]['indicador']['id_fornecedores_despesas'];
                $_SESSION[CHAVE_CONTRATO]['nome'] = $nome;
                return true;
            } else {
                return false;
            }
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getConsultaDadosValidador($data) {
        try {
            $modulos = getModulos();
            $isVeiculo = false;
            $condicao = "";
            if (isset($data['id_cliente']) && is_numeric($data['id_cliente'])) {
                $condicao = "f.id_fornecedores_despesas = ".valoresSelect2($data['id_cliente']);
            } else {
                $data['busca'] = trim($data['busca']);
                if (Validation::validaCpf($data['busca'])) {
                    $data['busca'] = str_replace(".",'', $data['busca']);
                    $data['busca'] = str_replace("-",'', $data['busca']);
                    $data['busca'] = str_replace("/", '', $data['busca']);
                    $condicao = "REPLACE(REPLACE(REPLACE(cnpj, '.', '') , '-', ''), '/', '') = ".valoresTexto2($data['busca']);
                } else if (Validation::validaEmail($data['busca'])) {
                    $condicao = "conteudo_contato = ".valoresTexto2($data['busca']);
                } else if (Validation::validaCelular($data['busca'])) {
                    $condicao = "conteudo_contato = ".valoresTexto2($data['busca']);
                } else if ($modulos['seg'] && Validation::validaPlaca($data['busca'])) {
                    $isVeiculo = true;
                    $condicao = "REPLACE(placa, '-', '') = ".valoresTexto2(str_replace('-', '', $data['busca']));
                } else {
                    $condicao = "(";
                    if (is_numeric($data['busca'])) {
                        $condicao .= "f.id_fornecedores_despesas = ".valoresSelect2($data['busca'])." or ";
                    } else {
                        $condicao .= "f.razao_social like ".valoresTexto2($data['busca'])." collate Latin1_General_CI_AI or ";
                    } 
                    $condicao .= "conteudo_campo = ".valoresTexto2($data['busca']).")";
                } 
            }
            $dados = $this->usuarioRepository->getConsultaDadosValidador($condicao);
            if (count($dados)) {
                $image = getImages(CONTRATO, 'Clientes', $dados['id_fornecedores_despesas']);
                if ($image && count($image)) {
                    $dados['image'] = $image[0]['url'];
                } else {
                    $dados['image'] = SITE.'/assets/images/users/person.png';
                }
                if ($isVeiculo) {
                    $dados['isVeiculo'] = 1;
                }
                return $dados;
            }
            throw new Exception('Usuário não encontrado', 404);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function consultaClientes($data) {
        try {
            $nome = isset($data['q']) ? utf8_decode($data['q']) : '';
            $condicao = "tipo in ('C', 'P') AND razao_social like '".$nome."%'";
            $dados = $this->usuarioRepository->listaUsuariosPesquisa($condicao);
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}


