<?php

class RegulamentoService {

    public function getRegulamentos() {
        try {
            $getFields = [
                'functionPage' => 'buscarRegulamentos',
                'loja' => CONTRATO
            ];    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, ERP_URL.'Modulos/Sivis/ws_api.php?'.http_build_query($getFields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $res = curl_exec($ch);     
            $dados = json_decode($res, 1);
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}