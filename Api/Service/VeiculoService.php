<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 29/10/2019
 * Time: 12:28
 */

class VeiculoService
{
    private $veiculoRepository;
    private $produtoRepository;

    public function __construct()
    {
        $this->veiculoRepository = new VeiculoRepository();
        $this->produtoRepository = new ProdutoRepository();
    }

    public function getInformacoesVeiculoPlaca($placa) {
        try {
            $json = file_get_contents(ERP_URL."Modulos/Seguro/ajax/fipejson.php?functionPage=getPlaca&codPlaca=".$placa . "&contrato=" . CONTRATO);
            if($json) {
                $apiDados = json_decode($json, 1);
                if($apiDados['LicensePlate']) {
                    return [
                        'placa'         => $placa,
                        'ano_modelo'    => $apiDados['YearModel'] ? $apiDados['YearModel'] : $apiDados['Ano'],
                        'marca'         => escreverTexto($apiDados['Marca']),
                        'brand'         => escreverTexto($apiDados['Brand']),
                        'modelo'        => escreverTexto($apiDados['Modelo']),
                        'model'         => escreverTexto($apiDados['Model']),
                        'codigo_fipe'   => $apiDados['FipeCode']
                    ];
                }
            }
            return [];
        }catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getVeiculos($data) {
        try {
            $sLimit = 0;
            $sQtd = 6;
            if (isset($data['iDisplayLength']) && $data['iDisplayLength'] != '-1') {
                $data['page'] = (isset($data['page']) && is_numeric($data['page'])) ? $data['page'] : 1;
                $sQtd   = $data['iDisplayLength'];
                $sLimit = (intval($data['page']) - 1) * $sQtd;
            }
            $id      = (int) $_SESSION[CHAVE_CONTRATO]['restricted_id'];
            $dados   = $this->veiculoRepository->getVeiculosPorDados('v.id_fornecedores_despesas = '
                .valoresSelect2($id).' AND id_plano is not null', ['limit' => $sLimit, 'qtd' => $sQtd]);
            if (isset($dados['aaData'])) {
                $dados['aaData'] = array_map(function($item) {
                    $item['servicos'] = $this->produtoRepository->getServicosCobertura('id_produtomp = '.valoresSelect2($item['produto_id']));
                    return $item;
                }, $dados['aaData']);
            } else if (count($dados)) {
                $dados = array_map(function($item) {
                    $item['servicos'] = $this->produtoRepository->getServicosCobertura('id_produtomp = '.valoresSelect2($item['produto_id']));
                    return $item;
                }, $dados);
            }
            if (isset($data['iDisplayLength'])) {
                $dados["sEcho"] =  intval(filter_input(INPUT_GET, 'sEcho'));
                $dados['pagination'] = [
                    'current'   => intval($data['page']),
                    'next'      => paginationNext($dados['total'], intval($data['page']), $sQtd),
                    'prev'      => paginationPrev($dados['total'], intval($data['page']), $sQtd),
                    'qtd_pages' => paginationQtd($dados['total'], $sQtd)
                ];
            } 
            return $dados;

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    public function excluirVeiculo($data) {
        try {
            $veiculo = $this->veiculoRepository->getVeiculoPorDados('id', $data['id_veiculo']);
            if ($veiculo['id_fornecedores_despesas'] != $data['usuario_id']) {
                throw new Exception('Veiculo não pertencente', 403);
            }
            
        } catch (Exception $e) {
            $code = $e->getCode() ? $e->getCode() : 500;
            throw new Exception($e->getMessage(), $code);
        }
    }
}