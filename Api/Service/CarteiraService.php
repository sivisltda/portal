<?php
class CarteiraService {
    private $carteiraRepository;
    
    public function __construct() {
        $this->carteiraRepository = new CarteiraRepository();
    }

    public function getDadosCarteira($data) {
        try {
            $carteiras = $this->carteiraRepository->getDadosCarteira();
            if (count($carteiras)) {
                $dados = $this->carteiraRepository->getDadosUsuarioCarteira($data['id_usuario']);
                if (count($dados)) {    
                    $data['contrato'] = CONTRATO;
                    $carteiraPadrao = current(array_filter($carteiras, function($item) {
                        return !$item['id_grupo'];
                    }));
                    $dados = array_map(function($item) use($carteiras, $carteiraPadrao, $data) {
                        $carteira = current(array_filter($carteiras, function($itemCarteira) use($item){
                            return $itemCarteira['id_grupo'] == $item['conta_movimento'];
                        }));
                        $carteira = $carteira ? $carteira : $carteiraPadrao;
                        if ($carteira) {
                            $data = array_merge($carteira, $data, ['id_plano' => $item['id_plano']]);
                            return $this->getItemCarteira($data);
                        }
                        return false;
                    }, $dados);                    
                    return array_filter($dados);
                }
                return [];
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function getItemCarteira($data) {
        try {
            $dados['id']            = $data['id'];
            $dados['descricao']     = $data['descricao'];
            $dados['mensagem']      = $this->getTemplateCarteira($data);
            $dados['foto_capa']     = ERP_URL.'Pessoas/'.$data['contrato']
            .'/Empresa/Cartao/'.$data['id'].'/'.$data['foto_capa'];
            $dados['foto_verso']    = ERP_URL.'Pessoas/'.$data['contrato']
            .'/Empresa/Cartao/'.$data['id'].'/'.$data['foto_verso'];
            $dados['margin_top']    = $data['margin_top'];
            $dados['margin_left']   = $data['margin_left'];
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function getTemplateCarteira($data) {
        try {
            $getFields = [
                'functionPage' => 'getInfoCarteira',
                'loja' => $data['contrato'],
                'id_plano' => $data['id_plano'],
                'id_carteira' => $data['id'],
                'id_usuario' => $data['id_usuario']
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, ERP_URL.'Modulos/Sivis/ws_api.php?'.http_build_query($getFields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $res = curl_exec($ch);
            $res = str_replace(['border="1"', 'width:500px'], ['border="0"', ''], $res);   
            return $res;        
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }
}