<?php
    class FuncionarioService {

        private $funcionarioRepository;
        private $usuarioRepository;
        private $leadRepository;
        private $companhaRepository;

        public function __construct() {
            $this->funcionarioRepository = new FuncionarioRepository();
            $this->usuarioRepository = new UsuarioRepository();
            $this->leadRepository = new LeadRepository();
            $this->companhaRepository = new CampanhaRepository();
        }

        public function getComissaoFuncionario($data, $sLimit, $sQtd) {
            try {   
                $dadosFuncionario = $this->funcionarioRepository->getUsuarioFuncionario($_SESSION[CHAVE_CONTRATO]['indicador']['id_fornecedores_despesas']);
                $getFields = [
                    'functionPage' => 'getComissaoFuncionario',
                    'loja' => CONTRATO,
                    'id_usuario' => $dadosFuncionario['id_usuario'],
                    'txtUsuario' => $dadosFuncionario['id_usuario'],
                    'dti' => $data["inicio"],
                    'dtf' => $data["fim"]
                ];          
                $ch = curl_init();
                if (TIPO_COMISSAO == 1) {
                    curl_setopt($ch, CURLOPT_URL, ERP_URL.'Modulos/Seguro/Comissoes_clientes_all_server_processing.php?'.http_build_query($getFields));
                } else {
                    curl_setopt($ch, CURLOPT_URL, ERP_URL.'Modulos/Seguro/Comissoes_server_processing.php?'.http_build_query($getFields));
                }
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                $res = curl_exec($ch);
                $dados = json_decode($res, 1);
                $output = [
                    "sEcho" => intval(filter_input(INPUT_GET, 'sEcho')),
                    "planos" => []
                ];                                                
                //--------------------------------------------------------------  
                $aaData = [];
                $aaDataPage = [];
                $iFilter = 0;
                $num_comissao_plano_pri = 0;
                $num_comissao_plano = 0;
                $num_comissao_servico = 0;
                $num_comissao_produto = 0;                  
                $comissao_plano_pri = 0.00;
                $comissao_plano = 0.00;
                $comissao_servico = 0.00;
                $comissao_produto = 0.00;                
                $receber = 0.00;
                $recibido = 0.00;
                $reprovado = 0.00;                
                foreach ($dados["aaData"] as $item) {
                    if (valoresNumericos2($item["comissao_plano_pri"]) > 0) {
                        $comissao_plano_pri += valoresNumericos2($item["comissao_plano_pri"]);
                        $num_comissao_plano_pri++;
                    }
                    if (valoresNumericos2($item["comissao_plano"]) > 0) {
                        $comissao_plano += valoresNumericos2($item["comissao_plano"]);
                        $num_comissao_plano++;
                    }
                    if (valoresNumericos2($item["comissao_servico"]) > 0) {
                        $comissao_servico += valoresNumericos2($item["comissao_servico"]);
                        $num_comissao_servico++;
                    }
                    if (valoresNumericos2($item["comissao_produto"]) > 0) {
                        $comissao_produto += valoresNumericos2($item["comissao_produto"]);
                        $num_comissao_produto++;
                    }
                    if ($item["status"] == "Pendente" || $item["status"] == "Aguarda") {
                        $receber += valoresNumericos2($item["comissao_plano_pri"]) + valoresNumericos2($item["comissao_plano"]) + valoresNumericos2($item["comissao_servico"]) + valoresNumericos2($item["comissao_produto"]);
                    } else if ($item["status"] == "Aprovado") {
                        $recibido += valoresNumericos2($item["comissao_plano_pri"]) + valoresNumericos2($item["comissao_plano"]) + valoresNumericos2($item["comissao_servico"]) + valoresNumericos2($item["comissao_produto"]);
                    } else if ($item["status"] == "Reprovado") {
                        $reprovado += valoresNumericos2($item["comissao_plano_pri"]) + valoresNumericos2($item["comissao_plano"]) + valoresNumericos2($item["comissao_servico"]) + valoresNumericos2($item["comissao_produto"]);
                    }
                }                
                $output['comissoes'][0]["id"] = "comissao_plano_pri";
                $output['comissoes'][0]["quantidade"] = $num_comissao_plano_pri;
                $output['comissoes'][0]["status"] = "1ª Parcela";
                $output['comissoes'][0]["valor"] = escreverNumero($comissao_plano_pri, 1);                
                $output['comissoes'][1]["id"] = "comissao_plano";
                $output['comissoes'][1]["quantidade"] = $num_comissao_plano;
                $output['comissoes'][1]["status"] = "Plano";
                $output['comissoes'][1]["valor"] = escreverNumero($comissao_plano, 1);                
                $output['comissoes'][2]["id"] = "comissao_servico";
                $output['comissoes'][2]["quantidade"] = $num_comissao_servico;
                $output['comissoes'][2]["status"] = "Serviço";
                $output['comissoes'][2]["valor"] = escreverNumero($comissao_servico, 1);                
                $output['comissoes'][3]["id"] = "comissao_produto";
                $output['comissoes'][3]["quantidade"] = $num_comissao_produto;
                $output['comissoes'][3]["status"] = "Produto";
                $output['comissoes'][3]["valor"] = escreverNumero($comissao_produto, 1);                               
                $output['comissoes'][4]["id"] = "comissao";
                $output['comissoes'][4]["quantidade"] = ($num_comissao_plano_pri + $num_comissao_plano + $num_comissao_servico + $num_comissao_produto);
                $output['comissoes'][4]["status"] = "Total";
                $output['comissoes'][4]["valor"] = escreverNumero(($comissao_plano_pri + $comissao_plano + $comissao_servico + $comissao_produto), 1);              
                //-------------------------------------------------------------- 
                $output['valoresReceber']["receber"] = $receber; 
                $output['valoresReceber']["recibido"] = $recibido; 
                $output['valoresReceber']["reprovado"] = $reprovado; 
                $output['valoresTotal'] = ($receber + $recibido + $reprovado);
                //--------------------------------------------------------------
                $filtros = explode(",", $data["tipoComissao"]);
                for ($i = 0; $i < count($dados["aaData"]); $i++) {
                    if (!isset($data["tipoComissao"]) || ((valoresNumericos2($dados["aaData"][$i]["comissao_plano_pri"]) > 0 && in_array("comissao_plano_pri", $filtros)) ||
                        (valoresNumericos2($dados["aaData"][$i]["comissao_plano"]) > 0 && in_array("comissao_plano", $filtros)) ||                                
                        (valoresNumericos2($dados["aaData"][$i]["comissao_servico"]) > 0 && in_array("comissao_servico", $filtros)) ||   
                        (valoresNumericos2($dados["aaData"][$i]["comissao_produto"]) > 0 && in_array("comissao_produto", $filtros)))) {
                            $aaData[] = $dados["aaData"][$i];
                            $iFilter++;
                    }
                }
                for ($i = $data["iDisplayStart"]; $i < ($data["iDisplayStart"] + $data["iDisplayLength"]); $i++) {
                    if (isset($aaData[$i])) {
                        $aaDataPage[] = $aaData[$i];
                    }
                }
                $output['aaData'] = $aaDataPage;
                //--------------------------------------------------------------
                $output['iTotalRecords'] = $dados['iTotalRecords'];
                $output['iTotalDisplayRecords'] = $iFilter;
                $output['usuario'] =  [
                    'razao_social'  => $dadosFuncionario['razao_social'],
                    'id'            => $dadosFuncionario['id_fornecedores_despesas']
                ];
                return $output;
                
                /*$sDate = "";
                $sTipo = "";
                
                if (isset($data["inicio"]) && isset($data["fim"])) {
                    $inicio = valoresData2($data["inicio"]);
                    $fim = valoresDataHora2($data["fim"], '23:59:59');
                    $sDate = "and data_venda BETWEEN $inicio and $fim ";
                }

                if (isset($data['tipoComissao'])) {
                    $tipos = explode(',', $data['tipoComissao']);
                    $sTipo = "(".implode(" or ", array_map(function($item) {
                        return trim($item)." >  0";
                    }, $tipos)).")";
                }

                $funcionarioId = $_SESSION[CHAVE_CONTRATO]['indicador']['id_fornecedores_despesas'];
                $dadosFuncionario = $this->funcionarioRepository->getUsuarioFuncionario($funcionarioId);
                if (TIPO_COMISSAO == 1) {
                    $this->funcionarioRepository->criaTabelaProvissionamentoCliente($dadosFuncionario, $data);
                    $this->funcionarioRepository->criaTabelaComissaoCliente($dadosFuncionario, $sDate);
                } else {
                    $this->funcionarioRepository->criaTabelaProvissionamento($dadosFuncionario, $data);
                    $this->funcionarioRepository->criaTabelaComissao($dadosFuncionario, $sDate);
                }
                $output['aaData']    = $this->funcionarioRepository->listaPlanosPorFuncionarioTipo($sLimit, $sQtd, $sTipo);
                $output['comissoes'] = $this->funcionarioRepository->getlistaTipoComissao($sTipo);
                $output['planos']    = $this->funcionarioRepository->comissaoFuncionarioPorTipo($sTipo);
                $dadosTotais         = $this->funcionarioRepository->totalComissao($sTipo);
               
                $output['valoresReceber']['recibido']   = isset($dadosTotais['pago']) ? $dadosTotais['pago'] : 0;
                $output['valoresReceber']['receber']    = isset($dadosTotais['pendente']) ?  $dadosTotais['pendente'] : 0;
                $output['valoresReceber']['reprovado']  = isset($dadosTotais['reprovado']) ?  $dadosTotais['reprovado'] : 0;
                $output['valoresTotal']                 = isset($dadosTotais['valor_total']) ?  $dadosTotais['valor_total']: 0;
                $output['iTotalRecords']                = isset($dadosTotais['quantidade']) ? $dadosTotais['quantidade'] : 0;
                $output['iTotalDisplayRecords']         = isset($dadosTotais['quantidade']) ? $dadosTotais['quantidade'] : 0;*/                
            } catch (Exception $e) {
                throw new Exception($e->getMessage(), $e->getCode());
            }
        }

        public function getListaProspectsLeads($data, $sLimit, $sQtd) {
            try {
                $sTipo = "";
                $paginacao = [];
                if (isset($data["inicio"]) && isset($data["fim"])) {
                    $inicio = valoresData2($data["inicio"]);
                    $fim = valoresDataHora2($data["fim"], '23:59:59');
                    $sTipo = "dt_cadastro BETWEEN $inicio and $fim ";
                }
                $funcionarioId = $_SESSION[CHAVE_CONTRATO]['indicador']['id_fornecedores_despesas'];
                $sTipo.= ($sTipo ? ' and' : '')." funcionario = ".valoresSelect2($funcionarioId);
                if (isset($data['procedencia']) && $data['procedencia']) {
                    $sTipo.= ($sTipo ? ' and' : '')." id_procedencia = ".valoresSelect2($data['procedencia']);
                }
                if (isset($data['status'])) {
                    $sTipo.= ($sTipo ? ' and' : '')." status in (".filtroWhereIn($data['status']).")";
                }
                if (isset($data['busca']) && $data['busca']) {
                    $sTipo.= ($sTipo ? ' and' : '')." (razao_social like '".utf8_decode($data['busca'])."%' collate Latin1_General_CI_AI
                    or email like '".utf8_decode($data['busca'])."%' collate Latin1_General_CI_AI 
                    or nome_indicador like '".utf8_decode($data['busca'])."%' collate Latin1_General_CI_AI)"; 
                }
                if ($sLimit || $sQtd) {
                    $paginacao = [
                        'limit' => $sLimit,
                        'qtd' => $sQtd
                    ];
                }
                $dados = $this->funcionarioRepository->getListaProspectsLeads($sTipo, $paginacao);
                $dados['status'] = $this->funcionarioRepository->getStatusProspectsLeads();
                return $dados;
            } catch (Exception $e) {
                throw new Exception($e->getMessage(), $e->getCode());
            }
        }

        public function convertLeadProspect($data) {
            try {
                Conexao::initTransaction();
                if ($_SESSION[CHAVE_CONTRATO]['indicador']['id_usuario']) {
                    $data['id_user_resp'] = $_SESSION[CHAVE_CONTRATO]['indicador']['id_usuario'];
                } else {
                    $data['indicador']= $_SESSION[CHAVE_CONTRATO]['indicador']['id_fornecedores_despesas'];
                }
                $id_usuario = $this->usuarioRepository->salvarUsuario($data);
                $this->leadRepository->updateProspectLead($id_usuario, $data['id_lead']);
                if (isset($data['id_lead_indicador'])) {
                    $campanha = $this->companhaRepository
                    ->getCampanha('p.id_procedencia = '.valoresSelect2($data['procedencia'])
                    .' and getdate() between data_ini and data_fim and lc.inativo = 0');
                    if ($campanha) {
                        $infoCupom = $this->companhaRepository->getQtdCupons($campanha['id']);
                        $disponivel = $infoCupom['disponivel'];
                        for($i=1; $i<= $campanha['conv_led_pro']; $i++) {
                            if ($disponivel > 0) {
                                $this->companhaRepository
                                ->criarCupom($campanha['id'], $data['id_lead_indicador'], $data['id_lead']);
                                $disponivel--;
                            }
                        }
                    }
                }
                Conexao::commit();
                return $id_usuario;
            } catch (Exception $e) {
                Conexao::rollback();
                throw new Exception($e->getMessage(), $e->getCode());
            }
        }

        public function inativarUsuario($data) {
            try {   
                if ($data['tipo'] === 'lead') {
                    $this->leadRepository->inativaLead('id_lead = '.valoresSelect2($data['id']));
                } else {
                    $this->usuarioRepository->inativarUsuario($data['id']);
                    $this->leadRepository->inativaLead('prospect = '.valoresSelect2($data['id']));
                }
            } catch (Exception $e) {
                throw new Exception($e->getMessage(), $e->getCode());
            }
        }

        public function criarSessaoConsultor($data) {
            try {
                $consultor = $_SESSION[CHAVE_CONTRATO]['indicador']['id_fornecedores_despesas'];
                $_SESSION[CHAVE_CONTRATO]['consultor'] = $this->funcionarioRepository->getNomeConsultorPorCodigo($consultor);
                if (isset($data['rapido']) && $data['rapido']) {
                    $_SESSION[CHAVE_CONTRATO]['rapido'] = true;
                } else {
                    unset($_SESSION[CHAVE_CONTRATO]['rapido']);
                }
            } catch (Exception $e) {
                throw new Exception($e->getMessage(), $e->getCode());
            }
        }


    }