<?php

class GestorService {
    private $funcionarioRepository;
    private $produtoRepository;
    private $dependenteRepository;
    private $mensalidadeRepository;
    private $carteiraService;

    public function __construct() {
        $this->funcionarioRepository = new FuncionarioRepository();
        $this->produtoRepository = new ProdutoRepository();
        $this->dependenteRepository = new DependenteRepository();
        $this->mensalidadeRepository = new MensalidadeRepository();
        $this->carteiraService = new CarteiraService();
    }

    public function getListaFuncionarios() {
        return $this->funcionarioRepository->listaFuncionarios();
    }

    public function getListaProdutosChunck() {
        return $this->produtoRepository->getListaProdutosChunck("tipo = 'C' AND conta_produto <> 0");
    }

    public function getTiposPagamentos() {
        return $this->produtoRepository->getListaTipoPagamento('p.conta_produto > 0 and p.inativa = 0');
    }

    public function getClientes($data, $sLimit, $sQtd) {
        try {
            $sWhere = "";
            if (isset($data["inicio"]) && isset($data["fim"])) {
                $inicio = valoresData2($data["inicio"]);
                $fim = valoresDataHora2($data["fim"], '23:59:59');
                $sWhere = "dt_cadastro BETWEEN $inicio and $fim";
            }
            if(isset($data['tipo_plano']) && $data['tipo_plano']) {
                $sWhere.= ($sWhere ? ' and' : '')." conta_produto = ".valoresSelect2($data['tipo_plano']);
            }
    
            if(isset($data['consultor']) && $data['consultor']) {
                $sWhere.= ($sWhere ? ' and' : '')." (indicador = ".valoresSelect2($data['consultor'])." or id_funcionario = ".valoresSelect2($data['consultor']).")";
            }
    
            if(isset($data['tipo_pagamento']) && $data['tipo_pagamento']) {
                $sWhere.= ($sWhere ? ' and' : '').($data['tipo_pagamento'] > 1 ? ' parcela > 1' : ' parcela = '.valoresSelect2($data['tipo_pagamento']));
            }
    
            if(isset($data['status']) && $data['status']) {
                $listStatus = explode(',', $data['status']);
                $status = join(',', array_map(function($item) {
                    return valoresTexto2($item);
                }, $listStatus));
                $sWhere.= ($sWhere ? ' and' : '')." status in (".$status.")";
                //echo $sWhere; exit;
            }
    
            if(isset($data['txtBusca']) && $data['txtBusca']) {
                $search = utf8_decode($data['txtBusca']);
                if (preg_match('/([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})/', $search)){
                    $cnpj = str_replace(".",'',$search);
                    $cnpj = str_replace("-",'',$cnpj);
                    $sWhere.= ($sWhere ? ' and' : '')." REPLACE(REPLACE(cnpj, '.', '') , '-', '') = ".valoresTexto2($cnpj);
                } else if (is_numeric($search)) {
                    $sWhere.= ($sWhere ? ' and' : '')." id_fornecedores_despesas = ".valoresSelect2($search);
                } else {
                    $sWhere.= ($sWhere ? ' and' : '')." (razao_social like '%".$search."%' collate Latin1_General_CI_AI or nome_responsavel like '%".$search."%' collate Latin1_General_CI_AI 
                    or nome_funcionario like '%".$search."%' collate Latin1_General_CI_AI or cidade_nome like '%".$search."%' collate Latin1_General_CI_AI 
                    or email like '%".$search."%' collate Latin1_General_CI_AI or celular like '%".$search."%' or telefone like '%".$search."%' 
                    or endereco like '%".$search."%' collate Latin1_General_CI_AI or bairro like '%".$search."%')";
                } 
            }
            $output["aaData"]               = $this->funcionarioRepository->listaClientes($sWhere, $sLimit, $sQtd);
            $totais                         = $this->funcionarioRepository->totalCliente($sWhere);
            $output["iTotalRecords"]        = $totais['total'];
            $output["iTotalDisplayRecords"] = $totais['total'];
            unset($totais['total']);
            $output['status']               = $totais; 
            return $output;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getCliente($data) {
        try {
            $id = $data['id'];
           if($cliente = $this->funcionarioRepository->getCliente('id_fornecedores_despesas = '.valoresSelect2($id))) {
               $cliente['dependentes'] = [];
               $cliente['mensalidades'] = [];
               $cliente['carteiras'] = $this->carteiraService->getDadosCarteira(['id_usuario' => $cliente['id']]); 
               if($cliente['titular'] === $cliente['id']) {
                   if($dependentes = $this->dependenteRepository->listaDependentes('id_titular = '.valoresSelect2($id))) {
                       $cliente['dependentes'] = $dependentes;
                   }
                   if ($cliente['id_plano']) {
                       $cliente['mensalidades'] = $this->mensalidadeRepository->listaMensalidadesPlano(
                           'id_plano_mens = '.valoresSelect2($cliente['id_plano'])
                        );
                   }
               }
               return $cliente;
            }
            throw new Exception('Cliente não encontrado', 404);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getListaFuncionariosPorGestor() {
        try {
            $loginAtivo   = $_SESSION[CHAVE_CONTRATO]['login_ativo'];
            $funcionario  = $_SESSION[CHAVE_CONTRATO]['indicador'];
            $dados = $this->funcionarioRepository->getListaFuncionariosPorGestor($funcionario['id_fornecedores_despesas']);
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}