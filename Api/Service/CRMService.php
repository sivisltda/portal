<?php

class CRMService {

    private $usuarioRepository;
    private $leadRepository;
    private $configRepository;

    public function __construct() {
        $this->usuarioRepository = new UsuarioRepository();
        $this->leadRepository = new LeadRepository();
        $this->configRepository = new ConfigRepository();
    }

    public function getProcedencias() {
        return $this->configRepository->getProcedencia();
    }

    public function getStatus() {
        return $this->usuarioRepository->getStatus();
    }

    public function salvarLead($data) {
        try {
            $data['consultor'] = (isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['consultor'])) 
                ? $_SESSION[CHAVE_CONTRATO]['consultor']['id'] : null;
            $data['assunto'] = $this->makeAssunto($data);    
            $id = $this->leadRepository->salvarLead($data); 
            return $id;   
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function makeAssunto($data) {
        $dados = [];
        $response = "";
        if (isset($data['veiculo'])) {
            $veiculo = $data['veiculo'];
            $dicionario = ['placa' => 'Placa', 'modelo' => 'Modelo', 'marca' => 'Marca', 'ano_modelo' => 'Ano', 
            'combustivel' => 'Combustível', 'mes_referencia' => 'Mês de Referência', 'valor' => 'Valor Fipe'];
            $dados['veiculo'] = implode(', <br/>', array_map(function($item) use($veiculo, $dicionario) {
                return "<b>".$dicionario[$item]."</b>: ".$veiculo[$item];
            }, array_keys($dicionario)));
        }
        if (isset($data['plano'])) {
            $plano = $data['plano'];
            $dicionario = ['nome' => 'Nome', 'taxa_adesao' => 'Taxa de Adesão', 'valor_mensal' => 'Valor Mensal'];
            $dadosPlano = array_map(function($item) use($plano, $dicionario) {
                return "<b>".$dicionario[$item]."</b>: ".$plano[$item];
            }, array_keys($dicionario));
            $dados['plano'] = implode(', <br/>', $dadosPlano);
            if (isset($plano['acessorios'])) {
                $dadosAcessorios = array_map(function($item) {
                    return "<li><b>".$item['descricao']."</b> -R$ ".$item['preco_venda']."</li>";
                }, $plano['acessorios']);
                $dados['plano'].="<h4>Acessórios</h4><ul>".implode('', $dadosAcessorios)."</ul>";
            }
        }
        if (isset($dados['veiculo'])) {
            $response.= "<h4>Dados do Veículo</h4>".$dados['veiculo'];
        }
        if (isset($dados['plano'])) {
            $response.= "<h4>Dados do Plano</h4>".$dados['plano'];
        } 
      
        return $response;
    }

}