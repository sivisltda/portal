<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 11/10/2019
 * Time: 12:00
 */

class AgendaService
{
    private $agendaRepository;

    public function __construct() {
        $this->agendaRepository = new AgendaRepository();
    }

    /**
     * Faz o calendário
     * @param string $step
     * @param string $now
     * @return array
     */
    public function make($step, $now = null) {
        $arrayReturn = [];
        $now =  $now ? $now : date('Y-m-d H:i:s');
        $start = strtotime($now);
        $limit = strtotime($now.' + '.$step);
        while ($start >= strtotime($now) and $start < $limit) {
            list($year, $month, $day) = explode('-',date('Y-m-d', $start));
            $name_month = month()[$month-1];
            $dayMonth = gregoriantojd($month, $day, $year);
            $weekMonth = jddayofweek($dayMonth, 0);
            $index_month = array_search($month, array_column($arrayReturn, 'number'));
            if($index_month === false) {
                array_push($arrayReturn, [
                    'number' => $month,
                    'name' => $name_month,
                    'year' => $year,
                    'days' => []
                ]);
                $index_month = array_search($month, array_column($arrayReturn, 'number'));
            }
            array_push($arrayReturn[$index_month]['days'], [
                'number' => $day,
                'day' => days_of_week()[$weekMonth],
                'date' => date('d/m/Y', $start)
            ]);
            $start = strtotime($year.'-'.$month.'-'.$day.' + 1 day');
        }
        return $arrayReturn;
    }

    public function hours_interval($start, $end, $interval) {
        $times = [$start, $end];
        $interval = $interval * 60;
        $count_time = [];
        $i = 0;
        $intervals = [];
        foreach($times as $time) {
            list($h,$m,$s) = explode(':',$time);
            $seconds = ($h * 3600) + ($m * 60) + $s;
            $count_time[$i] = $seconds;
            $i++;
        }
        $is = 0;
        foreach(hours() as $hour){
            list($h, $m, $s) = explode(':', $hour);
            $seconds = ($h * 3600) + ($m * 60) + $s;
            if($seconds >= $count_time[0] && $seconds < $count_time[1]){
                if($is == 0){
                    $is = $seconds;
                }
                while($is < $seconds + 3600){
                    $intervals [] = second_time($is);
                    $is+= $interval;
                }
            }
        }
        return $intervals;
    }

    public function getListAgenda($numero_contrato) {
        try {
            $isNotContrato = CONTRATO != $numero_contrato;
            $isNotContrato && toggleConnection($numero_contrato);
            $dados = $this->agendaRepository->getListAgenda();
            $isNotContrato && defaultConnection();
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function getListHorarioAgenda($data) {
        try {
            $isNotContrato = CONTRATO != $data['numero_contrato'];
            $isNotContrato && toggleConnection($data['numero_contrato']);
            $id_agenda = $data['id_agenda'] ? $data['id_agenda'] : null;
            $dados = $this->agendaRepository->getListHorarioAgenda($id_agenda, $data['data_inicio']);
            $isNotContrato && defaultConnection();
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function getAgendaPorId($data) {
        try {
            $isNotContrato = CONTRATO != $data['numero_contrato'];
            $isNotContrato && toggleConnection($data['numero_contrato']);
            $dados = $this->agendaRepository->getAgendaPorId($data['id_agenda']);
            $isNotContrato && defaultConnection();
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function getListaAgendamento($data) {
        try {
            $output = [];
            $sWhere = ' WHERE 1=1';
            $sLimit =  0;
            $sQtd = 5;
            $arrayStatus = ['inativo', 'ativo'];
            $data['page'] = (isset($data['page']) && is_numeric($data['page'])) ? $data['page'] : 1;
            $sLimit = (intval($data['page']) - 1) * $sQtd ;
            if($status = $data['status']) {
                $arrayStatus = array_map(function($s) {
                    return valoresTexto2(trim($s));
                }, explode(',', $status));
                $sWhere.= " AND status in (".implode(',', $arrayStatus).")";
            }
            $isNotContrato = CONTRATO != $data['numero_contrato'];
            if ($isNotContrato) {
                $data['ids'] = $this->agendaRepository->getIdsAgendamentoExternos("loja = ".valoresSelect2($data['numero_contrato']). " and id_pessoa = ".valoresSelect2($data['id_usuario']));
                $data['id_usuario_externo'] = $data['id_usuario'];
                $data['loja'] = CONTRATO;
                $data['id_usuario'] = "0";
            }
            if (!$isNotContrato || isset($data['ids'])) {
                $isNotContrato && toggleConnection($data['numero_contrato']);
                $output['aaData'] = $this->agendaRepository->getListaAgendamento($data, $sQtd, $sLimit, $sWhere);
                $output['total'] = $this->agendaRepository->getTotalListaAgendamento($data, $arrayStatus);
                $isNotContrato && defaultConnection();
                $output['pagination'] = [
                    'current'   => intval($data['page']),
                    'next'      => paginationNext($output['total']['total'], intval($data['page']), $sQtd),
                    'prev'      => paginationPrev($output['total']['total'], intval($data['page']), $sQtd),
                    'qtd_pages' => paginationQtd($output['total']['total'], $sQtd)
                ];
            } else {
                $output['aaData'] = [];
                $output['pagination'] = [
                    'current'   => intval($data['page']),
                    'next'      => false,
                    'prev'      => false,
                    'qtd_pages' => 0
                ];
                $output['total'] = [
                    'total' => 0,
                    'ativo' => 0,
                    'inativo' => 0
                ];
            }
            return $output;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function salvarAgendamento($data) {
        try {
            $isNotContrato = CONTRATO != $data['numero_contrato'];
            if ($isNotContrato) {
                toggleConnection($data['numero_contrato']);
                $data['id_usuario_externo'] = $data['id_usuario'];
                $data['loja'] = CONTRATO;
                $data['id_usuario'] = "0";
                $data['obs'] = "Agendado por: ".$_SESSION[CHAVE_CONTRATO]['nome']." - Loja : ".$data['numero_contrato']." <br/> ".$data['obs'];
            }

            $config = $this->agendaRepository->getConfigAgenda();
            if($config['adm_agen_umamar'] == 1) {
                if($this->agendaRepository->verificaAgendamentoDiario($data)) {
                    throw new Exception('Não é Permitido mais de um agendamento diário por cliente!', 403);
                }
            }
            if($config['adm_agen_hmarc'] == 1) {
                $data['adm_num_agendamento'] = $config['adm_num_agendamento'];
                if($this->agendaRepository->verificaAgendamentoHoraMarcado($data)) {
                    throw  new Exception('Não é permitido agendamento com horário marcado!', 403);
                }
            }
            if($config['adm_agen_semanal'] > 0) {
                $data['adm_agen_semanal'] = $config['adm_agen_semanal'];
                if($this->agendaRepository->verificaCadastroSemanal($data)) {
                    throw new Exception("Não é permitido cadastro semanal maior que " . $config['adm_agen_semanal'] . " por cliente!", 403);
                }
            }
            $data['id_agendamento'] = $this->agendaRepository->salvarAgendamento($data);
            if ($isNotContrato) {
                defaultConnection();
                $this->agendaRepository->salvarAgendamentoExterno($data);
            }
            $link = $this->agendaRepository->salvarLinkAgendamento($data);
            
            return ['agendamento' => $data['id_agendamento'], 'link' => $link];
        }catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function cancelarAgendamento($data) {
        try {
            if(is_numeric($data['id_agendamento'])) {
                $isNotContrato = CONTRATO != $data['numero_contrato'];
                $externo = false;
                if ($isNotContrato) {
                    $externo = true;
                    $data['loja'] = CONTRATO;
                    toggleConnection($data['numero_contrato']);
                }
                if($agenda = $this->agendaRepository->getAgendamentoPorId($data, $externo)) {
                    if(strtotime('now') < strtotime(convertDataIso($agenda['data_inicio']))) {
                        throw new Exception('Não pode cancelar um agendamento já iniciado', 403);
                    }
                    $this->agendaRepository->cancelarAgendamento($data);
                }
                $isNotContrato && defaultConnection();
                return true;
            }
            throw new Exception('Agendamento não encontrado', 404);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

}