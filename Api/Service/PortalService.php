<?php

class PortalService {

    private $configRepository;
    private $funcionarioRepository;
    private $usuarioRepository;
    private $planoRepository;
    private $veiculoRepository;

    public function __construct() {
        $this->configRepository      = new ConfigRepository(); 
        $this->funcionarioRepository = new FuncionarioRepository(); 
        $this->usuarioRepository     = new UsuarioRepository();
        $this->planoRepository       = new PlanoRepository();
        $this->veiculoRepository     = new VeiculoRepository();
    }

    public function getInfo() {
        try {
            $modulos = getModulos();
            $filiais = $this->configRepository->getFiliais();
            $config = [ 
                'nome'                  => NOME,
                'site_nome'             => SITE_NAME,
                'modelo'                => $modulos['seg'] ? 'seguro' : 'clube',
                'mdl_what'              => MOD_WHAT && WHAT_PADRAO > 0,
                'contrato'              => CONTRATO,
                'unico'                 => PLANO_UNICO,
                'image'                 => IMAGE,
                'rapido'                => RAPIDO,
                'cnpj'                  => CNPJ ? CNPJ : ($filiais[0]['cnpj'] ? $filiais[0]['cnpj'] : ''),
                'telefone'              => $filiais[0]['telefone'] ? $filiais[0]['telefone'] : '',
                'tipo_vencimento'       => TIPO_VENCIMENTO,
                'ex_pag_pess_jur'       => ACA_EX_PJ,
                'adesao_dependente'     => TAXA_ADESAO_DEPENDENTE,
                'ativa_dependente'      => $modulos['sau'],
                'procedencia'           => $this->configRepository->getProcedencia(1, 1),
                'filiais'               => $filiais,
                'cor_primaria'          => COR_PRIMARIA,
                'cor_secundaria'        => COR_SECUNDARIA,
                'cor_destaque'          => COR_DESTAQUE,
                'ano_limite'            => ANO_LIMITE,
                'franquia_padrao'       => FRANQUIA_DEFAULT,
                'agrupar_plano'         => AGRUPAR_PLANO,
                'plano_mens_contract'   => MENS_PLANO_CONTRACT,
                'adm_msg_boas_vindas'   => MENSAGEM_BOA_VINDAS,
                'aca_pes_cot_tip'       => ACA_PES_COT_TIP,
                'adm_msg_cotacao'       => MENSAGEM_COTACAO,
                'adm_associado'         => MENSAGEM_ASSOCIADO,
                'adm_msg_contratacao'   => MENSAGEM_CONTRATACAO,
                'adm_desc_isencao_ie'   => DESC_ISENCAO_IE,
                'logo_conclusao'        => LOGO_CONCLUSAO,
                'nao_pag_online'        => NAO_PAG_ONLINE,
                'adm_nao_valor_print'   => NAO_VALOR_PRINT,
                'adm_msg_taxa_adesao'   => MENSAGEM_ADESAO,
                'adm_msg_termos_email'  => MENSAGEM_TERMO_EMAIL,
                'adm_msg_termos_whats'  => MENSAGEM_TERMO_WHATS,
                'adm_msg_link_pag'      => MENSAGEM_LINK_PAG,
                'exc_ven_prorata'       => ACA_EX_VEN_PR && TIPO_VENCIMENTO == 2 ? 1 : 0,
                'adm_desconto_range'    => DESCONTO_RANGE,
                'seg_vei_cotac_person'  => SEG_VEI_COTAC_PERSON
            ];
            if (isset($_SESSION[CHAVE_CONTRATO]['consultor'])) {
                $config['consultor'] = $_SESSION[CHAVE_CONTRATO]['consultor'];
                if (isset($_SESSION[CHAVE_CONTRATO]['indicador']) && $_SESSION[CHAVE_CONTRATO]['indicador']['id_fornecedores_despesas'] == $config['consultor']['id']) {
                    $config['terminal_venda'] = 0;
                    $config['indicadores'] = $this->funcionarioRepository->getIndicadores();
                    $config['cotacao_interna'] = 1;
                }
            }
            if ($modulos['seg']) {
                $config['tipos_veiculos'] = $this->veiculoRepository->getTiposVeiculos();
            }
            if(TIPO_VENCIMENTO == 2) {
                $config['vencimentos'] = $this->configRepository->getDiasVencimentos();
            } else if(TIPO_VENCIMENTO == 1) {
                $config['dia_vencimento'] = DIA_VENCIMENTO;
            }
            if (isset($_SESSION[CHAVE_CONTRATO]['prospect']) 
            && is_numeric($_SESSION[CHAVE_CONTRATO]['prospect'])) {
                $prospect = $_SESSION[CHAVE_CONTRATO]['prospect'];
                $dadosProspect = $this->usuarioRepository->getUsuarioPorId($prospect);
                $dadosProspect['hasCnpj'] = !!$dadosProspect['cnpj'];
                unset($dadosProspect['cnpj']);
                if ($dadosProspect) {
                    $_SESSION[CHAVE_CONTRATO]['usuario'] = $prospect;
                    $config['usuario'] = $dadosProspect;
                    if (!$dadosProspect['hasCnpj']) {
                        if ($modulos['seg']) {
                            $config['veiculos'] = $this->veiculoRepository->getVeiculosPorDados('v.id_fornecedores_despesas = ' .valoresSelect2($prospect).
                                " AND (planos_status is null OR planos_status = 'novo')", [], true);
                        } else {
                            $config['planos'] = $this->planoRepository->getPlanos("(favorecido = ".valoresSelect2($prospect)
                            ." or favorecido in (select id_titular from sf_fornecedores_despesas_dependentes where id_dependente = ".valoresSelect2($prospect)
                            .")) and A.planos_status not in ('Inativo', 'Cancelado') and B.mostrar_site = 1");
                        }
                    }
                } 
                unset($_SESSION[CHAVE_CONTRATO]['prospect']);
            }
            return $config;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function verificaSessao($data) {
        try {
            if ($_SESSION && (isset($_SESSION[CHAVE_CONTRATO]) && isset($_SESSION[CHAVE_CONTRATO]['usuario']))) {
                if (intval($_SESSION[CHAVE_CONTRATO]['usuario']) === intval($data['id_usuario'])) {
                    return true;
                }
            }
            $usuario = $this->usuarioRepository->getUsuarioPorId($data['id_usuario']);
            if ($usuario) {
                $_SESSION[CHAVE_CONTRATO]['usuario'] = $usuario['id'];
                throw new Exception('Usuário não logado', 401);
            } 
            throw new Exception('Usuario não encontrado', 404);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function getCampoExtraBusca() {
        try {
            $dados = $this->configRepository->getCampoExtra('ativo_campo = 1 and busca_campo = 1');
            return count($dados) ? $dados[0] : null;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}