<?php 

class CredencialService {

    private $credencialRepository;
    private $usuarioRepository;
    private $planoRepository;
    private $mensalidadeRepository;

    public function __construct() {
        $this->credencialRepository = new CredencialRepository();
        $this->usuarioRepository = new UsuarioRepository();
        $this->planoRepository = new PlanoRepository();
        $this->mensalidadeRepository = new MensalidadeRepository();
    }

    public function verificaCriaCredencial($dados) {
        try {
            if ($dadosCredencial = $this->credencialRepository->getCredencial($dados['credencial_id'])) {
                Conexao::initTransaction();
                $id_usuario = $dados['id_usuario'];
                $dadosUsuarioCredencial = $this->credencialRepository->verificaCredencialUsuario($dados['credencial_id'], $id_usuario);
                if (!$dadosUsuarioCredencial) {
                    $dadosUsuarioCredencial = $this->credencialRepository->criaCredencialUsuario([
                        'id_usuario' => $id_usuario, 
                        'id_credencial' => $dadosCredencial['id_credencial'],
                        'dias' => $dadosCredencial['dias'],
                        'mensagem' => 'período de teste'
                    ]);                    
                    $this->usuarioRepository->converteProspectCliente($id_usuario);
                    
                    $this->usuarioRepository->atualizarStatus('id_fornecedores_despesas = '.valoresSelect2($id_usuario));
                    if (isset($dados['id_plano']) && $dados['id_plano']) {
                        $this->mensalidadeRepository->atualizaDataMensalidade($dadosUsuarioCredencial['dt_fim'], $dados, 1);
                        $this->planoRepository->atualizaStatusPlano($dados['id_plano']);
                    }
                }
                $tipoPagto = isset($dados['tipo']) ? $dados['tipo'] : null;
                if($tipoPagto === 'cartao')  {
                    $this->planoRepository->criarCartaoUsuario($id_usuario, $dados);
                }
                if (isset($dadosUsuarioCredencial['id_fornecedores_despesas_credencial'])) {
                    $dadosUsuarioCredencial['mensagem'] = 'Período de teste ativo';
                    $dadosUsuarioCredencial['status']  = 'pago';
                }
                Conexao::commit();
                return $dadosUsuarioCredencial;
            }
            return [
                'status' => 'nao_pago',
                'mensagem' => 'Credencial inválida'
            ];
        } catch (Exception $e) {
            Conexao::rollback();
            throw new Exception($e->getMessage());
        }
    }

    public function cancelaCredencial($dados) {
        try {
            $this->credencialRepository->cancelaCredencial($dados);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}