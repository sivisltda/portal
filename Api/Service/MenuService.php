<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 09/09/2020
 * Time: 11:00
 */

class MenuService
{
    private $dependenteRepository;
    private $faqRepository;

    public function __construct() {
        $this->dependenteRepository = new DependenteRepository();
        $this->faqRepository = new FaqRepository();
    }

    public function makeMenu($id_usuario) {
        $modulos = getModulos();
        //var_dump($_SESSION[CHAVE_CONTRATO]);exit;
        $opcoes = [
            [ 'id' => 1, 'label' => 'Principal', 'link' => BASE_URL."loja/".CONTRATO.'/dashboard', 'icon' => 'mdi mdi-gauge', 'class' => ''],
            [ 'id' => 2, 'label' => 'Meus Veículos', 'link' => BASE_URL."loja/".CONTRATO.'/veiculos', 'icon' => 'mdi mdi-car', 'class' => '' ],
            [ 'id' => 3, 'label' => 'Meus Pagamentos', 'link' => BASE_URL."loja/".CONTRATO.'/forma-pagamentos', 'icon' => 'mdi mdi-cash', 'class' => '' ],
            [ 'id' => 4, 'label' => 'Comissionamento', 'link' => BASE_URL."loja/".CONTRATO.'/comissao', 'icon' => 'mdi mdi-cash', 'class' => '' ],
            [ 'id' => 5, 'label' => 'Regulamentos', 'link' => BASE_URL."loja/".CONTRATO.'/regulamentos', 'icon' => 'mdi mdi-certificate', 'class' => '' ],
            [ 'id' => 6, 'label' => 'Dúvidas Frequentes', 'link' => BASE_URL."loja/".CONTRATO.'/duvidas', 'icon' => 'mdi mdi-help-circle-outline', 'class' => '' ],
            [ 'id' => 7, 'label' => 'Cotação', 'link' => BASE_URL."loja/".CONTRATO.'/cliente', 'icon' => 'mdi mdi-account-multiple-plus-outline', 'class' => '' ],
            [ 'id' => 8, 'label' => 'Prospects', 'link' => BASE_URL."loja/".CONTRATO.'/meus-clientes', 'icon' => 'mdi mdi-account-multiple-outline', 'class' => '' ],
            [ 'id' => 9, 'label' => 'Dependentes', 'link' => BASE_URL."loja/".CONTRATO.'/meus-dependentes', 'icon' => 'mdi mdi-account-multiple-outline', 'class' => '' ],
            [ 'id' => 10, 'label' => 'Minha Agenda', 'link' => BASE_URL."loja/".CONTRATO.'/minha-agenda', 'icon' => 'mdi mdi-calendar-check', 'class' => '' ],
            [ 'id' => 11, 'label' => 'Pagamentos', 'link' => BASE_URL."loja/".CONTRATO.'/pagamento', 'icon' => 'mdi mdi-cash-multiple', 'class' => '' ],
            [ 'id' => 12, 'label' => 'Sair', 'link' => 'javascript:void(0)', 'icon' => 'mdi mdi-exit-to-app', 'class' => 'logout' ],
            [ 'id' => 13, 'label' => 'Gestor', 'link' => BASE_URL."loja/".CONTRATO."/clientes", 'icon' => 'mdi mdi-account-multiple-outline', 'class' => ''],
            [ 'id' => 14, 'label' => 'Validador', 'link' => BASE_URL."loja/".CONTRATO."/validador", "icon" => 'mdi mdi-magnify', 'class' => '', 'target' => true],
            [ 'id' => 15, 'label' => 'Pendencias', 'link' => BASE_URL."loja/".CONTRATO."/pendencias", 'icon' => 'mdi mdi-account-multiple-outline', 'class' => ''],
        ];

        $menu =  [1];
        if(!(isset($_SESSION[CHAVE_CONTRATO]['indicador']) && in_array($_SESSION[CHAVE_CONTRATO]['login_ativo'], ['funcionario', 'gestor']))) {
            if($modulos['seg']) {
                array_push($menu, 2);
            }
            array_push($menu, 3);
            if($modulos['clb'] && $this->dependenteRepository->getHasDependente($id_usuario)) {
                array_push($menu, 9);               
            }
            $regulamentos = (new RegulamentoService())->getRegulamentos();
            if (count($regulamentos)) {
                array_push($menu, 5);
            }
            if(AGENDA) {
                array_push($menu, 10);
            }
            if (count($this->faqRepository->listaFaq())) {
                array_push($menu, 6);
            }
        } else if(isset($_SESSION[CHAVE_CONTRATO]['indicador']) && $_SESSION[CHAVE_CONTRATO]['login_ativo'] === 'funcionario') {
            array_push($menu,4,7,8,14);
            $modulos['seg'] && array_push($menu, 15);
        } else if (isset($_SESSION[CHAVE_CONTRATO]['indicador']) && $_SESSION[CHAVE_CONTRATO]['login_ativo'] === 'gestor') {
            array_push($menu,4,7,8,13,14);
            $modulos['seg'] && array_push($menu, 15);
        }
        array_push($menu, 12);

        return array_map(function($item) use($opcoes) {
            return current(array_filter($opcoes, function($op) use($item) {
                return $item === $op['id'];
            }));
        }, $menu);
    }

    public function getImageMenu() {
        $dados = getImages(CONTRATO, 'Empresa/Menu');
        if ($dados && count($dados)) {
            $image = current($dados);
            return $image['url'];
        }
        return LOGO_URL; 
    }

}