<?php

class PlanoService {
    
    private $planoRepository;
    private $dependenteRepository;
    private $mensalidadeRepository;
    private $produtoRepository;
    private $usuarioRepository;
    private $veiculoRepository;

    public function __construct() {
        $this->planoRepository = new PlanoRepository();
        $this->dependenteRepository = new DependenteRepository();
        $this->mensalidadeRepository = new MensalidadeRepository();
        $this->produtoRepository = new ProdutoRepository();
        $this->usuarioRepository = new UsuarioRepository();
        $this->veiculoRepository = new VeiculoRepository();
    }

    public function criarPlano($data) {
        try {
            Conexao::initTransaction();
            $modulos = getModulos();
            if ($modulos['seg']) {
                if (isset($data['id_veiculo']) && $data['id_veiculo']) {
                    $data['id_parcela'] = $this->veiculoRepository->verificaPlanoVeiculo("ve.id = ".valoresSelect2($data['id_veiculo'])
                    ." and p.conta_produto = ".valoresSelect2($data['id_produto']));
                    if (MENS_PLANO_CONTRACT) {
                        $acessoriosPadrao = $this->produtoRepository->getIdsMateriaPrimaParcela($data['id_produto']);
                        $acessorios = array_filter($data['acessorios'], function($acessorio) use($acessoriosPadrao){
                            return !in_array($acessorio, $acessoriosPadrao);
                        });
                        if (!count($acessorios)) {
                            $parcela = $this->produtoRepository->getProdutoParcela("p.conta_produto = ".valoresSelect2($data['id_produto']). " and parcela = 12");
                            if (!$parcela) {
                                throw new Exception('Configuração de parcelas incorreto', 500);
                            }
                        } else {
                            $parcela = $this->produtoRepository->getProdutoParcela("p.conta_produto = ".valoresSelect2($data['id_produto']). " and parcela <= 1");
                            if (!$parcela) {
                                throw new Exception('Configuração de parcelas incorreto', 500);
                            }
                        }
                        $data['id_parcela'] = $parcela['parcela_id'];
                    }
                }
            }
            $data['horario_id']  = "1";
            $data['dias_ferias'] = ($dias_ferias = $this->planoRepository->getDiaFerias("1")) ? $dias_ferias : 0;
            $whereParcela = isset($data['id_parcela']) && $data['id_parcela'] ? "pp.id_parcela = ".valoresSelect2($data['id_parcela']) : "id_produto = " . valoresSelect2($data['id_produto']);
            $data['produto']     = $this->produtoRepository->getProdutoParcela($whereParcela);
            $data['dt_fim']      = $this->planoRepository->getDataPrimVencimento($data);
            $plano               = $this->planoRepository->salvaPlanoUsuario($data);
            $data['id_plano']    = $plano['id_plano'];
            if (is_null($data['id_plano'])) {
                throw new Exception('Ocorreu erro ao salvar o plano');
            }          
            $id_mensalidade = $this->mensalidadeRepository->salvarMensalidades($data);
            if (is_null($id_mensalidade)) {
                throw new Exception('Ocorreu erro ao gerar as mensalidades');
            }                       
            $valorAcessorios = 0;
            if ($modulos['seg']) {
                $data['acessorios'] = isset($data['acessorios']) ? $data['acessorios'] : [];
                $valorAcessorios    = $this->planoRepository->criarPlanoAccessorios($data);
            }
            $valorConvenio = $this->planoRepository->criarConvenio($data);     
            Conexao::commit();
            $dados = $this->planoRepository->getPlano("id_plano = ".valoresSelect2($data['id_plano'])." and A.planos_status not in ('Inativo', 'Cancelado')");
            if (!$dados) {
                throw new Exception('Plano não encontrado', 404);
            }
            if ($modulos['seg']) {
                $dados['acessorios'] = $valorAcessorios;
                $dados['cupom'] = $valorConvenio;
            }         
            return $dados;
        } catch (Exception $e) {
            Conexao::rollback();
            throw new Exception($e->getMessage());
        }
    }

    public function cancelarPlano($data) {
        try {
            $modulos = getModulos();
            if ($plano = $this->planoRepository->getPlano('(favorecido = '.valoresSelect2($data['id_usuario'])
            .' or favorecido in (select id_titular from sf_fornecedores_despesas_dependentes where id_dependente = '.valoresSelect2($data['id_usuario']).'))'
            .' and id_plano = '.valoresSelect2($data['id_plano']))) {
                if ($plano['favorecido'] == $data['id_usuario']) {
                    if (is_null($plano['status']) || strtolower($plano['status']) == 'novo') {
                        if ($modulos['clb']) {
                            $this->dependenteRepository->verificaRemoveUsuarioDependentePlano($data);
                            $this->usuarioRepository->atualizarStatus('id_fornecedores_despesas in (select id_dependente 
                            from sf_fornecedores_despesas_dependentes where id_titular = '.valoresSelect2($data['id_usuario']).')');
                        }
                    } 
                    $this->planoRepository->cancelaPlano($plano['id']);
                } else {
                    if ($modulos['clb']) {
                        if ($this->dependenteRepository->verificaDependente(['id_titular' => $plano['favorecido'], 'id_dependente' => $data['id_usuario']])) {
                            $this->dependenteRepository->removeDependente(['id_titular' => $plano['favorecido'], 'id_dependente' => $data['id_usuario']]);
                        }
                    }
                    $this->usuarioRepository->atualizarStatus('id_fornecedores_despesas = '.valoresSelect2($data['id_usuario']));
                }                
            } else {
                throw new Exception('Plano não encontrado', 404);
            }
        } catch(Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function listaPlanos($where) {
        try {
            $planos = $this->planoRepository->getPlanos($where);
            return array_map(function($plano) {
                $condicao = "and id_produto = ".valoresSelect2($plano['id_produto'])." and id_parcela !=".valoresSelect2($plano['id_parcela']);
                $plano['parcelas'] = $this->produtoRepository->getListaProdutosParcelas($condicao);
                return $plano;
            }, $planos);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
        
    public function cancelarCupom($data) {
        try {
            return $this->planoRepository->removeCupom($data['id_convenio'], $data['id_usuario']);            
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
        
    public function verificarCupom($data) {
        try {
            return $this->planoRepository->verificarCupom($data);            
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}