<?php 

class ProdutoService {

    private $produtoRepository;
    private $dependenteRepository;
    private $usuarioRepository;

    public function __construct() {
        $this->produtoRepository = new ProdutoRepository();  
        $this->dependenteRepository = new DependenteRepository();
        $this->usuarioRepository = new UsuarioRepository();
        $this->enderecoRepository = new EnderecoRepository();
        $this->veiculoRepository = new VeiculoRepository();
    }

    public function getListaProduto($data) {
        try {
            $dados = [];
            $modulos = getModulos();
            if (isset($_SESSION[CHAVE_CONTRATO]['produto']) && $_SESSION[CHAVE_CONTRATO]['produto']) {
                $data['id'] = $_SESSION[CHAVE_CONTRATO]['produto'];
                $dados['produto'] = $this->getItemProduto($data);
                $dados['produto']['termos'] = $this->getTermos($dados['produto']);
            } else if (!$modulos['seg']) {
                $data['tipoPessoa'] = $data['tipoPessoa'] ? $data['tipoPessoa'] : 'F';        
                $condicaoTipo = $data['tipoPessoa'] === 'F' ? ' and (pes_jur = 0 or pes_jur is null)' : ' and (pes_jur = 1)';
                $dados['produtos'] = $this->produtoRepository->listaProdutos("P.tipo in ('C') and P.inativa = 0 and mostrar_site = 1".$condicaoTipo, 0);
                $dados['produtos'] = array_map(function($item) {
                    return $this->getRegrasTermos($item);
                }, $dados['produtos']);
                
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getItemProduto($data) {
        try {
            $data['tipoPessoa'] = $data['tipoPessoa'] ? $data['tipoPessoa'] : 'F';        
            $condicaoTipo = " and (pes_jur is null or ".($data['tipoPessoa'] === 'F' ? 'pes_jur = 0' : 'pes_jur = 1').")";   
            $produto = $this->produtoRepository->getProduto("P.conta_produto = ".valoresSelect2($data['id']). 
            " and P.inativa = 0 and mostrar_site = 1".$condicaoTipo, 0);
            if (!$produto) { 
                throw new Exception('Produto não encontrado', 404);
            }          
            return $this->getRegrasTermos($produto);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function getProdutoServicoSeguro($data) {
        try {
            $dados = [];
            $condicao = "";
            if (isset($data['usuario']) && $data['usuario']) {
                $condicao = " and (grupo_cidades is null or grupo_cidades in (select id_regiao from sf_regioes_cidades 
                inner join sf_regioes on id = id_regiao inner join sf_fornecedores_despesas on id_cidade = cidade 
                where id_fornecedores_despesas = " . valoresNumericos2($data['usuario']) . " and sf_regioes.inativo = 0)) ";
            }
            $produtos = array_filter($this->produtoRepository->getProdutosSeguro($data, $condicao), function($produto) {
                return count($produto['parcelas']) > 0;
            });
            $dados['produtos'] = $produtos ;
            if (!count($dados['produtos'])) {
                throw new Exception('Infelizmente o seu veículo não está na faixa de preço/ região de cobertura coberto pelos nossos planos.', 404);
            }
            $dados['servicos'] = $this->produtoRepository->getServicosSeguro(valoresNumericos2($data['valor']) . " between p1.segMin and p1.segMax 
            and (p1.conta_produto in (select id_produto from sf_produtos_veiculo_tipos where id_tipo = " . valoresSelect2($data['tipo_veiculo']) . ") or
            p1.conta_produto not in (select id_produto from sf_produtos_veiculo_tipos))", $data['valor']);
            $dados['produtos'] = array_map(function($item) {
                return $this->getRegrasTermos($item);
            }, $dados['produtos']);
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function getCoberturas($data) {
        try {
            $dados = [];
            $dados['coberturas'] = $this->produtoRepository->getCoberturas("c.inativa = 0 and c.tipo = 'A'  
            and (c.veiculo_tipo = ".valoresSelect2($data['tipo_veiculo']) ." or c.veiculo_tipo is null) 
            and (cmc.id_conta_ref in (select conta_movimento from sf_produtos where conta_produto = ".valoresSelect2($data['produto_id'])."))");            
            $dados['coberturas'] = array_map(function($item) use($data) {
                $item['items'] = $this->produtoRepository->getCoberturasItens($data['produto_id'], $item["id"], "p.inativa = 0 and p.tipo = 'A'  
                and (p.veiculo_tipo = ".valoresSelect2($data['tipo_veiculo'])." or p.veiculo_tipo is null)", $data['valor']);
                return $item;
            }, $dados['coberturas']);            
            $dados['servicos'] = $this->produtoRepository->getServicosSeguro("p1.conta_produto = ".valoresSelect2($data['produto_id']).
            "and (p1.conta_produto in (select id_produto from sf_produtos_veiculo_tipos where id_tipo = " . valoresSelect2($data['tipo_veiculo']) . ") or
            p1.conta_produto not in (select id_produto from sf_produtos_veiculo_tipos))", $data['valor']);
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    private function getRegrasTermos($produto) {
        $modulos = getModulos();
        if ($modulos['sau'] && DEPENDENTE_PARENTESCO) {
            $produto['dependentes'] = $this->getRegrasDependentes($produto);
        }
        $produto['termos'] = $this->getTermos($produto);
        return $produto;
    }

    private function getRegrasDependentes($produto) {
        return $this->dependenteRepository->getRegrasDependentePlano("id_produto = ".valoresSelect2($produto['id']));
    }

    private function getTermos($produto) {
        $where = $produto['id_grupo'] ? "grupo = ".valoresSelect2($produto['id_grupo'])." or " : "";
        return $this->produtoRepository->getTermos($where."grupo is null");
    }

}