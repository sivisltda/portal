<?php

class CampanhaService {

    private $campanhaRepository;
    private $pessoaRepository;
    private $leadRepository;

    public function __construct() {
        $this->campanhaRepository = new CampanhaRepository();
        $this->pessoaRepository = new PessoaRepository();
        $this->leadRepository = new LeadRepository();
    }

    public function getCampanhaPorId($id) {
        try {
            $dados = $this->campanhaRepository->getCampanha(
            "id = " . valoresSelect2($id) . " and inativo = 0 
            and cast(getdate() as date) between data_ini and data_fim");
            if ($dados['id_foto']) {
                $dados['id_foto'] = ERP_URL . "Pessoas/" . CONTRATO . "/Campanha/" . $dados['id'] . "/" . rawurlencode($dados['id_foto']);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function verifyIndicador($condicao) {
        try {
            $dados = $this->pessoaRepository->getPessoa($condicao);
            if (!$dados) {
                throw new Exception('Lead não encontrado', 404);
            }
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function salvarLead($dados) {
        try {
            $dados['procedencia'] = $_SESSION[CHAVE_CONTRATO]['campanha']['id_procedencia'];
            if ($dados["tipo"] == "indicar" && isset($dados['indicador_lead'])) {
                $this->pessoaRepository->salvarUsuario($dados['indicador_lead']);
                $indicador = $this->pessoaRepository->getPessoa('id_fornecedores_despesas = ' . valoresSelect2($dados['indicador_lead']));
                $dados['consultor'] = $indicador['consultor'];
                $id = $this->leadRepository->salvarLead($dados);
            } else if ($dados["tipo"] == "cadastrar" && !isset($dados['indicador_lead'])) {
                $id = $this->pessoaRepository->salvarPessoa($dados);
            }
            return $id;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function getDadosLeads($data) {
        try {
            $campanha = $_SESSION[CHAVE_CONTRATO]['campanha'];
            $dados = [];
            $dados['indicados'] = $this->campanhaRepository->getLeads('l.procedencia = ' . valoresSelect2($campanha['id_procedencia']) . ' and indicador_cliente = ' . valoresSelect2($data['indicador_lead']));
            $dados['cupons'] = $this->campanhaRepository->getCupons('id_campanha = ' . valoresSelect2($campanha['id']) . ' and indicador_cliente = ' . valoresSelect2($data['indicador_lead']));
            $dados['financeiro'] = $this->campanhaRepository->getFinanceiro($data['indicador_lead']);
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

}
