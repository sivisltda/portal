<?php

class PendenciaService {

    private $pendenciaRepository;
    private $veiculoRepository;
    private $convenioRepository;

    public function __construct() {
        $this->pendenciaRepository = new PendenciaRepository();
        $this->veiculoRepository = new VeiculoRepository();
        $this->convenioRepository = new ConvenioRepository();
    }

    public function getListaPendencia($data) {
        try {
            $loginAtivo = $_SESSION[CHAVE_CONTRATO]['login_ativo'];
            $idUsuario = $_SESSION[CHAVE_CONTRATO]['restricted_id'];
            $funcionario = $_SESSION[CHAVE_CONTRATO]['indicador'];
            $funcionarios = (new GestorService())->getListaFuncionariosPorGestor();            
            $in_func = "0";
            foreach ($funcionarios as $item) {
                $in_func .= "," . $item['id_usuario'];    
            }            
            //var_dump($loginAtivo, $idUsuario, $funcionario);exit;
            if (!$idUsuario || ($idUsuario && !in_array($loginAtivo, ['gestor', 'funcionario'])) || !count($funcionario)) {
                throw new Exception('Você não pode acessar esse recurso', 403);
            }            
            if (isset($data['funcionario_id']) && $data['funcionario_id']) {
                $condicao = "(fd.id_user_resp in (select id_usuario from sf_usuarios
                where funcionario = " . valoresSelect2($data['funcionario_id']) . ") or 
                fd.prof_resp = " . valoresSelect2($data['funcionario_id']) . ")";
            } else {
                $condicao = "(fd.id_user_resp in (" . $in_func . ") or 
                fd.prof_resp = " . valoresSelect2($funcionario['id_fornecedores_despesas']) . ")";                
            }
            if (isset($data['cliente_id']) && is_numeric($data['cliente_id'])) {
                $condicao .= " and fd.id_fornecedores_despesas = " . valoresSelect2($data['cliente_id']);
            }
            if (isset($data['busca']) && $data['busca']) {
                $condicao .= " and (fd.razao_social like '" . utf8_decode($data['busca']) . "%' collate Latin1_General_CI_AI
                or ve.placa like '" . utf8_decode($data['busca']) . "%' collate Latin1_General_CI_AI)";
            }
            if (isset($data["inicio"]) && $data['inicio'] && isset($data["fim"]) && $data['fim']) {
                $inicio = valoresData2($data["inicio"]);
                $fim = valoresDataHora2($data["fim"], '23:59:59');
                $condicao .= " and vp.dt_cadastro BETWEEN $inicio and $fim ";
            }
            //echo $condicao;exit;
            $dados = $this->pendenciaRepository->getListPendencias($condicao);
            $items = array_map(function($item) {
                return [
                    'id_plano' => $item['id_plano'],
                    'id_mens' => $item['id_mens'],
                    'id_cliente' => $item['id_fornecedores_despesas'],
                    'contrato_id' => $item['contrato_id']
                ];
            }, $dados);
            $contratos = $this->getBuscarContratos($items);
            //print_r($contratos);exit;
            $dados = array_map(function($item) use($contratos) {
                $item['contrato'] = $contratos && count($contratos['contratos']) ? $contratos['contratos'][$item['id_plano']] : 0;
                $item['pagamento'] = (is_numeric($item['pagamento_id']) ? 1 : 
                    ($contratos && isset($contratos['comprovantes'][$item['id_mens']]['existe']) ? $contratos['comprovantes'][$item['id_mens']]['existe'] : 0));
                $item['residencia'] = $contratos && isset($contratos['residencia'][$item['id_plano']]['existe']) ? $contratos['residencia'][$item['id_plano']]['existe'] : 0;
                $item['habilitacao'] = $contratos && isset($contratos['habilitacao'][$item['id_plano']]['existe']) ? $contratos['habilitacao'][$item['id_plano']]['existe'] : 0;
                $item['documento'] = $contratos && isset($contratos['documento'][$item['id_plano']]['existe']) ? $contratos['documento'][$item['id_plano']]['existe'] : 0;
                $item['contrato_url'] = $this->getUrlContratoAssinatura($item);
                $item['comprovante_url_p'] = $this->getUrlComprovante($item, $contratos, 'P');
                $item['comprovante_url_r'] = $this->getUrlComprovante($item, $contratos, 'R');
                $item['comprovante_url_h'] = $this->getUrlComprovante($item, $contratos, 'H');
                $item['contrato_assinatura'] = $this->getUrlContratoAssinaturaUrl($item);
                return $item;
            }, $dados);
            //$dados['pagamento'] = (is_numeric($dados['pagamento_id']) ? "1" : $dados['pagamento']);
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    private function getUrlContratoAssinatura($item) {
        if ($item['contrato_id']) {
            return ERP_URL . "/Modulos/Seguro/FormAssinatura.php?id=" . $item['id_plano']
                    . "&idContrato=" . $item['contrato_id'] . "&crt=" . CONTRATO
                    . "&idCliente=" . $item['id_fornecedores_despesas'];
        }
        return "";
    }

    private function getUrlContratoAssinaturaUrl($item) {
        if ($item['contrato_id']) {
            return ERP_URL . "/util/ImpressaoPdf.php?id_plan=" . $item['id_plano'] . "&idContrato=" . $item['contrato_id'] .
                    "&model=S&tpImp=I&NomeArq=Contrato&PathArq=../Modulos/Comercial/modelos/ModeloContrato.php&emp=1&crt=" . CONTRATO;
        }
        return "";
    }

    private function getUrlComprovante($item, $contratos, $tipo) {
        if ($tipo == "P" && $contratos && strlen($contratos['comprovantes'][$item['id_mens']]['arquivo']) > 0) {
            return ERP_URL . "/Pessoas/" . CONTRATO . "/Documentos/" . $item['id_fornecedores_despesas'] . "/" . $contratos['comprovantes'][$item['id_mens']]['arquivo'];
        } else if ($tipo == "R" && $contratos && strlen($contratos['residencia'][$item['id_plano']]['arquivo']) > 0) {
            return ERP_URL . "/Pessoas/" . CONTRATO . "/Documentos/" . $item['id_fornecedores_despesas'] . "/" . $contratos['residencia'][$item['id_plano']]['arquivo'];
        } else if ($tipo == "H" && $contratos && strlen($contratos['habilitacao'][$item['id_plano']]['arquivo']) > 0) {
            return ERP_URL . "/Pessoas/" . CONTRATO . "/Documentos/" . $item['id_fornecedores_despesas'] . "/" . $contratos['habilitacao'][$item['id_plano']]['arquivo'];
        } else if ($tipo == "D" && $contratos && strlen($contratos['documento'][$item['id_plano']]['arquivo']) > 0) {
            return ERP_URL . "/Pessoas/" . CONTRATO . "/Documentos/" . $item['id_fornecedores_despesas'] . "/" . $contratos['documento'][$item['id_plano']]['arquivo'];
        } else {
            return "";
        }
    }

    public function getBuscarContratos($items) {
        try {
            $params = [
                "functionPage" => 'buscarContratos',
                "loja" => CONTRATO,
                "items" => $items
            ];
            $response = request(ERP_URL . 'Modulos/Sivis/ws_api.php', 'POST', $params, [
                'Content-Type: application/json'
            ]);
            return $response;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function getListaAcessorios($data) {
        try {
            $dados = [];
            $funcionario = $_SESSION[CHAVE_CONTRATO]['indicador'];
            $acessorios = $this->veiculoRepository->getAcessorios($data['id_plano']);
            $dados['acessorios'] = array_map(function ($item) {
                return [
                    'nome' => $item['descricao'],
                    'valor' => $item['preco_venda']
                ];
            }, $acessorios);
            $dados['convenios'] = $this->pendenciaRepository->getConveniosPorPlano($data['id_plano']);
            $dados['permissao'] = $this->pendenciaRepository->getValoresDesconto($funcionario['id_usuario']);
            return $dados;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function saveUploadComprovante($data) {
        try {
            if (!isset($_FILES['arquivo'])) {
                throw new Exception('Arquivo não enviado', 400);
            }
            $params = array_merge($data, [
                'functionPage' => 'saveUploadComprovante',
                'loja' => CONTRATO,
                'arquivo' => curl_file_create($_FILES['arquivo']['tmp_name'], $_FILES['arquivo']['type'], $_FILES['arquivo']['name'])
            ]);
            $response = request(ERP_URL . 'Modulos/Sivis/ws_api.php', 'POST', $params, [], true);
            if (isset($response['erro'])) {
                throw new Exception($response['erro'], 500);
            }
            return [
                'id_mens' => $data['id_mens'],
                'id_plano' => $data['id_plano'],
                'url' => $response['arquivo']
            ];
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function salvarConvenios($data) {
        try {
            $funcionario = $_SESSION[CHAVE_CONTRATO]['indicador'];
            $dadosConvenio = $this->pendenciaRepository->getConveniosPorPlano($data['id_plano']);
            $permissao = $this->pendenciaRepository->getValoresDesconto($funcionario['id_usuario']);
            $valorDescontoMensalidade = $this->valorReferente($dadosConvenio['valor_total'], $permissao['fin_desc_plano_tot']);
            $valorDescontoAdesao = $this->valorReferente($dadosConvenio['valor_adesao'], $permissao['fin_desc_serv_tot']);

            if (!(intval($permissao['fin_desc_plano']) || intval($permissao['fin_desc_serv']))) {
                throw new Exception('Você não tem permissão para aplicar desconto, entre em contato com o suporte', 403);
            }
            if (intval($permissao['fin_desc_plano']) && floatval($data['desconto']) > valoresNumericos2(escreverNumero($valorDescontoMensalidade))) {
                throw new Exception('O valor de desconto da mensalidade não poder ser maior que ' . escreverNumero($valorDescontoMensalidade, 1), 400);
            }
            if (intval($permissao['fin_desc_serv']) && floatval($data['adesao']) > valoresNumericos2(escreverNumero($valorDescontoAdesao))) {
                throw new Exception('O valor da taxa de adesão não poder ser maior que ' . escreverNumero($valorDescontoAdesao, 1), 400);
            }
            Conexao::initTransaction();            
            if (intval($permissao['fin_desc_serv']) && floatval($data['adesao']) > 0) {
                if (!intval($dadosConvenio['convenio_adesao'])) {
                    $id_convenio = $this->convenioRepository->inserirConvenio([
                        'descricao' => 'Desconto Adesão ' . $dadosConvenio['id_plano'],
                        'convenio_especifico' => 1,
                        'id_conv_plano' => $dadosConvenio['id_plano']
                    ]);
                    $this->convenioRepository->inserirConvenioRegra([
                        'id_convenio' => $id_convenio,
                        'valor' => escreverNumero($data['adesao']),
                        'tp_valor' => 'D'
                    ]);                
                    $this->convenioRepository->inserirConvenioProduto([
                        'id_convenio' => $id_convenio,
                        'id_produto' => $dadosConvenio['conta_produto_adesao']
                    ]);              
                    $this->convenioRepository->inserirConvenioUsuario([
                        'id_fornecedor' => $dadosConvenio['id_fornecedores_despesas'],
                        'id_convenio' => $id_convenio,
                        'dt_inicio' => $dadosConvenio['dt_inicio_mens'],
                        'dt_fim' => escreverDataSoma($dadosConvenio['dt_inicio_mens'], '+1 month -1 day')
                    ]);
                } else {
                    $this->convenioRepository->atualizaConvenioRegra([
                        'id_convenio' => $dadosConvenio['convenio_adesao'],
                        'valor' => escreverNumero($data['adesao']),
                        'tp_valor' => 'D'
                    ]);
                }
            }            
            if (intval($permissao['fin_desc_plano']) && floatval($data['desconto']) > 0) {
                if (!intval($dadosConvenio['convenio_veiculo'])) {
                    $id_convenio = $this->convenioRepository->inserirConvenio([
                        'descricao' => $dadosConvenio['placa'],
                        'convenio_especifico' => 1,
                        'id_conv_plano' => $dadosConvenio['id_plano']
                    ]);
                    $this->convenioRepository->inserirConvenioRegra([
                        'id_convenio' => $id_convenio,
                        'valor' => escreverNumero($data['desconto']),
                        'tp_valor' => 'D'
                    ]);
                    $this->convenioRepository->inserirConvenioProduto([
                        'id_convenio' => $id_convenio,
                        'id_produto' => $dadosConvenio['conta_produto']
                    ]);
                    $this->convenioRepository->inserirConvenioUsuario([
                        'id_fornecedor' => $dadosConvenio['id_fornecedores_despesas'],
                        'id_convenio' => $id_convenio,
                        'dt_inicio' => $dadosConvenio['dt_inicio_mens'],
                        'dt_fim' => escreverDataSoma($dadosConvenio['dt_inicio_mens'], '+1 year -1 day')
                    ]);
                } else if (intval($dadosConvenio['convenio_veiculo'])) {
                    $this->convenioRepository->atualizaConvenioRegra([
                        'id_convenio' => $dadosConvenio['convenio_veiculo'],
                        'valor' => escreverNumero($data['desconto']),
                        'tp_valor' => 'D'
                    ]);
                }
            }
            Conexao::commit();
        } catch (Exception $e) {
            Conexao::rollback();
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function finalizarAdesao($data) {
        try {
            $this->pendenciaRepository->atualizarStatusVeiculo('Aguarda', "status = 'Pendente' and id = (select top 1 id_veiculo from sf_vendas_planos 
            where id_plano = " . valoresSelect2($data['id_plano']) . " and dt_cancelamento is null)");
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function cancelarAdesao($data) {
        try {
            $this->pendenciaRepository->atualizarStatusVeiculo('Reprovado', "status = 'Pendente' and id = (select top 1 id_veiculo from sf_vendas_planos 
            where id_plano = " . valoresSelect2($data['id_plano']) . " and dt_cancelamento is null)");
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function removerConvenio($data) {
        try {
            if (!(isset($data['id_plano']) && $data['id_plano'])) {
                throw new Exception('id_plano é obrigatório', 400);
            }
            $dadosConvenio = $this->pendenciaRepository->getConveniosPorPlano($data['id_plano']);
            if (!($dadosConvenio['convenio_veiculo'] || $dadosConvenio['convenio_adesao'])) {
                throw new Exception('Convenio não encontrado', 404);
            }
            if (!(($dadosConvenio['convenio_veiculo'] && $dadosConvenio['convenio_veiculo'] === $data['id_convenio']) || ($dadosConvenio['convenio_adesao'] && $dadosConvenio['convenio_adesao'] === $data['id_convenio']))) {
                throw new Exception('Não pode remover esse convênio', 403);
            }
            $idProduto = $dadosConvenio['convenio_veiculo'] === $data['id_convenio'] ? $dadosConvenio['conta_produto'] : $dadosConvenio['conta_produto_adesao'];
            Conexao::initTransaction();
            $this->convenioRepository->removeConvenioUsuario([
                'id_fornecedor' => $dadosConvenio['id_fornecedores_despesas'],
                'id_convenio' => $data['id_convenio'],
            ]);
            $this->convenioRepository->removeConvenioProduto([
                'id_convenio' => $data['id_convenio'],
                'id_produto' => $idProduto
            ]);
            $this->convenioRepository->removeConvenioRegra($data['id_convenio']);
            $this->convenioRepository->removeConvenio($data['id_convenio']);
            Conexao::commit();
        } catch (Exception $e) {
            Conexao::rollback();
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function enviarContratoWhatsapp($data) {
        try {
            $mensagem = "Olá Sr. *" . $data['razao_social'] . "* \nesse é um link para assinatura digital do seu contrato :\n"
                    . ERP_URL . "/Modulos/Seguro/FormAssinatura.php?id=" . $data['id_plano'] . "&crt=" . CONTRATO .
                    "&idContrato=" . $data['contrato_id'] . "&idCliente=" . $data['id_usuario'] . "\nAtt. " . NOME;
            $response = request(ERP_URL . "/util/sms/send_sms.php", "GET", [
                "btnEnviar" => 'S',
                "txtTipo" => "1",
                "txtSendSms" => $data['celular'] . "|" . $data['id_usuario'],
                "ckeditor" => $mensagem,
                "hostname" => CONEXAO_HOSTNAME,
                "database" => CONEXAO_DATABASE,
                "username" => CONEXAO_USERNAME,
                "password" => CONEXAO_PASSWORD
            ]);
            if ($response && count($response) > 0 && $response[0]['qtd_sucesso'] > 0) {
                return true;
            }
            throw new Exception($response, 500);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function enviarVistoriaWhatsapp($data) {
        try {
            $mensagem = "Olá Sr. *" . $data['razao_social'] . "* \neste é o link para sua vistoria :\n" .
            $data['url_vistoria'] . "\nAtt. " . NOME;
            $response = request(ERP_URL . "/util/sms/send_sms.php", "GET", [
                "btnEnviar" => 'S',
                "txtTipo" => "1",
                "txtSendSms" => $data['celular'] . "|" . $data['id_usuario'],
                "ckeditor" => $mensagem,
                "hostname" => CONEXAO_HOSTNAME,
                "database" => CONEXAO_DATABASE,
                "username" => CONEXAO_USERNAME,
                "password" => CONEXAO_PASSWORD
            ]);
            if ($response && count($response) > 0 && $response[0]['qtd_sucesso'] > 0) {
                return true;
            }
            throw new Exception($response, 500);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function removerContrato($data) {
        try {
            $params = [
                "functionPage" => 'removerContrato',
                "loja" => CONTRATO,
                "id_usuario" => $data['id_usuario'],
                "id_plano" => $data['id_plano'],
                "contrato_id" => $data['contrato_id']
            ];
            $response = request(ERP_URL . 'Modulos/Sivis/ws_api.php', 'POST', $params, [
                'Content-Type: application/json'
            ]);
            return $response;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    private function valorReferente($valorTotal, $valor) {
        return valoresNumericos2($valorTotal) * valoresNumericos2($valor) / 100;
    }

}
