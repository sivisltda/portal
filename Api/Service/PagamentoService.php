<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 26/08/2020
* Time: 15:27
*/

class PagamentoService {

    private $produtoRepository;
    private $planoRepository;
    private $mensalidadeRepository;
    private $pagamentoRepository;
    private $usuarioRepository;
    private $configRepository;
    private $credencialRepository;

    public function __construct() {
        $this->produtoRepository        = new ProdutoRepository();
        $this->planoRepository          = new PlanoRepository();
        $this->mensalidadeRepository    = new MensalidadeRepository();
        $this->pagamentoRepository      = new PagamentoRepository();
        $this->usuarioRepository        = new UsuarioRepository();
        $this->configRepository         = new ConfigRepository();
        $this->credencialRepository     = new CredencialRepository();
    }

    /**
     * Busca a info Inicial
     * @return array
     * @throws Exception
     */
    public function getInfoInicial($id_usuario) {
        try {
            $veiculoRepository = new VeiculoRepository();
            $modulos = getModulos();
            return [
                'tipo'       => PLANO_UNICO ? 'unico' : 'multiplo',
                'terminal'   => TERMINAL_VENDA,
                'link_whats' => MENSAGEM_LINK_PAG,                
                'modelo'     => $modulos['seg'] ? 'seguro' : 'clube',
                'data'       => PLANO_UNICO ? $this->getPlano($id_usuario)
                    : ($modulos['seg'] ? 
                    array_map(function($item) {
                        return [
                            'id' => $item['id'],
                            'nome' => $item['descricao']
                        ];
                    }, $veiculoRepository->getTiposVeiculos()) 
                    : $this->produtoRepository->listaGruposProdutos("id_contas_movimento in (select conta_movimento from sf_vendas_planos vp
                inner join sf_produtos p on p.conta_produto = vp.id_prod_plano
                where favorecido = ".valoresSelect2($id_usuario).") and inativa = 0"))
            ];
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function getPlano($id_usuario) {
        try {
            $plano = $this->planoRepository->getPlano('favorecido = '.valoresSelect2($id_usuario)."
             and dt_cancelamento is null");
            $condicao = "and id_produto = ".valoresSelect2($plano['id_produto'])." and id_parcela !=".valoresSelect2($plano['id_parcela']);
            $plano['parcelas'] = $this->produtoRepository->getListaProdutosParcelas($condicao);
            return $plano;
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function getListaMensalidadesPorPlano($id_plano) {
        try {
            $dados = $this->pagamentoRepository->getListaMensalidadesPorPlano($id_plano);
            $dados = array_map(function($item) {
                $item['items'] = $this->mensalidadeRepository->getItemsMensalidade($item['id_mens']);
                return $item;
            }, $dados);
            return $dados;
        } catch (Exception $e) { 
            throw new Exception($e->getMessage());
        }
    } 

    /**
     * Atualiza a Forma de Pagamento
     * @param $data
     * @throws Exception
     */
    public function atualizarFormaPagamento($data) {
        try {
            Conexao::initTransaction();
            $this->mensalidadeRepository->atualizarParcelaMensalidades($data);
                
            if (isset($data['cartao'])) {
                $this->planoRepository->criarCartaoUsuario($data['usuario_id'], $data);
            }
            Conexao::commit();
        } catch (Exception $e) {
            Conexao::rollback();
            throw new Exception($e->getMessage());
        }
    }

    public function calculaMultaMensalidade($id_mens) {
        try {
            $dadosMens = $this->mensalidadeRepository->getInfoMensalidade($id_mens);
            if (!count($dadosMens)) {
                throw new Exception('Mensalidade não encontrada', 404);
            }
            return $this->verificaMulta($dadosMens);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function processarPagamento($data) {
        try {
            $dados = $this->mensalidadeRepository->getMensalidadePlano('id_mens = '.valoresSelect2($data['id_mens']));
            //var_dump($dados);exit;
            if (!$dados) {
                throw new Exception('mensalidade Inválida', 404);
            }
            if ($dados['id_venda']) {
                throw new Exception('mensalidade já paga', 402);
            }
            $dados['usuario_id'] = $data['usuario_id'];
            $data["id_usuario"] = $data['usuario_id'];
            $data['id_prim_mens'] = $dados['id_prim_mens'];
            $modulos = getModulos();
            if (!isset($data['parcela']) && isset($data['id_parcela'])) {
                $this->mensalidadeRepository->atualizarParcMens($data);
                $parcelaNova = $this->produtoRepository->getProdutoParcela('id_parcela = '.valoresSelect2($data['id_parcela']));
                $data['parcela'] = $parcelaNova['parcela'];
            }
            if ($modulos['seg'] && MENS_PLANO_CONTRACT && isset($data['id_produto'])) {
                $total = $this->planoRepository->getTotalAcessorios('id_mens = '.valoresSelect2($data['id_mens']));
                if (!$total) {
                    $parcela = $this->produtoRepository->getProdutoParcela("p.conta_produto = ".valoresSelect2($data['id_produto']). " and parcela = 12");
                    if (!$parcela) {
                        throw new Exception('Configuração de parcelas incorreto', 500);
                    }
                }
            }
            if (!isset($parcela)) {
                if (isset($data['id_parcela']) && $data['id_parcela']) {
                    $condicaoParcela = 'id_parcela = '.valoresSelect2($data['id_parcela']);
                } else if (isset($data['parcela']) && $data['parcela'] && isset($data['id_produto'])) {
                    $condicaoParcela = "p.conta_produto = ".valoresSelect2($data['id_produto']). " and parcela = ".valoresSelect2($data['parcela']);
                } else {
                    throw new Exception('Parametros inválidos', 400);
                }
                $parcela = $this->produtoRepository->getProdutoParcela($condicaoParcela);
                if (!$parcela) {    
                    throw new Exception('Parcela do Produto inválido', 400);
                }
            }
           
            $dados['parcela'] = $parcela['parcela'];
            $dados['num_parcela'] = $parcela['num_parcela'];
            $data['num_parcela'] = $dados['num_parcela'];
            /*if (!isset($data['nao_atualiza'])) {
                if (TIPO_VENCIMENTO == 2 || TIPO_VENCIMENTO == 1) {                        
                    $data['data_vencimento'] = TIPO_VENCIMENTO == 2 ? $this->mensalidadeRepository->getDiaVencimentoMensalidade($dados['id_mens']) : DIA_VENCIMENTO;
                }
                $data['produto']     = $parcela;
                $data['dt_fim']      = valoresData2($dados['dt_inicio_mens']);
                $data['id_plano']    = $dados['id_plano'];
                $this->mensalidadeRepository->salvarMensalidades($data);
                $this->planoRepository->atualizaStatusPlano($data['id_plano']);
            }*/
            $response = [];
            if ($dados['parcela'] == 0 && !isset($data['nao_atualiza'])) {
                if (isset($data['cartao'])) {
                    $this->planoRepository->criarCartaoUsuario($data['usuario_id'], $data);
                }
                $this->atualizaMensalidadeECriaCredencial($data);
                $response = $this->processaDCC($data);
                if ($response['TotalSucesso']) {
                    $this->criaCredencial($data);
                    $this->criarChamadoPosVenda($data);
                }
            } else {
                if (isset($data['cartao'])) {
                    $data =  array_merge($data, $this->mensalidadeRepository->getValorRealMensalidade($data['id_mens'])); 
                    if ($dados['parcelar_site'] && isset($dados['num_parcela'])) {
                        $data['num_parcelas'] = $dados['num_parcela'];
                    }
                    $response = $this->processaCartao($data);
                } else if (isset($data['link'])) {
                    $response = $this->verificaCriaLink($dados, $data);
                } else if ($dados['parcela'] == -2) {
                    $response = $this->processaPix($dados);
                } else {                    
                    $response = $this->verificaCriaBoleto($dados, $data);
                }
            }
            if (!(isset($response['TotalDcc']) && $response['TotalDcc'])) {
                $mensagem = isset($response['MensagemDCC']) && $response['MensagemDCC'] ? $response['MensagemDCC'] : 'Ocorreu um erro ao fazer a transação';
                $code = isset($response['TotalErro']) && $response['TotalErro'] ? 400 : 500;
                throw new Exception($mensagem, $code);
            }
            return [
                'plano'     => $this->planoRepository->getPlano("id_plano = ".valoresSelect2($dados['id_plano'])." and A.planos_status not in ('Inativo', 'Cancelado')", 0),
                'response'  => $response
            ];
            
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function verificaMulta($data) {
        try {
            $valor = valoresNumericos2($data['valor']);
            $configMulta = $this->configRepository->getConfigMultaPorData($data['data']);
            if (!count($configMulta)) {
                return escreverNumero(0, 1);
            }
            $mul_tipo = $configMulta['aca_tipo_multa'];
            if ($configMulta['aca_forma_multa'] == 0) {
                $mul_taxa = ($configMulta['aca_taxa_multa_dias'] <= $configMulta['dias'] ? (($valor * $configMulta['aca_taxa_multa']) / 100) : 0);
                $mul_mora = (($valor * $configMulta['aca_mora_multa']) / 100);
            } else {
                $mul_taxa = ($configMulta['aca_taxa_multa_dias'] <= $configMulta['dias'] ? $configMulta['aca_taxa_multa'] : 0);
                $mul_mora = $configMulta['aca_mora_multa'];
            }
            if ($configMulta['aca_tolerancia_multa'] > 0) {
                $dias = $configMulta['dias'] - $configMulta['aca_tolerancia_multa'];
            } else {
                $dias = $configMulta['dias'];
            }
            $totalDias = $this->configRepository->getCalculaDiaUtil($data['data']);
            if ($totalDias > 0) {
                $dias = $dias - $totalDias;
            }
            if ($dias <= 0) {
                return escreverNumero(0, 1);
            }
            if ($mul_tipo == 1) {
                $total = ($mul_mora * $dias);
            } else if ($mul_tipo == 2) {
                $total = (($mul_mora * $dias) + $mul_taxa);
            } else if ($mul_tipo == 3) {
                $total = $mul_taxa;
            }
            return escreverNumero($total, 1);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Processa Pagamento
     * @param $data
     * @return mixed
     * @throws Exception
     */
    public function processaCartao($data) {
        try {
            $postfiels = [
                'hostname'          => CONEXAO_HOSTNAME,
                'database'          => CONEXAO_DATABASE,
                'username'          => CONEXAO_USERNAME,
                'password'          => CONEXAO_PASSWORD,
                'login_usuario'     => getLoginUser(),
                'btnSalvar'         => 'S',
                'txtPlanoItem'      => isset($data['id_mens']) ? $data['id_mens'] : 0,
                'txtCartaoNome'     => $data['nome_cartao'],
                'txtCartaoNumero'   => $data['cartao_numero'],
                'txtCartaoBandeira' => $data['cartao_bandeira'],
                'txtCartaoCodSeg'   => $data['cvv'],
                'txtCartaoValidade' => str_replace(' ', '', $data['data_expiracao']),
                'txtCartaoTipo'     => 0,
                'txtTpPayment'      => isset($data['tp_pagamento']) ? $data['tp_pagamento'] : "1",
                'txtValor'          => $data['valor_pagar'],
                'txtParcelas'       => isset($data['num_parcelas']) ? $data['num_parcelas'] : 1,
                'txtId'             => $data['usuario_id'],
                'txtTipoPag'        => $data['id_tipo_documento']
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, ERP_URL.'util/dcc/makeItemDCC.php');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postfiels);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $res = curl_exec($ch);
            if (is_numeric($res)) {
                $data['id_trans'] = $res;
                $this->atualizaMensalidadeECriaCredencial($data);
                $idVenda = $this->processaVenda($data);
                if ($idVenda) {
                    $this->criaCredencial($data);
                    $this->criarChamadoPosVenda($data);
                }
                return [
                    'MensagemDCC'   => '',
                    'TotalErro'     => 0,
                    'TotalSucesso'  => 1,
                    'TotalDcc'      => 1,
                ];
            } else {
                return [
                    'MensagemDCC'   => $res,
                    'TotalErro'     => 1,
                    'TotalSucesso'  => 0,
                    'TotalDcc'      => 1,
                ];
            }
            return $res;
        } catch (Exception $error) {
            throw new Exception($error);
        }
    }

    public function processaVenda($data) {
        $adesaoSoma = $data['id_mens'] === $data['id_prim_mens'] && !intval($data['adesao_soma']) && valoresNumericos2($data['taxa_adesao']) > 0;
        try {
            $getFields = [
                'hostname'          => CONEXAO_HOSTNAME,
                'database'          => CONEXAO_DATABASE,
                'username'          => CONEXAO_USERNAME,
                'password'          => CONEXAO_PASSWORD,
                'login_usuario'     => getLoginUser(),
                'Trn'               => $data['id_trans'],
                'txtValorAde'       => $data['taxa_adesao'],
                'adesaoSoma'        => $adesaoSoma,
                'txtParcelas'       => isset($data['num_parcelas']) ? $data['num_parcelas'] : 1
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, ERP_URL.'/Modulos/Sivis/Dcc_Transacao_Pagamento.php?'.http_build_query($getFields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $res = curl_exec($ch);
            if (is_numeric($res)) {
                salvarLog(['tabela' => 'sf_vendas', 'id_item' => $res,
                'usuario' => getLoginUser(), 'acao' => 'V', 
                'descricao' => 'VENDA DTPAG: '.date('d/m/Y'),
                'id_fornecedores_despesas' => $data['usuario_id']]);
            }
            return $res;

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function processaDCC($data) {
        try {
            $getFields = [
                'onLineDCC' => 'S',
                'cc' => encrypt(CONTRATO.'|'.KEY_CRIPT, KEY_SIVIS, true),
                'IdMens' => $data['id_mens']
            ];
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, ERP_URL.'Modulos/Sivis/ws_atualizabanco.php?'.http_build_query($getFields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $res = curl_exec($ch);
            return json_decode($res, 1);
        } catch (Exception $error) {
            throw new Exception($error->getMessage());
        }
    }

    public function processaPix($data) {
        try {
            $getFields = [
                'onLinePIX' => 'S',
                'cc' => encrypt(CONTRATO.'|'.KEY_CRIPT, KEY_SIVIS, true),
                'IdMens' => $data['id_mens']
            ];
                
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, ERP_URL.'Modulos/Sivis/ws_atualizabanco.php?'.http_build_query($getFields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $res = curl_exec($ch);
            $dados = json_decode($res, 1);
            if (!$dados && !is_numeric($dados)) {
                throw new Exception($dados);
            }
            return [
                'MensagemDCC'   => '',
                'TotalSucesso'  => 1,
                'TotalDcc'      => 1,
                'boleto'        => [
                    'id'        => $dados,
                    'url'       => ERP_URL.'Boletos/boleto_pix.php?id=M-'.$data['id_mens'].'&crt='.CONTRATO
                ] 
            ];
        } catch (Exception $error) {
            throw new Exception($error->getMessage());
        }
    }

    public function processaBoletoRecorrente($data) {
        try {
            $getFields = [
                'onLineBoletoDCC' => 'S',
                'cc' => encrypt(CONTRATO.'|'.KEY_CRIPT, KEY_SIVIS, true),
                'IdMens' => $data['id_mens']
            ];
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, ERP_URL.'Modulos/Sivis/ws_atualizabanco.php?'.http_build_query($getFields));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $res = curl_exec($ch);
            $dados = json_decode($res, 1);
            if (!$dados || !(isset($dados['boleto']) && $dados['boleto']['id'])) {
                throw new Exception('Ocorreu um erro ao gerar o documento de cobrança!');
            }
            return $dados;
        } catch (Exception $error) {
            throw new Exception($error->getMessage());
        }
    }
    
    public function verificaCriaBoleto($data, $parcela) {
        try {
            $dados = $this->pagamentoRepository->verificaBoleto("id_mens = ".valoresSelect2($data['id_mens'])." and favorecido = ".valoresSelect2($data['usuario_id']));
            if (!$dados) {
                throw new Exception('Código de Mensalidade inválido', 400);
            }
            if ($parcela['parcela'] == -1) {
                if (!is_numeric($dados['id_boleto']) || (($dados['id_mens'] === $dados['id_prim_mens']) && ($dados['bol_valor'] !== $dados['valor_mens']))) {
                    $response = $this->processaBoletoRecorrente($data);
                    if (isset($response['TotalSucesso']) && $response['TotalSucesso'] == 1 && isset($response['boleto']['id'])) {
                        $boletoAnt = $dados['id_boleto'];    
                        $atualiza = is_numeric($dados['id_boleto']) && $response['boleto']['id'] != $dados['id_boleto'];  
                        salvarLog(['tabela' => 'sf_boleto', 'id_item' => $response['boleto']['id'],
                        'usuario' => getLoginUser(), 'acao' => $atualiza ? 'A' : 'I', 
                        'descricao' => $atualiza ? 'ALTERACAO BOLETO RECORRENTE: '.$boletoAnt.' -> '.$response['boleto']['id'] 
                        : 'INCLUSAO BOLETO RECORRENTE',
                        'id_fornecedores_despesas' => $data['usuario_id']]);
                        $dados['id_boleto'] = $response['boleto']['id'];
                    }
                }
            } else if (!is_numeric($dados['id_boleto'])) {
                $dados['id_boleto'] = $this->pagamentoRepository->inserirBoleto($data, $parcela);
            }
            if ($dados['id_boleto']) {
                $response = [
                    'MensagemDCC'   => '',
                    'TotalSucesso'  => 1,
                    'TotalDcc'      => 1,
                    'boleto'        =>  [
                        'id'        => $dados['id_boleto'],
                        'url'       => ERP_URL.'Boletos/Boleto.php?id=M-'.$dados['id_mens'].'&crt='.CONTRATO
                    ] 
                ];
            } else {
                throw new Exception('Ocorreu um erro ao gerar o boleto!', 500);
            }
            return $response;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
    
    public function verificaCriaLink($data, $parcela) {
        try {
            $dados = $this->pagamentoRepository->verificaLink("id_mens = ".valoresSelect2($data['id_mens'])." and favorecido = ".valoresSelect2($data['usuario_id']));
            if (!$dados) {
                throw new Exception('Código de Mensalidade inválido', 400);
            }
            if (!is_numeric($dados['id_link'])) {
                $dados['id_link'] = $this->pagamentoRepository->inserirLink($data, $parcela);
                $dados = $this->pagamentoRepository->verificaLink("id_mens = ".valoresSelect2($data['id_mens'])." and favorecido = ".valoresSelect2($data['usuario_id']));                
            }
            if ($dados['id_link']) {
                $link = encrypt(($dados['data_parcela'] . "|" . $dados['id_link'] . "|" . CONTRATO . "|" . $dados['valor']), "VipService123", true);
                $response = [
                    'MensagemDCC'   => '',
                    'TotalSucesso'  => 1,
                    'TotalDcc'      => 1,
                    'boleto'        =>  [
                        'id'        => $dados['id_link'],
                        'url'       => ERP_URL.'Modulos/Financeiro/frmLinkPagamento.php?link='.$link
                    ] 
                ];
            } else {
                throw new Exception('Ocorreu um erro ao gerar o Link!', 500);
            }
            return $response;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function atualizaMensalidadeECriaCredencial($data) {
        try {
            if (isset($data['data_vencimento']) && ACA_EX_VEN_PR && $data['id_mens'] === $data['id_prim_mens']) {
                $diaVencimento = $this->diaVencimento($data['data_vencimento']);
                $this->mensalidadeRepository->atualizaDataMensalidade($diaVencimento, $data, $data['num_parcela']);
            }
        } catch(Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    public function criaCredencial($data) {
        if (isset($data['data_vencimento']) && ACA_EX_VEN_PR && $data['id_mens'] === $data['id_prim_mens']) {
            $agora = new DateTime();
            $diaVencimento = $this->diaVencimento($data['data_vencimento']);
            $dataVencimento = DateTime::createFromFormat('d/m/Y', $diaVencimento);
            $dataDiff = intval($agora->diff($dataVencimento)->format('%R%a'));
            if ($dataDiff > 0) {
                $this->credencialRepository->criaCredencialUsuario([
                    'id_usuario' => $data['usuario_id'], 
                    'id_credencial' => 1,
                    'dias' => $dataDiff,
                    'mensagem' => 'período grátis'
                ]);
            }
        }
    }

    public function diaVencimento($numDia) {
        try {
            list($dia, $mes, $ano ) = explode('/', date('d/m/Y', strtotime('now')));
            $diaVencimento = $numDia.'/'.$mes.'/'.$ano; 
            $dataVencimento = DateTime::createFromFormat('d/m/Y', trim($diaVencimento));
            $agora = new DateTime();
            $diff = intval($agora->diff($dataVencimento)->format('%R%a'));
            $proximoMes = (new DateTime())->modify('+1 month -1 days');
            $diffProxMes = intval($agora->diff($proximoMes)->format('%R%a'));
    
            if ($diff + 10 < 0) {
                $dataVencimento->modify('+1 month');
                $diffDataVencimento = intval($agora->diff($dataVencimento)->format('%R%a'));
                if ($diffDataVencimento > $diffProxMes + 10) {
                    $dataVencimento->modify('-1 month');
                }
            }
            return  $dataVencimento->format('d/m/Y');
            
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }
    
    private function criarChamadoPosVenda($data) {
        try {
            $dados = $this->planoRepository->getDadosPlano('vp.id_plano = '.valoresSelect2($data['id_plano']));
            if ($data['id_mens'] === $dados['id_prim_mens']) {
                $mensagem = "O Cliente ".$dados['razao_social']." da matrícula ".$dados['id_fornecedores_despesas']
                ." acabou de contratar o plano ".$dados['descricao']." com o início na ".$dados['dt_inicio'].". \n";
                if ($dados['placa']) {
                    $mensagem.="Pertencente ao veículo com a placa ".$dados['placa']." da marca ".$dados['marca']
                    ." do modelo ".$dados['modelo'].".";
                }
                $usuarios = $this->usuarioRepository->listaUsuariosParaChamado();
                $dataUsuario = [
                    'id_usuario' => $data['usuario_id'],
                    'mensagem' => $mensagem,
                    'obs' => '',
                    'procedencia' => $dados['procedencia']
                ];
                foreach($usuarios as $usuario) {
                    $this->usuarioRepository->inserirChamado(array_merge($dataUsuario, ['usuario_resp' => $usuario['id_usuario']]));
                }
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}