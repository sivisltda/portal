<?php

class WhatsAppService {
  private $whatsappRepository;

  public function __construct(){
    $this->whatsappRepository = new WhatsAppRepository();
  }


  public function getModelo() {
    try {
        $modelo = $this->whatsappRepository->getModelo('id_email = '.valoresSelect2(WHAT_PADRAO));
        return $modelo['mensagem'];
    } catch (Exception $e) {
        throw new Exception($e->getMessage());
    }
}


public function salvarWhatsApp($data) {
  try {
    $id = $this->whatsappRepository->insert($data);
    if(!is_numeric($id)) {
      throw new Exception('Ocorreu um erro ao salvar mensagem do whats');
    }
    return $id;
  } catch (Exception $e) {
    throw new Exception($e->getMessage());
  }
}
}