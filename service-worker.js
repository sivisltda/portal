const version = 28;
const includeUrl = [
    "/App/View/login.php",
    "getMarca",
    "buscarCores",
    "manifest.json"
];

self.addEventListener('install', function(event) {
    console.log('Install Event', event);
    self.skipWaiting();
});

self.addEventListener('activate', function (event) {
    deleteCacheVersionPrevious(version);
});

self.addEventListener('fetch', function(event) {
    const request = event.request;
    let promiseResponse = null;
    const verifyUrl = checkUrlInclude(request, includeUrl);
    const nameCache = verifyUrl ? 'amd-url-' : 'amd-files-';
    if(/^http/.test(request.url)) {
        if(['script', 'style', 'image'].includes(request.destination) ||  verifyUrl) {
            promiseResponse = caches.match(request.url)
            .then(cacheResponse => {
                if(cacheResponse) {
                    return cacheResponse;
                } else {
                   return fetch(request).then(response => {
                       let responseClone = response.clone();
                       saveInfoCache(request, responseClone, nameCache+version);
                       return response;
                  });
                }
            });
        } else {
            promiseResponse = fetch(request).then(response => {
                if(response) {
                    let responseClone = response.clone();
                    saveInfoCache(request, responseClone, 'amd-url-'+version);
                    return response;
                }
            }).catch(erro => {
                console.log(erro);
                return caches.match(request.url);
            });
        }
        event.respondWith(promiseResponse);
    }
    //console.log('Fetch Event ', event);
});

function saveInfoCache(request, response, name) {
    caches.open(name).then(cache => {
        cache.put(request.url, response);
    });
}


function deleteCacheVersionPrevious(version) {
    for (let i = 1; i < version; i++ ) {
      caches.delete('amd-files-'+i);
      caches.delete('amd-url-'+i);
    }
}

function checkUrlInclude(request, urls) {
     return urls.find(url => {
        const regex = new RegExp(url);
        return regex.test(request.url);
    });
}
