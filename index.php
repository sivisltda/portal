<?php
session_start();
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

define('DR', DIRECTORY_SEPARATOR);
require_once __DIR__.DR."Api".DR."utils".DR."Languages".DR."funcoesAux.php";
requireOnce(__DIR__."/Api/config/config.default.php");
requireOnce(__DIR__."/Api/bootstrap/app.php");
