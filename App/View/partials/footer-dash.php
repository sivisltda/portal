
<footer class="footers d-print-none" id="footer-dash">
    <div class="container-fluid">
        <div class="row justify-content-center">            
            <div class="col-md-4" style="text-align: center;">                
                <a style="" target="_blank" href="http://www.sivis.com.br/"> <b> <?= date('Y') ?> © SIVIS Tecnologia </b> </a>
            </div>
        </div>
    </div>
</footer>
<input type="hidden" id="site" value="<?=SITE?>">
<input type="hidden" id="api" value="<?=API?>">
<input type="hidden" id="erp" value="<?= ERP_URL ?>">
<input type="hidden" id="contrato" value="<?= CONTRATO ?>">

<div class="boxLoader">
<div id="loader"></div>
</div>

<!-- end Footer -->