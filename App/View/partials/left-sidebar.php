<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!-- LOGO -->
        <a href="dashboard" class=" container-logo-dashboard text-center">
            <img class="logo-dashboard" src="<?= $image ?>" alt="<?= SITE_NAME?>" />           
        </a>
        <!--- Sidemenu -->
        <ul id="menu-side-nav" class="metismenu side-nav">
            <?php  
            foreach($menu as $item) { ?>
                <li class="side-nav-item">
                    <a href="<?= $item['link'] ?>" 
                        <?= isset($item['target']) && $item['target'] ? 'target="__blank"' : ''?>
                        class="side-nav-link <?= $item['class'] ?>">
                        <i class="<?= $item['icon'] ?>"></i>
                        <span> <?= $item['label'] ?></span>
                    </a>
                </li>
            <?php } ?>
        </ul>
        <!-- End Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->

</div>
