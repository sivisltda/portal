<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8" />
        <title><?= SITE_NAME;?></title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="<?= SITE_NAME ?>" name="description" />
        <meta content="Sivis Tecnologia" name="author" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= LOGO_WHATS ? LOGO_WHATS : SITE."assets/img/icon.png"?>">
        <!-- App css -->
        <!-- build:css -->
        <link href="<?= SITE?>assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <!-- endbuild -->
        <link href="<?=SITE?>assets/css/card_credito.css" rel="stylesheet" type="text/css" />
        <link href="<?=SITE?>assets/css/DatPayment.css" rel="stylesheet" type="text/css" />

        <link href="<?=SITE?>Plugins/sweetAlert/sweetalert2.min.css" rel="stylesheet" type="text/css" />

        <link href="<?=SITE?>assets/css/vendor/dataTables.bootstrap4.css" rel="stylesheet">
        <link href="<?=SITE?>assets/css/vendor/responsive.bootstrap4.css" rel="stylesheet">

        <link href="<?=SITE?>assets/css/main.css" rel="stylesheet" type="text/css" />

        <?= isset($style) ? $style : ''?>
        <?php requireOnce(__DIR__.'/style.php');?>
        <style>
        .authentication-bg{
            background: none !important;
        }
        </style>
    </head>

    <body class="authentication-bg">