<input type="hidden" id="cor_primaria" value="<?= COR_PRIMARIA ?>">
<input type="hidden" id="cor_secundaria" value="<?= COR_SECUNDARIA ?>">
<input type="hidden" id="cor_destaque" value="<?= COR_DESTAQUE ?>">
<input type="hidden" id="contrato" value="<?= CONTRATO ?>">
<input type="hidden" id="site" value="<?= SITE ?>">
<input type="hidden" id="api" value="<?= API ?>">
<input type="hidden" id="erp" value="<?= ERP_URL ?>">
<footer class="footer-checkout mt-3 p-2 px-md-0 d-print-none">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-3">
                <div class="d-flex justify-content-center justify-content-md-start">
                    <div class="brand brand-card-visa">
                        <img src="<?= SITE ?>assets/img/icon-card-visa.png" class="img-fluid" alt="Visa">
                    </div>
                    <div class="brand brand-card-mastercard">
                        <img src="<?= SITE ?>assets/img/icon-card-mastercard.png" class="img-fluid" alt="Master Card">
                    </div>
                    <div class="brand brand-card-elo">
                        <img src="<?= SITE ?>assets/img/icon-card-elo.png" class="img-fluid" alt="Elo">
                    </div>
                    <div class="brand brand-card-amex">
                        <img src="<?= SITE ?>assets/img/icon-card-amex.png" class="img-fluid" alt="Amex">
                    </div>
                    <div class="brand boleto">
                            <img src="<?= SITE ?>assets/img/boleto.png" class="img-fluid" alt="Boleto" 
                                style="height: 34px;margin-top: -4px;margin-left: -3px;">
                        </div>
                </div>
            </div>
            <?php if (TELEFONE) {?>
            <div class="col-md-3 text-center text-md-right">
                <p class="font-weight-bold">SAC: <?= TELEFONE ?></p>
                <p>Dúvidas? Fale Conosco!</p>
            </div>
            <?php } ?>
        </div>
        <div class="row justify-content-center mt-1">
            <div class="text-center">
                <p class="font-weight-bold"><?= date('Y') ?> © Desenvolvido por SIVIS TECNOLOGIA </p>
                <p><?= SITE_NAME ?></p>
                <p>CNPJ: <?= CNPJ ?></p>
            </div>
        </div>
    </div>
</footer>
<div class="boxLoader">
    <div id="loader"></div>
</div>