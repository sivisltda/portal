<style type="text/css">
    .boxLoader #loader {
        color: <?= COR_PRIMARIA?>;
    }
    .bg-primaria {
        background: <?= COR_PRIMARIA?> !important;
    }
    .bg-secundaria {
        background: <?= COR_SECUNDARIA ?> !important;
    }

    .bg-destaque {
        background: <?= COR_DESTAQUE ?> !important;
    }
    .texto-primaria {
        color: <?= COR_PRIMARIA ?> !important;
    }

    .texto-secundaria {
        color: <?= COR_SECUNDARIA ?> !important;
    }
    .texto-destaque {
        color: <?= COR_DESTAQUE ?> !important;
    }

    .btn-primaria {
        background: <?= COR_PRIMARIA?> !important;
        color: #fff;
    }

    .btn-secundaria {
        background: <?= COR_SECUNDARIA ?> !important;
        color: #fff;
    }

    .btn-destaque {
        background: <?= COR_DESTAQUE ?> !important;
        color: #fff;
    }


    .btn-primaria:hover, .btn-secundaria:hover, .btn-destaque:hover {
        filter: brightness(90%);
    }

    .btn-next.active,
    .info-ordem::after,
    .lista-forma-pagamento .link-pagamento.active 
    {
        background: <?= COR_PRIMARIA ?>;
    }


    .btn-next.active:hover {
        filter: brightness(90%);
    }

    .info-valores-plano {
        background: <?= COR_PRIMARIA ?>;
    }

    .horizontal-steps .horizontal-steps-content .step-item,
    .horizontal-steps .horizontal-steps-content .step-item.current:before,
    .horizontal-steps .horizontal-steps-content .step-item.current span
    { 
        color: <?= COR_PRIMARIA ?>;
    }

    .lista-dependente .btn:hover {
        background: #fff;
        color: <?= COR_SECUNDARIA ?>;
    }

    .form-checkout label {
        color: <?= COR_PRIMARIA ?>;
    }

    .forma-pagamento h4 {
        color: <?= COR_SECUNDARIA?>;
    }

    .container-dependente {
        border-color: <?= COR_SECUNDARIA ?>;
    }

    .custom-checkbox .custom-control-input:checked~.custom-control-label::before { 
        background-color: <?= COR_SECUNDARIA ?>;
        color: <?= COR_SECUNDARIA ?>;
    }

    .action-service input[data-switch]:checked+label {
        background-color: <?= COR_SECUNDARIA ?>;
    }

    .form-checkout .item-adicional .btn-adicional i {
        color: <?= COR_SECUNDARIA ?>;
    }
    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
        color: #fff;
        background-color: <?= COR_PRIMARIA ?>;
    }

</style>
