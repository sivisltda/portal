<!-- Topbar Start -->
<div class="navbar-custom d-print-none">
    <div class="d-flex">
        <div class="topbar-menu">
            <button class="button-menu-mobile open-left disable-btn">
                <i class="mdi mdi-menu"></i>
            </button>
        </div>
        <?php $menuPerfil = isset($_SESSION[CHAVE_CONTRATO]['indicador']) && $_SESSION[CHAVE_CONTRATO]['indicador']['id_fornecedores_despesas'] !== $_SESSION[CHAVE_CONTRATO]['restricted_id']; ?>
        <div class="topbar-info d-flex justify-content-between">
            <div class="px-2 <?= $menuPerfil ? 'menu-perfil-usuario': ''?>">
                <a href="javascript:void(0)" class="d-flex link-menu-perfil">
                    <div class="icon">
                        <span class="mdi mdi-briefcase-outline"></span>
                    </div>
                    <div class="text-empresa">
                        <p class="nome-empresa"><?= CONTRATO." - ".strtoupper(SITE_NAME) ?></p>
                        <p class="endereco-empresa"><?= ENDERECO ?></p>
                    </div>               
                </a>
                <?php if($menuPerfil) { ?>
                    <div class="tipo-perfil">
                        <div class="form-group">
                            <label for="tipoUsuario">Tipo de Perfil:</label>
                            <select name="tipoUsuario" id="tipoUsuario" class="form-control">
                                <option value="funcionario" <?= $_SESSION[CHAVE_CONTRATO]['login_ativo'] === 'funcionario' ? 'selected' : ''?>>Consultor</option>
                                <option value="cliente" <?= $_SESSION[CHAVE_CONTRATO]['login_ativo'] === 'cliente' ? 'selected' : ''?>>Cliente</option>
                            </select>
                        </div>
                    </div>
                <?php } ?> 
            </div>
            <ul class="list-unstyled topbar-right-menu mb-0">                    
                <li class="dropdown notification-list">
                    <a class="nav-link dropdown-toggle nav-user arrow-none mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <?php
                        $file_headers = get_headers(ERP_URL."/Pessoas/".CONTRATO."/Clientes/".$_SESSION[CHAVE_CONTRATO]['restricted_id'].".png");
                        if($file_headers[0] == 'HTTP/1.1 404 Not Found') { ?>
                            <img src="<?= SITE.'assets/images/users/person.png'?>" alt="<?= $_SESSION[CHAVE_CONTRATO]['nome'] ?>"  class="rounded">
                    <?php } else { ?>
                        <img src="<?= ERP_URL."/Pessoas/".CONTRATO."/Clientes/".$_SESSION[CHAVE_CONTRATO]['restricted_id'].".png" ?>" alt="<?= $_SESSION[CHAVE_CONTRATO]['nome']?>" class="rounded">
                    <?php } ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">
                        <?php 
                            $modulos = getModulos(); 
                            if ($modulos['sau']) {
                        ?>
                         <a href="perfil" class="dropdown-item notify-item">
                            <i class="mdi mdi-account-circle"></i>
                            <span>Meu Perfil</span>
                        </a>
                        <?php } ?>
                        <a href="javascript:void(0)" class="dropdown-item notify-item logout">
                            <i class="mdi mdi-logout"></i>
                            <span>Sair</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div> 
    </div>    
</div>