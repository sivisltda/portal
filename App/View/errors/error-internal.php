<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <title>Proclubes - Erro Internal</title>
        <link rel="stylesheet" href="<?= SITE ?>assets/css/app.min.css" type="text/css" />
        <link rel="stylesheet" href="<?= SITE ?>assets/css/style-error.css" type="text/css">
        <link rel="shortcut icon" href="<?= SITE ?>assets/images/logo.png">
    </head>
    <body>
        <div class="container-erro">
            <div class="card-erro box-shadow">
                <div class="card-erro-info">
                    <img src="<?= SITE ?>assets/images/logo.png" class="img-logo-erro" />
                    <h1 class="mt-4 mb-3">500 - Erro Interno</h1>
                    <p>Ocorreu um interno em nosso servidor, por favor tente novamente mais tarde</p>
                    <p>Ou entre em contato com o nosso suporte</p>
                    <p><?= $exception->getMessage(); ?></p>
                </div>
                <div class="card-erro-image">
                    <img src="<?= SITE ?>assets/images/robo.jpg" class="img-fluid"/>
                </div>
            </div>
        </div>
    </body>
</html>