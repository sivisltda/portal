<?php

if( empty($_SESSION[CHAVE_CONTRATO]['restricted_id']) ){
    header('Location: login');
    exit;
}
else{
    $cod_usu = $_SESSION[CHAVE_CONTRATO]['restricted_id'];
}

function isSaude() {
    $modulos = getModulos();
    if (!$modulos['sau']) {
        header('Location: dashboard');
        exit;
    }
}

?>