<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 24/06/2020
 * Time: 09:33
 */

if (!(isset($_SESSION[CHAVE_CONTRATO]['restricted_id']) && $_SESSION[CHAVE_CONTRATO]['login_ativo'] === 'cliente')) {
    header('Location: dashboard');
    exit;
}
