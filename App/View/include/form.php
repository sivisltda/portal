<?php 

    function createInput($inputs) {
        $html = "";
        foreach($inputs as $input) {
            $html.= '<div class="'.$input['size'].'">';
            $html.= '<div class="form-group mb-3">';
            $html.= '<label for="'.$input['id'].'">'.$input['label'].'</label>';
            if($input['type'] === 'select') {
                $html.= createSelect($input);
            } else {
                $html.= createInputText($input);
            }
            $html.='<div class="invalid-feedback"></div>';
            $html.="</div></div>";
        }
        return $html;
    }

    function createSelect($input) {
        $html ='<select name="'.$input['name'].'" id="'.$input['id'].'" ';
        foreach($input['attributes'] as $attribute => $value) {
            if($attribute !== 'placeholder') $html.=$attribute.'="'.$value.'" ';
        }                
        $html.=$input['required'] ? 'required' : ''; 
        $html.='>';
        if(isset($input['attributes']['placeholder'])) {
            $html.='<option value="">'.$input['attributes']['placeholder'].'</option>';
        }
        foreach($input['options'] as $value => $option) {
            $html.='<option value="'.$value.'">'.$option.'</option>';
        }
        $html.='</select>';
        return $html;
    }

    function createInputText($input) {
        $html ='<input type="'.$input['type'].'" name="'.$input['name'].'" id="'.$input['id'].'" ';
        foreach($input['attributes'] as $attribute => $value) {
            $html.=$attribute.'="'.$value.'" ';
        }
        $html.=$input['required'] ? 'required' : ''; 
        $html.='/>'; 
        return $html;
    }