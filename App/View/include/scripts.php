<!-- App js -->
<script src="<?=SITE?>assets/js/app.min.js"></script>
<script src="<?=SITE?>Plugins/sweetAlert/sweetalert2.all.js"></script>
<script src="<?=SITE?>assets/js/vendor/jquery.dataTables.js"></script>
<script src="<?=SITE?>assets/js/vendor/dataTables.bootstrap4.js"></script>
<script src="<?=SITE?>assets/js/vendor/dataTables.responsive.min.js"></script>
<script src="<?=SITE?>assets/js/vendor/fnReloadAjax.js"></script>
<script src="<?=SITE?>assets/js/vendor/responsive.bootstrap4.min.js"></script>
