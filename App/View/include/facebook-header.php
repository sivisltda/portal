<meta property="og:locale" content="pt_BR">
<meta property="og:url" content="<?= $dados['url'] ?>">
<meta property="og:title" content="<?= $dados['titulo'] ?>">
<?php 
    if (LOGO_WHATS) {
?>
    <meta property="og:image" content="<?= LOGO_WHATS ?>">
    <meta property="og:image:type" content="image/<?= getExtensao(LOGO_WHATS)?>">
<?php        
    }
?>
<meta property="og:site_name" content="<?= $dados['titulo'] ?>>">
<meta property="og:description" content="<?= limitText(strip_tags($dados['descricao']), 400) ?>">
<meta property="og:type" content="website">