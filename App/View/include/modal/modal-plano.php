<div class="modal fade" id="modalPlano" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Plano - <span class="nome-plano"></span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row mb-2">
                            <div class="col-md-4">
                                <h4>Mensalidades</h4>
                            </div>
                            <div class="col-md-8">
                                <div class="row" id="status_filtro"></div>
                            </div>
                        </div>
                        <table id="table-mensalidade" class="table table-striped dt-responsive nowrap w-100 dataTable no-footer dtr-inline">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th>Período</th>
                                    <th>Status</th>
                                    <th>Valor a Pagar</th>
                                    <th>Valor Pago</th>
                                    <th>Dt. Venc.</th>
                                    <th>Dt. Pag.</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal"> Fechar </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h4>Detalhes da Mensalidade</span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <table class="table table-bordered table-responsive dt-responsive table-sm" id="table-details">
                            <thead class="bg-primary text-white"></thead>
                            <tbody></tbody>
                            <tfoot></tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal"> Fechar </button>
            </div>
        </div>
    </div>
</div>
