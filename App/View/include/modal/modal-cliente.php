<div class="modal fade" id="modalCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="nome-cliente"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <ul class="accordion" id="accordionInfo">
                    <li id="headingInfo" class="card mb-0">
                        <a href="#info" class="card-header bg-secundaria text-white" data-toggle="collapse"  aria-expanded="true" aria-controls="info" >Informações</a>
                        <div id="info" class="collapse show" aria-labelledby="headingInfo" data-parent="#accordionInfo">
                            <div class="row">
                                <div class="col-lg-6">
                                    <ul class="accordion" id="accordionCliente">
                                        <li id="headingClienteInfo" class="card card-100 mb-0">
                                            <a href="#clienteInfo" class="card-header bg-info text-white" data-toggle="collapse" data-target="#clienteInfo" aria-expanded="true" aria-controls="clienteInfo">Informação Pessoal</a>
                                            <div id="clienteInfo" class="collapse show" aria-labelledby="headingClienteInfo" data-parent="#accordionCliente">
                                                <div class="card-body"  id="info-pessoal"></div>
                                            </div>                                        
                                        </li>
                                    </ul>                                    
                                </div>                                    
                                <div class="col-lg-6">
                                    <ul class="accordion" id="accordionEndereco">
                                        <li id="headingClienteEndereco" class="card card-100 mb-0">
                                            <a href="#clienteEndereco" class="card-header bg-info text-white" data-toggle="collapse" data-target="#clienteEndereco" aria-expanded="true" aria-controls="clienteEndereco">Endereço</a>
                                            <div id="clienteEndereco" class="collapse show" aria-labelledby="headingClienteEndereco" data-parent="#accordionEndereco">
                                                <div class="card-body" id="info-endereco"></div>
                                            </div> 
                                        </li>
                                    </ul>                                    
                                </div>                                    
                            </div>                                    
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="accordion" id="accordionPlano">
                                        <li id="headingPlano" class="card card-100 mb-0">
                                            <a href="#plano" class="card-header bg-info text-white" data-toggle="collapse"  aria-expanded="true" aria-controls="plano">Plano</a>
                                            <div id="plano" class="collapse show px-2" aria-labelledby="headingPlano" data-parent="#accordionPlano"></div>                         
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li id="headingMensalidade" class="card card-100 mb-0">
                        <a href="#mensalidade" class="card-header bg-secundaria text-white" data-toggle="collapse"  aria-expanded="false" aria-controls="mensalidade">Mensalidades</a>
                        <div id="mensalidade" class="collapse" aria-labelledby="headingMensalidade" data-parent="#accordionInfo">
                            <div class="row">
                                <div class="col-12">
                                    <table id="table-mensalidade" class="table table-striped dt-responsive">
                                        <thead class="bg-secundaria text-white">
                                            <tr>
                                                <th>Período</th>
                                                <th>Valor</th>
                                                <th>Data Venc.</th>
                                                <th>Data Pag.</th>
                                                <th>Status</th>
                                                <th>Ação</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li id="headingDependentes" class="card card-100 mb-0">
                        <a href="#dependentes" class="card-header bg-secundaria text-white" data-toggle="collapse"  aria-expanded="false" aria-controls="dependentes">Dependentes</a>
                        <div id="dependentes"  class="collapse" aria-labelledby="headingDependentes" data-parent="#accordionInfo">
                            <div class="row">
                                <div class="col-12">
                                    <table id="table-dependentes" class="table table-striped dt-responsive">
                                        <thead class="bg-secundaria text-white">
                                            <tr>
                                                <th>Nome</th>
                                                <th>Parentesco</th>
                                                <th>Data de Inclusão</th>
                                                <th>Ação</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>                            
                                    </table>                                          
                                </div>
                            </div>
                        </div>                    
                    </li>
                </ul>                                       
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal"> Fechar </button>
            </div>
        </div>
    </div>
</div>