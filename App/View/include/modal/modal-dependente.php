<div class="modal fade" id="modalDetailsDependentes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 1060;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h4>Detalhes do Dependente <span id="nome-dependente"></span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="detalhe-dependente"></div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal"> Fechar </button>
            </div>
        </div>
    </div>
</div>