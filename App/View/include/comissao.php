<div class="row">
    <div class="col-xl-10 mb-3">
        <label>Período:</label>
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="row">
                    <div class="col-4 p-1">
                        <button class="btn btn-light btn-block auto-date btn-radius" type="button" data-from="" data-to="">Todo</button>
                    </div>
                    <div class="col-4 p-1">
                        <button class="btn btn-light btn-block auto-date btn-radius" type="button" data-from="<?php echo sprintf("01/%s", date("m/Y")); ?>" data-to="<?php echo date("d/m/Y"); ?>">Este mês</button>
                    </div>
                    <div class="col-4 p-1">
                        <button class="btn btn-light btn-block auto-date btn-radius" type="button" data-from="<?php echo date("d/m/Y"); ?>" data-to="<?php echo date("d/m/Y"); ?>">Hoje</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="row">
                    <div class="input-group input-radius col-12 col-sm-6 p-1">
                        <div class="input-group-prepend"><span class="input-group-text">De</span></div>
                        <input type="text" class="form-control" data-mask="00/00/0000" value="<?= sprintf("01/%s", date("m/Y")) ?>" placeholder="dd/mm/aaaa" id="rel-date-from">
                    </div>
                    <div class="input-group input-radius col-12 col-sm-6 p-1">
                        <div class="input-group-prepend"><span class="input-group-text">Até</span></div>
                        <input type="text" class="form-control" data-mask="00/00/0000" value="<?= date("d/m/Y") ?>" placeholder="dd/mm/aaaa" id="rel-date-to">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-2 col-sm-4 mb-2 pt-xl-4 ml-sm-auto">
        <button type="button" class="btn btn-primaria btn-block btn-radius" id="btnfind">Filtrar</button>
    </div>
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body card-vendas">
                <h4 class="header-title mb-3">Comissão</h4>
                <div class="row">
                    <div class="col-12 order-2 order-xl-1">
                        <table id="tblComissao" class="table table-responsive2 dt-responsive">
                            <tbody></tbody>
                            <tfoot></tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6">
        <div class="row">
            <div class="col-lg">
                <div class="card widget-flat">
                    <div class="card-body p-0">
                        <div class="d-flex">
                            <div class="bg-success text-white item-widget">
                                <i class="mdi mdi-cash"></i>
                            </div>
                            <div class="col">
                                <h3 id="quantidade_total"></h3>
                                <span>Vendas</span>
                            </div>
                        </div>
                    </div> <!-- end card-body-->
                </div>
            </div>
            <div class="col-lg">
                <div class="card widget-flat">
                    <div class="card-body p-0">
                        <div class="d-flex">
                            <div class="bg-warning text-white item-widget">
                                <i class="mdi mdi-cash"></i>
                            </div>
                            <div class="col">
                                <h3 id="valor_comissao"></h3>
                                <span>Comissão</span>
                            </div>
                        </div>
                    </div> <!-- end card-body-->
                </div>
            </div>
            <div class="col-lg">
                <div class="card widget-flat">
                    <div class="card-body p-0">
                        <div class="d-flex">
                            <div class="bg-danger text-white item-widget">
                                <i class="mdi mdi-cash"></i>
                            </div>
                            <div class="col">
                                <h3 id="valor_reprovado"></h3>
                                <span>Reprovado</span>
                            </div>
                        </div>
                    </div> <!-- end card-body-->
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card w-100" id="card-vendas-resumo">
                    <div class="card-body">
                        <h4 class="header-title mb-3">Comissão / Recebida</h4>
                        <hr/>
                        <div class="row splits">
                            <div class="col">
                                <div class="split cliente">
                                    <div class="empresa">Recebidos</div>
                                    <div class="valor" id="recebido"></div>
                                    <svg width="16" height="10">
                                        <polygon points="0,0 16,0 8,10" class="triangulo" />
                                        <line x1="0" y1="0" x2="8" y2="10" class="linha" />
                                        <line x1="16" y1="0" x2="8" y2="10" class="linha" />
                                    </svg>
                                </div>
                            </div>
                            <div class="col">
                                <div class="split sivis text-right">
                                    <div class="empresa">A receber</div>
                                    <div class="valor" id="receber"></div>
                                    <svg width="16" height="10">
                                        <polygon points="0,0 16,0 8,10" class="triangulo" />
                                        <line x1="0" y1="0" x2="8" y2="10" class="linha" />
                                        <line x1="16" y1="0" x2="8" y2="10" class="linha" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div class="progress-bar-wrapper">
                            <div class="percentual percentual-1" id="per-1"></div>
                            <div class="progress-bar">
                                <div class="cliente" id="progressBar"></div>
                            </div>
                            <div class="percentual percentual-2" id="per-2"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-xl-12 mb-3">
        <div class="card">
            <div class="card-body card-dashboard">
                <h4 class="header-title mb-3">Vendas</h4>
                <hr/>
                <table id="tbFuncionarios" class="table table-striped dt-responsive">
                    <thead class="bg-secundaria text-white">
                    <tr>
                        <th></th>
                        <th>Parcela</th>
                        <th>Nome</th>
                        <th>Dt. Venda</th>
                        <th>Plano</th>
                        <th>Val.Plano.</th>
                        <th>Val.Serv.</th>
                        <th>Val.Prod.</th>
                        <th>C.Plano 1ª</th>
                        <th>C.Plano</th>
                        <th>C.Serv.</th>
                        <th>C.Prod.</th>
                        <th>C.Total</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalDetailsComissao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h4>Comissão Por Venda</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="detalhes">
                </div>
            </div>
        </div>
    </div>
</div>