<?php requireOnce(__DIR__.'/../partials/header.php'); ?>

        <!-- Begin page -->
        <div class="wrapper">

            <?php requireOnce(__DIR__.'/../partials/left-sidebar.php', compact('menu', 'image')); ?>

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content top-menu">

                    <?php requireOnce(__DIR__.'/../partials/topbar.php'); ?>

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Meus Dependentes</h4>
                                </div>
                            </div>
                        </div>
                        <div id="content">

                        </div>
                    </div> 

                </div>                
                
                <?php requireOnce(__DIR__.'/../partials/footer-dash.php'); ?>

                <div class="modal fade" id="modalDependente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4>Detalhes Dependente <span class="nome-plano"></span></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div id="content-btn" class="d-flex justify-content-end"></div>
                                <form id="form-dependente"></form>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" id="btn-dependente">Salvar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END wrapper -->
        <?php requireOnce(__DIR__.'/../include/scripts.php');?>
        <!-- App js -->
        <script type='module' src="<?=SITE?>assets/js/partials/menu.js"></script>
        <script type='module' src="<?=SITE?>assets/js/pages/dependentes.js"></script>

    </body>
</html>
