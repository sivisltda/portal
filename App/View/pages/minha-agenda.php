<?php
    $style='
        <link href="'.SITE.'assets/css/agenda.css" rel="stylesheet">
    ';
    requireOnce(__DIR__.'/../partials/header.php', compact('style'));
?>


<!-- Begin page -->
<div class="wrapper">

    <?php requireOnce(__DIR__.'/../partials/left-sidebar.php', compact('menu', 'image')); ?>

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content top-menu">

            <?php requireOnce(__DIR__.'/../partials/topbar.php'); ?>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="row">
                                <h4 class=" col-md-2 col-lg page-title">Minha Agenda</h4>
                                <div class="col-md-5 col-lg p-2">
                                    <div class="checkbox-status">
                                        <input type="checkbox" name="search_data_status[]" value="ativo" checked id="check_pago_data">
                                        <label class="pago" for="check_pago_data">
                                            <div class="titulo">Proximos Eventos</div>
                                            <span class="quantidade" id="quant_ativo_data">0</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-5 col-lg p-2">
                                    <div class="checkbox-status">
                                        <input type="checkbox" name="search_data_status[]" value="inativo" id="check_estornado_data">
                                        <label class="estornado" for="check_estornado_data">
                                            <div class="titulo">Eventos Finalizados</div>
                                            <span class="quantidade" id="quant_inativo_data">0</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mb-3">
                        <div class="card card-100">
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-4 mb-3">
                                        <?php 
                                            if (count($grupo) > 1) {
                                        ?>
                                            <select class="form-control mb-3" name="grupo" id="grupo">
                                                <?php 
                                                    foreach($grupo as $item) {
                                                        echo "<option value='".$item['numero_contrato']."'>".$item['razao_social']."</option>";
                                                    }
                                                ?>
                                            </select>            
                                        <?php } ?>
                                        <ul id="list-agenda" class="list-group"></ul>
                                    </div>
                                    <div class="col-xl-9 col-lg-8 calendar">
                                        <input type="hidden" id="agenda_id">
                                        <div class="d-flex justify-content-end mb-3">
                                            <a href="javascript:void()" id="nova-agenda" class="btn btn-success btn-lg">Agendamento</a>
                                        </div>
                                        <div class="calendar-action">
                                            <button class="btn btn-calendar btn-calendar-prev"><span class="mdi mdi-arrow-left"></span> <span class="label-prev">Anterior</span></button>
                                            <h5 id="mes-agenda" class="text-capitalize"></h5>
                                            <button class="btn btn-calendar btn-calendar-next"><span class="label-next">Próximo</span> <span class="mdi mdi-arrow-right"></span></button>
                                        </div>
                                        <ul id="list-agendamento" class="mt-2"></ul>
                                        <nav class="d-flex justify-content-end" id="pagination" aria-label="Page navigation example"></nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            requireOnce(__DIR__.'/../partials/footer-dash.php');            
            requireOnce(__DIR__.'/../include/modal/modal-plano.php');
            ?>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->

</div>
<!-- END wrapper -->
<?php requireOnce(__DIR__.'/../partials/right-sidebar.php'); ?>
<!-- App js -->
<?php  requireOnce(__DIR__.'/../include/scripts.php');?>
<script src="<?= SITE ?>assets/js/vendor/fullcalendar.min.js"></script>
<script src="<?= SITE ?>assets/js/vendor/locale-all.js"></script>
<script src="<?= SITE ?>assets/js/vendor/moment.min.js"></script>
<script src="<?= SITE ?>assets/js/vendor/moment.pt-br.js"></script>

<script type='module' src="<?= SITE ?>assets/js/partials/menu.js"></script>
<script type='module' src="<?= SITE ?>assets/js/pages/agenda.js"></script>
</body>
</html>
