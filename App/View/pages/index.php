<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <title><?= NOME ?></title>
        <meta http-equiv="Content-Type" content="text/html;" />
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="<?= NOME ?>" name="description" />
        <meta content="Sivis Tecnologia" name="author" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= LOGO_WHATS ? LOGO_WHATS : SITE."assets/img/icon.png"?>">

        <!-- third party css -->
        <!-- third party css end -->

        <!-- App css -->

        <!-- build:css -->
        <link href="<?=SITE?>assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <!-- endbuild -->

        <link href="<?=SITE?>Plugins/sweetAlert/sweetalert2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=SITE?>Plugins/credCard/card.css" rel="stylesheet" type="text/css" />
        <link href="<?=SITE?>assets/css/DatPayment.css" rel="stylesheet" type="text/css" />
        <link href="<?=SITE?>Plugins/slick/slick.css" rel="stylesheet" >
        <link href="<?=SITE?>assets/css/style-amd.css" rel="stylesheet" type="text/css">
        <link href="<?=SITE?>assets/css/nav-carrousel.css" rel="stylesheet" type="text/css">
        <link href="<?=SITE?>assets/css/print-single.css" rel="stylesheet" type="text/css" media="print">
        <link rel="manifest" id="my-manifest-placeholder">
        <?php requireOnce(__DIR__.'/../partials/style.php'); ?>        
    </head>
    <body>
        <div class="boxLoader">
            <div id="loader"></div>
        </div>
        <!-- Begin page -->
        <div id="app"></div>
        <!-- bundle -->
        <?php requireOnce(__DIR__.'/../partials/footer.php');?>
        <script>
            const myDynamicManifest = {
                "display": "standalone",
                "name": "<?= SITE_NAME ?>",
                "description": "Portal",
                "short_name": "<?= NOME ?>",
                "id": "<?= "/loja/".CONTRATO?>",
                "scope": "<?= BASE_URL."loja/".CONTRATO?>",
                "start_url": "<?= BASE_URL."loja/".CONTRATO?>",
                "background_color": "<?= COR_PRIMARIA ?>",
                "theme_color": "<?= COR_SECUNDARIA ?>",
                "icons": [{
                    "src": "<?= LOGO_WHATS ?>",
                    "sizes": "554x554",
                    "type": "image/png" 
                }]
            }
            const stringManifest = JSON.stringify(myDynamicManifest);
            const blob = new Blob([stringManifest], {type: 'application/json'});
            const manifestURL = URL.createObjectURL(blob);
            document.querySelector('#my-manifest-placeholder').setAttribute('href', manifestURL);
        </script>
        <script src="<?=SITE?>assets/js/app.min.js"></script>
        
        <!-- third party js -->
        <script type='text/javascript' src="<?=SITE?>Plugins/sweetAlert/sweetalert2.all.js"></script>
        <script type='text/javascript' src="<?=SITE?>Plugins/slick/slick.min.js"></script>
        <script type="text/javascript" src="<?=SITE?>Plugins/credCard/jquery.creditCardValidator.js"></script>
        <script type="text/javascript" src="<?=SITE?>Plugins/credCard/DatPayment.js"></script>
        <script type="text/javascript" src="<?=SITE?>Plugins/credCard/stripe.js"></script>
        <script type="text/javascript" src="<?=SITE?>Plugins/moment/moment.js"></script>
        <!-- third party js ends -->
        <script type='module' src="<?=SITE?>assets/js/pages/script.js"></script>
        <script type="text/javascript">
            if('serviceWorker' in navigator) {
                window.addEventListener('load', function() {
                    navigator.serviceWorker.register('<?= BASE_URL."service-worker.js" ?>')
                        .then(function(registration) {
                            console.log('Service Worker active', registration.scope);
                        }).catch(function(erro) {
                        console.error('Erro register Service Worker', erro);
                    });
                });
            }
        </script>

    </body>

</html>
