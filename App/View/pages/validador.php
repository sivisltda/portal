<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <title><?= NOME ?></title>
        <meta http-equiv="Content-Type" content="text/html;" />
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="<?= NOME ?>" name="description" />
        <meta content="Sivis Tecnologia" name="author" />
        <link rel="shortcut icon" href="<?= LOGO_WHATS ? LOGO_WHATS : SITE."assets/img/icon.png"?>">
        <link href="<?=SITE?>assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=SITE?>Plugins/sweetAlert/sweetalert2.min.css" rel="stylesheet" type="text/css" />
        <?php requireOnce(__DIR__.'/../partials/style.php'); ?>
        <link href="<?=SITE?>assets/css/validador.css" rel="stylesheet" type="text/css" />
       
        <style type="text/css">
            .container-app { 
                <?php 
                $modulos = getModulos();
                if ($modulos['sau']) { ?>
                    background-image: url(<?= SITE ?>/assets/images/background-validador-saude.jpg);
                <?php } else if ($modulos['seg']) { ?>
                    background-image: url(<?= SITE ?>/assets/images/background-validador-seguro.png);
                <?php } else {?>
                    background-image: url(<?= SITE ?>/assets/images/background-validador.png);
                <?php }?>
            }
        </style>
    </head>
    <body>
        <div class="boxLoader">
            <div id="loader"></div>
        </div>
        <div class="container-app">
            <div class="card-validador">
                <form class="form" id="form-buscador">
                    <div class="form-group">
                        <label class="font-weight text-white">CPF / E-mail / Celular / Nome / Matricula
                            <?= $modulos['seg'] ? ' / Placa': '' ?>
                            <?= $campoExtra ? ' / '.$campoExtra['descricao_campo'] : '' ?>:
                        </label>
                        <div class="row align-items-center">
                            <div class="col-md-9 mb-2">
                                <input type="search" name="busca" placeholder="<?= MENSAGEM_ASSOCIADO ?>" class="form-control input-search" />
                                <input type="hidden" name="id_cliente">
                                <div class="invalid-feedback"></div>
                            </div>
                            <div class="col-md-3 mb-2">
                                <button type="submit" class="btn btn-search btn-block btn-primaria">
                                    <span class="mdi mdi-magnify"></span> Buscar
                                </button>
                            </div>
                        </div>
                    </div>                   
                </form>
                <div id="resposta" class="response"></div>
            </div>
            <img src="<?= LOGO_URL ?>" alt="<?= NOME ?>"  class="img-empresa" /> 
        </div>
        <input type="hidden" id="cor_primaria" value="<?= COR_PRIMARIA ?>">
        <input type="hidden" id="cor_secundaria" value="<?= COR_SECUNDARIA ?>">
        <input type="hidden" id="cor_destaque" value="<?= COR_DESTAQUE ?>">
        <input type="hidden" id="contrato" value="<?= CONTRATO ?>">
        <input type="hidden" id="site" value="<?= SITE ?>">
        <input type="hidden" id="api" value="<?= API ?>">
        <input type="hidden" id="erp" value="<?= ERP_URL ?>">
        <input type="hidden" id="modulos" value='<?= json_encode(getModulos())?>' />
        <footer class="footer-checkout p-3 px-md-0">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="text-center">
                        <p class="font-weight-bold"><?= date('Y') ?> © Desenvolvido por SIVIS TECNOLOGIA </p>
                        <p><?= SITE_NAME ?></p>
                        <p>CNPJ: <?= CNPJ ?></p>
                    </div>
                </div>
            </div>
        </footer>
        <script type='text/javascript' src="<?=SITE?>assets/js/app.min.js"></script>
        <script type='text/javascript' src="<?=SITE?>Plugins/sweetAlert/sweetalert2.all.js"></script>
        <script type='text/javascript' src="<?=SITE?>Plugins/typehead/bloodhound.min.js"></script>
        <script type='text/javascript' src="<?=SITE?>Plugins/typehead/typeahead.bundle.min.js"></script>
        <script type='module' src="<?=SITE?>assets/js/pages/validador.js"></script>
    </body>

</html>
