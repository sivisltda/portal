<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <title><?= $dados['titulo'] ?></title>
        <meta http-equiv="Content-Type" content="text/html;" />
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="<?= strip_tags($dados['descricao']) ?>" name="description" />
        <meta content="Sivis Tecnologia" name="author" />
        <link rel="shortcut icon" href="<?= LOGO_WHATS ? LOGO_WHATS : SITE . "assets/img/icon.png" ?>">
        <link href="<?= SITE ?>assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= SITE ?>Plugins/sweetAlert/sweetalert2.min.css" rel="stylesheet" type="text/css" />
        <?php
        requireOnce(__DIR__ . '/../include/facebook-header.php', compact('dados'));
        requireOnce(__DIR__ . '/../partials/style.php');
        ?>
        <link href="<?= SITE ?>assets/css/quem-indica.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            .btn-grad {
                background-image: linear-gradient(to right, <?= COR_PRIMARIA ?> 0%, <?= COR_PRIMARIA . 'B3' ?>  51%, <?= COR_PRIMARIA ?>  100%);
                color: #fff;
                box-shadow: 0 0 20px #eee;
                transition: 0.5s;
                background-size: 200% auto;
            }
            .table-valores-indicator {
                width: 100%;                
            }
            .table-valores-indicator thead{
                color: <?= COR_PRIMARIA ?>;
            }
            .pago {
                color: #0acf97;
            }
        </style>
    </head>
    <body>
        <div class="boxLoader">
            <div id="loader"></div>
        </div>
        <header class="shadow-2">
            <div class="container">
                <nav class="px-0 navbar navbar-expand navbar-default flex-column flex-md-row bd-navbar">
                    <a class="navbar-brand mr-0 mr-md-2" href="https://<?= SITE_CLI ?>">
                        <img  src="<?= LOGO_URL ?>"  class="img-fluid logo"/>
                    </a>
                </nav>
            </div>
        </header>
        <section class="bg-banner shadow-2" style="background-image: url(<?= $dados['id_foto'] ?>);">
            <div class="container h-100">
                <div class="container-btn justify-content-center">
                    <a class="btn px-5 btn-rounded btn-grad text-uppercase" href="#indique">Indique agora</a>
                </div>
            </div> 
        </section>
        <section id="info">
            <div class="container h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-7 mb-lg-0 mb-3 box-info">
                        <div class="card mb-0 rounded-2 shadow-2 p-4">
                            <h2 class="titulo texto-primaria">Indique e Ganhe:</h2>
                            <h1 class="subtitulo"><?= $dados['titulo'] ?></h1>
                            <h4 class="titulo texto-primaria mt-2">Período:</h4>
                            <h3 class="subtitulo"><?= $dados['data_ini'] ?> a <?= $dados['data_fim'] ?></h3>
                            <div class="d-flex justify-content-end mt-2">
                                <a class="btn btn-rounded btn-grad text-uppercase" href="#como-funciona">Saiba Mais</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="como-funciona" class="bg-primaria pt-4">
            <div class="container">
                <h4 class="titulo mb-4">Regulamento:</h4>
                <div class="card shadow-2 rounded-2">
                    <div class="card-body descricao p-4">
                        <?= $dados['descricao'] ?>
                    </div>
                </div>
                <div id="indique" class="row py-3 justify-content-between">
                    <div class="col-lg-6 col-md-6">
                        <h4 class="mb-4 text-white">Indique seus amigos</h4>
                        <div class="card box-card shadow-2 rounded-2">
                            <div class="card-body p-4">
                                <form id="form-lead"></form>
                                <div id="numbers-indicator" class="row"></div>
                            </div>
                            <div id="valores-indicator" class="card-body px-4 pt-0"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <nav class="nav tab-indicator nav-tabs mb-4" id="myTab" role="tablist">
                            <a class="tab-item h4 active" id="recompensa-tab" data-toggle="tab" 
                               href="#recompensa" role="tab" aria-controls="recompensa" aria-selected="true">
                                <span>Indicados</span>
                                <span id="num-indicados" class="badge bg-white texto-primaria">0</span>
                            </a>
                            <a class="tab-item h4" id="cupons-tab" data-toggle="tab" 
                               href="#cupons" role="tab" aria-controls="cupons" aria-selected="false">
                                <span>Cupons</span>
                                <span id="num-cupons" class="badge bg-white texto-primaria">0</span>
                            </a>
                        </nav>
                        <div class="card box-card shadow-2 rounded-2">
                            <div class="card-body">
                                <div class="tab-content texto-primaria p-2" id="myTabContent">
                                    <div class="tab-pane fade show active" id="recompensa" role="tabpanel" aria-labelledby="recompensa-tab"></div> 
                                    <div class="tab-pane fade" id ="cupons" role="tabpanel" aria-labelledby="cupons-tab"></div>  
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <input type="hidden" id="nome_empresa" value="<?= NOME ?>" />
        <input type="hidden" id="consultor" value="<?= isset($dados['consultor']) ? $dados['consultor'] : null ?>" />
        <input type="hidden" id="cor_primaria" value="<?= COR_PRIMARIA ?>">
        <input type="hidden" id="cor_secundaria" value="<?= COR_SECUNDARIA ?>">
        <input type="hidden" id="cor_destaque" value="<?= COR_DESTAQUE ?>">
        <input type="hidden" id="contrato" value="<?= CONTRATO ?>">
        <input type="hidden" id="site" value="<?= SITE ?>">
        <input type="hidden" id="api" value="<?= API ?>">
        <input type="hidden" id="erp" value="<?= ERP_URL ?>">
        <footer>
            <div class="box-info-empresa py-2 bg-white">
                <a href="<?= SITE_CLI ?>" class="logo">
                    <img src="<?= LOGO_URL ?>" alt="<?= NOME ?>" class="img-fluid"/>
                </a>
                <div class="nome-empresa"><?= NOME ?></div>
                <div class="razao-social"><?= SITE_NAME ?></div>
                <div class="cnpj"><?= CNPJ ?></div>
            </div>
            <div class="copy-right bg-primaria">
                <div>©<?= date('Y'); ?> Desenvolvido por <strong><a target="_blank" class="text-white" href="https://www.sivis.com.br">SIVIS TECNOLOGIA</a></strong></div> 
            </div>
        </footer>
        <script type='text/javascript' src="<?= SITE ?>assets/js/app.min.js"></script>
        <script type='text/javascript' src="<?= SITE ?>Plugins/sweetAlert/sweetalert2.all.js"></script>
        <script type='module' src="<?= SITE ?>assets/js/pages/quem-indica.js"></script>
    </body>
</html>  