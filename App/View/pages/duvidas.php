<?php requireOnce(__DIR__.'/../partials/header.php'); ?>

        <!-- Begin page -->
        <div class="wrapper">

            <?php requireOnce(__DIR__.'/../partials/left-sidebar.php', compact('menu', 'image')); ?>

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content top-menu">

                    <?php requireOnce(__DIR__.'/../partials/topbar.php'); ?>

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Dúvidas</h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 

                        <div class="row">
                            <div class="col-md-12">                                
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title mt-0 mb-3">
                                            Dúvidas frequentes
                                        </h4>
                                        <hr/>
                                        <div id="accordion" class="custom-accordion mb-4"></div> 
                                    </div>
                                </div>
                            </div> <!-- end col-->

                        </div>
                        <!-- end row -->
                        
                    </div> <!-- container -->

                </div> <!-- content -->

                <?php requireOnce(__DIR__.'/../partials/footer-dash.php'); ?>

            </div>

        </div>
        <!-- END wrapper -->

        <?php requireOnce(__DIR__.'/../partials/right-sidebar.php'); ?>

        <!-- App js -->
        <?php  requireOnce(__DIR__.'/../include/scripts.php');?>  
        <script type='module' src="<?=SITE;?>assets/js/partials/menu.js"></script>
        <script type='module' src="<?=SITE;?>assets/js/pages/duvidas.js"></script>
    </body>
</html>