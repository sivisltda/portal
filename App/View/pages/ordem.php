<?php requireOnce(__DIR__.'/../partials/header.php'); ?>

        <!-- Begin page -->
        <div class="wrapper">

            <?php requireOnce(__DIR__.'/../partials/left-sidebar.php', compact('menu', 'image')); ?>

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content top-menu">

                    <?php requireOnce(__DIR__.'/../partials/topbar.php'); ?>

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Ordem: <?= $numero ?></h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 

                        <div class="row"></div>
                        <!-- end row -->
                        
                    </div> <!-- container -->

                </div> <!-- content -->

                <?php requireOnce(__DIR__.'/../partials/footer-dash.php'); ?>

            </div>

        </div>
        <!-- END wrapper -->

        <?php requireOnce(__DIR__.'/../partials/right-sidebar.php'); ?>

        <!-- App js -->
        <?php  requireOnce(__DIR__.'/../include/scripts.php');?>  
        <script type='module' src="<?=SITE;?>assets/js/partials/menu.js"></script>
    </body>
</html>