<?php 
    $style = '<link href="'.SITE.'assets/css/pendencias.css" rel="stylesheet" type="text/css" />';    
    requireOnce(__DIR__.'/../partials/header.php', compact('style')); 
?>

        <!-- Begin page -->
        <div class="wrapper">

            <?php requireOnce(__DIR__.'/../partials/left-sidebar.php', compact('menu', 'image')); ?>

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->
            
            <div class="content-page">
                <div class="content top-menu">

                    <?php requireOnce(__DIR__.'/../partials/topbar.php'); ?>

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Pendências</h4>
                                </div>
                            </div>
                        </div>
                        <?php if (isset($funcionarios) && is_array($funcionarios) && count($funcionarios) > 0) {?>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="funcionario">Funcionário</label>
                                    <select name="funcionario" id="funcionario" class="form-control">
                                        <option value="">Selecione um funcionário</option>
                                        <?php foreach ($funcionarios as $funcionario) {?>
                                        <option value="<?= $funcionario['id_fornecedores_despesas']?>">
                                        <?= $funcionario['razao_social']?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="row">
                            <div class="col-xl-12 mb-1">
                                <label>Período:</label>
                                <div class="row">
                                    <div class="col-12 col-lg-5">
                                        <div class="row">
                                            <div class="col-4 p-1">
                                                <button class="btn btn-secundaria btn-block auto-date btn-radius" type="button" data-from="" data-to="">Todo</button>
                                            </div>
                                            <div class="col-4 p-1">
                                                <button class="btn btn-secundaria btn-block auto-date btn-radius" type="button" data-from="<?= sprintf("%s-01", date("Y-m")) ?>" data-to="<?= date("Y-m-d"); ?>">Este mês</button>
                                            </div>
                                            <div class="col-4 p-1">
                                                <button class="btn btn-secundaria btn-block auto-date btn-radius" type="button" data-from="<?= date("Y-m-d"); ?>" data-to="<?= date("Y-m-d"); ?>">Hoje</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-7">
                                        <div class="row">
                                            <div class="input-group input-radius col-12 col-sm-6 p-1">
                                                <div class="input-group-prepend"><span class="input-group-text bg-secundaria text-white">De</span></div>
                                                <input type="date" class="form-control"  value="<?= date('Y-m-d', strtotime('-1 month + 1 day')) ?>" placeholder="dd/mm/aaaa" id="rel-date-from">
                                            </div>
                                            <div class="input-group input-radius col-12 col-sm-6 p-1">
                                                <div class="input-group-prepend"><span class="input-group-text bg-secundaria text-white">Até</span></div>
                                                <input type="date" class="form-control" value="<?= date("Y-m-d") ?>" placeholder="dd/mm/aaaa" id="rel-date-to">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3 align-items-end">
                          <div class="col-12 col-md-4">
                            <label for="nomeCliente">Cliente:</label>
                            <div class="box-autocomplete">
                                <input type="text" class="form-control" name="cliente" id="nomeCliente" placeholder="Nome do Cliente" required>
                                <input type="hidden" name="idCliente" id="idCliente">
                            </div>
                          </div>
                          <div class="col-12 col-md-4">
                              <label for="busca">Pesquisa:</label>
                              <input type="text" name="busca" placeholder="Digite o nome do cliente ou a placa do veiculo" id="busca" class="form-control">
                          </div>
                          <?php if (PORTAL_CONTRATO > 0) { ?>
                          <div class="col-12 col-md-2 mt-2 mt-md-0">
                              <input id="idPortalConstrato" type="hidden" value="<?php echo PORTAL_CONTRATO;?>">
                              <button id="btnAssinatura" class="btn btn-secundaria btn-block btn-radius">Assinatura</button>
                          </div>
                          <?php } ?>
                          <div class="col-12 col-md-2 mt-2 mt-md-0">
                              <button id="btnBusca" class="btn btn-primaria btn-block btn-radius">Buscar</button>
                          </div>
                        </div>
                        <div id="lista-pendencia" class="row"></div>
                    </div> <!-- container -->
                </div> <!-- content -->
                <?php requireOnce(__DIR__.'/../partials/footer-dash.php'); ?>
            </div>
        </div>
        <?php requireOnce(__DIR__.'/../partials/modal-desconto.php'); ?>
        <?php requireOnce(__DIR__.'/../include/scripts.php');?>  
        <script src="<?=SITE?>Plugins/typehead/bloodhound.min.js"></script>
        <script src="<?=SITE?>Plugins/typehead/typeahead.bundle.min.js"></script>        
        <script type='module' src="<?=SITE;?>assets/js/partials/menu.js"></script>
        <script type='module' src="<?=SITE;?>assets/js/pages/pendencia.js"></script>
    </body>
</html>