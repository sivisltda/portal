<?php
    $style = '<link href="'.SITE.'assets/css/clientes.css" rel="stylesheet" type="text/css" />';
    $style .= '<link href="'.SITE.'assets/css/carteirinha.css" rel="stylesheet" type="text/css" />';
    requireOnce(__DIR__.'/../partials/header.php', compact('style')); 
?>
<?php requireOnce(__DIR__.'/../partials/style.php'); ?>  
<!-- Begin page -->
<div class="wrapper">

<?php requireOnce (__DIR__.'/../partials/left-sidebar.php', compact('menu', 'image')); ?>

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <div class="content top-menu">

        <?php requireOnce(__DIR__.'/../partials/topbar.php'); ?>

        <div class="container-fluid">                                                
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">                                   
                        <h4 class="page-title">Meus Prospects</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 px-0">                                
                    <div class="card card-datatable-controls">
                        <div class="card-body">
                            <div class="row mb-2" id="status_filtro"></div>
                            <div class="row">
                                <div class="col-xl-12 mb-3">
                                    <label>Período:</label>
                                    <div class="row">
                                        <div class="col-12 col-lg-5">
                                            <div class="row">
                                                <div class="col-4 p-1">
                                                    <button class="btn btn-secundaria btn-block auto-date btn-radius" type="button" data-from="" data-to="">Todo</button>
                                                </div>
                                                <div class="col-4 p-1">
                                                    <button class="btn btn-secundaria btn-block auto-date btn-radius" type="button" data-from="<?= sprintf("%s-01", date("Y-m")) ?>" data-to="<?= date("Y-m-d"); ?>">Este mês</button>
                                                </div>
                                                <div class="col-4 p-1">
                                                    <button class="btn btn-secundaria btn-block auto-date btn-radius" type="button" data-from="<?= date("Y-m-d"); ?>" data-to="<?= date("Y-m-d"); ?>">Hoje</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-7">
                                            <div class="row">
                                                <div class="input-group input-radius col-12 col-sm-6 p-1">
                                                    <div class="input-group-prepend"><span class="input-group-text bg-secundaria text-white">De</span></div>
                                                    <input type="date" class="form-control" placeholder="dd/mm/aaaa" id="rel-date-from">
                                                </div>
                                                <div class="input-group input-radius col-12 col-sm-6 p-1">
                                                    <div class="input-group-prepend"><span class="input-group-text bg-secundaria text-white">Até</span></div>
                                                    <input type="date" class="form-control" placeholder="dd/mm/aaaa" id="rel-date-to">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <select id="tipo_plano" class="form-control select2">
                                                    <option value="">Selecione um tipo de plano</option>
                                                    <?php 
                                                        foreach($produtos as $produto) {
                                                            echo "<option value=".$produto['id'].">".$produto['nome']."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group mt-1">
                                                <select id="tipo_pagamento" class="form-control select2">
                                                    <option value="">Selecione um modo de pagamento</option>
                                                    <?php 
                                                        foreach($tipos_pagamentos as $tp) {
                                                            echo "<option value=".$tp['id'].">".$tp['descricao']."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <select id="consultor" class="form-control select2">
                                                    <option value="">Selecione um Consultor</option>
                                                    <?php 
                                                        foreach($funcionarios as $func) {
                                                            echo "<option value=".$func['id'].">".$func['nome']."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Pesquisar..." name="txtBusca" id="txtBusca" maxlength="100"  value="" title="Nome">
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <button type="button" class="btn btn-primaria btn-block btn-radius" id="btnfind">Filtrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="row">                           
                <div class="col-12 px-0">
                    <div class="card">
                        <div class="card-body">
                            <table id="tbClientes" class="table table-striped dt-responsive">
                                <thead class="bg-secundaria text-white">
                                    <tr>
                                        <th></th>
                                        <th>Id</th>
                                        <th>Nome</th>
                                        <th>CPF / CNPJ</th>
                                        <th>Email</th>
                                        <th>Plano</th>
                                        <th>Status</th>
                                        <th>Responsavel</th>
                                        <th>Consultor</th>
                                        <th>Dependente</th>
                                        <th>Pagamento</th>
                                        <th>Cidade</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div> 

    <?php 
        requireOnce(__DIR__.'/../partials/footer-dash.php'); 
    ?>
</div>

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

</div>


<?php 
    requireOnce(__DIR__.'/../include/modal/modal-cliente.php');
    requireOnce(__DIR__.'/../include/modal/modal-dependente.php');
    requireOnce(__DIR__.'/../include/modal/modal-plano.php');
    requireOnce(__DIR__.'/../include/scripts.php');
?>                 

<script type='module' src="<?=SITE?>assets/js/pages/clientes.js"></script>
<!-- Google Maps -->

</body>
</html>