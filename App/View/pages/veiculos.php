<?php
    $style = '<link href="'.SITE.'assets/css/veiculos.css" rel="stylesheet">';
    $style.= '<style type="text/css">
        .cardCars {
            background-color: '.COR_PRIMARIA.'26;
        }
        .cardCars hr,
        .cardCars .table th, .cardCars .table td {
            border-top-color: '.COR_SECUNDARIA.';
        }
        .cardCars table thead th {
            border-bottom-color: '.COR_SECUNDARIA.';
        }
        .cardCars .table-header,
        .cardCars .table-footer {
            background-color: '.COR_SECUNDARIA.';
        }
        .body-cobertura ul li + li, .body-cobertura ul li:first-of-type {
            border-bottom-color: '.COR_SECUNDARIA.';
        }
    </style>';
    requireOnce(__DIR__.'/../partials/header.php', compact('style')); ?>

        <!-- Begin page -->
        <div class="wrapper">

            <?php requireOnce(__DIR__.'/../partials/left-sidebar.php', compact('menu', 'image')); ?>

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content top-menu">

                    <?php requireOnce(__DIR__.'/../partials/topbar.php'); ?>

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Meus Veículos</h4>
                                </div>
                            </div>
                        </div>
                        

                        <div class="row">
                            <input type="hidden" id="marcas">
                            <div class="col-md-12" id="veichles"></div>
                        </div>
                    </div> 

                </div> 

                <?php requireOnce(__DIR__.'/../partials/footer-dash.php'); ?>

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        <!-- App js -->
        <?php requireOnce(__DIR__."/../include/scripts.php");?>
        <!-- App js -->
        <script type='module' src="<?= SITE ?>assets/js/partials/menu.js"></script>
        <script type='module' src="<?= SITE ?>assets/js/pages/veiculos.js"></script>
    </body>
</html>
