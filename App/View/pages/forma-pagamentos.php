    <?php
        $style='
            <link rel="stylesheet" href="'.SITE.'assets/css/forma-pagamentos.css"> 
            <link rel="stylesheet" href="'.SITE.'assets/css/nav-carrousel.css"> 
            <link rel="stylesheet" href="'.SITE.'Plugins/slick/slick.css">
            <link rel="stylesheet" href="'.SITE.'Plugins/slick/slick-theme.css">
        ';
        requireOnce(__DIR__.'/../partials/header.php', compact('style')); 
    ?>

    <!-- Begin page -->
    <div class="wrapper">

        <?php requireOnce(__DIR__.'/../partials/left-sidebar.php', compact('menu', 'image')); ?>

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content top-menu">

                <?php requireOnce(__DIR__.'/../partials/topbar.php'); ?>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box">
                                <h4 class="page-title">Formas Pagamentos</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="filtro-plano"></div>
                    <div class="row box-plano-pagamento">
                        <div id="info-plano" class="col-12 mb-3"></div>
                        <div class="col-12 mb-3">
                            <div class="row">
                                <div class="col-sm-8 mb-4">
                                    <h4>Legenda</h4>
                                    <div class="legenda-pagamento">
                                        <div class="item-legenda">
                                            <span class="icone-legenda bg-primaria"></span>
                                            <span class="nome-legenda texto-primaria">Valor Pagar</span>
                                        </div>
                                        <div class="item-legenda">
                                            <span class="icone-legenda bg-secundaria"></span>
                                            <span class="nome-legenda texto-secundaria">Valor Pago</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 d-flex justify-content-end align-items-end mb-4" id="container-btn-alterar"></div>
                            </div>                           
                            <div class="carrousel"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="bloqueio" value="<?= NAO_ALT_PAGAMENTO ?>">
    <?php 
        requireOnce(__DIR__.'/../partials/footer-dash.php');
        requireOnce(__DIR__."/../include/modal/modal-detalhes.php");
        requireOnce(__DIR__."/../include/modal/modal-pagamento.php");
        requireOnce(__DIR__."/../include/modal/modal-mensal.php");
        requireOnce(__DIR__."/../include/scripts.php");
    ?>
    
    <script type="text/javascript" src="<?= SITE ?>Plugins/slick/slick.min.js"></script>
    <script type="text/javascript" src="<?=SITE?>Plugins/credCard/jquery.creditCardValidator.js"></script>
    <script type="text/javascript" src="<?=SITE?>Plugins/credCard/DatPayment.js"></script>
    <script type='module' src="<?= SITE ?>assets/js/partials/menu.js"></script>
    <script type='module' src="<?= SITE ?>assets/js/pages/forma-pagamentos.js"></script>
    <!-- App js -->

    </body>
</html>