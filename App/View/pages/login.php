<?php 
$modulos = getModulos();
if (isset($_SESSION[CHAVE_CONTRATO]['restricted_id'])) {
    header('Location: dashboard');
    exit;
}
?>
<!DOCTYPE html>
<html lang="pt-BR">

    <head>
        <meta charset="utf-8" />
        <title><?= SITE_NAME;?></title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="<?= SITE_NAME;?>" name="description" />
        <meta content="Sivis Tecnologia" name="author" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= LOGO_WHATS ? LOGO_WHATS : SITE."assets/img/icon.png"?>">

        <!-- App css -->
        <!-- build:css -->
        <link href="<?=SITE?>assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <!-- endbuild -->

        <link href="<?=SITE?>assets/css/main.css" rel="stylesheet" type="text/css" />
        <link href="<?=SITE?>assets/css/login.css" rel="stylesheet" type="text/css" />
        
        <link href="<?=SITE?>Plugins/sweetAlert/sweetalert2.min.css" rel="stylesheet" type="text/css" />
        <?php  requireOnce(__DIR__.'/../partials/style.php'); ?>
        <style type="text/css">
            body {
                <?php if ($modulos['seg']) { ?>
                    background-image: linear-gradient( rgba(249, 249, 249, 0.66) 0%, rgba(255, 255, 255, 0.66) 100%), url(<?= SITE ?>/assets/images/fundo-login-seguro.jpg);
                <?php } else if($modulos['sau']) { ?>
                    background-image: linear-gradient( rgba(249, 249, 249, 0.66) 0%, rgba(255, 255, 255, 0.66) 100%), url(<?= SITE ?>/assets/images/fundo-login-saude.jpg);
                <?php } else { ?>
                    background-image: linear-gradient( rgba(249, 249, 249, 0.66) 0%, rgba(255, 255, 255, 0.66) 100%), url(<?= SITE ?>/assets/images/fundo-login.jpg);
                <?php } ?>
            }           
       </style>
    </head>
    <body>
        <div class="boxLoader">
            <div id="loader"></div>
        </div>
        <div class="account-pages">
            <div class="container">
                <div class="row <?= $modulos['seg'] || $modulos['sau'] ? 'justify-content-end' : 'justify-content-center' ?>">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header py-3 text-center">
                                <span><img src="<?= LOGO_URL ?>" alt="<?= SITE_NAME ?>" height="60"></span>
                            </div>
                            <div class="card-body p-3">
                                <h3 class="h4 text-center mb-3"><?= SITE_NAME ?></h3>
                                <div class="text-center w-75 m-auto">
                                    <h4 class="text-dark-50 text-center mt-0 font-weight-bold">Login</h4>
                                    <p class="text-muted mb-3">Digite seus dados para acessar a área restrita</p>
                                </div>
                                <form id="frm-login" action=""  >
                                    <div class="form-group mb-3">
                                        <label for="email">Email:</label>
                                        <input class="form-control" type="email" id="email" required="required" placeholder="Email">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="password">Senha:</label>
                                        <input class="form-control" type="password" required="required" id="password" placeholder="Senha">
                                    </div>
                                    <div class="form-group mb-3">
                                        <!-- <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="checkbox-signin" checked>
                                            <label class="custom-control-label" for="checkbox-signin">Remember me</label>
                                        </div> -->
                                    </div>
                                    <div class="form-group mb-0 text-center">
                                        <button class="btn btn-secundaria btn-cotacao btn-block font-weight-bold" type="submit"> Login </button>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <input type="hidden" id="site" value="<?=SITE?>">
        <input type="hidden" id="api" value="<?=API?>">
        <!-- App js -->
        <script type='text/javascript' src="<?=SITE?>Plugins/sweetAlert/sweetalert2.all.js"></script>
        <script src="<?=SITE?>assets/js/app.min.js"></script>
        <script type='module' src="<?=SITE?>assets/js/pages/login.js?v=<?php echo rand();?>"></script>
    </body>
</html>

