<?php
    $style='
    <link href="'.SITE.'assets/css/vendor/fullcalendar.min.css" rel="stylesheet">
    <link href="'.SITE.'assets/css/agenda.css" rel="stylesheet">
    ';
    requireOnce(__DIR__.'/../partials/header.php', compact('style'));
 ?>
        <div class="wrapper">
            <?php requireOnce(__DIR__.'/../partials/left-sidebar.php', compact('menu', 'image')); ?>
            <div class="content-page">
                <div class="content top-menu">
                    <?php requireOnce(__DIR__.'/../partials/topbar.php'); ?>
                    <div class="container-fluid">                                                
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">                                   
                                    <h4 class="page-title">Agendamento Online</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 px-0 mb-3">
                            <div class="card card-100">
                                <div class="card-body ">
                                    <h4 class="header-title mb-3">Agenda</h4>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-xl-4 col-md-5 mb-3">
                                            <?php if (count($grupo) > 1) { ?>
                                                <select class="form-control mb-3" name="grupo" id="grupo">
                                                    <?php 
                                                        foreach($grupo as $item) {
                                                            echo "<option value='".$item['numero_contrato']."'>".$item['razao_social']."</option>";
                                                        }
                                                    ?>
                                                </select>            
                                            <?php } ?>
                                            <ul id="list-agenda" class="list-group"></ul>    
                                        </div>
                                        <div class="col-xl-8 col-md-7 mb-3">
                                            <input type="hidden" id="agenda_id">
                                            <div class="calendar">
                                                <div class="calendar-action">
                                                    <button class="btn btn-calendar btn-calendar-prev"><span class="mdi mdi-arrow-left"></span> <span class="label-prev">Anterior</span></button>
                                                    <h5>Escolha o seu horário</h5>
                                                    <button class="btn btn-calendar btn-calendar-next"><span class="mdi mdi-arrow-right"></span><span class="label-next">Próximo</span></button>
                                                </div>
                                                <ul class="calendar-container-days" id="list-dias"></ul>
                                                <ul class="calendar-container-hours mt-3"></ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>   
                    <?php 
                        requireOnce(__DIR__.'/../partials/footer-dash.php');
                        requireOnce(__DIR__.'/../include/modal/modal-plano.php');
                    ?>
                </div>
            </div>
        </div>
      
        <div class="modal fade" id="modalAgendamento" tabindex="-1" role="dialog" aria-labelledby="modalAgendamentoTitle" aria-hidden="true">
            <div class="modal-dialog  modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalAgendamentoTitle">Agendar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="form-agendamento">
                            <div class="row">
                                <div class="col-7">
                                    <div class="form-group">
                                        <label for="data_inicio">Data Início:</label>
                                        <input type="text" id="data_inicio" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="form-group">
                                        <label for="hora_inicio">Horário:</label>
                                        <input type="text" id="hora_inicio" class="form-control" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-7">
                                    <div class="form-group">
                                        <label for="data_fim">Data Fim:</label>
                                        <input type="text" id="data_fim" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="form-group">
                                        <label for="hora_fim">Horário:</label>
                                        <input type="text" id="hora_fim" class="form-control" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label for="assunto">Assunto*:</label>
                                <input type="text" name="assunto" id="assunto" class="form-control">
                                <div class="invalid-feedback"></div>
                            </div>
                            <div class="form-group mb-2">
                                <label for="obs">Observação:</label>
                                <textarea name="obs" id="obs" rows="4" class="form-control"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="d-flex justify-content-end">
                            <button class="btn btn-success mr-2" id="btn-salva-agendamento"><span class="mdi mdi-check"></span> Salvar</button>
                            <button class="btn btn-warning text-white" data-dismiss="modal"><span class="mdi mdi-reply"></span> Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php requireOnce(__DIR__.'/../include/scripts.php');?>   
        <script src="<?= SITE ?>assets/js/vendor/fullcalendar.min.js"></script>   
        <script src="<?= SITE ?>assets/js/vendor/locale-all.js"></script> 
          
        <script type='module' src="<?= SITE ?>assets/js/partials/menu.js"></script>
        <script type='module' src="<?= SITE ?>assets/js/pages/agenda.js"></script>
    </body>
</html>
