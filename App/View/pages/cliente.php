<?php 
    $style = '
    <link href="'.SITE.'Plugins/credCard/card.css" rel="stylesheet" type="text/css" />
    <link href="'.SITE.'assets/css/DatPayment.css" rel="stylesheet" type="text/css" />
    <link href="'.SITE.'Plugins/slick/slick.css" rel="stylesheet" type="text/css" />
    <link href="'.SITE.'assets/css/style-amd.css" rel="stylesheet" type="text/css" />
    <link href="'.SITE.'assets/css/cliente.css" rel="stylesheet" type="text/css" />
    <link href="'.SITE.'assets/css/nav-carrousel.css" rel="stylesheet" type="text/css" />
    <link href="'.SITE.'assets/css/pendencias.css" rel="stylesheet" type="text/css" />        
    <link href="'.SITE.'assets/css/print-single.css" rel="stylesheet" type="text/css" media="print">
    ';
    $codigoConsultor = $_SESSION[CHAVE_CONTRATO]['indicador']['id_fornecedores_despesas'];
    $src = BASE_URL."/loja/".CONTRATO."/consultor/".$codigoConsultor;
?>

<?php requireOnce(__DIR__.'/../partials/header.php', compact('style')); ?>
<?php requireOnce(__DIR__.'/../partials/style.php'); ?>    

        <!-- Begin page -->
        <div class="wrapper">

            <?php requireOnce(__DIR__.'/../partials/left-sidebar.php', compact('menu', 'image')); ?>

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content top-menu">

                    <?php requireOnce(__DIR__.'/../partials/topbar.php'); ?>

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div id="container-button" class="d-print-none row justify-content-center align-items-center">
                            <div class="col-sm-5 mb-1">
                                <div class="page-title-box">
                                    <h4 class="page-title">Cotação</h4>
                                </div>
                            </div>
                            <div class="col-sm-7 mb-1">
                                <div class="row">
                                    <div class="col-6">
                                        <button id="normal" class="btn btn-secundaria btn-cotacao btn-block" data-tipo="normal">Cotação Normal</button>
                                    </div>
                                    <div class="col-6">
                                        <button id="rapido" class="btn btn-primaria btn-cotacao btn-block" data-tipo="rapido">Cotação Rápida</button>
                                    </div>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title -->       
                        <!-- end row -->
                        <div id="app"></div>
                    </div> <!-- container -->
                </div> <!-- content -->
                <?php requireOnce(__DIR__.'/../partials/footer-dash.php'); ?>
            </div>
        </div>
        <!-- END wrapper -->
        <?php requireOnce(__DIR__.'/../partials/modal-desconto.php'); ?>
        <?php requireOnce(__DIR__.'/../partials/right-sidebar.php'); ?>
        <!-- App js -->
        <?php  requireOnce(__DIR__.'/../include/scripts.php');?>          
        <script type="text/javascript" src="<?= SITE ?>Plugins/slick/slick.min.js"></script>
        <script type="text/javascript" src="<?=SITE?>Plugins/credCard/jquery.creditCardValidator.js"></script>
        <script type="text/javascript" src="<?=SITE?>Plugins/credCard/DatPayment.js"></script>
        <script type='module' src="<?=SITE;?>assets/js/partials/menu.js"></script> 
        <script type='module' src="<?=SITE;?>assets/js/pages/cliente.js"></script>
    </body>
</html>