<?php
    $style = '<link href="'.SITE.'assets/css/carteirinha.css" rel="stylesheet" type="text/css" />';
    requireOnce(__DIR__.'/../partials/header.php', compact('style'));
?>
        <!-- Begin page -->
        <div class="wrapper">
            <?php requireOnce(__DIR__.'/../partials/left-sidebar.php', compact('menu', 'image')); ?>
            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <div class="content top-menu">
                    <?php requireOnce(__DIR__.'/../partials/topbar.php'); ?>
                    <div class="container-fluid">                                                
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">                                   
                                    <h4 class="page-title">Principal</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">                                
                                <div class="card">
                                    <div class="card-body profile-user-box bg-primaria">
                                        <div class="row">
                                            <div class="col-sm d-flex align-items-center mb-2">
                                                <span class="m-2">
                                                    <?php
                                                        $image = ERP_URL."Pessoas/".CONTRATO."/Clientes/".$_SESSION[CHAVE_CONTRATO]['restricted_id'].".png";
                                                        $file_headers = get_headers($image);
                                                        if($file_headers[0] == 'HTTP/1.1 404 Not Found') { ?>
                                                            <img src="<?= SITE.'assets/images/users/person.png'?>" alt="<?= $_SESSION[CHAVE_CONTRATO]['nome'] ?>" style="height: 50px; width: 50px;" class="rounded-circle img-thumbnail">
                                                    <?php } else { ?>
                                                        <img src="<?= $image ?>" alt="<?= $_SESSION[CHAVE_CONTRATO]['nome']?>" style="height: 50px; width: 50px;" class="rounded-circle img-thumbnail">
                                                    <?php } ?>    
                                                </span>
                                                <div class="media-body">
                                                    <h4 class="text-white nome" style="font-size: 18px;"><?= $_SESSION[CHAVE_CONTRATO]['nome']?></h4>
                                                </div> 
                                            </div>                                                                                        
                                             <div class="col-sm-3 mb-2 d-flex justify-content-end align-items-center">
                                                <a id="editProfile" href="perfil" class="btn btn-outline bg-secundaria text-white">
                                                    <i class="mdi mdi-account-settings-variant mr-1"></i> Editar Perfil
                                                </a> 
                                            </div>
                                        </div> 
                                        <?php if (CONTRATO == '177') { ?>
                                        <div class="d-flex justify-content-between">
                                            <div class="col-sm-3 px-0 text-right">
                                                <a href="https://www.clubedaconsulta.com.br/guia-inclusao/" class="btn btn-outline bg-secundaria text-white" target="__blank">
                                                    <span class="mdi mdi-calendar-check"></span>
                                                    INCLUSÃO DE FUNCIONÁRIO
                                                </a>
                                            </div> 
                                            <div class="col-sm-3 px-0 text-right">
                                                <a href="https://www.clubedaconsulta.com.br/guia-exclusao/" class="btn btn-outline bg-secundaria text-white" target="__blank">
                                                    <span class="mdi mdi-calendar-check"></span>
                                                    EXCLUSÃO DE FUNCIONÁRIO
                                                </a>
                                            </div> 
                                            <div class="col-sm-3 px-0 text-right">
                                                <a href="https://www.clubedaconsulta.com.br/guiaconsulta/" class="btn btn-outline bg-secundaria text-white" target="__blank">
                                                    <span class="mdi mdi-calendar-check"></span>
                                                    GUIA DE CONSULTA
                                                </a>
                                            </div> 
                                        </div>
                                        <?php } ?>                                             
                                    </div> 
                                </div>
                            </div> 
                        </div>
                        <div id="links"></div>
                        <div class="row">
                            <div class="col-lg-5 mb-3">
                                <div class="card card-100">
                                    <div class="card-body">
                                        <h4 class="header-title mt-0 mb-3">INFORMAÇÕES PESSOAIS</h4>
                                        <hr/>
                                        <div class="text-left" id="info-pessoal"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7 mb-3" id="info-plano"></div>
                        </div>
                        <div id="carteirinha" class="row"></div>
                    </div> 
                </div> 
                <?php 
                    requireOnce(__DIR__.'/../partials/footer-dash.php'); 
                    requireOnce(__DIR__.'/../include/modal/modal-plano.php');
                ?>
            </div>
            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->
        </div>
        <!-- END wrapper -->
        <?php requireOnce(__DIR__.'/../partials/right-sidebar.php'); ?>
        <!-- App js -->
        <?php  requireOnce(__DIR__.'/../include/scripts.php') ;?>   
        <script type='module' src="<?=SITE;?>assets/js/partials/menu.js"></script>
        <script type='module' src="<?=SITE;?>assets/js/pages/dashboard.js"></script>
    </body>
</html>
