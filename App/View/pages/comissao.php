<?php
$style = '<link href="'.SITE.'assets/css/comissao.css" rel="stylesheet" type="text/css" />';
requireOnce(__DIR__.'/../partials/header.php', compact('style'));
?>
            <!-- Begin page -->
            <div class="wrapper">

                <?php requireOnce(__DIR__.'/../partials/left-sidebar.php', compact('menu', 'image')); ?>

                <!-- ============================================================== -->
                <!-- Start Page Content here -->
                <!-- ============================================================== -->

                <div class="content-page">
                    <div class="content top-menu">

                        <?php requireOnce(__DIR__.'/../partials/topbar.php'); ?>

                        <div class="container-fluid">                                                
                            <div class="row justify-content-between">
                                <div class="col-12 col-sm-4 col-lg-7 mb-2">
                                    <div class="page-title-box">                                   
                                        <h4 class="page-title">Comissão</h4>
                                    </div>
                                </div>
                                <?php if(isset($_SESSION[CHAVE_CONTRATO]['indicador']) && $_SESSION[CHAVE_CONTRATO]['login_ativo'] === 'funcionario') { ?>
                                    <div class="col-12 col-sm-8 col-lg-5 mb-2" style="max-width: 530px;">
                                        <div class="form-group">
                                            <label for="link">Link do Consultor</label>
                                            <div class="input-group mb-3">
                                                <input type="text" id="txtLink" class="form-control" placeholder="Link do Consultor"
                                                value="<?= BASE_URL.'loja/'.CONTRATO.'/consultor/'.$_SESSION[CHAVE_CONTRATO]['indicador']['id_fornecedores_despesas']?>">
                                                <div class="input-group-append">
                                                    <button class="btn btn-secundaria" type="button" id="btnLink">
                                                        <i class="mdi mdi-content-copy"></i> Copiar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="row">
                                <div class="col-xl-10 mb-3">
                                    <label>Período:</label>
                                    <div class="row">
                                        <div class="col-12 col-lg-5">
                                            <div class="row">
                                                <div class="col-6 p-1">
                                                    <button class="btn btn-secundaria btn-block auto-date btn-radius" type="button" data-from="<?= sprintf("%s-01", date("Y-m")) ?>" data-to="<?= date("Y-m-d"); ?>">Este mês</button>
                                                </div>
                                                <div class="col-6 p-1">
                                                    <button class="btn btn-secundaria btn-block auto-date btn-radius" type="button" data-from="<?= date("Y-m-d"); ?>" data-to="<?= date("Y-m-d"); ?>">Hoje</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-7">
                                            <div class="row">
                                                <div class="input-group input-radius col-12 col-sm-6 p-1">
                                                    <div class="input-group-prepend"><span class="input-group-text bg-secundaria text-white">De</span></div>
                                                    <input type="date" class="form-control"  value="<?= sprintf("%s-01", date("Y-m")) ?>" placeholder="dd/mm/aaaa" id="rel-date-from">
                                                </div>
                                                <div class="input-group input-radius col-12 col-sm-6 p-1">
                                                    <div class="input-group-prepend"><span class="input-group-text bg-secundaria text-white">Até</span></div>
                                                    <input type="date" class="form-control" value="<?= date("Y-m-d") ?>" placeholder="dd/mm/aaaa" id="rel-date-to">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-sm-4 mb-2 pt-xl-4 ml-sm-auto">
                                    <button type="button" class="btn btn-primaria btn-block btn-radius" id="btnfind">Filtrar</button>
                                </div>
                                <div class="col-xl-12">
                                    <ul class="nav nav-tabs bg-nav-pills nav-pills nav-justified" id="myTab" role="tablist">
                                        <li class="nav-item nav-fill">
                                            <a class="nav-link active" id="comissao-tab" data-toggle="tab" href="#comissao" role="tab" aria-controls="comissao" aria-selected="true">Comissão</a>
                                        </li>
                                        <li class="nav-item nav-fill">
                                            <a class="nav-link" id="plano-tab" data-toggle="tab" href="#plano" role="tab" aria-controls="plano" aria-selected="false">Plano</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane py-3 fade show active" id="comissao" role="tabpanel" aria-labelledby="comissao-tab">
                                            <div class="row">
                                                <div class="col-xl-7 overflow-hidden">
                                                    <div id="list-counter" class="container-counter"></div>
                                                </div>
                                                <div class="col-xl-5">
                                                    <div class="card w-100" id="card-vendas-resumo">
                                                        <div class="card-body">
                                                            <h4 class="header-title mb-3">Comissão / Recebida</h4>
                                                            <hr/>
                                                            <div class="row splits">
                                                                <div class="col">
                                                                    <div class="split sivis text-left">
                                                                        <div class="empresa">A receber</div>
                                                                        <div class="valor" id="receber"></div>
                                                                        <svg width="16" height="10">
                                                                            <polygon points="0,0 16,0 8,10" class="triangulo" />
                                                                            <line x1="0" y1="0" x2="8" y2="10" class="linha" />
                                                                            <line x1="16" y1="0" x2="8" y2="10" class="linha" />
                                                                        </svg>
                                                                    </div>
                                                                </div>
                                                                <div class="col">
                                                                    <div class="split cliente text-right">
                                                                        <div class="empresa">Recebidos</div>
                                                                        <div class="valor" id="recebido"></div>
                                                                        <svg width="16" height="10">
                                                                            <polygon points="0,0 16,0 8,10" class="triangulo" />
                                                                            <line x1="0" y1="0" x2="8" y2="10" class="linha" />
                                                                            <line x1="16" y1="0" x2="8" y2="10" class="linha" />
                                                                        </svg>
                                                                    </div>
                                                                </div>
                                                               
                                                            </div>
                                                            <div class="progress-bar-wrapper">
                                                                <div class="percentual percentual-2" id="per-2"></div>
                                                                <div class="progress-bar">
                                                                    <div class="cliente" id="progressBar"></div>
                                                                </div>
                                                                <div class="percentual percentual-1" id="per-1"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-12 mb-3">
                                                    <div class="card">
                                                        <div class="card-body card-dashboard">
                                                            <h4 class="header-title mb-3">Vendas</h4>
                                                            <hr/>
                                                            <table id="tbFuncionarios" class="table table-striped dt-responsive">
                                                                <thead class="bg-secundaria text-white">
                                                                    <tr>
                                                                        <th></th>
                                                                        <th>Parcela</th>
                                                                        <th>Nome</th>
                                                                        <th>Dt. Venda</th>
                                                                        <th>Plano</th>
                                                                        <th>Val.Plano.</th>
                                                                        <th>Val.Serv.</th>
                                                                        <th>Val.Prod.</th>
                                                                        <th>C.Plano 1ª</th>
                                                                        <th>C.Plano</th>
                                                                        <th>C.Serv.</th>
                                                                        <th>C.Prod.</th>
                                                                        <th>C.Total</th>
                                                                        <th>Status</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>                
                                            </div>  
                                        </div>
                                        <div class="tab-pane py-3 fade" id="plano" role="tabpanel" aria-labelledby="plano-tab">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="card">
                                                        <div class="card-body card-vendas">
                                                            <h4 class="header-title mb-3">Comissão</h4>
                                                            <div class="row">
                                                                <div class="col-12 order-2 order-xl-1">
                                                                    <table id="tblComissao" class="table table-responsive2 dt-responsive">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Descricao</th>
                                                                                <th>Quantidade</th>
                                                                                <th>1ª Parc.</th>
                                                                                <th>Plano</th>
                                                                                <th>Produto</th>
                                                                                <th>Serviço</th>
                                                                                <th>Subtotal</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody></tbody>
                                                                        <tfoot></tfoot>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                  
                        </div>
                    </div>
                </div>
            </div>
        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->
        </div>
        <!-- END wrapper -->
        <?php requireOnce(__DIR__.'/../partials/footer-dash.php'); ?>
    </div>
</div>
<!-- END wrapper -->
<?php requireOnce(__DIR__.'/../include/scripts.php');?>
<!-- App js -->
<script type='module' src="<?=SITE?>assets/js/partials/menu.js"></script>
<script type='module' src="<?=SITE?>assets/js/pages/comissao.js"></script>

</body>
</html>
