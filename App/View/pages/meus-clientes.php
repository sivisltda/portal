<?php 
    $style = '<link href="'.SITE.'assets/css/meus-clientes.css" rel="stylesheet" type="text/css" />';
    requireOnce(__DIR__.'/../partials/header.php', compact('style')); 
?>

        <!-- Begin page -->
        <div class="wrapper">

            <?php requireOnce(__DIR__.'/../partials/left-sidebar.php', compact('menu', 'image')); ?>

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content top-menu">

                    <?php requireOnce(__DIR__.'/../partials/topbar.php'); ?>

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Meus Prospects</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12 mb-3">
                                <label>Período:</label>
                                <div class="row">
                                    <div class="col-12 col-lg-5">
                                        <div class="row">
                                            <div class="col-4 p-1">
                                                <button class="btn btn-secundaria btn-block auto-date btn-radius" type="button" data-from="" data-to="">Todo</button>
                                            </div>
                                            <div class="col-4 p-1">
                                                <button class="btn btn-secundaria btn-block auto-date btn-radius" type="button" data-from="<?= sprintf("%s-01", date("Y-m")) ?>" data-to="<?= date("Y-m-d"); ?>">Este mês</button>
                                            </div>
                                            <div class="col-4 p-1">
                                                <button class="btn btn-secundaria btn-block auto-date btn-radius" type="button" data-from="<?= date("Y-m-d"); ?>" data-to="<?= date("Y-m-d"); ?>">Hoje</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-7">
                                        <div class="row">
                                            <div class="input-group input-radius col-12 col-sm-6 p-1">
                                                <div class="input-group-prepend"><span class="input-group-text bg-secundaria text-white">De</span></div>
                                                <input type="date" class="form-control"  value="<?= date('Y-m-d', strtotime('-1 month + 1 day')) ?>" placeholder="dd/mm/aaaa" id="rel-date-from">
                                            </div>
                                            <div class="input-group input-radius col-12 col-sm-6 p-1">
                                                <div class="input-group-prepend"><span class="input-group-text bg-secundaria text-white">Até</span></div>
                                                <input type="date" class="form-control" value="<?= date("Y-m-d") ?>" placeholder="dd/mm/aaaa" id="rel-date-to">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-5">
                                <div class="group-form">
                                    <label for="procedencia">Procedência:</label>
                                    <select name="procedencia" id="procedencia" class="form-control">
                                        <option value="">Selecione uma Procedencia</option>
                                        <?php 
                                            foreach($procedencias as $procedencia) {
                                                echo '<option value="'.$procedencia['id_procedencia'].'">'
                                                .$procedencia['nome_procedencia'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <label for="busca">Pesquisa:</label>
                                    <input type="text" name="busca" placeholder="Digite o nome ou e-mail" id="busca" class="form-control">
                                </div>
                            </div>
                            <div class="col-12 col-lg-3 mt-4">
                                <button id="btnBusca" class="btn btn-primaria btn-block btn-radius">Buscar</button>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12 overflow-hidden">
                                <div id="list-counter" class="container-counter"></div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-xl-12 mb-3">
                                <div class="card">
                                    <div class="card-body card-dashboard">
                                        <h4 class="header-title mb-3">Prospects / Leads</h4>
                                        <hr/>
                                        <table id="tblProspect" class="table table-striped dt-responsive">
                                            <thead class="bg-secundaria text-white">
                                                <tr>
                                                    <th></th>
                                                    <th>Dt. Cadastro</th>
                                                    <th>Nome</th>
                                                    <th>Tipo</th>
                                                    <th>Telefone</th>
                                                    <th>Email</th>
                                                    <th>Procedência</th>
                                                    <th>Indicador</th>
                                                    <th>Status</th>
                                                    <th>Ação</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>                
                <?php requireOnce(__DIR__.'/../partials/footer-dash.php'); ?>
            </div>
        </div>
        <div id="modal-aux" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>
        <!-- END wrapper -->
        <?php requireOnce(__DIR__.'/../include/scripts.php');?>
        <!-- App js -->
        <script type='module' src="<?=SITE?>assets/js/partials/menu.js"></script>
        <script type='module' src="<?=SITE?>assets/js/pages/meus-clientes.js"></script>
    </body>
</html>
