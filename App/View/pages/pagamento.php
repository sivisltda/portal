<?php
    $style='
        <link rel="stylesheet" href="'.SITE.'assets/css/pagamento.css">
        <link rel="stylesheet" href="'.SITE.'Plugins/slick/slick.css">
        <link rel="stylesheet" href="'.SITE.'Plugins/slick/slick-theme.css">
        ';
    requireOnce(__DIR__.'/../partials/header.php', compact('style')); 
?>

<!-- Begin page -->
<div class="wrapper">

    <?php requireOnce(__DIR__.'/../partials/left-sidebar.php', compact('menu', 'image')); ?>

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content top-menu">

            <?php requireOnce(__DIR__.'/../partials/topbar.php'); ?>

            <div class="container-fluid">
                <div class="row justify-content-between pb-4 pt-2">
                    <div class="col-12">
                        <div class="page-title-box d-flex justify-content-between align-items-center">
                            <h4 class="page-title" style="line-height: normal">Pagamentos</h4>
                            <button class="btn btn-success" data-toggle="modal" data-backdrop="static" data-target="#myModalProcessar"><i class="mdi mdi-cash-multiple"></i> Novo</button>
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="row">
                                    <div class="col-4 p-1">
                                        <button class="btn btn-light btn-block auto-date btn-radius" type="button" data-from="" data-to="">Todo</button>
                                    </div>
                                    <div class="col-4 p-1">
                                        <button class="btn btn-light btn-block auto-date btn-radius" type="button" data-from="<?php echo sprintf("01/%s", date("m/Y")); ?>" data-to="<?php echo date("d/m/Y"); ?>">Este mês</button>
                                    </div>
                                    <div class="col-4 p-1">
                                        <button class="btn btn-light btn-block auto-date btn-radius" type="button" data-from="<?php echo date("d/m/Y"); ?>" data-to="<?php echo date("d/m/Y"); ?>">Hoje</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="row">
                                    <div class="input-group input-radius col-12 col-sm-6 p-1">
                                        <div class="input-group-prepend"><span class="input-group-text">De</span></div>
                                        <input type="text" class="form-control" value=""  placeholder="dd/mm/aaaa"
                                               id="rel-date-from" data-provide="datepicker" data-date-format="d/m/yyyy">
                                    </div>
                                    <div class="input-group input-radius col-12 col-sm-6 p-1">
                                        <div class="input-group-prepend"><span class="input-group-text">Até</span></div>
                                        <input type="text" class="form-control" data-mask="00/00/0000" value="" placeholder="dd/mm/aaaa" id="rel-date-to">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mt-1">
                        <div class="form-group">
                            <select id="status" name="status" class="form-control select2 select2-multiple" multiple="multiple"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 mt-1">
                        <div class="form-group">
                            <select id="tipo_pagamento" name="tipo_pagamento" class="form-control select2 select2-multiple" multiple="multiple"></select>
                        </div>
                    </div>
                    <div class="col-lg-5 mt-1">
                        <div class="form-group">
                            <input id="cliente" name="cliente" placeholder="Digite o nome do cliente" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-3 mt-1">
                        <button type="button" class="btn btn-primaria btn-block btn-radius" id="btnfind">Filtrar</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 px-0">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title mb-3">Histórico</h4>
                                <hr/>
                                <table id="tbPagamentos" class="table table-striped dt-responsive dataTable no-footer dtr-inline collapsed">
                                    <thead class="bg-amd text-white">
                                        <tr>
                                            <th>Data</th>
                                            <th>Nome</th>
                                            <th>Valor</th>
                                            <th>Tipo Pag.</th>
                                            <th>N° do Cartão</th>
                                            <th>Status</th>
                                            <th>Mensagem de Erro</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php requireOnce(__DIR__.'/../partials/footer-dash.php'); ?>
<div tabIndex="-1" role="dialog" aria-labelledby="mymodalLabel" id="myModalProcessar" class="modal fade">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Processar Pagamento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body mb-0">
                <form id="formPgCard" class="needs-validation" novalidate>
                    <div id="formaPagamento" class="card-body py-0 pt-lg-2">
                        <div class="form-group row mb-md-3">
                            <label class="col-md-3 col-form-label" for="nomeCliente">Cliente:</label>
                            <div class="col-md-9 box-autocomplete">
                                <input type="text" class="form-control" name="cliente" id="nomeCliente" placeholder="Nome do Cliente" required>
                                <input type="hidden" name="idCliente" id="idCliente">
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="form-group row mb-md-3">
                            <label class="col-md-3 col-form-label" for="txtmodPag">Forma de Pagamento:</label>
                            <div class="col-md-9">
                                <select class="form-control" name="txtmodPag" id="txtmodPag" required></select>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div id="informacoesPagamento">
                            <div class="form-group row mb-lg-3">
                                <label class="col-lg-3 col-form-label" for="numcredcard">Valor / Parcelas:</label>
                                <div class="col-md-6 col-lg-4 mb-2 mb-lg-0 input-group" style="position:relative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">R$</span>
                                    </div>
                                    <input type="text" class="form-control" id="txtValor" name="txtValor" value="" readonly placeholder="Valor" required>
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="col-md-6 col-lg-5 mb-2 mb-lg-0">
                                    <select class="form-control" id="txtParcelas" name="txtParcelas" required></select>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="form-group row mb-lg-3">
                                <label class="col-lg-3 col-form-label" for="numcredcard">Número do Cartão:</label>
                                <div class="col-md-12 col-lg-9 mb-2 mb-lg-0" style="position:relative">
                                    <input type="text" class="form-control" id="numcredcard" name="numcredcard" value="" placeholder="Número do cartão" required>
                                    <div class="invalid-feedback"></div>
                                    <span id="credcard-thumb" class="credcard-thumb"></span>
                                </div>
                            </div>
                            <div class="form-group row mb-lg-3">
                                <label class="col-lg-3 col-form-label" for="validcard"> Validade / CVC:</label>
                                <div class="col-md-6 col-lg-4 mb-2 mb-lg-0">
                                    <input type="text" id="validcard" name="validcard" class="form-control" data-toggle="input-mask" data-mask-format="00/0000" placeholder="Validade  ex: MM/AAAA" required maxlength="5" autocomplete="off">
                                    <div class="invalid-feedback"></div>
                                </div>
                                <div class="col-md-6 col-lg-5 mb-2 mb-lg-0" style="position:relative">
                                    <input type="text" class="form-control" data-toggle="input-mask" data-mask-format="0009" id="codCC" name="codCC" value="" maxlength="4" placeholder="CVC" required>
                                    <div class="invalid-feedback"></div>
                                    <span id="cvc-thumb" class=""></span>
                                </div>
                            </div>
                            <div class="form-group row mb-lg-3">
                                <label class="col-lg-3 col-md-12 col-form-label" for="nomcredcard"> Nome no Cartão:</label>
                                <div class="col-lg-9 col-md-12">
                                    <input type="text" class="form-control" id="nomcredcard" name="nomcredcard" value="" placeholder="Nome no cartão" required>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnProcessar">Processar</button>
                <button type="button" class="btn btn-light" id="btnCancelarProcessar" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
    <!-- App js -->
    <?php requireOnce(__DIR__.'/../include/scripts.php');?>
    <script src="<?=SITE?>Plugins/slick/slick.min.js"></script>
    <script src="<?=SITE?>Plugins/credCard/jquery.creditCardValidator.js"></script>
    <script src="<?=SITE?>Plugins/typehead/bloodhound.min.js"></script>
    <script src="<?=SITE?>Plugins/typehead/typeahead.bundle.min.js"></script>
    <script type='module' src="<?=SITE;?>assets/js/partials/menu.js"></script> 
    <script type="module" src="<?=SITE?>assets/js/pages/pagamento.js"></script>
    <!-- App js -->

</body>
</html>