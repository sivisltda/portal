let state = stateInit();

function stateInit() {
    return {
        usuario: null,
        indicadores: [],
        estados: [],
        cidades: [],
        endereco: {},
        coresTema: {},
        dependentes: [],
        parentesco: {},
        procedencia: [],
        produtos: [],
        produto: null,
        servicos: [],
        servicosPlano: [],
        coberturas: [],
        cores: {},
        cambios: {},
        consultor: null,
        cupom: null,
        personalizado: null,
        veiculos: [],
        veiculo: null,
        lead: null,
        marcas: [],
        modelos: [],
        anos: [],
        tipoVeiculo: [],
        selTipoVeiculo: 0,
        selConsultaFipe: 0,
        acessorios: [],
        planos: [],
        plano: null,
        mensalidade: null,
        mensalidades: [],
        termos: [], 
        prorata: null,
        vencimentos: [],
        tipoVencimento: null, 
        diaVencimento: null,
        parcela: null,
        idParcela: null, 
        transacao: null,
        etapaMaxima: 1,
        rangeComissao: null
    };
}

export default {
    setters: {
        setUsuario(usuario) {
            state.usuario = usuario;
        },
        setIndicadores(indicadores) {
            state.indicadores = indicadores;
        },
        setEstados(estados) {
            state.estados = estados;
        },
        setCidades(cidades) {
            state.cidades = cidades;
        },
        setEndereco(endereco) {
            state.endereco = endereco;
        },
        setCoresTemas(coresTema) {
            state.coresTema = coresTema;
        },
        setDependentes(dependentes) {
            state.dependentes = dependentes;
        },
        setParentesco(parentesco) {
            state.parentesco = parentesco;
        },
        setProcedencia(procedencia) {
            state.procedencia = procedencia;
        },
        setProdutos(produtos) {
            state.produtos = produtos;
        },
        setProduto(produto) {
            state.produto = produto;
        },
        setServicos(servicos) {
            state.servicos = servicos;
        },
        setServicosPlano(servicosPlano) {
            state.servicosPlano = servicosPlano;
        },
        setCoberturas(coberturas) {
            state.coberturas = coberturas;
        },
        setCores(cores) {
            state.cores = cores;
        },
        setCambios(cambios) {
            state.cambios = cambios;
        },        
        setConsultor(consultor) {
            state.consultor = consultor;
        },
        setCupom(cupom) {
            state.cupom = cupom;
        },
        setPersonalizado(personalizado) {
            state.personalizado = personalizado;
        },
        setVeiculos(veiculos) {
            state.veiculos = veiculos;
        },
        setVeiculo(veiculo) {
            state.veiculo = veiculo;
        },
        setLead(lead) {
            state.lead = lead;
        },
        setMarcas(marcas) {
            state.marcas = marcas;
        },
        setModelos(modelos) {
            state.modelos = modelos;
        },
        setAnos(anos)  {
            state.anos = anos;
        },
        setTipoVeiculo(tipoVeiculo) {
            state.tipoVeiculo = tipoVeiculo;
        },
        setSelTipoVeiculo(selTipoVeiculo) {
            state.selTipoVeiculo = selTipoVeiculo;
        },
        setSelConsultaFipe(selConsultaFipe) {
            state.selConsultaFipe = selConsultaFipe;
        },
        setAcessorios(acessorios) {
            state.acessorios = acessorios;
        },
        setPlanos(planos) {
            state.planos = planos;
        },
        setPlano(plano) {
            state.plano = plano;
        },
        setMensalidade(mensalidade) {
            state.mensalidade = mensalidade;
        },
        setMensalidades(mensalidades) {
            state.mensalidades = mensalidades;
        },
        setTermos(termos) {
            state.termos = termos;
        },
        setProrata(prorata) {
            state.prorata = prorata;
        },
        setVencimentos(vencimentos) {
            state.vencimentos = vencimentos;
        },
        setTipoVencimento(tipoVencimento) {
            state.tipoVencimento = tipoVencimento;
        },
        setDiaVencimento(diaVencimento) {
            state.diaVencimento = diaVencimento;
        },
        setParcela(parcela) {
            state.parcela = parcela;
        },
        setIdParcela(idParcela) {
            state.idParcela = idParcela;
        },
        setTransacao(transacao) {
            state.transacao = transacao;
        },
        setEtapaMaxima(etapaMaxima) {
            state.etapaMaxima = etapaMaxima;
        },
        setRangeComissao(rangeComissao) {
            state.rangeComissao = rangeComissao;
        }
    },
    getters: {
        getUsuario() {
            return state.usuario;
        },
        getIndicadores() {
            return state.indicadores;
        },
        getEstados() {
            return state.estados;
        },
        getCidades() {
            return state.cidades;
        },
        getEndereco() {
            return state.endereco;
        },
        getCoresTema() {
            return state.coresTema;
        },
        getDependentes() {
            return state.dependentes;
        },
        getParentesco() {
            return state.parentesco;
        },
        getProcedencia() {
            return state.procedencia;
        },
        getProdutos() {
            return state.produtos;
        },
        getProduto() {
            return state.produto;
        },
        getServicos() {
            return state.servicos;
        },
        getServicosPlano() {
            return state.servicosPlano;
        },
        getCoberturas() {
            return state.coberturas;
        },
        getCores() {
            return state.cores;
        },
        getCambios() {
            return state.cambios;
        },
        getConsultor() {
            return state.consultor;
        },
        getCupom() {
            return state.cupom;
        },
        getPersonalizado() {
            return state.personalizado;
        },
        getVeiculos() {
            return state.veiculos;
        },
        getVeiculo() {
            return state.veiculo;
        },
        getLead() {
            return state.lead;
        },
        getMarcas() {
            return state.marcas;
        },
        getModelos() {
            return state.modelos;
        },
        getAnos()  {
            return state.anos;
        },
        getTipoVeiculo() {
            return state.tipoVeiculo;
        },
        getSelTipoVeiculo() {
            return state.selTipoVeiculo;
        },
        getSelConsultaFipe() {
            return state.selConsultaFipe;
        },
        getAcessorios() {
            return state.acessorios || [];
        },
        getPlanos() {
            return state.planos;
        },
        getPlano() {
            return state.plano;
        },
        getMensalidade() {
            return state.mensalidade;
        },
        getMensalidades() {
            return state.mensalidades;
        },
        getTermos() {
            return state.termos;
        },
        getProrata() {
            return state.prorata;
        },
        getVencimentos() {
            return state.vencimentos;
        },
        getTipoVencimento() {
            return state.tipoVencimento;
        },
        getDiaVencimento() {
            return state.diaVencimento;
        },
        getParcela() {
            return state.parcela;
        },
        getIdParcela() {
            return state.idParcela;
        },
        getTransacao() {
            return state.transacao;
        },
        getEtapaMaxima() {
            return state.etapaMaxima;
        },
        getRangeComissao() {
            return state.rangeComissao;
        }
    },
    actions: {
        serialize() {
            return JSON.stringify(state);
        },
        unserialize(item) {
            state = JSON.parse(item);
        },
        clear() {
            state = stateInit();
        }
    }
};