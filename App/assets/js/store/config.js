let state = {
    etapaAtiva: null,
    modo: null,
    consultor: null,
    config: null
};

export default {
    setters: {
        setEtapaAtiva(etapaAtiva) {
            state.etapaAtiva = etapaAtiva;
        },
        setModo(modo) {
            state.modo = modo;
        },
        setConsultor(consultor) {
            state.consultor = consultor;
        },
        setConfig(config) {
            state.config = config;
        }
    },
    getters: {
        getEtapaAtiva() {
            return state.etapaAtiva;
        },
        getModo() {
            return state.modo;
        },
        getConsultor() {
            return state.consultor;
        },
        getConfig() {
            return state.config;
        }
    },
    actions: {
        serialize() {
            return JSON.stringify(state);
        },
        unserialize(item) {
            state = JSON.parse(item);
        }
    }
};