let state = {
    plano: null,
    planos: [],
    agenda: null,
    agendas: [],
    agendamento: null,
    agendamentos: [], 
    mensalidade: null,
    mensalidades: [],
    veiculo: null,
    veiculos: [],
};

export default {
    setters: {
        setPlano(plano) {
            state.plano = plano;
        },
        setPlanos(planos) {
            state.planos = planos;
        },
        setAgenda(agenda) {
            state.agenda = agenda;
        },
        setAgendas(agendas) {
            state.agendas = agendas;
        },
        setAgendamento(agendamento) {
            state.agendamento = agendamento;
        },
        setAgendamentos(agendamentos) {
            state.agendamentos = agendamentos;
        },
        setMensalidade(mensalidade) {
            state.mensalidade = mensalidade;
        },
        setMensalidades(mensalidades) {
            state.mensalidades = mensalidades;
        },
        setVeiculo(veiculo) {
            state.veiculo = veiculo;
        },
        setVeiculos(veiculos) {
            state.veiculos = veiculos;
        },
    },
    getters: {
        getPlano() {
            return state.plano;
        },
        getPlanos() {
            return state.planos;
        },
        getAgenda() {
            return state.agenda;
        },
        getAgendas() {
            return state.agendas;
        },
        getAgendamento() {
            return state.agendamento;
        },
        getAgendamentos() {
            return state.agendamentos;
        },
        getMensalidade() {
            return state.mensalidade;
        },
        getMensalidades() {
            return state.mensalidades;
        },
        getVeiculo() {
            return state.veiculo;
        },
        getVeiculos() {
            return state.veiculos;
        },
    }
};