 let config =  {
    site: $("#site").val(),
    api: $("#api").val(),
    contrato: $("#contrato").val(),
    urlErp: $("#erp").val(),
    cor_primaria: $('#cor_primaria').val(),
    cor_secundaria: $('#cor_secundaria').val(),
    cor_destaque: $('#cor_destaque').val()
};

export function getConfig() {
    return config;
}

export function setConfig(item) {
    config = Object.assign(config, item);
}