import * as portalService from './../service/portal.service.js';
import store from './../store/app.js';
import configStore from './../store/config.js';
import * as storageService from './../service/storage.service.js';
import {isExistsTag} from './../core/function.js';
import {setApp, clearRoute, clickAnterior, clickProximo} from './../core/route.js';
import {chamaSwalQueue, requisitaCPF} from './../core/auth.js';

const {setProcedencia, setTipoVencimento, setVencimentos, setDiaVencimento, setConsultor, setIndicadores, setCoresTemas,
    setTipoVeiculo, setUsuario, setVeiculos, setPlanos} = store.setters;

let module = null;

import {getConfig, setConfig} from '../config/config.js';

export default function carregaPortal(containerApp, errCallback) {
    const contrato = getContrato();
    setConfig({
        contrato
    });
    clearRoute();
    setApp(containerApp);
    getInfoPortal(containerApp, errCallback);
}

function getInfoPortal(containerApp, errCallback) {
    portalService.getInfoPortal(async function (res) {
        try {
            setConfig({
                nome: res.nome,
                modelo: res.modelo,
                site_nome: res.site_nome,
                contrato: res.contrato,
                telefone: res.telefone,
                cnpj: res.cnpj,
                unico: res.unico,
                image: res.image,
                rapido: res.rapido,
                ex_pag_pess_jur: res.ex_pag_pess_jur,
                ativa_dependente: res.ativa_dependente,
                adesao_dependente: res.adesao_dependente,
                terminal_venda: res.hasOwnProperty('terminal_venda') ? res.terminal_venda : 0,
                ano_limite: res.ano_limite,
                franquia_padrao: res.franquia_padrao,
                mdl_what: res.mdl_what,
                cotacao_interna: res.hasOwnProperty('cotacao_interna'),
                agrupar_plano: res.agrupar_plano,
                plano_mens_contract: res.plano_mens_contract,
                adm_msg_boas_vindas: res.adm_msg_boas_vindas,
                aca_pes_cot_tip: res.aca_pes_cot_tip,
                adm_msg_cotacao: res.adm_msg_cotacao,
                adm_msg_contratacao: res.adm_msg_contratacao,
                logo_conclusao: res.logo_conclusao,
                adm_associado: res.adm_associado,
                adm_msg_taxa_adesao: res.adm_msg_taxa_adesao,
                adm_msg_termos_email: res.adm_msg_termos_email,
                adm_msg_termos_whats: res.adm_msg_termos_whats,
                adm_msg_link_pag: res.adm_msg_link_pag,
                exc_ven_prorata: res.exc_ven_prorata,
                nao_pag_online: res.nao_pag_online,
                adm_nao_valor_print: res.adm_nao_valor_print,
                vencimentos: res.vencimentos,
                adm_desconto_range: res.adm_desconto_range,
                seg_vei_cotac_person: res.seg_vei_cotac_person
            });
            setCoresTemas({
                cor_primaria: res.cor_primaria,
                cor_secundaria: res.cor_secundaria,
                cor_destaque: res.cor_destaque
            });
            const config = getConfig();
            const app = storageService.getItem(`loja-${config.contrato}`);
            const dados = app ? JSON.parse(app) : false;
            const prospect = isExistsTag('prospect');
            if (!config.rapido && (!(prospect && Number.isInteger(parseInt(prospect))) && dados && dados.usuario)) {
                swal({
                    title: 'Informações encontradas',
                    type: 'warning',
                    html: `Atenção! Encontramos alguns dados de uma cotação anterior referente ao usuário ${dados.usuario.razao_social}</br>Deseja carregar essas informações?`,
                    showCancelButton: true,
                    confirmButtonColor: res.cor_secundaria,
                    cancelButtonColor: '#9E9E9E',
                    confirmButtonText: 'Sim', cancelButtonText: 'Não'
                }).then(function (reswal) {
                    if (reswal.value) {
                        portalService.verificaSessaoUsuario(dados.usuario.id, function () {
                            chamaRequisitaCPF(containerApp, dados, res, app);
                        }, function (erro) {
                            if (erro.status === 401) {
                                chamaRequisitaCPF(containerApp, dados, res, app);
                            } else {
                                setaDados(containerApp, res);
                            }
                        });
                    } else {
                        portalService.logout(function () {
                            setaDados(containerApp, res);
                        }, function (erro) {
                            console.log(erro);
                            swal('Atenção', 'Ocorreu um erro ao deslogar', 'error');
                            setaDados(containerApp, res);
                        });
                    }
                });
            } else {
                if (config.rapido) {
                    clear(config);
                }
                await setaDados(containerApp, res);
            }
        } catch (e) {
            console.log(e);
            errCallback(e);
            swal('Erro', 'Não foi possível carregar o módulo', 'error');
        }
    }, function (error) {
        console.log(error);
        errCallback(error);
        swal('Erro', 'Não foi possível carregar as informações do portal', 'error');
    });
}

function clear(config) {
    store.actions.clear();
    storageService.removeItem(`loja-${config.contrato}`);
}

function chamaRequisitaCPF(containerApp, dados, res, app) {
    chamaSwalQueue(requisitaCPF(dados.usuario, function () {
        carregaDadosSucesso(containerApp, res, app);
    }, function () {
        setaDados(containerApp, res);
    }), function (resp) {
        if (resp.valid) {
            swal('Sucesso', 'Informações carregadas com sucesso', 'success');
        } else if (resp.dismiss) {
            setaDados(containerApp, res);
        }
    });
}

function carregaDadosSucesso(containerApp, res, app) {
    store.actions.unserialize(app);
    configStore.setters.setConfig(getConfig());
    containerApp.html(template(res));
    carregaModule(containerApp, res.modelo);
}

async function setaDados(containerApp, res) {
    configStore.setters.setConfig(getConfig());
    if (res.hasOwnProperty('usuario') && res.usuario) {
        setUsuario(res.usuario);
    }
    if (res.hasOwnProperty('consultor')) {
        setConsultor(res.consultor);
    }
    if (res.hasOwnProperty('procedencia')) {
        setProcedencia(res.procedencia);
    }
    if (res.hasOwnProperty('tipo_vencimento')) {
        setTipoVencimento(res.tipo_vencimento);
    }
    if (res.hasOwnProperty('vencimentos')) {
        setVencimentos(res.vencimentos);
    }
    if (res.hasOwnProperty('dia_vencimento')) {
        setDiaVencimento(res.dia_vencimento);
    }
    if (res.hasOwnProperty('indicadores')) {
        setIndicadores(res.indicadores);
    }
    if (res.hasOwnProperty('tipos_veiculos')) {
        setTipoVeiculo(res.tipos_veiculos);
    }
    if (!res.isLogged) {
        storageService.removeItem(`loja-${res.contrato}`);
    }
    if (res.hasOwnProperty('veiculos')) {
        setVeiculos(res.veiculos);
    }
    if (res.hasOwnProperty('planos')) {
        setPlanos(res.planos);
    }
    containerApp.html(template(res));
    await carregaModule(containerApp, res.modelo);
}

function getContrato() {
    const pathname = window.location.pathname;
    const partes = pathname.split('/').filter(Boolean);
    const index = partes.indexOf('loja');
    return index > -1 ? partes[index + 1] : false;
}

function template(config) {
    return `${templateHeader(config)}${templateMain()}`;
}


function templateHeader(config) {
    const consultor = config.hasOwnProperty('consultor') ? config.consultor : null;
    const infoConsultor = function () {
        return config.rapido || consultor ? `
		<div class="info-consultor bg-primaria">${consultor && config.rapido
                ? 'COTAÇÃO RÁPIDA - ' + consultor.nome
                : (consultor ? 'CONSULTOR(A): ' + consultor.nome.toUpperCase() : 'COTAÇÃO RÁPIDA') }</div>`
                : '';
    }
    return `<header class="d-print-none">
	<div class="line-header bg-primaria"></div>
	<div class="container container-header">
		${infoConsultor()}
		<div class="content-header">
			<div class="img-logo">
				<img alt="Logo" src="${config.image}" class="img-fluid">
			</div>
			<div class="horizontal-steps">
				<ul id="list-item" class="horizontal-steps-content"></ul>
				<div class="process-line bg-primaria" style="width: 0%;"></div>
			</div>
		</div>
	</div>
    </header>`;
}

function templateMain() {
    return `<section id="main" class="tab-content"></section>
	<section id="navegacao-botao" class="d-print-none">
		<hr/>
		<div class="container-fluid d-flex justify-content-center py-2">
			<button id="btnAnt" class="btn btn-link btn-prev texto-secundaria mr-5">Voltar</button>
			<button id="btnProx" class="btn btn-next active">Continuar</button>
		</div>
	</section>`;
}

async function carregaModule(app, modelo) {
    app.find("#btnAnt").click(clickAnterior);
    app.find("#btnProx").click(clickProximo);
    if (!module) {
        module = await import(`./${modelo}/index.js`);
    }
    module.load(app);
}