import { adicionarEtapaEntrePeriodo, eventEtapas, verificaEtapa,
    eventProximo, getNumeroEtapa, listenEvent, incrementarEtapa, verificaEAdicionaTemplateTab, getApp }
    from './../../../core/route.js';

let numeroEtapa = null;

let app         = null;
let config      = null;
let tabPanel    = null;
let btnProx     = null;
let btnAnt      = null;


export const configEtapa = {
    id: '#nome_etapa',
    title: 'Etapa',
    icon: 'mdi-account-circle',
    tooltip: 'Etapa'
};

/**
 * Função init - Função que inicia o processo dessa etapa
 * @param {null|number} posicao  
 * @returns this
 */
export function init(posicao) {
    app         = getApp();
    config      = configFunc();
    tabPanel    = app.find('#main');
    btnProx     = app.find('#btnProx');
    btnAnt      = app.find('#btnAnt');
    numeroEtapa = posicao ? posicao : incrementarEtapa();
    if (!posicao) {
        eventEtapas(numeroEtapa, configEtapa, iniciaTela);
    } else {
        if (!verificaEtapa(configEtapa.id)) {
            adicionarEtapaEntrePeriodo(numeroEtapa, configEtapa, iniciaTela);
        }
    } 
    verificaEAdicionaTemplateTab(tabPanel, configEtapa.id, template(configEtapa.id.replace('#','')));
    return this;     
}

/**
 * Função que é chamada todas vez que essa etapa é chamada
 */
export function iniciaTela() {
    numeroEtapa = getNumeroEtapa(configEtapa.id) || numeroEtapa;
    const tab = tabPanel.find(configEtapa.id);
    console.log(tab);
    btnAnt.hide();
    btnProx.hide(); 
}

/**
 * Função que retorna o template da etapa
 * @param {string} id 
 * @returns {string}
 */
function template(id) {
    return `<div class="tab-pane py-3 bg-white" id="${id}">
        Template
    </div>`;
}

/**
 * Função que chama o evento de todas vez que a etapa é avançada
 */
export function eventNext() {
    eventProximo(numeroEtapa, function() {
        listenEvent(numeroEtapa+1);
    });
}