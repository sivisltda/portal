import { getConfig as configFunc } from "./../../../config/config.js";
import store from '../../../store/app.js';

const config = configFunc();
const { getProcedencia } = store.getters;

export function getForm(formRoot, events) {
    const procedencias = getProcedencia() ? getProcedencia().reduce(function(acc, item) {
        acc[item.id_procedencia] = item.nome_procedencia;
        return acc;
    }, {}): {};
    return {
        type: 'column',
        root: formRoot,
        group: [
            {
                title: 'Informações Pessoais',
                inputs: [
                    {
                        type: 'text',  size: 'col-md-6', id: 'razao_social_c', label: 'Nome completo *:',
                        name: 'razao_social', required: true,
                        attributes : {
                            class: 'form-control',
                            placeholder: 'Seu nome'
                        }
                    }, {
                        type: 'text',  size: 'col-md-3', id: 'cnpj_c', label: (config.aca_pes_cot_tip === "0" ? 'CPF' : (config.aca_pes_cot_tip === "1" ? 'CNPJ' : 'CPF/CNPJ')) + ' *:',
                        name: 'cnpj', required: true, visibility: false,
                        attributes : {
                            class: 'form-control',
                            placeholder: (config.aca_pes_cot_tip === "0" ? 'xxx.xxx.xxx-xx' : (config.aca_pes_cot_tip === "1" ? 'xx.xxx.xxx/xxxx-xx' : 'xxx.xxx.xxx-xx ou xx.xxx.xxx/xxxx-xx')),
                            "data-toggle": "input-mask",
                            "data-type": (config.aca_pes_cot_tip === "0" ? 'cpf' : (config.aca_pes_cot_tip === "1" ? 'cnpj' : 'cpf-cnpj'))
                        }
                    }, {
                        type: 'text',  size: 'col-md-3', id: 'rg_c', label: 'RG *:',
                        name: 'inscricao_estadual', required: true,
                        attributes : {
                            class: 'form-control',
                            placeholder: 'Seu RG'
                        }
                    }, {
                        type: 'date',  size: 'col-md-4 col-lg-3', id: 'data_nascimento_c', label: 'Data de Nascimento *:',
                        name: 'data_nascimento', required: true,
                        attributes : {
                            class: 'form-control',
                            placeholder: 'DD/MM/AAAA'
                        }
                    }, {
                        type: 'select',  size: 'col-md-4 col-lg-2', id: 'sexo_c', label: 'Sexo *:',
                        name: 'sexo', required: true,
                        options: {
                            "": "Selecione um sexo",
                            "M": "Masculino",
                            "F": "Feminino"
                        },
                        attributes : {
                            class: 'form-control'
                        }
                    }, {
                        type: 'select',  size: 'col-md-4 col-lg-2', id: 'estado_civil_c', label: 'Estado Civil *:',
                        name: 'estado_civil', required: true,
                        options: {
                            "": "Selecione um Estado Civil",
                            "SOLTEIRO(A)": "SOLTEIRO(A)",
                            "CASADO(A)": "CASADO(A)",
                            "SEPARADO(A)": "SEPARADO(A)",
                            "DIVORCIADO(A)": "DIVORCIADO(A)",
                            "VIÚVO(A)": "VIÚVO(A)"
                        },
                        attributes : {
                            class: 'form-control'
                        }
                    }, {
                        type: "tel", size: "col-md-4 col-lg-2", id: "celular_c", label: "Celular *:",
                        name: "celular", required: true,
                        attributes: {
                            class: "form-control mask",
                            placeholder: "(00) 00000-0000",
                            "data-toggle": "input-mask",
                            "data-mask-format": "(00) 00000-0000"
                        }
                    }, {
                        type: "select", size: "col-md-4 col-lg-3", id: "procedencia_c", 
                        label: "Procedencia *:",
                        name: "procedencia", required: true,
                        options: procedencias,
                        attributes: {
                            class: "form-control select2",
                            placeholder: "Selecione uma procedência"
                        }
                    }
                ]
            },
            {
                title: 'Endereço',
                inputs: [
                    {
                        type: 'text',  size: 'col-md-3', id: 'cep_c', label: 'CEP *:',
                        name: 'cep', required: true,
                        attributes : {
                            class: 'form-control endereco',
                            placeholder: 'xxxxx-xxx'
                        }
                    }, {
                        type: 'select',  size: 'col-md-3', id: 'estado_c', label: 'Estado *:',
                        name: 'estado', required: true,
                        attributes : {
                            class: 'form-control estado',
                            placeholder: 'Estado'
                        }
                    }, {
                        type: 'select',  size: 'col-md-3', id: 'cidade_c', label: 'Cidade *:',
                        name: 'cidade', required: true,
                        attributes : {
                            class: 'form-control cidade',
                            placeholder: 'Cidade'
                        }
                    }, {
                        type: 'text',  size: 'col-md-3', id: 'bairro_c', label: 'Bairro *:',
                        name: 'bairro', required: true,
                        attributes : {
                            class: 'form-control bairro',
                            placeholder: 'Bairro'
                        }
                    }, {
                        type: 'text',  size: 'col-md-6', id: 'endereco_c', label: 'Endereço *:',
                        name: 'endereco', required: true,
                        attributes : {
                            class: 'form-control endereco',
                            placeholder: 'Endereço'
                        }
                    }, {
                        type: 'text',  size: 'col-md-3', id: 'numero_c', label: 'Número *:',
                        name: 'numero', required: true,
                        attributes : {
                            class: 'form-control numero',
                            placeholder: 'Número'
                        }
                    }, {
                        type: 'text',  size: 'col-md-3', id: 'complemento_c', label: 'Complemento:',
                        name: 'complemento', required: false,
                        attributes : {
                            class: 'form-control',
                            placeholder: 'Complemento'
                        }
                    }
                ]
            },
            {
                title: "Informações do Veículo",
                inputs: [
                    {
                        type: 'text', size: 'col-md-2', id: 'placa_c', label: 'Placa *:',
                        name: 'placa', required: true,
                        attributes: {
                            class: 'form-control text-uppercase',
                            disabled: 'disabled'
                        },
                        additional: [
                            { class: '', icone: '<i class="mdi mdi-help-circle-outline"></i>', action: '', tooltip: '' }
                        ]
                    }, {
                        type: 'text', size: 'col-md-3', id: 'chassi_c', label: 'Chassi *:',
                        name: 'chassi', required: true,
                        attributes: {
                            class: 'form-control text-uppercase'
                        },
                        additional: [
                            { class: '', icone: '<i class="mdi mdi-help-circle-outline"></i>', action: '', tooltip: '' }
                        ]
                    }, {
                        type: 'text', size: 'col-md-3', id: 'renavam_c', label: 'Renavam *:',
                        name: 'renavam', required: true,
                        attributes: {
                            class: 'form-control',
                            placeholder: 'xxx-xxxx'
                        },
                        additional: [
                            { class: '', icone: '<i class="mdi mdi-help-circle-outline"></i>', action: '', tooltip: '' }
                        ]
                    }, {
                        type: 'select', size: 'col-md-2', id: 'cor_c', label: 'Cor *:',
                        name: 'cor', required: true,
                        attributes: {
                            class: 'form-control',
                        }
                    }, {
                        type: 'text', size: 'col-md-2', id: 'ano_modelo_c', label: 'Ano *:',
                        name: 'ano_modelo', required: true,
                        attributes: {
                            class: 'form-control',
                            placeholder: '',
                            disabled: 'disabled'
                        }                        
                    }, {
                        type: 'text',  size: 'col-md-2', id: 'marca_c', label: 'Marca *:',
                        name: 'marca', required: true,
                        attributes: {
                            class: 'form-control',
                            placeholder: '',
                            disabled: 'disabled'
                        }
                    }, {
                        type: 'text',  size: 'col-md-3', id: 'modelo_c', label: 'Modelo *:',
                        name: 'modelo', required: true,
                        attributes: {
                            class: 'form-control',
                            placeholder: '',
                            disabled: 'disabled'
                        }
                    }, {
                        type: 'text', size: 'col-md-2', id: 'combustivel_c', label: 'Combustível *:',
                        name: 'combustivel', required: true,
                        attributes: {
                            class: 'form-control',
                            placeholder: 'combustivel',
                            disabled: 'disabled'
                        }                        
                    }, {
                        type: 'text', size: 'col-md-2', id: 'cilindrada_c', label: 'Cilindrada:',
                        name: 'cilindrada', required: true,
                        attributes: {
                            class: 'form-control text-uppercase'
                        }
                    }, {
                        type: 'select', size: 'col-md-3', id: 'cambio_c', label: 'Câmbio *:',
                        name: 'cambio', required: true,
                        attributes: {
                            class: 'form-control',
                        }                        
                    }
                ]
            }
        ],
        events,
        rules: {
            razao_social: {
                rule: "required|min:2",
                message: { required: "Nome é um campo obrigatório", min: "mínimo :min caracteres" }
            }, email: {
                rule: "required|email",
                message: { required: "Email é um campo obrigatório", email: "Campo deve ser do tipo email" }
            }, cnpj: {
                rule: "required|" + (config.aca_pes_cot_tip === "0" ? 'cpf' : (config.aca_pes_cot_tip === "1" ? 'cnpj' : 'cpf-cnpj')),
                message: {
                    required: (config.aca_pes_cot_tip === "0" ? 'CPF' : (config.aca_pes_cot_tip === "1" ? 'CNPJ' : 'CPF/CNPJ')) + " é um campo obrigatório",
                    cpf: "CPF inválido",
                    cnpj: "CNPJ inválido",
                    "cpf-cnpj": "CPF ou CNPJ inválido"
                }
            }, inscricao_estadual: {
                rule: "required|max:26",
                message: { required: "RG é um campo obrigatório", max: "Máximo de 26 caracteres" }
            }, data_nascimento: {
                rule: "required",
                message: { required: "Data de Nascimento é um campo obrigatório"}
            }, celular: {
                rule: "required",
                message: { required: "Celular é um campo obrigatório" }
            },
            procedencia: {
                rule: "required",
                message: { required: "Procedencia é um campo obrigatório" }
            }, sexo: {
                rule: "required",
                message: { required: "Sexo é um campo obrigatório" }
            },estado_civil: {
                rule: "required",
                message: { required: "Estado Civil é um campo obrigatório" }
            }, cep: {
                rule: "required",
                message: { required: "CEP é um campo obrigatório" }
            }, numero: {
                rule: "required",
                message: { required: "Número é um campo obrigatório" }
            }, endereco: {
                rule: "required",
                message: { required: "Endereço é um campo obrigatório" }
            }, bairro: {
                rule: "required",
                message: { required: "Bairro é um campo obrigatório" }
            }, cidade: {
                rule: "required",
                message: { required: "Cidade é um campo obrigatório" }
            }, estado: {
                rule: "required",
                message: { required: "Estado é um campo obrigatório" }
            }, placa: {
                rule: "required",
                message: { required: "Placa é um campo obrigatório" }
            }, chassi: {
                rule: "required",
                message: { required: "Chassi é um campo obrigatório" }
            }, marca: {
                rule: "required",
                message: { required: "Marca é um campo obrigatório" }
            }, modelo: {
                rule: "required",
                message: { required: "Modelo é um campo obrigatório" }
            }, ano_modelo: {
                rule: "required",
                message: { required: "Ano é um campo obrigatório" }
            }, renavam: {
                rule: "required",
                message: { required: "Renavam é um campo obrigatório" }
            },cor: {
                rule: "required",
                message: { required: "Cor é um campo obrigatório" }
            },cambio: {
                rule: "required",
                message: { required: "Câmbio é um campo obrigatório" }
            }
        }
    };
}