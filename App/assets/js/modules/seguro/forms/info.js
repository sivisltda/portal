import { getConfig as configFunc } from "./../../../config/config.js";
import store from './../../../store/app.js';

const config = configFunc();
const { getIndicadores, getTipoVeiculo } = store.getters;

export function getForm(formRoot, events) {
    let camposPessoais = {};
    if(!config.rapido) {
        camposPessoais = {
            title: 'Dados do Contratante',
            inputs: [
                {
                    type: 'text',  size: 'col-sm-8', id: 'razao_social', label: 'Qual seu nome?',
                    name: 'razao_social', required: true, visibility: true,
                    attributes : {
                        class: 'form-control',
                        placeholder: 'Seu nome'
                    }
                },  {
                    type: 'email', size: 'col-sm-8', id: 'email', label: 'Qual seu email?',
                    name: 'email', required: true, visibility: false,
                    attributes: {
                        class: 'form-control',
                        placeholder: 'nome@dominio.com'
                    }
                },  {
                    type: 'text', size: 'col-sm-7', id: 'cnpj', label: 'Qual seu ' + (config.aca_pes_cot_tip === "0" ? 'CPF' : (config.aca_pes_cot_tip === "1" ? 'CNPJ' : 'CPF/CNPJ')) + '?',
                    name: 'cnpj', required: true,  visibility: false,
                    attributes: {
                        class: 'form-control',
                        placeholder: (config.aca_pes_cot_tip === "0" ? 'xxx.xxx.xxx-xx' : (config.aca_pes_cot_tip === "1" ? 'xx.xxx.xxx/xxxx-xx' : 'xxx.xxx.xxx-xx ou xx.xxx.xxx/xxxx-xx')),
                    }
                },  {
                    type: 'tel', size: 'col-sm-8', id: 'celular', label: 'Qual o seu telefone?',
                    name: 'celular', required: true, visibility: false,
                    attributes: {
                        class: 'form-control',
                        placeholder: '(xx) xxxxx-xxxx',
                        "data-toggle": "input-mask",
                        "data-mask-format": "(99) 99999-9999"
                    }
                },  {
                    type: 'text', size: 'col-sm-8', id: 'cep', label: 'Qual o CEP da sua residência?',
                    name: 'cep', required: true, visibility: false,
                    attributes: {
                        class: 'form-control',
                        placeholder: 'xxxxx-xxx',
                        "data-toggle": "input-mask",
                        "data-mask-format": "99999-999"
                    },
                    additional: [
                        { type: 'button', class: '', icone: '<i class="mdi mdi-help-circle-outline"></i>', action: clickInfoCep, tooltip: '' }
                    ]
                }
            ].filter(function(item) {
                return Object.keys(item).length;
            })
        };
    } 

    let campoAgencia = {};
    const indicadores = getIndicadores();
    if (!config.rapido && indicadores.length) {
        const options = indicadores.reduce(function(acc, item) {
            acc[item.id] = item.razao_social + (item.cidade_nome ?  ' - '+item.cidade_nome : '');
            return acc;
        },  {});
        campoAgencia = {
            type: 'select', size: 'col-sm-8', id: 'indicador', label: 'Está em uma Agência?',
            name: 'indicador', required: false, visibility: true,
            options,
            attributes: {
                class: 'form-control select2',
                placeholder: 'Selecione uma Agência'
            }
        };
    }
    const form = {
        type: 'row',
        root: formRoot,
        group: [
           camposPessoais, 
           {
                title: 'Dados do Veículo',
                inputs: [
                    campoAgencia,
                    {
                        type: 'radio', size: 'col-sm-8', id: 'consulta_fipe', label: '',
                        name: 'consulta_fipe', required: true, visibility: true, left: true,
                        options: (config.seg_vei_cotac_person === 1 ? 
                            { 0: "Fipe", 1: "Personalizado", 2: "Zero Km"} : 
                            { 0: "Fipe", 2: "Zero Km"})
                    },  {
                        type: 'text', size: 'col-sm-8', id: 'valor', label: 'Valor Protegido',
                        name: 'valor', required: true, visibility: false,
                        attributes: {
                            class: 'form-control',
                            placeholder: '0.000,00'
                        }                        
                    },  {
                        type: 'select', size: 'col-sm-8', id: 'tipo_veiculo', label: 'Tipo do seu veículo?',
                        name: 'tipo_veiculo', required: true, visibility: true,
                        options: getTipoVeiculo(),
                        attributes: {
                            class: 'form-control select2',
                            placeholder: 'Selecione o Tipo do seu veículo',
                            optionsId: 'id',
                            optionsValue: 'descricao'
                        }
                    },  {
                        type: 'text', size: 'col-sm-8', id: 'placa', label: 'Qual a placa do seu veículo?',
                        name: 'placa', required: !config.rapido, visibility: false,
                        attributes: {
                            class: 'form-control text-uppercase',
                            placeholder: 'xxx-xxxx',
                            "data-toggle": "input-mask",
                            "data-mask-format": "AAA-9A99"
                        },
                        additional: [
                            { type: 'button' ,   class: '', icone: '<i class="mdi mdi-help-circle-outline"></i>', action: '', tooltip: '' }
                        ]
                    },  {
                        type: 'select', size: 'col-sm-8', id: 'marca', label: 'Marca do seu veículo?',
                        name: 'marca', required: true, visibility: false,
                        attributes: {
                            class: 'form-control js-data-marca-ajax select2',
                            placeholder: 'Selecione a marca do seu veículo'
                        }
                    },  {
                        type: 'select', size: 'col-sm-8', id: 'ano_modelo', label: 'Ano do seu veículo?',
                        name: 'ano_modelo', required: true, visibility: false,
                        attributes: {
                            class: 'form-control js-data-ano-ajax select2',
                            placeholder: 'Selecione o ano do seu veículo'
                        }
                    },  {
                        type: 'select', size: 'col-sm-8', id: 'modelo', label: 'Modelo do seu veículo?',
                        name: 'modelo', required: true, visibility: false,
                        attributes: {
                            class: 'form-control js-data-modelo-ajax select2',
                            placeholder: 'Selecione o modelo do seu veículo'
                        }
                    }
                ].filter(function(item) {
                    return Object.keys(item).length;
                })
            }
        ].filter(Boolean),
        events,
        rules: {
            razao_social: {
                rule: "required|min:2",
                message: { required: "Nome é um campo obrigatório", min: "mínimo :min caracteres" }
            }, email: {
                rule: "requiredIf:has_email|email",
                message: {
                    requiredIf: "Email é um campo obrigatório",
                    email: "Campo deve ser do tipo email"
                }
            }, cnpj: {
                rule: "required|" + (config.aca_pes_cot_tip === "0" ? 'cpf' : (config.aca_pes_cot_tip === "1" ? 'cnpj' : 'cpf-cnpj')),
                message: {
                    required: (config.aca_pes_cot_tip === "0" ? 'CPF' : (config.aca_pes_cot_tip === "1" ? 'CNPJ' : 'CPF/CNPJ')) + " é um campo obrigatório",
                    cpf: "CPF inválido",
                    cnpj: "CNPJ inválido",
                    "cpf-cnpj": "CPF ou CNPJ inválido"
                }
            }, celular: {
                rule: "required",
                message: {
                    required: "Telefone é um campo obrigatório"
                }
            }, cep: {
                rule: "required",
                message: {
                    required: "CEP é um campo obrigatório"
                }
            }, placa: {
                rule: !config.rapido ? "required|placa" : null,
                message: {
                    required: "Placa é um campo obrigatório"
                }
            }, marca: {
                rule: "required",
                message: {
                    required: "Marca é um campo obrigatório"
                }
            }, modelo: {
                rule: "required",
                message: {
                    required: "Modelo é um campo obrigatório"
                }
            }, ano_modelo: {
                rule: "required",
                message: {
                    required: "Ano é um campo obrigatório"
                }
            }
        }
    };
    return form;
}

function clickInfoCep(e) {
    e.preventDefault();
    const t = $(this);
}