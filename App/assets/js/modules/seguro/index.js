import { createItemList, loadInit } from './../../core/route.js';
import { scrollPrecoFloat } from './../../core/function.js';
import { getConfig as configFunc } from './../../config/config.js';
import store from './../../store/app.js';
import {abrirModalWhatsapp} from './../../shared/whatsapp.js';

import * as info from './steps/info.js';
import * as cotacao from './steps/cotacao.js';
import * as single from './steps/single.js';
import * as contratacao from './steps/contratacao.js';
import * as conclusao from './steps/conclusao.js';

const config = configFunc();

export function load(app) {
    const lista = app.find('#list-item');
    const {getProdutos, getProduto} = store.getters;
    info.init().eventNext();
    const produto = getProduto();    
    if (!config.agrupar_plano) {
        if (getProdutos().length > 1)  {
            cotacao.init().eventNext();
        }       
        if (produto && produto.editavel) {
            single.init().eventNext();
        }  
    } else if (produto) {  
        single.init().eventNext();        
    }
    if(!config.rapido) {
        contratacao.init().eventNext();
    }
    conclusao.init().eventNext();
    createItemList(lista);
    loadInit();

    $(document).on('focusin', '[data-toggle=input-mask]', function(e) {
        e.preventDefault();
        const t = $(this);
        t.mask(t.data('mask-format'));
    });
    $(window).scroll(scrollPrecoFloat);
    
    var listEvents = [];
    var allDocEvnts = $._data(document, "events").click;
    Object.keys(allDocEvnts).forEach(function (key) {
        listEvents.push(allDocEvnts[key].selector);
    });
    if (!listEvents.includes('#btn-imprimir')) {
        if (config.adm_desconto_range) {
            $(document).on('change','#rangeComissao', single.aplicarRange);
        }
        $(document).on('click', '#btn-cupom', single.aplicarCupom);
        $(document).on('click', '#btn-imprimir', single.printPDF);
        $(document).on('click', '#btn-whatsapp', abrirModalWhatsapp);        
        $(document).on('show.bs.modal', '#details', single.modalPendencia);
        $(document).on('hidden.bs.modal', '#details', single.refreshPendencia);
    }
}