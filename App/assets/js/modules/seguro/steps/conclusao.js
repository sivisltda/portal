import { incrementarEtapa, eventProximo, eventEtapas, listenEvent, adicionarEtapaEntrePeriodo,
    verificaEtapa, getNumeroEtapa, verificaEAdicionaTemplateTab, getApp } from "./../../../core/route.js";

import  { getConfig as configFunc } from './../../../config/config.js';
import {geraMenu} from './../../../shared/menu-seguro.js';
import store from './../../../store/app.js';
import * as storageService from './../../../service/storage.service.js';

let numeroEtapa         = null;

let config              = null;
let app                 = null;
let btnProx             = null;
let btnAnt              = null;
let tabPanel            = null;

let listaMenuInfo       = null;
let conclusao           = null;

const configEtapa = {
    id: '#conclusao',
    title: 'Conclusão',
    tooltip: ''
};


export function init(posicao) {
    app      = getApp();
    config   = configFunc();
    tabPanel = app.find('#main');
    btnProx  = app.find('#btnProx');
    btnAnt   = app.find('#btnAnt');
    numeroEtapa = posicao ? posicao : incrementarEtapa();

    if (!posicao) {
        eventEtapas(numeroEtapa, configEtapa, iniciaTela);
    } else {
        if(!verificaEtapa(configEtapa.id)) {
            adicionarEtapaEntrePeriodo(numeroEtapa, configEtapa, iniciaTela);
        }
    }
    verificaEAdicionaTemplateTab(tabPanel, configEtapa.id, template(configEtapa.id.replace('#','')));
    return this;
}

function iniciaTela() {
    numeroEtapa     = getNumeroEtapa(configEtapa.id) || numeroEtapa;
    conclusao       = tabPanel.find(configEtapa.id);
    listaMenuInfo   = conclusao.find('.info-valores-plano');
    app.addClass('plano-fixo');
    geraMenu(listaMenuInfo);
    if (!config.cotacao_interna) {
        store.actions.clear();
        storageService.setItem(`loja-${config.contrato}`, store.actions.serialize());
    }
    btnAnt.hide();
    btnProx.hide();
}

function template(id) {
    return `<div class="tab-pane limite" id="${id}">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl d-print-none container-info">
                <h4 class="titulo text-center mb-3">Parabéns pela contratação</h4>
                <div class="text-center">
                    <img id="protection" src="${config.site}assets/img/car-protection.png" >
                    <p class="w-75 mb-2 mx-auto mt-4"> Muito obrigado pela contratação dos nossos serviços. </p>
                    <p class="w-75 mb-2 mx-auto"> Sua compra foi efetuada com <strong>sucesso</strong> e seu veículo está sob análise. </p>
                    <p> Em breve lhe retornaremos.</p>
                    <p class="w-75 mb-2 mx-auto"> Recomendamos que você agende uma vistoria através do telefone <strong> ${config.telefone} </strong> </p>
                    <p class="w-75 mb-2 mx-auto"> Clique <a href="${config.api.replace('/api/', '/login')}" >aqui</a> para acessar a área do cliente </p>
                </div>    
            </div>
            <div class="menu-right info-valores-plano"></div>
        </div>
    </div>
</div>  `;
}

export function eventNext() {
    eventProximo(numeroEtapa, function () {
        listenEvent(numeroEtapa+1);
    });
}


