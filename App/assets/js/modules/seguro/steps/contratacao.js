import { getNumeroEtapa, incrementarEtapa, eventProximo, eventEtapas, listenEvent,  
    adicionarEtapaEntrePeriodo, verificaEtapa, verificaEAdicionaTemplateTab, getApp } 
    from './../../../core/route.js';

import { geraEventos, convertSerializeObject, convertDataIso, convertDataBr, textToNumber, errorCamposInvalidos } from './../../../core/function.js';    

import { createInput } from './../../../core/form.js';
import { validate, validate_cpf, validate_cnpj } from './../../../core/validator.js';

import  { getConfig as configFunc } from './../../../config/config.js';

import * as usuarioService from './../../../service/usuario.sevice.js';
import * as enderecoService from './../../../service/endereco.service.js';
import * as produtoService from './../../../service/produto.service.js';
import * as veiculoService from './../../../service/veiculo.service.js';
import * as storageService from './../../../service/storage.service.js';

import * as contratacaoFormInput from './../forms/contratacao.js';

import {geraMenu} from './../../../shared/menu-seguro.js';
import * as tipoPagamento from './../../../shared/pagamento.js';

import store from './../../../store/app.js';

const { getVeiculo, getUsuario, getCidades, getEndereco, getEstados, getCores, getPlano, getIdParcela, getParcela,
    getProduto, getTermos, getAcessorios, getCambios } = store.getters;
const { setTransacao, setEndereco, setVeiculo } = store.setters;

let app                     = null;
let config                  = null;
let btnProx                 = null;
let btnAnt                  = null;
let tabPanel                = null;

let numeroEtapa             = null;
    
let form                    = null;
let listaMenuInfo           = null;
let telaPagamento           = null;
let formContratacao         = null;

let telaInfo                = null;
let contratacao             = null;
let containerPagamento      = null;    

const valoresInputs = {
    cnpj: ''
};

const configEtapa = {
    id: '#contratacao',
    title: 'Contratação',
    tooltip: ''
};

export function init(posicao) {
    app      = getApp();
    config   = configFunc();
    btnProx  = app.find('#btnProx');
    btnAnt   = app.find('#btnAnt');
    tabPanel = app.find('#main');
    numeroEtapa = posicao ? posicao : incrementarEtapa();
    if (!posicao) {
        eventEtapas(numeroEtapa, configEtapa, iniciaTela);
    } else {
        if (!verificaEtapa(configEtapa.id)) {
            adicionarEtapaEntrePeriodo(numeroEtapa, configEtapa, iniciaTela);
        }
    }
    verificaEAdicionaTemplateTab(tabPanel, configEtapa.id, template(configEtapa.id.replace('#','')));
    return this;
}

function iniciaTela() {
    numeroEtapa         = getNumeroEtapa(configEtapa.id) || numeroEtapa;
    contratacao         = tabPanel.find(configEtapa.id);
    app.addClass('plano-fixo');
    telaPagamento       = contratacao.find('#pagamento');
    telaInfo            = contratacao.find('#atualizar-info');
    listaMenuInfo       = contratacao.find('.info-valores-plano');
    formContratacao     = contratacao.find('#form-contratacao');
    btnAnt.show();
    btnProx.hide();
    telaInfo.show();
    telaPagamento.hide();

    montarFormContratacao(); 
    montarTabPagamento();
    setarInformacoes();
    atualizaDadosInfo();
}

function template(id) {
    return `<div class="tab-pane limite" id="${id}">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl d-print-none d-flex container-info">
                    ${templateFormContratacao()}
                    ${templateFormaPagamento()}           
                </div>
                <div class="menu-right info-valores-plano"></div>
            </div>
        </div>
    </div>        
    `;
}

export function eventNext() {
    eventProximo(numeroEtapa, function () {
        listenEvent(numeroEtapa+1);
    });
}

function montarFormContratacao() {
    const events = [
        {
            root: formContratacao,
            target: '[name="cep"]',
            actions: [
                { event: 'blur', action: buscarCep }
            ]
        }, {
            root: formContratacao,
            target: '[name="estado"]',
            actions: [
                { event: 'change', action: buscarEstado }
            ]
        }, {
            root: formContratacao,
            target: '[name="cnpj"]',
            actions: [
                { event: 'focusin', action: focusInCnpj },
                { event: 'blur', action: getTipoDocumento }
            ]
        }
    ];

    form = contratacaoFormInput.getForm(formContratacao, events);
    let htmlTemplate = form.group.map(function(grupo) {
        return `<h5>${grupo.title}</h5><div class="row mb-4">${createInput(grupo.inputs, form.type)}</div>`;
    }).join('');
    htmlTemplate+=templateVencimento();
    htmlTemplate+=templateTermos();
    htmlTemplate+=`<div class="row">
        <div class="col-md-12 mt-3 d-flex justify-content-end">
            <button class="btn btn-secundaria" id="btn-atualiza-info">${config.nao_pag_online ? 'Concluir' : 'Pagamento'}</button>
            <button class="btn ml-1 btn-secundaria" id="btn-atualiza-outro">Cadastrar Novo</button>    
        </div>
    </div>`;
    formContratacao.html(htmlTemplate);
    formContratacao.on('submit', atualizarInfo);
    formContratacao.find('.select2').select2();
    formContratacao.find('#btn-atualiza-info').on('click', function(e) {
        e.preventDefault();
        formContratacao.submit();
    });    
    formContratacao.find('#btn-atualiza-outro').on('click', function(e) {
        e.preventDefault();
        setVeiculo({});
        listenEvent(1);
    });    
    geraEventos(form.events); 
    
    enderecoService.buscarEstados(function() {
        formContratacao.find('[name="estado"]').html('<option value="">Selecione um estado</option>'
        +getEstados().map(function(e) {
            return `<option value="${e.estado_codigo}">${e.estado_sigla}</option>`;
        }).join('')).select2();        
    }, function(erro) {
        console.error(erro);
        swal('Erro', 'Ocorreu um erro aos estados', 'error');        
    });
    
    produtoService.getListaCores(function() {
        const cores = getCores();
        const veiculo = getVeiculo();
        const option = "<option value=''>Selecione</option>" + (Object.keys(cores).map(function(index) {
            return `<option value="${index}">${cores[index]}</option>`;
        }).join(''));
        formContratacao.find('[name="cor"]').html(option).select2();
        if (veiculo) {
            formContratacao.find('[name="cor"]').val(veiculo.cor).change();
        }
    }, function(err) {
        console.error(err);
        swal('Erro', 'Ocorreu um erro ao buscar as cores', 'error');
    });
    
    produtoService.getListaCambio(function() {
        const cambios = getCambios();
        const veiculo = getVeiculo();
        const option = "<option value=''>Selecione</option>" + (Object.keys(cambios).map(function(index) {
            return `<option value="${cambios[index].id}">${cambios[index].nome}</option>`;
        }).join(''));
        formContratacao.find('[name="cambio"]').html(option).select2();
        if (veiculo) {
            formContratacao.find('[name="cambio"]').val(veiculo.cambio).change();
        }
    }, function(err) {
        console.error(err);
        swal('Erro', 'Ocorreu um erro ao buscar os cambios', 'error');
    });
}

function montarTabPagamento() {
    containerPagamento = telaPagamento.find('#collapseTwoContratacao');
    const valorAcrescimo = getAcessorios().reduce(function(acc, item) {
        acc+= textToNumber(item.preco_venda);
        return acc;
    }, 0);
   
    const plano = getPlano();
    containerPagamento.html(tipoPagamento.templatePagamento(valorAcrescimo));
    tipoPagamento.geraEventos(containerPagamento);
    tipoPagamento.subscribe('changeParcela', configEtapa.id, function() {
        atualizaDadosInfo();
    });
    tipoPagamento.subscribe('processoBoleto', configEtapa.id, function() {
        const data = { id_mens: plano.id_prim_mens, id_parcela: getIdParcela() };
        tipoPagamento.processarBoleto(data, function() {
            setTransacao('boleto');
            listenEvent(numeroEtapa + 1);  
        }, function(erro) {
            console.log(erro);
        });
    });
    tipoPagamento.subscribe('processoLink', configEtapa.id, function() {
        tipoPagamento.processarLink({ id_mens: plano.id_prim_mens, id_parcela: getIdParcela(), link: 1}, function() {
            setTransacao('link');
            listenEvent(numeroEtapa + 1);  
        }, function(erro) {
            console.log(erro);
        });
    });    
    tipoPagamento.subscribe('processoCartao', configEtapa.id, function(t) {
        tipoPagamento.processarCartao(t, { id_mens: plano.id_prim_mens, id_parcela: getIdParcela(), cartao: 1 }, function() {         
            setTransacao('cartao'); 
            listenEvent(numeroEtapa + 1);            
        }, function(erro) {
            console.log(erro);
        });
    });
}

function atualizaDadosInfo() {
    const valorAcrescimo = getAcessorios().reduce(function(acc, item) {
        acc+= textToNumber(item.preco_venda);
        return acc;
    }, 0);    
    geraMenu(listaMenuInfo);
    const dadosValores = tipoPagamento.makeValores();
    tipoPagamento.setValorPagar(containerPagamento, dadosValores, valorAcrescimo);
}

function templateFormContratacao() {
    return `<div id="atualizar-info" class="custom-accordion p-2">
        <div class="accordion-header" id="headingOneContratacao">
            <a href="#collapseOneContratacao" class="custom-accordion-title d-flex justify-content-between pt-2 pb-2"
            data-toggle="collapse" aria-expanded="true" aria-controls="collapseOneContratacao">
                <h5 class="accordion-titulo texto-secundaria">Completar as informações</h5>
                <span class="float-right"><i class="mdi mdi-chevron-down accordion-arrow"></i></span>
            </a>
        </div>
        <div id="collapseOneContratacao" class="accordion-body flex-1 collapse show" aria-labelledby="headingOneContratacao">
            <form id="form-contratacao" class="form-checkout" novalidate>
            </form>
        </div>
    </div>`;
}

function templateFormaPagamento() {
    return `<div id="pagamento" class="custom-accordion p-2">
        <div class="accordion-header" id="headingTwoContratacao">
            <a href="#collapseTwoContratacao" class="custom-accordion-title d-flex justify-content-between pt-2 pb-2"
                data-toggle="collapse" aria-expanded="true" aria-controls="collapseTwoContratacao">
                <h5 class="accordion-titulo texto-secundaria">Pagamento</h5>
                <span class="float-right"><i class="mdi mdi-chevron-down accordion-arrow"></i></span>
            </a>
        </div>
        <div id="collapseTwoContratacao" class="accordion-body flex-1 collapse show" 
        aria-labelledby="headingTwoContratacao"></div></div>`;
}

function templateVencimento() {
    const vencimentosHtml = config.vencimentos ? config.vencimentos.map(function(item) {
        return `<option value="${item.id}">Dia ${item.dia}</option>`;
    }).join('') : '';    
    return vencimentosHtml.length > 0 ?
    `<div class="termos mb-4">
        <h5>Vencimento</h5>    
        <div class="row mb-4">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="vencimento_c">Dia padrão *:</label>
                    <select name="vencimento" id="vencimento_c" class="form-control" required="">${vencimentosHtml}</select>
                    <div class="invalid-feedback"></div>
                </div>
            </div>     
        </div>     
    </div>` : ``; 
}

function templateTermos() {
    const termosAceito = getTermos();
    const produto = getProduto();
    const usuario = getUsuario();
    const veiculo = getVeiculo();
    const termosHtml = produto.termos ? produto.termos.map(function(termo) {
        return `<div class="custom-control custom-checkbox form-check mb-2">
        <input id="termo-${termo.id}" name="termo[]" type="checkbox" class="custom-control-input" value="${termo.id}" ${Number.parseInt(termo.obrigatorio) ? 'required' : ''} ${textToNumber(veiculo.cor) ? 'checked' : ''}>
        <label class="custom-control-label" for="termo-${termo.id}">${(Number.parseInt(termo.obrigatorio) ? '<span class="icon-required">*</span>' : '') + termo.termo}</label>
    </div>`;
    }).join('') : '';
    return `<div class="termos mb-4">
        <h5>Termos:</h5>
        ${termosHtml}
        <div class="custom-control custom-checkbox form-check">
            <input id="termo-email" name="termo-email" type="checkbox" class="custom-control-input" value="0" required checked>
            <label class="custom-control-label" for="termo-email">${config.adm_msg_termos_email}</label>
        </div>
        <div class="custom-control custom-checkbox form-check">
            <input id="termo-celular" name="termo-celular" type="checkbox" class="custom-control-input" value="-1" required checked>
            <label class="custom-control-label" for="termo-celular">${config.adm_msg_termos_whats}</label>
        </div>
    </div>`; 
}


function setarInformacoes() {
    const dados = Object.assign({}, getUsuario(), getVeiculo());
    Object.keys(dados).forEach(function(index) {
        const campo = formContratacao.find(`[name="${index}"]`);
        if (campo.length) {
            const valor = index === 'data_nascimento' ? convertDataIso(dados[index]) : dados[index];
            campo.val(valor);
            if (['estado', 'cidade', 'cor', 'procedencia'].includes(index)) {
                campo.change();
            } else if ('cnpj' === index && valor) {
                (validate_cpf(valor) || validate_cnpj(valor)) && campo.prop('readonly', 'readonly');
            }
        }
    });
}

function focusInCnpj(e) {
    e.preventDefault();
    const campoCPF = $(this);
    const options = {
        onKeyPress: function (cpf, ev, el, op) {
            const masks = ['000.000.000-000', '00.000.000/0000-00'];
            campoCPF.mask((cpf.length > 14) ? masks[1] : masks[0], op);
        }
    };
    campoCPF.length > 11 ? campoCPF.mask('00.000.000/0000-00', options) : campoCPF.mask('000.000.000-00', options);
}

function getTipoDocumento(e) {
    e.preventDefault();
    const t = $(this);
    const valor = t.val();
    if (valor === '') {
        t.removeClass('is-valid').addClass('is-invalid')
        .siblings('.invalid-feedback').text('Documento obrigatório');
        return false;
    }
    if (valor === valoresInputs.cnpj) return false;

    if (validate_cpf(valor) || validate_cnpj(valor)) {
        valoresInputs.cnpj = valor;
        usuarioService.consultaUsuario({cnpj: valor, usuario: true}, function(res) {
            t.removeClass('is-invalid').addClass('is-valid');
        }, function(erro) {
            const msg = erro.responseJSON.erro;
            t.val('').removeClass('is-valid').addClass('is-invalid').siblings('.invalid-feedback').text(msg);
            valoresInputs.cnpj = '';
        });
    } else {
        t.removeClass('is-valid').addClass('is-invalid')
        .siblings('.invalid-feedback').text('Documento inválido');
    }
}


function buscarCep(e) {
    e.preventDefault();
    const t = $(this);
    enderecoService.popularEnderecoViaCep(t.val(), function () {
        const endereco = getEndereco();
        t.closest('.section').find('.hide').removeClass('hide');
        Object.keys(endereco).forEach(function(chave) {
            const input = formContratacao.find(`[name="${chave}"]`);
            if (input.length && endereco[chave]) {
                input.val(endereco[chave]);
                 (!['estado', 'cidade', 'cep'].includes(chave)) ? input.attr('readonly', 'readonly') : input.change();
            }
        });
        formContratacao.find('[name="numero"]').focus();
    }, function (erro) {
        console.log(erro);
        t.val('');
        if (erro.status === 2) {
            swal('Erro', 'Ocorreu um erro ao buscar o endereço', 'error');
        } else {
            swal('Erro', 'Ocorreu um erro ao buscar endereço pelo CEP', 'error');
        }
    });
}

function buscarEstado(e) {
    e.preventDefault();
    const t = $(this);
    
    enderecoService.buscarCidadesPorEstado(t.val(), function() {
        const usuario = getUsuario();
        formContratacao.find('[name="cidade"]').html('<option value="">Selecione uma cidade</option>'
            +(getCidades().map(function(c) {
                return `<option value="${c.cidade_codigo}">${c.cidade_nome}</option>`;
            }).join(''))).select2().val(usuario.cidade).change();
        
    }, function (erro) {
        
        console.log(erro);
        swal('Erro', 'Ocorreu um erro ao buscar as cidades', 'error');
    });
}

function validaTermos() {
    const termos = Array.from(formContratacao.find('input[name="termo[]"], #termo-email, #termo-celular'));
    const validaTermos = termos.filter(function(input) {
        if (input.hasAttribute('required') && !input.checked) {
            input.classList.add('is-invalid');
            return true;
        }
        input.classList.contains('is-invalid') && input.classList.remove('is-invalid');
        return false;
    });
    if (validaTermos.length) {
        swal('Atenção!', 'Deve aceitar todos os termos obrigatórios', 'warning');
        return false;
    }
    return true;
}

function atualizarInfo(e) {
    e.preventDefault();
    
    const data = convertSerializeObject(formContratacao.serialize());
    const validations = validate(form.rules, formContratacao);
    if (validations.length) {
        const errors = validations.reduce(function(acc, i) {
            acc[i.field] = i.message;
            return acc; 
        },  {});
        errorCamposInvalidos(formContratacao, errors);
        swal('Atenção!', 'Informe os campos obrigatórios', 'warning');
        return false;
    }
    data.data_nascimento    = convertDataBr(data.data_nascimento);
    data.email_marketing    = formContratacao.find('[name="termo-email"]').prop('checked');
    data.celular_marketing  = formContratacao.find('[name="termo-celular"]').prop('checked'); 
    if (!validaTermos()) {
        return false;
    }
    const termos = Array.from(formContratacao.find('input[name="termo[]"]:checked')).map(function(i) {
        return i.value;
    });
    usuarioService.patchUsuario(data);
    veiculoService.patchVeiculo({ renavam: data.renavam, cor: data.cor, chassi: data.chassi, cilindrada: data.cilindrada, cambio: data.cambio });
    const veiculo = getVeiculo();
    const vencimento = data.hasOwnProperty('vencimento') ? { vencimento: data.vencimento } : {};
    usuarioService.atualizarUsuario(Object.assign(vencimento, getUsuario(), { veiculo }, { termos }),
    function (res) {
        storageService.setItem(`loja-${config.contrato}`, store.actions.serialize());
        if (config.nao_pag_online) {
            listenEvent(numeroEtapa + 1);
        } else {
            telaPagamento.show();
            telaInfo.hide();        
        }
    }, function (erro) {
        console.log(erro);
        if (erro.status === 422) {
            const errors = erro.responseJSON;
            errorCamposInvalidos(formContratacao, errors);
        }
        swal('Erro', 'Ocorreu um erro ao atualizar as informações', 'error');        
    });
}


