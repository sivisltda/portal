import { getNumeroEtapa, listenProximo,
    incrementarEtapa, eventProximo, eventEtapas, listenEvent, adicionarEtapaEntrePeriodo,
    verificaEtapa, removerEtapa, verificaEAdicionaTemplateTab, getApp } 
    from "./../../../core/route.js";

import { textToNumber, numberFormat, ativarCarrousel, goStepCarrousel } from "./../../../core/function.js";

import { getConfig as configFunc } from "./../../../config/config.js";

import * as produtoService from "./../../../service/produto.service.js"; 
import * as planoService from "./../../../service/plano.service.js";
import * as veiculoService from "./../../../service/veiculo.service.js";
import * as crmService from "./../../../service/crm.service.js";

import * as cobertura from "./single.js";

import store from "./../../../store/app.js";

let numeroEtapa       = null;

let app               = null;
let config            = null;
let tabPanel          = null;
let btnProx           = null;
let btnAnt            = null;

let listaProdutos     = null;
let produtoEscolhido  = null;

const { getVeiculo, getProduto, getProdutos, getServicos, getConsultor, getCupom, getLead } = store.getters;
const { setPlano, setAcessorios } = store.setters;

const configEtapa   = {
    id: '#cotacao',
    title: 'Cotação',
    tooltip: ''
};

export function init(posicao) {
    app = getApp();
    config = configFunc();
    btnProx = app.find('#btnProx');
    btnAnt = app.find('#btnAnt');
    tabPanel = app.find('#main');
    numeroEtapa = posicao ? posicao : incrementarEtapa();
    if (!posicao) {
        eventEtapas(numeroEtapa, configEtapa, iniciaTela);
    } else {
        if (!verificaEtapa(configEtapa.id)) {
            adicionarEtapaEntrePeriodo(numeroEtapa, configEtapa, iniciaTela);
        }
    }
    verificaEAdicionaTemplateTab(tabPanel, configEtapa.id, template(configEtapa.id.replace('#','')));
    return this;
}

function iniciaTela() {
    numeroEtapa = getNumeroEtapa(configEtapa.id) || numeroEtapa;   
    carregaInfo();
    btnAnt.show();
    btnProx.show();
}

function template(id) {
    return `<div class="tab-pane" id="${id}">
        <div class="container">
            <div id="lista-produto" class="container-carrousel container-items-plano"></div>    
        </div>
    </div>
    `;
}

function carregaInfo() {
    const veiculo = getVeiculo();
    produtoEscolhido = getProduto();
    const produtos = getProdutos();
    if (!produtoEscolhido) {
        const produto = veiculo.produto_id ? veiculo.produto_id : produtos[0].id;
        produtoService.setProdutoPorId(produto);
    }
    listaProdutos = tabPanel.find(configEtapa.id).find("#lista-produto");
    ativarCarrousel(
        listaProdutos, 
        templateProdutos(),
        {
            arrows: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            responsive: [
                {
                    breakpoint: 980,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        }
    );
    goStepCarrousel(listaProdutos, 'active');
    listaProdutos.on('click', '.btn-plano', function (e) {
        e.preventDefault();
        const t = $(this);
        produtoService.setProdutoPorId(t.data('id'));
        listenProximo(numeroEtapa);
    });
    
}

export function eventNext() {
    eventProximo(numeroEtapa, function () {
        produtoEscolhido = getProduto();
        if (produtoEscolhido) {
            produtoService.setServicosProduto(produtoEscolhido);
            if (produtoEscolhido.editavel || config.rapido) {
                if (produtoEscolhido.editavel) {
                    cobertura.init(numeroEtapa+1).eventNext();
                } else {
                    removerEtapa('#cobertura');
                }
                const consultor = getConsultor();
                if (config.rapido && consultor && !produtoEscolhido.editavel) {
                    setAcessorios([]);
                    crmService.salvarLead(getLead(), function() {
                        listenEvent(numeroEtapa+1);
                    }, function(erro) {
                        console.log(erro);
                        swal('Erro', 'Ocorreu um erro ao salvar o Lead', 'error');
                    });
                } else {
                    listenEvent(numeroEtapa+1);
                }
            } else {
               salvarPlanoSemAcessorios();
            }
        } else {
            swal('Atenção', 'Para ir pra próxima etapa é necessário selecionar um plano', 'warning');
        }
    });
}

function salvarPlanoSemAcessorios() {
    const veiculoEscolhido = getVeiculo();
    const acessorios = [];
    setAcessorios([]);
    planoService.salvarPlano({ 
        id_produto: produtoEscolhido.id,
        id_veiculo: veiculoEscolhido.id,
        acessorios}, function (res) {
       setPlano(res);
       veiculoService.patchVeiculo({ prim_mens : res.prim_mens, valor_mens: res.valor_mens, id_plano: res.id_plano, acessorios });
       removerEtapa('#cobertura');
       listenEvent(numeroEtapa+1);
    }, function(erro) {
        console.log(erro);
        swal('Erro', 'Ocorreu um erro ao salvar o plano', 'error');
    });
}

function templateProdutos() {
    const servicos = getServicos();
    return getProdutos().map(function(produto) {
        return templateItemPlano(produto, servicos);
    }).join('');
}

function templateItemCobertura(produto, servicos) {
    return servicos.filter(function(servico) {
        return produto.servicos.includes(servico.id);
    }).map(function(servico) {
        return '<li>'+servico.nome+'</li>';
    }).join('');
}

function templateItemPlano(produto, servicos) {    
    const valorProtecoes = produto.acessorios.reduce(function(acc, item) {
        return acc + textToNumber(item.valor);
    }, textToNumber(produto.valor_mensal));    
    const [real, centavo] = numberFormat(valorProtecoes).split(',');
    const valorAnual = numberFormat((valorProtecoes * 12), 1);
    const escolhido = produtoEscolhido && produtoEscolhido.id === produto.id;    
    const parcelaAnual = produto.parcelas.filter(function(p) { return p.parcela === 12});   
    return `<div id="plano-${produto.id}" class="card card-plano-item${((produtoEscolhido && produtoEscolhido.id == produto.id)
        || (!produtoEscolhido && produto.recomendado)) ? ' active' : '' }">
            <div class="card-header bg-secundaria">
                <h5 class="card-title">${produto.nome}</h5>
                <div class="container-valor-plano"><span class="valor-real">${real}</span><sub class="valor-centavo">, ${centavo}</sub>
                <sub class="mes">Médio Mensal</sub></div>
                ${parcelaAnual.length > 0 ? `<div class="valor-plano-anual">Total Anual = ${valorAnual} em 12x sem juros</div>` : ``}
                <div class="mb-0 mt-1 text-center">${config.adm_msg_taxa_adesao}: <span class="adesao">${produto.taxa_adesao}</span></div>
            </div>         
            <div class="card-body">
                ${produto.mensagem ? produto.mensagem : ''}
                <ul class="card-body-scrollbar">
                    ${templateItemCobertura(produto, servicos)}
                </ul>    
            </div>
            <div class="card-footer">
                <button class="btn btn-plano ${escolhido ? 'btn-secundaria' : 'btn-primaria'} btn-block" data-id="${produto.id}">
                    ${ escolhido ? 'selecionado' : 'selecionar'}
                </button>
            </div>
    </div>`;
}