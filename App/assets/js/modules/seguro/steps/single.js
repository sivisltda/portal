import { getNumeroEtapa, incrementarEtapa, eventProximo, eventEtapas, listenEvent, 
  adicionarEtapaEntrePeriodo, verificaEtapa, verificaEAdicionaTemplateTab, getApp }
   from "../../../core/route.js";
import { getConfig as configFunc } from "../../../config/config.js";
import { textToNumber } from './../../../core/function.js';
import { chamaSwalQueue } from "./../../../core/auth.js";
import * as planoService from "../../../service/plano.service.js";
import * as veiculoService from "../../../service/veiculo.service.js";
import * as crmService from "../../../service/crm.service.js";
import * as storageService from './../../../service/storage.service.js';
import * as produtoService from '../../../service/produto.service.js';
import * as menuLateral from './../../../shared/menu-seguro.js';
import { makeModal } from './../../../shared/pendencia.js';
import store from '../../../store/app.js';

const {getUsuario, getProduto, getVeiculo, getVeiculos, getCupom, getAcessorios, 
       getCoberturas, getConsultor, getLead, getRangeComissao, getCoresTema} = store.getters;
const {setAcessorios, setPlano, setIdParcela, setVeiculo, setCupom, setRangeComissao } = store.setters;

let numeroEtapa         = null;
let config              = null;
let app                 = null;
let btnProx             = null;
let btnAnt              = null;
let tabPanel            = null;

let padroes = [];

const configEtapa = {
  id: '#cobertura',
  title: 'Cobertura',
  tooltip: ''
};

export function init(posicao) {
    app      = getApp();
    config   = configFunc();    
    btnProx  = app.find('#btnProx');
    btnAnt   = app.find('#btnAnt');
    tabPanel = app.find('#main');
    numeroEtapa = posicao ? posicao : incrementarEtapa();
    if (!posicao) {
        eventEtapas(numeroEtapa, configEtapa, iniciaTela);
    } else {
        if (!verificaEtapa(configEtapa.id)) {
            adicionarEtapaEntrePeriodo(numeroEtapa, configEtapa, iniciaTela);
        }
    }
    verificaEAdicionaTemplateTab(tabPanel, configEtapa.id, template(configEtapa.id.replace('#','')));
    return this;
}

function iniciaTela() {
  numeroEtapa             = getNumeroEtapa(configEtapa.id) || numeroEtapa;
  const cobertura         = tabPanel.find(configEtapa.id);
  const hero              = cobertura.find('#hero-saudacao');
  const listaMenuInfo     = cobertura.find('.info-valores-plano');
  const listaCoberturas   = cobertura.find('#content-item-acessorio');
  hero.html(montaTemplateHero());
  app.addClass('plano-fixo');
  iniciaCoberturas(listaCoberturas, listaMenuInfo);
  btnAnt.show();
  btnProx.show();
}

function template(id) {
  return `<div class="tab-pane limite" id="${id}">
    <div class="container-fluid">
        <div class="row">
            <div class="d-print-none container-info">
              <div id="tab-content" class="tab-content">
                <div id="hero-saudacao" class="pb-4 pt-2"></div>
                <div id="content-item-acessorio"></div>
              </div>
            </div>
            <div class="menu-right info-valores-plano"></div>
        </div>
    </div>
  </div>
  `;
}

/**
 * Função que chama o evento de todas vez que a etapa é avançada
 */
 export function eventNext() {
  eventProximo(numeroEtapa, function () {
    const consultor = getConsultor();
    const produtoEscolhido = getProduto();
    const veiculoEscolhido = getVeiculo();    
    const idRangeComissao = getRangeComissao();
    if(!config.rapido) {
        const acessorios = getAcessorios();
        const idAcessorios = acessorios.map(function(item) {
            return item.id_servico_acessorio;
        });
        planoService.salvarPlano({
            id_produto: produtoEscolhido.id,
            id_veiculo: veiculoEscolhido.id,
            acessorios: idAcessorios,
            id_range: idRangeComissao
        }, function(res) {
            setPlano(res);
            setIdParcela(res.id_parcela);   
            veiculoService.patchVeiculo({ prim_mens : res.prim_mens, valor_mens: res.valor_mens, id_plano: res.id_plano });
            listenEvent(numeroEtapa + 1);
        });
    } else if (consultor) {
        crmService.salvarLead(getLead(), function() {
            listenEvent(numeroEtapa+1);
        }, function(erro) {
            console.log(erro);
            swal('Erro', 'Ocorreu um erro ao salvar o Lead', 'error');
        });        
    } else {
        listenEvent(numeroEtapa + 1);
    }
});
}

/**
 * Aplicar Range
 */
export function aplicarRange(event) {
    const cobertura         = tabPanel.find(configEtapa.id);
    const listaMenuInfo     = cobertura.find('.info-valores-plano');
    setRangeComissao(event.target.value);
    menuLateral.geraMenu(listaMenuInfo);
}

/**
 * Aplicar Cupom
 */
export function aplicarCupom() {
    const produtoEscolhido = getProduto();
    const veiculoEscolhido = getVeiculo();    
    const cupom = getCupom();
    const acessorios = getAcessorios();    
    const idAcessorios = acessorios.map(function(item) {
        return item.id_servico_acessorio;
    });         
    const cobertura         = tabPanel.find(configEtapa.id);
    const listaMenuInfo     = cobertura.find('.info-valores-plano');
    
    if (config === null) {
        config = configFunc();
    }    
    if(!config.rapido) {
        if (!cupom) {
            planoService.salvarPlano({
                id_produto: produtoEscolhido.id,
                id_veiculo: veiculoEscolhido.id,
                cupom_chave: $("#cupom").val(),
                acessorios: idAcessorios
            }, function(res) {
                $("#cupom").removeClass('is-invalid').siblings('.invalid-feedback').text('');
                setPlano(res);
                setIdParcela(res.id_parcela);
                veiculoEscolhido.cupom = res.cupom;
                veiculoEscolhido.valor_desc_adesao = res.valor_desc_adesao;
                setVeiculo(veiculoEscolhido);
                veiculoService.patchVeiculo({ prim_mens : res.prim_mens, valor_mens: res.valor_mens, id_plano: res.id_plano });
                menuLateral.geraMenu(listaMenuInfo);
                storageService.setItem(`loja-${config.contrato}`, store.actions.serialize());
            }, function (err) {
                const erro = err.responseJSON;
                $("#cupom").addClass('is-invalid').siblings('.invalid-feedback').text(erro.erro);
            });
        } else {
            swal({
                title: 'Atenção',
                text: `Confirma o cancelamento deste desconto?`,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Sim',
                cancelButtonText: 'Não'
            }).then(function (res) {
                if (res.value) {
                    planoService.cancelarCupom({id_convenio: cupom.id_convenio}, function () {
                        setCupom(null); 
                        veiculoEscolhido.cupom = null;
                        veiculoEscolhido.valor_desc_adesao = 0;
                        setVeiculo(veiculoEscolhido);   
                        menuLateral.geraMenu(listaMenuInfo);
                        storageService.setItem(`loja-${config.contrato}`, store.actions.serialize());                        
                    }, function (erro) {
                        console.log(erro);
                        swal('Erro', 'Ocorreu um erro ao cancelar', 'error');
                    });
                }
            });       
        }
    } else {
        if (!cupom) {            
            planoService.verificarCupom({
                id_produto: produtoEscolhido.id,
                cupom_chave: $("#cupom").val()
            }, function(res) {
                $("#cupom").removeClass('is-invalid').siblings('.invalid-feedback').text('');
                setCupom(res);
                menuLateral.geraMenu(listaMenuInfo);
            }, function (err) {
                const erro = err.responseJSON;
                $("#cupom").addClass('is-invalid').siblings('.invalid-feedback').text(erro.erro);
                console.log(erro);
            });
        } else {
            swal({
                title: 'Atenção',
                text: `Confirma o cancelamento deste desconto?`,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Sim',
                cancelButtonText: 'Não'
            }).then(function (res) {
                if (res.value) {
                    setCupom(null); 
                    menuLateral.geraMenu(listaMenuInfo);
                }
            });            
        }
    }
}

/**
 * Refresh Pendencia
 */
export function refreshPendencia() {
    const produtoEscolhido = getProduto();
    const veiculoEscolhido = getVeiculo();    
    const acessorios = getAcessorios();
    const idRangeComissao = getRangeComissao(); 
    const idAcessorios = acessorios.map(function(item) {
        return item.id_servico_acessorio;
    });       
    const cobertura         = tabPanel.find(configEtapa.id);
    const listaMenuInfo     = cobertura.find('.info-valores-plano');    
    if (config === null) {
        config = configFunc();
    }     
    if(!config.rapido) {
        planoService.salvarPlano({
            id_produto: produtoEscolhido.id,
            id_veiculo: veiculoEscolhido.id,
            acessorios: idAcessorios,
            id_range: idRangeComissao
        }, function(res) {
            setCupom(res.cupom);
            veiculoEscolhido.cupom = res.cupom;
            veiculoEscolhido.valor_desc_adesao = res.valor_desc_adesao;
            setVeiculo(veiculoEscolhido);
            menuLateral.geraMenu(listaMenuInfo);
        });
    } else {
        const cupom = getCupom();
        console.log("depois de aplicar rapida");
    }
}

/**
 * Load Modal Pendencia
 */
export function modalPendencia() {
    const produtoEscolhido = getProduto();
    const veiculoEscolhido = getVeiculo();    
    const acessorios = getAcessorios();
    const idRangeComissao = getRangeComissao();    
    const idAcessorios = acessorios.map(function(item) {
        return item.id_servico_acessorio;
    });       
    if (config === null) {
        config = configFunc();
    }   
    if(!config.rapido) {
        planoService.salvarPlano({
            id_produto: produtoEscolhido.id,
            id_veiculo: veiculoEscolhido.id,
            acessorios: idAcessorios,
            id_range: idRangeComissao
        }, function(res) {
            setPlano(res);
            setIdParcela(res.id_parcela);
            veiculoService.patchVeiculo({ prim_mens : res.prim_mens, valor_mens: res.valor_mens, id_plano: res.id_plano });
            makeModal($('#details'), { id_plano: res.id, valor: produtoEscolhido.valor_mensal, plano: produtoEscolhido.nome }, "descontos");
        });
    } else {
        const cupom = getCupom();
        makeModal($('#details'), { id_plano: 0, valor: produtoEscolhido.valor_mensal, plano: produtoEscolhido.nome }, "descontos");
    }
}

/**
 * Imprimir Pdf com os dados
 */
export function printPDF() {
    const produtoEscolhido = getProduto();
    const veiculoEscolhido = getVeiculo();    
    const acessorios = getAcessorios();
    const idRangeComissao = getRangeComissao();    
    const idAcessorios = acessorios.map(function(item) {
        return item.id_servico_acessorio;
    });       
    if (config === null) {
        config = configFunc();
    }     
    if(!config.rapido) {
        planoService.salvarPlano({
            id_produto: produtoEscolhido.id,
            id_veiculo: veiculoEscolhido.id,
            acessorios: idAcessorios,
            id_range: idRangeComissao
        }, function(res) {
            setPlano(res);
            setIdParcela(res.id_parcela);
            veiculoService.patchVeiculo({ prim_mens : res.prim_mens, valor_mens: res.valor_mens, id_plano: res.id_plano });
            const veiculos = getVeiculos();
            if (veiculos.length > 1) {
                chamaSwalQueue(escolhendoVeiculo(veiculos), function (resp) {});                
            } else {
                window.open(`${config.api}dashboard/pendencia?functionPage=imprimir&id=${veiculoEscolhido.id}`,'_blank');  
            }
        });
    } else {
        const cupom = getCupom();
        window.open(`${config.api}dashboard/pendencia?functionPage=imprimirRapida&id=${produtoEscolhido.id}&acessorios=${idAcessorios}
        &placa=${veiculoEscolhido.placa}&marca=${veiculoEscolhido.marca}&modelo=${veiculoEscolhido.modelo}
        &ano_modelo=${veiculoEscolhido.ano_modelo}&codigo_fipe=${veiculoEscolhido.codigo_fipe}&mes_referencia=${veiculoEscolhido.mes_referencia}
        &valor=${veiculoEscolhido.valor}&id_convenio=${(cupom) ? cupom.id_convenio : ''}`,'_blank');
    }
}

function escolhendoVeiculo(veiculos, opcoes = {}, callback = null) {
    const coresTema = getCoresTema();
    return {
        title: 'Seleção do Veículos',
        html: `<div>
                    <p>Selecione o(s) veículo(s) para impressão</p>
                    <ul id="sel-veiculos">${templateItemSelecaoVeiculo('veiculo', veiculos, 'lista-veiculo')}</ul>
                </div>`,
        confirmButtonText: 'Imprimir',
        cancelButtonText: `Sair`,
        cancelButtonColor: coresTema.cor_primaria,
        showLoaderOnConfirm: true, ...opcoes,
        preConfirm: function () {
            return new Promise(function (resolve) {
                const checkVeiculo = $('#sel-veiculos').find('input[type="checkbox"]:checked').first();
                if (checkVeiculo.length) {
                    window.open(`${config.api}dashboard/pendencia?functionPage=imprimir&id=${$('#sel-veiculos')
                    .find('input[type="checkbox"]:checked').map(function() {
                        return this.value;
                    }).get().join(",")}`,'_blank');
                    resolve(true);
                    callback && callback();
                } else {
                    throw new Error('Selecione um veículo');
                }
            }).catch(function (erro) {
                console.log(erro);
                Swal.showValidationError('Selecione um veículo');
                callback && callback();
            });
        }
    };
}

function templateItemSelecaoVeiculo(name, veiculos, parent) {
    return veiculos.map(function (p) {
        return `<li>
              <div class="row justify-content-between">
                  <div class="col-8 col-sm-9 text-left">
                      ${"<h6>" + (p.placa.length > 0 ? p.placa : "Zero Km") + " - " + p.modelo + "</h6>"}
                  </div>
                  <div class="col-4 col-sm-3 action-service">
                      <input type="checkbox" id="${name}-${p.id}" checked 
                      name="${name}[]" value="${p.id}" data-type="multiple" data-parent="${parent}" data-switch="bool">
                      <label for="${name}-${p.id}" data-on-label="sim" data-off-label="não"></label>
                  </div>
              </div>
          </li>`;
    }).join('');
}

function montaTemplateHero() {
  const veiculo = getVeiculo();
  const usuario = getUsuario() ? getUsuario() : getLead();
  return `<div class="box-saudacao">
  ${usuario ? `<h3 class="saudacao"><span class="title-saudacao texto-secundaria">Olá </span> ${usuario.razao_social ? usuario.razao_social : usuario.nome}</h3>` : ''}
  <div class="texto-saudacao">
    Aqui você escolhe as coberturas que mais necessita.<br/>
    Veículo: <strong>${veiculo.modelo+' - '+veiculo.ano_modelo}</strong>.
</div>`;
}

function templateCoberturas(coberturas) {
  return coberturas.map(function(cobertura) {
    if (!cobertura.agrupar_item) {
      return `<div class="item-acessorio">
      <div class="box-descricao">
          <h4 class="title-plano">${cobertura.questao}</h4>
          ${cobertura.descricao ? '<div class="descricao-plano">'+cobertura.descricao+'</div>' : ''}
      </div>
      <div class="box-item-checkbox">${templateItem(cobertura)}</div>
    </div>`;
    }
    return `<div class="item-acessorio agrupa-item">
      <div class="item-acessorio-header">
        <div class="box-descricao">
          <h4 class="title-plano">${cobertura.questao}</h4>
        </div>
        <div class="box-item-checkbox"></div>
      </div>
      <div class="content-acessorio">${templateItem2(cobertura)}</div>
    </div>    
        `;
  }).join('');
}

function montaInfoPlano(produto) {
  return `<div class="item-acessorio">
  <div class="box-descricao">
      <h4 class="title-plano texto-secundaria">${produto.nome}</h4>
      <div class="descricao-plano">${produto.descricao}</div>
  </div>
  <div class="box-item-checkbox">
    <div class="box-checkbox">
      <div class="item-checkbox">
        <input type="checkbox" id="plano" name="plano" checked disabled
        value="${produto.id}" data-switch="bool">
        <label for="plano" data-on-label="sim" data-off-label="não"></label>
      </div>
    </div>
  </div>
</div>`;
}

function templateItem(cobertura) {
  return cobertura.items.map(function(item) {
    const checked = cobertura.desativado ? false : cobertura.selecionados.includes(item.id);  
    return `${cobertura.so_padrao === 1 && item.padrao === 0 ? '' : `<div class="box-checkbox">
      <div class="item-checkbox">
        <input type="checkbox" id="cobertura-${item.id}" name="cobertura[]" 
        value="${item.id}" data-type="${cobertura.tipo}" data-parent="${cobertura.id}"
        data-texto="${item.nome}" data-valor="${item.valor}" data-switch="bool"
        data-padrao="${item.padrao}" ${cobertura.desativado ? 'disabled': ''} ${checked ? 'checked' : ''}>
        <label for="cobertura-${item.id}" data-on-label="sim" data-off-label="não"></label>
      </div>
      ${item.descricao ? '<div class="item-checkbox-descricao">'+item.descricao+'</div>' : ''}
    </div>`}`;
  }).join('');
}

function templateItem2(cobertura) {
  return cobertura.items.map(function(item) {
    const checked = cobertura.desativado ? false : cobertura.selecionados.includes(item.id);  
    return `<div class="d-flex">
      <div class="box-descricao">${cobertura.descricao}</div>
      <div class="box-item-checkbox">
        <div class="item-checkbox">
          <input type="checkbox" id="cobertura-${item.id}" name="cobertura[]" 
          value="${item.id}" data-type="${cobertura.tipo}" data-parent="${cobertura.id}"
          data-texto="${item.nome}" data-valor="${item.valor}" data-switch="bool"
          data-padrao="${item.padrao}" ${cobertura.desativado ? 'disabled': ''} ${checked ? 'checked' : ''}>
          <label for="cobertura-${item.id}" data-on-label="sim" data-off-label="não"></label>
        </div>
      </div>
    </div>`;
  }).join('');
}

function iniciaCoberturas(listaCoberturas, listaMenuInfo) {
    const veiculo = getVeiculo();
    const produto = getProduto();
    const consultor = getConsultor();  
    const cupom = getCupom();
    if (!getRangeComissao() && consultor && !cupom) {
        setRangeComissao(parseInt(consultor.max_comis_pri));
    } else if (!getRangeComissao() && consultor && cupom) {
        setRangeComissao(parseInt(consultor.max_comis_pri) - parseInt(cupom.valor));
    }
    produtoService.getListaCoberturas({tipo_veiculo: veiculo.tipo_veiculo, produto_id: produto.id, valor: textToNumber(veiculo.valor)}, function (res) {
        padroes = produtoService.getItemsPadroes();
        renderizarCobertura(listaCoberturas, listaMenuInfo, produto, res);
        if ($('input[name="cobertura[]"]').length > 0) {
            displayRender($($('input[name="cobertura[]"]')[0]), listaCoberturas, listaMenuInfo, produto);
        }
    }, function (erro) {
        console.log(erro.responseText);
        swal('Erro', 'Ocorreu um erro de buscar as coberturas', 'error');
    });
    listaCoberturas.on('change', 'input[name="cobertura[]"]', function () {
        displayRender($(this), listaCoberturas, listaMenuInfo, produto);
    });
}

function displayRender(t, listaCoberturas, listaMenuInfo, produto) {
    const parent = t.data('parent');
    if (t.data('type') === 'single') {
        Array.from(listaCoberturas.find('*[data-parent="'+parent+'"]')).forEach(function (item) {
            if (item.value !== t.val()) {
                item.checked = false;
            }
        });
    }
    if (listaCoberturas.find('*[data-parent="'+parent+'"]:checked').length === 0) {
        const itemPadrao = padroes.find(function(item) {
            return item.cobertura == parent;
        });
        itemPadrao && listaCoberturas.find(`#cobertura-${itemPadrao.id}`).prop('checked', 'checked');
    }
    setAcessorios(filtraItens(listaCoberturas, produto));
    renderizarCobertura(listaCoberturas, listaMenuInfo, produto, getCoberturas());
}

function renderizarCobertura(listaCoberturas, listaMenuInfo, produto, coberturas) {
  const acessorios = getAcessorios();
  const idsAcessorios = acessorios.map(function(item) {return item.id_servico_acessorio;});
  const coberturasFiltradas = coberturas.map(function(cobertura) {
    cobertura.desativado = cobertura.dependencias.length ? !idsAcessorios.find(function(item) {
      return cobertura.dependencias.includes(item);
    }) : false;
    cobertura.selecionados = cobertura.items.filter(function(item) {
      return cobertura.desativado ? false : idsAcessorios.includes(item.id);
    }).map(function(item) { return item.id;});    
    if (cobertura.desativado) {
      const idsItemCobertura = cobertura.items.map(function(item) {
        return item.id;
      });
      setAcessorios(acessorios.filter(function(item) {
       return !idsItemCobertura.includes(item.id_servico_acessorio);
      }));
    } else if (!cobertura.selecionados.length) {
      cobertura.selecionados = cobertura.items.filter(function(item) { 
        return item.padrao;
      });
      if (cobertura.selecionados.length) {
        const item = cobertura.selecionados[0];
        setAcessorios([...acessorios, {
          id_servico_acessorio: item.id,
          preco_venda: item.valor,
          descricao: item.nome,
          parent: cobertura.id,
          padrao: item.padrao
        }]);
        cobertura.selecionados = cobertura.selecionados.map(function(item) { return item.id;});
      }
    }    
    return cobertura;
  });    
  const templatePlano = montaInfoPlano(produto) + templateCoberturas(coberturasFiltradas);
  listaCoberturas.html(templatePlano);
  menuLateral.geraMenu(listaMenuInfo);
}

function filtraItens(listaCoberturas, produtoEscolhido) {
  return  Array.from(listaCoberturas.find('input[name="cobertura[]"]:checked'))
    .filter(function (item) {
      return !(produtoEscolhido.servicos.indexOf(parseInt(item.value)) > -1 || padroes.indexOf(item.value) > -1);
    }).map(function (item) {
      const element   = $(item);
      const valor     = element.data('valor');
      const texto     = element.data('texto');
      const id        = element.val();
      const parent    = element.data('parent');
      const padrao    = element.data('padrao');
      return {
        id_servico_acessorio: id,
        descricao: texto,
        preco_venda: valor,
        parent, 
        padrao
      };
    });
}