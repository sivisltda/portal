import { getNumeroEtapa, incrementarEtapa, eventProximo, eventEtapas, listenEvent, 
    adicionarEtapaEntrePeriodo, verificaEtapa, verificaEAdicionaTemplateTab, getApp }
     from "./../../../core/route.js";

import { getConfig as configFunc } from "./../../../config/config.js";

import * as planoService from "./../../../service/plano.service.js";
import * as veiculoService from "./../../../service/veiculo.service.js";
import * as crmService from "./../../../service/crm.service.js";

import {iniciaCoberturas} from './../shared/cobertura.js';

import store from "./../../../store/app.js";

let numeroEtapa         = null;
let config              = null;
let app                 = null;
let btnProx             = null;
let btnAnt              = null;
let tabPanel            = null;

const { getProduto, getVeiculo, getCupom, getAcessorios, getConsultor, getLead } = store.getters;
const { setPlano } = store.setters;

const configEtapa = {
    id: '#cobertura',
    title: 'Cobertura',
    tooltip: ''
};

export function init(posicao) {
    app      = getApp();
    config   = configFunc();
    btnProx  = app.find('#btnProx');
    btnAnt   = app.find('#btnAnt');
    tabPanel = app.find('#main');
    numeroEtapa = posicao ? posicao : incrementarEtapa();
    if (!posicao) {
        eventEtapas(numeroEtapa, configEtapa, iniciaTela);
    } else {
        if(!verificaEtapa(configEtapa.id)) {
            adicionarEtapaEntrePeriodo(numeroEtapa, configEtapa, iniciaTela);
        }
    }
    verificaEAdicionaTemplateTab(tabPanel, configEtapa.id, template(configEtapa.id.replace('#','')));
    return this;
}

function iniciaTela() {
    numeroEtapa         = getNumeroEtapa(configEtapa.id) || numeroEtapa;
    const cobertura     = tabPanel.find(configEtapa.id);
    const listaCoberturas = cobertura.find('#accordionOne');
    const listaMenuInfo = cobertura.find('.info-valores-plano');
    app.addClass('plano-fixo');
    iniciaCoberturas(listaCoberturas, listaMenuInfo);
    btnAnt.show();
    btnProx.show();
}

function template(id) {
    return `<div class="tab-pane limite" id="${id}">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg p-0 d-print-none container-info">
                    <div class="cobertura-header p-2">
                        <h2>Essa parte que você <br>configura e personaliza sua proteção</h2>
                    </div>
                    <div class="cobertura-body">
                        <form>                                    
                            <ul id="accordionOne" class="custom-accordion"></ul>
                        </form>
                    </div>
                </div>
                <div class="menu-right info-valores-plano"></div>
            </div>
        </div>
    </div>
    `;
}

export function eventNext() {
    eventProximo(numeroEtapa, function () {
        const consultor = getConsultor();
        const produtoEscolhido    = getProduto();
        const veiculoEscolhido    = getVeiculo();
        if(!config.rapido) {
            const acessorios = getAcessorios();
            const idAcessorios = acessorios.map(function(item) {
                return item.id_servico_acessorio;
            });   
            planoService.salvarPlano({
                id_produto: produtoEscolhido.id,
                id_veiculo: veiculoEscolhido.id,
                acessorios: idAcessorios
            }, function(res) {
                setPlano(res)
                veiculoService.patchVeiculo({ prim_mens : res.prim_mens, valor_mens: res.valor_mens, id_plano: res.id_plano });
                listenEvent(numeroEtapa + 1);
            });
        } else if (consultor) {
            crmService.salvarLead(getLead(), function() {
                listenEvent(numeroEtapa+1);
            }, function(erro) {
                console.log(erro);
                swal('Erro', 'Ocorreu um erro ao salvar o Lead', 'error');
            });        } else {
            listenEvent(numeroEtapa + 1);
        }
    });
}

