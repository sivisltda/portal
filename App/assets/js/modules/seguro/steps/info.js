import { getNumeroEtapa, incrementarEtapa, listenEvent, eventProximo,
        eventEtapas, verificaEtapa, removerEtapa, adicionarEtapaEntrePeriodo,
        verificaEAdicionaTemplateTab, getApp } from "../../../core/route.js";

import { blurInput, geraEventos, convertSerializeObject, selecionaOption,
        maskMoney, textToNumber, errorCamposInvalidos } from "../../../core/function.js";

import { createInput } from "../../../core/form.js";
import { chamaSwalQueue, requisitaCPF } from "./../../../core/auth.js";

import { validate_email, validate_cpf, validate_cnpj, validate, validacaoInput,
        validacaoPlaca } from "../../../core/validator.js";

import store from "../../../store/app.js";

import { getConfig as configFunc } from "../../../config/config.js";

import * as userInfo from "../forms/info.js";
import * as crmLead from "../../../shared/crmLead.js";
import * as usuarioService from "../../../service/usuario.sevice.js";
import * as veiculoService from "../../../service/veiculo.service.js";
import * as enderecoService from "../../../service/endereco.service.js";
import * as produtosService from "../../../service/produto.service.js";
import * as planoService from "../../../service/plano.service.js";
import * as cotacao from "./cotacao.js";
import * as single from "./single.js";

/*Store*/
const {getUsuario, getVeiculo, getEndereco, getVeiculos, getTipoVeiculo, getSelTipoVeiculo, getSelConsultaFipe,
    getAnos, getModelos, getCoresTema, getConsultor} = store.getters;
const {setUsuario, setVeiculo, setAcessorios, setProduto, setCoberturas, setSelTipoVeiculo, setSelConsultaFipe,
    setPlano} = store.setters;

/* route numero etapa */
let numeroEtapa = null;

/* Config  */
let app = null;
let config = null;
let tabPanel = null;
let btnProx = null;
let btnAnt = null;

/* Personal */
let formUser = null;
let templateForm = null;

const valoresInputs = {
    email: '',
    cep: '',
    cnpj: '',
    tipoVeiculo: ''
};

const configEtapa = {
    id: '#info',
    title: 'Sobre você',
    tooltip: ''
};

export function init(posicao) {
    app = getApp();
    config = configFunc();
    tabPanel = app.find('#main');
    btnProx = app.find('#btnProx');
    btnAnt = app.find('#btnAnt');
    numeroEtapa = posicao ? posicao : incrementarEtapa();
    if (!posicao) {
        eventEtapas(numeroEtapa, configEtapa, iniciaTela);
    } else {
        if (!verificaEtapa(configEtapa.id)) {
            adicionarEtapaEntrePeriodo(numeroEtapa, configEtapa, iniciaTela);
        }
    }
    verificaEAdicionaTemplateTab(tabPanel, configEtapa.id, template(configEtapa.id.replace('#', '')));
    return this;
}

function template(id) {
    return `<div class="tab-pane  py-3 bg-white" id="${id}">
      <div class="container">
          <h3 class="texto-secundaria">
          ${config.adm_msg_boas_vindas ? config.adm_msg_boas_vindas : 'Olá, para que possamos fazer sua cotação, precisamos de alguns dados seus, é super rápido!!!'}
          </h3>
          <div class="row my-4">
              <div class="col-lg-8 offset-lg-2">
                  <form class="form-horizontal form-checkout" id="form-inicio" novalidate></form>
              </div>
          </div>
      </div>
  </div>`;
}

function montarTemplateForm() {
    const events = [
        {
            root: formUser,
            target: '[name="email"]',
            actions: [
                {event: 'blur', action: getConsultaUsuario}
            ]
        }, {
            root: formUser,
            target: '[name="cnpj"]',
            actions: [
                {event: 'blur', action: getTipoDocumento}
            ]
        },
        {
            root: formUser,
            target: '[name="has_email"]',
            actions: [
                {event: 'change', action: changeHasEmail}
            ]
        },
        {
            root: formUser,
            target: '[name="cep"]',
            actions: [
                {event: 'blur', action: buscarCep}
            ]
        },
        {
            root: formUser,
            target: '[name="consulta_fipe"]',
            actions: [
                {event: 'change', action: changeConsultaFipe}
            ]
        },
        {
            root: formUser,
            target: '[name="placa"]',
            actions: [
                {event: 'change', action: buscarPlaca}
            ]
        }, {
            root: formUser,
            target: '[name="marca"], [name="modelo"], [name="ano_modelo"], [name="tipo_veiculo"]',
            actions: [
                {event: 'change', action: changeVeiculo}
            ]
        }, {
            root: formUser,
            target: '[name="tipo_veiculo"]',
            actions: [
                {event: 'change', action: changeTipoVeiculo}
            ]
        }, {
            root: formUser,
            target: '[name="marca"]',
            actions: [
                {event: 'change', action: selecioneUmaMarca}
            ]
        }, {
            root: formUser,
            target: '[name="ano_modelo"], [name="valor"]',
            actions: [
                {event: 'change', action: selecioneUmAno}
            ]
        }, {
            root: formUser,
            target: '[name="modelo"]',
            actions: [
                {event: 'change', action: selecioneUmModelo}
            ]
        }
    ];    
    templateForm = userInfo.getForm(formUser, events);
    const usuarioFormTemplate = templateForm.group.filter(function (item) {
        return Object.keys(item).length;
    }).map(function (section, index) {
        return `<div class="section${index === 1 && !config.rapido ? ' box-veiculo hide' : ''}">
          <h4 class="mb-2 subtitulo texto-secundaria">${section.title}</h4>
          ${index === 1 ? '<div class="box-selecao-veiculo d-flex justify-content-end"></div>' : ''}
          ${createInput(section.inputs, templateForm.type)}
      </div>`;
    }).join('');
    formUser.html(usuarioFormTemplate);
    const campoIndicador = formUser.find('[name="indicador"]');
    if (campoIndicador.length) {
        campoIndicador.select2();
    }
    blurInput(formUser, ['email', 'has_email'], validacaoInput(templateForm));
    const CpfCnpjMaskBehavior = function (val) {
        return (config.aca_pes_cot_tip === "0" ? '000.000.000-00' : (config.aca_pes_cot_tip === "1" ? '00.000.000/0000-00' : val.replace(/\D/g, '').length <= 11 ? '000.000.000-009' : '00.000.000/0000-00'));
    };
    const cpfCnpjpOptions = {
        onKeyPress: function (val, e, field, options) {
            field.mask(CpfCnpjMaskBehavior.apply({}, arguments), options);
        }
    };
    formUser.find('[name="cnpj"]').mask(CpfCnpjMaskBehavior, cpfCnpjpOptions);
    geraEventos(templateForm.events);
}

function iniciaTela() {
    numeroEtapa = getNumeroEtapa(configEtapa.id) || numeroEtapa;
    app.removeClass('plano-fixo');
    btnAnt.hide();
    btnProx.show();
    carregaInfo();
}

function carregaInfo() {
    formUser = tabPanel.find(configEtapa.id).find('#form-inicio');
    montarTemplateForm();
    const veiculo = getVeiculo();
    const usuario = getUsuario();
    setSelTipoVeiculo(getTipoVeiculo()[0].id);
    const tipo_veiculo = getTipoVeiculo().filter((item) => item.id === getSelTipoVeiculo())[0];
    if (config.rapido) {
        formUser.find(':input').closest('.form-group').removeClass('hide');
        formUser.find('[name="tipo_veiculo"]').select2().val(veiculo ? veiculo.tipo_veiculo : tipo_veiculo.id).change();
        formUser.find('[name="placa"]').val(veiculo ? veiculo.placa : '');
    } else if (usuario) {
        valoresInputs.email = usuario.email;
        if ((usuario.hasCnpj)) {
            formUser.find('[name="razao_social"], [name="email"]').closest('.form-group').removeClass('hide');
            formUser.find('[name="email"]').val(usuario.email);
            formUser.find('[name="razao_social"]').val(usuario.razao_social);
            removeCampoCpf();
        } else {
            formUser.find(':input').closest('.form-group').removeClass('hide');
            removeCampoCpf();
            setarInformacoesUsuario();
            if (veiculo) {
                formUser.find('.box-veiculo').removeClass('hide');
                formUser.find('[name="tipo_veiculo"]').select2().val(veiculo ? veiculo.tipo_veiculo : tipo_veiculo.id).change();
                templateSelecaoVeiculo();
                setarInformacoesVeiculo();
            }
        }
    }
    maskMoney(formUser.find('input[name="valor"]'));
    $(document).on('click', 'input[name="veiculo[]"]', function () {
        const t = $(this);
        if (t.data('type') === 'single') {
            Array.from($(document).find('input[name="veiculo[]"]'))
                    .filter(function (item) {
                        return item.dataset.parent == t.data('parent') && item.value != t.val();
                    }).forEach(function (item) {
                item.checked = false;
            });
        }
    });
    if (!$._data($("#form-inicio")[0], "events").submit) {
        formUser.on('submit', salvarUsuarioVeiculo);    
    }
}

export function eventNext() {
    eventProximo(numeroEtapa, function () {
        formUser.submit();
    });
}

function changeHasEmail(e) {
    e.preventDefault();
    const t = $(this);
    if (t.is(':checked')) {
        formUser.find('[name="email"]').prop('readonly', false)
                .closest('.form-group').removeClass('hide').next().addClass('hide');
    } else {
        formUser.find('[name="email"]').prop('readonly', true).val('')
                .closest('.form-group').addClass('hide').next().removeClass('hide');
    }
}

function getTipoDocumento(e) {
    e.preventDefault();
    const t = $(this);
    const valor = t.val();
    if (valor === '') {
        t.removeClass('is-valid').addClass('is-invalid')
                .siblings('.invalid-feedback').text('Documento obrigatório');
        return false;
    }
    if (valor === valoresInputs.cnpj)
        return false;

    if (validate_cpf(valor) || validate_cnpj(valor)) {
        valoresInputs.cnpj = valor;
        usuarioService.consultaUsuario({cnpj: valor}, function () {
            t.removeClass('is-invalid').addClass('is-valid');
            const elementoPai = t.closest('.form-group');
            elementoPai.next('.form-group').removeClass('hide');
        }, function (erro) {
            const msg = erro.responseJSON.erro;
            t.val('').removeClass('is-valid').addClass('is-invalid').siblings('.invalid-feedback').text(msg);
            valoresInputs.cnpj = '';
        });
    } else {
        t.removeClass('is-valid').addClass('is-invalid')
                .siblings('.invalid-feedback').text('Documento inválido');
    }
}

function getConsultaUsuario(e) {
    e.preventDefault();
    const t = $(this);
    const valor = t.val();
    if (valor !== "" && validate_email(valor)) {
        t.removeClass('is-invalid').addClass('is-valid');
        if (valoresInputs.email === valor) {
            return false;
        }
        valoresInputs.email = valor;
        usuarioService.getBuscarUsuario(valor, function (res) {
            if (res.hasOwnProperty('hasCnpj')) {         
                removeCampoCpf();
                let r = 1;
                chamaSwalQueue(requisitaCPF(res, function (data) {
                    $(':input').closest('.form-group').removeClass('hide');
                    setarInformacoesUsuario();
                    if (data.veiculos.length) {
                        veiculoService.setListaVeiculos(data.veiculos);
                        Swal.insertQueueStep(escolhendoVeiculo(data.veiculos, null, function () {
                            r = 2;
                        }));
                        templateSelecaoVeiculo();
                    }                    
                }, function () {
                    r = 1;
                }), function (resp) {                  
                    if (resp.hasOwnProperty('dismiss') && [Swal.DismissReason.esc,
                        Swal.DismissReason.cancel].includes(res.dismiss)) {
                        if (r === 1) {
                            t.val('').focus();
                        } else if (r === 2) {
                            clearVeiculo();
                        }
                    }
                });
            } else {               
                $(':input').closest('.form-group').removeClass('hide');
                setaUsuario(res);
                if (res.hasOwnProperty('veiculos')) {
                    veiculoService.setListaVeiculos(res.veiculos);
                    chamaSwalQueue(escolhendoVeiculo(res.veiculos), function (resp) {
                        if (resp.dismiss === 'cancel') {
                            clearVeiculo();
                        }
                    });                    
                    templateSelecaoVeiculo();
                }
            }                      
        }, function (erro) {
            if (erro.status === 500) {
                swal('Erro', 'Ocorreu um erro ao buscar o usuario', 'error');
            } else if (erro.status === 404) {
                t.closest('.form-group').next().removeClass('hide').find(':input').focus();                
            }  
            const campoCPF = formUser.find('[name="cnpj"]');
            if (!campoCPF.length) {                      
                setUsuario(null);
                iniciaTela();
            }            
        });
    } else {
        t.addClass('is-invalid').next('.invalid-feedback').text('Email inválido');
    }
}

function changeConsultaFipe(e) {
    e.preventDefault();
    const t = $(this);
    if (t && t.val()) {
        exibirValor(t.val());        
        setCoberturas([]);
        preenchendoMarca(); 
    }
}

function exibirValor(tipo) {
    if (tipo === "1") {
        $('[name="valor"]').closest('.form-group').removeClass('hide');
        $('[name="valor"]').closest('.form-group').show();
        $('[name="placa"]').closest('.form-group').show();
    } else if (tipo === "2") {
        $('[name="valor"]').closest('.form-group').hide();
        $('[name="placa"]').closest('.form-group').hide();
        $('[name="marca"]').closest('.form-group').removeClass('hide');
        $('[name="ano_modelo"]').closest('.form-group').removeClass('hide');
        $('[name="modelo"]').closest('.form-group').removeClass('hide');
    } else {
        $('[name="valor"]').closest('.form-group').hide();            
        $('[name="placa"]').closest('.form-group').show();                    
    }    
    setSelConsultaFipe(tipo);
}

function buscarCep(e) {
    e.preventDefault();
    const t = $(this);
    if (t.val().length === 9) {
        enderecoService.popularEnderecoViaCep(t.val(), function () {
            formUser.find('.box-veiculo').removeClass('hide');            
            const tipo_veiculo = getTipoVeiculo().filter((item) => item.id === getSelTipoVeiculo())[0];
            formUser.find('[name="tipo_veiculo"]').select2().val(tipo_veiculo.id).change();
            formUser.find('[name="consulta_fipe"]').val([getSelConsultaFipe()]);
            exibirValor(getSelConsultaFipe());
            formUser.find('[name="tipo_veiculo"]').focus();
        }, function (erro) {
            console.log(erro);
            t.val('');
            if (erro.status === 2) {
                swal('Erro', 'Ocorreu um erro ao buscar o endereço', 'error');
            } else {
                swal('Erro', 'Ocorreu um erro ao buscar endereço pelo CEP', 'error');
            }
        });
    }
}

function buscarPlaca(e) {
    e.preventDefault();
    const t = $(this);
    if (t.val().length === 8 && validacaoPlaca(t.val())) {
        t.removeClass('is-invalid').addClass('is-valid');
        veiculoService.getBuscaPlaca(t.val(), function (res) {
            const veiculo = getVeiculo();           
            setVeiculo(Object.assign(res.data, {
                id: res.data.id || null,
                consulta_fipe: res.consulta_fipe || getSelConsultaFipe(),
                tipo_veiculo: veiculo.tipo_veiculo || getSelTipoVeiculo()
            }));
            setarInformacoesVeiculo();
        }, function (erro) {
            console.log(erro);
            formUser.find('[name="marca"]').val('').change();
            formUser.find('[name="ano"]').html('<option value="">Nenhum ano encontrado</option>').select2();
            formUser.find('[name="modelo"]').html('<option value="">Nenhum modelo encontrado</option>').select2();
            if (erro.status === 403) {
                swal('Atenção', 'A placa desse veículo já foi cadastrado. Por favor entre em contato com o administrativo ' + 
                ($(".font-weight-bold:contains('SAC')").html() ? 'através do telefone ' + $(".font-weight-bold:contains('SAC')").html().substr(5) : "!"), 'warning')
                        .then(function () {
                            t.val('');
                        });
            } else if (erro.status === 500) {
                t.val('');
                swal('Erro', 'Ocorreu um erro ao validar a placa do veículo', 'error');
            }
        });
    }
}

function changeVeiculo(e) {    
    e.preventDefault();
    const t = $(this);
    if (!config.rapido && t.val()) {
        const elementoPai = t.closest('.form-group');
        if (elementoPai.length) {
            const elementoIrmao = elementoPai.next('.form-group');
            if (elementoIrmao.length && validacaoInput(templateForm, t)) {
                elementoIrmao.removeClass('hide');
            }
        }
    }
}

function changeTipoVeiculo(e) {
    e.preventDefault();
    const t = $(this);
    if (t && t.val()) {
        setSelTipoVeiculo(t.val());
        setCoberturas([]);
        preenchendoMarca();
    }
}

function preenchendoMarca() {
    veiculoService.getMarca(function (res) {
        const marcas = '<option value="">Selecione uma Marca</option>' + (res.map(function (marca) {
            return `<option value="${marca.Value}">${marca.Label}</option>`;
        }).join(''));
        formUser.find('[name="marca"]').html(marcas).select2();
        const veiculo = getVeiculo();
        if (veiculo && veiculo.marca)
            selecionaOption(formUser.find('[name="marca"]'), veiculo.marca);
    }, function (err) {
        console.log(err);
        if (err.erro === 2) {
            veiculoService.alertaErroFipe();
        } else {
            swal('Erro', 'Ocorreu um erro ao listar as marcas', 'error');
        }
    });
}

function setaUsuario(res) {
    setUsuario(usuarioService.transformUsuario(res));
    setarInformacoesUsuario();
}

function removeCampoCpf() {
    const campoCPF = formUser.find('[name="cnpj"]');
    if (campoCPF.length) {
        campoCPF.closest('.form-group').remove();
    }
}

function escolhendoVeiculo(veiculos, opcoes = {}, callback = null) {
    const coresTema = getCoresTema();
    return {
        title: 'Seleção do Veículos',
        html: `<div>
                    <p><div><input class="filterSearch" placeholder="Buscar" type="text"/></div></p>
                    <ul id="sel-veiculos">${templateItemSelecaoVeiculo('veiculo', veiculos, 'lista-veiculo')}</ul>
                </div>`,
        cancelButtonText: `Novo`,
        cancelButtonColor: coresTema.cor_primaria,
        showLoaderOnConfirm: true, ...opcoes,
        preConfirm: function () {
            return new Promise(function (resolve) {
                const checkVeiculo = $('#sel-veiculos').find('input[type="checkbox"]:checked').first();
                formUser.find('.section.box-veiculo').removeClass('hide');
                if (checkVeiculo.length) {
                    const veiculo = getVeiculos().find(function (item) {
                        return item.id == checkVeiculo.val();
                    });
                    setVeiculo(veiculo);
                    setAcessorios(veiculo.acessorios);
                    setarInformacoesVeiculo();
                    resolve(true);
                    callback && callback();
                } else {
                    throw new Error('Selecione um veículo');
                }
            }).catch(function (erro) {
                console.log(erro);
                Swal.showValidationError('Selecione um veículo');
                callback && callback();
            });
        },
        onOpen: function () {
            $(document).on("input propertychange paste change", '.filterSearch', function(e) {
                var value = $(this).val().toLowerCase();
                console.log('b ' + value);
                $("#sel-veiculos").find('li').filter(function() {
                  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        }
    };
}

function salvarUsuarioVeiculo(e) {
    e.preventDefault();
    formUser.find(':input').removeClass('is-invalid').siblings('.invalid-feedback').html('');
    const formUsuario = $(this);
    const validations = validate(templateForm.rules, formUsuario);
    if (validations.length) {
        const errors = validations.reduce(function (acc, i) {
            acc[i.field] = i.message;
            return acc;
        }, {});
        errorCamposInvalidos(formUser, errors);
        swal('Atenção!', 'Informe os campos obrigatórios', 'warning');
        return false;
    }
    const veiculo = getVeiculo(); 
    if (!veiculo.hasOwnProperty('id')) {
        setVeiculo(Object.assign(veiculo, {id: null, consulta_fipe: getSelConsultaFipe(), tipo_veiculo: getSelTipoVeiculo()}));
    }
    const data = convertSerializeObject(formUsuario.serialize());
    veiculoService.patchVeiculo({placa: data.placa, tipo_veiculo: data.tipo_veiculo, consulta_fipe: data.consulta_fipe});
    if (config.rapido) {
        verificaValorProduto();
    } else {
        const endereco = getEndereco();
        if (endereco.hasOwnProperty('cidade') && endereco.hasOwnProperty('estado')) {
            usuarioService.patchUsuario(Object.assign(data, endereco));
            const veiculo = getVeiculo();
            delete veiculo.acessorios;
            usuarioService.salvarUsuario(Object.assign(getUsuario(), {veiculo}), function () {
                verificaValorProduto();
            }, function (erro) {
                console.log(erro);
                if (erro.status === 422) {
                    const errors = erro.responseJSON;
                    errorCamposInvalidos(formUser, errors);
                }
                swal('Erro', 'Ocorreu um erro ao salvar o usuário', 'error');
            });
        }
    }
}

function templateItemSelecaoVeiculo(name, veiculos, parent) {
    return veiculos.map(function (p) {
        return `<li>
              <div class="row justify-content-between">
                  <div class="col-8 col-sm-9 text-left">
                      ${"<h6>" + (p.placa.length > 0 ? p.placa : "Zero Km") + " - " + p.modelo + "</h6>"}
                  </div>
                  <div class="col-4 col-sm-3 action-service">
                      <input type="checkbox" id="${name}-${p.id}" 
                      name="${name}[]" value="${p.id}" data-type="single" data-parent="${parent}" data-switch="bool">
                      <label for="${name}-${p.id}" data-on-label="sim" data-off-label="não"></label>
                  </div>
              </div>
          </li>`;
    }).join('');
}

function templateSelecaoVeiculo() {    
    formUser.find('.box-selecao-veiculo').html(`
        <div class="row justify-content-between align-items-center">
            <div class="ml-2">
                <button id="btn-sel-veiculo" class="btn btn-secundaria my-2">Escolher o Veículo</button>    
            </div>    
        </div>         
    `);
    formUser.find('#btn-sel-veiculo').on('click', function (e) {
        e.preventDefault();
        chamaSwalQueue(escolhendoVeiculo(getVeiculos(), {showCloseButton: true}), function (res, r) {
            if (res.dismiss === 'cancel') {
                clearVeiculo();
            }
        });
    });
}

function setarInformacoesVeiculo() {
    const veiculo = getVeiculo();
    const campoIndicador = formUser.find('[name="indicador"]');
    if (campoIndicador.length) {
        const usuario = getUsuario();
        usuario && campoIndicador.val(usuario.indicador).change();
    }
    const tipo_veiculo = getTipoVeiculo().filter((item) => item.id === getSelTipoVeiculo())[0];
    formUser.find('[name="tipo_veiculo"]').select2().val(veiculo ? veiculo.tipo_veiculo : tipo_veiculo.id).change();    
    formUser.find('[name="placa"]').val(veiculo.placa ? veiculo.placa.toUpperCase() : '');
    formUser.find('[name="renavam"]').val(veiculo.renavam ? veiculo.renavam : '');
    formUser.find('[name="consulta_fipe"]').val([(veiculo.consulta_fipe ? veiculo.consulta_fipe : getSelConsultaFipe())]);
    if (veiculo && veiculo.valor) {
        formUser.find('[name="valor"]').val(veiculo.valor.substring(3));        
    }
    exibirValor((veiculo.consulta_fipe ? veiculo.consulta_fipe : getSelConsultaFipe()));
}

function setarInformacoesUsuario() {
    const usuario = getUsuario();
    formUser.find('[name="email"]').val(usuario.email);
    formUser.find('[name="razao_social"]').val(usuario.razao_social);
    formUser.find('[name="cnpj"]').val(usuario.cnpj).blur();
    formUser.find('[name="celular"]').val(usuario.celular);
    formUser.find('[name="cep"]').val(usuario.cep).blur();
}

function verificaValorProduto() {
    const consultor = getConsultor();
    if (config.rapido && consultor) {
        crmLead.callLead(function () {
            buscarServicos();
        }, function (erro) {
            console.log(erro);
            swal('Erro', 'Ocorreu um erro ao salvar o Lead', 'error');
        });
    } else {
        buscarServicos();
    }
}

function buscarServicos() {
    const coresTema = getCoresTema();
    const veiculo = getVeiculo();
    const marca = formUser.find('[name="marca"]').val();
    const tipo_veiculo = formUser.find('[name="tipo_veiculo"]').val();
    produtosService.getListaServicos({tipo_veiculo,
        valor: textToNumber(veiculo.valor), marca, ano_modelo: veiculo.ano_modelo}, function (resp) {
        const produto = resp.produtos[0];
        setProduto(produto);
        if (!config.agrupar_plano) {
            if (resp.produtos.length === 1) {
                if (produto.editavel) {
                    single.init(numeroEtapa + 1).eventNext();
                    removerEtapa('#cotacao');
                    listenEvent(numeroEtapa + 1);
                } else if (!config.rapido) {
                    const acessorios = [];
                    setAcessorios(acessorios);
                    planoService.salvarPlano({
                        id_produto: produto.id,
                        id_veiculo: veiculo.id_veiculo,
                        acessorios},
                            function (res) {
                                setPlano(res);
                                veiculoService.patchVeiculo({prim_mens: res.prim_mens, valor_mens: res.valor_mens, id_plano: res.id_plano, acessorios});
                                listenEvent(numeroEtapa + 1);
                            }, function (erro) {
                        console.log(erro.status);
                        swal('Erro', 'Ocorreu um erro ao salvar o plano', 'error');
                    });
                } else {
                    listenEvent(numeroEtapa + 1);
                }
            } else {
                cotacao.init(numeroEtapa + 1).eventNext();
                listenEvent(numeroEtapa + 1);
            }
        } else {  
            single.init(numeroEtapa + 1).eventNext();
            listenEvent(numeroEtapa + 1);
        }
    }, function (erro) {
        console.error(erro.responseText);
        if (erro.status === 404) {
            swal({title: 'Atenção!', html: `
                <p>${erro.responseJSON.erro}</p>
                <p>Mas não se preocupe, que posteiramente entraremos contato.</p>`,
                confirmButtonColor: coresTema.cor_secundaria,
                type: 'warning'});
        } else {
            swal('Erro', 'Ocorreu ao buscar os serviços', 'error');
        }
    });
}

function selecioneUmaMarca(e) {
    e.preventDefault();
    const t = $(this);
    const campoModelo = formUser.find('[name="modelo"]');
    const campoAno = formUser.find('[name="ano_modelo"]');
    if (t.val()) {
        veiculoService.getModeloOuAno(t.val(), function () {
            const veiculo = getVeiculo();
            const anos = '<option value="">Selecione o ano do modelo do veiculo</option>' + (getAnos().map(function (ano) {
                return `<option value="${ano.Value}">${ano.Label}</option>`;
            }).join(''));
            campoAno.html(anos).select2();
            if (veiculo && veiculo.ano_modelo) {
                const combustivel = veiculo.combustivel && Number.isInteger(veiculo.combustivel) ? " " + veiculo.combustivel : '';
                const ano_modelo = ("" + veiculo.ano_modelo).includes('32000') ? 'Zero km' : veiculo.ano_modelo;
                selecionaOption(campoAno, (ano_modelo + combustivel));
            }
        }, function (err) {
            const erro = err.responseJSON;
            if (erro && erro.erro === 2) {
                veiculoService.alertaErroFipe();
            } else {
                swal('Erro', 'Ocorreu um erro ao listar o Modelo ou Ano', 'error');
            }
        });
    } else {
        campoModelo.html('<option value="">Selecione um Modelo</option>');
        campoAno.html('<option value="">Selecione um Ano</option>');
    }
}

export function selecioneUmAno(e) {
    e.preventDefault();
    const t = $(this);
    const marca = formUser.find('[name="marca"]').val();
    const campoModelo = formUser.find('[name="modelo"]');
    const campoAno = formUser.find('[name="ano_modelo"]');
    if (t.val() && marca) {
        veiculoService.getModeloPorAno(marca, t.val(), function () {
            const veiculo = getVeiculo();
            const modelos = '<option value="">Selecione um Modelo</option>' + (getModelos().map(function (modelo) {
                return `<option value="${modelo.Value}">${modelo.Label}</option>`;
            }).join(''));
            campoModelo.html(modelos).select2();
            if (veiculo && veiculo.modelo) {
                selecionaOption(campoModelo, ("" + veiculo.modelo));
            }
        }, function (err) {
            const erro = err.responseJSON;
            if (erro && erro.erro === 2) {
                veiculoService.alertaErroFipe();
            } else {
                swal('Erro', 'Ocorreu um erro os modelos do veiculos', 'error');
                campoModelo.html('<option value="">Selecione um Modelo</option>');
            }
        });
    } else {
        campoAno.html('<option value="">Selecione um Ano</option>');
    }
}

export function selecioneUmModelo(e) {
    e.preventDefault();
    const t = $(this);
    const marca = formUser.find('[name="marca"]').val();
    const ano = formUser.find('[name="ano_modelo"]').val();
    const valor = formUser.find('[name="valor"]').val();
    if (t.val() && marca && ano) {
        veiculoService.getInfoVeiculo(marca, ano, t.val(), valor, function () {
            const veiculo = getVeiculo();
            if (!veiculo.id) {
                swal('Atenção', 'Antes de prosseguir verifique se os dados do seu veículo estão corretos!', 'warning');
            }
            btnProx.show();
        }, function (err) {
            console.log(err);
            const erro = err.responseJSON;
            if (erro && erro.erro === 2) {
                veiculoService.alertaErroFipe();
            } else {
                swal('Erro', 'Ocorreu um erro ao listar os anos', 'error');
            }
        });
    } else {
        veiculoService.patchVeiculo({marca: '', modelo: '', ano_modelo: '',
            codigo_fipe: '', combustivel: '', sigla_combustivel: '',
            mes_referencia: '', valor: ''
        });
    }
}

function clearVeiculo() {
    setVeiculo({});
    const tipo_veiculo = getTipoVeiculo().filter((item) => item.id === getSelTipoVeiculo())[0];
    formUser.find('.box-veiculo').removeClass('hide');
    formUser.find('[name="tipo_veiculo"]').select2().val(tipo_veiculo.id).change();    
    formUser.find('[name="placa"]').val('');
    formUser.find('[name="renavam"]').val('');
    formUser.find('[name="consulta_fipe"]').val([0]);
    exibirValor(0);
    formUser.find('[name="valor"]').val('');
    formUser.find('[name="marca"]').val('').change();
}