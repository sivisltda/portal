import { textToNumber } from "./../../../core/function.js";

import {geraMenu} from "./../../../shared/menu-seguro.js";

import store from './../../../store/app.js';
import * as produtoService from './../../../service/produto.service.js';


const {getVeiculo, getProduto, getAcessorios} = store.getters;
const {setAcessorios} = store.setters;

let padroes = [];

 export function iniciaCoberturas(listaCoberturas, listaMenuInfo) {
    const veiculo = getVeiculo();
    const produto = getProduto();
    const dados =  {tipo_veiculo: veiculo.tipo_veiculo, produto_id: produto.id, valor: textToNumber(veiculo.valor)};
    produtoService.getListaCoberturas(dados, function (res) {
        padroes = produtoService.getItemsPadroes();
        listaCoberturas.html(templateCoberturas(res));
        geraMenu(listaMenuInfo);
        selecionaCoberturas(listaCoberturas);        
    }, function (erro) {
        console.log(erro.responseText);
        swal('Erro', 'Ocorreu um erro de buscar as coberturas', 'error');
    });
    listaCoberturas.on('change', 'input[name="cobertura[]"]', function () {
        const t = $(this);
        const parent = t.data('parent');
        const isPadrao = padroes.findIndex(function(item) {
            return item.id == t.val();
        });
        if (textToNumber(t.data('valor')) > 0 || isPadrao > -1) {
            if (t.data('type') === 'single') {
                Array.from(listaCoberturas.find('*[data-parent="'+parent+'"]')).forEach(function (item) {
                    if (item.value !== t.val()) {
                        item.checked = false;
                    }
                });
            }
        }
        if (listaCoberturas.find('*[data-parent="'+parent+'"]:checked').length === 0) {
            const itemPadrao = padroes.find(function(item) {
                return item.cobertura == parent;
            });
            itemPadrao && listaCoberturas.find(`#cobertura-${itemPadrao.id}`).prop('checked', 'checked');
        }
        setAcessorios(filtraItens(listaCoberturas, produto)); 
        geraMenu(listaMenuInfo);
    });
}

function filtraItens(listaCoberturas, produtoEscolhido) {
    return  Array.from(listaCoberturas.find('input[name="cobertura[]"]:checked'))
        .filter(function (item) {
            return !(produtoEscolhido.servicos.indexOf(parseInt(item.value)) > -1 || padroes.indexOf(item.value) > -1);
        }).map(function (item) {
            const element   = $(item);
            const valor     = element.data('valor');
            const texto     = element.parent().prev('.nome-cobertura').text();
            const id        = element.val();
            const parent    = element.data('parent');
            const padrao    = element.data('padrao');
            return {
                id_servico_acessorio: id,
                descricao: texto,
                preco_venda: valor,
                parent,
                padrao
            };
        })
}

function selecionaCoberturas(listaCoberturas) {
    const acessorios = getAcessorios();
    listaCoberturas.find('input[name="cobertura[]"]').prop('checked', false);
    const idPadroes = padroes.map(function(p) { return p.id});
    idPadroes.forEach(function(p) {
        listaCoberturas.find("#cobertura-"+p).prop('checked', true).change();
    });

    if (acessorios.length) {
        acessorios.filter(function(item) {
            return !idPadroes.includes(item.id_servico_acessorio); 
        }).forEach(function(item) {
            listaCoberturas.find("#cobertura-" + item.id_servico_acessorio).prop('checked', true).change();
        });
    } 
}


function templateCoberturas(coberturas) {
    return coberturas.map(function (cobertura) {
        return `<li class="px-3 py-3">
            <div class="accordion-header" id="heading${cobertura.id}">
                <a href="#collapse${cobertura.id}" class="custom-accordion-title d-flex justify-content-between pt-2 pb-2" 
                data-toggle="collapse" aria-expanded="true" aria-controls="collapse${cobertura.id}">
                    <div class="row w-100">
                        <div class="col-6 text-right pr-0">
                            <h5 class="accordion-titulo texto-secundaria">${cobertura.questao}</h5>
                        </div>
                        <div class="offset-1 col-5">
                            <span class="float-left" style="margin-left: 3rem;"><i class="mdi mdi-chevron-down texto-secundaria accordion-arrow"></i></span>
                        </div>
                    </div>
                </a>
            </div>
            <div id="collapse${cobertura.id}" class="accordion-body collapse show" aria-labelledby="heading${cobertura.id}">
                <ul id="coberturas" class="list-coberturas assistencia">
                    <li>
                        ${cobertura.descricao ? `<div class="row">
                            <div class="offset-1 col-10 text-center mb-2">
                                <h6>${cobertura.descricao}</h6>
                            </div>
                        </div>` : ``}                        
                        <ul>${templateProtecao('cobertura', cobertura, cobertura.id, false)}</ul>
                    </li>
                </ul>
            </div>
        </li>`;
    }).join('');
}

function templateProtecao(name, protecao, parent, titulo = true) {
    if(protecao.items) {
        return protecao.items.map(function (p) {
            return `${p.so_padrao === 1 && p.padrao === 0 ? '' : `<li>
                    <div class="row justify-content-between">
                        <div class="col-6 nome-cobertura text-right">
                            ${titulo ? "<h6>" + p.nome + "</h6>" : p.nome}
                        </div>
                        <div class="offset-1 col-5 d-flex justify-content-start action-service">
                            <input type="checkbox" id="${name}-${p.id}" ${p.check ? "checked" : ''} 
                            name="${name}[]" value="${p.id}" data-type="${protecao.tipo}" data-parent="${parent}"
                            data-valor="${p.valor}" data-switch="bool" data-padrao="${p.padrao}">
                            <label for="${name}-${p.id}" data-on-label="sim" data-off-label="não"></label>
                        </div>
                    </div>
                </li>`}`;
        }).join('');
    }
    return '';
}