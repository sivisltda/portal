import store from './../../../store/app.js';

const { getParentesco } = store.getters;

let regras = {};

export function getFormDependente(i, parentesco = false) {
    const listParentescos = getParentesco();
    const adicional = [
        {
            type: "text",
            size: "col-md-8",
            id: "nome_adicional"+i,
            label: "Nome Completo *:",
            name: "nome",
            required: true,
            attributes: {
                class: "form-control",
                placeholder: "Nome do dependente"
            }
        },  {
            type: "date",
            size: "col-md-4",
            id: "data_nascimento_adicional"+i,
            label: "Data Nascimento *:",
            name: "data_nascimento",
            required: true,
            attributes: {
                class: "form-control mask data_nascimento_dependente",
                placeholder: "DD/MM/AAAA",
                min: '1900-01-01',
                max: '3000-12-31'
            }
        }, {
            type: 'text',
            size: "col-md-4",
            id: "cnpj_adicional"+i,
            label: "CPF *:",
            name: "cnpj",
            required: true,
            attributes: {
                class: "form-control cpf_cnpj mask",
                placeholder: "Digite seu CPF",
                "data-toggle": "input-mask",
                "data-mask-format": "000.000.000-00"
            }
        },  {
            type: "email",
            size: "col-md-4",
            id: "email_adicional"+i,
            label: "E-mail: ",
            name: "email",
            required: true,
            attributes: {
                class: "form-control email",
                placeholder: "Digite seu e-mail"
            }
        }, {
            type: "tel",
            size: "col-md-4",
            id: "cel_adicional"+i,
            label: "Celular: ",
            name: "celular",
            required: true,
            attributes: {
                class: "form-control mask",
                placeholder: "(00) 00000-0000",
                "data-toggle": "input-mask",
                "data-mask-format": "(00) 00000-0000"
            }
        }, {
            type: "select",
            size: "col-md-4",
            id: "sexo_adicional"+i,
            label: "Sexo *:",
            name: "sexo",
            options: {
                "M": "Masculino",
                "F": "Feminino"
            },
            required: true,
            attributes: {
                class: "form-control",
                placeholder: "Selecione"
            }
        }, {
            type: "select",
            size: "col-md-4",
            id: "estado_civil_adicional"+i,
            label: "Estado Civil *:",
            name: "estado_civil",
            options: {
                "SOLTEIRO(A)": "SOLTEIRO(A)",
                "CASADO(A)": "CASADO(A)",
                "SEPARADO(A)": "SEPARADO(A)",
                "DIVORCIADO(A)": "DIVORCIADO(A)",
                "VIÚVO(A)": "VIÚVO(A)"
            },
            required: true,
            attributes: {
                class: "form-control",
                placeholder: "Selecione"
            }
        }
    
    ];

    if(parentesco) {
        adicional.push({
            type: "select",
            size: "col-md-4",
            id: "parentesco_adicional"+i,
            label: "Grau de parentesco *:",
            name: "parentesco",
            options: listParentescos,
            required: true,
            attributes : {
                class: "form-control parentesco",
                placeholder: "Selecione"
            }
        });
    }

    return adicional;

}


export function rulesDependente(empresarial) {
    const rules = {
        nome: {
            rule: "required|min:2",
            message: { required: "Nome é um campo obrigatório", min: "mínimo :min caracteres" }
        }, data_nascimento: {
            rule: "required|date",
            message: { required: "Data de Nascimento é um campo obrigatório", date: "Data Inválida" }
        }, cnpj: {
            rule: "required|cpf",
            message: { required: "CPF. é um campo obrigatório", cpf: "CPF inválido" }
        }, sexo: {
            rule: "required",
            message: { required: "Sexo é um campo obrigatório" }
        }, estado_civil: {
            rule: "required",
            message: { required: "Estado Cívil é um campo obrigatório" }
        }
    };

    if(!empresarial) {
        rules.parentesco = {
            rule: "required",
            message: { required: "Parentesco é um campo obrigatório"}
        };
    }
    setRules(rules);
} 


export function setRules(rules) {
    regras = rules;
}

export function getRules() {
    return regras;
}