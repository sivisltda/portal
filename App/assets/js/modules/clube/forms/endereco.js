
import store from './../../../store/app.js';

const { getEstados, getCidades } = store.getters;

export function getFormAddress(sufix_name = null, sufix_id = null) {
    const optionsEstados = getEstados().reduce(function(acc, item){
        acc[item.estado_codigo] = item.estado_nome;
        return acc;
    }, {});
    const optionsCidades = getCidades().reduce(function(acc, item) {
        acc[item.cidade_codigo] = item.cidade_nome;
        return acc;
    }, {});
    return [
        {
            type      : "text",
            size      : "col-md-8",
            id        : "cep"+(sufix_id ? '_'+sufix_id : ''),
            label     : "CEP *:",
            name      : "cep"+(sufix_name ? '_'+sufix_name : ''),
            required  : true,
            attributes : {
                class             : "form-control mask",
                placeholder       : "00000-000",
                "data-toggle"       : "input-mask",
                "data-mask-format"  : "00000-000"
            }
        }, {
            type      : "select",
            size      : "col-md-8",
            id        : "estado"+(sufix_id ? '_'+sufix_id : ''),
            label     : "Estado *:",
            name      : "estado"+(sufix_name ? '_'+sufix_name : ''),
            options   : optionsEstados,
            required  : true,
            attributes : {
                class         : "form-control",
                placeholder   : "Selecione um estado"
            }
        }, {
            type      : "select",
            size      : "col-md-8",
            id        : "cidade"+(sufix_id ? '_'+sufix_id : ''),
            label     : "Cidade *:",
            name      : "cidade"+(sufix_name ? '_'+sufix_name : ''),
            options   : optionsCidades,
            required  : true,
            attributes : {
                class         : "form-control",
                placeholder   : "Selecione uma cidade"
            }
        }, {
            type      : "text",
            size      : "col-md-8",
            id        : "bairro"+(sufix_id ? '_'+sufix_id : ''),
            label     : "Bairro *:",
            name      : "bairro"+(sufix_name ? '_'+sufix_name : ''),
            required  : true,
            attributes : {
                class         : "form-control",
                placeholder   : "Bairro"
            }
        }, {
            type      : "text",
            size      : "col-md-8",
            id        : "endereco"+(sufix_id ? '_'+sufix_id : ''),
            label     : "Endereço *:",
            name      : "endereco"+(sufix_name ? '_'+sufix_name : ''),
            required  : true,
            attributes : {
                class         : "form-control",
                placeholder   : "Rua, Avenida... "
            }
        }, {
            type      : "text",
            size      : "col-md-8",
            id        : "numero"+(sufix_id ? '_'+sufix_id : ''),
            label     : "Número *:",
            name      : "numero"+(sufix_name ? '_'+sufix_name : ''),
            required  : true,
            attributes : {
                class         : "form-control",
                placeholder   : "Número"
            }
        }, {
            type      : "text",
            size      : "col-md-8",
            id        : "complemento"+(sufix_id ? '_'+sufix_id : ''),
            label     : "Complemento:",
            name      : "complemento"+(sufix_name ? '_'+sufix_name : ''),
            required  : false,
            attributes : {
                class         : "form-control",
                placeholder   : "Seu complemento"
            }
        }
    ];
}

export function rulesEndereco(sufix_id = null) {
    return {
        ["cep"+(sufix_id ? '_'+sufix_id : '')]: {
            rule: "required",
            message: { required: "CEP é um campo obrigatório" }
        }, ["numero"+(sufix_id ? '_'+sufix_id : '')]: {
            rule: "required",
            message: { required: "Número é um campo obrigatório" }
        }, ["endereco"+(sufix_id ? '_'+sufix_id : '')]: {
            rule: "required",
            message: { required: "Endereço é um campo obrigatório" }
        }, ["bairro"+(sufix_id ? '_'+sufix_id : '')]: {
            rule: "required",
            message: { required: "Bairro é um campo obrigatório" }
        }, ["cidade"+(sufix_id ? '_'+sufix_id : '')]: {
            rule: "required",
            message: { required: "Cidade é um campo obrigatório" }
        }, ["estado"+(sufix_id ? '_'+sufix_id : '')]: {
            rule: "required",
            message: { required: "Estado é um campo obrigatório" }
        }
    }
}