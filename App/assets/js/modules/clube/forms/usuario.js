import * as enderecoForm from './endereco.js';
import store from '../../../store/app.js';

const { getProcedencia } = store.getters;


export function getFormDefault(formRoot, events = []) {
    return {
        type: 'row',
        root: formRoot,
        events,
        title: 'Informações Pessoais',
        inputs: [
            {
                type: 'email', size: 'col-md-8', id: 'email', label: 'Qual seu email?',
                name: 'email', required: true, visibility: true,
                attributes: {
                    class: 'form-control',
                    placeholder: 'Seu e-mail'
                }
            }, {
                type: "text", size: "col-md-8", id: "cnpj", label: "CPF/CNPJ *:",
                name: "cnpj", required: true, visibility: false,
                attributes: {
                    class: "form-control cpf_cnpj mask",
                    placeholder: "Seu CPF ou CNPJ",
                    "data-toggle": "input-mask",
                    "data-type": "cpf-cnpj"
                }
            }
        ],
        info: [
            "<p>Atenção: Email e CPF/CNPJ não poderão ser alterados posteriamente. Seus dados são muito importantes " +
            "para a identificação e validação do seu plano.</p>"
        ],
        rules: {
            email: {
                rule: "required|email",
                message: {
                    required: "Email é um campo obrigatório",
                    email: "Campo deve ser do tipo email"
                }
            }, cnpj: {
                rule: "required|cpf",
                message: {
                    required: "CPF é um campo obrigatório",
                    cpf: "CPF inválido"
                }
            }
        }            
    };
}

export function formDados(formRoot, events = []) {   
    const procedencias = getProcedencia() ? getProcedencia().reduce(function(acc, item) {
        acc[item.id_procedencia] = item.nome_procedencia;
        return acc;
    }, {}): {};
    return {
        type: 'row',
        root: formRoot,
        events,
        group: [
            {
                title: 'Dados do Contratante',
                inputs: [
                    {
                        type: "text",
                        size: "col-md-8",
                        id: "razao_social",
                        label: "Nome *:",
                        name: "razao_social",
                        required: true,
                        visibility: true,
                        attributes: {
                            class: "form-control",
                            placeholder: "Digite seu nome"
                        }
                    },{
                        type: "date",
                        size: "col-md-8",
                        id: "data_nascimento",
                        label: "Data de Nascimento *:",
                        name: "data_nascimento",
                        required: true,
                        attributes: {
                            class: "form-control data_nascimento",
                            placeholder: "DD/MM/AAAA",
                            min: '1900-01-01',
                            max: '3000-12-31'
                        }
                    }, {
                        type: "select",
                        size: "col-md-8",
                        id: "sexo",
                        label: "Sexo *:",
                        name: "sexo",
                        options: {
                            "M": "Masculino",
                            "F": "Feminino"
                        },
                        required: true,
                        attributes: {
                            class: "form-control",
                            placeholder: "Selecione"
                        }
                    }, {
                        type: "select",
                        size: "col-md-8",
                        id: "estado_civil",
                        label: "Estado Civil *:",
                        name: "estado_civil",
                        options: {
                            "SOLTEIRO(A)": "SOLTEIRO(A)",
                            "CASADO(A)": "CASADO(A)",
                            "SEPARADO(A)": "SEPARADO(A)",
                            "DIVORCIADO(A)": "DIVORCIADO(A)",
                            "VIUVO(A)": "VIÚVO(A)"
                        },
                        required: true,
                        attributes : {
                            class: "form-control",
                            placeholder: "Selecione"
                        }
                    }, {
                        type: "tel",
                        size: "col-md-8",
                        id : "celular",
                        label: "Celular *:",
                        name: "celular",
                        required: true,
                        attributes : {
                            class: "form-control mask",
                            placeholder: "(00) 00000-0000",
                            "data-toggle": "input-mask",
                            "data-mask-format": "(00) 00000-0000"
                        }
                    }, {
                        type: "switch",
                        size: "col-md-8",
                        id: "nfe_iss",
                        label: "ISS Retido?",
                        name: "nfe_iss",
                        required: true,
                        value: 0,
                        attributes: {
                            label: {
                                on: "sim",
                                off: "não"
                            }
                        }                        
                    }, {
                        type: "select",
                        size: "col-md-8",
                        id: "procedencia",
                        label: "Como conheceu a empresa? *:",
                        name: "procedencia",
                        required: true,
                        options: procedencias,
                        attributes: {
                            class: "form-control",
                            placeholder   : "Selecione uma procedência"
                        }                        
                    }                    
                ]
            }, {
                title: 'Dados de Endereço',
                inputs: enderecoForm.getFormAddress()
            }
        ],
        rules: {
            razao_social: {
                rule: "required|min:2",
                message: { required: "Nome é um campo obrigatório", min: "mínimo :min caracteres" }
            },
            data_nascimento: {
                rule: "required|date",
                message: { required: "Data de Nascimento é um campo obrigatório" }
            },
            sexo: {
                rule: "required",
                message: { required: "Sexo é um campo obrigatório" }
            },
            estado_civil: {
                rule: "required",
                message: { required: "Estado Civil é um campo obrigatório" } 
            },
            celular: {
                rule: "required",
                message: { required: "Celular é um campo obrigatório" }
            },
            procedencia: {
                rule: "required",
                message: { required: "Procedencia é um campo obrigatório" }
            },
            ...enderecoForm.rulesEndereco()
        }   
       
    }
}

export function formDadosEmpresarial(formRoot, events = []) {
    const procedencias = getProcedencia() ? getProcedencia().reduce(function(acc, item) {
        acc[item.id_procedencia] = item.nome_procedencia;
        return acc;
    }, {}): {};
    return {
        type: 'row',
        root: formRoot,
        events,
        group: [
            {
                title: "Dados do Contratante",
                inputs: [
                    {
                        type: "text",
                        size: "col-md-8",
                        id: "razao_social",
                        label: "Razão Social *:",
                        name: "razao_social",
                        visibility: true,
                        required: true,
                        attributes: {
                            class: "form-control",
                            placeholder: "Digite a Razão Social"
                        }
                    },{
                        type: "date",
                        size: "col-md-8",
                        id: "data_nascimento",
                        label: "Data de Fundação *:",
                        name: "data_nascimento",
                        required: true,
                        attributes: {
                            class: "form-control data_nascimento",
                            placeholder: "DD/MM/AAAA",
                            min: '1900-01-01',
                            max: '3000-12-31'
                        }
                    }, {
                        type: "switch",
                        size: "col-md-8",
                        id: "regime_tributario",
                        label: "Isenção de I. E.?",
                        name: "regime_tributario",
                        required: true,
                        value: 1,
                        attributes: {
                            label: {
                                on: "sim",
                                off: "não"
                            }
                        }
                    }, {
                        type: "text",
                        size: "col-md-8",
                        id: "inscricao_estadual",
                        label: "Inscrição Estadual :",
                        name: "inscricao_estadual",
                        required: true,
                        attributes: {
                            class: "form-control",
                            placeholder: "Digite a inscrição estadual"
                        }
                    }, {
                        type: "text",
                        size: "col-md-8",
                        id: "contato",
                        label: "Responsável *:",
                        name: "contato",
                        required: true,
                        attributes: {
                            class: "form-control",
                            placeholder: "Nome do Responsável"
                        }
                    }, {
                        type: "tel",
                        size: "col-md-8",
                        id: "telefone",
                        label: "Telefone Fixo *:",
                        name: "telefone",
                        required: true,
                        attributes: {
                            class: "form-control mask",
                            placeholder: "(00) 0000-0000",
                            "data-toggle": "input-mask",
                            "data-mask-format": "(00) 0000-0000"
                        }
                    }, {
                        type: "tel",
                        size: "col-md-8",
                        id: "celular",
                        label: "Celular *:",
                        name: "celular",
                        required: true,
                        attributes: {
                            class: "form-control mask",
                            placeholder: "(00) 00000-0000",
                            "data-toggle": "input-mask",
                            "data-mask-format": "(00) 00000-0000"
                        }
                    }, {
                        type: "switch",
                        size: "col-md-8",
                        id: "nfe_iss",
                        label: "ISS Retido?",
                        name: "nfe_iss",
                        required: true,
                        value: 0,
                        attributes: {
                            label: {
                                on: "sim",
                                off: "não"
                            }
                        }                        
                    }, {
                        type: "select",
                        size: "col-md-8",
                        id: "procedencia",
                        label: "Como conheceu a empresa? *:",
                        name: "procedencia",
                        required: true,
                        options: procedencias,
                        attributes: {
                            class: "form-control"
                        }                                                
                    }
                ]
            }, {
                title: 'Dados de Endereço',
                inputs: enderecoForm.getFormAddress()
            }
        ], 
        rules: {
            razao_social: {
                rule: "required|min:2",
                message: { required: "Razão Social é um campo obrigatório", min: "mínimo :min caracteres" }
            },
            data_nascimento: {
                rule: "required|date",
                message: { required: "Data de Fundação é um campo obrigatório" }
            },            
            contato: {
                rule: "required|min:2",
                message: { required: "Responsável é um campo Obrigatório", min: "mínimo :min caracteres" }
            },
            celular: {
                rule: "required",
                message: { required: "Celular é um campo obrigatório" }
            },
            procedencia: {
                rule: "required",
                message: { required: "Procedencia é um campo obrigatório" }
            },
            ...enderecoForm.rulesEndereco()

        }
        
    }
}
