import { adicionarEtapaEntrePeriodo, eventEtapas, verificaEtapa, eventProximo, listenEvent, 
    incrementarEtapa, getNumeroEtapa, verificaEAdicionaTemplateTab, getApp } 
    from './../../../core/route.js';

import { textToNumber, animaNumero, numberFormat, valorDesconto } from './../../../core/function.js';
    
import { getConfig as configFunc } from './../../../config/config.js';

import store from './../../../store/app.js';    

import * as regraDependentes from './../../../shared/regraDependentes.js';
import * as infoMenuLateral from './../shared/infoMenuLateral.js';
import * as tipoPagamento from './../../../shared/pagamento.js';

const { getPlano, getIdParcela, getDependentes } = store.getters;
const { setTransacao }  = store.setters;

let numeroEtapa             = null;
let app                     = null;
let config                  = null;
let btnProx                 = null;
let btnAnt                  = null;
let tabPanel                = null;
let tabPagamento            = null;

export const configEtapa = {
    id: '#pagamento',
    title: 'Pagamento',
    icon: 'mdi-account-circle',
    tooltip: 'Pagamento'
};


export function init(posicao) {
    app           = getApp();
    config        = configFunc();
    tabPanel      = app.find('#main');
    btnProx       = app.find('#btnProx');
    btnAnt        = app.find('#btnAnt');
    numeroEtapa = posicao ? posicao : incrementarEtapa();
    if (!posicao) {
        eventEtapas(numeroEtapa, configEtapa, iniciaTela);
    } else {
        if (!verificaEtapa(configEtapa.id)) {
            adicionarEtapaEntrePeriodo(numeroEtapa, configEtapa, iniciaTela);
        }
    } 
    verificaEAdicionaTemplateTab(tabPanel, configEtapa.id, template(configEtapa.id.replace('#','')));
    return this;     
}

export function iniciaTela() {
    numeroEtapa = getNumeroEtapa(configEtapa.id) || numeroEtapa;
    tabPagamento = tabPanel.find(configEtapa.id);
    montarTabPagamento();
    atualizaInfo();
    app.addClass('plano-fixo');
    btnAnt.show();
    btnProx.hide();    
}

function template(id) {
    return `<div class="tab-pane limite py-3 bg-white" id="${id}">
        <div class="container-fluid">   
            <div class="row">
                <div class="col-xl d-print-none d-flex container-info">
                <div class="row w-100 d-flex flex-column">
                    <div class="container-titulo-principal mb-4">
                        <h3 class="text-center texto-secundaria">${configEtapa.title}</h3>
                    </div>
                    <div class="col-12 flex-1" id="tipo-pagamento"></div>
                </div>
            </div>
                <div class="menu-right info-valores-plano"></div>
            </div>            
        </div>
    </div>`;
}

export function eventNext() {
    eventProximo(numeroEtapa, function() {
        listenEvent(numeroEtapa+1);
    });
}

function montarTabPagamento() {
    const containerPagamento = tabPagamento.find('#tipo-pagamento');
    const dadosValores = tipoPagamento.makeValores();
    const dadosDependentes = makeDependente(dadosValores, 1);
    containerPagamento.html(tipoPagamento.templatePagamento(dadosDependentes.valoresDependente));
    const plano = getPlano();
    tipoPagamento.geraEventos(containerPagamento);
    tipoPagamento.subscribe('changeParcela', configEtapa.id, function() {
        atualizaInfo();
    });
    tipoPagamento.subscribe('processoBoleto', configEtapa.id, function() {
        tipoPagamento.processarBoleto({ id_mens: plano.id_prim_mens, id_parcela: getIdParcela()}, function() {
            setTransacao('boleto');
            listenEvent(numeroEtapa + 1);  
        }, function(erro) {
            console.log(erro);
        });
    });
    tipoPagamento.subscribe('processoLink', configEtapa.id, function() {
        tipoPagamento.processarLink({ id_mens: plano.id_prim_mens, id_parcela: getIdParcela(), link: 1 }, function() {
            setTransacao('link');
            listenEvent(numeroEtapa + 1);  
        }, function(erro) {
            console.log(erro);
        });
    });
    tipoPagamento.subscribe('processoCartao', configEtapa.id, function(t) {
        tipoPagamento.processarCartao(t, { id_mens: plano.id_prim_mens, id_parcela: getIdParcela(), cartao: 1 }, 
        function() {         
            setTransacao('cartao'); 
            listenEvent(numeroEtapa + 1);            
        }, function(erro) {
            console.log(erro);
        });
    });
}

function atualizaInfo() {
    const dadosValores = tipoPagamento.makeValores();
    const dadosDependentes = makeDependente(dadosValores, 1);
    tipoPagamento.setValorPagar(tabPagamento, dadosValores, dadosDependentes.valoresDependente);
    atualizaValores(tabPagamento.find('.info-valores-plano'), dadosValores);
}

function atualizaValores(tabInfo, dadosValores) {
    const { parcela, parcelar, numParcela, produto, plano } = dadosValores;
    const dadosDependentes = makeDependente({produto, plano});
    const valorTotal = dadosValores.valorTotal(dadosDependentes.valoresDependente);
    const valor = parcelar ? valorTotal / numParcela : valorTotal;
    const msg = numParcela > 1 && parcelar ? 'mensal <br/>em '+numParcela+'x' : (!parcelar && numParcela !== 1 ? 'em '+numParcela+' meses' : 'mensal');
    const campos = { tp_pagamento: 'Tipo de Pagamento', parcelar: 'Parcelamento', dia_vencimento: 'Dia de Vencimento', dt_cadastro: 'Data de Cadastro', status: 'Status',
                    convenio: 'Convênio'};
    const templateMenu = templateMenuDireito(produto);
    tabInfo.show().html(templateMenu);
    animaNumero(tabInfo.find('.container-valor-plano'), valor, msg);
    if (numParcela > 1 && parcelar) {
        tabInfo.find('.valor-plano-anual').text(`Valor Total = ${numberFormat(valorTotal, 1)}`);
    } else {
        tabInfo.find('.valor-plano-anual').text('');
    }
    tabInfo.find('.detalhes').html(Object.keys(campos).map(function(key) {
        if (plano.hasOwnProperty(key)) {
            return `<p><strong>${campos[key]}:</strong> ${plano[key]}</p>`;
        } else if(key === 'tp_pagamento') {
            return `<p><strong>${campos[key]}:</strong> ${ parcela.parcela < 2 ? 
            (parcela.parcela == -2 ? 'Pix Recorrente' : parcela.parcela == -1 ? 'Boleto Recorrente' : (parcela.parcela == 1 ? 'Boleto' : 'Cartão de Crédito Recorrente')) : 'Mensal'}</p>`;
        } else if (key === 'parcelar' && numParcela > 1) {
            return `<p><strong>${campos[key]}:</strong> ${plano.gerar_parc ? 'Sim': 'Não'}</p>`;
        } else if (key === 'convenio' && plano.convenio_descricao) {
            return `<p><strong>${campos[key]}:</strong> ${plano.convenio_descricao} 
            - <strong>Desconto:</strong> ${plano.convenio_tipo === 'P' ? textToNumber(plano.convenio_valor)+'%' : numberFormat(textToNumber(plano.convenio_valor), 1) } 
            - <strong>Válido Por:</strong> ${plano.convenio_meses} mes(es)</p>`;
        }
        return '';
    }).join(''));
}

function makeDependente(data, habilitaAdesao) {
    const {produto, plano} = data;
    const dependentes = getDependentes();
    const adesaoDependente = habilitaAdesao ? textToNumber(config.adesao_dependente) : 0;    
    const dependentesCobranca = produto.hasOwnProperty('dependentes') && produto.dependentes.find(function(d) { return textToNumber(d.valor) > 0;});
    const valoresDependente = !dependentesCobranca ? 0 : regraDependentes.valorDependentes(produto, plano, dependentes, adesaoDependente);
    return {
        dependentes,
        adesaoDependente,
        dependentesCobranca,
        valoresDependente
    };
}

function templateMenuDireito(produto) {
    const dependentes = getDependentes();
    const plano = getPlano();
    const desconto = valorDesconto(plano.convenio_tipo, plano.convenio_valor);
    const parcela = getIdParcela() ? produto.parcelas.find(function(p) {return p.id_parcela == getIdParcela();}) : produto.parcelas[0];
    const valorAdesaoUnico = plano.desconto_adesao ? desconto(textToNumber(produto.taxa_adesao)) : textToNumber(produto.taxa_adesao);
    const valorAdesaoDependente = dependentes.reduce(function(acc, d) {
        const regra = regraDependentes.getRegraDependente(produto.dependentes, d);
        if (textToNumber(config.adesao_dependente) && regra && textToNumber(regra.valor)) {
            acc += valorAdesaoUnico;
        }
        return acc;
    }, 0);
    const valorAdesao = !parcela.valor_zerado ? valorAdesaoUnico + valorAdesaoDependente : valorAdesaoDependente;
    const templateDependentes = function(dependentes) {
        return `<div class="regras-dependentes">
            <h5>Regras:</h5>
            <ul class="mt-1 ml-2" style="list-style: disc;">${regraDependentes.templateRegraDependentes(dependentes)}</ul>
        </div>`;
    };
    return `<div class="logo-cotacao d-print">
        <img src="${config.image}" alt="${config.nome}">
    </div>
    <div class="data-cotacao d-print">${config.time}</div>
    <div class="print-titulo">
        <h3 class="text-right">${config.rapido ? 'Cotação '+config.nome : 'Proposta de Adesão'}</h3>
    </div>
    ${config.consultor ? '<div class="nome-consultor">'+config.consultor.nome+'</div>' : ''}
    <h4>Valor do Plano</h4>
    <div class="container-valor-plano"></div>
    <div class="valor-plano-anual"></div>
    ${produto && textToNumber(produto.taxa_adesao) > 0 ?
     '<div class="container-valor-adesao mb-1">A ' + config.adm_msg_taxa_adesao + ' no valor de: '+numberFormat(valorAdesao, 1)+'</div>': ''}
    <div class="nome-produto">${produto ? produto.nome : ''}</div>
    <div class="detalhes"></div>
    ${infoMenuLateral.tabelaResumo(produto, plano, dependentes)}
    ${infoMenuLateral.tabelaPagamentoFinal(produto, plano, dependentes)}
    ${produto.hasOwnProperty('dependentes') && produto.dependentes.length ? templateDependentes(produto.dependentes) : ''}`;
}

function setPlanoValorDesconto(cupom, parcela, acrescimo) {
    const desconto = valorDesconto(cupom.convenio_tipo, cupom.convenio_valor);
    const numParcela = textToNumber(parcela.parcela) < 2 ? 1 : textToNumber(parcela.parcela);
    const diff = numParcela > 1 && cupom.convenio_meses ? numParcela - cupom.convenio_meses : 0;
    const valorMensal = textToNumber(parcela.valor_unico) / numParcela;   
}