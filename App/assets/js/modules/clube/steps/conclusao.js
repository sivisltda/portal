import { adicionarEtapaEntrePeriodo, eventEtapas, verificaEtapa, finalized, eventProximo, getNumeroEtapa, listenEvent,
        incrementarEtapa, verificaEAdicionaTemplateTab, getApp } from './../../../core/route.js';
import store from './../../../store/app.js';
import { getConfig as configFunc } from './../../../config/config.js';
import * as storageService from './../../../service/storage.service.js';

const {getDependentes, getTransacao} = store.getters;

let app = null;
let config = null;
let tabPanel = null;
let btnProx = null;
let btnAnt = null;
let numeroEtapa = null;

export const configEtapa = {
    id: '#conclusao',
    title: 'Conclusão',
    icon: 'mdi-account-circle',
    tooltip: 'Conclusão'
};

export function init(posicao) {
    app = getApp();
    config = configFunc();
    tabPanel = app.find('#main');
    btnProx = app.find('#btnProx');
    btnAnt = app.find('#btnAnt');
    numeroEtapa = posicao ? posicao : incrementarEtapa();
    if (!posicao) {
        eventEtapas(numeroEtapa, configEtapa, iniciaTela);
    } else {
        if (!verificaEtapa(configEtapa.id)) {
            adicionarEtapaEntrePeriodo(numeroEtapa, configEtapa, iniciaTela);
        }
    }
    verificaEAdicionaTemplateTab(tabPanel, configEtapa.id, template(configEtapa.id.replace('#', '')));
    return this;
}

export function iniciaTela() {
    numeroEtapa = getNumeroEtapa(configEtapa.id) || numeroEtapa;
    const tab = tabPanel.find(configEtapa.id);
    app.removeClass('plano-fixo');
    templateConclusao(tab);
    store.actions.clear();
    storageService.setItem(`loja-${config.contrato}`, store.actions.serialize());
    btnAnt.hide();
    btnProx.hide();
}

function template(id) {    
    return `<div class="tab-pane py-3 bg-white" id="${id}">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-7 mb-4">
                    <img src="${config.logo_conclusao}" class="img-fluid"/>
                </div>
                <div class="col-lg-5 text-center">
                    ${config.adm_msg_contratacao}
                </div>
            </div>            
        </div>
    </div>`;
}

function templateConclusao(tab) {
    /*tab.find('h3.titulo-conclusao').html('Parabéns, você fez <br/> uma ótima escolha!');
    tab.find('h4.subtitulo-conclusao').text('Seu pedido foi realizado com sucesso.');
    tab.find('.msg-detalhes').html(mensagemConclusao());*/
    storageService.removeItem(`loja-${config.contrato}`);
    finalized();
}

/*function mensagemConclusao() {
    let items = [];
    const transacao = getTransacao();
    const dependentes = getDependentes();
    if (transacao === 'boleto') {
        items.push(`<h5 class="texto-status ml-2 text-left texto-secundaria">Estamos aguardando a confirmação do pagamento do boleto!</h5>`);
    } else {
        items.push(`<h5 class="texto-status ml-2 text-left texto-secundaria">O cadastro foi efetuado com sucesso!</h5>`);
    }
    const msgsDependentes = dependentes.length ? 'e os seus dependentes receberão' : 'receberá';
    items.push(`<p>${transacao === 'boleto' ? 'Mediante a confirmação do pagamento do boleto, você ' + msgsDependentes + ' um email '
            + 'com informações para acessar a área do cliente, onde encontrará os detalhes da sua conta'
            : 'Você ' + msgsDependentes + ' um email com informações para acessar a área do cliente, onde encontrará os detalhes da sua conta'}</p>`);
    if (!(transacao === 'boleto')) {
        const {hostname, origin, pathname} = window.location;
        const url = origin + (hostname === 'localhost' ? pathname.substring(0, pathname.indexOf('/', 1)) : '') + '/loja/' + config.contrato + '/login';
        items.push(`<p class="texto-menor">Para acessar o painel da área do cliente e só clicar no botão abaixo</p>`);
        items.push(`<a href="${url}" class="my-3 btn btn-secundaria">Acessar a área do cliente</a>`);
    }
    return items.join('');
}*/

export function eventNext() {
    eventProximo(numeroEtapa, function () {
        listenEvent(numeroEtapa + 1);
    });
}