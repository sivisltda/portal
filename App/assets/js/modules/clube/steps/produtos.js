import { adicionarEtapaEntrePeriodo, eventEtapas, verificaEtapa, removerEtapa, eventProximo, 
    listenEvent, incrementarEtapa, verificaEAdicionaTemplateTab, getNumeroEtapa, getApp }
    from './../../../core/route.js';

import { textToNumber, formataNumero,ativarCarrousel, goStepCarrousel, numberFormat } from './../../../core/function.js';    

import * as tabDependentes from './dependente.js';

import * as produtoService from './../../../service/produto.service.js';
import * as planoService from './../../../service/plano.service.js';
import * as usuarioService from './../../../service/usuario.sevice.js';

import { getConfig as configFunc } from './../../../config/config.js';

import store from './../../../store/app.js';

const { getProdutos, getProduto, getPlano, getIdParcela, getTermos, getPlanos, getUsuario, getTipoVencimento, 
    getVencimentos, getDiaVencimento } = store.getters;
const { setTermos, setDiaVencimento, setProduto, setProrata } = store.setters;

let numeroEtapa         = null;

let config              = null;
let app                 = null;
let btnProx             = null;
let btnAnt              = null;
let tabPanel            = null;

let tabPanelProduto     = null;

export const configEtapa = {
    id: '#produtos',
    title: 'Produtos',
    icon: 'mdi-account-circle',
    tooltip: 'Produtos'
};

export function init(posicao) {
    app      = getApp();
    config   = configFunc();
    btnProx  = app.find('#btnProx');
    btnAnt   = app.find('#btnAnt');
    tabPanel = app.find('#main');
    numeroEtapa = posicao ? posicao : incrementarEtapa();
    if (!posicao) {
        eventEtapas(numeroEtapa, configEtapa, iniciaTela);
    } else {
        if (!verificaEtapa(configEtapa.id)) {
            adicionarEtapaEntrePeriodo(numeroEtapa, configEtapa, iniciaTela);
        }
    } 
    verificaEAdicionaTemplateTab(tabPanel, configEtapa.id, template(configEtapa.id.replace('#','')));
    return this;     
}

export function iniciaTela() {
    numeroEtapa = getNumeroEtapa(configEtapa.id) || numeroEtapa;
    tabPanelProduto = tabPanel.find(configEtapa.id);
    app.removeClass('plano-fixo');
    const usuario = getUsuario();
    const plano = getPlano(); 
    const produto = getProduto();
    if ((plano || produto) && getProdutos().length) {       
        (!produto || (plano && produto.id !== plano.id_produto)) && produtoService.setProdutoPorId(plano.id_produto);
        montaItemProduto();        
    } else {
        if (plano) {
            produtoService.getItemProduto({ id: plano.id_produto, tipoPessoa: usuario.juridico_tipo }, function() {
                montaItemProduto(); 
                   
            }, function(erro) {
                console.log(erro);
                swal('Erro', 'Ocorreu um erro ao buscar o produto', 'error').then(function() {
                    if (erro.status === 404) {
                        listenEvent(numeroEtapa-1);
                    }
                });
            });      
        } else {
            produtoService.getListaProdutos(usuario.juridico_tipo, function() {
                montaListaProdutos(); 
            }, function(erro) {
                console.log(erro);
                swal('Erro', 'Ocorreu um erro ao trazer os produtos', 'error');
            });
        }   
    }   
    btnAnt.show();
}

function template(id) {
    const produto = getProduto();
    return `<div class="tab-pane py-3 bg-white" id="${id}">
        <div class="tab-content">
            <div class="tab-pane${!produto ? ' active' : ''}" id="tab-lista-produto">
                <div class="container">
                    <h3 class="text-center text-uppercase texto-secundaria mb-4">Escolha seu plano</h3>
                    <div class="container-carrousel" id="lista-produto"></div>
                </div>
            </div>
            <div class="tab-pane${produto ? ' active' : ''}" id="tab-produto">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 mb-3" id="info-produto"></div>
                    </div>
                </div>    
            </div>
        </div>            
    </div>`;
}

export function eventNext() {
    eventProximo(numeroEtapa, function() {
        salvarPlano();
    });
}


function montaItemProduto() {
    btnProx.show();
    const tabItemProduto = tabPanelProduto.find('#tab-produto'); 
    const usuario = getUsuario();
    tabItemProduto.find('#info-produto').html(templateItemProduto());
    tabItemProduto.on('change', 'input[name="termo[]"]', changeTermos);
    tabItemProduto.on('change', '#data-vencimento', changeDataVencimento);
    tabItemProduto.find('#btn-voltar-produtos').on('click', clickVoltarProdutos)
    tabPanelProduto.find('#tab-lista-produto').removeClass('active');
    montarTemplateProrata(tabItemProduto);
    tabItemProduto.addClass('active');
    if (getPlano()) {
        tabPanel.find('input[name="termo[]"]:required').each(function(_, input) {
            input.checked = true;
        });
    }
    usuario && tabPanel.find('#termo-email').prop('checked', Number.parseInt(usuario.email_marketing));
    usuario && tabPanel.find('#termo-celular').prop('checked', Number.parseInt(usuario.celular_marketing));
}

function montaListaProdutos() {
    const listaProdutos = tabPanelProduto.find('#lista-produto');
    const tabItemProduto = tabPanelProduto.find('#tab-produto'); 
    const planosProdutos = getPlanos().map(function(plano) { return plano.id_produto; });
    const produtos = getProdutos().filter(function(p) {return !planosProdutos.includes(p.id)});
    const template = produtos.map(function(item){
        return templateProduto(item);
    }).join(''); 
    ativarCarrousel(
        listaProdutos, 
        template,
        {
            arrows: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            responsive: [
                {
                    breakpoint: 980,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        }
    );
    goStepCarrousel(listaProdutos, 'active');
    listaProdutos.find('.btn-seleciona').on('click', clickProduto);
    tabPanelProduto.find('#tab-lista-produto').addClass('active');
    tabItemProduto.removeClass('active');
}

function templateProduto(produto) {
    const dependentesCobranca = produto.hasOwnProperty('dependentes') && produto.dependentes.find(function(d) { return textToNumber(d.valor) > 0;});
    const mes = dependentesCobranca ? 'mês / <br/> dependentes' : 'mês';
    return `<div class="card bg-primaria text-white">
        <div class="card-header">
            <h4 class="my-0 font-weight-normal text-center">${produto.nome}</h4>
        </div>
        <div class="card-body">
            <h2 class="card-title texto-secundaria pricing-card-title">
                <div class="container-valor-plano${dependentesCobranca ? ' inclui-dependente': ''}">
                    ${formataNumero(produto.valor, mes)}
                </div>
            </h2>
            <div class="mt-3 texto-descricao">${produto.descricao}</div>
        </div>
        <div class="card-footer">
            <button class="btn btn-block text-uppercase btn-seleciona btn-secundaria" data-id="${produto.id}">Selecione</button>
        </div>
    </div>`;
}

function templateItemProduto() {
    const produto = getProduto();
    const plano = getPlano();
    const dependentesCobranca = produto.hasOwnProperty('dependentes') && produto.dependentes.find(function(d) { return textToNumber(d.valor) > 0;});
    const mes = dependentesCobranca ? 'mês / <br/> dependentes' : 'mês';
    const botaoProdutos = !plano ? '<button id="btn-voltar-produtos" class="btn btn-link texto-secundaria">< Voltar</button>' : '';
    if (produto) {
        return `${botaoProdutos}<h3 class="mb-2 text-center texto-secundaria">${produto.nome}</h3>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h3 class="pricing-card-title texto-secundaria"><div class="container-valor-plano${dependentesCobranca ? ' inclui-dependente': ''}">${formataNumero(produto.valor, mes)}</div></h3>
                <div id="info-prorata"></div>
                <div class="produto-descricao mt-4">${produto.descricao}</div>
                ${templateVencimento()}
                ${templateTermos()}
            </div>
        </div>
        `;
    }
    return '';
}

function clickProduto(e) {
    e.preventDefault();
    const id = $(this).data('id');
    produtoService.setProdutoPorId(id);
    montaItemProduto();
}

function clickVoltarProdutos(e) {
    e.preventDefault();
    setProduto(null);
    tabPanelProduto.find('#tab-lista-produto').addClass('active');
    tabPanelProduto.find('#tab-produto').removeClass('active');
}

function changeTermos(e) {
    e.preventDefault();
    const t = $(this);
    const termos = getTermos();
    const valor = t.val();
    const index = termos.indexOf(valor);
    if (t.prop('checked') && index === -1) {
        setTermos([...termos, valor]);
    } else if (index > -1) {
        setTermos(termos.splice(index, 1));        
    }
}

function changeDataVencimento(e) {
    e.preventDefault();
    const t = $(this);
    setDiaVencimento(t.val());
    const tabItemProduto = tabPanelProduto.find('#tab-produto'); 
    montarTemplateProrata(tabItemProduto);
    
}

function templateTermos() {
    const termosAceito = getTermos();
    const produto = getProduto();
    const usuario = getUsuario();
    const termosHtml = produto.termos ? produto.termos.map(function(termo) {
        return `<div class="custom-control custom-checkbox form-check mb-2">
        <input id="termo-${termo.id}" name="termo[]" type="checkbox" class="custom-control-input" value="${termo.id}" ${Number.parseInt(termo.obrigatorio) ? 'required' : ''} ${termosAceito.includes(termo.id) ? 'checked' : ''}>
        <label class="custom-control-label" for="termo-${termo.id}">${(Number.parseInt(termo.obrigatorio) ? '<span class="icon-required">*</span>' : '') + termo.termo}</label>
    </div>`;
    }).join('') : '';
    return `<div class="termos mt-4">
        <h4 class="mb-3 texto-secundaria">Termos:</h4>
        ${termosHtml}
        <div class="custom-control custom-checkbox form-check">
            <input id="termo-email" name="termo-email" type="checkbox" class="custom-control-input" value="0" required checked>
            <label class="custom-control-label" for="termo-email">${config.adm_msg_termos_email}</label>
        </div>
        <div class="custom-control custom-checkbox form-check">
            <input id="termo-celular" name="termo-celular" type="checkbox" class="custom-control-input" value="-1" required checked>
            <label class="custom-control-label" for="termo-celular">${config.adm_msg_termos_whats}</label>
        </div>
    </div>`; 
}

function templateVencimento() {
    const tipoVencimento = getTipoVencimento();
    const diaVencimento = getDiaVencimento();
    if (tipoVencimento === "2") {
        return `<div class="data-vencimento mt-4">
        <p class="mt-3 mb-1">Escolha o melhor dia para o vencimento das suas mensalidades</p>
        <div class="col-md-6 my-2">
            <div class="form-group">
                <label for="data-vencimento texto-primaria">Data de Vencimento:</label>
                <select id="data-vencimento" name="data-vencimento" class="form-control">
                    <option value="">Selecione um dia</option>
                    ${getVencimentos().map(function(item) {
                        return '<option value="'+item.dia+'" '+(diaVencimento == item.dia ? 'selected' : '')+'>'+item.dia+'º dia</option>';
                    }).join('')}
                </select>
                <div class="invalid-feedback">
                    Por favor, escolha um dia para o vencimento.
                </div>            
            </div>            
        </div>
    </div>`;
    }
    return '';
}

function salvarPlano() {
    if (!getProduto()) {
        swal('Atenção', 'Seleciona um plano antes de continuar', 'warning');
        return false;
    }
    const tabPanelProduto = tabPanel.find(configEtapa.id);
    const tipoVencimento = getTipoVencimento();
    const { id, dependentes } = getProduto();
    const id_parcela = getIdParcela() ? getIdParcela() : (getPlano() ? getPlano().id_parcela : null);
    const dataVencimento = getDiaVencimento();
    const email_marketing = tabPanel.find('#termo-email').prop('checked') ? 1 : 0;
    const celular_marketing = tabPanel.find('#termo-celular').prop('checked') ? 1 : 0;
    let data = { id_produto: id,  id_parcela, email_marketing, celular_marketing };
    if (tipoVencimento === "2") {
        if(!dataVencimento) {
            tabPanelProduto.find('#data-vencimento').addClass('is-invalid');
            swal('Atenção', 'Escolha uma data de vencimento', 'warning');
            return false;
        }
        data = Object.assign({}, data, { data_vencimento: dataVencimento });
    }
    tipoVencimento === "2" && tabPanelProduto.find('#data-vencimento').removeClass('is-invalid');
    const termos = Array.from(tabPanelProduto.find('input[name="termo[]"], #termo-email, #termo-celular'));
    const validaTermos = termos.filter(function(input) {
        if(input.hasAttribute('required') && !input.checked) {
            input.classList.add('is-invalid');
            return true;
        }
        input.classList.contains('is-invalid') && input.classList.remove('is-invalid');
        return false;
    });
    if(validaTermos.length) {
        swal('Atenção!', 'Deve aceitar todos os termos obrigatórios', 'warning');
        return false;
    }
    planoService.salvarPlano(data, function(res) {        
        usuarioService.patchUsuario({ email_marketing: data.email_marketing });
        planoService.alteraOuCriaPlanoLista(res);
        if (config.ativa_dependente && dependentes.length) {
            if (!verificaEtapa(tabDependentes.configEtapa.id)) {
                tabDependentes.init(numeroEtapa+1).eventNext();
            }
        } else {
            if (verificaEtapa(tabDependentes.configEtapa.id)) {
                removerEtapa(tabDependentes.configEtapa.id);
            } 
        }
        listenEvent(numeroEtapa+1);
    }, function(erro) {        
        console.log(erro);
        swal('Erro', 'Ocorreu um erro ao salvar o plano', 'error');
    });
}

function montarTemplateProrata(tabItemProduto) {
    const produto = getProduto();
    tabItemProduto.find('#info-prorata').html('');
    const {data, prorata} = calcularProrata(textToNumber(produto.valor));
    if (!config.exc_ven_prorata && prorata > 0) {
        const dependentesCobranca = produto.hasOwnProperty('dependentes') && produto.dependentes.find(function(d) { return textToNumber(d.valor) > 0;});
        tabItemProduto.find('#info-prorata').html(`<div class="alert alert-warning mt-3" role="alert">
            SERÁ GERADO UM ACRÉSCIMO DE ${numberFormat(prorata, 1)}${dependentesCobranca ? ' POR DEPENDENTE' : ''} EM SUA PRÓXIMA MENSALIDADE 
            DEVIDO A UMA PRO RATA EM RAZÃO DA DATA DE VENCIMENTO DE SEU PLANO.
        </div>`);
    }
    tabItemProduto.find('#info-prorata').append(`<div class="mt-3"><span class="text-bold">Data de Vencimento:</span> ${data}</div>`);
    
}


function calcularProrata(valor) {
    const tipoVencimento = getTipoVencimento();
    const diaVenc = getDiaVencimento();
    const respostas = {data: '', prorata: 0 };
    respostas.data = moment().add(1, 'months').format('DD/MM/YYYY');
    if ([1, 2].includes(Number.parseInt(tipoVencimento)) && diaVenc) {
        const hoje = moment();
        const diaVencimento = moment().date(diaVenc);
        const dataVencimento = diaVencimento.isBefore(hoje)  || diaVencimento.isSame(hoje, 'day') ? diaVencimento.add(1, 'months') : diaVencimento;
        respostas.data = dataVencimento.format('DD/MM/YYYY');
        if (hoje.date() < getDiaVencimento() || dataVencimento.subtract(1, 'days').diff(hoje, 'days') < 20) {
            const proximoVenReal = moment().add(1, 'months').subtract(1, 'days');
            const diffDias = proximoVenReal.diff(hoje, 'days') + 1;
            const difNovaDt = dataVencimento.diff(hoje, 'days');
            if (!config.exc_ven_prorata) {
                respostas.prorata = ((valor / diffDias) * difNovaDt).toFixed(2);
            }
        }
        if (respostas.prorata > 0) {
            respostas.data = moment(respostas.data, 'DD/MM/YYYY').add(1, 'months').format('DD/MM/YYYY');
        }     
    }
    setProrata(respostas);
    return respostas;
}