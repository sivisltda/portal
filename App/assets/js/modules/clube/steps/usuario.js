import { adicionarEtapaEntrePeriodo, eventEtapas, eventProximo, verificaEtapa, listenEvent, 
    incrementarEtapa, verificaEAdicionaTemplateTab, getNumeroEtapa, removerEtapa, getApp }
from './../../../core/route.js';

import { geraEventos, geraErrors, blurInput, convertSerializeObject, buscarCPF, convertDataIso, convertDataBr, errorCamposInvalidos }
from './../../../core/function.js';

import { chamaSwalQueue, requisitaCPF } from "./../../../core/auth.js";
import { createInput } from '../../../core/form.js';
import { addFieldRules, removeFieldRules, validacaoInput, validate, validate_email, validate_cnpj, validate_cpf } 
from './../../../core/validator.js';    

import { getConfig as configFunc } from "../../../config/config.js";


import * as listaPlanos from './listaplanos.js'; 
import * as tabDependentes from './dependente.js';

import store from './../../../store/app.js';

import * as userForm from './../forms/usuario.js'; 
import * as usuarioService from './../../../service/usuario.sevice.js';
import { popularEnderecoViaCep, buscarCidadesPorEstado, buscarEstados, transformEndereco } 
from './../../../service/endereco.service.js';

const { getUsuario, getCidades, getEndereco, getPlanos } = store.getters;
const { setUsuario, setEndereco, setProduto, setPlanos, setPlano, setIdParcela, 
    setDependentes, setTransacao } = store.setters;
    
let numeroEtapa     = null;
let app             = null;
let btnProx         = null;
let btnAnt          = null;
let tabPanel        = null;
let config          = null;

let form            = null;
let templateForm    = null;
let mostrarForm     = 0;
let formTemplate    = null;
let tipoDocumento   = null;

export const configEtapa = {
    id: '#info',
    title: 'Sobre',
    icon: 'mdi-account-circle',
    tooltip: 'Sobre'
};


const valoresInputs = {
    email: '',
    cnpj: '',
    estado: '',
    cep: ''
};

const errorActions = {
    responseUsuario: [
        {
            code: 400,
            action: errorInvalidEmailCNPJ
        }, {
            code: 401,
            action: errorDadosIncorretos
        }, {
            code: 403,
            action: errorPlanoAtivo
        }, {
            code: 404,
            action: function(error) {
                adicionaCampoCpf();
                form.find('.form-default').find('[name="email"]').val(valoresInputs.email)
                .closest('.form-group').next().removeClass('hide');   
            }
        }, {
            code: 500,
            action: function(error) {
                errorSwalAlert(error.responseJSON);
            }
        }
    ], submitUsuario: [
        {
            code: 500,
            action: function(error) {
                errorSwalAlert("Ocorreu um problema para salvar seus dados");
            }
        },{
            code: 403,
            action: errorPlanoAtivo
        }, {
            code: 401,
            action: function(error) {
                errorSwalAlert("Você não pode cadastrar pois já é um cliente");
            }
        }, {
            code: 422,
            action: errorCampos
        }
    ]
};


export function init(posicao) {
    app       = getApp();
    btnProx   = app.find('#btnProx');
    btnAnt    = app.find('#btnAnt');
    tabPanel  = app.find('#main');
    config = configFunc();
    numeroEtapa = posicao ? posicao : incrementarEtapa();
    if (!posicao) {
        eventEtapas(numeroEtapa, configEtapa, iniciaTela);
    } else {
        if (!verificaEtapa(configEtapa.id)) {
            adicionarEtapaEntrePeriodo(numeroEtapa, configEtapa, iniciaTela);            
        }
    }
    verificaEAdicionaTemplateTab(tabPanel, configEtapa.id, template(configEtapa.id.replace('#','')));
    return this;     
}

export function iniciaTela() {
    numeroEtapa = getNumeroEtapa(configEtapa.id) || numeroEtapa;
    app.removeClass('plano-fixo');
    btnAnt.hide();
    btnProx.show();
    carregaInfo();    
}

function template(id) {
    return `<div class="tab-pane py-3 bg-white" id="${id}">
        <div class="container">
            <h3 class="texto-secundaria">
            ${config.adm_msg_boas_vindas ? config.adm_msg_boas_vindas : 'Olá, para que possamos fazer sua cotação, precisamos de alguns dados seus, é super rápido!!!'}
            </h3>
            <div class="row my-4">
                <div class="col-lg-8 offset-lg-2">
                    <form class="form-horizontal form-checkout" id="form-dados" novalidate></form>
                </div>
            </div>
        </div>
    </div>`;
}

function carregaInfo() {
    form = tabPanel.find(configEtapa.id).find('#form-dados');
    montarTemplateForm(form); 
    form.on('submit', submitUsuario);
    const usuario = getUsuario();
    if (usuario) {
        mostrarForm = 1;
        valoresInputs.email = usuario.email;
        form.find('[name="email"]').val(usuario.email);
        removeCampoCpf();
        templateUsuario(usuario.juridico_tipo);
    } 
}

function montarTemplateForm(form) {
    const events = [
        {
            root: form,
            target: '[name="email"]',
            actions: [
                { event: 'blur', action: getConsultaUsuario }                
            ]
        }, {
            root: form,
            target: '[name="cnpj"]',
            actions: [
                { event: 'blur', action: getTipoDocumento }
            ]
        }
    ];
    templateForm = userForm.getFormDefault(form, events);
    const classeGroup = templateForm.type === 'row' ?  '' : 'row';
    form.html(`<div class="form-default ${classeGroup}">${createInput(templateForm.inputs, templateForm.type)}
    </div><div id="form-user"></div>`);  
    geraEventos(templateForm.events);   
    buscarCPF(form.find('[data-type="cpf-cnpj"]')); 
}

export function eventNext() {
    eventProximo(numeroEtapa, function() {
       form.submit();
    });
}


function getTipoDocumento(e) {
    e.preventDefault();
    const t = $(this);
    const valor = t.val();
    if (valor === '') {
        t.removeClass('is-valid').addClass('is-invalid')
        .siblings('.invalid-feedback').text('Documento obrigatório');
        return false;
    }
    if (valor === valoresInputs.cnpj) return false;

    if  (validate_cpf(valor) || validate_cnpj(valor)) {
        valoresInputs.cnpj = valor;
        usuarioService.consultaUsuario({cnpj: valor, usuario: true}, function(res) {
            t.removeClass('is-invalid').addClass('is-valid');
            tipoDocumento = valor.length > 14 ? 'J' : 'F';
            mostrarForm = 1;
            templateUsuario(tipoDocumento); 
        }, function(erro) {
            
            const msg = erro.responseJSON.erro;
            t.val('').removeClass('is-valid').addClass('is-invalid').siblings('.invalid-feedback').text(msg);
            valoresInputs.cnpj = '';

        });
    } else {
        t.removeClass('is-valid').addClass('is-invalid')
        .siblings('.invalid-feedback').text('Documento inválido');
    }
}

function getConsultaUsuario(e) {
    e.preventDefault();
    const t = $(this);
    const valor = t.val();
    t.removeClass('is-invalid').text('');
    if (valor === "") {
        t.removeClass('is-valid').addClass('is-invalid').focus()
        .siblings('.invalid-feedback').text('E-mail obrigatório');
        return false;
    }
    if (!validate_email(valor)) {
        t.removeClass('is-valid').addClass('is-invalid').focus()
        .siblings('.invalid-feedback').text('E-mail inválido');
        return false;
    }
    if (valoresInputs.email === valor) return false;
    form.find('#form-user').html('');
    form.find('[name="cnpj"]').val('').closest('.form-group').addClass('hide');
    
    valoresInputs.email = valor;
    resetDados();
    usuarioService.getBuscarUsuario(valor, function (resp) {   
        mostrarForm = 0;
        if (resp.hasOwnProperty('hasCnpj')) {
            chamaSwalQueue(requisitaCPF(resp, function(data) {
                setEndereco(transformEndereco(data));
                removeCampoCpf();
                mostrarForm = 1;
                templateUsuario(getUsuario().juridico_tipo);
            }, function(erro) {
                valoresInputs.email = '';
                t.val('');
            }), function (res) {
                if (res.hasOwnProperty('dismiss')) {
                    if ([Swal.DismissReason.esc, Swal.DismissReason.cancel].includes(res.dismiss)) {
                        valoresInputs.email = '';
                        t.val('');
                    }
                }
            } );
        } else {
            adicionaCampoCpf();
            form.find('.form-default').find('[name="email"]').val(valor);
        }            
    }, function (error) {
        mostrarForm = 0;
        geraErrors(errorActions, 'responseUsuario', error);
        
    });
}

function requisitaCPF2(res, input) {
    return {
        input: 'text',
        type: 'warning',
        confirmButtonText: 'Ok',
        cancelButtonText: 'Cancelar',
        allowEscapeKey: false,
        allowOutsideClick: false,
        showCancelButton: true,
        title: 'Atenção',
        html: '<strong class="text-center">Atenção, '+res.razao_social+'</strong><br/> Verificamos que seu e-mail já possui um cadastro feito anteriormente, para continuar' 
                + ' é necessário que informe os **03 primeiros dígitos do seu CPF/CNPJ**',
        inputPlaceholder: 'Digite seu CPF',
        showLoaderOnConfirm: true,
        inputAttributes: {
            "class": "form-control",
            "maxlength": 3,
            "minlength": 3
        },
        inputValidator: (value) => {
            const cnpj = value.replace(/\D/g, '');
            if (cnpj.length !== 3) {
                return 'Informação inválida';
            }
            return new Promise(function(resolve) {
                setaInfoUsuario({cnpj}, function(data) {               
                  
                    resolve();
                }, function(msg)  {
                    swal('Erro', msg, 'error').then(function() {
                        chamaSwalQueue(requisitaCPF(res, input), function(res) {
                            if (res.hasOwnProperty('dismiss')) {
                                if ([Swal.DismissReason.esc, Swal.DismissReason.cancel].includes(res.dismiss)) {
                                    input.val('');
                                    valoresInputs.email = '';
                                }
                            }
                        });
                    });
                    resolve(msg);
                });
            });
        }
    };
}

function setaInfoUsuario(dados, callback, errCallback) {    
    usuarioService.getBuscarDadosUsuario(dados, function(res) { 
        callback(res);
    }, function(erro) {
        console.log(erro);
        geraErrors(errorActions, 'responseUsuario', erro);
        errCallback(erro.responseJSON.erro);
    });
}

function changeRegimeTributario(e) {
    e.preventDefault();
    const t = $(this);
    if (t.prop('checked')) {
        form.find('[name="inscricao_estadual"]').attr('readonly', 'readonly').val('');
        removeFieldRules('inscricao_estadual','required');
    } else {
        form.find('[name="inscricao_estadual"]').removeAttr('readonly');
        addFieldRules('inscricao_estadual', 'required', 'Inscrição Estadual obrigatório');
    }
}

function errorDadosIncorretos(error) {
    const response = error.responseJSON;
    swal({ type: "warning", title: response.title, text: response.message }).then(function(res) {
        if (res.value) {
            if (response.status === 'email_invalid') {
                form.find('[name="email"]').val('');
            } else {
                form.find('[name="cnpj"]').val('');
            }
        }
    });
}

function errorCampos(error) {
    const errors = error.responseJSON.erro;
    errorCamposInvalidos(form, errors);
}

function errorInvalidEmailCNPJ(error) {
    if (error.responseText === '"email:invalid"') {
        swal("Opa", "Digite um email válido", "error");
    } else if (error.responseText === "cnpj:invalid") {
        swal("Opa", "Digite um CPF / CNPJ válido!", "error");
    }
}

function errorSwalAlert(text) {
    swal({type: "error", title: "Erro...", text: text});
}

function errorPlanoAtivo(error) {
    form.find('[name="email"]').val('');
    valoresInputs.email = '';
    swal({type: "warning", title: "Plano Ativo", html: `Atenção, você já possui esse plano com vínculo!` });
}

function templateUsuario(tipoPessoa) {
    const formDadosUsuario = form.find('#form-user');
    const events = [
        {
            root: formDadosUsuario,
            target: '[name="cep"]',
            actions: [
                { event: 'blur', action: buscarCep }
            ]
        },
        {
            root: formDadosUsuario,
            target: '[name="estado"]',
            actions: [
                { event: 'change', action: selectEstado }
            ]
        }, {
            root: formDadosUsuario, 
            target: '[name="regime_tributario"]',
            actions: [
                { event: 'change', action: changeRegimeTributario }
            ]
        }
    ];
    
    buscarEstados(function() {
        formTemplate = tipoPessoa === 'J' ? userForm.formDadosEmpresarial(formDadosUsuario, events) 
            : userForm.formDados(formDadosUsuario, events);
        const classeGroup = templateForm.type === 'row' ?  '' : 'row';
        const templateFormUsuario = formTemplate.group.map(function(secao, index) {
            const sectionHide = index > 0 ? ' hide' : '';
            return `<div class="section${sectionHide}"><h4 class="mb-2 subtitulo texto-secundaria">${secao.title}</h4>
            <div class="${classeGroup}">${createInput(secao.inputs, formTemplate.type)}</div></div>`;
        }).join('');
        formDadosUsuario.html(templateFormUsuario);
        formDadosUsuario.find('[name="estado"]').select2();
        geraEventos(formTemplate.events);
        blurInput(formDadosUsuario, ['procedencia'], validacaoInput(formTemplate));
        formDadosUsuario.find('[name="procedencia"]').on('blur', function(e) {
            e.preventDefault();
            const section = $(this).closest('.section').next();
            section.removeClass('hide').find('[name="cep"]').closest('.form-group').removeClass('hide');
        });
        form.find('[name="nome"]').focus();  
        const usuario = getUsuario();
        if (usuario) {
            setaInfoFormUsuario(formDadosUsuario, usuario);
        }
    }, function(erro) {
        console.log(erro);
        swal('Ocorreu um erro ao carregar o estado');
    });
    
}

function setaInfoFormUsuario(formDadosUsuario, usuario) {
    Object.keys(usuario).forEach(function(chave) {
        const input = formDadosUsuario.find(`[name="${chave}"]`);
        if (input.length) {
            const valor = chave === 'data_nascimento' ? convertDataIso(usuario[chave]) : usuario[chave];
            input.val(valor);
            if (chave === 'estado') {
                input.select2().change(); 
            } else if (chave === 'regime_tributario' && valor === "2") {
                input.prop('checked', true).change();
            } else if (chave === 'nfe_iss' && valor === "1") {
                input.prop('checked', true).change();
            } 
        }  
    });
    mostrarForm && formDadosUsuario.find('.hide').removeClass('hide');
}

function selectEstado(e) {
    e.preventDefault();
    const t = $(this);
    const valor = t.val();
    if (valor === "") {
        t.addClass('is-invalid').siblings('.invalid-feedback').text('Estado obrigatório');
    }
    if (valor && valor !== valoresInputs.estado) {
        valoresInputs.estado = valor;
        
        buscarCidadesPorEstado(valor, function() {
            preencheSelectCidade();
        }, function (erro) {
            console.log(erro);
            swal('Erro', 'Ocorreu um erro ao buscar as cidades', 'error');
        });
    }
}

function buscarCep(e) {
    e.preventDefault();
    const t = $(this);
    const cep = t.val();
    if (cep === "") {
        t.addClass('is-invalid').siblings('.invalid-feedback').text('Cep obrigatório');
        return false;
    }
    if (cep === valoresInputs.cep) {
        return false;
    }
    valoresInputs.cep = cep;
    const formDadosUsuario = form.find('#form-user');
    if (cep.length === 9) { 
        popularEnderecoViaCep(cep, function() {
            const endereco = getEndereco();
            t.closest('.section').find('.hide').removeClass('hide');
            Object.keys(endereco).forEach(function(chave) {
                const input = formDadosUsuario.find(`[name="${chave}"]`);
                if (input.length && endereco[chave]) {
                    input.val(endereco[chave]);
                     (!['estado', 'cidade', 'cep'].includes(chave)) ? input.attr('readonly', 'readonly') : 
                        (['estado'].includes(chave) ? input.select2() : input.change());
                }                
                preencheSelectCidade();
            });
            formDadosUsuario.find('[name="numero"]').focus();
        }, function(erro) {
            console.log(erro);
            t.val('');
            valoresInputs.cep = '';
            if (erro.status === 2) {
                swal('Erro', 'Ocorreu um erro ao buscar o endereço', 'error');
            } else {
                swal('Erro', 'Ocorreu um erro ao buscar endereço pelo CEP', 'error');
            }
        });
    }
}


function preencheSelectCidade() {
    const formDadosUsuario = form.find('#form-user');
    const endereco = getEndereco();
    const cidades = getCidades().map(function(item) {
        return `<option value="${item.cidade_codigo}">${item.cidade_nome}</option>`;
    }).join('');
    formDadosUsuario.find('[name="cidade"]').html(cidades).select2();
    if (endereco.cidade) {
        formDadosUsuario.find('[name="cidade"]').val(endereco.cidade).change();
    }
}

function submitUsuario(e) {
    e.preventDefault();
    if (!formTemplate) {
        return false;
    }
    const t = $(this);
    const usuario = getUsuario();
    const validations = validate(formTemplate.rules, t);
    if (validations.length) {
        const errors = validations.reduce(function(acc, i) {
            acc[i.field] = i.message;
            return acc; 
        },  {});
        errorCamposInvalidos(t, errors);
        swal('Atenção!', 'Informe os campos obrigatórios', 'warning');
        return false;
    }
    const dados = Object.assign({}, convertSerializeObject(t.serialize()));
    if ( tipoDocumento) {
        dados.juridico_tipo =  tipoDocumento;
    }
    if (t.find('[name="regime_tributario"]').length) {
        dados.regime_tributario = t.find('[name="regime_tributario"]').prop('checked') ? "2" : "1";
    }
    if (t.find('[name="nfe_iss"]').length) {
        dados.nfe_iss = t.find('[name="nfe_iss"]').prop('checked') ? "1" : "0";
    }
    dados.data_nascimento = convertDataBr(dados.data_nascimento);
    const data = usuario ? Object.assign({}, usuario, dados) : dados;
    
    usuarioService.salvarUsuario(data, function(){
        const planos = getPlanos().filter(function(plano) { return plano.status === 'Novo' || plano.status === ''});
        if (planos.length) {
            if (!verificaEtapa(listaPlanos.configEtapa.id)) {
                listaPlanos.init(numeroEtapa+1).eventNext();
            }
        } else {
            removerEtapa(listaPlanos.configEtapa.id);
        }
        listenEvent(numeroEtapa+1);
    }, function(erro) {
        console.log(erro);
        geraErrors(errorActions, 'responseUsuario', erro);
        swal('Erro', 'Ocorreu um erro ao salvar o usuário', 'error');
    });
}

function resetDados() {
    valoresInputs.cnpj = '';
    valoresInputs.estado = '';
    valoresInputs.cep = '';
    setUsuario(null);
    setEndereco(null);
    setProduto(null);
    setPlanos([]);
    setPlano(null);
    setIdParcela(null); 
    setDependentes([]);
    setTransacao(null);
    removerEtapa(listaPlanos.configEtapa.id);
    removerEtapa(tabDependentes.configEtapa.id);
}

function removeCampoCpf() {
    const campoCPF = form.find('[name="cnpj"]');
    if (campoCPF.length) {
        campoCPF.closest('.form-group').remove();
    }
}

function adicionaCampoCpf() {
    const campoCPF = form.find('[name="cnpj"]');
    if (!campoCPF.length) {
        form.find('.form-default').html(`${createInput(templateForm.inputs, templateForm.type)}`);
        geraEventos(templateForm.events); 
        buscarCPF(form.find('[data-type="cpf-cnpj"]')); 
    }
}