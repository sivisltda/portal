import { adicionarEtapaEntrePeriodo, eventEtapas, verificaEtapa, eventProximo, listenEvent, 
    getNumeroEtapa, incrementarEtapa, verificaEAdicionaTemplateTab, getApp }
    from './../../../core/route.js';

import { convertSerializeObject, convertDataIso, convertDataBr, blurInput, errorCamposInvalidos }
    from './../../../core/function.js';

import { getConfig as configFunc } from './../../../config/config.js';

import { validate, validacaoInput } from './../../../core/validator.js';    
import { createInput } from './../../../core/form.js';    
import store from './../../../store/app.js';

import * as dependenteService from './../../../service/dependente.service.js'
import * as formDependente from './../../../shared/formdependente.js';
import * as regraDependentes from './../../../shared/regraDependentes.js';

const { getProduto, getDependentes, getParentesco } = store.getters;

let numeroEtapa     = null;
let app             = null;
let config          = null;
let tabPanel        = null;
let btnProx         = null;
let btnAnt          = null;

let containerDependente = null;
let listaDependente     = null;

const valoreInputs = {
    email: '', 
    cnpj: ''
}

export const configEtapa = {
    id: '#dependente',
    title: 'Dependentes',
    icon: 'mdi-account-circle',
    tooltip: 'Dependentes'
};

export function init(posicao) {
    app      = getApp();
    config   = configFunc();
    tabPanel = app.find('#main');
    btnProx  = app.find('#btnProx');
    btnAnt   = app.find('#btnAnt');
    numeroEtapa = posicao ? posicao : incrementarEtapa();
    if (!posicao) {
        eventEtapas(numeroEtapa, configEtapa, iniciaTela);
    } else {
        if (!verificaEtapa(configEtapa.id)) {
            adicionarEtapaEntrePeriodo(numeroEtapa, configEtapa, iniciaTela);
        }
    } 
    verificaEAdicionaTemplateTab(tabPanel, configEtapa.id, template(configEtapa.id.replace('#','')));
    return this;     
}

export function iniciaTela() {
    numeroEtapa = getNumeroEtapa(configEtapa.id) || numeroEtapa;
    const tab = tabPanel.find(configEtapa.id);
    containerDependente = tab.find('#tab-dependente');
    listaDependente = tab.find('#lista-dependente');
    app.addClass('plano-fixo');
    getDependentesInicio();
    btnAnt.show();
    btnProx.show();
}

function template(id) {
    return `<div class="tab-pane limite py-3 bg-white" id="${id}">
    <div class="container-fluid">  
        <div class="row">
            <div class="col-lg d-print-none container-info">
                <div class="row">
                    <div class="col-12 container-titulo-principal mb-4">
                        <h3 class="text-center texto-secundaria">${configEtapa.title}</h3>
                    </div>
                    <div class="col-12" id="tab-dependente"></div>
                </div>     
            </div>
            <div class="px-0 menu-right lista-dependente bg-secundaria text-white p-3" id="lista-dependente"></div>
        </div>            
    </div>
</div>`;
}

export function eventNext() {
    eventProximo(numeroEtapa, function() {
        const produto = getProduto();
        const { numMin, numMax } = regraDependentes.getRegrasQtdRegras(produto.dependentes);
        const dependentes = getDependentes();
        if (dependentes.length >= numMin) {
            listenEvent(numeroEtapa+1);
        } else {
            swal('Atenção!', 'Quantidade de dependentes insuficiente', 'warning');
        }
    });
}

function getDependentesInicio() {
    
    dependenteService.getListaDependentes(function() {
        templateAcaoMenu();
        templateDefaultDependente();
        
    }, function(erro) {
        console.log(erro);
        
        swal('Erro', 'Ocorreu um ao erro ao buscar os dependentes', 'error');
    });
}

function criarFormDependentes(titulo) {
    const produto = getProduto();
    const parentesco = !produto.pes_jur ? getParentesco() : null;
    const inputForm = formDependente.criarFormDependentes('', parentesco);
    formDependente.rulesDependente(produto.pes_jur);
    const templateForm = `<div class="item-dependente mt-3">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <form class="form-dependente" novalidate>
                    <h4 class="texto-secundaria mb-3">${titulo}</h4>
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Atenção!</strong> Não informando nem o CPF ou e-mail o dependente não terá o acesso ao painel
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row">${createInput(inputForm, 'column')}
                    <input type="hidden" id="id_dependente" name="id_dependente" /></div>
                    <div class="mt-3 text-right"><button class="btn text-uppercase btn-secundaria">Salvar</button></div>
                </form>
            </div>
        </div>
    </div>`;
    containerDependente.html(templateForm);
    const form = containerDependente.find('.form-dependente');
    form.on('submit', submitDependente);  
    
    form.find('[name="data_nascimento"]').on('blur', blurDataNascimento);
    const rules = formDependente.getRules();
    blurInput(form, [], validacaoInput({ rules }));
    form.on('blur', '[name="cnpj"], [name="email"]', blurCNPJEmail);
    
}

function templateAcaoMenu() {
    const produto = getProduto();
    const dependentes = getDependentes();
    const { numMin, numMax } = regraDependentes.getRegrasQtdRegras(produto.dependentes);
    const templateLimite =  dependentes.length >= numMax ? `
        <h4>Limite Dependente excedido</h4>
        <p>Caso quiser adicionar é preciso remover um dependente.</p>
    ` 
    : `<div class="limite-dependente">
        <span class="numero-limite"><strong>Mínimo:</strong> ${numMin || '------'}</span>
        <span class="numero-limite"><strong>Máximo:</strong> ${numMax || '------'}</span>
    </div>
    <div class="container-botao-adicionar">
        <button class="btn mostrar-form text-uppercase">Adicionar</button>
    </div>`;
    let html = `<div class="container-menu-dependente mb-2">
       ${templateLimite}
    </div>
    ${templateRegra(produto)}`;
    if (dependentes.length) {
        html+=`<h5 class="titulo-numero-dependente">${dependentes.length} Dependentes</h5>`;
        html+=`<div class="lista-dependentes p-2">${templateListaDependentes(dependentes)}</div>`;
    } else {
        html+=`<h5 class="titulo-numero-dependente">Nenhum Dependente Cadastrado</h5>`;
    }  
  
    listaDependente.html(html);
    listaDependente.find('.mostrar-form').on('click', mostrarFormDependente);
    listaDependente.find('.btn-remove-dependente').on('click', removeDependente);
}

function templateListaDependentes(dependentes) {
    const opcaoCampos = {
        nome: 'Nome', data_nascimento: 'Data de Nasc.', email: 'E-mail',
        cnpj: 'CPF', estado_civil: 'Estado Civil', celular: 'Celular',
        nome_parentesco: 'Parentesco'
    };
    return dependentes.map(function(dependente) {
        return `<div class="card card-dependente mb-3 pb-3"><div class="row">
            <div class="col-xl-8">
                <div class="card-body">
                    ${Object.keys(opcaoCampos).map(function(i) {
                        if (dependente[i]) {
                            return '<p class="card-text mb-0"><strong>'+opcaoCampos[i]+':</strong> '+dependente[i]+'</p>';
                        }
                        return null;
                    }).join('')}                
                </div>
            </div>
            <div class="col-xl">
                <div class="d-flex h-100 align-items-center justify-content-center">
                    <button class="btn text-uppercase btn-remove-dependente" data-id="${dependente.id_dependente}">Remover</button>
                </div>
            </div>
        </div></div>`;
    }).join('');
}


function removeDependente(e) {
    e.preventDefault();
    const id = $(this).data('id');
    const dependente = getDependentes().find(function(d) {return Number.parseInt(d.id_dependente) === id});
    swal({ title: 'Remover', html: `Você deseja realmente remover o ${dependente.nome} da sua lista de dependentes?`, type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#9E9E9E',
        confirmButtonText: 'Sim',  cancelButtonText: 'Não' })
        .then(function(res) {
            if (res.value) { 
                dependenteService.removeDependente({ id_dependente: id }, function() {
                    swal('Sucesso', 'Dependente removido com sucesso', 'success').then(function() {
                        getDependentesInicio();
                    });
                }, function(erro) {
                    swal('Erro', erro.responseJSON.erro, 'error');
                });
            }
        });
  
}

function mostrarFormDependente(e) {
    e.preventDefault();
    criarFormDependentes('Novo Dependente');
}

function submitDependente(e) {
    e.preventDefault();
    const form = $(this);
    const produto = getProduto();
    const dependentes = getDependentes();
    const rules = formDependente.getRules();
    const validations = validate(rules, form);
    if (validations.length) {
        const errors = validations.reduce(function(acc, i) {
            acc[i.field] = i.message;
            return acc; 
        },  {});
        errorCamposInvalidos(form, errors);
        swal('Atenção!', 'Informe os campos obrigatórios', 'warning');
        return false;
    }
    const dados = convertSerializeObject(form.serialize());
    dados.data_nascimento = convertDataBr(dados.data_nascimento);
    const { resultado, mensagem } = 
    regraDependentes.validacaoDependente(dependentes, produto.dependentes, dados);
    if (!resultado) {
        swal('Atenção!', mensagem, 'warning');
        return false;
    }
    salvarDependente(dados);
   
}

function blurDataNascimento(e) {
    e.preventDefault();
    const t = $(this);
    t.removeClass('is-invalid');
    const data_nascimento = t.val();
    const rules = formDependente.getRules();
    if (data_nascimento && validacaoInput({ rules })(t)) {
        const produto = getProduto();
        const form = t.closest('form');
        const regras = regraDependentes.configRegrasIdade(
            form, ['cnpj', 'email', 'celular'], produto.dependentes, getParentesco(), 
            produto.pes_jur);
        regras.forEach(function(r) {
             r.regra && r.acao();
        });
    } else {
        t.addClass('is-invalid');
    }
}

function blurCNPJEmail(e) {
    e.preventDefault();
    const t = $(this);
    const valor = t.val();
    const nomeInput = t.attr('name');
    const rules = formDependente.getRules();
    const validacao = validacaoInput({ rules })(t);
    if (!validacao || !t.val().trim().length || valoreInputs[nomeInput] === valor) {
        return false;
    }
    const form = t.closest('form');
    const cnpj =  nomeInput === 'cnpj' ? t.val() : null;
    const email = nomeInput === 'email' ? t.val() : null;
    const id_dependente = form.find('[name="id_dependente"]').val();
    valoreInputs[nomeInput] = t.val();
    
    dependenteService.consultarUsuarioPorCPF({ cnpj, email, id_dependente}, function(data) {
        if(data.id) {
            swal({
                title: 'Usuário já encontrado', html: `Atenção! 
                </br> Você já deseja incluir ${data.razao_social}, como seu dependente?`, type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#9E9E9E',
                confirmButtonText: 'Sim',  cancelButtonText: 'Não'
            }).then(function(res) {
                if (res.value) {
                    data.id_dependente = data.id;
                    incluindoDependente(data);
                } else if(res.dismiss === Swal.DismissReason.cancel) {
                    t.val(''); 
                    valoreInputs[nomeInput] = '';       
                }
            });
        }
        
    }, function(erro) {
        console.log(erro);
        if (erro.status === 404) {
            t.closest('.form-group').next().find(':input').focus();
        } else if (erro.status === 403) {   
            const mensagem = erro.responseJSON.erro;
           t.val('').addClass('is-invalid').siblings('.invalid-feedback').text(mensagem);
           valoreInputs[nomeInput] = '';
        }
        
    });
}

function templateDefaultDependente() {
    const dependentes = getDependentes();
    const produto = getProduto();
    const { _, numMax } = regraDependentes.getRegrasQtdRegras(produto.dependentes);
    const templateLimite = dependentes.length >= numMax ? `
    <p class="info-botao mt-3">Limite Excedido!</p>
    <p class="info-botao">Para Adicionar novos dependentes é necessário remover um acessando o menu ao lado ou clicando no botão abaixo</p>
    <div class="d-flex justify-content-center mt-3"><button class="btn btn-secundaria">Gerenciar Dependente</button></div>
    ` : `<p class="info-botao mt-3">Para cadastrar deve acessar o menu ao lado ou clique no botão abaixo</p>
    <div class="d-flex justify-content-center mt-3"><button class="btn text-uppercase btn-secundaria mostrar-form">Adicionar Dependente</button></div>`;
    containerDependente.html(`<div class="container-dependente"><div class="media-dependente">
    <img src="${config.site}assets/images/dependente.png" alt="Dependente" class="img-fluid" />
    <div class="container-titulo"><h3 class="texto-secundaria">${dependentes.length} Dependentes Cadastrado(s)</h3></div>
    </div>${templateRegra(produto)}
    ${templateLimite}</div>`);
    containerDependente.find('.mostrar-form').on('click', mostrarFormDependente);
}

function templateRegra(produto) {
    let html = '';
    if (produto.dependentes.length) {
        html+=`<div class="regras-dependentes p-2">
            <h5>Regras</h5>
            <ul class="mt-1 ml-2" style="list-style: disc;">${regraDependentes.templateRegraDependentes(produto.dependentes)}</ul>
        </div>`;
    }
    return html;
}

function setaDadosForm(form, dependente = {}) {
    Object.keys(dependente).forEach(function(key) {
        const input = form.find('[name="'+key+'"]');
        if (input.length) {
            const valor = key === 'data_nascimento' ?  convertDataIso(dependente[key]) : dependente[key];
            input.val(valor);
        }
    });  
}

function incluindoDependente(dependente) {
    dependente.nome = dependente.razao_social;
    const produto = getProduto();
    if (!produto.pes_jur) {
        swal({
            type: 'warning',
            title: 'Inclusão de Dependente',
            html: `Para incluir o dependente ${dependente.nome} é necessário informar o grau de parentesco.`,
            input: 'select',
            inputOptions: getParentesco(),
            inputPlaceholder: 'Selecione um tipo de parentesco',
            showCancelButton: true,
            inputValidator: (value) => {
                if (!value) {
                    return 'Parentesco e obrigatório'
                }
            }
        }).then(function(res) {
            dependente.parentesco = res.value;
            salvarDependente(dependente);
        });
    } else {
        salvarDependente(dependente);
    }
}

function salvarDependente(dados) {
    
    dependenteService.cadastrarDependentes(dados, function(res) {
        console.log(res);
        swal('Sucesso', 'Dependente Salvo com sucesso', 'success')
        .then(function(res) {
            if (res.value) {
                getDependentesInicio();
            }
        });
        
    }, function(erro) {
        console.log(erro);
        if (erro.status === 422) {
            const form = containerDependente.find('.form-dependente');
            errorCamposInvalidos(form, error.responseJSON);
        }
        swal('Erro', 'Ocorreu um erro ao salvar o Dependente', 'error');
        
    });
}