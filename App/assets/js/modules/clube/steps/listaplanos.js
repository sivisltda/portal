import { adicionarEtapaEntrePeriodo, eventEtapas, verificaEtapa, removerEtapa, eventProximo, 
    listenEvent, incrementarEtapa, getNumeroEtapa, verificaEAdicionaTemplateTab, getApp }
    from './../../../core/route.js';

import { formataNumero, ativarCarrousel, goStepCarrousel } from './../../../core/function.js';

import * as planoService from './../../../service/plano.service.js';  
import { getConfig as configFunc } from './../../../config/config.js';

import store from './../../../store/app.js';

const { getPlanos, getPlano, getUsuario } = store.getters;
const { setPlano, setIdParcela, setProduto, setDiaVencimento } = store.setters;

export const configEtapa = {
    id: '#list-planos',
    title: 'Planos',
    icon: 'mdi-account-circle',
    tooltip: 'Planos'
};

let numeroEtapa     = null;
let app             = null;
let config          = null; 
let tabPanel        = null;
let btnProx         = null;
let btnAnt          = null;

let planos          = [];

const camposItems = {
    valor_adesao: 'Valor Adesão', dt_inicio: 'Data Início', dt_fim: 'Data Fim', dt_cadastro: 'Data de Cadastro', 
    tipo_pagamento: 'Tipo Pagamento', prox_venc: 'Próximo Venc.', dependentes: 'Dependentes'   
};

const opcoesMeses = {
    "2": 'bimestral', "3": 'trimestral', "6": 'semestral', "12": 'anual'
};

export function init(posicao) {
    app      = getApp();
    config   = configFunc();     
    tabPanel = app.find('#main');
    btnProx  = app.find('#btnProx');
    btnAnt   = app.find('#btnAnt');
    numeroEtapa = posicao ? posicao : incrementarEtapa();
    if (!posicao) {
        eventEtapas(numeroEtapa, configEtapa, iniciaTela);
    } else {
        if (!verificaEtapa(configEtapa.id)) {
            adicionarEtapaEntrePeriodo(numeroEtapa, configEtapa, iniciaTela);
        }
    } 
    verificaEAdicionaTemplateTab(tabPanel, configEtapa.id, template(configEtapa.id.replace('#','')));
    return this;     
}

export function iniciaTela() { 
    numeroEtapa = getNumeroEtapa(configEtapa.id) || numeroEtapa;
    planos = getPlanos().filter(function(plano) { return plano.status === 'Novo' || plano.status === ''});
    app.removeClass('plano-fixo');
    const tab = tabPanel.find(configEtapa.id);
    const planosLista = tab.find('#planos-lista');
    const html = planos.map(function(plano) {
        return templatePlano(plano);
    });
    ativarCarrousel(
        planosLista, 
        html,
        {
            arrows: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            centerMode: false,
            responsive: [
                {
                    breakpoint: 980,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        }
    );
    goStepCarrousel(planosLista, 'active');
    planosLista.find('.btn-seleciona').on('click', selecionaPlano);
    planosLista.find('.btn-desvincula').on('click', desvinculaPlano);
    tab.find('#btn-novo-plano').on('click', clickNovoPlano);
    btnAnt.show();
    btnProx.show();
    if(getPlano()) {
        tab.find('#btn-novo-plano').prop('disabled', false);
    } else {
        btnProx.hide();
    }
}

function template(id) {
    const produto = getPlano();
    return `<div class="tab-pane py-3 bg-white" id="${id}">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center mb-4">
                <h3 class="text-center text-uppercase texto-secundaria">Escolha seu plano</h3>
                <button id="btn-novo-plano" class="btn btn-secundaria" ${Number.parseInt(config.unico) && !produto ? ' disabled' : ''}>${Number.parseInt(config.unico) ? 'Trocar de Plano' : 'Novo Plano'}</button>
            </div>
            <div id="planos-lista" class="container-carrousel row"></div>
        </div>
    </div>`;
}

export function eventNext() {
    eventProximo(numeroEtapa, function() {
        const plano = getPlano();
        if (plano) {
            setDiaVencimento(plano.dia_vencimento);
        }
        listenEvent(numeroEtapa+1);
    });
}


function templatePlano(plano) {
    const planoEscolhido = getPlano() ? getPlano() : planos[0];
    const usuario = getUsuario();
    if (plano.favorecido === usuario.id) {
        setPlano(planoEscolhido);
    }
    const msgMes = Number.parseInt(plano.parcela) < 2 || plano.gerar_parc ? 'mês' : (opcoesMeses.hasOwnProperty(plano.parcela) ? opcoesMeses[plano.parcela] : plano.parcela+" meses");
    const selecionado = planoEscolhido && planoEscolhido.id_produto === plano.id_produto;
    const botao =  plano.favorecido === usuario.id ?
     `<button class="btn btn-block btn-seleciona ${selecionado ? 'btn-secundaria' : 'btn-secundaria'}" data-id="${plano.id}">${ selecionado ? 'Selecionado' : 'Selecione'}</button>`
     : `<button class="btn btn-block btn-desvincula btn-secundaria" data-id="${plano.id}">Desvincular</button>`;
    return `<div class="card bg-primaria text-white h-100">
        <div class="card-header">
            <h4 class="my-0 font-weight-normal text-center">${plano.nome}</h4>
        </div>
        <div class="card-body">
            <h2 class="card-title pricing-card-title texto-secundaria">
                <div class="container-valor-plano">
                    ${formataNumero(plano.valor_plano, msgMes)}
                </div>
            </h2>
            <ul class="mt-3">${itemsPlano(plano)}</ul>
        </div>
        <div class="card-footer">${botao}</div>
    </div>`;
}


function itemsPlano(plano) {
    return Object.keys(camposItems).map(function(campo) {
      if (plano[campo])  {
        return `<li><strong>${camposItems[campo]}:</strong> ${plano[campo]}</li>`;  
      } 
      return false;  
    }).filter(Boolean).join('');
}


function selecionaPlano(e) {
    e.preventDefault();
    const planos = getPlanos();
    const id = $(this).data('id');
    const plano = planos.find(function(p) { return Number.parseInt(p.id) === id });
    setPlano(plano);
    setIdParcela(plano.id_parcela);
    setDiaVencimento(plano.dia_vencimento);
    listenEvent(numeroEtapa+1);
}

function desvinculaPlano(e) {
    e.preventDefault();
    const id = $(this).data('id');
    const usuario = getUsuario();
    swal({ title: 'Desvincular do Plano', html: `Atenção, ${usuario.razao_social}! 
    </br> Você deseja desvincular desse plano?`, type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#9E9E9E',
    confirmButtonText: 'Sim',  cancelButtonText: 'Não' })
    .then(function(res) {
        if(res.value) {
            planoService.cancelarPlanoUsuario(id, function() {                
                retiraEtapa(id);
                swal('Sucesso', 'Plano Desvinculado com sucesso', 'success');
            }, function(erro) {
                console.log(erro);
                swal('Erro', 'Não foi posível desvincular do plano', 'error');
            });
        }
    });


}

function clickNovoPlano(e) {
    e.preventDefault();
    if (!Number.parseInt(config.unico)) {
        setPlano(null);
        setProduto(null);
        setIdParcela(null);
        listenEvent(numeroEtapa+1);
    } else {
        const plano = getPlano();
        const usuario = getUsuario();
        swal({ title: 'Plano Encontrado', html: `Atenção, ${usuario.razao_social}! 
        </br> Você já iniciou uma contratação do ${plano.nome}, Deseja realmente cancelar esse plano?`, type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#9E9E9E',
        confirmButtonText: 'Sim',  cancelButtonText: 'Não' })
        .then(function(res) {
            if (res.value) {
                cancelarPlano(function() {
                    setPlano(null);
                    setProduto(null);
                    setIdParcela(null);
                    listenEvent(numeroEtapa+1);
                });
            }
        });
    }
}

function cancelarPlano(callback) {
    const plano = getPlano();
    planoService.cancelarPlanoUsuario(plano.id, function() {
        swal('Sucesso', 'Plano anterior cancelado com sucesso, você já pode continuar seu cadastro!', 'success')
        .then(function() {
            callback();
        });
    }, function(erro, codigo) {
        if (codigo === 1) {
            retiraEtapa(plano.id);
        }
        swal(erro);        
    });
}

function retiraEtapa(id) {
    const planos = getPlanos();
    planoService.removePlanoPorId(id);
    setProduto(null);
    setPlano(null);
    setIdParcela(null);
    if (planos.length === 1) {
        if (verificaEtapa(configEtapa.id)) {
            removerEtapa(configEtapa.id);
        }                     
    }
    listenEvent(numeroEtapa);
}