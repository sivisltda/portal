import { textToNumber, numberFormat, valorDesconto } from './../../../core/function.js';
import * as regraDependentes from './../../../shared/regraDependentes.js';

import { getConfig as configFunc } from './../../../config/config.js';
import store from './../../../store/app.js';

const { getIdParcela, getProrata } = store.getters;
const config = configFunc();

export function tabelaPagamento(produto) {
    const parcelas = produto.parcelas.map(function(parcela, index) {
        const descricao = parcela.parcela == -2 ? 'Pix Recorrente' : parcela.parcela == -1 ? 'Boleto Recorrente' : (parcela.parcela == 0 ? 'Cartão de Crédito Recorrente' : parcela.parcela+'x (Mensal)');
        return `<div class="row mb-1 py-1 border-bottom${index === 0 ? ' border-top' :''} no-gutters">
        <div class="col-md-1"><span class="check"></span></div>
        <div class="col-md-4">${descricao}</div>
        <div class="col-md-4">${parcela && textToNumber(parcela.desconto) === 0 ? '' : parcela.desconto+'% Desconto'}</div>
        <div class="col-md-3">${parcela.valor_unico}</div></div>`;
    });
    if (parcelas.length) {
        return `<h5>Forma de Pagamento:</h5>${parcelas.join('')}`;
    }
    return '';
}


export function tabelaPagamentoFinal(produto, plano, dependentes) {
    const desconto = valorDesconto(plano.convenio_tipo, plano.convenio_valor);
    const dependentesCobranca = produto.hasOwnProperty('dependentes') && produto.dependentes.find(function(d) { return textToNumber(d.valor) > 0;});
    const valorDependente = !dependentesCobranca ? 0 : regraDependentes.valorDependentes(produto, plano, dependentes);
    const parcelar = produto.parcelar;
    const parcelas = produto.parcelas.map(function(parcela, index) {
        const parcelaItem =  textToNumber(parcela.parcela);
        const numParcela  = parcelaItem < 1 ? 1 : parcelaItem;
        const valorDependenteFinal = numParcela * valorDependente;
        const valorTotal = (parcela.valor_zerado ? 0 : desconto(textToNumber(parcela.valor_unico))) + valorDependenteFinal;
        const valor = valorTotal / numParcela;
        const msgParcela = parcelar ? parcelaItem+'x ('+numberFormat(valor, 1)+')' : (numParcela === 1 ? 'Mensal' : 'Por '+parcelaItem+' meses');
        const descricao = parcelaItem == -2 ? 'Pix Recorrente' : parcelaItem == -1 ? 'Boleto Recorrente' : (parcelaItem == 0 ? 'Cartão de Crédito Recorrente' : msgParcela);
        return `<div class="row mb-1 py-1 border-bottom${index === 0 ? ' border-top' :''} no-gutters">
        <div class="col-md-1 container-check"><span class="check${parcela.id_parcela === getIdParcela() ? ' active' : ''}"></span></div>
        <div class="col-md-4">${descricao}</div>
        <div class="col-md-4">${parcela && textToNumber(parcela.desconto) === 0 ? '' : parcela.desconto+'% Desconto'}</div>
        <div class="col-md-3">${numberFormat(valorTotal, 1)}</div></div>`;
    });
    if (parcelas.length) {
        return `<h5 class="mt-3">Forma de Pagamento:</h5>${parcelas.join('')}`;
    }
    return '';
}


export function tabelaResumo(produto, plano, dependentes) {
    const desconto = valorDesconto(plano.convenio_tipo, plano.convenio_valor);
    const parcela = getIdParcela() ? produto.parcelas.find(function(p) { return p.id_parcela === getIdParcela();}) : produto.parcelas[0];
    const numParcela = textToNumber(parcela.parcela) < 1 ? 1 : textToNumber(parcela.parcela);
    const valorParcela =  parcela.valor_zerado ? 'R$ 0,00' : (produto.parcelar ? numberFormat(textToNumber(parcela.valor_unico) / numParcela, 1) : parcela.valor_unico)
    const items = [
        {
            id: produto.id, descricao: produto.nome, qtd: 1,
            valor: valorParcela, subtotal: valorParcela
        }
    ];

    const listaDependentes = dependentes.reduce(function(acc, item) {
        const regra = regraDependentes.getRegraDependente(produto.dependentes, item);
        if (regra) {
            const valor = numberFormat((produto.parcelar ? textToNumber(regra.valor) : textToNumber(regra.valor) * numParcela), 1);
            const index = acc.findIndex(function(adicional) { return adicional.id === regra.id; });
            if(index === -1) {
                acc.push({
                    id: regra.id, descricao: 'Dependente', qtd: 1, valor, subtotal: valor
                });
            } else {
                acc[index].qtd++;
                acc[index].subtotal = numberFormat(textToNumber(acc[index].valor) * acc[index].qtd, 1); 
            }
        }
        return acc;
    }, []);

    items.push(...listaDependentes);
    if (textToNumber(produto.taxa_adesao)) {
        const valorAdesao = numberFormat((plano.desconto_adesao ? desconto(textToNumber(produto.taxa_adesao)) : textToNumber(produto.taxa_adesao)), 1);
        const listaTaxaAdesao = textToNumber(config.adesao_dependente) ? dependentes.reduce(function(acc, item) {
            const regra = regraDependentes.getRegraDependente(produto.dependentes, item);
            if (regra && textToNumber(regra.valor)) {
                acc.qtd++;
                acc.subtotal = numberFormat((acc.qtd * textToNumber(acc.valor)), 1);    
            }
            return acc;
        }, {
            id: produto.id, descricao: '*Taxa de Adesão',
            qtd: parcela.valor_zerado ? 0 : 1, valor: valorAdesao, subtotal: valorAdesao
        }) : {
            id: produto.id, descricao: '*Taxa de Adesão',
            qtd: 1, valor: valorAdesao, subtotal: valorAdesao
        };
        items.push(listaTaxaAdesao);
    }
    const prorata = getProrata();
    if (prorata && prorata.prorata) {
        items.push({id: null, descricao: 'Pro rata', 
         qtd: 1,
         valor: numberFormat(prorata.prorata, 1), 
         subtotal: numberFormat(prorata.prorata, 1)});
    }
    if (textToNumber(plano.convenio_valor)) {
        items.push({
            id: null, descricao: '*'+plano.convenio_descricao, qtd: 1, valor: -plano.convenio_valor, subtotal: plano.convenio_valor 
        });
    }

    const template = items.filter(Boolean).map(function(item, index) {
        return `<div class="row mb-1 py-1 border-bottom${index === 0 ? ' border-top' :''} no-gutters">
        <div class="col-md-1">${item.qtd}</div>
        <div class="col-md-5">${item.descricao}</div>
        <div class="col-md-3">${item.valor}</div>
        <div class="col-md-3">${item.subtotal}</div></div>`;
    }).join('');
    return `<h5>Resumo:</h5>${template}`;
}