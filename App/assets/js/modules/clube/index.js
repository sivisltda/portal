import { createItemList, loadInit } from './../../core/route.js';
import { scrollPrecoFloat } from './../../core/function.js';

import * as info from './steps/usuario.js';
import * as listaPlanos from './steps/listaplanos.js';
import * as produtos from './steps/produtos.js';
import * as dependente from './steps/dependente.js';
import * as pagamento from './steps/pagamento.js';
import * as conclusao from './steps/conclusao.js';

import { getConfig as configFunc } from './../../config/config.js';
import store from './../../store/app.js';

const config = configFunc();


export function load(app) {    
    const lista = app.find('#list-item');
    const { getPlanos, getProduto } = store.getters;    
    info.init().eventNext();
    if (getPlanos().length) {
        listaPlanos.init().eventNext();
    }
    produtos.init().eventNext();
    const produto = getProduto();
    if (produto && produto.dependentes && config.ativa_dependente) {
        dependente.init().eventNext();
    }
    if (!config.nao_pag_online) {
        pagamento.init().eventNext();
    }
    conclusao.init().eventNext();
    createItemList(lista);
    loadInit();
    $(window).scroll(scrollPrecoFloat);
    $(document).on('focusin', '[data-toggle=input-mask]', function(e) {
        e.preventDefault();
        const t = $(this);
        t.mask(t.data('mask-format'));
    });
}
