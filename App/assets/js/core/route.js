let etapa = [];
let event = [];
let numeroEtapa = 1;
let numeroEvent = 1;
let listaNavegacao = null;
let isFinal = 0;
let app = null;

import store from '../store/app.js';
import configStore from '../store/config.js';
import * as storageService from './../service/storage.service.js';
/**
 * Função que cria os items das etapas
 * @param list string
 */
export function createItemList(list) {
    listaNavegacao = list;
    const html = etapa.map(function(et) {
        return criaStringItem({link: et.obj.id, title: et.obj.title, etapa: et.numero, tooltip: et.obj.tooltip });
    }).join('');
    listaNavegacao.html(html);
    setTimeout(function() {
        listaNavegacao.find('[data-toggle="tooltip"]').tooltip();
        listaNavegacao.find('a').on('click', function(e) {
            e.preventDefault();
            clickEtapa($(this).data('etapa'));
        });
    }, 500);
}

export function clearRoute() {
    etapa = [];
    event = [];
    numeroEtapa = 1;
    numeroEvent = 1;
    listaNavegacao = null;
    isFinal = 0;
}

export function setApp(item) {
    app = item;
}

export function getApp() {
    return app;
}

export function finalized() {
    isFinal = 1;
}

export function getEtapa() {
    return etapa.find(function(et) {
        return et.numero === numeroEtapa;
    });
}

function criaStringItem({ link, title, etapa, tooltip }) {
    return `<li class="step-item nav-item">
        <a href="${link}" class="nav-link" data-etapa="${etapa}">
            <span class="texto-secundaria" data-toggle="tooltip" data-placement="bottom" title="${title}" data-original-title="${tooltip}">${title}</span>
        </a>
    </li>`;
}

/**
 * Adiciona uma etapa dinamicamente entre período informado
 * @param posicao int
 * @param obj object
 * @param acao function
 */
export function adicionarEtapaEntrePeriodo(posicao, obj, acao) {
    etapa =  etapa.map(function (et) {
        if(et.numero >= posicao) {
            et.numero++;
        }
        return et;
    });
    etapa.splice((posicao - 1), 0, { numero: posicao, obj, acao });
    createItemList(listaNavegacao);
}


export function getNumeroEtapa(id) {
    const index = verificaEtapa(id);
    return index ? etapa[index].numero : false;
}


export function verificaEAdicionaTemplateTab(tabPanel, id, template) {
    const tabs = Array.from(tabPanel.find('.tab-pane'));
    const tab  = tabs.find(function(t) {
        return `#${t.getAttribute('id')}` === id;
    }); 
    if(!tab) {
        tabPanel.append(template);
    }    
}

/**
 * Verifica se tem o id da etapa na lista de eventos
 * @param id
 */
export function verificaEtapa(id) {
    const index = etapa.findIndex(function(et) {
        return et.obj.id === id;
    });
    return (index === -1) ? false : index;
}

/**
 * Remove a etapa do id da lista de eventos
 * @param id
 */
export function removerEtapa(id) {
    const index = verificaEtapa(id);
    if (index) {
        store.setters.setEtapaMaxima(etapa[index].numero - 1);
        removerEvent(etapa[index].numero);
        etapa.splice(index, 1);
        etapa = etapa.map(function (et, i) {
            if(i >= index) {
                et.numero--;
            }
            return et;
        });
        createItemList(listaNavegacao); 
        return true;
    }
    return false;
}

/**
 * Função que incrementar a Etapa
 * @returns int
 */
export function incrementarEtapa() {
    return numeroEvent++;
}

/**
 * Função que adiciona as etapas com seus respectivos eventos
 * @param numero int
 * @param obj object
 * @param acao function
 */
export function eventEtapas(numero, obj, acao) {
    etapa.push({numero, obj, acao });
}

/**
 * Função que ouve o evento através do seu número
 * @param numero int
 */
export function listenEvent(numero) {
    if (!isFinal) {
        numeroEtapa = numero;
        if(numeroEtapa > store.getters.getEtapaMaxima()) {
            store.setters.setEtapaMaxima(numeroEtapa);        
        }
        const v = etapa.find(function (et) {
            return et.numero === numero;
        });
        if (v) {
            const config = configStore.getters.getConfig();
            window.location.hash = v.obj.id;
            config && storageService.setItem(`loja-${config.contrato}`, store.actions.serialize());
            v.acao();
            etapas(numero);
        }
    }
}

export function next(numeroEtapaInt, numero = 1) {
    return numeroEtapaInt + numero;
}

/**
 * Função que adiciona o evento do botão proxima na  lista de eventos
 * @param  numero int
 * @param  acao function
 */
export function eventProximo(numero, acao) {
    const index = event.findIndex(function(ev) {
        return ev.numero === numero;
    });
    if(index === -1) {
        event.push({numero, acao});
    } else {
        event = event.map(function (ev) {
            if(ev.numero >= numero) {
                ev.numero++;
            }
            return ev;
        });
        event.splice((numero - 1), 0, {numero, acao});
    }
}


function removerEvent(numero) {
    const index = event.findIndex(function(ev) {
        return ev.numero === numero;
    });
    event.splice(index, 1);
    event = event.map(function (ev) {
        if(ev.numero > numero) {
            ev.numero--;
        }
        return ev;
    });
}

/**
 * Função que ouve o evento do botão proximo pelo numero indicado
 * @param numero int
 */
export function listenProximo(numero) {
    const v = event.find(function (ev) {
        return ev.numero === numero;
    });
    if (v) {
        v.acao();
    }
}

export function loadInit() {
    const hash = window.location.hash;
    const { getUsuario } = store.getters;
    if (hash && getUsuario()) {
        const numEtapa = getNumeroEtapa(hash);
        listenEvent(numEtapa ? numEtapa :  1)
    } else {
        listenEvent(1);
    }
}

export function clickEtapa(numero) {
    if(store.getters.getEtapaMaxima() >= numero) {
        listenEvent(numero);
    }
}

/**
 * Evento do botão anterior
 * @param e Event
 */
export function clickAnterior(e) {
    e.preventDefault();
    numeroEtapa--;
    listenEvent(numeroEtapa);
}

/**
 * Evento do botão próximo
 * @param e Event
 */
export function clickProximo(e) {
    e.preventDefault();
    listenProximo(numeroEtapa);
}

/**
 * Função de mostra as etapas
 * @param numero int
 */
export function etapas(numero) {
    const width = 100 / (etapa.length - 1);
    const processLine = app.find(".process-line");
    if(processLine) {
        processLine.css({width: width * (numero - 1) + "%"});
    }
    listaNavegacao.find('.nav-item a').parent().removeClass('current');
    app.find("section#main.tab-content > div.tab-pane").removeClass("active");
    const a = listaNavegacao.find(".nav-item:nth-child(" + numero + ")").find("a");
    a.parent().addClass("current");
    app.find("section#main.tab-content").find(a.attr('href')).addClass("active");
}

