
export function createInput(inputs, type, col = 'col-sm-4', row = 'mb-3') {
    let html = "";
    inputs.forEach(function(input) {
        if(type === "column") {
            html+= '<div class="'+input['size']+'">';
            html+= '<div class="form-group'+(input.hasOwnProperty('additional') ? ' item-adicional': '')+'">';
            if (input['label'].length > 0) {            
                html+= '<label for="'+input['id']+'">'+input['label']+'</label>';
            }
        } else if(type === "row") {
            html+= '<div class="form-group row '+ row +(!input.visibility ? ' hide' : '') + (input.left ? ' justify-content-end' : '') +'">';
            if (input['label'].length > 0) {
                html+= '<label class="'+col+' col-form-label text-md-right" for="'+input['id']+'">'+input['label']+'</label>';    
            }
            if(input.hasOwnProperty('withButton')) {
                html+= '<div class="input-button-container '+input['size']+'">';
                html+=input.hasOwnProperty('additional') ? '<div class="item-adicional">' : '';
            }else {
                html+= '<div class="'+input['size']+(input.hasOwnProperty('additional') ? ' item-adicional': '')+'">';
            }
        }
        if(input['type'] === 'select') {
            html+= createSelect(input);
        } else if(input['type'] === 'radio') {
            html+= createRadio(input);
        } else if(input['type'] === 'switch') {
            html+= createSwitch(input);
        } else {
            html+= createInputText(input);
        }
        html+='<div class="invalid-feedback"></div>';
        html+= createAdicional(input);
        html+= createInputButton(input);
        html+="</div></div>";
    });
    return html;
}

function createSwitch(input) {
    return `<div><input type="checkbox" id="${input['id']}" name="${input['name']}"
    ${input['value'] == 1 ? 'checked' : ''}
     value="${input['value']}" data-switch="primary">
            <label for="${input['id']}" data-on-label="${input['attributes']['label']['on']}"
            data-off-label="${input['attributes']['label']['off']}"></label></div>`;
}

function createSelect(input) {
    let html ='<select name="'+input['name']+'" id="'+input['id']+'" ';
    for(let a in input['attributes']) {
        if(a !== 'placeholder') html+=a+'="'+input['attributes'][a]+'" ';
    }                
    html+=input['required'] ? 'required' : ''; 
    html+='>';
    if(input['attributes'].hasOwnProperty('placeholder')) {
        html+='<option value="">'+input['attributes']['placeholder']+'</option>';
    }
    if(input['attributes'].hasOwnProperty('optionsId') && input['attributes'].hasOwnProperty('optionsValue')) {
        for(let o in input['options']) {
            html+='<option value="'+input['options'][o][input['attributes']['optionsId']]+'">'+input['options'][o][input['attributes']['optionsValue']]+'</option>';
        }        
    } else {
        for(let o in input['options']) {
            html+='<option value="'+o+'">'+input['options'][o]+'</option>';
        }    
    }
    html+='</select>';
    return html;
}

function createInputText(input) {
    let html ='<input type="'+input['type']+'" name="'+input['name']+'" id="'+input['id']+'" ';
    for(let a in input['attributes']) {
        html+=a+'="'+input['attributes'][a]+'" ';
    }
    html+=input['required'] ? 'required' : ''; 
    html+='/>'; 
    return html;
}

function createRadio(input) {
    let html ='';
    for(let o in input['options']) {
        html+='<input type="radio" id="'+input['id']+o+'" name="'+input['name']+'" value="'+o+'" class="m-1">';
        html+='<label for="'+input['id']+o+'">'+input['options'][o]+'</label>';
    }
    return html;
}

function createAdicional(input) {
    if(input.hasOwnProperty('additional')) {
        return input.additional.map(function (e) {
            let template = "";
            switch (e.type) {
                case 'prepend':
                    template = `<span class="prepend ${e.class}"></span>`;
                    break;
                default:
                    template = `<a class="btn-adicional ${e.class}">${e.icone}</a>`;
                    break;
            }
            return template;
        }).join('');
    }
    return '';
}

function createInputButton(input) {
    if(input.hasOwnProperty('withButton')) {
        const {id, label, classe} = input.withButton;
        return `${input.hasOwnProperty('additional') ? '</div>' : ''}<div class="input-button">
                <button class="btn btn-amd ${classe}" type="button" id="${id}">${label}</button>
              </div>`;
    }
    return '';
}