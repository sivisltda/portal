import {getEtapa, getApp} from './route.js';

export function geraErrors(lista, chave, error) {
    if(lista.hasOwnProperty(chave)) {
        const code = error.status;
        const item = lista[chave].find(function(erro) {
            return erro.code == code;
        });
        item && item.action(error);
    }
}

export function errorCamposInvalidos(form, errors) {
    const inputs = Object.keys(errors).reduce(function(acc, i) {
        const input = form.find('[name="'+i+'"]');
        if (input.length) {
            acc[i] = input;
        }
        return acc;
    }, {});
    const keysInputs = Object.keys(inputs);
    keysInputs.forEach(function(i) {
        inputs[i].addClass('is-invalid').siblings('.invalid-feedback').text(errors[i]);
    });
    let top =  0;
    if (keysInputs.length === 1) {
       top = inputs[keysInputs[0]].offset().top;
    } else {
        top = keysInputs.map(function(i) {
            return inputs[i].offset().top;
        }).reduce(function(a, b) {
            return Math.min(a, b);
        }, 1000);
    }
    $(window).scrollTop(top);  
}

/**
 * Função que converte a data para formato ISO
 * @param data string
 * @returns string
 */
export function convertDataIso(data) {
    if (data !== "" && data !== null && data !== undefined) {
        if(/^\d{4}-\d{2}-\d{2}$/.test(data)) {
            return data;
        }else if(/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/.test(data)) {
            const [dia, mes, ano] = data.split("/");
            return ano + "-" + mes + "-" + dia;
        }
    }
}

export function convertDataTempoIso(sData) {
    if (sData !== "" && sData !== null) {
        const [data, tempo] = sData.split(/\s/);
        const [dia, mes, ano] = data.split("/");
        return (ano + "-" + mes + "-" + dia+' '+(tempo ? tempo : '')).trim();
    }
}


/**
 * Função que converte a data para formato BR
 * @param data string
 * @return string
 */
export function convertDataBr(data) {
    if (data !== "" && data !== null) {
        if(/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/.test(data)) {
            return data;
        }else if(/^\d{4}-\d{2}-\d{2}$/.test(data)) {
            const [ano, mes, dia] = data.split("-");
            return dia + "/" + mes + "/" + ano;
        }
    }
}

/**
 * Função que converte o serialize para objeto
 * @param text string
 * @returns object
 */
export function convertSerializeObject(text) {
    const partials = text.split("&");
    return partials.reduce(function (acc, cur, i) {
        const [nome, valor] = cur.split("=");
        acc[nome] = decodeURIComponent(valor.trim());
        return acc;
    }, {});
}

/**
 * Função que retorna a diferença de idade
 * @param data_nascimento string
 * @returns int
 */
export function verificaIdade(data_nascimento) {
    const hoje = new Date().getTime();
    data_nascimento = convertDataIso(data_nascimento);
    const birth = new Date(data_nascimento).getTime();
    return Math.floor((((((hoje - birth) / 1000) / 60) / 60) / 24) / 365.25);
}

export function datasMaior(data1, data2) {
    const timedata1 = convertDataTempoIso(data1);
    const timedata2 = convertDataTempoIso(data2);
    return timedata1 >= timedata2;
}

export function datasMenor(data1, data2) {
    const timedata1 = convertDataTempoIso(data1);
    const timedata2 = convertDataTempoIso(data2);
    return timedata1 < timedata2;
}


/**
 * Função que limpa campo de um formulário
 * @param form
 */
export function clearForm(form) {
    const fields = convertSerializeObject(form.serialize());
    $.each(fields, function(i, v) {
        $('input[name="'+i+'"], textarea[name="'+i+'"], select[name="'+i+'"]').val("");
    });
}

export const debounce = function(func, wait, immediate) {
    let timeout;
    return function() {
        let context = this, args = arguments;
        let later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};


/**
 * Funçao que verifica se o dispositivo é mobile
 * @returns boolean
 */
export function isMobile() {
    const userAgent = navigator.userAgent.toLowerCase();
    if (userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i) != -1)
        return true;
}

/**
 * Função que que cria o scroll do preço
 */
 export function scrollPrecoFloat() {
    const app               = getApp();
    const altura            = $(window).scrollTop();
    const etapaSelecionada  = getEtapa();
    const indiceEtapa       = app.find(etapaSelecionada.obj.id);
    const alturaDocumento   = $(document).height();
    const menu              = indiceEtapa.find('.menu-right');
    //const scrollHeight      = document.documentElement.scrollHeight;
    if (menu.length) {
        //console.log(menu.offset().top, altura, scrollHeight);
        if (altura > 100) {
            menu.closest('#app').addClass('fixed-menu'); 
        } else {
            menu.closest('#app').removeClass('fixed-menu'); 
        }
        if (altura + $(window).height() >= alturaDocumento) {
            menu.addClass('h-footer').removeClass('h100');
        } else {
            menu.addClass('h100').removeClass('h-footer');
        }
    }
}



export function scrollMenuRight() {
    const app = getApp();
    const altura = $(window).scrollTop();
    const items =  $(document).find('.menu-lateral-right');
    const item = Array.from(items).find(function(i) {
        return $(i).width() > 0 && $(i).height() > 0;
    });
    if(item) {
        const valtura = item.closest('.tab-pane').offsetTop;
        const alturaDocumento = $(document).height();
        const alturaJanela = $(window).height();
        /* console.log(
            'altura => '+altura, 
            'valtura => '+valtura, 
            'alturaJanela => '+alturaJanela,
            'alturaDocumento => '+alturaDocumento
        ); */
        if (altura >= valtura) {
            if(!app.hasClass('menu-fixo')) $('body').addClass('menu-fixo');
        } else {
            if(app.hasClass('menu-fixo')) $('body').removeClass('menu-fixo');
        }

        if (altura +  alturaJanela + 335 >= alturaDocumento) {
            $(item).css('height', 'calc(100vh - 335px)');
        } else {
            $(item).css('height', '100%');
        }
    }
}

/**
 * Formata número
 * @param num
 * @param ident
 * @returns {string}
 */
export function numberFormat(num, ident = 0) {
    let x = 0;
    if (num < 0) {
        num = Math.abs(num);
        x = 1;
    }
    if (isNaN(num))
        num = '0';
    let cents = Math.floor((num * 100 + 0.5) % 100);
    num = Math.floor((num * 100 + 0.5) / 100).toString();
    if (cents < 10)
        cents = '0' + cents;
    for (let i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + '.'
            + num.substring(num.length - (4 * i + 3));
    let ret = num + ',' + cents;
    if (x === 1)
        ret = ' - ' + ret;
    return ident === 0 ? ret : 'R$' + " " + ret;
}

/**
 * Converte texto para número
 * @param valor
 * @returns {Number}
 */
export function textToNumber(valor) {
    valor = valor ? valor.replace(/\ /g, "").replace('R$', "").replace("–", "-").replace(/\./g, "").replace(/\,/g, ".") : 0;
    return isNaN(valor) === false ? parseFloat(valor) : parseFloat(0);
}

export function valorDesconto(tipo, desconto) {
    return function(valor) {
        return valor - (tipo ? (tipo == 'D' ? textToNumber(desconto) : textToNumber(desconto) / 100 *  valor) : 0);
    }
};

/**
 * Anima o número
 * @param target
 * @param numero
 * @param periodo
 */
export function animaNumero(target, numero, periodo = 'mensal') {
    numero = parseFloat(numero).toFixed(2).replace('.', ',');
    const [reais, centavos] = numero.split(',');
    for (let i = 0; i <= 50; i++) {
        if (i === 50) {
            setTimeout(function () {
                target.html(templateNumero(reais, centavos, periodo));
            }, 1);
        } else {
            setTimeout(function () {
                target.html(templateNumero(Math.floor((Math.random() * 50) + 200), Math.floor((Math.random() * 99) + 10), periodo));
            }, 1);
        }
    }
}

export function templateNumero(reais, centavos, periodo = '') {
    return `<span class="valor-real">R$ ${reais}</span><sub class="valor-centavo">,${centavos}</sub><sub class="mes">${periodo}</sub>`;
}

export function geraEventos(eventos) {
    for (let i = 0;
         i < eventos.length; i++) {
        for (let a = 0; a < eventos[i].actions.length; a++) {
            const evento = eventos[i].actions[a].event;
            eventos[i].root.find(eventos[i].target).off(evento).on(evento, eventos[i].actions[a].action);
        }
    }
}

export function getListaPorTipo(lista, campo, valor) {
    return lista.find(function (item) {
        return item[campo] === valor;
    });
}

export function selecionaOption(campo, valor, callback) {
    let item = Array.from(campo.find('option')).find(function(option) {
        return option.innerText.replace(/\s/g, '').toLowerCase() === valor.replace(/\s/g, '').toLowerCase();
    });

    if(!item) {
        let find_modelo = valor.split(" ");
        for (let i = find_modelo.length; i > 0; i--) {
            let item_join = find_modelo.slice(0, i).join(' ').replace(/\s/g, '').toLowerCase();
            if (!item) {
                item = Array.from(campo.find('option')).find(function(option) {
                    return option.innerText.replace(/\s/g, '').toLowerCase().includes(item_join);
                });
            }
        }
    }

    if(item && item.value) {
        campo.val(item.value).change();
    }else {
        campo.val(valor).change();
        callback && callback();
    }
}

export function slug(frase) {
    return frase.toLowerCase()
        .replace(/[àÀáÁâÂãäÄÅåª]+/g, 'a')       // Special Characters #1
        .replace(/[èÈéÉêÊëË]+/g, 'e')       	// Special Characters #2
        .replace(/[ìÌíÍîÎïÏ]+/g, 'i')       	// Special Characters #3
        .replace(/[òÒóÓôÔõÕöÖº]+/g, 'o')       	// Special Characters #4
        .replace(/[ùÙúÚûÛüÜ]+/g, 'u')       	// Special Characters #5
        .replace(/[ýÝÿŸ]+/g, 'y')       		// Special Characters #6
        .replace(/[ñÑ]+/g, 'n')       			// Special Characters #7
        .replace(/[çÇ]+/g, 'c')       			// Special Characters #8
        .replace(/[ß]+/g, 'ss')       			// Special Characters #9
        .replace(/[Ææ]+/g, 'ae')       			// Special Characters #10
        .replace(/[Øøœ]+/g, 'oe')       		// Special Characters #11
        .replace(/[%]+/g, 'pct')       			// Special Characters #12
        .replace(/\s+/g, '-')           		// Replace spaces with -
        .replace(/[^\w\-]+/g, '')       		// Remove all non-word chars
        .replace(/\-\-+/g, '-')         		// Replace multiple - with single -
        .replace(/^-+/, '')             		// Trim - from start of text
        .replace(/-+$/, '');            		// Trim - from end of text
}

export function limitaTexto(msg, tamanho) {
    if (msg.length > tamanho) {
        msg = msg.substr(0, (tamanho - 3));
        const corte = msg.lastIndexOf(' ');
        msg = msg.substr(0, corte)+'...';
    }
    return msg;

}

export function criarDataTable(qtd, columns, url, callback) {
    let configuracao =  {
        "iDisplayLength": qtd,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "bLengthChange": false,
        "bFilter": false,
        "ordering": false,
        "bAutoWidth": false,
        ...columns,
        "oLanguage": {
            'oPaginate': {
                'sFirst': "Primeiro",
                'sLast': "Último",
                'sNext': "Próximo",
                'sPrevious': "Anterior"
            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"
        },
        "sPaginationType": "full_numbers",
        drawCallback: callback
    };

    if(url) {
        configuracao = Object.assign(configuracao, {"sAjaxSource": url});
    }
    return configuracao;
}

function isType(o,t) {    return (typeof o).indexOf(t.charAt(0).toLowerCase()) === 0;}

// Here's the meat and potatoes
export  function image(src,cfg) {
    let img, prop, target;
    cfg = cfg || (isType(src,'o') ? src : {});
    img = $(src);
    if (img) {
        src = cfg.src || img.src;
    } else {
        img = document.createElement('img');
        src = src || cfg.src;
    }

    if (!src) {
        return null;
    }

    prop = isType(img.naturalWidth,'u') ? 'width' : 'naturalWidth';
    img.alt = cfg.alt || img.alt;

    // Add the image and insert if requested (must be on DOM to load or
    // pull from cache)
    img.src = src;

    target = $(cfg.target);
    if (target) {
        target.insertBefore(img, $(cfg.insertBefore) || null);
    }

    // Loaded?
    if (img.complete) {
        if (img[prop]) {
            if (isType(cfg.success,'f')) {
                cfg.success.call(img);
            }
        } else {
            if (isType(cfg.failure,'f')) {
                cfg.failure.call(img);
            }
        }
    } else {
        if (isType(cfg.success,'f')) {
            img.onload = cfg.success;
        }
        if (isType(cfg.failure,'f')) {
            img.onerror = cfg.failure;
        }
    }

    return img;
}

export function blurInput(form, excetoInput, validacao) {
    form.on('keyup blur change', ':input', function(e) {
        e.preventDefault();
        if (!excetoInput.includes(e.target.name)) {
            const t = $(this);
            const elementoPai = t.closest('.form-group');
            const elementoIrmao = elementoPai.next('.form-group');
            const validate = validacao(t);
            if (elementoIrmao.length && validate) {
                elementoIrmao.removeClass('hide');
                const input = elementoIrmao.find(':input')
                input.attr('type') && ['checkbox', 'switch', 'radio'].includes(input.attr('type')) && input.change();
            } 
        }
    });
}

export function formataNumero(numero, periodo) {
    numero = textToNumber(numero).toFixed(2).replace('.', ',');
    const [reais, centavos] = numero.split(',');
    return `<span class="valor-real">R$ ${reais}</span>
    <sub class="valor-centavo">, ${centavos}</sub>
    ${periodo ? '<sub class="mes">'+periodo+'</sub>': ''}`;
}

export function buscarCPF(e) {
    var options = {
        onKeyPress: function (cpf, ev, el, op) {
            var masks = ['000.000.000-000', '00.000.000/0000-00'];
            e.mask((cpf.length > 14) ? masks[1] : masks[0], op);
        }
    }
    e.length > 11 ? e.mask('00.000.000/0000-00', options) : e.mask('000.000.000-00#', options);
}

export function montaCartaoCredito({form, container, number, expiry, cvc, name, submit}) {
    const payment_form = new DatPayment({
        form_selector: form,
        card_container_selector: container,
        number_selector: number,
        date_selector: expiry,
        cvc_selector: cvc,
        name_selector: name,
        submit_button_selector: submit,
        placeholders: {
            number: '•••• •••• •••• ••••',
            expiry: '••/••••',
            cvc: '•••',
            name: 'NOME CARTÃO'
        },
        
    });
    /*
    validators: {
            number: function (number) {
                //return Stripe.card.validateCardNumber(number);
            },
            expiry: function (expiry) {
                expiry = expiry.split(' / ');
                //return Stripe.card.validateExpiry(expiry[0] || 0, expiry[1] || 0);
            },
            cvc: function (cvc) {
                //return Stripe.card.validateCVC(cvc);
            },
            name: function (value) {
                return value.length > 0;
            }
        }
    */
   /*  payment_form.form.addEventListener('payment_form:submit', function (e) {
        //var form_data = e.detail;
        //payment_form.unlockForm();
    });
    payment_form.form.addEventListener('payment_form:field_validation_success', function (e) {
        var input = e.detail;
    });
    payment_form.form.addEventListener('payment_form:field_validation_failed', function (e) {
        var input = e.detail;
    }); */
}


export function ativarCarrousel(container, items, options = {}) {
    if (container.hasClass('slick-initialized')) {
        container.slick('destroy');
    }
    container.html(items);
    container.slick({
        dots: options.hasOwnProperty('dots') ? options.dots : false,
        arrows: options.hasOwnProperty('arrows') ? options.arrows : false,
        infinite: options.hasOwnProperty('infinite') ? options.infinite : false,
        speed: options.hasOwnProperty('speed') ? options.speed : 300,
        slidesToShow: options.hasOwnProperty('slidesToShow') ? options.slidesToShow : 3,
        slidesToScroll: options.hasOwnProperty('slidesToScroll') ? options.slidesToScroll : 3,
        responsive: options.hasOwnProperty('responsive') ? options.responsive: [],
        prevArrow: '<button type="button" class="nav-carrousel prev"><i class="mdi mdi-chevron-left"></i></button>',
        nextArrow: '<button type="button" class="nav-carrousel next"><i class="mdi mdi-chevron-right"></i></button>',
    });
}

export function goStepCarrousel(container, search) {
    const steps = Array.from(container.find('.slick-slide'));
    const step = steps.findIndex(function(i) {
        return i.classList.contains(search);
    });
    step > -1 && container.slick('slickGoTo', step);
}

export function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export function isExistsTag(nome) {
    const partes = window.location.pathname.split('/').filter(Boolean);
    const key = partes.indexOf(nome);
    const valor = key > -1 ? partes[key + 1] : false;
    return valor;
}


export function maskMoney(campo) {
    const MoneyOpts = {
        reverse:true,
        maxlength: false,
        placeholder: '0,00',
        onKeyPress: function(v, ev, curField, opts) {
            const mask = curField.data('mask').mask;
            const decimalSep = (/0(.)00/gi).exec(mask)[1] || ',';
            if (curField.data('mask-isZero') && curField.data('mask-keycode') == 8)
            $(curField).val('');
            else if (v) {
            // remove previously added stuff at start of string
            v = v.replace(new RegExp('^0*\\'+decimalSep+'?0*'), ''); //v = v.replace(/^0*,?0*/, '');
            v = v.length == 0 ? '0'+decimalSep+'00' : (v.length == 1 ? '0'+decimalSep+'0'+v : (v.length == 2 ? '0'+decimalSep+v : v));
            $(curField).val(v).data('mask-isZero', (v=='0'+decimalSep+'00'));
            }
        }
    };
    campo.mask("#.##0,00", MoneyOpts);
}
