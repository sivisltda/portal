let numero = 0;
let inscritos = [];

export function subscribe(acao) {
    inscritos.push(acao);
}


export function asc() {
    numero++;
    enviar();
}

export function des() {
    numero--;
    enviar();
}

function enviar() {
    inscritos.forEach(function(inscrito) {
        inscrito(numero);
    });
}
