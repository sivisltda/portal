import store from './../store/app.js';
import {getBuscarDadosUsuario} from './../service/usuario.sevice.js';

const {getCoresTema} = store.getters;

export function requisitaCPF(usuario, callback, errCallback) {
  const coresTema = getCoresTema();
  return {
    input: 'text',
    type: 'warning',
    confirmButtonText: 'Ok',
    cancelButtonText: 'Cancelar',
    cancelButtonColor: '#9E9E9E',
    confirmButtonColor: coresTema.cor_secundaria,
    allowEscapeKey: false,
    allowOutsideClick: false,
    showCancelButton: true,
    title: 'Atenção',
    html: '<strong class="text-center">Atenção, '+usuario.razao_social+'</strong><br/> '
    +'Verificamos que seu e-mail já possui um cadastro feito anteriormente, para continuar' 
    + ' é necessário que informe os **03 primeiros dígitos do seu CPF/CNPJ**',
    inputPlaceholder: 'Digite seu CPF',
    showLoaderOnConfirm: true,
    inputAttributes: {
      "class": "form-control",
      "maxlength": 3,
      "minlength": 3
    },
    inputValidator: (value) => {
      const cnpj = value.replace(/\D/g, '');
      if (cnpj.length !== 3) {
        return 'CPF inválido';
      }
      return new Promise(function(resolve) {
        getBuscarDadosUsuario({cnpj}, function(data) {
          callback(data);
          resolve();
        }, function(erro) {
          const msg = erro.responseJSON.erro;
          if (erro.status === 403) {
            chamaSwalQueue({type: "warning", title: "Plano Ativo", html: `Atenção, você já possui um plano Ativo!`,  
            showCancelButton: false, confirmButtonText: 'OK' }, function(res) {
              errCallback();
            });
            resolve(msg)
          } else {
            swal('Erro', msg, 'error').then(function() {
              chamaSwalQueue(requisitaCPF(usuario, callback, errCallback), function(res) {
                if (res.hasOwnProperty('dismiss')) {
                  if ([Swal.DismissReason.esc, Swal.DismissReason.cancel].includes(res.dismiss)) {
                    errCallback();
                  }
                }
              });
            });
            resolve(msg);
          }
        });
      });
    }
  };
}

export function chamaSwalQueue(objConfig, callback) {
  const coresTema = getCoresTema();
  Swal.mixin({
    confirmButtonText: 'Avançar',
    cancelButtonText: 'Cancelar',
    confirmButtonColor: coresTema.cor_secundaria,
    cancelButtonColor: '#9E9E9E',    
    allowEscapeKey: false,
    allowOutsideClick: false,
    showCancelButton: true,
    keydownListenerCapture: false
  }).queue([objConfig]).then(function(res) {
    callback(res);
  });
}