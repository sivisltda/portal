import * as loading from './../core/loading.js';

/**
 * Faz a requisicao ao servidor
 * @param url string
 * @param method string
 * @param data Object
 * @param success Function
 * @param error Function|null
 * @param type String|null
 * @param options Object|null
 */
export default function request(url, method, data, success, error = null, type = 'json', options = {}) {
    $.ajax({
        url: url,
        method: method,
        dataType: type,
        data: data,
        beforeSend: function() {
            loading.asc();
        },
        success: success,
        error: error,
        complete: function() {
            loading.des();
        },
        ...options
    });
}
