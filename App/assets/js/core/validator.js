
import { convertSerializeObject, verificaIdade, convertDataBr } from './function.js';

export function validate(rules, form) {
    const errors = [];
    const obj = convertSerializeObject(form.serialize());
    for (let e in obj) {
        const input = form.find('[name="'+e+'"]');
        if(input.length && input.is(":visible")) {
            let id = input.attr('name');
            if (rules.hasOwnProperty(id)) {
                const validate = rulesValidation(form, rules[id].rule, obj[e], rules[id].message, obj, input);
                if (validate) {
                    errors.push(Object.assign({}, { field: id }, validate));
                }
            }
        }
    }
    return errors;
}

const messagesValidacao = {
    required: 'O campo é obrigatório',
    requireIf: 'O campo é obrigatório',
    email: 'Email inválido',
    confirm: 'Confirmação não combina com :campo',
    min: 'O mínimo de caracteres é :min',
    max: 'O mínimo de caracteres é :max',
    placa: 'Placa Inválida',
    cpf: 'CPF inválido',
    requiredMaxAge: 'Apartir de :idade anos esse campo é obrigatório',
    creditCard: 'Cartão inválido'
};

export function rulesValidation(form, rule, value, message, obj, input) {
    if(rule) {
        const rules = rule.includes("|") ? rule.split("|") : [rule];
        const messageReturn = function(r, message) {
            return message.hasOwnProperty(rules[r]) ? { message: message[rules[r]] } : { message };
        };
        for (let r in rules) {
            switch (true) {
                case rules[r] === "required":
                    if (value === "") return messageReturn(r, message);
                    break;
                case /^requiredIf:/.test(rules[r]):
                    const nameField = rules[r].replace(/^requiredIf:/, '');
                    const field = form.find('[name="'+nameField+'"]');
                    const typeField = field.attr('type');
                    const isChecked = typeField === 'checkbox' ? field.is(':checked') : field.val();
                    if (isChecked && value === "") {
                        return messageReturn(r, message);
                    }
                    break;    
                case rules[r] === "email":
                    if (value && !validate_email(value))
                        return messageReturn(r, message);
                    break;
                case /^confirm:/.test(rules[r]):
                    const fieldC = rules[r].replace(/^confirm:/, '');
                    if(form.find('[name="'+fieldC+'"]').val() !== value) {
                        return { message: message['confirm'] };
                    }
                    break;
                case /^min:/.test(rules[r]):
                const min = rules[r].replace(/^min:/, '');
                if(value.length < parseInt(min)) {
                    return { message: message['min'].replace(/:min/, min) };
                }
                break;
                case rules[r] === "date":
                    if (value) {
                        const patternValidaData = /^(((0[1-9]|[12][0-9]|3[01])([-.\/])(0[13578]|10|12)([-.\/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([-.\/])(0[469]|11)([-.\/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([-.\/])(02)([-.\/])(\d{4}))|((29)(\.|-|\/)(02)([-.\/])([02468][048]00))|((29)([-.\/])(02)([-.\/])([13579][26]00))|((29)([-.\/])(02)([-.\/])([0-9][0-9][0][48]))|((29)([-.\/])(02)([-.\/])([0-9][0-9][2468][048]))|((29)([-.\/])(02)([-.\/])([0-9][0-9][13579][26])))$/;
                        if (!(patternValidaData.test(value) || patternValidaData.test(convertDataBr(value)))) {
                            return { message: 'Data inválida' };
                        }
                    }
                break;    
                case /^max:/.test(rules[r]):
                    const max = rules[r].replace(/^max:/, '');
                    if(value.length > parseInt(max)) {
                        return { message: message['max'].replace(/:max/, max) };
                    }
                    break;
                case rules[r] === "cpf":
                    if(value) {
                        if(!(validate_cpf(value))) {
                            return messageReturn(r, message);
                        }
                    }
                break;
                case rules[r] === "cnpj":
                    if(value) {
                        if(!(validate_cnpj(value))) {
                            return messageReturn(r, message);
                        }
                    }
                break;
                case rules[r] === "cpf-cnpj":
                    if(value) {
                        if(!(validate_cpf(value) || validate_cnpj(value))) {
                            return messageReturn(r, message);
                        }
                    }
                break;
                case rules[r] === "placa":
                    if(value) {
                        if(!(/^[a-zA-Z]{3}-?[0-9]{4}$/.test(value) || /^[a-zA-Z]{3}-?[0-9]{1}[a-zA-Z]{1}[0-9]{2}$/.test(value))) {
                            return { message: 'Placa inválida'};
                        }
                    }
                break;
                case /^requiredMaxAge:/.test(rules[r]):
                    const age = rules[r].replace(/^requiredMaxAge:/, '');
                    const data_nascimento = obj.data_nascimento;
                    if((data_nascimento && verificaIdade(data_nascimento) > age) || obj.parentesco !== "3") {
                        if(!value) {
                            return { message: message['requiredMaxAge'].replace(/:age/, age) };
                        }
                    }
                break;
                case rules[r] === "creditCard":
                    if (value) {
                       const result = $(input).validateCreditCard();
                       if(!result.valid) {
                            return { message: 'Cartão inválido'};
                       }
                    }
                break;
                case rules[r] === "mesano":
                    if (value) {
                        value = value.replace(/\s/g, '');
                        if (!(/^(0[1-9]|1[012])[\/\-]\d{4}$/.test(value))) {
                            return { message: 'Mês/Ano inválido' };
                        }
                    }
                break;

            }
        }
        return false;
    }
}

export function addFieldRules(rules, field, rule, message) {
    if(rules.hasOwnProperty(field)) {
        const rulesObj  = rules[field].rule.includes('|') ? rules[field].rule.split('|') : [];
        const nameMessage = rule.includes(':') ? rule.substr(0, rule.indexOf(':')) : rule;
        if (!rules[field].rule.includes(rule)) {
            rulesObj.push(rule);
            rules[field].rule = rulesObj.join('|');
            rules[field].message[nameMessage] = message;
        }
    }
    return rules;

}


export function toggleFieldRules(rules, field, ruleNew, ruleOld, message) {
    const nameMessage = ruleNew.includes(':') ? ruleNew.substr(0, ruleNew.indexOf(':')) : ruleNew;
    if(rules.hasOwnProperty(field)) {
        if(rules[field].rule.includes(ruleOld)) {
            const rulesField  = rules[field].rule.includes('|') ? rules[field].rule.split('|') : [ruleOld];
            const rulesObj = rulesField.map(function(v, i) {
                if(v === ruleOld) {
                    return ruleNew;
                }
                return v;
            });
            rules[field].rule = rulesObj.join('|');
            rules[field].message[nameMessage] = message;
        }
    }
    return rules;
}

export function removeFieldRules(rules, field, ruleOld) {
    if(rules.hasOwnProperty(field)) {
        if(rules[field].rule.includes(ruleOld)) {
            const rulesField  = rules[field].rule.includes('|') ? rules[field].rule.split('|') : [ruleOld];
            const rulesObj = rulesField.filter(function(v,i) {
                return v !== ruleOld;
            });
            rules[field].rule = rulesObj.join('|');
        }
    }
    return rules;
}


export function validate_cnpj(val) {
    if(val.length === 18) {
        if (val.match(/^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/) != null) {
            const val1 = val.substring(0, 2);
            const val2 = val.substring(3, 6);
            const val3 = val.substring(7, 10);
            const val4 = val.substring(11, 15);
            const val5 = val.substring(16, 18);
    
            let i;
            let number;
            let result = true;
    
            number = (val1 + val2 + val3 + val4 + val5);
    
            let s = number;
    
            let c = s.substr(0, 12);
            let dv = s.substr(12, 2);
            let d1 = 0;
    
            for (i = 0; i < 12; i++)
                d1 += c.charAt(11 - i) * (2 + (i % 8));
    
            if (d1 == 0)
                result = false;
    
            d1 = 11 - (d1 % 11);
    
            if (d1 > 9)
                d1 = 0;
    
            if (dv.charAt(0) != d1)
                result = false;
    
            d1 *= 2;
            for (i = 0; i < 12; i++) {
                d1 += c.charAt(11 - i) * (2 + ((i + 1) % 8));
            }
    
            d1 = 11 - (d1 % 11);
            if (d1 > 9)
                d1 = 0;
    
            if (dv.charAt(1) != d1)
                result = false;
    
            return result;
        }
    }
    return false;
}

export function validate_cpf(strCPF) {
    if (strCPF.length === 14) {
        strCPF = strCPF.replace('.', '');
        strCPF = strCPF.replace('.', '');
        strCPF = strCPF.replace('-', '');

        var cpf = strCPF;
        var numeros, digitos, soma, i, resultado, digitos_iguais;
        digitos_iguais = 1;
        if (cpf.length < 11)
            return false;
        for (i = 0; i < cpf.length - 1; i++)
            if (cpf.charAt(i) != cpf.charAt(i + 1)) {
                digitos_iguais = 0;
                break;
            }
        if (!digitos_iguais) {
            numeros = cpf.substring(0, 9);
            digitos = cpf.substring(9);
            soma = 0;
            for (i = 10; i > 1; i--)
                soma += numeros.charAt(10 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0))
                return false;
            numeros = cpf.substring(0, 10);
            soma = 0;
            for (i = 11; i > 1; i--)
                soma += numeros.charAt(11 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1))
                return false;
            return true;
        }
        else
            return false;
    }
}

export function validate_email(email) {
    if ((/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email))
    && !validaAcentuacao(email)
    ) {
        return true;
    } 
    return false;
}

export function validaAcentuacao(texto) {
    return (/[áàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+/.test(texto));
}

export function validacaoPlaca(placa) {
    placa = placa.replace(/-/, '');
    const regexPlaca = /^[a-zA-Z]{3}[0-9]{4}$/;
    const regexPlacaMerc = /^[a-zA-Z]{3}[0-9]{1}[a-jA-J]{1}[0-9]{2}$/;
    if(regexPlaca.test(placa) || regexPlacaMerc.test(placa)){
        return true;
    }
    return false;
}

export function focusErrosInputs(form, input) {
    if(form.rules.hasOwnProperty(input.prop('name'))) {
        input.removeClass('is-invalid').siblings('.invalid-feedback').text('');
        const rule = form.rules[input.prop('name')];
        const validate = rulesValidation(form, rule.rule, input.val(), rule.message, {}, input);
        if(validate) {
            input.addClass('is-invalid').siblings('.invalid-feedback').text(validate.message);
            return false;
        }
    }
    return true;    
}


export function validacaoInput(form) {
    return function(input) {
        if(form.hasOwnProperty('rules') && form.rules.hasOwnProperty(input.prop('name'))) {
            return focusErrosInputs(form, input);
        }        
        return true;
    };   
}
