import * as menuService from '../service/menu.service.js';
import * as adminService from '../service/admin.service.js';

const listaMenu         = $('#menu-side-nav');
const menuPerfil        = $('.menu-perfil-usuario');

$(document).ready(function() {
    //menuService.preencheLista(listaMenu);
    
    $(document).on('click', '.logout', function(e) {
        e.preventDefault();
        menuService.logout(function() {
            window.location.href = 'login';
        }, function(erro) {
            console.log(erro);
            window.location.href = 'login';
        });
    });

    if (menuPerfil.length) {
        menuPerfil.find('.link-menu-perfil').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            console.log(e);
            const t = $(this);
            t.closest('.menu-perfil-usuario').toggleClass('active');
        });

        menuPerfil.find('#tipoUsuario').on('change', function (e) {
            e.preventDefault();
             adminService.trocarSessao($(this).val(), function() {              
                window.location.href = 'dashboard';
            }, function(erro) {
                console.log(erro);
                swal('erro','Ocorreu um erro ao alterar a sessão', 'error');
            });
         });
    }


});