import request from "./../core/request.js";
import { getConfig as configFunc } from './../config/config.js';
import store from './../store/app.js';

const { setPlanos, setPlano } = store.setters;
const { getPlanos } = store.getters;
const config = configFunc();

export function processarPagamento(data, callback, errCallback, dashboard = 0) {
    const route = dashboard ? 'dashboard' : 'portal';
    request(config.api+route+'/pagamento', 'post', Object.assign(data, { functionPage: 'processarPagamento'}), function(res) {
        if (dashboard) {
            atualizaPlanos(res.plano, data.id_mens);
        }
        if (res.response.TotalSucesso) {
            callback(res.response);
        } else {    
            errCallback(1, 'Ocorreu um erro ao efetuar o pagamento');
        }
    }, function(erro) {
        console.log(erro);
        errCallback(2, erro.responseJSON ? erro.responseJSON.erro : 'Ocorreu um erro ao efetuar o pagamento');
    });
}

export function alterarCartao(data, callback, errCallback) {
    request(config.api+'dashboard/pagamento', 'post', Object.assign(data, { functionPage: 'alterarCartao'}), function(res) {
        callback(res);
    }, function(erro) {
        errCallback(erro);
    });
}

function getVerificaMulta(id_mens, callback, errCallback) {
    request(config.api+'dashboard/pagamento', 'get', {
        functionPage: 'verificaMulta', id_mens
    }, function(res) {
        callback(res);
    }, function(erro) {
        errCallback(erro);
    });
}

function atualizaPlanos(plano, id_mens) {
    plano.id_mens = id_mens;
    const planos = getPlanos().map(function(item) {
        if (item.id == plano.id) {
            item = plano;
        }
        return item;
    });
    setPlanos(planos);
    setPlano(plano);
}