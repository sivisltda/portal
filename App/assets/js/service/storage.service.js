export function setItem(chave, valor) {
    window.localStorage.setItem(chave, valor);
}

export function getItem(chave) {
    return window.localStorage.getItem(chave);
}

export function removeItem(chave) {
    window.sessionStorage.removeItem(chave);
}