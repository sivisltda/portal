import request from './../core/request.js';
import { getConfig as configFunc } from './../config/config.js';
import storeDashboard from './../store/dashboard.js';

const { setMensalidades, setPlanos } = storeDashboard.setters;
const { getPlano } = storeDashboard.getters;

const config = configFunc();

export function getListaMensalidades(plano_id, callback, errCallback) {
    request(config.api + 'dashboard/pagamento?functionPage=ListaMensalidadesPorPlano&plano_id=' + plano_id, 'get', {},
        function (mensalidades) {
            setMensalidades(mensalidades);
            callback(mensalidades);
        }, function (erro) {
            errCallback(erro);
        });
}

export function getListaPlanos(tipo_veiculo, callback, errCallback) {
    request(config.api + 'dashboard/pagamento?functionPage=listaPlanos', 'get', {
        tipo_veiculo
    }, function (planos) {
        setPlanos(planos);
        callback(planos);
    }, function (erro) {
        errCallback(erro);
    });
}

export function inicioFormaPagamento(callback, errCallback) {
    request(config.api+'dashboard/pagamento', 'get', { functionPage: 'inicioFormaPagamento'}, function(res) {
       callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}

export function alterarFormaPagto(data, callback, errCallback) {
    request(config.api+'dashboard/pagamento', 'post', Object.assign({ id_plano: getPlano().id, functionPage: 'alterarFormaPagamento' }, data), function (res) {
        callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}

export function getListaMensalidadesAdmin(plano, callback, errCallback) {
    request(config.api+'dashboard/plano', 'get', {id_plano: plano, functionPage: 'listaMensalidades'}, function (res) {
        callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}

export function getTipoPagamentosCartao(callback, errCallback) {
    request(config.api+'dashboard/pagamento', 'get', { functionPage: 'getTipoPagamentos'}, function (res) {
       callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}


export function getFiltroMensalidades(callback, errCallback) {
    request(config.api+'dashboard/plano', 'get', { functionPage: 'filtrosMensalidades' }, function (res) {
        callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}

