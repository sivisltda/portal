import {getConfig as configFunc} from './../config/config.js';
import request from './../core/request.js';

const config = configFunc();

export function getUrlClientes() {
  return config.api+'dashboard/gestor?functionPage=getClientes';
}

export function getCliente(id, callback, errCallBack) {
  request(`${config.api+'dashboard/gestor'}`, 'get', {
      functionPage: 'getCliente', id
  }, function(response){
      callback(response);
  }, function(error) {
      errCallBack(error);
  });
}

