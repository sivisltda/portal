import request from './../core/request.js';
import { getConfig as configFunc } from './../config/config.js';
import store from './../store/app.js';
import * as storageService from './storage.service.js'; 

const { setPlanos, setPlano, setIdParcela } = store.setters;
const { getPlano, getPlanos, getProduto } = store.getters;
const config = configFunc();

export function alteraOuCriaPlanoLista(plano) {
    const planos = getPlanos();
    const indexPlano = planos.findIndex(function(item) { return item.id === plano.id; });
    if (indexPlano > -1) {
        planos[indexPlano] = plano;
    } else {
        planos.push(plano);
    }
    setPlano(plano);
    setIdParcela(plano.id_parcela);
    setPlanos(planos);
}

export function removePlanoPorId(id) {
    const planos = getPlanos().filter(function(plano) {
        return plano.id != id;
    });
    setPlanos(planos);
    const plano = getPlano();
    if (plano && plano.id == id) {
        setPlano(null);
    }
    storageService.setItem(`loja-${config.contrato}`, store.actions.serialize());
}

export function salvarPlano(data, callback, errCallback) {
    request(config.api + 'portal/plano', 'post', Object.assign(data, { functionPage: 'salvarPlano' }), function (res) {
        callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}

export function verificarCupom(data, callback, errCallback) {
    request(config.api + 'portal/plano', 'post', Object.assign(data, { functionPage: 'verificarCupom' }), function (res) {
        callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}

export function cancelarCupom(data, callback, errCallback) {
    request(config.api + 'portal/plano', 'post', Object.assign(data, { functionPage: 'cancelarCupom' }), function (res) {
        callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}

export function salvarAcessorios(data, callback, errCallback) {
    request(config.api + 'portal/plano', 'post', Object.assign(data, { functionPage: 'salvarAcessorios' }), function (res) {
        callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}

export function cancelarPlanoUsuario(id_plano, callback, errCallback) {
    request(config.api + 'portal/plano', 'post', { functionPage: 'CancelarPlano', id_plano }, function(res) {
        callback(res);
    }, function(error) {
        if (error.status === 404) {
            errCallback({
                type: 'warning', title: 'Atenção', text: 'Plano não encontrado'
            }, 1);
        } else {
            errCallback({
                type: 'error', title: 'Error', text: 'Ocorreu um erro ao cancelar o plano'
            }, 2);
        }        
    });
}
