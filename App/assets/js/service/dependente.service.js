import request from './../core/request.js';
import { getConfig as configFunc } from './../config/config.js';
import store from './../store/app.js';

const config = configFunc();
const { setDependentes, setParentesco } = store.setters;

export function getListaDependentes(callback, errCallback, dashboard = 0) {
    const url = dashboard ? 'dashboard/dependentes' : 'portal/dependentes';
    request(config.api+url, 'get', { functionPage: 'getListaDependentes', dashboard }, function(res) {
        setDependentes(res.lista);
        if (res.parentesco) {
            setParentesco(res.parentesco);
        }
        callback(res);
    },function(erro) {
        errCallback(erro)
    });
}


export function cadastrarDependentes(values, callback, errCallback, dashboard = 0) {
    const data = Object.assign(values, {
        functionPage: 'cadastrarDependentes',
        dashboard: dashboard
    });
    request(config.api+'portal/dependentes', 'post', data, function(res) {
        callback(res);
    }, function (erro) {
       errCallback(erro);
    });
}


export function removeDependente(data, callback, errCallback, dashboard = 0) {
    Object.assign(data, {
        functionPage: 'removeDependente',
        dashboard
    });
    request(config.api+'portal/dependentes', 'post', data, function() {
        callback();
    }, function(erro) {
        errCallback(erro);
    });
}


export function consultarUsuarioPorCPF(data, callback, errCallback) {
    request(config.api+"portal/dependentes", 'post', Object.assign({}, data,{
        functionPage: 'verificaDadosDependente',
    }), function(res) {
        callback(res);
    }, function(err) {
        errCallback(err);
    });
}


export function listarParentescos(callback, errCallback) {
    request(config.api+"portal/dependentes", 'get', { functionPage: 'ListaParentescos' }, function(res) {
        setParentesco(res);
        callback();
    }, function(err) {
        errCallback(err);
    });
}


