import request from './../core/request.js';
import { getConfig as configFunc } from './../config/config.js';

import store from './../store/app.js';

const { setUsuario, setVeiculo, setVeiculos, setPlanos } = store.setters;
const { getUsuario, getVeiculo } = store.getters;

const config = configFunc();

export function getBuscarUsuario(email, callback, errCallbak) {
    request(config.api+'portal/usuario', 'get', { email, functionPage: 'buscarUsuario' }, function (res) {
       callback(res);
    }, function(erro) {
       errCallbak(erro);
    });
}

export function getBuscarDadosUsuario(campo, callback, errCallbak) {
    request(config.api+'portal/usuario', 'get', Object.assign(campo, { functionPage: 'buscarDadosUsuario' }), function (res) {
        setUsuario(transformUsuario(res));
        if (config.modelo === 'seguro' && res.hasOwnProperty('veiculos')) {
            setVeiculos(res.veiculos);            
        } 
        if(res.hasOwnProperty('planos')) {
            setPlanos(res.planos);
        }
        callback(res);
    }, function (erro) {
        errCallbak(erro);
    });
}

export function salvarUsuario(usuario, callback, errCallback) {
    request(config.api+'portal/usuario', 'post', Object.assign({}, usuario,  {functionPage: 'salvarUsuario'}), function(res) {
        setUsuario(Object.assign({}, getUsuario(), usuario, {id: res.id_fornedores_despesas}));
        if (config.modelo === 'seguro' && res.hasOwnProperty('id_veiculo')) {
            setVeiculo({...getVeiculo(), id: res.id_veiculo});
        }
        callback();
    }, function (erro) {
       errCallback(erro);
    });
}

export function atualizarUsuario(usuario, callback, errCallback) {
    request(config.api+'portal/usuario', 'post', Object.assign({}, usuario,  {functionPage: 'atualizarUsuario'}), function(res) {
        callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}

export function loginUsuario(email, password, callback, errCallback) {
    request(config.api+'portal/usuario', 'post', { functionPage: 'restrictedArea', email, password }, function (res) {
        callback(res);
    }, function(erro) {
        errCallback(erro);
    });
}

export function salvarCartao(data, callback, errCallback) {
    request(config.api+'portal/plano', 'post', Object.assign(data, {functionPage: 'salvarCartao'}), function(res) {
        callback(res);
    }, function (erro) {
       errCallback(erro);
    });
}

export function consultaUsuario(search, callback, errCallback) {
    request(config.api + "portal/usuario", 'get' , 
        Object.assign(search, { functionPage: "ConsultarUsuario" }), function(res) {
        callback(res);
    }, function(erro) {
        errCallback(erro);
    });
}

export function consultaUsuarioValidador(dados, callback, errCallback) {
    request(config.api + "portal/usuario", 'post', 
    Object.assign(dados, { functionPage: "ConsultaUsuarioValidador" }), function(res) {
        callback(res);
    }, function(erro) {
        errCallback(erro);
    });
}

export function getDadosUsuario(callback, errCallback) {
    request(config.api + "dashboard/usuario", 'get', 
        { functionPage: "getUsuario" }, function(res) {
        callback(res);
    }, function(erro) {
        errCallback(erro);
    });
}

export function patchUsuario(obj) {
    setUsuario(transformUsuario(Object.assign({}, getUsuario(), obj)));
}

export function transformUsuario(item) {
    const modelo = {
        id: item.id,
        razao_social: item.razao_social,
        email: item.email,
        celular: item.celular,
        inscricao_estadual: item.inscricao_estadual ? item.inscricao_estadual : '',
        data_nascimento: item.data_nascimento ? item.data_nascimento : '',
        estado_civil: item.estado_civil,
        sexo: item.sexo,
        endereco: item.endereco,
        cep: item.cep,
        estado: item.estado,
        cidade: item.cidade,
        bairro: item.bairro,
        juridico_tipo: item.juridico_tipo,
        numero: item.numero ? item.numero : '',
        complemento: item.complemento ? item.complemento : '',
        indicador: item.indicador,
    };
    if(item.hasOwnProperty('cnpj')) modelo.cnpj = item.cnpj;
    if(item.hasOwnProperty('tipo')) modelo.tipo = item.tipo;
    if(item.hasOwnProperty('regime_tributario')) modelo.regime_tributario = item.regime_tributario;
    if(item.hasOwnProperty('nfe_iss')) modelo.nfe_iss = item.nfe_iss;
    if(item.hasOwnProperty('contato')) modelo.contato = item.contato;
    if(item.hasOwnProperty('telefone')) modelo.telefone = item.telefone;
    if(item.hasOwnProperty('procedencia')) modelo.procedencia = item.procedencia;
    if(item.hasOwnProperty('email_marketing')) modelo.email_marketing = item.email_marketing;
    if(item.hasOwnProperty('celular_marketing')) modelo.celular_marketing = item.celular_marketing;
    return modelo;
}