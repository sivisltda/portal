import request from './../core/request.js';
import { getConfig as configFunc } from './../config/config.js';

const config = configFunc();

export function getListaFaq(callback, errCallback) {
    request(config.api+'dashboard/faq', 'get', { functionPage: 'listaFaq'}, function(data) {
        callback(data);
    }, function(erro) {
        errCallback(erro);
    });
}