import { getConfig as configFunc } from './../config/config.js';
import request from "./../core/request.js";

import store from './../store/app.js';

const config = configFunc();
const { setCidades, setEndereco, setEstados } = store.setters;
const { getCidades, getEstados } = store.getters;

/**
 * Função para evento de buscar Estado e carregar as cidades do mesmo
 * @param elementEstado
 * @param elementCidade
 * @param callback Function
 */
export function buscarEstado(elementEstado, elementCidade, callback) {
    const estado = elementEstado.val();
    if(estado) {
        carregaCidade(elementCidade, estado, null, callback);
    }
}

/**
 * Função para popular endereco via Cep
 * @param cep
 * @param callback
 * @param errCallback
 */
export function popularEnderecoViaCep(cep, callback, errCallback) {
    buscarPorCep(cep.replace(/\D/g, ''), function(data) {
        if(data.erro) {
            errCallback({status: 1});
        } else {
            buscarEstados(function() {
                const estado = getEstados().find((e) => e.estado_sigla === data.uf.toUpperCase());
                if(estado) {
                    buscarCidadesPorEstado(estado.estado_codigo, function() {
                        const cidade = getCidades().find((c) => c.cidade_nome.toLowerCase() === data.localidade.toLowerCase());
                        if(cidade) {
                            setEndereco({
                                cep,
                                estado: estado.estado_codigo,
                                cidade: cidade.cidade_codigo,
                                bairro: data.bairro,
                                endereco: data.logradouro,
                                complemento: data.complemento
                            });
                            callback();
                        } else {
                            errCallback({status: 2});
                        }
                    }, function (erro) {
                        console.log(erro);
                        errCallback({status: 2});
                    });
                } else {
                    errCallback({status: 2});
                }
            }, function(erro) {
                console.log(erro);
                swal('Erro', 'Não foi possivel carregar os estados', 'error');
                
            })
        }
    },function(erro) {
        console.log(erro);
        errCallback({status: 1});
    });
}


/**
 * Função que popula os dados do endereço
 * @param elementCep
 * @param elementEstado
 * @param elementCidade
 * @param elementEndereco
 * @param elementBairro
 * @param elementNumero
 * @param callback Function
 */
export function popularEndereco(elementCep, elementEstado, elementCidade, elementEndereco, elementBairro, elementNumero, callback) {
    const cep = elementCep.val();
    if (cep.length === 9) {
        let cidade = "";
        buscarPorCep(cep, function(data) {
            if(data.erro) {
                elementCep.focus().val("");
                $.toast({
                    heading: "Opa",
                    text: "O CEP informado não foi encontrado!",
                    stack: 6,
                    bgColor: "#cc0000",
                    textColor: "#FFF",
                    position: "mid-center",
                    icon: "warning"
                });
                callback();
            } else {
                const str = data;               
                const estado = elementEstado.find("option:contains('" + str.uf.toUpperCase() + "')").val();
                cidade = decodeURIComponent(str.localidade);
                if(str.logradouro) {
                    elementEndereco.attr("readonly", true).val(decodeURIComponent(str.logradouro));
                }
                if(str.bairro) {
                    elementBairro.attr("readonly", true).val(decodeURIComponent(str.bairro));
                }
                if(str.uf) {
                    elementEstado.attr("readonly", true).val(estado);
                }
                elementNumero.focus();
                carregaCidade(elementCidade, estado, cidade, callback);
            }
        }, function() {
            callback();
            $.toast({
                heading: "Opa",
                text: "O CEP informado não foi encontrado!",
                stack: 6,
                bgColor: "#cc0000",
                textColor: "#FFF",
                position: "mid-center",
                icon: "warning"
            });
        });
    }
}


/**
 * Função que carrega a cidade 
 * @param campo string
 * @param estado string
 * @param cidade string
 * @param callback Function
 */
export function carregaCidade(campo, estado, cidade, callback) {
    campo.hide();
    let idCidade = $.isNumeric(cidade) ? cidade : "";
    buscarCidadesPorEstado(estado, function() {
        let options = '<option value="">Selecione a Cidade</option>';
        if (j.length > 0) {
            for (let i = 0; i < j.length; i++) {
                if (cidade === j[i].cidade_nome) {
                    idCidade = j[i].cidade_codigo;
                }
                options = options +'<option value="'+j[i].cidade_codigo+'">'+j[i].cidade_nome+"</option>";
            }
            campo.html(options);
            campo.show().val(idCidade);
            callback();
        }
    }, function(err) {
        swal('Erro', err.responseText, 'error');
        callback();
    });
}

/**
 * Função que carrega o estado
 * @param callback function
 * @param errCallback function
 */
export function buscarEstados(callback, errCallback) {
    if (getEstados().length) {
        callback();
    } else {
        request(config.api + "portal/endereco?functionPage=CarregaEstado", 'get', {}, function(estados) {
            setEstados(estados);
            callback();
        }, function(err) {
            errCallback(err);
        });
    }
}

/**
 * Função para pegar o endereço via cep
 * @param cep string
 * @param callback function
 * @param errCallback function
 */
export function buscarPorCep(cep, callback, errCallback) {
    request("https://viacep.com.br/ws/" + cep + "/json/", 'get', {}, function(data) {
        callback(data);
    }, function(erro) {
        errCallback(erro);
    });
}

/**
 * Funcao de buscar as cidades do estado
 * @param estado
 * @param callback
 * @param errCallback
 */
export function buscarCidadesPorEstado(estado, callback, errCallback) {
    const cidades = getCidades();
    if (cidades.length && cidades[0].cidade_codigoEstado == estado) {
        callback();
    } else {
        request(config.api + "portal/endereco?functionPage=carregaCidade&estado=" + estado, 'get', {}, function(data) {
            setCidades(data);
            callback();
        }, function(erro) {
            errCallback(erro);
        });
    }
}

export function transformEndereco(data) {
    return {
        cep: data.cep,
        endereco: data.endereco,
        bairro: data.bairro,
        cidade: data.cidade,
        estado: data.estado
    };
}