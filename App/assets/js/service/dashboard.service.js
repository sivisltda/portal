import { getConfig as configFunc } from '../config/config.js';
import request from '../core/request.js';

const config = configFunc();

export function getInfoDashboard(callback, errCallback) {
    request(config.api+'dashboard/dashboard', 'get', { functionPage: 'GetDashboard'}, function(data) {
        callback(data);
    }, function (erro) {
        errCallback(erro);
    });
}

