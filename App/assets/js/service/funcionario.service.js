import request from './../core/request.js';
import { getConfig as configFunc } from './../config/config.js';
const config = configFunc();

export function criarSessaoConsultor(data, callback, errCallback) {
    request(config.api+'dashboard/funcionario', 'post', 
    Object.assign({}, data, { functionPage: 'criarSessaoConsultor' }), function(res) {
        callback();
    }, function(erro) {
        errCallback(erro);
    });
}

export function convertLeadProspect(data, callback, errCallback) {
    request(config.api+'dashboard/funcionario', 'post',  Object.assign({}, data, { functionPage: 'convertLeadProspect' }),
    function(res) {
        callback(res);
    }, function(erro) {
        errCallback(erro);
    });
}

export function inativarUsuario(data, callback, errCallback) {
    request(config.api+'dashboard/funcionario', 'post',  Object.assign({}, data, { functionPage: 'inativarUsuario' }),
    function(res) {
        callback(res);
    }, function(erro) {
        errCallback(erro);
    });
}

export function verificaEmail(email, callback, errCallback) {
    request(config.api+'dashboard/funcionario', 'get', {functionPage: 'verificaEmail', email},
    function(res) {
        callback(res);
    }, function(erro) {
        errCallback(erro);
    });
}