import { getConfig as configFunc } from './../config/config.js';
import request from './../core/request.js';

const config = configFunc();

export function getInfoPortal(callback, errCallback) {
    request(config.api+'portal/portal', 'get', { functionPage: 'getInfo' }, function(data) {
        callback(data);
    }, function(erro) {
        errCallback(erro);
    });
}

export function verificaSessaoUsuario(id_usuario, callback, errCallback) {
    request(config.api+'portal/portal', 'post', { functionPage: 'verificaSessao', id_usuario }, function() {
        callback();
    }, function(erro) {
        errCallback(erro);
    });
}

export function logout(callback, errCallback) {
    request(config.api+'portal/portal', 'get', { functionPage: 'logout' }, function(data) {
        callback(data);
    }, function(erro) {
        errCallback(erro);
    });
}