import { getConfig as configFunc } from '../config/config.js';
import request from '../core/request.js';

const config = configFunc();

function getMenuInfo(callback, errCallback) {
    request(config.api+'dashboard/dashboard', 'get', {functionPage: 'getMenu'},
        function(data) {
            callback(data);
        }, function (erro) {
            errCallback(erro);
        }
    );
}


export function preencheLista(listaMenu) {
    const path = window.location.pathname.split('/').filter(Boolean);
    getMenuInfo(function(data) {
        const lista = data.map(function(item) {
            return `<li class="side-nav-item">
                <a href="${item.link}" class="side-nav-link${path.includes(item.link) ? ' active' : ''}">
                    <i class="${item.icon}"></i>
                    <span> ${item.label}</span>
                </a>
            </li>`;
        }).join('');
        listaMenu.html(lista);
    }, function(erro) {
        console.log(erro);
    });
}

export function logout(callback, errCallback) {
    request(config.api+'dashboard/dashboard', 'post', { functionPage: 'logout'}, function () {
        callback();
    }, function(erro) {
        errCallback(erro);
    });
}
