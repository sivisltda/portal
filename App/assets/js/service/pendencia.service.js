import {getConfig as configFunc} from './../config/config.js';
import request from './../core/request.js';

const config = configFunc();

export function getListaPendencias(data, callback, errCallBack) {
  request(`${config.api}dashboard/pendencia`, 'get', Object.assign({
    functionPage: 'getListaPendencias',
  }, data),function(res) {
    callback(res);
  }, function(erro) {
    errCallBack(erro);
  });
}

export function getListaAcessorios(data, callback, errCallBack) {
  request(`${config.api}dashboard/pendencia`, 'get', Object.assign({
    functionPage: 'getListaAcessorios',
  }, data),function(res) {
    callback(res);
  }, function(erro) {
    errCallBack(erro);
  });
}

export function salvarConvenios(data, callback, errCallBack) {
  request(`${config.api}dashboard/pendencia`, 'post', Object.assign({
    functionPage: 'salvarConvenios'
  }, data), function (res) {
    callback(res);
  }, function(erro) {
    errCallBack(erro);
  });
}

export function saveUpload(data, callback, errCallBack) {
  request(`${config.api}dashboard/pendencia`, 'post', data, function(res) {
    callback(res);
  }, function(erro) {
    errCallBack(erro);
  }, 'json', {
    contentType: false,
    processData: false,
  });
}//

export function finalizarAdesao(data, callback, errCallBack) {
  request(`${config.api}dashboard/pendencia`, 'post', Object.assign({
    functionPage: 'finalizarAdesao'
  }, data), function(res) {
    callback(res);
  }, function (erro) {
    errCallBack(erro);
  });
}

export function cancelarAdesao(data, callback, errCallBack) {
  request(`${config.api}dashboard/pendencia`, 'post', Object.assign({
    functionPage: 'cancelarAdesao'
  }, data), function(res) {
    callback(res);
  }, function (erro) {
    errCallBack(erro);
  });
}

export function removerConvenio(data, callback, errCallBack) {
  request(`${config.api}dashboard/pendencia`, 'post', Object.assign({
    functionPage: 'removerConvenio'
  }, data), function(res) {
    callback(res);
  }, function(erro) {
    errCallBack(erro);
  });
}

export function enviarContratoWhatsapp(data, callback, errCallBack) {
  request(`${config.api}dashboard/pendencia`, 'post', Object.assign({
    functionPage: 'enviarContratoWhatsapp'
  }, data), function(res) {
    callback(res);
  }, function(erro) {
    errCallBack(erro);
  });
}

export function enviarVistoriaWhatsapp(data, callback, errCallBack) {
  request(`${config.api}dashboard/pendencia`, 'post', Object.assign({
    functionPage: 'enviarVistoriaWhatsapp'
  }, data), function(res) {
    callback(res);
  }, function(erro) {
    errCallBack(erro);
  });
}

export function removerContrato(data, callback, errCallBack) {
  request(`${config.api}dashboard/pendencia`, 'post', Object.assign({
    functionPage: 'removerContrato'
  }, data), function(res) {
    callback(res);
  }, function(erro) {
    errCallBack(erro);
  });
}