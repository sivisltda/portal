import request from './../core/request.js';
import { getConfig as configFunc } from './../config/config.js';
import state from './../store/app.js';

const { setMarcas, setModelos, setAnos, setVeiculo, setVeiculos, setCupom } = state.setters;
const { getVeiculo, getVeiculos, getTipoVeiculo, getSelTipoVeiculo, getSelConsultaFipe } = state.getters;
const config = configFunc();

export function patchVeiculo(obj) {
    const veiculo       = Object.assign({}, getVeiculo(), obj);
    const veiculos      = getVeiculos();    
    const indexVeiculo  = veiculos.findIndex(function(item) {
        return item.id === veiculo.id || item.id === null;
    });
    if (indexVeiculo > -1) {
        veiculos[indexVeiculo] = veiculo;
    } else {
        veiculos.push(veiculo);
    }
    setVeiculo(veiculo);  
    setCupom(veiculo.cupom);    
    setVeiculos(veiculos);
}

export function setListaVeiculos(listaVeiculos) {
    setVeiculos(listaVeiculos);
}

export function setItemVeiculo(item) {
    setVeiculo(item);       
}

export function getMarca(callback, errCallback, count = 0) {
    const errorFunction = function(err) {
        if(count < 2) {
            count++;
            setTimeout(function() {
                getMarca(callback, errCallback, count);
            }, 1000);
        }  else {
            errCallback({erro: 2, msg: err});
        }
    };
    const veiculo = getVeiculo();
    const tipo_veiculo = getTipoVeiculo().filter((item) => item.id === getSelTipoVeiculo())[0];
    request(config.urlErp + 'Modulos/Seguro/ajax/fipejson.php', 'get', { functionPage: 'getMarca',
        tipo: veiculo && veiculo.tipo_veiculo ? veiculo.tipo_veiculo : tipo_veiculo.id, 
        tipoFipe: veiculo && veiculo.consulta_fipe ? veiculo.consulta_fipe : getSelConsultaFipe(), 
        contrato: config.contrato }, function(res) {
        if(Array.isArray(res)) {
            setMarcas(res);
            callback(res);
        }else {
         errorFunction('Erro');
        }
    }, function(err) {
       errorFunction(err);
    }, null, {
        timeout: 15000
    });
}

export function getModeloOuAno(marca, callback, errCallback, count = 0) {
    const erroFunction = function(err) {
        if(count < 2) {
            count++;
            setTimeout(function () {
                getModeloOuAno(marca, callback, errCallback, count);
            }, 1000);
        }else {
            errCallback(Object.assign({erro: 2, msg: err }));
        }
    };
    const veiculo = getVeiculo();
    const tipo_veiculo = getTipoVeiculo().filter((item) => item.id === getSelTipoVeiculo())[0];
    const tipoFipe = getSelConsultaFipe();
    request(config.urlErp+'Modulos/Seguro/ajax/fipejson.php', 'get', { functionPage: 'getModelo', codMarca: marca,
        tipo: veiculo && veiculo.tipo_veiculo ? veiculo.tipo_veiculo : tipo_veiculo.id,
        tipoFipe: tipoFipe, contrato: config.contrato}, function (res) {
        if(res.hasOwnProperty('Modelos') && res.hasOwnProperty('Anos')) {
            setModelos(res.Modelos);
            setAnos(res.Anos.map(function(ano) {
                ano.Label = ano.Value.includes('32000') ? ano.Label.replace('32000', 'Zero km') : ano.Label;
                return ano;
            }).filter(function(item) {
                return (tipoFipe !== "2" || (tipoFipe === "2" && item.Value.includes('32000')));
            }));
        }else {
            erroFunction('Erro');
        }
       callback();
    }, function (err) {
       erroFunction(err);
    }, null, {
        timeout: 15000
    });
}

export function getModeloPorAno(marca, ano, callback, errCallback, count = 0) {
    const errorFunction = function(err) {
        if(count < 2) {
            count++;
            setTimeout(function() {
                getModeloPorAno(marca, ano, callback, errCallback, count);
            }, 1000);
        } else {
            errCallback({erro: 2, msg: err});
        }
    };
    const veiculo = getVeiculo();
    const tipo_veiculo = getTipoVeiculo().filter((item) => item.id === getSelTipoVeiculo())[0];
    request(config.urlErp+'Modulos/Seguro/ajax/fipejson.php', 'get', { functionPage: 'getModeloPorAno', codMarca: marca,
        codAno: ano, tipo: veiculo && veiculo.tipo_veiculo ? veiculo.tipo_veiculo : tipo_veiculo.id, 
        tipoFipe: getSelConsultaFipe(), contrato: config.contrato }, function (res) {
        if(Array.isArray(res)) {
           setModelos(res);
           callback(res);
        }else {
          errorFunction('Erro');
        }
    }, function (err) {
        errorFunction(err);
    }, null, {
        timeout: 15000
    });
}

export function getInfoVeiculo(marca, ano, modelo, valor, callback, errCallback, count = 0) {
    const errorFunction = function(err) {
        if(count < 2) {
            count++;
            setTimeout(function() {
                getInfoVeiculo(marca, ano, modelo, valor, callback, errCallback, count);
            });
        }else {
            errCallback({erro: 2, msg: err});
        }
    };
    const veiculo = getVeiculo();
    const tipo_veiculo = getTipoVeiculo().filter((item) => item.id === getSelTipoVeiculo())[0];
    request(config.urlErp+'Modulos/Seguro/ajax/fipejson.php', 'get', {functionPage: 'getVeiculo', codMarca: marca,
        codModelo: modelo, tipo: veiculo && veiculo.tipo_veiculo ? veiculo.tipo_veiculo : tipo_veiculo.id, 
        tipoFipe: getSelConsultaFipe(), codAno: ano, contrato: config.contrato, valor: valor}, function (res) {
        if(res.hasOwnProperty('CodigoFipe')) {
            patchVeiculo(transformVeiculo(res));
            callback(res);
        }else {                        
           errorFunction('Erro');
        }
    }, function (err) {
        errorFunction(err);
    }, null, {
        timeout: 15000
    });
}

export function getBuscaPlaca(placa, callback, errCallback) {
    const veiculo = getVeiculo();
    const id_veiculo = veiculo ? veiculo.id : null;
    request(config.api+'portal/veiculo', 'get', { functionPage: 'BuscaPlaca', placa: placa, id_veiculo }, function (res) {
        setVeiculo(res);    
        callback(res);
    }, function (err) {
        errCallback(err);
    });
}

export function transformVeiculo(item) {
    return {
        valor: item.Valor,
        marca: item.Marca,
        modelo: item.Modelo,
        ano_modelo: item.AnoModelo,
        codigo_fipe: item.CodigoFipe,
        combustivel: item.Combustivel,
        mes_referencia: item.MesReferencia,
        sigla_combustivel: item.SiglaCombustivel
    };
}

export function alertaErroFipe() {
    swal({title: 'Atenção',
        html: 'Estamos com um problema de conexão com o Servidor da Fipe! <br/> Tente novamente mais tarde ou entre em contato conosco pelo número abaixo!</br>' 
        +config.telefone, type: 'warning'});
}