import request from './../core/request.js';
import { getConfig as configFunc } from './../config/config.js';
import state from './../store/app.js';

const { setProduto, setCores, setCambios, setProdutos, setServicos, setServicosPlano, setCoberturas } = state.setters;
const { getCores, getCambios, getProdutos, getServicos, getServicosPlano, getEndereco } = state.getters;

const config = configFunc();

export function setProdutoPorId(id) {
    const produto = getProdutos().find(function (p) {
        return parseInt(p.id) === parseInt(id);
    });   
    if (produto.hasOwnProperty('parcelas')) {
        setProduto(ordenaParcelas(produto));
    } else {
        setProduto(produto);
    }

}

export function getItemsPadroes() {
    return getServicosPlano().filter(function(servico) {
        return servico.padrao;
    });
}


export function getListaProdutos(tipoPessoa, callback, errCallback) {
    request(config.api+'portal/produto', 'get', { functionPage: 'listaProduto', tipoPessoa }, function(res) {
        if (res.produtos) {
            setProdutos(res.produtos);
        } else {
            setProduto(ordenaParcelas(res.produto));
        } 
        callback();
    }, function(erro) {
        errCallback(erro);
    });
}

export function getItemProduto( { id, tipoPessoa }, callback, errCallback) {
    request(config.api+'portal/produto', 'get', { functionPage: 'itemProduto', id, tipoPessoa }, function(res) {
        setProduto(ordenaParcelas(res));
        callback();
    }, function(erro) {
        errCallback(erro);
    });
}

export function getListaServicos(dados, callback, errCallback) {
    const { cidade } = getEndereco();
    request(config.api + 'portal/produto', 'post', { functionPage: 'buscarServicos', cidade, ...dados}, function (res) {
        setProdutos(res.produtos);
        setServicos(res.servicos);
        setServicosPlano(res.servicos);
        callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}

export function getListaCoberturas({tipo_veiculo, produto_id, valor}, callback, errCallback) {
    request(config.api + 'portal/produto', 'post', { functionPage: 'buscarCoberturas', tipo_veiculo, produto_id, valor }, function (res) {
        setCoberturas(res.coberturas);
        setServicosPlano(res.servicos);
        callback(res.coberturas);
    }, function (erro) {
        errCallback(erro);
    });
}

export function getListaCores(callback, errCallback) {
    const cores = getCores();
    if (Object.keys(cores).length) {
       setCores(cores); 
       callback();
    } else {
        request(config.api + 'portal/produto', 'get', { functionPage: 'buscarCores' }, function (res) {
            setCores(res);
            callback();
        }, function (erro) {
            errCallback(erro);
        });
    }
}

export function getListaCambio(callback, errCallback) {
    const cambios = getCambios();
    if (Object.keys(cambios).length) {
       setCambios(cambios); 
       callback();
    } else {
        request(config.api + 'portal/produto', 'get', { functionPage: 'buscarCambio' }, function (res) {
            setCambios(res);
            callback();
        }, function (erro) {
            errCallback(erro);
        });
    }
}

function ordenaParcelas(produto) {
    produto.parcelas = produto.parcelas.sort(function(a, b) {
        return Number.parseInt(a.parcela) - Number.parseInt(b.parcela);
    });
    return produto;
}


export function setServicosProduto(produto) {
    const servicos = getServicos().filter(function(servico) {
        return produto.servicos.includes(servico.id);
    });
    setServicosPlano(servicos);
}