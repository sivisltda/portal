import request from './../core/request.js';
import { getConfig as configFunc } from './../config/config.js';
import { textToNumber, numberFormat } from './../core/function.js';

import store from './../store/app.js';

const { getProduto, getVeiculo, getAcessorios, getLead } = store.getters;
const { setLead } = store.setters;

const config = configFunc();

export function salvarLead(data, callback, errCallback) {
    const veiculo = getVeiculo();
    const produto = getProduto();
    setLead(data);
    const acessorios = getAcessorios().filter(function(item) {
        return !item.padrao;
    });
    if (data) {
        if (veiculo) {
            data.veiculo = veiculo;
        }
        if (produto) {
            const valorAcessorio = acessorios.reduce(function(acc, item) {
                acc+= textToNumber(item.preco_venda);
                return acc;
            }, 0);
            data.plano = {
                nome: produto.nome,
                taxa_adesao: produto.taxa_adesao,
                valor_mensal: numberFormat(textToNumber(produto.valor_mensal ? produto.valor_mensal : produto.valor)+valorAcessorio, 1)
            };

            if (acessorios.length) {
                data.plano.acessorios = acessorios;
            }
        }
        request(config.api+'portal/crm', 'post', Object.assign({}, data, {functionPage: 'salvarLead'}), function(res) {
           setLead({id: res.id, ...getLead()}); 
           callback();
        }, function(erro) {
            errCallback(erro);
        });
    } else {
        callback();
    }
}

export function getModeloWhats(callbak, errCallback) {
    request(config.api+'portal/crm', 'get', {functionPage: 'getModeloWhats'}, function(res) {
        callbak(res);
    }, function(erro) {
        errCallback(erro);
    });
}

export function enviaWhatsApp(data, callback, errCallback) {
    request(config.api+'portal/crm', 'post', Object.assign({}, data, {functionPage: 'enviaWhatsApp'}), function(res) {
        callback();
     }, function(erro) {
         errCallback(erro);
     });
} 
