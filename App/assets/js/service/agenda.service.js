import request from './../core/request.js';
import { getConfig as configFunc } from './../config/config.js';
import store from './../store/dashboard.js';

const config = configFunc();
const { setAgendas, setAgendamentos } = store.setters;


export function cancelaAgendamento({ id_agendamento, numero_contrato }, callback, errCalback) {
    request(config.api + 'dashboard/agenda', 'get', {functionPage: 'CancelaAgendamento', id_agendamento, numero_contrato },
    function (res) {
        callback(res);
    }, function (erro) {
        errCalback(erro);
    });
}

export function getListaAgenda(numero_contrato, callback, errCalback) {
    request(config.api+'dashboard/agenda', 'get', { functionPage: 'ListaAgenda', numero_contrato },
    function(res) {
        setAgendas(res);
        callback(res);
    }, function(erro){
        errCalback(erro);
    });
}

export function getListaDias({ id_agenda, data_inicio, numero_contrato }, callback, errCalback) {
    request(config.api+'dashboard/agenda', 'get', { functionPage: 'ListaDias', id_agenda, data_inicio, numero_contrato },
    function(res) {
        callback(res);
    }, function(erro){
        errCalback(erro);
    });
}

export function getListaHorarios({ id_agenda, data_inicio, numero_contrato }, callback, errCalback) {
   request(config.api+'dashboard/agenda', 'get', { functionPage: 'ListaHorariosLivres', id_agenda, data_inicio, numero_contrato },
    function(res) {
        callback(res);
    }, function(erro) {
        errCalback(erro);
    });
}


export function salvarAgendamento(data , callback, errCallback) {
    request(config.api+'dashboard/agenda', 'post', Object.assign(data, { functionPage: 'SalvarAgendamento' }),
    function (res) {
    callback(res);
    }, function(erro) {
        errCallback(erro);
    });
}

export function getListaAgendamentos({ pagina, id_agenda, data_inicio, status, numero_contrato }, callback, errCallback) {
    request(config.api+'dashboard/agenda', 'get',
        { functionPage: 'ListarAgendamentos', page: pagina, id_agenda, data_inicio, status, numero_contrato },
    function(res) {
        setAgendamentos(res);
        callback(res);
    }, function(erro) {
        errCallback(erro);
    });
}

export function verificaPlanoAtivo(callback, errCallback) {
   request(config.api+'dashboard/agenda', 'get', { functionPage: 'VerificaUsuarioAtivo' },
   function(res) {
        callback(res);
    }, function(erro) {
        errCallback(erro);
    });
}
