import request from './../core/request.js';
import { getConfig as configFunc } from './../config/config.js';

const config = configFunc();

export function getCarterinhas(callback, errCallback) {
  request(config.api+'dashboard/dashboard', 'get', {functionPage: 'getCarteiras'},
  function(data) {
    callback(data);
  }, function(erro) {
    errCallback(erro);
  });
}