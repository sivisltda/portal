import request from '../core/request.js';
import { getConfig as configFunc } from './../config/config.js';

const config = configFunc();

export function getDashboard(callback, errCallback) {
    request(config.api+'dashboard/dashboard', 'get', { functionPage: 'dashboard' }, function (res) {
        callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}

export function getUsuario(callback, errCallback) {
    request(config.api+'dashboard/usuario', 'get', { functionPage: 'getUsuario'}, function (res) {
        callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}

export function atualizaUsuario(data, callback, errCallback) {
    request(config.api+'dashboard/usuario', 'post', Object.assign(data, {functionPage: 'atualizarUsuario'}), function (res) {
       callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}

export function trocarSessao(tipo, callback, erroCallback) {
    request(config.api + 'dashboard/usuario', 'post', {functionPage: 'trocarSessao', tipo}, function() {
        callback();
    },  function(erro) {
        erroCallback(erro);
    });
}

export function getVeiculos(options, callback, errCallback) {
    request(config.api+'dashboard/veiculo', 'get', Object.assign(options, { functionPage: 'veiculos' }), function (res) {
        callback(res);
    }, function (erro) {
        errCallback(erro);
    });
}