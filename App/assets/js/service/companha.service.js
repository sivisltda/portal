import request from './../core/request.js';
import { getConfig as configFunc } from './../config/config.js';

const config = configFunc();

export function getLead(search, callback, errCallback) {
  request(config.api+'portal/companha', 'get', Object.assign({
    functionPage: 'verificaLead'
  }, search), function(res) {
    callback(res);
  }, function(erro) {
    errCallback(erro);
  });
}

export function salvarLead(data, callback, errCallback) {
  request(config.api+'portal/companha', 'post', Object.assign(data, {
    functionPage: 'salvarLead' 
  }), function(res) {
    callback(res);
  }, function(erro) {
    errCallback(erro);
  });
}

export function getDadosLead(data, callback, errCallback) {
  request(config.api+'portal/companha', 'get', Object.assign(data,{
    functionPage: 'getDadosLead'
  }), function(res) {
    callback(res);
  }, function(erro) {
    errCallback(erro);
  });
}