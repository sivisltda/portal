import { verificaIdade, valorDesconto, textToNumber } from '../core/function.js';
import { addFieldRules, removeFieldRules } from '../core/validator.js';
import * as formDependente from './formdependente.js';

export function regrasPorIdade(dependentes, idade) {
    return dependentes.map(function(item) {
        return {
            regra: idade >= Number.parseInt(item.idade_min) && (idade <= Number.parseInt(item.idade_max_f) || idade <= Number.parseInt(item.idade_max_m)),
            parentesco: item.id_parentesco
        }
    }).filter(function(item) { return item.regra; });
}

export function listaParentescosPorRegras(regras) {
    return regras.map(function(item) {
        return item.parentesco;
    }).filter(Boolean);
}

export function regrasParentesco(form, regras = [], parentesco, listaParentescos = [], empresa) {
    const campoParentesco = form.find('[name="parentesco"]');
    return regras.map(function(item) {
        return {
            regra: item.regra,
            acao: function() {
                if (!empresa) {
                    campoParentesco.html(Object.keys(parentesco).filter(function(key) {
                        return listaParentescos.length ? listaParentescos.includes(key) : true;
                    }).map(function(key) {
                        return `<option value="${key}">${parentesco[key]}</option>`;
                    }).join(''));
                }   
            }
        }
    })
}

export function configRegrasIdade(form, campos = [], dependentesRegras, parentesco, empresa = false) {
    const rules = formDependente.getRules();
    const idade = verificaIdade(form.find('input[name="data_nascimento"]').val());
    campos.forEach(function(campo) {
        formDependente.setRules(addFieldRules(rules, campo, 'required', 'Campo obrigatório'));
    });
    const regras = regrasPorIdade(dependentesRegras, idade);
    const listaParentescos = listaParentescosPorRegras(regras);
    return [
        {
            regra: idade < 14,
            acao: function() {
                if(!empresa) {
                    campos.forEach(function(campo) {
                        formDependente.setRules(removeFieldRules(rules, campo, 'required'));
                    });
                }
                
            }     
        }, ...regrasParentesco(form, regras, parentesco, listaParentescos, empresa)
    ];
}

export function validacaoDependente(dependentes, regras, dependente) {
    const { _, numMax } = getRegrasQtdRegras(regras);
    if (dependente.id_dependente) {
        dependentes = dependentes.filter(function(d) {
            return d.id_dependente !== dependente.id_dependente;
        });
    }
    if (dependentes.length >= numMax) {
        return {
            resultado: false,
            mensagem: 'Quantidade máxima de dependentes excedida!'
        };
    }
    if (dependente.parentesco) {
        const parentescoDependentes = dependentes.filter(function(d) {
           return  d.id_parentesco === dependente.id_parentesco;
        });
        const regraParentesco = getRegraDependente(regras, dependente);
        if (regraParentesco && parentescoDependentes.length >= regraParentesco.qtd_max) {
            return {
                resultado: false,
                mensagem: `Quantidade máxima de dependentes por parentesco ${regraParentesco.nome_parentesco} excedida!`
            };
        }
    } else {
        const regrasIdades = getRegraDependente(regras, dependente);
        if (!regrasIdades) {
            return {
                resultado: false,
                mensagem: `A idade do Dependente ${dependente.nome} é inválida para esse plano!`
            };
        }
    }   
    return {
        resultado: true,
        mensagem: ''
    } 

}

export function getRegraDependente(dependentesRegras, item) {
    return dependentesRegras.find(function(r) {
        const idade = verificaIdade(item.data_nascimento);
        return ((r.id_parentesco && item.parentesco && r.id_parentesco === item.parentesco)
            || !(r.id_parentesco && item.parentesco)) && (idade >= textToNumber(r.idade_min) && ((item.sexo === 'F' && idade <= textToNumber(r.idade_max_f)) || 
            (item.sexo === 'M' && idade <= textToNumber(r.idade_max_m))));
        });
   ; 
}

export function getRegrasQtdRegras(regras) {
    return regras.reduce(function(acc, item) {
        acc['numMin'] = Math.max(Number.parseInt(item.qtd_min), (acc.hasOwnProperty('numMin') ? acc.numMin : 0));
        acc['numMax'] = Math.max(Number.parseInt(item.qtd_max), (acc.hasOwnProperty('numMax') ? acc.numMax : 0));
        return acc;
    }, {});
}

export function templateRegraDependentes(dependentes) {
    return dependentes.map(function(d) {
        let html = '';
        if (textToNumber(d.valor)) {
            html+=`<li>A cada dependente adicionado acrescenta ${d.valor} na mensalidade.</li>`;
        }
        if (d.id_parentesco) {
            html+=`<li>A quantidade de dependentes permitido é entre ${d.qtd_min} e ${d.qtd_max}, 
            com idade entre ${d.idade_min} a ${d.idade_max_f ? d.idade_max_f : d.idade_max_m} anos e do grau parentesco do tipo: ${d.nome_parentesco}.</li>`;
        } else {
            html+=`<li>A quantidade de dependentes permitido é entre ${d.qtd_min} e ${d.qtd_max}, 
            com idade entre ${d.idade_min} a ${d.idade_max_f ? d.idade_max_f : d.idade_max_m} anos.</li>`;
        }
        return html;
    }).join('');
}

export function valorDependentes(produto, plano, dependentes, adesaoDependente = 0) {
    const desconto = valorDesconto(plano.convenio_tipo, plano.convenio_valor);
    const valorAdesao = plano.desconto_adesao ? desconto(textToNumber(produto.taxa_adesao)) : textToNumber(produto.taxa_adesao);
    return dependentes.reduce(function(acc, d) {
        const regra = getRegraDependente(produto.dependentes, d);
        const valorDependente = regra ? desconto(textToNumber(regra.valor)) : 0;
        acc+= valorDependente + (valorDependente && adesaoDependente ? valorAdesao : 0);
        return acc;
    }, 0);
}