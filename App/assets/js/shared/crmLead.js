import { createInput } from './../core/form.js';
import { errorCamposInvalidos } from './../core/function.js';
import { validate } from './../core/validator.js';

import * as crmService from './../service/crm.service.js';
import store from './../store/app.js';

const { getCoresTema, getProcedencia } = store.getters;

const procedencias = getProcedencia() ? getProcedencia().reduce(function(acc, item) {
    acc[item.id_procedencia] = item.nome_procedencia;
    return acc;
}, {}): {};

const campoLead = {
    type: 'row',
    group: [
        {
            title: 'Dados do Lead',
            inputs: [
                {
                    type: 'text',  size: 'col-sm-7', id: 'nome', label: 'Nome:',
                    name: 'nome', required: true, visibility: true,
                    attributes : {
                        class: 'form-control',
                        placeholder: 'Seu nome'
                    }
                }, {
                    type: 'tel', size: 'col-sm-7', id: 'celular', label: 'Celular:',
                    name: 'celular', required: true, visibility: true,
                    attributes: {
                        class: 'form-control',
                        placeholder: '(xx) xxxxx-xxxx',
                        "data-toggle": "input-mask",
                        "data-mask-format": "(99) 99999-9999"
                    }
                }, {
                    type: "select", size: "col-sm-7", id: "procedencia", 
                    label: "Procedencia:",
                    name: "procedencia", required: true,
                    visibility: true,
                    options: procedencias,
                    attributes: {
                        class: "form-control select2",
                        placeholder: "Selecione uma procedência"
                    }
                }
            ]
        }
    ], rules: {
        nome: {
            rule: "required|min:2",
            message: { required: "Nome é um campo obrigatório", min: "mínimo :min caracteres" }
        }, celular: {
            rule: "required",
            message: {
                required: "Telefone é um campo obrigatório"
            }
        }, procedencia: {
            rule: "required",
            message: { required: "Procedencia é um campo obrigatório" }
        }
    }
};


export function callLead(callback, errCallback) {
    const coresTema = getCoresTema();
    swal({
        title: 'Salvar Lead',
        html: montarTemplate(),
        focusConfirm: false,
        type: 'question',
        preConfirm: function() {
            const form = $('.form-lead');
            const validations = validate(campoLead.rules, form);
            if (validations.length) {
                const errors = validations.reduce(function(acc, i) {
                    acc[i.field] = i.message;
                    return acc; 
                },  {});
                errorCamposInvalidos(form, errors);  
                Swal.showValidationError('Campos inválidos');
            } else {
                return {
                    nome: form.find('[name="nome"]').val(),
                    celular: form.find('[name="celular"]').val(),
                    procedencia: form.find('[name="procedencia"]').val()
                };
            }
        },
        confirmButtonText: 'Salvar',
        cancelButtonText: 'Cancelar',
        cancelButtonColor: '#9E9E9E',
        confirmButtonColor: coresTema.cor_secundaria,
        allowEscapeKey: false,
        allowOutsideClick: false,
        showCancelButton: true,
        keydownListenerCapture: false
    }).then(function(res) {
        if (res.value) {          
            crmService.salvarLead(res.value, function() {
                callback();
            }, function(erro) {
                console.log(erro);
                errCallback(erro);
            });
        } else {
            callback();
        }
    });
}


function montarTemplate() {
    const sections = campoLead.group.map(function(item) {
        return `<div class="section">
            <h4>${item.title}</h4>
            ${createInput(item.inputs, campoLead.type, 'col-sm-4', 'mb-2')}
        </div>`;
    }).join('');
    return `<form class="form-lead" action="javascript:void(0);">${sections}</form>`;
}