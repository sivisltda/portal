import * as pendenciaService from "./../service/pendencia.service.js";
import { textToNumber, numberFormat, maskMoney, convertSerializeObject, convertDataBr } from './../core/function.js';

export function makeModal(modal, item, tipo) {
    const tipos = {
        descontos: {
            title: 'Aprovação de Descontos',
            callback: function () {
                getModalDescontos(modal, item);
            },
            button: {
                direction: 'justify-content-center',
                items: [
                    {
                        type: 'confirm',
                        text: 'Confirmar Desconto',
                        action: () => {
                            modal.find('.modal-body').find('form').submit();
                        }
                    }
                ]
            }
        },
        contrato: {
            title: 'Link de Assinatura de Contrato de Adesão',
            callback: function () {
                templateModalAssinatura(modal, item);
            }
        },
        vistoria: {
            title: 'Link de Vistoria',
            callback: function () {
                templateVistoria(modal, item);
            }
        },
        pagamento: {
            title: 'Upload de Comprovante de Pagamento',
            callback: function () {
                templateModalComprovante(modal, item, 'P');
            },
            button: {
                direction: 'justify-content-between',
                items: [
                    {
                        type: 'confirm',
                        text: 'Enviar Comprovante',
                        action: () => {
                            modal.find('.modal-body').find('form').submit();
                        }
                    }
                ]
            }
        },
        residencia: {
            title: 'Upload de Comprovante de Residência',
            callback: function () {
                templateModalComprovante(modal, item, 'R');
            },
            button: {
                direction: 'justify-content-between',
                items: [
                    {
                        type: 'confirm',
                        text: 'Enviar Comprovante',
                        action: () => {
                            modal.find('.modal-body').find('form').submit();
                        }
                    }
                ]
            }
        },
        documento: {
            title: 'Upload de Documento do Veículo',
            callback: function () {
                templateModalComprovante(modal, item, 'D');
            },
            button: {
                direction: 'justify-content-between',
                items: [
                    {
                        type: 'confirm',
                        text: 'Enviar Comprovante',
                        action: () => {
                            modal.find('.modal-body').find('form').submit();
                        }
                    }
                ]
            }
        },
        habilitacao: {
            title: 'Upload de Comprovante de Habilitação',
            callback: function () {
                templateModalComprovante(modal, item, 'H');
            },
            button: {
                direction: 'justify-content-between',
                items: [
                    {
                        type: 'confirm',
                        text: 'Enviar Comprovante',
                        action: () => {
                            modal.find('.modal-body').find('form').submit();
                        }
                    }
                ]
            }
        }
    };
    modal.find('.modal-body').html('');
    if (tipos.hasOwnProperty(tipo)) {
        const modalTipo = tipos[tipo];
        modal.find('.modal-title').text(modalTipo.title);
        modalTipo.callback();
        montarFooterModal(modal, modalTipo);
    }
}

function getModalDescontos(modal, item) {
    pendenciaService.getListaAcessorios({id_plano: item.id_plano}, function (data) {
        templateModalDescontos(modal, item, data);
    }, function (error) {
        console.log(error);
        swal('Erro', 'Ocorreu um erro ao listar os acessórios', 'error');
    });
}

function templateModalDescontos(modal, item, data) {
    const total = data.acessorios.reduce(function (acc, acessorio) {
        acc += textToNumber(acessorio.valor);
        return acc;
    }, textToNumber(item.valor));
    const valorComDesconto = textToNumber(data.convenios.valor_total) - textToNumber(data.convenios.valor_desconto);
    const valorReferente = function (valorTotal, valor) {
        return valorTotal * (textToNumber(valor) / 100);
    };
    const valorDescontoMensalidade = valorReferente(
        total, data.permissao.fin_desc_plano_tot
    );
    const valorAdesao = textToNumber(data.convenios.valor_adesao);
    const valorDescontoAdesao = valorReferente(valorAdesao,data.permissao.fin_desc_serv_tot);
    const itemsDesconto = [
        {
            title: 'Total Mensalidade',
            valor: numberFormat(total, 1),
            items: [
                {
                    title: 'Desc.Mensalidade',
                    name: 'desconto',
                    readonly: false,
                    valor: data.convenios.valor_desconto,
                    valorMaximo: textToNumber(data.permissao.fin_desc_plano)
                            ? `Desc.Máximo: ${numberFormat(valorDescontoMensalidade, 1)}`
                            : false,
                    idConvenio: data.convenios.convenio_veiculo,
                    type: 'Desconto na Mensalidade'
                }, {
                    title: 'Mens.Desconto',
                    name: 'mensalidade',
                    readonly: true,
                    valor: numberFormat(valorComDesconto),
                    valorMaximo: false,
                    idConvenio: null,
                    type: ''
                }
            ]
        }, {
            title: 'Total Adesão',
            valor: 'R$ ' + data.convenios.valor_adesao,
            items: [
                {
                    title: 'Taxa de Adesão',
                    name: 'adesao',
                    readonly: false,
                    valor: data.convenios.valor_desconto_adesao,
                    valorMaximo: textToNumber(data.permissao.fin_desc_serv)
                            ? `Desc.Máximo: ${numberFormat(valorDescontoAdesao, 1)}`
                            : false,
                    idConvenio: data.convenios.convenio_adesao,
                    type: 'Desconto na Taxa de Adesão'
                }, {
                    title: 'Adesão com Desconto',
                    name: 'adesao_desconto',
                    readonly: true,
                    valor: numberFormat(valorAdesao - textToNumber(data.convenios.valor_desconto_adesao)),
                    valorMaximo: false,
                    idConvenio: null,
                    type: ''
                }
            ]

        }
    ];
    modal.find('.modal-body').html(`
    <div class="box-contratas">
      <ul class="mb-2">
        <li>
          <div class="titulo-plano">Composição do plano de benefícios automotivos</div>
        </li>
        <li>
          <div class="box-name-cobertura">
            <span>${item.plano}</span>
          </div>
          <div class="box-preco">${item.valor}</div>
        </li>
      ${data.acessorios.map(function (acessorio) {
        return `
          <li>
            <div class="box-name-cobertura">
              <span>${acessorio.nome}</span>
            </div>
            <div class="box-preco">${acessorio.valor}</div>
          </li>`;
    }).join('')}
      </ul>
      <form id="form-desconto" class="box-desconto">
        ${itemsDesconto.map(function (tipoDesconto) {
        return `
          <div class="item-box-desconto">
            <div class="box-total">
              <div class="info-total">
                <span>${tipoDesconto.title}</span>
              </div>
              <div class="valor-total">${tipoDesconto.valor}</div>
            </div>
            ${tipoDesconto.items.map(function (item) {
            return `<div class="item-desconto">
              <div class="box-info-desconto">
                <div class="info-desconto">
                  <div class="name-desconto">${item.title}</div>
                  ${item.valorMaximo ? `<div class="info-valor-maximo">${item.valorMaximo}</div>` : ''}
                </div>
                ${!item.idConvenio && !item.readonly ? '<span class="editavel">Editável</span>'
                    : (item.idConvenio ? `<a class="btn-remove-convenio" data-id-convenio="${item.idConvenio}" data-type="${item.type}"
                href="javascript:void(0)"><i class="mdi mdi-close"></i></a>` : '')}
              </div>
              <div class="box-input">
                <div class="form-group">
                  <input type="text" name="${item.name}" id="${item.name}" 
                  ${item.readonly ? 'readonly' : ''} value="${item.valor}"
                  class="form-control">
                </div>
              </div>
            </div>`;
        }).join('')}
          </div>`;
    }).join('<hr/>')}
      </form>
    </div>`);

    itemsDesconto.forEach(function (itemDesconto) {
        itemDesconto.items.forEach(function (item) {
            const campo = $('input[name="' + item.name + '"]');
            maskMoney(campo);
        });
    });

    modal.find('.modal-body').find('form').on('submit', function (e) {
        e.preventDefault();
        const t = $(this);
        const dados = Object.entries(convertSerializeObject(t.serialize())).reduce(function (acc, itemForm) {
            acc[itemForm[0]] = textToNumber(itemForm[1]);
            return acc;
        }, {});
        if (dados.desconto > textToNumber(numberFormat(valorDescontoMensalidade, 1))) {
            swal('Atenção', `O valor de desconto da mensalidade não poder ser maior que ${numberFormat(valorDescontoMensalidade, 1)}`, 'warning');
        } else if (dados.adesao > textToNumber(numberFormat(valorDescontoAdesao, 1))) {
            swal('Atenção', `O valor da taxa de adesão não poder ser maior que ${numberFormat(valorDescontoAdesao, 1)}`, 'warning');
        } else {
            pendenciaService.salvarConvenios(Object.assign({
                id_plano: data.convenios.id_plano,
                id_produto: data.convenios.conta_produto,
                id_servico: data.convenios.conta_produto_adesao
            }, dados), function (res) {
                swal('Sucesso', 'Descontos aplicados com sucesso', 'success').then(function () {
                    modal.modal('hide');
                });
            }, function (erro) {
                console.log(erro);
                swal('Erro', 'Ocorreu um erro ao salvar o convenio', 'error');
            });
        }
    });

    modal.find('.modal-body').find('input[name="desconto"]').on('change', function (e) {
        e.preventDefault();
        const t = $(this);
        const valorDesconto = !t.val() ? 0 : textToNumber(t.val());
        const valor = total - valorDesconto;
        t.closest('#form-desconto').find('input[name="mensalidade"]').val(numberFormat(valor));
    });

    modal.find('.modal-body').find('input[name="adesao"]').on('change', function (e) {
        e.preventDefault();
        const t = $(this);
        const valorDesconto = !t.val() ? 0 : textToNumber(t.val());
        const valor = valorAdesao - valorDesconto;
        t.closest('#form-desconto').find('input[name="adesao_desconto"]').val(numberFormat(valor));
    });

    modal.find('.modal-body').find('.btn-remove-convenio').on('click', function (e) {
        e.preventDefault();
        const t = $(this);
        const type = t.data('type');
        const id_convenio = t.data('id-convenio');
        swal({
            title: 'Atenção',
            text: `Você deseja remover o convênio de ${type} do(a) cliente ${item.razao_social}?`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não'
        }).then(function (res) {
            if (res.value) {
                pendenciaService.removerConvenio({id_plano: item.id_plano, id_convenio}, function () {
                    swal('Sucesso', 'Convênio removido com sucesso', 'success').then(function () {
                        getModalDescontos(modal, item);
                    });
                }, function (erro) {
                    console.log(erro);
                    swal('Erro', 'Ocorreu um erro ao remover o convênio', 'error');
                });
            }
        });
    });
}

function montarFooterModal(modal, tipoModal) {
    if (tipoModal.button && tipoModal.button.items.length) {
        let confirmar = -1;
        modal.find('.modal-footer').html(`
        <div class="w-100 d-flex flex-row-reverse ${tipoModal.button.direction}">
        ${tipoModal.button.items.map(function (item, index) {
            if (confirmar === -1)
                confirmar = item.type === 'confirm' ? index : -1;
            return `<button type="button" id="${item.type}" 
            class="btn ${item.type === 'confirm' ? 'btn-success' : 'btn-warning text-white'} btn-lg round"
            ${item.type === 'close' ? 'data-dismiss="modal"' : ''}>${item.text}</button>`;
        }).join('')}
      </div>  
    `);
        if (confirmar > -1) {
            const item = tipoModal.button.items[confirmar];
            modal.find('.modal-footer').find(`#${item.type}`).on('click', item.action);
        }
    } else {
        modal.find('.modal-footer').html('');
    }
}

function templateModalAssinatura(modal, item) {
    const buttons = {
        whatsapp: {
            text: 'Whatsapp',
            icon: '<i class="mdi mdi-whatsapp"></i>',
            class: 'btn-secundaria',
            onClick: function () {
                swal({
                    title: 'Atenção',
                    html: `Você deseja enviar o contrato via Whatsapp para o(a) cliente ${item.razao_social} <br> Através do número ${item.celular}?`,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Sim',
                    cancelButtonText: 'Não'
                }).then(function (res) {
                    if (res.value) {
                        pendenciaService.enviarContratoWhatsapp({
                            id_usuario: item.id_fornecedores_despesas,
                            razao_social: item.razao_social,
                            celular: item.celular,
                            id_plano: item.id_plano,
                            contrato_id: item.contrato_id
                        }, function (r) {
                            swal('Sucesso', r.message, 'success');
                        }, function (erro) {
                            console.log(erro);
                            swal('Erro', 'Ocorreu um erro ao enviar o contrato para Whatsapp', 'error');
                        });
                    }
                });
            }
        },
        contrato: {
            text: 'Ver Contrato',
            icon: '<i class="mdi mdi-certificate"></i>',
            class: 'btn-secundaria',
            onClick: function () {
                window.open(item.contrato_assinatura, '__blank');
            }
        },
        cancelar: {
            text: 'Excluir Contrato',
            icon: '<i class="mdi mdi-delete"></i>',
            class: 'btn-danger',
            onClick: function () {
                if (item.contrato > 0) {
                    swal({
                        title: 'Atenção',
                        html: `Confirma a exclusão do contrato? Este processo é irreversível!`,
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Sim',
                        cancelButtonText: 'Não'
                    }).then(function (res) {
                        if (res.value) {
                            pendenciaService.removerContrato({
                                id_usuario: item.id_fornecedores_despesas,
                                id_plano: item.id_plano,
                                contrato_id: item.contrato_id
                            }, function (r) {
                                swal('Sucesso', r.message, 'success');
                                initGetListaPendencias();
                            }, function (erro) {
                                console.log(erro);
                                swal('Erro', 'Ocorreu um erro ao enviar o contrato para Whatsapp', 'error');
                            });
                        }
                    });
                } else {
                    swal('Erro', 'O contrato ainda não foi assinado!', 'error');
                }
            }
        }
    };
    modal.find('.modal-body').html(`
    <h5>Link de assinatura</h5>
    <div class="row">
      <div class="col-12">
        <div class="form-group">
          <div class="input-group mb-3">
            <input type="text" id="txtLink" class="form-control" value="${item.contrato_url}">
            <div class="input-group-append">
              <button class="btn btn-secundaria" type="button" id="btnLink">
                <i class="mdi mdi-content-copy"></i> Copiar
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12">
        <ul class="list-items">
        ${Object.entries(buttons).map(function (itemButton) {
        return `<li class="item">
          <button class="btn ${itemButton[1].class} item-link link-whatsapp ${itemButton[0]}">
            ${itemButton[1].icon}
            ${itemButton[1].text}
          </button>
        </li>`
    }).join('')}
        </ul>
      </div>
    </div>
  `);
    Object.entries(buttons).forEach(function (button) {
        modal.find('.modal-body').find(`.${button[0]}`).on('click', button[1].onClick);
    });
    modal.find('.modal-body').find('#btnLink').on('click', function (event) {
        event.preventDefault();
        $(this).closest('.form-group').find('#txtLink').select();
        $(this).tooltip({
            placement: 'top',
            title: 'Copiado'
        });
        document.execCommand("copy");
    });
}

function templateVistoria(modal, item) {
    const buttons = {
        whatsapp: {
            text: 'Whatsapp',
            icon: '<i class="mdi mdi-whatsapp"></i>',
            class: 'btn-secundaria',
            onClick: function () {
                swal({
                    title: 'Atenção',
                    html: `Você deseja enviar o link de vistoria via Whatsapp para o(a) cliente ${item.razao_social} <br> Através do número ${item.celular}?`,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Sim',
                    cancelButtonText: 'Não'
                }).then(function (res) {
                    if (res.value) {
                        pendenciaService.enviarVistoriaWhatsapp({
                            id_usuario: item.id_fornecedores_despesas,
                            razao_social: item.razao_social,
                            celular: item.celular,
                            url_vistoria: item.url_vistoria
                        }, function (r) {
                            swal('Sucesso', r.message, 'success');
                        }, function (erro) {
                            console.log(erro);
                            swal('Erro', 'Ocorreu um erro ao enviar a vistoria para o Whatsapp', 'error');
                        });
                    }
                });
            }
        },
        contrato: {
            text: 'Abrir Link',
            icon: '<i class="mdi mdi mdi-link"></i>',
            class: 'btn-secundaria',
            onClick: function () {
                window.open(item.url_vistoria, '__blank');
            }
        }
    };
    modal.find('.modal-body').html(`
    <h5>Link de vistoria</h5>
    <div class="row">
      <div class="col-12">
        <div class="form-group">
          <div class="input-group mb-3">
            <input type="text" id="txtLink" class="form-control" value="${item.url_vistoria}">
            <div class="input-group-append">
              <button class="btn btn-secundaria" type="button" id="btnLink">
                <i class="mdi mdi-content-copy"></i> Copiar
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12">
        <ul class="list-items">
        ${Object.entries(buttons).map(function (itemButton) {
        return `<li class="item">
          <button class="btn ${itemButton[1].class} item-link link-whatsapp ${itemButton[0]}">
            ${itemButton[1].icon}
            ${itemButton[1].text}
          </button>
        </li>`
    }).join('')}
        </ul>
      </div>
    </div>
  `);
    Object.entries(buttons).forEach(function (button) {
        modal.find('.modal-body').find(`.${button[0]}`).on('click', button[1].onClick);
    });
    modal.find('.modal-body').find('#btnLink').on('click', function (event) {
        event.preventDefault();
        $(this).closest('.form-group').find('#txtLink').select();
        $(this).tooltip({
            placement: 'top',
            title: 'Copiado'
        });
        document.execCommand("copy");
    });
}

function templateModalComprovante(modal, item, tipo) {
    modal.find('.modal-body').html(`
  <div class="row">
    <div class="col-12 d-flex justify-content-between mb-2">
      <h5>Upload</h5>
      ${item.comprovante_url_h && tipo === 'H' ? `<div class="d-flex justify-content-end">
          <a class="btn btn-secundaria text-white" href="${item.comprovante_url_h}" 
          target="__blank">Link do comprovante</a>
        </div>` : ''}
      ${item.comprovante_url_p && tipo === 'P' ? `<div class="d-flex justify-content-end">
          <a class="btn btn-secundaria text-white" href="${item.comprovante_url_p}" 
          target="__blank">Link do comprovante</a>
        </div>` : ''}
      ${item.comprovante_url_r && tipo === 'R' ? `<div class="d-flex justify-content-end">
          <a class="btn btn-secundaria text-white" href="${item.comprovante_url_r}" 
          target="__blank">Link do comprovante</a>
        </div>` : ''}
    </div>
    <div class="col-12">
      <form class="form-file">
        <label class="label-file">
          <div class="container-file">
            <div class="texto-file">Nome do Arquivo</div>
            <div class="bg-secundaria text-white btn">
              <i class="mdi mdi-file"></i>
            </div>
            <input type="file" name="file" class="input-file">
          </div>
        </label>
        <div class="texto-file">Nome do Arquivo</div>
      </form>
    </div>
  </div>`);
    modal.find('.modal-body').find('.input-file').on('change', onChangeFile);
    modal.find('.modal-body').find('form').on('submit', function (e) {
        e.preventDefault();
        const inputFile = $(this).find('.input-file')[0];
        const file = inputFile.files.item(0);
        if (file) {
            const formData = new FormData();
            formData.append('id_cliente', item.id_fornecedores_despesas);
            formData.append('id_plano', item.id_plano);
            formData.append('id_mens', item.id_mens);
            formData.append('functionPage', 'saveComprovante');
            formData.append('arquivo', file);
            formData.append('tipo', tipo);
            pendenciaService.saveUpload(formData, function (res) {
                swal('Sucesso', 'Comprovante enviado com sucesso', 'success').then(function () {
                    modal.modal('hide');
                    initGetListaPendencias();
                });
            }, function (erro) {
                console.log(erro);
                const mensagem = erro.responseJSON.erro;
                swal('Erro', mensagem, 'error');
            });
        }
    });
}

function onChangeFile(e) {
    const fileToUpload = e.target.files.item(0);
    const label = $(this).closest('.label-file');
    label.find('.texto-file').text(fileToUpload.name);
    label.next('.texto-file').text(fileToUpload.name);
}