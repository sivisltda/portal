import { getConfig as configFunc }  from "../config/config.js";
import store from "../store/app.js";
import {numberFormat, textToNumber, animaNumero, valorDesconto} from "./../core/function.js";
import * as produtoService from "./../service/produto.service.js";
const config = configFunc();
const { getVeiculo, getUsuario, getProduto, getAcessorios, getCupom, getServicosPlano, getConsultor } = store.getters;

export default function geraMenuInfoValores(lista) {
    const produto = getProduto();
    const veiculo = getVeiculo();
    const acessorios = getAcessorios();
    const cupom = getCupom;
    lista.html(template(produto, veiculo, acessorios));
    const valor = textToNumber(produto.valor_mensal);
    const valorItem = acessorios.length ? 
        acessorios.reduce(function(acc, item) {
            acc += textToNumber(item.preco_venda);
            return acc; 
        }, 0)
    : 0;
    const valorReal = cupom && cupom.convenios_site > 0 ? cupom.valor_desconto : (valor + valorItem);
    animaNumero(lista.find('.container-valor-plano'), valorReal, '<span>mensal</span>'+
        '<span style="display: block;">em 12x sem juros</span>');
    animaNumero(lista.find('.container-cobertura'), valor, '');
    animaNumero(lista.find('.container-adicionais'), valorItem, '');
    const valorAnual = valorReal * 12 ;
    lista.find('.valor-plano-anual').html(`Total = ${numberFormat(valorAnual, 1)}`);
    lista.find(".link-protecao").on('click', function(e) {
        e.preventDefault();
        lista.find('.lista-coberturas, .lista-adicional, .lista-observacoes').toggle();
        const icone = lista.find('.arrow-protecao');
        if(lista.find('.lista-coberturas').is(":visible")) {
            icone.removeClass('mdi-chevron-right').addClass('mdi-chevron-down');
        }else {
            icone.removeClass('mdi-chevron-down').addClass('mdi-chevron-right');
        }
    });
}

function template(produto, veiculo, acessorios = []) {
    const consultor = getConsultor();
    return `<div class="logo-cotacao d-print">
        <img src="${config.image}" alt="${config.nome}">
    </div>
    <div class="data-cotacao d-print">${config.time || new Date().toLocaleString()}</div>
    <div class="print-titulo">
        <h3 class="text-right">${config.rapido ? 'Cotação '+config.nome : 'Proposta de Adesão'}</h3>
    </div>
    ${consultor ? '<div class="nome-consultor">'+consultor.nome+'</div>' : ''}
    <a href="javascript:void(0)" class="link-protecao">
        <span class="float-right d-print-none"><i class="mdi mdi-chevron-down arrow-protecao"></i></span>
        <h4>Valor da sua Proteção</h4>
        <div class="container-valor-plano"></div>
        <div class="valor-plano-anual"></div>
        <div class="container-valor-adesao">${calcularValorAdesao(produto)}</div>
        <div class="container-valor-desconto"></div>
    </a>
    <div class="tabela-pagamento d-print">${tabelaPagamento(produto, acessorios)}</div>
    <div class="container-info-veiculo">
        <div class="info-veiculo">
            ${infoDadosVeiculo(veiculo)}
        </div>
        <div class="container-fipe">
            <p class="info-fipe"><span>${getTipoVeiculo(veiculo)} está avaliado em:</span><span class="valor-fipe">${veiculo.valor}</span></p>
            <p class="data-fipe">de acordo com a tabela FIPE código ${veiculo.codigo_fipe.trim()}.</p>
            <p class="data-fipe">de ${veiculo.mes_referencia.trim()}.</p>
        </div>
    </div>
    <ul class="lista-detalhes-servicos">
        <li>
            <div class="lista-detalhes-item">
                <span class="sinal"></span>
                <span class="nome-servico">Coberturas</span>
                <div class="preco container-cobertura"></div>
            </div>
            <ul class="lista-coberturas">
                ${templateItemServicos(produto, acessorios)}
            </ul>
        </li>
        <li>
            <div class="lista-detalhes-item">
                <span class="sinal"></span>
                <span class="nome-servico">Adicionais</span>
                <div class="preco container-adicionais"></div>
            </div>
            <ul class="lista-adicional">${templateAdicionais(acessorios)}</ul>
        </li>
    </ul>
    <p class="mt-1">* Consulte o regulamento</p>
   ${qrCodeObservacoes()}
   ${adicionaObservacao()}
   ${adicionaAssinatura(consultor)}
   ${config.rapido ? '<div id="rodape-impressao"><span class="text-uppercase">'+config.site_nome+'</span></div>' : ''}   
    <div class="my-5 d-flex d-print-none justify-content-around">
        <button id="btn-imprimir" class="btn btn-light btn-acao-cotacao"><i class="mdi mdi-printer"></i> Imprimir</button>
        ${config.mdl_what && (config.cotacao_interna || (!config.rapido && !consultor)) ? '<button id="btn-whatsapp" class="btn btn-light btn-acao-cotacao"><i class="mdi mdi-whatsapp"></i> Whatsapp</button>' : ''}
    </div>`;
}

function qrCodeObservacoes() {
    if(config.rapido) {
        return `<ul class="lista-observacoes">
                <li>O valor será cobrado todo mês no seu cartão de crédito,boleto ou pix.</li>
            </ul>
            <div id="qr-code" class="row align-items-center">
            <div class="col-4"><img src="${config.urlErp}/util/qrcode/php/qr_img.php?d=${urlLink()}&s=10"
            alt="cotaçao" class="img-fluid img-circle"></div><div class="col-8">
            <div class="item-contato">${itemContato()}</div></div></div>`;
    }
    return '';
}

function urlLink() {
    const consultor = getConsultor();
    const urlBase = config.api.replace('/api/', '');
    return consultor ? urlBase+'/consultor/'+consultor.indicador : urlBase;
}

function itemContato() {
    let html = '';
    const consultor = getConsultor();
    const urlBase = config.api.replace('/api/', '');
    if(consultor) {
        html = ` <h4>Consultor</h4>
                 <h6>${consultor.nome}</h6>
                 <h5>${consultor.celular}</h5>
                 <div class="site">${urlBase+'/consultor/'+consultor.indicador}</div>`;
    } 
    return html;
}

function adicionaObservacao() {
    if(!config.rapido) {
        return `<div class="observacao">
            <h5>Observações:</h5>
        </div>`;
    }
    return '';
}

function infoDadosVeiculo(dados) {
    let items = { placa: 'Placa', marca: 'Marca', modelo: 'Modelo', ano_modelo: 'Ano' };
    if (!config.rapido) {
        items = Object.assign({ razao_social: 'Nome', cnpj: 'CPF' }, items);
        dados = Object.assign({}, getUsuario(), dados);
    }
    return Object.keys(items).map(function(i) {
        return `<div class="row">
                    <div class="col-4"><strong>${items[i]}:</strong></div>
                    <div class="col-8">${dados[i]}</div>
                </div>`;
    }).join('');
}

function getTipoVeiculo(veiculo) {
   const tiposVeiculos =  {
        1: 'O seu carro',
        2: 'A sua moto',
        3: 'O seu caminhão'
    };
    return tiposVeiculos[veiculo.tipo_veiculo];
}

function adicionaAssinatura(consultor) {
    const nome = consultor ? consultor.nome : config.nome + 'Presidente';
    const celular = consultor ? consultor.celular : '';
    if(!config.rapido) {
        const usuario = getUsuario();
        return `
            <div class="assinatura row">
                <div class="col-5 linha">
                    <span class="titulo-assinatura">${config.adm_associado}</span>
                    <span class="nome-cliente">${usuario.razao_social}</span>
                </div>
                <div class="col-5 linha">
                    <span class="titulo-assinatura">${nome}</span>
                    <span class="nome-cliente">${celular}</span>        
                </div>
            </div>
        `;
    }
    return '';
}

function templateItemServicos(produto, acessorios) {
    const servicos       = getServicosPlano();
    const itemsPadrao    = produtoService.getItemsPadroes();
    if (servicos && servicos.length) {
        const padroesId     = itemsPadrao.map(function(p) { return p.id; });
        let acessoriosId    = [];
        if (acessorios.length) {
            acessoriosId = acessorios.filter(function(a) {
                return !padroesId.includes(a.id_servico_acessorio);
            }).map(function(a) { return a.parent; });
        }
        const html = servicos.filter(function(item) {
            return (!item.cobertura && produto.servicos.includes(item.id)) 
            || (padroesId.includes(item.id) && (!acessoriosId.length 
                || (acessoriosId.length && !acessoriosId.includes(Number.parseInt(item.cobertura)))));
        }).map(function(item) {
            return `<li>${item.nome}</li>`;
        }).join('');
        return html;
    }
    return '';
}

function templateAdicionais(acessorios = []) {
        return acessorios.filter(function(item) {
            return textToNumber(item.preco_venda);
        }).map(function (item) {
            return `<li>
                    <div class="row justify-content-between">
                        <div class="col-9">${item.descricao}</div>
                        <div class="col-3 valor">${item.preco_venda}</div>
                    </div>
            </li>`;
        }).join('');
    }
    
function calcularValorAdesao(produto) {
    const { taxa_adesao } = produto;
    if (textToNumber(taxa_adesao) > 0) {
        return `<p class="mb-0">A taxa de adesão no valor de:
            <span class="adesao">${taxa_adesao}</span></p>`;
    } else {
       return '';
    }
}

function tabelaPagamento(produto, acessorios = []) {
    const valorAcessorio = acessorios.reduce(function(acc, item) {
        acc+= textToNumber(item.preco_venda);
        return acc;
    }, 0);
    const parcelas = produto.parcelas.map(function(parcela) {
        const desconto = valorDesconto('P', parcela.desconto);
        const valorFinalAcessorio = desconto(valorAcessorio);
        const valorFinal = textToNumber(parcela.valor_unico) + (valorFinalAcessorio * (parcela.parcela < 1 ? 1 : parcela.parcela));
        const descricao = parcela.parcela == -2 ? 'Pix Recorrente' : parcela.parcela == -1 ? 'Boleto Recorrente' : (parcela.parcela == 0 ? 'Cartão de Crédito Recorrente' : parcela.parcela+'x (Mensal)');
        return `<div class="row align-items-center no-gutters">
        <div class="w-20"><span class="check"></span></div>
        <div class="col-md">${descricao}</div>
        <div class="col-md-4">${parcela && textToNumber(parcela.desconto) === 0 ? '' : parcela.desconto+'% Desconto'}</div>
        <div class="col-md-3">${numberFormat(valorFinal, 1)}</div></div>`;
    });
    if (parcelas.length) {
        return `<h5>Forma de Pagamento:</h5>${parcelas.join('')}`;
    }
    return '';
}