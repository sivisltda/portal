import store from './../store/app.js';
import { createInput } from './../core/form.js';
import { numberFormat, textToNumber, montaCartaoCredito, convertSerializeObject, valorDesconto } from './../core/function.js';
import { getConfig } from './../config/config.js';
import { validate } from './../core/validator.js';
import * as formCartao from '../forms/pagamento.js';
import * as regraDependente from './regraDependentes.js';
import * as pagamentoService from './../service/pagamento.service.js';

const { getProduto, getVeiculo, getPlano, getIdParcela, getProrata, getDependentes, getAcessorios } = store.getters;
const { setIdParcela, setParcela } = store.setters;
const config = getConfig();

const observer = {
    changeParcela: [],
    processoBoleto: [],
    processoCartao: [],
    processoLink: []    
};

let itemTipoPagamentos  = [];
let valorAcrescimo = 0;

const regrasPagamento = {
    cartao: {
        titulo: 'Cartão de Crédito',
        subTitulo: 'Pagamento Recorrente',
        icon: 'mdi mdi-credit-card',
        id: 'cartao-tab',
        controls: 'cartao',
        template: templateCartao,
        regra: function(item) {
            return item === 0;
        }
    }, boleto: {
        titulo: 'Boleto',
        subTitulo: 'Boleto',
        icon: 'mdi mdi-barcode-scan',
        id: 'boleto-tab',
        controls: 'boleto',
        template: templateBoleto,
        regra: function(item) {
            return item === 1;
        }
    }, pix: {
        titulo: 'Pix',
        subTitulo: 'Pix',
        icon: 'mdi mdi-barcode-scan',
        id: 'pix-tab',
        controls: 'pix',
        template: templatePix,
        regra: function(item) {
            return item === -2;
        }
    }, link: {
        titulo: 'Link',
        subTitulo: 'Link',
        icon: 'mdi mdi-link',
        id: 'link-tab',
        controls: 'link',
        template: templateLink,
        regra: function(item) {
            return item === -3;
        }
    }, boletoRecorrente: {
        titulo: 'Boleto',
        subTitulo: 'Boleto Recorrente',
        icon: 'mdi mdi-barcode-scan',
        id: 'boleto-tab',
        controls: 'boleto',
        template: templateBoleto,
        regra: function(item) {
            return item === -1;
        }
    }, mensal: {
        titulo: 'Cartão de Crédito',
        subTitulo: '',
        icon: 'mdi mdi-credit-card',
        id: 'mensal-tab',
        controls: 'mensal',
        template: templateMensal,
        regra: function(item) {
            return item > 1;
        }
    }
};

function getLabelContratual(item) {
    return item + " x ";
}

export function subscribe(chave, id, acao) {
    const observe = observer[chave].find(function(item) {
        return item.id === id;
    });
    if (!observe) {
        observer[chave].push({id, acao});
    }
}

function subject(chave, valor = null) {
    observer[chave].forEach(function(item) {
        item.acao(valor);
    });
}

export function templatePagamento(acrescimo) {
    const produto = getProduto();
    const plano = getPlano();
    const acessorios = getAcessorios().filter(function(item) {
        return !item.padrao;
    });
    if (!getIdParcela()) {
        setIdParcela(plano.id_parcela);
    }
    valorAcrescimo = acrescimo;    
    const parcelas = produto.parcelas;    
    if (parcelas.length > 0) {
        let parcela_bol = parcelas.filter(function(p) {
            return p.parcela === "-1";
        });
        let parcela_link = parcelas.filter(function(p) {
            return p.parcela === "-3";
        });
        if (parcela_bol.length > 0 && parcela_link.length === 0) {
            parcelas.push({desconto: parcela_bol[0].desconto, id_parcela: parcela_bol[0].id_parcela, parcela: "-3",
            valor_comissao: parcela_bol[0].valor_comissao, valor_desconto: parcela_bol[0].valor_desconto,
            valor_unico: parcela_bol[0].valor_unico, valor_zerado: 0});
        }
    }    
    let isMensal = false;    
    const regras = getRegraPagamento();
    if (config.plano_mens_contract && !acessorios.length) {
        let isBoleto = false;
        itemTipoPagamentos = parcelas.map(function(p) {
            const numParcela = textToNumber(p.parcela);
            const tipo = regras.find(function(regra) {
                return regra.regra(numParcela);
            });
            if((numParcela === 12 || ([-1, 1].includes(numParcela) && !isBoleto))) {
                if (!isBoleto) isBoleto = [-1, 1].includes(numParcela);
                const valor_unico = [-1, 1].includes(numParcela) ?  numberFormat(textToNumber(p.valor_unico) * 12, 1) : p.valor_unico;
                return {
                    ...p,
                    valor_unico,
                    tipo
                };
            }
            return false;
        }).filter(Boolean);
    } else {
        itemTipoPagamentos = parcelas.map(function(p) {
            const numParcela = textToNumber(p.parcela);
            const tipo = regras.find(function(regra) {
                return regra.regra(numParcela);
            });
            if((tipo.chave !== 'mensal') || !isMensal) {
                if (!isMensal) isMensal = tipo.chave === 'mensal';
                return {
                    ...p,
                    tipo
                };
            }
            return false;
        }).filter(Boolean);
    }
    return `<div class="row h-100">
        <div class="col-md-6 mb-3">
            <div class="info-ordem">
                <span class="info-ordem-titulo texto-secundaria">N° de ordem:</span>
                <span class="info-ordem-numero texto-primaria">${plano.id_mens}</span>
            </div>
            <div class="forma-pagamento w-100">
                <h4>Selecione uma forma de Pagamento</h4>
                <ul class="lista-forma-pagamento nav nav-tabs">
                    ${templateItemTipoPagamento(plano)}
                </ul>
            </div>
        </div>
        <div class="col-md-6 separador-pagamento mb-3">
            <div class="info-ordem">
                <span class="info-ordem-titulo texto-secundaria">Valor a Pagar:</span>
                <span class="info-ordem-valor texto-primaria"></span>
                <span class="info-prorata btn-secundaria"></span>
            </div>
            <div class="tipo-pagamento tab-content" id="myTabContent">
                ${templateTabTipoPagamento(plano)}
            </div>
        </div>
    </div>`;
}

export function geraEventos(containerPagamento) {
    if (containerPagamento.find('.form-cartao').length) {
        montaCartaoCredito({
            form: '.form-cartao',
            container: '.card-wrapper',
            name: 'input.nome_cartao',
            number: 'input.cartao_numero',
            expiry: 'input.data_expiracao',
            cvc: 'input.cvv',
            submit: '.btn-pagar'
        });
    }
    if (containerPagamento.find('.form-cartao1').length) {
        montaCartaoCredito({
            form: '.form-cartao1',
            container: '.card-wrapper1',
            name: 'input.nome_cartao',
            number: 'input.cartao_numero',
            expiry: 'input.data_expiracao',
            cvc: 'input.cvv',
            submit: '.btn-pagar'
        });
    }        
    containerPagamento.find('.form-cartao, .form-cartao1').on('submit', eventProcessarCartao);
    containerPagamento.find('#cartao_bandeira, #cartao_bandeira1').select2({templateResult: formCartao.formatState, templateSelection: formCartao.formatState});    
    formCartao.actionState('#cartao_numero, #cartao_numero1', '#cartao_bandeira, #cartao_bandeira1');
    
    const boletos = containerPagamento.find('.btn-boleto');
    boletos.on('click', eventProcessarBoleto);    
    
    const links = containerPagamento.find('.btn-link');
    links.on('click', eventProcessarLink);   
    
    const listaFormaPagamento = containerPagamento.find('.lista-forma-pagamento');
    listaFormaPagamento.find('a[data-toggle="tab"]').on('shown.bs.tab', selectTabListaFormaPagamento);
    const selectParcela = containerPagamento.find('#parcelas');
    if (selectParcela.length) {
        selectParcela.on('change', changeSelectParcela);
    }
}

function getRegraPagamento() {
    return Object.keys(regrasPagamento).map(function(chave) {
        const item = regrasPagamento[chave];
        item.chave = chave;
        return item;
    });
}

function templatePix(habilita = 0) {
    return `<h4 class="texto-secundaria">Pagamento via Pix</h4>
    <div class="alert alert-danger" role="alert">
    <h4 class="alert-heading">Atenção!</h4>
    <p>Clique no botão para gerar o seu Pix</p>
    </div>
    <div class="d-flex justify-content-center mt-2">
        <button type="submit" class="btn btn-secundaria text-uppercase btn-boleto"><i class="fa fa-billet"></i> Gerar pix</button>
    </div>`;
}

function templateLink(habilita = 0) {
    return `<h4 class="texto-secundaria">Pagamento via Link</h4>
    <div class="alert alert-danger" role="alert">
    <h4 class="alert-heading">Atenção!</h4>
    <p>Clique no botão para gerar o seu Link</p>
    </div>
    <div class="d-flex justify-content-center mt-2">
        <button type="submit" class="btn btn-secundaria text-uppercase btn-link"><i class="fa fa-billet"></i> Gerar Link</button>
    </div>`;
}

function templateBoleto(habilita = 0) {
    const textoBoleto = ["173", "973"].includes(config.contrato) ? '<b>Importante:</b><br>\n' +
    '<p>Os boletos mensais são enviados exclusivamente para o email do contratante e/ou via WhatsApp e o pagamento ' +
    'deve ser realizado nas agências bancárias ou casas lotéricas até o dia do vencimento.</p>' : '';
    const alerta = habilita ? `<div class="alert alert-danger" role="alert">
    <h4 class="alert-heading">Atenção!</h4>
    <p>Pagar o boleto somente após 24 horas</p>
    </div>` : `<div class="alert alert-danger" role="alert">
    <h4 class="alert-heading">Atenção!</h4>
    <p>Clique no botão para gerar o seu boleto</p>
    </div>`;
    return `<h4 class="texto-secundaria">Pagamento via Boleto</h4>
    ${textoBoleto}
    ${alerta}
    <div class="d-flex justify-content-center mt-2">
        <button type="submit" class="btn btn-secundaria text-uppercase btn-boleto"><i class="fa fa-billet"></i> Gerar boleto</button>
    </div>`;
}

function templateCartao(id = '') {
    const textoCartao = ["973", "173"].includes(config.contrato) && !id ? '<b>Importante</b><br/>\n' +
    '<p>Os pagamentos são mensais e as cobranças recorrentes são realizadas automaticamente no cartão de crédito ' +
    'no dia do vencimento selecionado na etapa anterior.</p>' : '';
    const form = formCartao.formDados([], id);
    return `<h4 class="texto-secundaria">Pagamento via Cartão de Crédito</h4>
    ${textoCartao}
    <form class="form-cartao${id}" novalidate>
    <div class="card-wrapper${id}"></div>
    <div class="row mt-3">${createInput(form.inputs, form.type)}</div>
    <div class="row"><div class="col-sm-12 mt-2">
    <button class="btn btn-secundaria text-uppercase btn-block btn-pagar">Pagar agora</button></div></div></form>`;
}

function templateMensal() {
    const produto = getProduto();
    const meses = produto.parcelas.filter(function(p) {
        return p.parcela > 1;
    }).map(function(p) {
        const valorTotal = textToNumber(p.valor_unico) + (valorAcrescimo * p.parcela);
        const msg = produto.parcelar || produto.parcelar_site ? `${p.parcela+'x '+numberFormat((valorTotal / textToNumber(p.parcela)), 1)} = ` : ''; 
        return `<option value="${p.id_parcela}"${p.id_parcela === getIdParcela() ? ' selected' : ''}>
        ${p.parcela} meses - ${msg}${numberFormat(valorTotal, 1)}</option>`;
    }).join('');
    return `
        <div class="row">
            <div class="col-12">
                <div class="form-group mb-4">
                    <label for="parcelas texto-secundaria">${produto.parcelar_site ? 'Parcelamento' : 'Período'}:</label>
                    <select id="parcelas" class="form-control">${meses}</select>
                </div>
                ${templateCartao('1')}
            </div>
        </div>       
    `;
}

function templateItemTipoPagamento(plano) {
    let html = itemTipoPagamentos.map(function(item) {
        const itemRegraPagto = item.tipo;
        const classeActive = itemRegraPagto.regra(textToNumber(plano.parcela)) ? ' active' : ''; 
        return `<li class="nav-item w-100">
            <a href="#${itemRegraPagto.chave}" class="link-pagamento nav-link ${classeActive}" id="${itemRegraPagto.id}" data-toggle="tab" role="tab" aria-controls="${itemRegraPagto.controls}" aria-selected="true">
                <div class="col-icone">
                    <span class="${itemRegraPagto.icon}"></span>
                </div>
                <div class="col-info-tipo-pagamento">
                    <div class="info-tipo-pagamento-titulo">${itemRegraPagto.titulo}</div>
                    <div class="info-tipo-pagamento-subtitulo">${(itemRegraPagto.chave === "mensal" ? item.parcela + "x" : itemRegraPagto.subTitulo)}</div>
                </div>
            </a>
        </li>`;
    }).filter(Boolean).join(''); 
    if (config.terminal_venda) {
        html+=`<li class="nav-item w-100">
        <a href="#manual" class="link-pagamento nav-link" id="manual-tab" data-toggle="tab" role="tab" aria-controls="manual" aria-selected="true">
            <div class="col-icone">
                <span class="mdi mdi-barcode-scan"></span>
            </div>
            <div class="col-info-tipo-pagamento">
                <div class="info-tipo-pagamento-titulo">Manual</div>
                <div class="info-tipo-pagamento-subtitulo">Negociação</div>
            </div>
        </a>
    </li>`;
    }
    return html;
}

function templateTabTipoPagamento(plano) {
    let html = itemTipoPagamentos.map(function(item) {
        const itemRegraPagto = item.tipo;
        const classeActive = itemRegraPagto.regra(textToNumber(plano.parcela)) ? ' show active' : ''; 
        const templateItem = item.parcela == 1 ? itemRegraPagto.template(1) : itemRegraPagto.template();
        return `<div id="${itemRegraPagto.chave}" class="tab-pane fade${classeActive}" role="tabpanel" aria-labelledby="${itemRegraPagto.controls}">
                ${templateItem}
        </div>`;
    }).join('');
    if (config.terminal_venda) {
        html+= `<div id="manual" class="tab-pane fade" role="tabpanel" aria-labelledby="manual-tabs">
            <h4>Manual</h4>
        </div>`;
    }
    return html;
}

function selectTabListaFormaPagamento(e) {
    const tabAtual = e.target; // aba que acabou de ser ativada
    const tabAnt = e.relatedTarget; // aba, anteriormente, ativa
    const idAtiva = tabAtual.getAttribute('href').replace('#', '');
    if (idAtiva !== 'manual') {
        const itemPagamento = itemTipoPagamentos.find(function(item) {
            return idAtiva === item.tipo.chave;
        });
        setIdParcela(itemPagamento.id_parcela);
        setParcela(itemPagamento);
        subject('changeParcela', itemPagamento.id_parcela);
    }
}

function changeSelectParcela(e) {
    e.preventDefault();
    const t = $(this);
    const produto =  getProduto();
    const parcela = produto.parcelas.find(function(p) {
        return Number.parseInt(p.id_parcela) === Number.parseInt(t.val());
    });
    setIdParcela(parcela.id_parcela);
    setParcela(parcela);
    subject('changeParcela', parcela.id_parcela);
}

function eventProcessarCartao(e) {
    e.preventDefault();
    const t = $(this);
    const form = formCartao.formDados([]);
    t.find('.invalid-feedback').html('');
    const validations  = validate(form.rules, t);
    if (validations.length) {
        $.each(validations, function(i, v) {
            t.find(`[name="${v.field}"]`).addClass('is-invalid').next('.invalid-feedback').text(v.message);
        });
        swal('Atenção!', 'Informe os campos obrigatórios', 'warning');
        return false;
    }
    subject('processoCartao', t);    
}

function eventProcessarBoleto(e) {
    e.preventDefault();
    subject('processoBoleto');
}

function eventProcessarLink(e) {
    e.preventDefault();
    subject('processoLink');
}

export function processarCartao(t, data, callback, errCallback) {
    const produto = getProduto();
    const dados = Object.assign({}, data, convertSerializeObject(t.serialize()));
    dados.id_produto = produto.id;
    pagamentoService.processarPagamento(dados, function(res) {
        swal('Sucesso!', 'Transação paga com sucesso', 'success').then(function(rep) {
           callback();
        });
    }, function(erro, mensagem) {        
        if(erro === 1) {
            swal({
                title: 'Não foi possivel efetuar a compra com o seu cartão',
                text: "Gostaria de alterar seu cartão?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Sim, vou mudar o cartão!',
                cancelButtonText: 'Não, tentarei mais tarde!',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#9E9E9E'
            }).then((result) => {
                if (result.value) {
                    t.find('[name="cartao_numero"], [name="data_expiracao"], [name="cvv"], [name="nome_cartao"], [name="cartao_bandeira"]').val('');
                }
            });
        } else if(erro === 2) {
            swal({type: 'error', title: 'Erro', text: mensagem});
        }
        errCallback(erro);
    });       
}

export function processarBoleto(data, callback, errCallback) {
    const produto = getProduto();
    data.id_produto = produto.id;
    pagamentoService.processarPagamento(data, function(res) {
        window.open(res.boleto.url);
        swal('Sucesso!', 'Documento gerado com sucesso!', 'success').then(function(rep) {
           callback(res);
        });        
   }, function (erro, mensagem) {
        console.log(erro);
        if (erro === 1) {
            swal({
                title: 'Erro',
                text: "Não foi possivel gerar o seu documento, tente novamente mais tarde",
                type: 'error'
            });
        } else if(erro === 2) {
            swal({type: 'error', title: 'Erro', text: mensagem });
        }
        errCallback(erro);
   });
}

export function processarLink(data, callback, errCallback) {
    const produto = getProduto();
    data.id_produto = produto.id;
    pagamentoService.processarPagamento(data, function(res) {
        window.open(res.boleto.url);
        swal('Sucesso!', 'Documento gerado com sucesso!', 'success').then(function(rep) {
           callback(res);
        });        
   }, function (erro, mensagem) {
        console.log(erro);
        if (erro === 1) {
            swal({
                title: 'Erro',
                text: "Não foi possivel gerar o seu documento, tente novamente mais tarde",
                type: 'error'
            });
        } else if(erro === 2) {
            swal({type: 'error', title: 'Erro', text: mensagem });
        }
        errCallback(erro);
   });
}

export function makeValores() {
    const veiculo = getVeiculo();
    const produto = getProduto();
    const plano = getPlano();
    const desconto = valorDesconto(plano.convenio_tipo, plano.convenio_valor);    
    const parcelas = produto.parcelas;        
    const parcela = getIdParcela() && parcelas.find(function(p) {return p.id_parcela == getIdParcela();}) ? 
          parcelas.find(function(p) {return p.id_parcela == getIdParcela();}) : parcelas[0];
    const numParcela = textToNumber(parcela.parcela) < 1 ? 1 : textToNumber(parcela.parcela);
    const parcelar = produto.parcelar;
    const acessorios = getAcessorios().filter(function(item) {
        return !item.padrao;
    });    
    const valorUnico = textToNumber(parcela.valor_unico) * (config.plano_mens_contract && !acessorios.length && ["-1", "1"].includes(parcela.parcela) ? 12 : 1);        
    const valorAdesao = (textToNumber(produto.taxa_adesao) > 0 && veiculo !== null && textToNumber(produto.taxa_adesao) >= textToNumber(veiculo.valor_desc_adesao) ? (textToNumber(produto.taxa_adesao) - textToNumber(veiculo.valor_desc_adesao)) : 0);    
    const valorTotal = function(acrescimo) {
        return (parcela.valor_zerado ? 0 : desconto(valorUnico)) + (numParcela * acrescimo);
    }
    const valorProrata = function() {
        const prorata =  getProrata();
        if (!config.exc_ven_prorata && prorata) {
            const qtd = produto.hasOwnProperty('dependentes') && getDependentes().length ? getDependentes().reduce(function(acc, item) {
                const regra = regraDependente.getRegraDependente(produto.dependentes, item);
                if (regra && textToNumber(regra.valor)) {
                    acc++;
                }
                return acc;
            }, 0) : 0; 
            return parseFloat(prorata.prorata) * (qtd ? qtd : 1);
        }
        return 0;
    }
    return {
        produto,
        plano,
        desconto,
        parcelas,
        parcela,
        numParcela,
        parcelar,
        valorUnico, 
        valorAdesao,
        valorTotal,
        valorProrata
    }
}

export function setValorPagar(container, dadosValores, acrescimo) {
    const { parcela, parcelar, numParcela, produto, valorAdesao} = dadosValores;    
    const prorata = getProrata();
    const valorTotal = dadosValores.valorTotal(acrescimo);
    const valorProrata = dadosValores.valorProrata();        
    const valor = !produto.adesao_soma && valorAdesao ? valorAdesao : ((parcelar ? valorTotal / numParcela : valorTotal) + (parcela.valor_zerado ? 0 : valorAdesao) + valorProrata);
    container.find('.info-ordem-valor').text(numberFormat(valor, 1));
    if (!config.exc_ven_prorata && prorata && prorata.prorata) {
        container.find('.info-prorata').addClass('active').text(`Valor de pro rata de: ${numberFormat(valorProrata, 1)} referente ao vencimento para dia ${prorata.data}.`);
    }
}
