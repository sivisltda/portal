
export function templateCarteiras(item, full) {
    return item.map(function(carteira) {
        return templateCarteira(carteira, full);
    }).join('');
}

function templateCarteira(dados, full) {
    return `<div class="${!full ? "col-lg-6 " : ""}mb-3">
    <div class="card">
        <div class="card-body">
            <h4 class="header-title mb-3">Cartão Digital - ${dados.descricao}</h4>
            <hr/>
            <div class="fundo-preto">                        
                <div class="card-digital">
                    <div class="card-image front">
                        <img src="${dados.foto_capa}" alt="${dados.descricao}" />
                    </div>
                    <div class="card-image back">
                        <img src="${dados.foto_verso}" alt="${dados.descricao}" />
                        <div class="info-usuario-card">
                            <div class="details" style="
                            margin-top: ${dados.margin_top}%; 
                            margin-left: ${dados.margin_left / 5}%;
                            margin-right: ${dados.margin_left / 5}%;
                            ">${dados.mensagem}</div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-link flip-card show-mobile"><i class="mdi mdi-credit-card-multiple"></i></button>
                <button class="btn btn-link btn-close-card show-mobile"><i class="mdi mdi-window-close"></i></button>
            </div>
            <button class="btn btn-primary btn-block open-carteira show-mobile">Mostrar carterinha</button>               
        </div>
    </div>
</div>`;
}