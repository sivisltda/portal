import { getConfig as configFunc }  from "../config/config.js";
import {textToNumber, numberFormat, isMobile} from "../core/function.js";
import * as crmService from "../service/crm.service.js";
import * as produtoService from "./../service/produto.service.js";
import {getValorDesconto, getValorRangeComissao, getValorRangeDesconto} from './../shared/cupom.js';
import store from "../store/app.js";

const config = configFunc();
const {getUsuario, getProduto, getVeiculo, getAcessorios, getServicosPlano,
   getLead} = store.getters;

String.prototype.convertUtf8 = function() {
  const chars = {
      "&Aacute;" : "Á", "&aacute;" : "á", "&Acirc;" : "Â", "&acirc;" : "â", "&Agrave;" : "À", "&agrave;" : "à",
      "&Aring;" : "Å", "&aring;" : "å", "&Atilde;" : "Ã", "&atilde;" : "ã", "&Auml;" : "Ä", "&auml;" : "ä",
      "&AElig;" : "Æ", "&aelig;" : "æ", "&Eacute;" : "É", "&eacute;" : "é", "&Ecirc;" : "Ê", "&ecirc;" : "ê",
      "&Egrave;" : "È", "&egrave;" : "è", "&Euml;" : "Ë", "&euml;" : "ë", "&ETH;" : "Ð", "&eth;" : "ð",
      "&Iacute;" : "Í", "&iacute;" : "í", "&Icirc;" : "Î", "&icirc;" : "î", "&Igrave;" : "Ì", "&igrave;" : "ì",
      "&Iuml;" : "Ï", "&iuml;" : "ï", "&Oacute;" : "Ó", "&oacute;" : "ó", "&Ocirc;" : "Ô", "&ocirc;" : "ô",
      "&Ograve;" : "Ò", "&ograve;" : "ò", "&Oslash;" : "Ø", "&oslash;" : "ø", "&Otilde;" : "Õ", "&otilde;" : "õ",
      "&Ouml;" : "Ö", "&ouml;" : "ö", "&Uacute;" : "Ú", "&uacute;" : "ú", "&Ucirc;" : "Û", "&ucirc;" : "û",
      "&Ugrave;" : "Ù", "&ugrave;" : "ù", "&Uuml;" : "Ü", "&uuml;" : "ü", "&Ccedil;" : "Ç", "&ccedil;" : "ç",
      "&Ntilde;" : "Ñ", "&ntilde;" : "ñ", "&lt;" : "<", "&gt;" : ">", "&amp;" : "&", "&quot;" : '"', "&reg;" : "®",
      "&copy;" : "©", "&Yacute;" : "Ý", "&yacute;" : "ý", "&THORN;" : "Þ", "&thorn;" : "þ", "&szlig;" : "ß",
      "&nbsp;": ""
  };
  const strChars = Object.keys(chars).join('|');
  return this.replace(new RegExp(strChars, 'gi'), function(i) {
      return chars[i];
  });
};

function getDicionario() {
  let dicionario = {};
  const usuario = getUsuario();
  const lead = getLead();
  const produto = getProduto();
  const veiculo = getVeiculo();

  const getStringServicos = function() {
    return getServicosPlano().map(function(servico) {
      return `✔ ${servico.nome}; %0a`;
    }).join('');
  };

  if (lead) {
    dicionario = Object.assign(dicionario, {
      '||A_mat||': lead.id,
      '||A_nom||': lead.nome,
      '||A_nomc||': lead.nome,
      '||A_nomp||': lead.nome.split(' ')[0],
      '||A_email||': lead.email || '',
      '||A_celular||': lead.celular
    });
  } else if (usuario) {
    dicionario = Object.assign(dicionario, {
      '||A_mat||': usuario.id,
      '||A_nom||': usuario.razao_social,
      '||A_nomc||': usuario.razao_social,
      '||A_nomp||': usuario.razao_social.split(' ')[0],
      '||A_email||': usuario.email,
      '||A_celular||': usuario.celular,
      '||A_dt_cad||': usuario.data_cadastro,
      '||A_cpf||': usuario.cnpj,
      '||A_rg||': usuario.inscricao_estadual,
      '||A_dt_nasc||': usuario.data_nascimento,
      '||A_sexo||': usuario.sexo,
      '||A_est_civil||': usuario.estado_civil,
      '||A_cep||': usuario.cep,
      '||A_lograd||': usuario.endereco,
      '||A_end_num||': usuario.numero,
      '||A_comp_end||': usuario.complemento,
      '||A_bairro||': usuario.bairro,
      '||A_cidade||': usuario.cidade,
      '||A_uf||': usuario.estado
    });
  }
  if(produto) {
    dicionario = Object.assign(dicionario, {
      '||P_desc||': produto.nome,
      '||P_adesao||': produto.taxa_adesao,
      '||P_val||': numberFormat(produto.valor_mensal, 1),
      '||P_servicos||': getStringServicos()
    });    
    if (veiculo) {
      const valorDesconto = (config.adm_desconto_range ? getValorRangeDesconto() : getValorDesconto());               
      const acessorios = getAcessorios();            
      const valorAcessorio = acessorios.reduce(function(acc, item) {
        return acc + textToNumber(item.preco_venda);
      }, 0);
      const valorReal = ((textToNumber(produto.valor_mensal) + valorAcessorio) - valorDesconto);      
      const valorFranquiaCalc = (produto.cota_defaut_prod_tp === "1" ? produto.cota_defaut_prod : ((textToNumber(veiculo.valor) * produto.cota_defaut_prod)/100));
      const valorFranquia = (textToNumber(produto.piso_franquia) > valorFranquiaCalc ? produto.piso_franquia : numberFormat(valorFranquiaCalc, 1));      
      dicionario = Object.assign(dicionario, {
        '||P_servicos||': `✔ ${$(produto.mensagem.trim()).text().replace(/[\t\n]+/g,'')}%0a` + 
        acessorios.filter(function (item) {
            return item.padrao;
        }).map(function(acessorio) {
            return `✔ ${acessorio.descricao.trim()} - *${acessorio.preco_venda}*%0a`;
        }).join(''),   
        '||V_acessorios||': acessorios.filter(function (item) {
            return !item.padrao;
        }).map(function(acessorio) {
            return `✔ ${acessorio.descricao.trim()} - *${acessorio.preco_venda}*%0a`;
        }).join(''),   
        '||V_marca||': veiculo.marca,
        '||V_modelo||': veiculo.modelo,
        '||V_ano_modelo||': veiculo.ano_modelo,
        '||V_valor_fipe||': veiculo.valor,
        '||V_tipo||': veiculo.descricao_tipo_veiculo,
        '||V_placa||': veiculo.placa,
        '||V_chassi||': veiculo.chassi,
        '||V_combustivel||': veiculo.combustivel,
        '||V_franquia||': valorFranquia,
        '||V_acessorios_total||': numberFormat(valorAcessorio, 1),
        '||V_total||': numberFormat(valorReal, 1)
      });
    }  
  }  
  return dicionario;
}

function convertTextWhatsApp(msg) {
  const dicionario =  {
    ":)" : {
      "regex": ":\\\)",
      "simbolo": "😊"
    }, "(s)": {
      "regex": "\\\(s\\\)",
      "simbolo": "💲"
    }, "(t)": {
      "regex": "\\\(t\\\)",
      "simbolo": "✂"
    }, "(b)" : {
      "regex": "\\\(b\\\)",
      "simbolo": "🏢"
    }, "(p)": {
      "regex": "\\\(p\\\)",
      "simbolo": "✌"
    }, "(*)": {
      "regex": "\\\(\\\*\\\)",
      "simbolo": "✳️"
    }, "(c)": {
      "regex": "\\\(c\\\)",
      "simbolo": "📱"
    }, "(g)": {
      "regex": "\\\(g\\\)",
      "simbolo": "🌐"
    }, "(i001)": {
      "regex": "\\\(i001\\\)",
      "simbolo": "✅"
    }, "(i002)": {
      "regex": "\\\(i002\\\)",
      "simbolo": "▶️"
    }, "(i003)": {
      "regex": "\\\(i003\\\)",
      "simbolo": "😀"
    }, "(i004)": {
      "regex": "\\\(i004\\\)",
      "simbolo": "👆🏼"
    }, "(i005)": {
      "regex": "\\\(i005\\\)",
      "simbolo": "🚙"
    }, "(i006)": {
      "regex": "\\\(i006\\\)",
      "simbolo": "❌"
    }, "(i007)": {
      "regex": "\\\(i007\\\)",
      "simbolo": "🚫"
    }, "(i008)": {
      "regex": "\\\(i008\\\)",
      "simbolo": "⚠️"
    }, "(i009)": {
      "regex": "\\\(i009\\\)",
      "simbolo": "🕙"
    }, "(i010)": {
      "regex": "\\\(i010\\\)",
      "simbolo": "🛠"
    }, "(i011)": {
      "regex": "\\\(i011\\\)",
      "simbolo": "💵"
    }, "(i012)": {
      "regex": "\\\(i012\\\)",
      "simbolo": "👨🏽"
    }, "(i013)": {
      "regex": "\\\(i013\\\)",
      "simbolo": "💰"
    }, "&": {
      "regex": "&",
      "simbolo": "%26"
    }
  };
  const srtDicionario = Object.values(dicionario).map(function(item) {
    return item.regex;
  }).join('|');
  return msg.replace(new RegExp(srtDicionario, 'gi'), function(i) {
    return dicionario[i].simbolo;
  }).replace(/\n/gi, '%0a').replace(/\r/gi, '');
}

export function abrirModalWhatsapp(e) {
  e.preventDefault();
  swal({
    type: 'question',
    title: 'Enviar Proposta via Whatsapp',
    text: 'Você deseja enviar a proposta via whatsapp?',
    showCancelButton: true,
    confirmButtonColor: config.cor_secundaria,
    cancelButtonColor: "#9E9E9E",
    confirmButtonText: 'Sim',  cancelButtonText: 'Não'
  }).then(res => {
    if (res.value) {
      crmService.getModeloWhats(function(res) {
        const dicionario = getDicionario(); 
        const srtDicionario = Object.keys(dicionario).map(function(item) {
          return '\\|\\|'+item.replace(/\|\|/gi, '')+'\\|\\|';
        }).join('|');
        const mensagem = res.data.replace(new RegExp(srtDicionario, 'gi'), function(i) {
          return dicionario[i];
        });
        if (config.cotacao_interna) {
          const usuario =  getUsuario() ? getUsuario() : getLead();
          const msgWhats = convertTextWhatsApp(mensagem);
          const telefone = usuario.celular.replace(/\D/g, '');
          if(isMobile()) {
            window.open(`https://api.whatsapp.com/send?phone=+55${telefone}&text=${msgWhats}`, '_blank');
          } else {
            window.open(`https://web.whatsapp.com/send?phone=+55${telefone}&text=${msgWhats}`, 'Whatsapp', 'height=600,width=600,menubar=no,left=300,' +
                'top=300,location=yes,resizable=no,scrollbars=yes,status=yes,centerscreen=yes');
          }
        } else {
          const usuario = getUsuario();
          const telefone = usuario.celular.replace(/\D/g, '');
          const data = {id_usuario: usuario.id, mensagem, telefone};
          crmService.enviaWhatsApp(data, function(res) {
            console.log(res);
            swal('Sucesso', 'Mensagem enviada com sucesso', 'success');
          }, function(erro){
            console.log(erro);
            swal('Erro', 'Ocorreu um erro para enviar a mensagem pro Whats', 'error');
          });
        }
      });     
    }
  });
}
