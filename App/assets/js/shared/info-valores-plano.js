import { getConfig as configFunc } from '../config/config.js';
import { textToNumber } from './../core/function.js';

const config = configFunc();

export default function geraMenuInfoValores(produto) {
    return `<div class="logo-cotacao d-print">
        <img src="${config.image}" alt="${config.nome}">
    </div>
    <div class="data-cotacao d-print">${config.time || new Date().toLocaleString()}</div>
    <div class="print-titulo">
        <h3 class="text-right">${config.rapido ? 'Cotação '+config.nome : 'Proposta de Adesão'}</h3>
    </div>
    ${config.consultor ? '<div class="nome-consultor">'+config.consultor.nome+'</div>' : ''}
    <h4>Valor do Plano</h4>
    <div class="container-valor-plano"></div>
    <div class="valor-plano-anual"></div>
    ${produto && textToNumber(produto.taxa_adesao) > 0 ? '<div class="container-valor-adesao mb-1">A taxa de adesão no valor de: R$ '+produto.taxa_adesao+'</div>' : ''}
    <div class="nome-produto">${produto ? produto.nome : ''}</div>
    ${tabelaPagamento(produto)}`;

}

function tabelaPagamento(produto) {
    const parcelas = produto.parcelas.map(function(parcela, index) {
        const descricao = parcela.parcela == -2 ? 'Pix Recorrente' : parcela.parcela == -1 ? 'Boleto Recorrente' : (parcela.parcela == 0 ? 'Cartão de Crédito Recorrente' : parcela.parcela+'x (Mensal)');
        return `<div class="row mb-1 py-1 border-bottom${index === 0 ? ' border-top' :''} no-gutters">
        <div class="w-20"><span class="check"></span></div>
        <div class="col-md">${descricao}</div>
        <div class="col-md-4">${parcela && textToNumber(parcela.desconto) === 0 ? '' : parcela.desconto+'% Desconto'}</div>
        <div class="col-md-3">${parcela.valor_unico}</div></div>`;
    });
    if (parcelas.length) {
        return `<h5>Forma de Pagamento:</h5>${parcelas.join('')}`;
    }
    return '';
}
