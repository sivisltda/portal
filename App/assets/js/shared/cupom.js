import {getConfig as configFunc} from './../config/config.js';
import { convertSerializeObject, animaNumero, textToNumber } from "./../core/function.js";

import * as planoService from "./../service/plano.service.js";
import store from "./../store/app.js";

const config = configFunc();
const {getCupom, getConsultor, getProduto, getAcessorios, getIdParcela, getRangeComissao} = store.getters;

export function getValorDesconto() {
    let valorDesconto = 0;    
    const cupom = getCupom();
    if (cupom && cupom.tipo === "1") {
        const valorReal = getValorReal();
        if (cupom.tp_valor === 'P') {
            valorDesconto = valorReal * textToNumber(cupom.valor) / 100;
        } else if (cupom.tp_valor === 'D') {
            valorDesconto = textToNumber(cupom.valor);
        }
        return valorDesconto;
    }
    return null;
}

export function getValorRangeComissao() {
    let valorComissao = 0;    
    const rangeComissao = getRangeComissao();
    if (rangeComissao) {
        const valorReal = getValorReal();
        valorComissao = valorReal * rangeComissao / 100;
        return valorComissao;
    }
    return null;
}

export function getValorRangeDesconto() {
    let valorComissao = 0;    
    const consultor = getConsultor(); 
    const rangeComissao = (parseInt(consultor.max_comis_pri) - getRangeComissao());
    if (rangeComissao > 0) {
        const valorReal = getValorReal();
        valorComissao = valorReal * rangeComissao / 100;
        return valorComissao;
    }
    return null;
}

export function getValorReal() {
    const produto = getProduto();
    const acessorios = getAcessorios();
    const valor_item = acessorios.length ? acessorios.reduce(function (acc, item) {
        acc += textToNumber(item.preco_venda);
        return acc;
    }, 0) : 0;
    const acessoriosFilter = acessorios.filter(function (acessorio) {
        return !acessorio.padrao;
    });
    const condContratual = config.plano_mens_contract && !acessoriosFilter.length;
    const parcela = produto.parcelas.find(function (parcela) {
        return condContratual ? parcela.parcela == 12 : (getIdParcela() ? parcela.id_parcela == getIdParcela() : parcela.parcela > -1);
    }) || produto.parcelas[0];
    const valor_mensal = (parcela ? (parcela.parcela > 1 ? textToNumber(parcela.valor_unico) / textToNumber(parcela.parcela) : textToNumber(parcela.valor_unico)) : 0.00);
    return (valor_item + valor_mensal);
}