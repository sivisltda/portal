import {getConfig as configFunc} from './../config/config.js';
import {numberFormat, textToNumber, valorDesconto} from './../core/function.js';
import {getEtapa} from './../core/route.js';
import * as produtoService from "./../service/produto.service.js";
import store from './../store/app.js';
import {getValorDesconto, getValorRangeComissao, getValorRangeDesconto} from './../shared/cupom.js';

const config = configFunc();
const {getProduto, getVeiculo, getUsuario, getAcessorios, getCupom, getConsultor, getServicosPlano, getIdParcela, getRangeComissao} = store.getters;

export function geraMenu(lista) {
    const produto = getProduto();
    const veiculo = getVeiculo();
    const acessorios = getAcessorios();
    const cupom = getCupom();  
    lista.html(template(produto, veiculo, acessorios, cupom));
    lista.find('.toggle-menu').on('click', onClickToggle);
}

function template(produto, veiculo, acessorios, cupom) {    
    const etapaSelecionada  = getEtapa();
    const consultor = getConsultor();   
    const valorItem = acessorios.length ? acessorios.reduce(function (acc, item) {
        acc += textToNumber(item.preco_venda);
        return acc;
    }, 0) : 0;        
    const acessoriosFilter = acessorios.filter(function (acessorio) {
        return !acessorio.padrao;
    });
    const condContratual = config.plano_mens_contract && !acessoriosFilter.length;
    const parcela = produto.parcelas.find(function (parcela) {
        return condContratual ? parcela.parcela == 12 : (getIdParcela() ? parcela.id_parcela == getIdParcela() : parcela.parcela > -1);
    }) || produto.parcelas[0];
    const valor = parcela.parcela > 1 ? textToNumber(parcela.valor_unico) / textToNumber(parcela.parcela) : textToNumber(parcela.valor_unico);
    
    const rangeComissao = getRangeComissao();        
    const valorDesconto = (config.adm_desconto_range ? getValorRangeDesconto() : getValorDesconto());
    const valorReal = ((valor + valorItem) - valorDesconto);
    const parcelaBoleto = produto.parcelas.find(function (parcela) {
        return ["1", "-1"].includes(parcela.parcela);
    });
    const valorBoleto = (valorDesconto > 0 ? ((valor + valorItem) - valorDesconto) : (parcelaBoleto ? (textToNumber(parcelaBoleto.valor_unico) - textToNumber(parcelaBoleto.valor_desconto)) + valorItem : 0)) * (condContratual ? 12 : 1);
    const valorFranquiaCalc = (produto.cota_defaut_prod_tp === "1" ? produto.cota_defaut_prod : ((textToNumber(veiculo.valor) * produto.cota_defaut_prod) / 100));
    const valorFranquia = (textToNumber(produto.piso_franquia) > valorFranquiaCalc ? produto.piso_franquia : numberFormat(valorFranquiaCalc, 1));
    const valorAdesao = (textToNumber(produto.taxa_adesao) > 0 && textToNumber(produto.taxa_adesao) >= textToNumber(veiculo.valor_desc_adesao) ? (textToNumber(produto.taxa_adesao) - textToNumber(veiculo.valor_desc_adesao)) : 0);
    const aplicado_cupom = (cupom && cupom.hasOwnProperty('cupom'));    
    const totCoberturas = acessorios.length ? acessorios.reduce(function (acc, item) {
        acc += item.padrao ? textToNumber(item.preco_venda) : 0;
        return acc;
    }, 0) : 0;        
    const totAcessorios = acessorios.length ? acessorios.reduce(function (acc, item) {
        acc += !item.padrao ? textToNumber(item.preco_venda) : 0;
        return acc;
    }, 0) : 0;
    
    return `<div class="parte-cima">
    <div class="header-info">
      <div class="logo-cotacao">
        <img src="${config.image}" alt="${config.nome}">
      </div>
      <div class="data-info">
        <div class="data-cotacao d-print">${config.time || new Date().toLocaleString()}</div>
        <div class="print-titulo">
            <h3 class="text-right">${config.rapido ? 'Cotação ' + config.nome : 'Proposta de Adesão'}</h3>
        </div>
      </div>
    </div>
    <a href="javascript:void(0)" class="toggle-menu">
      <span class="float-right d-print-none"><i class="mdi arrow-protecao mdi-chevron-right"></i></span>
      <div class="texto text-center">Valor Médio Mensal:</div>
      <div class="box-valor-plano bg-primaria">
        ${valorDesconto > 0 ? `<div class="texto-valor-adesao texto-riscado">De ${numberFormat((valor + valorItem), 1)} / mês</div>` : ``}              
        <div class="texto-valor-mensalidade">
            <span class="valor">${numberFormat(valorReal, 1)}</span> / mês
        </div>
        <div class="texto-info">${config.adm_msg_cotacao.replace("\n", "<br/>")}
        ${parcelaBoleto ? '<span class="no-print">' + (!condContratual ? '<br/>' : '') + 'Valor para boleto bancário: ' + numberFormat(valorBoleto, 1) + '</span>' : ''}</div>
      </div>
      ${textToNumber(produto.taxa_adesao) > valorAdesao ? `<div class="texto-valor-adesao texto-riscado">${config.adm_msg_taxa_adesao}: ${produto.taxa_adesao}</div>` : ``}      
      ${valorAdesao > 0 ? `<div class="texto-valor-adesao">${config.adm_msg_taxa_adesao}: ${numberFormat(valorAdesao, 1)}</div>` : ``}      
      <div class="texto-valor-adesao mb-1">Cota de Participação: ${valorFranquia}</div>
    </a>
    <div class="container-info-menu">
      <div class="tabela-pagamento d-print">${tabelaPagamento(produto, acessorios, valorDesconto)}</div>
      <div class="container-info-veiculo">
        <div class="info-veiculo">
          ${infoDadosVeiculo(veiculo)}
        </div>
        <div class="box-fipe top-line">
          <p class="info-fipe"><span>${getTipoVeiculo(veiculo)} está avaliado em:</span><span class="valor-fipe">${veiculo.valor}</span></p>
          <p class="data-fipe">de acordo com a tabela FIPE código ${veiculo.codigo_fipe.trim()}.</p>
          <p class="data-fipe">de ${veiculo.mes_referencia.trim()}.</p>
        </div>
      </div>
      <ul class="lista-detalhes-servicos top-line">
        <li>
            <div class="lista-detalhes-item">
                <span class="sinal"></span>
                <span class="nome-servico">Coberturas:</span>
                <div class="preco container-cobertura">${numberFormat(valor + totCoberturas, 1)}</div>
            </div>
            <div class="mensagem-lado">
              ${config.agrupar_plano ? `<div class="d-print-none">${produto.mensagem}</div><div class="d-print">${produto.descricao}</div>` : produto.mensagem}
            </div>
            <ul class="lista-coberturas">
              ${renderListaCoberturas(acessorios)}
            </ul>    
        </li>
        <li class="top-line">
            <div class="lista-detalhes-item">
                <span class="sinal"></span>
                <span class="nome-servico">Adicionais:</span>
                <div class="preco container-adicionais">${numberFormat(totAcessorios, 1)}</div>
            </div>
            <ul class="lista-adicional">
              ${renderListaAcessorios(acessorios)}
            </ul>
        </li>
      </ul>
     ${!config.agrupar_plano ? `<ul class="regulamento top-line">
        <li>Consulte o regulamento e o Manual da Assistência 24H.</li>
      </ul>` : ''} 
      <div class="duvidas bottom-line">
            Duvidas? <a href="tel:+55${config.telefone.replace(/\D/g, '')}" class="num-telefone">${config.telefone}</a>
      </div>          
      ${(config.adm_desconto_range /*&& !aplicado_cupom*/ ? `<div class="bottom-line">
            <div class="msg-cupom">Comissão <b>${rangeComissao}%</b></div>
            <input type="range" class="w-100" id="rangeComissao" min="${(parseInt(consultor.max_comis_pri) - parseInt(consultor.max_desc_pri))}" max="${parseInt(consultor.max_comis_pri)}" step="1" value="${rangeComissao}" ${etapaSelecionada.obj.id === `#cobertura` ? `` : `disabled`}>
            <div class="msg-cupom">Valor aproximado <b>${numberFormat(getValorRangeComissao(), 1)}</b></div>
      </div>` : `<div class="msg-cupom">${aplicado_cupom ? `Cupom aplicado.` : `Tem cupom? Insira abaixo.`}
      ${(!config.rapido && etapaSelecionada.obj.id === `#cobertura` && consultor) ?
      `<a class="link-icon-pendencia" data-toggle="modal" href="#details" data-backdrop="static" data-item="" data-tipo="descontos">
           <i class="mdi mdi-checkbox-marked-circle-outline text-success"></i>
      </a>` : ``}
      </div>
      <div class="form-group row d-print-none mb-1">
            <label class="col-sm-3 col-form-label" for="cupom">Cupom:</label>
            <div class="col-sm-7 col-9">
                ${aplicado_cupom ? `<b>` + (cupom.cupom_texto === "" ? "Desconto de R$ " + cupom.valor : cupom.cupom_texto) + `</b>` : `<input type="text" id="cupom" name="cupom" class="form-control" placeholder="Insira seu cupom"><div class="invalid-feedback"></div>`}
            </div>
            <div class="col-sm-2 col-3 text-center">
                <button id="btn-cupom" class="btn texto-secundaria"><i class="mdi ${aplicado_cupom ? `mdi-delete` : `mdi-check`}"></i></button>
            </div>
      </div>`)}
      ${adicionaObservacao()}
      <div class="my-5 d-flex d-print-none justify-content-around">
      <button id="btn-imprimir" class="btn btn-light btn-acao-cotacao"><i class="mdi mdi-printer"></i> Imprimir</button>
      ${config.mdl_what && (config.cotacao_interna || (!config.rapido && !consultor))
            ? '<button id="btn-whatsapp" class="btn btn-light btn-acao-cotacao"><i class="mdi mdi-whatsapp"></i> Whatsapp</button>'
            : ''}
      </div>
    </div>
  </div>
  <div class="parte-baixo">
    ${adicionaAssinatura(consultor)}
  </div>`;
}

function renderListaCoberturas(acessorios) {
    return acessorios.filter(function (item) {
        return item.padrao;
    }).map(function (acessorio) {
        return `<li>
        <div class="row justify-content-between">
          <div class="col-9">${acessorio.descricao}</div>
          <div class="col-3 valor">${acessorio.preco_venda}</div>
        </div>
      </li>`;
    }).join('');
}

function renderListaAcessorios(acessorios) {
    return acessorios.filter(function (item) {
        return !item.padrao;
    }).map(function (acessorio) {
        return `<li>
        <div class="row justify-content-between">
          <div class="col-9">${acessorio.descricao}</div>
          <div class="col-3 valor">${acessorio.preco_venda}</div>
        </div>
      </li>`;
    }).join('');
}

function adicionaObservacao() {
    if (!config.rapido) {
        return `<div class="observacao">
          <h5>Observações:</h5>
      </div>`;
    }
    return '';
}

function getTipoVeiculo(veiculo) {
    const tiposVeiculos = {
        1: 'O seu carro',
        2: 'A sua moto',
        3: 'O seu caminhão'
    };
    return tiposVeiculos[veiculo.tipo_veiculo] ? tiposVeiculos[veiculo.tipo_veiculo] : 'O seu automóvel';
}

function tabelaPagamento(produto, acessorios = [], descontoMaster) {
    const valorAcessorio = acessorios.reduce(function (acc, item) {
        acc += textToNumber(item.preco_venda);
        return acc;
    }, 0);
    const parcelas = produto.parcelas.map(function (parcela) {
        const desconto = valorDesconto('P', parcela.desconto);
        const valorFinalAcessorio = desconto(valorAcessorio);
        const tipoPagamentos = {
            "-2": "Pix Recorrente",
            "-1": "Boleto Recorrente",
            "0": "Cartão de Crédito",
            "1": "Boleto"
        };
        const valorFinal = textToNumber(parcela.valor_unico) + (valorFinalAcessorio * (parcela.parcela < 1 ? 1 : parcela.parcela));
        const descricao = tipoPagamentos[parcela.parcela] ? tipoPagamentos[parcela.parcela] : parcela.parcela + 'x (Mensal)';
        return `<div class="row align-items-center no-gutters">
      <div class="w-20"><span class="check"></span></div>
      <div class="col-md">${descricao}</div>
      <div class="col-md-4">${parcela && textToNumber(parcela.desconto) === 0 ? '' : parcela.desconto + '% Desconto'}</div>
      <div class="col-md-3">${numberFormat(valorFinal - descontoMaster, 1)}</div></div>`;
    });
    if (parcelas.length) {
        return `<h5>Forma de Pagamento:</h5>${parcelas.join('')}`;
    }
    return '';
}

function infoDadosVeiculo(dados) {
    let items = {placa: 'Placa', marca: 'Marca', modelo: 'Modelo', ano_modelo: 'Ano'};
    if (!config.rapido) {
        items = Object.assign({razao_social: 'Nome', cnpj: 'CPF'}, items);
        dados = Object.assign({}, getUsuario(), dados);
    }
    return Object.keys(items).map(function (i) {
        return `<div class="row">
                  <div class="col-4"><strong>${items[i]}:</strong></div>
                  <div class="col-8">${dados[i]}</div>
              </div>`;
    }).join('');
}

function adicionaAssinatura(consultor) {
    const nome = consultor ? consultor.nome : config.nome + ' Presidente';
    const celular = consultor ? consultor.celular : '';
    if (!config.rapido) {
        const usuario = getUsuario();
        return `<div class="assinatura row">
            <div class="col-5 linha">
                <span class="titulo-assinatura">${config.adm_associado}</span>
                <span class="nome-cliente">${usuario.razao_social}</span>
            </div>
            <div class="col-5 linha">
                <span class="titulo-assinatura">${nome}</span>
                <span class="nome-cliente">${celular}</span>
            </div>
        </div>`;
    }
    return '';
}

function onClickToggle(e) {
    e.preventDefault();
    const t = $(this);
    const container = t.closest('.menu-right');
    container.toggleClass('open');
    const icone = container.find('.arrow-protecao');
    if (icone.hasClass('mdi-chevron-right')) {
        icone.removeClass('mdi-chevron-right').addClass('mdi-chevron-down');
    } else {
        icone.removeClass('mdi-chevron-down').addClass('mdi-chevron-right');
    }
}