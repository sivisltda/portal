import { geraEventos, convertDataIso, convertSerializeObject, convertDataBr  } from './../core/function.js';
import * as loading from './../core/loading.js';
import { createInput } from './../core/form.js';
import {removeFieldRules, addFieldRules, validate } from './../core/validator.js';

import store from './../store/app.js';

import * as adminService from './../service/admin.service.js';
import * as enderecoService from './../service/endereco.service.js';
import * as usuarioForm from './../modules/clube/forms/usuario.js';


const form = $('#frm-login');
const modulos = JSON.parse($('#modulos').val());
const boxLoader = $('.boxLoader');
const { getCidades, getEndereco } = store.getters; 
const valoresInputs = {
    estado: '',
    cep: ''
};

let usuario = {};
let formTemplate = null;

$(document).ready(function() {
    loading.subscribe(function(numero) {
        !!numero ? boxLoader.show() : boxLoader.hide();
    });
    
    adminService.getUsuario(function(data) {
        usuario = data;
        enderecoService.buscarEstados(function() {
            
            montarForm();
            preencherUsuario();
        }, function(erro) {
            
            console.log(erro);
            swal('Erro', 'Ocorreu um erro ao trazer informações do Usuario', 'error');
        });
    }, function(erro) {
        
        console.log(erro);
        swal('Erro', 'Ocorreu um erro ao trazer informações do Usuario', 'error');
    });

    $(document).on('focusin', '[data-toggle=input-mask]', function(e) {
        e.preventDefault();
        const t = $(this);
        t.mask(t.data('mask-format'));
    });

    form.on('submit', function(e) {
        e.preventDefault();
        const t = $(this);
        const validations = validate(formTemplate.rules, t);
        if (validations.length) {
            $.each(validations, function(i, v) {
                t.find(`[name="${v.field}"]`).addClass('is-invalid').siblings('.invalid-feedback').text(v.message);
            });
            swal('Atenção!', 'Informe os campos obrigatórios', 'warning');
            return false;
        }
        const dados = Object.assign({}, convertSerializeObject(t.serialize()));
        if (t.find('[name="regime_tributario"]').length) {
            dados.regime_tributario = t.find('[name="regime_tributario"]').prop('checked') ? "2" : "1";
        }
        if (t.find('[name="nfe_iss"]').length) {
            dados.nfe_iss = t.find('[name="nfe_iss"]').prop('checked') ? "1" : "0";
        }
        dados.data_nascimento = convertDataBr(dados.data_nascimento);
        
        adminService.atualizaUsuario(dados, function(){
            
            swal('Sucesso', 'Dados atualizado com sucesso', 'success');
        }, function(erro) {
            
            console.log(erro);
            swal('Erro', 'Ocorreu um erro ao salvar o usuário', 'error');
        });
    });

});

function montarForm() {
    const events = [
        {
            root: form,
            target: '#cep',
            actions: [
                { event: 'blur', action: buscarCep }
            ]
        },
        {
            root: form,
            target: '#estado',
            actions: [
                { event: 'change', action: selectEstado }
            ]
        }, {
            root: form, 
            target: '#regime_tributario',
            actions: [
                { event: 'change', action: changeRegimeTributario }
            ]
        }
    ];
   formTemplate =  usuario.juridico_tipo === 'J' ? usuarioForm.formDadosEmpresarial(form, events) 
        : usuarioForm.formDados(form, events);
    const classeGroup = formTemplate.type === 'row' ?  '' : 'row';
    const templateFormUsuario = formTemplate.group.map(function(secao, index) {
        const inputs = secao.inputs.filter(function(input) {
            return input.name !== 'procedencia';
        });
        return `<div class="section"><h4 class="mb-2 subtitulo texto-secundaria">${secao.title}</h4>
        <div class="${classeGroup}">${createInput(inputs, formTemplate.type)}</div></div>`;
    }).join('');
    form.html(`${templateFormUsuario}<div class="mt-2 text-right"><button type="submit" class="btn btn-secundaria">Salvar Usuário</button></div>`);
    form.find('#estado').select2();
    geraEventos(formTemplate.events);

}

function buscarCep(e) {
    e.preventDefault();
    const t = $(this);
    const cep = t.val();
    if (cep === "") {
        t.addClass('is-invalid').siblings('.invalid-feedback').text('Cep obrigatório');
        return false;
    }
    if (cep === valoresInputs.cep) {
        return false;
    }
    valoresInputs.cep = cep;
    if (cep.length === 9) {
        enderecoService.popularEnderecoViaCep(cep, function() {
            const endereco = getEndereco();
            t.closest('.section').find('.hide').removeClass('hide');
            ['cep', 'estado', 'cidade', 'bairro'].forEach(function(chave) {
                const input = form.find(`#${chave}`);
                if (input.length && endereco[chave]) {
                    input.val(endereco[chave]);
                     (!['estado', 'cidade', 'cep'].includes(chave)) ? input.attr('readonly', 'readonly') : input.change();
                }
                preencheSelectCidade();
            });
            form.find('[name="numero"]').focus();

        }, function(erro) {
            
            console.log(erro);
            t.val('');
            valoresInputs.cep = '';
            if (erro.status === 2) {
                swal('Erro', 'Ocorreu um erro ao buscar o endereço', 'error');
            } else {
                swal('Erro', 'Ocorreu um erro ao buscar endereço pelo CEP', 'error');
            }
        });
    }
}

function changeRegimeTributario(e) {
    e.preventDefault();
    const t = $(this);
    if (t.prop('checked')) {
        form.find('#inscricao_estadual').attr('readonly', 'readonly').val('');
        removeFieldRules('inscricao_estadual','required');
    } else {
        form.find('#inscricao_estadual').removeAttr('readonly');
        addFieldRules('inscricao_estadual', 'required', 'Inscrição Estadual obrigatório');
    }
}

function selectEstado(e) {
    e.preventDefault();
    const t = $(this);
    const valor = t.val();
    if (valor === "") {
        t.addClass('is-invalid').siblings('.invalid-feedback').text('Estado obrigatório');
    }
    if (valor !== valoresInputs.estado) {
        valoresInputs.estado = valor;
        
        enderecoService.buscarCidadesPorEstado(valor, function() {
            
            preencheSelectCidade();
        }, function (erro) {
            
            console.log(erro);
            swal('Erro', 'Ocorreu um erro ao buscar as cidades', 'error');
        });
    }
}


function preencheSelectCidade() {
    const html = getCidades().map(function(item) {
        return `<option value="${item.cidade_codigo}">${item.cidade_nome}</option>`;
    }).join('');
    form.find('#cidade').html(html).select2();
    if (usuario.cidade) {
        form.find('#cidade').val(usuario.cidade).change();
    }
}

function preencherUsuario() {
    Object.keys(usuario).forEach(function(chave) {
        const input = form.find(`#${chave}`);
        if (input.length) {
            const valor = chave === 'data_nascimento' ? convertDataIso(usuario[chave]) : usuario[chave];
            input.val(valor);
            chave === 'estado' && input.change();
            if (chave === 'regime_tributario' && valor === "2") {
                input.prop('checked', true).change();
            } else if (chave === 'nfe_iss' && valor === "1") {
                input.prop('checked', true).change();
            } 
            if (modulos['sau'] && ['razao_social', 'data_nascimento', 'sexo'].includes(chave)) {
                input.prop('disabled', true);
            }
        }  
    });
}