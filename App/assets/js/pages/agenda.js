import * as helpers from './../core/function.js';
import * as loading from './../core/loading.js';

import * as agendaService from './../service/agenda.service.js';

import { getConfig as configFunc } from './../config/config.js';

  const elIdAgenda          = $('#agenda_id');
  const listAgenda          = $('#list-agenda');
  const listaAgendamento    = $('#list-agendamento');
  const grupo               = $('#grupo');
  const listDias            = $('#list-dias');
  const listHorarios        = $('.calendar-container-hours');
  const modalAgendamento    = $('#modalAgendamento');
  const formAgendamento     = $('#form-agendamento');
  const btnSalvaAgendamento = $('#btn-salva-agendamento');
  const btnCalendarAnt      = $('.btn-calendar-prev');
  const btnCalendarProx     = $('.btn-calendar-next');
  const pagination          = $('#pagination');

  const now                 = new Date();
  const agendamento         = {};

  const configuracaoHoras   = [
      { turno: 'manha', label: 'Manhã', regra: { min: '00:00:00', max: '12:00:00' } },
      { turno: 'tarde', label: 'Tarde', regra: { min: '12:00:00', max: '18:00:00' } },
      { turno: 'noite', label: 'Noite', regra: { min: '18:00:00', max: '23:59:59'} }
  ];

  const configuracaoStatus = [
      { nome: 'cancelado', label: 'Cancelado', classe: 'danger'},
      { nome: 'ativo', label: 'Ativo', classe: 'success'},
      { nome: 'pendente', label: 'Pendente', classe: 'warning'},
      { nome: 'realizado', label: 'Realizado', classe: 'info'}
  ];

  const boxLoader = $('.boxLoader');
  const config = configFunc();

  let data_inicio = '';
  let pagina = 1;
  let numero_contrato = grupo.length ? grupo.val() : config.contrato;

$(document).ready(function() {
    moment.locale('pt-br');
    loading.subscribe(function(numero) {
        !!numero ? boxLoader.show() : boxLoader.hide();
    });
    if(listDias.length && listAgenda.length) {
        verificaPlanoAtivo(function (res) {
            if (res) {
                getListaAgenda(function() {
                    clickAgenda();
                });
            }

        });
    } else {
        getListaAgenda(function() {
            if(listAgenda.find('li').length) {
                clickAgenda();
            }
        });
    }

    $(document).on('change', 'input[name="search_data_status[]"]', function() {
        getListaAgendamentos();
    });

    $('#nova-agenda').on('click', function(e) {
        e.preventDefault();
        window.location.href = "agenda?id="+elIdAgenda.val();
    });

    if (grupo.length) {
        grupo.on('change', function(e) {
            e.preventDefault();
            const t = $(this);
            numero_contrato = t.val();
            getListaAgenda(function() {
                if(listAgenda.find('li').length) {
                    clickAgenda();
                }
            });
        });
    }


    listDias.on('click', '.dia-selecionado', function(e) {
        e.preventDefault();
        const t = $(this);
        listDias.find('li').removeClass('active');
        t.closest('li').addClass('active');
        data_inicio = t.data('date');
        if(listDias.length && listAgenda.length) {
            getListaHorarios();
        }
    });

    listAgenda.on('click', '.seleciona-agenda', function(e) {
        e.preventDefault();
        data_inicio = now.getDate()+'/'+('0'+(now.getMonth()+1)).substr(-2)+'/'+now.getFullYear();
        listAgenda.find('li').removeClass('active');
        const t = $(this);
        t.closest('li').addClass('active');
        elIdAgenda.val(t.data('id'));
        
        if(listDias.length && listAgenda.length) {
            getListaDias(t.data('id'), function () {
                listDias.find('li:first a').click();
            });
        } else {
            getListaAgendamentos();
        }
    });

    listaAgendamento.on('click', '.btn-cancelar', function (e) {
       e.preventDefault();
       const t = $(this);
       swal({
            type: 'warning',
            title: "Cancelar o Agendamento",
            html: `Deseja cancelar o Agendamento?`,
            showCancelButton: true,
            confirmButtonText: "Sim",
            cancelButtonText: "Não",
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#9E9E9E"
       }).then(function(r) {
            if(r.value) {
                cancelaAgendamento(t.data('id'), function() {
                    t.closest('.card').parent().remove();
                });
            }
       });
    });

    btnSalvaAgendamento.on('click', function (e) {
        e.preventDefault();
        formAgendamento.submit();
    });

    modalAgendamento.on('show.bs.modal', function(e) {
        const button = $(e.relatedTarget);
        agendamento.data_inicio = data_inicio;
        agendamento.data_fim = data_inicio;
        agendamento.hora_inicio = button.data('hora-inicial');
        agendamento.hora_fim = button.data('hora-final');
        agendamento.id_agenda = elIdAgenda.val();
        agendamento.id_agendamento = '';
        formAgendamento.find(':input').val('');
        $('#data_inicio').val(agendamento.data_inicio);
        $('#hora_inicio').val(agendamento.hora_inicio);
        $('#data_fim').val(agendamento.data_fim);
        $('#hora_fim').val(agendamento.hora_fim);
    });

    formAgendamento.on('submit', function(e) {
        e.preventDefault();
        const form = $(this);
        const assunto = $("#assunto");
        assunto.removeClass('is-invalid').next().text('');
        const valores = helpers.convertSerializeObject(form.serialize());
        if(valores.assunto) {
            Object.assign(agendamento, valores);
            salvarAgendamento(function() {                
            });
        }else {
            assunto.addClass('is-invalid').next().text('Assunto é um campo obrigatório');
        }
    });

    btnCalendarProx.on('click', function(e) {
        e.preventDefault();
        if(listDias.length) {
            const elemento = listDias[0];
            if(elemento) {
                const tamanho = elemento.offsetWidth;
                const tamanhoScroll = elemento.scrollWidth;
                const scrollLeft = elemento.scrollLeft;
                if((scrollLeft + tamanho) < tamanhoScroll) {
                    elemento.scrollTo(scrollLeft + tamanho, 0);
                }
            }
        } else {
            const proxDia = moment(helpers.convertDataIso(data_inicio)).add(1, 'months');
            data_inicio = proxDia.format('DD/MM/YYYY');            
            getListaAgendamentos();
        }
    });


    btnCalendarAnt.on('click', function(e) {
        e.preventDefault();
        if(listDias.length) {
            const elemento = listDias[0];
            if(elemento) {
                const tamanho = elemento.offsetWidth;
                const scrollLeft = elemento.scrollLeft;
                if(scrollLeft > 0) {
                    elemento.scrollTo(scrollLeft - tamanho, 0);
                }
            }
        }else {
            const antDia = moment(helpers.convertDataIso(data_inicio)).subtract(1, 'months');
            data_inicio = antDia.format('DD/MM/YYYY');            
            getListaAgendamentos();
        }

    });

    pagination.on('click', '.page-prev', function(e) {
        e.preventDefault();
        pagina-=1;
        getListaAgendamentos();
    });

    pagination.on('click', '.page-next', function(e) {
        e.preventDefault();
        pagina+=1;
        getListaAgendamentos();
    });

    pagination.on('click', '.page-number', function(e) {
        e.preventDefault();
        pagina = $(this).data('page');
        getListaAgendamentos();
    });

});

function cancelaAgendamento(id_agendamento, callback) {    
    agendaService.cancelaAgendamento({ id_agendamento, numero_contrato }, function (res) {
        swal('Cancelado', 'Cancelamento do agendamento feito com sucesso!', 'success');
        callback(res);
    }, function (erro) {
        console.error(erro);
        const msg = erro.responseJSON.erro ? erro.responseJSON.erro : 
        'Erro, Ocorreu um erro ao cancelar o agendamento';
        swal('Erro', msg, 'error');
    });
}

function getListaAgenda(callback) {
    agendaService.getListaAgenda(numero_contrato, function(res) {       
        const html = res.map(function(e) {
            return `<li class="list-group-item">
            <a class="seleciona-agenda" href="javascript:void(0)" data-id="${e.id_agenda}">${e.descricao_agenda}</a>
        </li>`
        }).join('');
        listAgenda.html(html);
        callback();
    }, function(erro){
        console.error(erro);
        swal('Erro', 'Ocorreu um erro ao listar a agenda', 'error');        
    });
}

function getListaDias(id_agenda, callback) {
    agendaService.getListaDias({ id_agenda, data_inicio, numero_contrato },
        function(res) {
            let template = '';
            res.forEach(function(m){
                m.days.forEach(function(d) {
                  template+=`<li><a class="dia-selecionado" href="#" data-date="${d.date}">
                        <span class="day-week">${d.day.substr(0,3)}</span>
                        <span class="day-month">${d.number}/${m.number}</span>
                    </a>
                </li>`;
                });
            });
            listDias.html(template);
            callback();
    }, function(erro) {
        console.error(erro);
        swal('Erro', 'Ocorreu um erro ao listar os dias', 'error');        
    });

}

function getListaHorarios() {
    agendaService.getListaHorarios({ id_agenda: elIdAgenda.val(), data_inicio, numero_contrato }, function(res) {
        const turnos = transformaListaHorarios(res);
        const templateTurno = Object.keys(turnos).map(function(index) {
            const turno = turnos[index];
            let template = `<li class="calendar-hours turno"><h6>${turno.nome}</h6><ul class="list-hours" id="${index}">`;
            if(turno.data.length) {
                template+= turno.data.map(function (h) {
                    return `<li><button class="btn btn-calendar-hour" data-toggle="modal" data-target="#modalAgendamento" 
                    data-hora-inicial="${h.hora_ini}" data-hora-final="${h.hora_fim}">${h.hora_ini}</button></li>`;
                }).join('');
            }else {
                template+=`<li>Não há horários disponíveis nesse período.</li>`;
            }
            template+='</ul></li>';
            return template;
        }).join('');
        listHorarios.html(templateTurno);
    }, function(erro) {
        console.error(erro);
        swal('Erro', 'Ocorreu um erro ao listar os horários', 'error');
    });
}


function salvarAgendamento(callback) {
    agendaService.salvarAgendamento(Object.assign(agendamento, { numero_contrato }), function (res) {
        agendamento.id_agendamento = res.agendamento;
        swal('Sucesso', 'Agendamento salvo com successo!' + (res.link.length > 0 ? '<br><b>Redirecionando para tela de Pagamento</b>' : ''), 'success').then(function (r) {
            window.location.href = (res.link.length > 0 ? res.link[0].link_link : 'minha-agenda');
        });
        callback();
    }, function(erro) {
        if (erro.status === 403) {
            swal('Erro', erro.responseJSON.erro, 'error');
        } else {
            swal('Erro', 'Erro ao salvar o agendamento', 'error');
        }
    });
}

function getListaAgendamentos() {    
    const checkBoxesStatus = [... document.querySelectorAll('input[name="search_data_status[]"]')];
    const status = checkBoxesStatus.filter(c => c.checked).map(c => c.value).join(',');
    agendaService.getListaAgendamentos({ pagina, id_agenda: elIdAgenda.val(), data_inicio, status, numero_contrato }, function(res) {
        $('#mes-agenda').html(moment(helpers.convertDataIso(data_inicio)).format('MMMM / YYYY'));
        $('#quant_ativo_data').text(res.total.ativo);
        $('#quant_inativo_data').text(res.total.inativo);
        pagination.html(paginacao(res.pagination));
        if (res.aaData.length) {
            listaAgendamento.html(res.aaData.map(function(evento){
                return templateAgendamento(evento);
            }).join(''));
        } else {
            listaAgendamento.html(`<li><div class="empty">Nenhum agendamento encontrado!</div></li>`);
        }
    }, function(erro) {
        console.log(erro.responseText);
        swal('Erro', 'Erro ao listar o Agendamento!', 'error');
    });
}

function templateAgendamento(evento) {
    const data_inicio_moment = moment(evento.data_inicio, "DD/MM/YYYY hh:mm:ss");
    const data_fim_moment    = moment(evento.data_fim, "DD/MM/YYYY hh:mm:ss");
    return `
    <li>
        <div class="card">
            <div class="card-header bg-black text-white font-weight-bold">
                <div class="d-flex justify-content-between">
                    <span>${data_inicio_moment.format('LL')}</span>
                    <span class="text-capitalize">${data_inicio_moment.format('dddd')}</span>
                </div>
            </div>
            <ul class="list-group list-group-flush ">
                <li class="list-group-item ">
                    <div class="row">
                        <div class="col-12 col-lg-3 mb-2">${data_inicio_moment.format('HH:mm')+' - '+data_fim_moment.format('HH:mm')}</div>
                        <div class="col-3 col-lg-2 mb-2"><span class="mdi mdi-circle text-${evento.inativo ? 'danger' : 'info'}"> ${evento.status}</span></div>
                        <div class="col-9 col-lg-7 mb-2">${evento.assunto}</div>
                        <div class="col-6 mb-2">${evento.obs ? 'Obs.: '+evento.obs : ''}</div>
                        <div class="col-6 mb-2">${evento.link.length > 0 ? '<a href="' + evento.link[0].link_link + '" target="_blank">Link para pagamento ' + (evento.link[0].id_venda ? '(PAGO)' : '') + '</a>' : ''}</div>
                        <div class="col-12 d-flex justify-content-end mt-3">
                            ${helpers.datasMaior(evento.data_inicio, new Date().toLocaleString()) && !evento.inativo && 
                            !(evento.link.length > 0 && evento.link[0].id_venda) ? 
                            '<button class="btn btn-warning btn-cancelar text-white" data-id="'+evento.id_agendamento+'">Cancelar</button>' : ''}
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </li>
    `;
}

function transformaListaHorarios(horarios) {
    let i = 0;
    return configuracaoHoras.reduce(function(acc, t){
        acc[i+t.turno] = {
          nome: t.label,
          data: horarios.filter(function(item) {
              return helpers.datasMaior(data_inicio+' '+item.hora_ini+':00', data_inicio+' '+t.regra.min)
                  && helpers.datasMenor(data_inicio+' '+item.hora_ini+':00', data_inicio+' '+t.regra.max)
                  && item.inativo === 0;
          })
        };
        i++;
        return acc;
    }, {});
}

function paginacao(pagination) {
    if(pagination.next || pagination.prev) {
        let template = `<ul class="pagination">
            <li class="page-item ${pagination.prev ? '': 'disabled'}"><a class="page-link page-prev" href="javascript:void(0)" tabindex="-1" aria-disabled="${pagination.prev}">Anterior</a></li>`;
        for(let i =1; i <= pagination.qtd_pages; i++) {
            template+=`<li class="page-item ${pagination.current === i ? 'active' : ''}"><a class="page-link page-number" href="javascript:void(0)" data-page="${i}">${i}</a></li>`;
        }
        template+=` <li class="page-item ${pagination.next ? '': 'disabled'}"><a class="page-link page-next" href="javascript:void(0)" tabindex="-1" aria-disabled="${pagination.next}">Próximo</a></li></ul>`;
        return template;
    }
}

function verificaPlanoAtivo(callback) {
    
    agendaService.verificaPlanoAtivo(function(res) {
        callback(true);
    }, function(erro) {
        console.error(erro);
        
        if(erro.status === 403) {
            swal('Atenção!', erro.responseJSON.erro, 'warning').then(function (res) {
               window.location.href = 'minha-agenda';
            });
        }else {
            swal('Erro','Ocorreu um erro ao verificar o usuário', 'error');
        }
        callback(false);
    });
}

function geraStatus(inativo, dia_inicio, dia_fim) {
    /*const status = inativo ? 'cancelado' : (helpers.datasMaior(data_inicio, new Date().toLocaleString())
        ? 'realizado' : (helpers.data))*/
}

function clickAgenda() {
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('id')) {
        const item_agenda = Array.from(listAgenda.find('li a'))
            .find(function(item) {
               return item.dataset.id == urlParams.get('id');
            });
            if (item_agenda) {
                item_agenda.click()
            } else {
                listAgenda.find('li:first a').click();
            }
    } else {
        listAgenda.find('li:first a').click();
    }

}