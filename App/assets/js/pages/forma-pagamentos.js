import * as helpers from './../core/function.js';
import * as loading from './../core/loading.js';
import { createInput } from './../core/form.js';
import { validate } from './../core/validator.js';

import * as mensalidadeService from './../service/mensalidade.service.js';
import * as pagamentoService from './../service/pagamento.service.js';
import * as formPagamento from './../forms/pagamento.js';
import { getConfig as configFunc } from './../config/config.js';
import store from './../store/dashboard.js';

let campoPlano           = null;
let campoTipoVeiculo     = null;
let terminal_vendas      = null;
let adm_msg_termos_whats = null;
const tipoPlano          = $('#filtro-plano');
const boxInfoPlano       = $('#info-plano');
const boxCarrousel       = $('.carrousel');
const modalDetalhes      = $('#details');
const modalPagamento     = $('#modal-pagamento');
const boxLoader          = $('.boxLoader');
const bloqueio           = Number.parseInt($('#bloqueio').val());
const config             = configFunc();

const configPagamento = [
    {
        icon: 'boleto.svg',
        bandeira: 'boleto',
        target: ['BOL']
    }, {
        icon: 'credit-card.svg',
        target:['CRE', 'DEB', 'DCC']
    }, {
        icon: 'money.svg',
        bandeira: 'dinheiro',
        target: ['DIN', 'CRT', 'TRA', 'DEF', 'DEC', 'CHE', 'DOC', 'TED', 'ANUAL']
    }
];

const { setPlano } = store.setters;
const { getPlano, getPlanos, getMensalidades } = store.getters;

$(document).ready(function () {
    loading.subscribe(function(numero) {
        !!numero ? boxLoader.show() : boxLoader.hide();
    });
    mensalidadeService.inicioFormaPagamento(function(res) {
        if(res.data) {
            if(res.tipo === 'multiplo') {
                tipoPlano.html(templateFiltro(res));
                campoTipoVeiculo = tipoPlano.find('#tipo_veiculo').select2();
                campoPlano       = tipoPlano.find('#plano');
                campoTipoVeiculo.on('change', changeTipoVeiculo);
                campoPlano.on('change', changePlano);
                carregaListaPlanos();
            } else {
                setaInfoPlano(res.data);
            }
            terminal_vendas = res.terminal;
            adm_msg_termos_whats = res.link_whats;
        } 
    }, function(erro) {
        console.log(erro);
       swal('Erro', 'Ocorreu um erro ao listar os tipos de planos', 'error');
    });
    $(document).on('click', '.acao-pagamento', processoPagamento);
    $(document).on('click', '.acao-tp-pagamento', processoAltPagamento);
    modalDetalhes.on('shown.bs.modal', openModalDetails);
    modalPagamento.on('show.bs.modal', openModalPagamento);
    $(document).on('focusin', '[data-toggle=input-mask]', function(e) {
        e.preventDefault();
        const t = $(this);
        t.mask(t.data('mask-format'));
    });
});

function carregaListaPlanos(tipo_veiculo = null) {
    mensalidadeService.getListaPlanos(tipo_veiculo, function (res) {
        if(res.length > 0) {
            const options = res.map(function(e) {
                return `<option value="${e.id}">
                ${e.id_veiculo ? e.nome+' ['+e.placa+'] '+e.modelo : e.nome}
                </option>`;
            }).join('');
            campoPlano.html(options).select2();
            if(res[0].tipo_veiculo)  campoTipoVeiculo.val(res[0].tipo_veiculo);
            helpers.selecionaOption(campoPlano, campoPlano.val());
        } else {
            campoPlano.html('<option value="">Nenhum plano encontrado</option>');
            boxInfoPlano.html('');
            //boxCarrousel.slick && boxCarrousel.slick('unslick');
            boxCarrousel.html('<h3>Nenhum Plano foi encontrado para esse tipo de Veículo</h3>');
        }
    }, function (erro) {
        console.log(erro);
        swal('Erro', 'Ocorreu um erro ao listar os planos', 'error');
    });
}

function changeTipoVeiculo() {
    const t = $(this);    
    carregaListaPlanos(t.val());
}

function processoAltPagamento(e) {
    e.preventDefault();
    const t = $(this);
    const parcela   = t.data('parcela');
    const idParcela = t.data('id-parcela');
    const mensagem  = parcela == -1 || parcela == 1 ? 'Boleto' : 'Mensal';
    if (parcela == -1  || parcela == 1) {
        swal({
            title: 'Atenção',
            text: `Você deseja alterar seu plano para tipo ${mensagem}?`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não'
        }).then(function(res) {
            if (res.value) {
                alterarFormaPagto({ id_parcela: idParcela });
            }
        });
    }
}

function processoPagamento(e) {
    e.preventDefault();
    const t = $(this);
    const tipo = t.data('tipo');
    const id_parcela = t.data('id-parcela');
    const atualiza = t.data('atualiza');
    const id_boleto = t.data('id-boleto');
    const url_boleto = t.data('url_boleto');
    const data = {id_mens: t.data('id'), id_parcela, nao_atualiza: atualiza ? null : true};
    const mensagem = tipo == 'boleto' ?
    (id_boleto ? 'Você deseja imprimir a segunda via desse boleto?' : 'Você deseja gerar o boleto dessa mensalidade?')  :
    'Você deseja fazer reprocessar o pagamento no cartão cadastrado?';
    swal({
        title: 'Atenção',
        text: mensagem,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sim',
        cancelButtonText: 'Não'
    }).then(function(res) {
        if (res.value) {
            if (tipo === 'boleto') {
                //gerarBoleto(data);
                window.open(url_boleto,'_blank');                
            } else {
                processarCartao(data, tipo);
            }       
        }
    });
   
}

function processarCartao(data, tipo) {
    pagamentoService.processarPagamento(data, function() {
        swal('Sucesso!', 'Transação paga com sucesso', 'success').then(function() {
            setaDadosPlano(getPlano());
            window.location.reload();
        });
    }, function(erro, mensagem) {
        if (tipo == 'dcc') {
            swal('Erro!', 'Ocorreu um erro ao processar o pagamento', 'error');
        } else if(erro === 1) {
            swal({
                title: 'Não foi possivel efetuar a compra com o seu cartão',
                text: "Gostaria de alterar seu cartão?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Sim, vou mudar o cartão!',
                cancelButtonText: 'Não, tentarei mais tarde!',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#9E9E9E'
            }).then((result) => {
                if (result.value) {
                    t.find('[name="cartao_numero"], [name="data_expiracao"], [name="cvv"], [name="nome_cartao"], [name="cartao_bandeira"]').val('');
                }
            });
        } else if(erro === 2) {
            swal({type: 'error', title: 'Erro', text: mensagem});
        }
    }, 1);
}

/*function gerarBoleto(data) {
    pagamentoService.processarPagamento(data, function(res) {
        window.open(res.boleto.url);
        swal('Sucesso!', 'Documento gerado com sucesso!', 'success');        
    }, function(erro, mensagem) {
        if (erro === 1) {
            swal({
                title: 'Erro',
                text: "Não foi possivel gerar o seu documento, tente novamente mais tarde",
                type: 'error'
            });
        } else if(erro === 2) {
            swal({type: 'error', title: 'Erro', text: mensagem });
        }
    }, 1);    
}*/ 

function changePlano() {
    const t = $(this);    
    const id_plano = t.val();
    const plano = getPlanos().find(function(p){
        return p.id === id_plano;
    });
    setaInfoPlano(plano);
}

function setaInfoPlano(plano) {
    setPlano(plano);
    setaDadosPlano(plano);
}

function setaDadosPlano(plano) {
    boxInfoPlano.html(templateInfoPlano(plano));
    montarCarrouselMensalidades(plano.id);
    criarBotaoAlterar(plano, bloqueio);        
}

function criarBotaoAlterar(plano, bloqueio) {
    const botaoAlterar = $('#container-btn-alterar').html('');
    let btnAlterarOpcoes = '';
    if (plano.parcela == 0) {
        btnAlterarOpcoes+= `<a class="dropdown-item" 
        href="#modal-pagamento" data-toggle="modal"
        data-backdrop="static" data-tipo="dcc-cartao" 
        data-cartao='${JSON.stringify(plano.cartao)}'
        data-parcela=${plano.parcela} data-id-parcela="${plano.id_parcela}">
        Alterar o Cartão
        </a>`;
    }
    if (plano.parcelas.length && !bloqueio) {
        btnAlterarOpcoes += plano.parcelas.filter(function(p) {
            return p.parcela < 1;
        }).map(function(p) {
            if (p.parcela == 0) {
                return `<a class="dropdown-item" 
                    href="#modal-pagamento" data-toggle="modal" 
                    data-backdrop="static" data-tipo="alterar" 
                    data-parcela=${p.parcela} data-id-parcela="${p.id_parcela}">
                    Alterar para DCC
                    </a>`;
            } else if (p.parcela == -1) {
                return `<a class="dropdown-item acao-tp-pagamento" 
                    href="javascript:void(0)" data-tipo="boleto" 
                    data-parcela=${p.parcela} data-id-parcela="${p.id_parcela}">
                    Alterar para Boleto Recorrente
                    </a>`;
            } 
            return '';
        }).join('');        
        const parcelasMensais = plano.parcelas.filter(function(p) { return p.parcela == 1});
        if (parcelasMensais.length) {
            btnAlterarOpcoes+= `<a class="dropdown-item acao-tp-pagamento" 
            href="javascript:void(0)" data-tipo="mensal" 
            data-parcela=${parcelasMensais[0].parcela} data-id-parcela="${parcelasMensais[0].id_parcela}">
            Alterar para Mensal
            </a>`;
        }
    }
    if (btnAlterarOpcoes.length) {
        botaoAlterar.html(`<div class="btn-group" role="group">
        <button id="btn-alt-pagamento" type="button"
        class="btn btn-secundaria dropdown-toggle" data-toggle="dropdown" 
        aria-haspopup="true" aria-expanded="false">Editar</button>
        <div class="dropdown-menu" aria-labelledby="btn-alt-pagamento">${btnAlterarOpcoes}</div>`);
    }    
}

function alterarFormaPagto(data) {
    mensalidadeService.alterarFormaPagto(data, function() {        
        swal('Sucesso', 'Forma de Pagamento alterado com sucesso!', 'success')
            .then(function() {
                setaDadosPlano(getPlano());
            });
    }, function(erro) {
        console.log(erro);
        swal('Erro', 'Ocorreu um erro ao alterar a forma de pagamento', 'error');
    });
}

function montarCarrouselMensalidades(id_plano) {
    mensalidadeService.getListaMensalidades(id_plano, function (res) {        
        if(res.length < 4){
            res = res.sort(function(a, b) {
                return a.num_mens - b.num_mens;
            });
        }
        if (res.length > 1) {
            boxCarrousel.addClass('container-carrousel');
        } else if (boxCarrousel.hasClass('container-carrousel')) {
            boxCarrousel.removeClass('container-carrousel');
        }
        const goTo = res.findIndex(function(item) { return !item.dt_pagamento}); 
        const items = res.map(function (m) {
            return templateMensalidade(m);
        }).join('');
        ativarCarrousel(items, goTo);
    }, function (erro) {
        console.log(erro);
        swal('Erro', 'Ocorreu um erro ao buscar as mensalidades', 'error');
    });
}

function templateModalPagamento(tipo, id, cartao) {
    const form = formPagamento.formDados([], id);
    const templateTrocaCartao = cartao ? `<h5 class="texto-secundaria mt-2">Cartão Atual</h5>
    <div class="d-flex align-items-center"><span class="bandeira ${cartao.bandeira}"></span>
    <span class="numero-credito">${cartao.legenda_cartao}</span></div>
    <h5 class="texto-secundaria my-2">Novo Cartão</h5>` : '';
    return `<h4 class="texto-secundaria text-center">Cartão de Crédito</h4> 
    ${tipo == 'dcc-cartao' ? templateTrocaCartao : ''}
    <form class="form-cartao${id}" novalidate>
    <div class="card-wrapper${id}"></div>
    <div class="row mt-3">${createInput(form.inputs, form.type)}</div>
    <div class="row"><div class="col-sm-12 mt-2">
    <button class="btn btn-secundaria text-uppercase btn-block btn-pagar">${tipo == 'cartao' ? 'Pagar agora' : 'Alterar'}</button>
    </div></div></form>`;
}

function templateInfoPlano(plano) {
    let info = `<div class="row mb-1"><div class="col-sm-3"><span class="font-weight-bold">Plano:</span> ${plano.nome}</div>
                <div class="col-sm-3"><span class="font-weight-bold">Status:</span> ${plano.status}</div>
                <div class="col-sm-3"><span class="font-weight-bold">Valor:</span> ${plano.valor_plano}</div>
                <div class="col-sm-3"><span class="font-weight-bold">Desde:</span> ${plano.dt_cadastro}</div></div>`;
    if(plano.id_veiculo) {
        info += `<div class="row mb-1"><div class="col-sm-3"><span class="font-weight-bold">Veiculo:</span> ${plano.placa}</div>
                 <div class="col-sm-3"><span class="font-weight-bold">Marca:</span> ${plano.marca}</div>
                 <div class="col-sm-6"><span class="font-weight-bold">Modelo:</span> ${plano.modelo}</div></div>`;
    }
    return info;
}

function templateMensalidade(item) {   
    const tipoPagamento = configPagamento.find(function (tp) {
        return tp.target.includes(item.tipo_pag_abrev);
    });
    const infoPagamento = function() {
        let html = `<h4 class="font-weight text-center">${item.descricao_pagamento}</h4>`;
        html+= '<div class="d-flex align-items-center">'+(item.numero_cartao ? 
        `<span class="bandeira ${(tipoPagamento && tipoPagamento.bandeira) ? tipoPagamento.bandeira : item.bandeira}"></span>
         <span class="numero-credito">${item.numero_cartao}</span>`
         : `<span class="bandeira boleto"></span><span class="numero-credito">${item.tipo_pag_abrev}</span>`)+'</div>';
         return html;
    };
   
    const condicaoBoleto = (parseInt(item.id_boleto) && parseInt(item.exp_remessa)) || (!parseInt(item.id_boleto) && item.parcela == -1);        
    const getValor = function(valor) {
        const valorReal = valor ? valor : 'R$ 0,00';
        const [reais, centavos] = valorReal.replace('R$ ', '').split(',');
        return helpers.templateNumero(reais, centavos);
    };
    
    console.log('aaaa');
    console.log(terminal_vendas);
    
    const itens_menu = ((item.parcela != 0  || (item.parcela == 0 && item.status == 'Vencido' && item.id_parcela_boleto)) && condicaoBoleto && item.id_boleto ?     
    '<a class="dropdown-item acao-pagamento" data-tipo="boleto" data-id="' + item.id_mens + '" '
    +'data-id-parcela="' + (item.parcela > 0 || item.parcela == -1 || item.parcela == 1 ? item.id_parcela : item.id_parcela_boleto) + '" '
    +'data-atualiza="' + (item.parcela != 0) + '" '  
    +'data-url_boleto="' + item.url_boleto + '" '  
    +'data-id-boleto="' + item.id_boleto + '" '
    +'href="javacrispt:void(0)">' + (item.id_boleto ? '2ª via de boleto' : 'Gerar Boleto') + '</a>' 
    : (adm_msg_termos_whats.length > 0 ? '<a class="dropdown-item" data-tipo="link" target="_blank" href="' + adm_msg_termos_whats + '"><b>2ª Via de Boleto</b></a>' : '')) + 
    ((item.status == 'Pendente' && terminal_vendas == '1') ? 
    '<a class="dropdown-item" data-tipo="cartao" data-id="'+item.id_mens+'" '
    +'data-toggle="modal" data-backdrop="static" '
    +'data-id-parcela="'+(item.parcela > 0 ? item.id_parcela : item.id_parcela_cartao)+'" '
    +'data-atualiza="'+(item.parcela > 0)+'" '
    +'href="#modal-pagamento">Pagar no Cartão</a>'
    : '');
    
    return `<div class="item"><div class="card m-0 box-fp border-left h-100"><div class="card-body text-center">
            <h3 class="mb-3"><span>${item.label_mens}</span></h3>
            <div class="row h-100">
                <div class="col-md-5 mb-2 d-flex justify-content-center">
                    <img src="${config.site}assets/images/${tipoPagamento ? tipoPagamento.icon : 'money.svg'}" class="img-fluid img-icone-pagamento">
                </div>
                <div class="col-md-7 mb-2">
                    <div class="valor-pagar texto-primaria">${getValor(item.valor_transacao)}</div>
                    <div class="valor-pago texto-secundaria mb-2">${getValor(item.valor_pago)}</div>
                    <span class="info-status ${helpers.slug(item.status)}">${item.status}</span>
                </div>
                <div class="mb-2 col-md-5 flex-column align-items-center d-flex">${infoPagamento()}</div>
                <div class="mb-2 col-md-7 flex-column align-items-center justify-content-center d-flex">
                    <div class="btn-group" role="group">
                        <button id="btn-acao-pagamento" type="button" ${item.dt_pagamento.length > 0 || itens_menu.length === 0 ? 'disabled' : ''} 
                            class="btn btn-secundaria dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Pagar agora
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btn-acao-pagamento">${itens_menu}</div>
                    </div>
                </div>    
                <div class="col-12 mt-2">
                    <div class="box-info-mensalidade">
                        <div class="info-mensalidade">
                            Data de Vencimento: <span class="dia">${item.dt_inicio_mens}</span>
                        </div>
                        <div class="info-mensalidade">
                            Data de Pagamento: <span class="dia data-pagamento">${item.dt_pagamento ? item.dt_pagamento : '--------' }</span>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-6 mt-2">
                            <button class="btn bg-secundaria text-white btn-block" data-toggle="modal" data-target="#details" 
                            data-id="${item.id_mens}" data-title="Detalhes da Mensalidade" data-type="items">
                                <i class="mdi mdi-eye"></i> Detalhes
                            </button>
                        </div>
                          <div class="col-md-6 mt-2">
                            <button class="btn bg-primaria text-white btn-block" data-toggle="modal" data-target="#details" 
                            ${item.transacoes.length === 0 ? 'disabled' : ''}
                            data-id="${item.id_mens}" data-title="Transações" data-type="transacoes">
                                <i class="mdi mdi-eye"></i> Transações
                            </button>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>`;
}

function ativarCarrousel(items, goTo) {
    if ( boxCarrousel.hasClass('slick-initialized')) {
        boxCarrousel.slick('destroy');
    }
    boxCarrousel.html(items);
    boxCarrousel.slick({
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true,
        prevArrow: '<button type="button" class="nav-carrousel prev"><i class="mdi mdi-chevron-left"></i></button>',
        nextArrow: '<button type="button" class="nav-carrousel next"><i class="mdi mdi-chevron-right"></i></button>',
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 900,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    boxCarrousel.slick('slickGoTo', goTo === -1 ? 0 : goTo);
}

function criarLinhasDetalhes(item) {
    let html = `<div class="row mb-3 item-detalhes align-items-start ${item.classe || ''}">`;
    html+= `<div class="col-2"><img src="${config.site}assets/images/${item.icone}" class="img-fluid">
            </div><div class="col-10"><div class="row">`;
    if (item.data && item.status) {
        html+= `<div class="col-8 mb-1"><span class="font-weight-bold">${item.data}</span></div>
                <div class="col-4 mb-1"><span class="font-weight-bold">${item.status}</span></div>`;
    }
    html+=`<div class="col-8"><span class="categoria-detalhes d-block">${item.tipo}</span>
        <div class="info-pagto-detalhes">${item.bandeira ? '<span class="bandeira '+item.bandeira+'"></span>': ''}
        <span class="descricao-detalhes">${item.descricao}</span></div></div>
    <div class="col-4 pt-3"><span class="valor-detalhes">${item.valor}</span></div></div></div></div>`;
    return html;
}

function openModalPagamento(e) {
    const t = $(e.relatedTarget);
    const tipo = t.data('tipo');
    const msg = tipo == 'cartao' ? 'Pagamento via Cartão' : ( tipo === 'dcc-cartao' ? 'Alterar o cartão' :'Alterar a Forma de Pagamento');
    const cartao = t.data('cartao');
    modalPagamento.find('.modal-title').text(msg);
    modalPagamento.find('.modal-body').html(templateModalPagamento(tipo, '', cartao));
    const form = modalPagamento.find('form');
    helpers.montaCartaoCredito({
        form: '.form-cartao',
        container: '.card-wrapper',
        name: 'input.nome_cartao',
        number: 'input.cartao_numero',
        expiry: 'input.data_expiracao',
        cvc: 'input.cvv',
        submit: '.btn-pagar'
    });
    modalPagamento.find('#cartao_bandeira').select2({templateResult: formPagamento.formatState, templateSelection: formPagamento.formatState});
    formPagamento.actionState('#cartao_numero', '#cartao_bandeira');
    form.on('submit', function(e) {
        e.preventDefault();
        const formDados = formPagamento.formDados([], '');
        form.find(':input').removeClass('is-invalid').next('.invalid-feedback').text('');
        const validations  = validate(formDados.rules, form);
        if (validations.length) {
            $.each(validations, function(i, v) {
                form.find(`[name="${v.field}"]`).addClass('is-invalid').next('.invalid-feedback').text(v.message);
            });
            swal('Atenção!', 'Informe os campos obrigatórios', 'warning');
            return false;
        }
        if (tipo === 'dcc-cartao') {
            const data = helpers.convertSerializeObject(form.serialize());
            pagamentoService.alterarCartao(data, function() {
                swal('Sucesso', 'Cartão alterado com sucesso!!!', 'success').then(function() {
                    location.reload();
                });
            }, function(erro) {
                console.log(erro);
                swal('Erro', 'Ocorreu um erro ao alterar o cartão', 'error');
            });
        } else {
            const id_parcela = t.data('id-parcela');
            const data = Object.assign({ id_parcela, cartao: 1}, helpers.convertSerializeObject(form.serialize()));
            if (tipo == 'cartao') {
                const atualiza = t.data('atualiza');
                const id_mens = t.data('id');
                processarCartao(Object.assign({id_mens, nao_atualiza: atualiza ? null : true}, data), tipo);
            } else {
                alterarFormaPagto(data);
            }
        }        
    });
}

function openModalDetails(e) {
    const t = $(e.relatedTarget);
    const id_mens = t.data('id');
    const mensalidade = getMensalidades().find(function (m) {
       return m.id_mens == id_mens
    });
    const tipo = t.data('type');
    let items = [];
    if(tipo === 'items') {
        items = mensalidade.items.map(function(i) {
            return {
                tipo: ['C', 'P'].includes(i.tipo) ? 'Plano' : (i.tipo === 'A' ? 'Adicional' : 'Serviço'),
                descricao: i.descricao,
                valor: i.preco_venda,
                icone: ['C', 'P'].includes(i.tipo) ? 'lista-de-controle.svg'
                    : (i.tipo === 'A' ? 'trabalhos.svg' : 'servicos.svg')
            };
        });
    } else if(tipo === 'transacoes') {
        items = mensalidade.transacoes.map(function (i) {
            const tipoPagamento = configPagamento.find(function (tp) {
                return tp.target.includes(i.tipo_pagamento);
            });
            return {
                tipo: 'Tipo de Pagamento',
                descricao: i.numero_cartao || i.descricao_pagamento,
                valor: i.valor_transacao,
                data: i.data_criacao,
                bandeira: i.bandeira,
                status: i.status,
                icone: tipoPagamento ? tipoPagamento.icon : 'money.svg',
                classe: helpers.slug(i.status)
            };
        });
    }

    const html = items.map(function (i) {
        return criarLinhasDetalhes(i);
    }).join('');
    modalDetalhes.find('#tituloDetails').html(t.data('title'));
    modalDetalhes.find('.modal-body').html(html);
}

function templateFiltro(data) {
    const tipoTexto = data.modelo === 'seguro' ? 'Tipo do Veiculo' : 'Tipo do Plano';
    const label = data.modelo === 'seguro' ? 'Veiculo' : 'Plano';
    return `<div class="col-md-5"><div class="row form-group mb-3"><label for="plano" class="col-xl-5">${tipoTexto}:</label>
            <div class="col-xl-7"> <select name="tipo_veiculo" id="tipo_veiculo" class="form-control form-control-sm select2">
            ${data.data.map(function(tipo) { return '<option value="'+tipo.id+'">'+tipo.nome+'</option>'}).join('')}
            </select></div></div></div><div class="col-md-7"><div class="row form-group mb-3">
            <label for="plano" class="col-xl-2">${label}:</label><div class="col-xl-10">
            <select name="plano" id="plano" class="form-control form-control-sm select2"></select></div></div></div>`;
}