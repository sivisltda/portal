import { getConfig as configFunc } from './../config/config.js';

import * as loading from './../core/loading.js';
import { convertSerializeObject } from './../core/function.js';
import { createInput } from './../core/form.js';
import { validate, validate_cpf, validate_email, validacaoInput } from './../core/validator.js';

import * as dependenteService from './../service/dependente.service.js';
import * as utilFormDependente from './../shared/formdependente.js';
import * as regraDependentes from './../shared/regraDependentes.js';

const formDependente    = $('#form-dependente');
const content           = $('#content');
const boxLoader         = $('.boxLoader');
const config            = configFunc();

let valoreInputs = {};
let parentesco = {};

$(document).ready(function() {  
    loading.subscribe(function(numero) {
        !!numero ? boxLoader.show() : boxLoader.hide();
    });
    dependenteService.getListaDependentes(function(res) {
        parentesco = res.parentesco ? res.parentesco : {};
        content.html(montaListaDependente(res));
        formDependente.html(`<div class="row">
        ${createInput(utilFormDependente.criarFormDependentes(1, parentesco), 'column')}
        </div><input type="hidden" name="id_dependente" id="id_dependente_${1}">`);
        utilFormDependente.rulesDependente(res.per_jur);
        formDependente.find('[name="data_nascimento"]').on('blur', function(e) {
            e.preventDefault();
            blurDataNascimento($(this), res.regra, res.pes_jur);
        });
        formDependente.on('blur', '[name="cnpj"], [name="email"]', blurCNPJEmail);
    }, function(erro) {
        console.log(erro);
        if(erro.status === 403) {
            window.history.back();
        }
        
    }, 1);

    $(document).on('focusin', '.mask', function(e) {
        e.preventDefault();
        const t = $(this);
        t.mask(t.data('mask-format'));
    });

    $('#modalDependente').on('show.bs.modal', function(e) {
        const button = $(e.relatedTarget);
        const dependente = button.data('dependente');
        $('#content-btn').html('');
        formDependente.find(':input').val('').removeClass('is-invalid').removeAttr('readonly');
        formDependente.find(':input').next().text('');
        if(dependente) {
            $.each(dependente, function(i,v) {
                const campo = $(`input[name="${i}"], select[name="${i}"]`);
                if(campo.length) {
                    campo.val(v);
                }
            });
            (function() {
                formDependente.find(':input').attr('readonly', 'readonly');
                formDependente.find('input[name="celular"]').removeAttr('readonly');
                if($('.parentesco').length) {
                    formDependente.find('.parentesco').attr('readonly', 'readonly');
                } 
            })();           
        }

    });

    $('#btn-dependente').on('click', function(e) {
        e.preventDefault();
        formDependente.submit();
    });

    $(document).on('click', '.btn-excluir-dependente', function(e) {
        e.preventDefault();
        const id_dependente = $(this).data('id');
        const nome = $(this).data('nome');
        swal({
            title: "Remover Dependente",
            text: `Deseja remover o(a) dependente ${nome}?`,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Sim, remover!",
            cancelButtonText: "Não, Cancelar!",
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#9E9E9E"
        }).then(result => {
            if (result.value) {
               deleteDependente(id_dependente, function(res) {
                swal('Sucesso', 'Dependente removido com sucesso!', 'success').then(function(res) {
                    if(res) {
                        window.location.reload();
                    }
                });
               }, function(error) {

               });
            } 
        });
    });

    formDependente.on('submit', function(e) {
        e.preventDefault();
        const form = $(this);
        form.find(':input').removeClass('is-invalid').next().text('');
        const data = convertSerializeObject(form.serialize());
        const rules = utilFormDependente.getRules();
        const validations = validate(rules, form);
        if (validations.length) {
            $.each(validations, function(v) {
                form.find(`[name="${v.field}"]`).addClass('is-invalid').next().text(v.message);
            });
            return false;
        }
        
        dependenteService.cadastrarDependentes(data, function(response) {
            console.log(response);
            swal('Sucesso', 'Dependente salvo com sucesso!', 'success').then(function(res) {
                if(res) {
                    window.location.reload();
                }
            });
            
        }, function(err) {
            swal('Erro', err.responseText, 'error');
            
        }, 1);
    });

    $(document).on('blur', '.cpf_cnpj', function(e) {
        e.preventDefault();
        const t = $(this);
        t.removeClass('is_invalid').next().text('');
        if(t.val() === '') {
            t.addClass('is-invalid').next().text('CPF/CNPJ Obrigatório');
            return false;
        }
        if(t.val().length === 14) {
            if(!validate_cpf(t.val())) {
                t.addClass('is-invalid').next().text('CPF inválido');
                return false;
            }
        } else if(t.val().length === 19) {
            if(!validate_cnpj(t.val())) {
                t.addClass('is-invalid').next().text('CNPJ inválido');
                return false;
            }
        }
    });
    
    $(document).on('blur', '.email', function(e) {
        e.preventDefault();
        const t = $(this);
        t.removeClass('is_invalid').next().text('');
        if(!validate_email(t.val())) {
            t.addClass('is-invalid').next().text('Email inválido');
            return false; 
        }
    });

});

function montaListaDependente(res) {
    let html = '';
    const qtdDependente = res.lista.length;
    const podeAdd = res.regra && res.regra.find(function(r) {
        return qtdDependente >= Number(r.qtd_min) && qtdDependente < Number(r.qtd_max);
    });

    if (res.adicionar) {
        if (podeAdd || !res.regra) {
            html+= '<div class="d-flex justify-content-end mb-3"><button class="btn btn-primary" data-toggle="modal"'
                   +' data-backdrop="static" data-target="#modalDependente">Novo Dependente</button></div>';
        } else {
            html+= '<div class="d-flex justify-content-end mb-3"><span class="btn btn-secundaria">Limite de Dependentes Excedido</span></div>';
        }
    }

    html+= `<div class="row align-content-stretch">${res.lista.map(function(item) {
        return `<div class="col-xl-4 col-lg-6 mb-3">${templateDependente(item)}</div>`;
    }).join('')}</div>`;
    return html;
}


function templateDependente(item) {
    const dados = { nome: 'Nome', data_nascimento: 'Data de Nasc.', email: 'Email',  cnpj: 'CPF', data_inclusao: 'Data de Inclusão', pri_cnh: 'Data de Admissão' };
    return `<a class="item-dependente-single" data-toggle="modal" data-backdrop="static" 
                    data-dependente='${JSON.stringify(item)}' data-target="#modalDependente">
        <div class="card card-dependente p-2">
        <div class="row align-items-center">
            <div class="col-4">
                <img src="${config.site}assets/images/users/person.png" alt="${item.nome}" class="rounded-circle img-thumbnail">                                            
            </div>
            <div class="col-8">
                ${Object.keys(dados).map(function(i) {
                    if(item.hasOwnProperty(i)) {
                        return '<p><strong>'+dados[i]+': </strong>'+item[i]+'</p>';
                    } return '';
                }).join('')}
                
            </div>
        </div> 
        <div class="d-flex justify-content-end">
            <span class="parentesco ${item.status ? item.status : ''}">${item.parentesco ? item.nome_parentesco : item.status}</span>
        </div>
    </div></a>`;
}

function deleteDependente(id_dependente, callback, errCallback) {
    $.get(config.api+"dashboard.php", { functionPage: 'RemoverDependente', id_dependente: id_dependente }, function(res) {
        callback(res);
    }, 'json').fail(function(err) {
        errCallback(err);
    });
}

function blurCNPJEmail(e) {
    e.preventDefault();
    const t = $(this);
    const valor = t.val();
    const nomeInput = t.attr('name');
    const rules = utilFormDependente.getRules();
    const validacao = validacaoInput({ rules })(t);
    if (!validacao || !t.val().trim().length || valoreInputs[nomeInput] === valor) {
        return false;
    }
    const form = t.closest('form');
    const cnpj =  nomeInput === 'cnpj' ? t.val() : null;
    const email = nomeInput === 'email' ? t.val() : null;
    const id_dependente = form.find('[name="id_dependente"]').val();
    valoreInputs[nomeInput] = t.val();
    
    dependenteService.consultarUsuarioPorCPF({ cnpj, email, id_dependente}, function(data) {
        if(data.id) {
            swal({
                title: 'Usuário já encontrado', html: `Atenção! 
                </br> Você já deseja incluir ${data.razao_social}, como seu dependente?`, type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#9E9E9E',
                confirmButtonText: 'Sim',  cancelButtonText: 'Não'
            }).then(function(res) {
                if (res.value) {
                    data.id_dependente = data.id;
                    incluindoDependente(data);
                } else if(res.dismiss === Swal.DismissReason.cancel) {
                    t.val(''); 
                    valoreInputs[nomeInput] = '';       
                }
            });
        }
        
    }, function(erro) {
        console.log(erro);
        if (erro.status === 404) {
            t.closest('.form-group').next().find(':input').focus();
        } else if (erro.status === 403) {   
            const mensagem = erro.responseJSON.erro;
           t.val('').addClass('is-invalid').siblings('.invalid-feedback').text(mensagem);
           valoreInputs[nomeInput] = '';
        }
        
    });
}

function blurDataNascimento(t, dependentesRegra, pes_jur) {
    t.removeClass('is-invalid');
    const data_nascimento = t.val();
    const rules = utilFormDependente.getRules();
    if (data_nascimento && validacaoInput({ rules })(t)) {
        const form = t.closest('form');
        const regras = regraDependentes.configRegrasIdade(
            form, ['cnpj', 'email', 'celular'], dependentesRegra, parentesco, pes_jur);
        regras.forEach(function(r) {
             r.regra && r.acao();
        });
    } else {
        t.addClass('is-invalid');
    }
}