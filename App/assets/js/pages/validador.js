import { convertSerializeObject } from './../core/function.js';
import * as usuarioService from './../service/usuario.sevice.js';

import { getConfig as configFunc } from './../config/config.js';

const form          = $('#form-buscador');
const resposta      = $('#resposta');
const campoBusca    = form.find('[name="busca"]');
const modulos       = JSON.parse($('#modulos').val());
const campoId       = form.find('[name="id_cliente"]');

const config        = configFunc();

const valoresInput = {
    busca: ''
};

const sourceListaClientes = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: config.api+'portal/usuario?functionPage=listaClientes',
    remote: {
        url: config.api+'portal/usuario?functionPage=listaClientes&q=%QUERY',
        wildcard: '%QUERY'
    }
});

$(document).ready(function() {
    resposta.html(templateInicio());
    form.on('submit', formBuscador);
    campoBusca.typeahead({
        classNames: {
          input: 'w-100'
        },
        hint: true,
        highlight: true,
        minLength: 1
    },  {
        name: 'busca',
        display: 'razao_social',
        source: sourceListaClientes,
        templates: {
            header: '<h3 class="titulo-autocomplete">Clientes / Prospects</h3>',
            limit: 10,
            suggestion: function(data) {
                return `<div class="box-suggestion"><strong>${data.razao_social}</strong>
                (${data.tipo === "P" ? "Prospect" : "Cliente"})</div>`
            }
        }
    });
    campoBusca.bind('typeahead:select', function(ev, suggestion) {
        console.log('Selection: ' , suggestion);
        campoId.val(suggestion.id);
        form.submit();
    });
    /* campoBusca.bind('typeahead:autocomplete', function(ev, suggestion) {
        console.log('Autocomplete: ' , suggestion);
        campoId.val(suggestion.id);
        form.submit();
    }); */
});

function formBuscador(e) {
    e.preventDefault();
    const t = $(this);
    campoBusca.removeClass('.is-invalid').next('.invalid-feedback').text('');
    const dados = convertSerializeObject(t.serialize());
    if (dados.busca === "") {
        campoBusca.addClass('.is-invalid').next('.invalid-feedback').text('Valor inválido para busca');
        return false;
    }
    if (valoresInput.busca === dados.busca) {
        return false;
    }
    valoresInput.busca = dados.busca;
    resposta.html(templateLoading());
    usuarioService.consultaUsuarioValidador(dados, function(res) {
        resposta.html(templateUsuario(Object.assign(res, { campo: dados.busca })));
        campoBusca.typeahead('val', '').focus();
        campoId.val('');
    }, function(error) {
        const erro = error.responseJSON.erro;
        if (error.status == 404) {
           resposta.html('<span class="nome-user">Usuário não encontrado!</span>');
        } else {
            console.log(erro);
            swal('Erro', 'Ocorreu um erro ao buscar o usuário', 'error');
        }
    });

}

 
function templateInicio() {
    return `
        <span class="nome-user text-center">Digite o dado do ${config.adm_associado}<br />
        que deseja buscar no campo acima.</span>
    `;
}

function templateUsuario(dados) {
    const status = (['Ativo', 'AtivoCred', 'AtivoAbono'].includes(dados.status) ? {
            classe: 'ativo',
            nome: 'Ativo'
        } : (modulos.sau && dados.status === 'AtivoEmAberto' ? {
            classe: 'suspenso',
            nome: 'Suspenso'
        } :{
            classe: dados.status.toLowerCase(),
            nome: dados.status
        }));
    return `
        <div class="tipo-busca">Pesquisa:  ${dados.campo}</div>
        ${dados.isVeiculo ? templateInfoVeiculo(dados) : ''}
        <img class="img-user" src="${dados.image}" alt="${dados.razao_social}" />
        <span class="nome-user">${dados.razao_social}</span>
        <div class="status-user ${status.classe}">${status.nome}</div>
        ${dados.usuario_atual ? '<span class="tipo-busca mb-0">'+dados.usuario_atual+'</span>' : ''}
        ${dados.nome_plano ? '<span class="nome-plano">'+dados.nome_plano+'</span>' : ''}
    `;
}

function templateInfoVeiculo(dados) {
    return `<div class="info-veiculo">
        <div class="tipo-busca">${dados.marca} - ${dados.modelo} - ${dados.ano_modelo}</div>
    </div>`;
}

function templateLoading() {
    return `<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    <span class="nome-user">Aguarde...</span>`;
}