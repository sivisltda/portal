import carregaPortal from './../modules/load-module.js';
import * as loading from './../core/loading.js';

const app = $('#app');
const boxLoader = $('.boxLoader');

$(document).ready(function(e) {
    loading.subscribe(function(numero) {
        !!numero ? boxLoader.show() : boxLoader.hide();
    });
    carregaPortal(app, function(erro) {
        console.log(erro);
    });

});
