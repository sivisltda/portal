import { getConfig as configFunc } from './../config/config.js';
import { textToNumber, numberFormat, convertSerializeObject, criarDataTable } from './../core/function.js';
import { validate } from './../core/validator.js';

import * as mensalidadeService from './../service/mensalidade.service.js';

import * as loading from './../core/loading.js';

const boxLoader      = $('.boxLoader');
const config         = configFunc();
const modalProcessar = $('#myModalProcessar');
const nomeCliente    = $('#nomeCliente');
const idCliente      = $('#idCliente');
const form           = $('#formPgCard');
const btnSubmit      = $('#btnProcessar');
const selectTipoPag  = $('#txtmodPag');
const selectParcelas = $('#txtParcelas');
const txtValor       = $('#txtValor');
const tblPagamento   = $('#tbPagamentos');
const boxStatus      = $('#status');
const boxTpPagamento = $('#tipo_pagamento');
const txtBusca       = $('#cliente');
const dataFrom       = $("#rel-date-from");
const dataTo         = $("#rel-date-to");
const btnBusca       = $('#btnfind');

const statusPesquisa = {
    "aprovado": "Aprovado",
    "negada": "Negada",
    "inválida": "Inválida",
    "estornado": "Estornado"
};

let tiposPagamentos  = [];

const sourceListaClientes = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: config.api+'dashboard/pagamento?functionPage=listaClientes',
    remote: {
        url: config.api+'dashboard/pagamento?functionPage=listaClientes&q=%QUERY',
        wildcard: '%QUERY'
    }
});

const formaPagamento = [
    { descricao: 'À vista', desconto: 10, qtd: 1 },
    { descricao: '2x (Cartão de Crédito)', desconto: 9, qtd: 2},
    { descricao: '3x (Cartão de Crédito)', desconto: 8, qtd: 3},
    { descricao: '4x (Cartão de Crédito)', desconto: 7, qtd: 4},
    { descricao: '12x (Cartão de Crédito)', desconto: 5, qtd: 12},
];

const rules = {
    idCliente: {
        rule: 'required',
        message: {
            required: 'Selecione um cliente'
        }
    },
    txtmodPag: {
        rule: 'required',
        message: {
            required: 'Selecione uma forma de pagamento'
        }
    },
    txtValor: {
        rule: 'required',
        message: {
            required: 'Valor é obrigatório'
        }
    },
    txtParcelas: {
        rule: 'required',
        message: {
            required: 'Selecione uma Parcela'
        }
    },
    numcredcard: {
        rule: 'required|creditCard',
        message: {
            required: 'O número do cartão de crédito obrigatório'
        }
    },
    validcard: {
        rule: 'required',
        message: {
            required: 'O validade do cartão é obrigatória'
        }
    },
    codCC: {
        rule: 'required',
        message: {
            required: 'Código de CVC é obrigatório'
        }
    },
    nomcredcard: {
        rule: 'required',
        message: {
            required: 'O nome do titular do cartão é obrigatório'
        }
    }
};

$(document).ready(function () {
    loading.subscribe(function(numero) {
        !!numero ? boxLoader.show() : boxLoader.hide();
    });

    btnSubmit.on('click', function(e) {
        e.preventDefault();
        form.submit();
    });

    montarTabela();

    mensalidadeService.getTipoPagamentosCartao(function(res) {
        tiposPagamentos = res;
        const items = tiposPagamentos.map(function(item) {
            return `<option value="${item.id_tipo_documento}" data-tipo="${item.cartao_dcc}">${item.descricao}</option>`;
        }).join('');
        boxTpPagamento.html(items).select2({ placeholder: 'Selecione um tipo Pagamento'});
        const itemsStatus = Object.keys(statusPesquisa).map(function(key) {
            return `<option value="${key}">${statusPesquisa[key]}</option> `;
        });
        boxStatus.html(itemsStatus).select2({placeholder: 'Selecione um status'});
    }, function(erro) {
        console.log(erro);
        swal('Erro', 'Ocorreu um erro ao buscar os tipos de pagamentos', 'error');
    });

    txtBusca.on('keypress', function(event) {
        if (event.keyCode === 13) {
            btnBusca.click();
        }
    });

    btnBusca.click(function () {
        tblPagamento.fnReloadAjax(finalFind());
    });

    $('.auto-date').click(function(e) {
        dataFrom.val($(this).data('from'));
        dataTo.val($(this).data('to'));
        btnBusca.click();
    });

    form.on('submit', function(e) {
       e.preventDefault();
       const form = $(this);
        const validations = validate(rules, form);
        if (validations.length) {
            $.each(validations, function(i, v) {
                form.find("#" + v.field).addClass('is-invalid').siblings('.invalid-feedback').text(v.message);
                if(v.field === 'idCliente') {
                    nomeCliente.addClass('is-invalid');
                    form.find("#" + v.field).addClass('is-invalid').siblings('.invalid-feedback').show();
                }
            });
            swal('Atenção!', 'Informe os campos obrigatórios', 'warning');
            return false;
        }
        const data = convertSerializeObject(form.serialize());
        const valorPagar = selectParcelas.find('option:selected').data('valor');
        const tpPagamento = selectTipoPag.find('option:selected').data('tipo');
        data.txtValor = numberFormat(parseFloat(valorPagar).toFixed(2));
        data.tpPagamento = tpPagamento;
        boxLoader.show();
        mensalidadeService.processaPagamento(data, function(res) {
            swal('Sucesso!', 'Transação efetuada com sucesso', 'success');
            modalProcessar.modal('dispose');
        }, function(erro) {
            console.log(erro);
            swal({type: 'error', title: 'Erro', text: 'Ocorreu um erro ao processar o pagamento'});
        });

    });

    selectTipoPag.on('change', function(e) {
        e.preventDefault();
        const t = $(this);
        if(t.val()) {
            txtValor.prop('readonly', false).blur();
        } else {
            txtValor.prop('readonly',  true).val('').blur();
        }
    });

   txtValor.mask('#.##0,00', {reverse: true});

    nomeCliente.bind('typeahead:select', function(e, suggestion) {
        e.preventDefault();
        console.log(suggestion);
        idCliente.val(suggestion.id);
    });


    txtValor.on('blur', function(e) {
        e.preventDefault();
        const t = $(this);
        const valor = textToNumber(t.val());
        const tipoPagamento = tiposPagamentos.find(function(tp) {
           return tp.id_tipo_documento === selectTipoPag.val();
        });
        if(t.val() && tipoPagamento) {
            if(tipoPagamento.cartao_dcc === "1") {
                const listaParcelas = formaPagamento.map(function(fp) {
                    const valorDesconto =  valor * (1 - (fp.desconto / 100));
                    const descricao = fp.qtd === 1 ?  fp.descricao : `${fp.qtd}x ${numberFormat((valorDesconto / fp.qtd).toFixed(2), 1)}`;
                    return `<option value="${fp.qtd}" data-valor="${valorDesconto}">${descricao} - ${numberFormat(valorDesconto, 1)} </option>`;
                }).join('');
                selectParcelas.html(listaParcelas).change();
            } else {
                const valorDesconto =  valor * (1 - (formaPagamento[0].desconto / 100));
                selectParcelas.html(`<option value="1" data-valor="${valorDesconto}">${formaPagamento[0].descricao} - ${numberFormat(valorDesconto, 1)}</option>`).change();
            }
        } else {
            selectParcelas.html('<option value="">Insira o valor a ser transacionado</option>').change();
        }
    });

    modalProcessar.on('hidden.bs.modal', function () {
        form.removeClass('was-validated').trigger("reset").find(':input').removeClass('is-invalid');
        form.find(':input').siblings('.invalid-feedback').hide();

        tblPagamento.fnReloadAjax(finalFind());
    });

    modalProcessar.on('shown.bs.modal', function () {
        const items = tiposPagamentos.map(function(item) {
            return `<option value="${item.id_tipo_documento}" data-tipo="${item.cartao_dcc}">${item.descricao}</option>`;
        }).join('');
        selectTipoPag.html('<option value="">Selecione um tipo de pagamento</option>'+items);

        nomeCliente.typeahead({
            classNames: {
              input: 'w-100'
            },
            hint: true,
            highlight: true,
            minLength: 1
        },  {
            name: 'cliente',
            display: 'razao_social',
            source: sourceListaClientes,
            templates: {
                header: '<h3 class="titulo-autocomplete">Clientes / Prospects</h3>',
                empty: [
                    '<div class="empty-message">',
                    'Nenhum cliente encontrado com esse nome',
                    '</div>'
                ].join('\n'),
                limit: 10,
                suggestion: function(data) {
                    return `<div class="box-suggestion"><strong>${data.razao_social}</strong>
                    (${data.tipo === "P" ? "Prospect" : "Cliente"})</div>`
                }
            }
        });
    });
});

function montarTabela() {
   const classesStatus = {
        "aprovado": "success",
        "negada": "danger",
        "inválida": "secondary",
        "estornado": "warning"
    }
    const columns = {
        "aoColumns": [
            {"className": "centerTable", "sWidth": '10%', data: "data_transacao"},
            {"className": "centerTable", "sWidth": '10%', data: "razao_social"},
            {"className": "centerTable", "sWidth": '10%', data: "valor_transacao"},
            {"className": "centerTable", "sWidth": '10%', data: "tipo_documento"},
            {"className": "centerTable", "sWidth": '10%', data: "dcc_numero_cartao"},
            {"className": "centerTable", "sWidth": '10%',  "render": function (data, type, row) {
                return `<span class="text-${classesStatus[row.status.toLowerCase()]}">${row.status}</span>`;
            }},
            {"className": "centerTable", "sWidth": '10%', data: "error_msg"}
        ]
    };

    const columnsOrder = {
        "columnDefs": [
            { "responsivePriority": 1, "targets": 0 },
            { "responsivePriority": 1, "targets": -2 },
        ]
    };
   tblPagamento.dataTable(criarDataTable(15, Object.assign({}, columns, columnsOrder), finalFind(), function (settings, data) {
        boxLoader.hide();
    }));
}

function finalFind() {
    boxLoader.show();
    let sData = "&txtBusca="+txtBusca.val();
    if ( dataFrom.val() !== "" && dataTo.val() !== "") {
        sData+= '&inicio=' + dataFrom.val() + '&fim=' + dataTo.val();
    }
    const filtros = {
        "status": boxStatus.val(),
        "tipo_pagamento": boxTpPagamento.val(),
    };
    sData+= Object.keys(filtros).map(function(item) {
        if (filtros[item].length) {
            return '&'+item+'=' + filtros[item].join(',');
        }
        return '';
    }).join('');

    return config.api+'dashboard/pagamento?functionPage=listaTransacoes'+sData;
}