import { validate_email } from './../core/validator.js';
import * as loading from './../core/loading.js';

import * as usuarioService from './../service/usuario.sevice.js';

const formLogin = $('#frm-login');
const boxLoader = $('.boxLoader');

$(document).ready(function() {
    loading.subscribe(function(numero) {
        !!numero ? boxLoader.show() : boxLoader.hide();
    });
    
    formLogin.on("submit", function(e){
        e.preventDefault();
    
        const email    =  formLogin.find('#email');
        const password =  formLogin.find('#password');
    
        formLogin.find(':input').removeClass('is-invalid').siblings('.invalid-feedback').text('');
    
        if(email.val() === '' || !validate_email(email.val())){
            email.addClass('is-invalid').siblings('.invalid-feedback').text('Digite um email válido');
            return false;
        }
    
        if(password.val() === '') {
            password.addClass('is-invalid').siblings('.invalid-feedback').text('Digite uma senha');
            return false;
        }      
    
        usuarioService.loginUsuario(email.val(), reverseString(password.val()), function(res) {   
            window.location = 'dashboard';
        }, function (erro) {
            console.log(erro.responseJSON);
            swal('Ops','Nenhum cadastro encontrado!','error');
            return false;
        });
    });

});


function reverseString(str) {
    return str.split("").reverse().join("");
}