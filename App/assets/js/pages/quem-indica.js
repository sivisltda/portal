import { convertSerializeObject, errorCamposInvalidos } from './../core/function.js';
import { textToNumber, numberFormat, valorDesconto } from './../core/function.js';
import { validate_email, validate } from './../core/validator.js';
import * as loading from './../core/loading.js';
import * as companhaService from './../service/companha.service.js';

const formLead = $('#form-lead');
const nomeEmpresa = $('#nome_empresa').val();
const tabIndicadores = $('#recompensa');
const tabCupons = $('#cupons');
const numberIndicadores= $('#numbers-indicator');
const valoresIndicator= $('#valores-indicator');
const boxLoader = $('.boxLoader');
const consultor = $('#consultor').val();
const numIndicados = $('#num-indicados');
const numCupons = $('#num-cupons');

let idLead = null;
let nomeLead = '';

const rules = {
  email: {
    rule: "required|email",
    message: {
        required: "Email é um campo obrigatório",
        email: "Campo deve ser do tipo email"
    }
  }, nome: {
    rule: "required|min:2",
    message: { required: "Nome é um campo obrigatório", min: "mínimo :min caracteres" }
  }, cpf: {
    rule: "required|cpf",
    message: { required: "Cpf é um campo obrigatório", cpf: "Cpf inválido!" }
  }, celular: {
    rule: "required",
    message: {
        required: "Telefone é um campo obrigatório"
    }
  }
};

$(document).ready(function() {
  loading.subscribe(function(numero) {
    !!numero ? boxLoader.show() : boxLoader.hide();
  });
  formLead.on('submit', function(e) { e.preventDefault(); });
  $(document).on('focusin', '[data-toggle=input-mask]', function(e) {
    e.preventDefault();
    const t = $(this);
    t.mask(t.data('mask-format'));
  });
  montarFormEmail();
});

function montarFormEmail() {
  formLead.html(templateEmail());
  formLead.find('input[name="email"]')
  .on('blur', getLeadEmail)
  .on('change', updateEmail);
  tabIndicadores.html(templateTabIndicatorNoLogin());
  tabCupons.html(templateTabCuponsNoLogin());
  formLead.find('#btn-cadastrar').on('click', function(e) {
    e.preventDefault();
    salvarLead('cadastrar');
  });
}

function montarDadosLead(idLead) {
  companhaService.getDadosLead({ indicador_lead: idLead }, function(res) {
    tabIndicadores.html(templateTabIndicator(res.indicados));
    tabCupons.html(templateTabCupons(res.cupons));
    numberIndicadores.html(templateIndicators(res.indicados));
    valoresIndicator.html(templateIndicatorsFin(res.financeiro));
    numIndicados.text(res.indicados.length);
    numCupons.text(res.cupons.length);
  }, function(erro) {
    console.log(erro);
    swal('Erro', 'Ocorreu um erro ao buscar os dados do Lead', 'error');
  });
}

function montarAddLead() {
  formLead.html(templateAdicionaLead());
  formLead.find('#btn-indicar').on('click', function(e) {
    e.preventDefault();
    salvarLead('indicar');
  })
}

function updateEmail(e) {
  e.preventDefault();
  formLead.find('.dados-lead').html('');
  formLead.find('#btn-cadastrar').attr('disabled', 'disabled');
}

function getLeadEmail(e) {
  e.preventDefault();
  const t = $(this);
  const valor = t.val();
  t.removeClass('is-invalid').text('');
  if (valor === "") {
      t.removeClass('is-valid').addClass('is-invalid').focus()
      .siblings('.invalid-feedback').text('E-mail obrigatório');
      return false;
  }
  if (!validate_email(valor)) {
    t.removeClass('is-valid').addClass('is-invalid').focus()
    .siblings('.invalid-feedback').text('E-mail inválido');
    return false;
  }
  t.addClass('is-valid').siblings('.invalid-feedback').text('');
  companhaService.getLead({ email: valor }, function(res) {
    idLead = res.id;
    nomeLead = res.razao_social;
    montarInfo(idLead);
  }, function(erro) {
    if (erro.status === 404) {
      formLead.find('.dados-lead').html(templateDadosUsuario());
      formLead.find('#btn-cadastrar').removeAttr('disabled');
    } else {
      swal('Erro', 'Ocorreu um erro ao buscar os dados do Lead', 'error');
    }
  });
}

function montarInfo(idLead) {
  montarDadosLead(idLead);
  montarAddLead();
}

function salvarLead(tipo) {
  formLead.find(':input').removeClass('is-invalid').addClass('is-valid')
  .siblings('.invalid-feedback').text('');
  const validations = validate(rules, formLead);
  if (validations.length) {
    const errors = validations.reduce(function(acc, i) {
        acc[i.field] = i.message;
        return acc; 
    },  {});
    errorCamposInvalidos(formLead, errors);        
    swal('Atenção!', 'Informe os campos obrigatórios', 'warning');
    return false;
  }
  const data = convertSerializeObject(formLead.serialize());
  data.tipo = tipo;
  if (tipo === 'indicar') {
    data.indicador_lead = idLead;
  } if (consultor) {
    data.consultor = consultor;
  }
  companhaService.salvarLead(data, function(res) {
    if (data.tipo === 'cadastrar') {
      idLead = res;
      nomeLead = data.razao_social ? data.razao_social : data.nome;
    } 
    montarInfo(idLead);
  },function(erro) {
    console.log(erro);
    if (erro.status === 422) {
      const errors = erro.responseJSON.erro;
      Object.keys(errors).forEach(function(el) {
        const input = formLead.find(`input[name="${el}"]`);
        if (input.length) {
          input.removeClass('is-valid').addClass('is-invalid').focus()
          .siblings('.invalid-feedback').text(errors[el]);
        }
      });
    } else {
      swal('Erro', erro.erro, 'error');
    }
  });
}

function templateEmail() {
  return `<div id="grupo-email">
    <h5 class="mb-3 texto-cadastro">
      Faça seu cadastro e começa a indicar, quantos mais 
      indicações melhor a sua chance de ganhar!
    </h5>
    <div class="form-group input-group-icon">
        <input type="email" id="email" name="email" 
        placeholder="E-mail" class="form-control">
        <span class="icon-group">
            <i class="mdi mdi-email-outline"></i>
        </span>
        <div class="invalid-feedback"></div>
    </div>
    <div class="dados-lead"></div>
    <div class="mt-3">
        <button id="btn-cadastrar" class="btn btn-block btn-rounded btn-grad">
            <i class="mdi mdi-check"></i>
            Próximo
        </button>
    </div>
  </div>
  `;
}

function templateAdicionaLead() {
  return `<div id="grupo-cadastro">
  <h5 class="mb-3 texto-cadastro">
    Olá <strong>${nomeLead}</strong>, Seja bem-vindo ao Programa de Indiçação da ${nomeEmpresa}, 
    Indique seus amigos e acumule recompensas. 
    Quanto mais indicar, mais você ganha!
  </h5>
  <div class="dados-lead">${templateDadosUsuario(1)}</div>
  <div class="mb-3">
      <button id="btn-indicar" class="btn btn-block btn-rounded btn-grad">
          <i class="mdi mdi-account-plus-outline"></i>
          Indicar
      </button>
  </div>
  `;
}

function templateDadosUsuario(indicador = 0) {
  return `<div class="form-group input-group-icon">
    <input type="text" id="nome" name="nome" 
    placeholder="${indicador ? 'Nome do Indicado': 'Seu nome'}" 
    class="form-control">
    <span class="icon-group">
        <i class="mdi mdi-account-outline"></i>
    </span>
    <div class="invalid-feedback"></div>
  </div>
  <div class="form-group input-group-icon">
    <input type="tel" id="celular" name="celular" 
    placeholder="${indicador ? 'Celular do Indicado': 'Seu celular'}" 
    class="form-control" data-toggle="input-mask" data-mask-format="(99) 99999-9999">
    <span class="icon-group">
        <i class="mdi mdi-cellphone"></i>
    </span>
    <div class="invalid-feedback"></div>
  </div>
  ${indicador ? `` : `<div class="form-group input-group-icon">
    <input type="text" id="cpf" name="cpf" placeholder="Seu CPF" 
    class="form-control" data-toggle="input-mask" data-mask-format="999.999.999-99">
    <span class="icon-group">
        <i class="mdi mdi-cellphone"></i>
    </span>
    <div class="invalid-feedback"></div>
  </div>`}
  `;
}

function templateIndicators(dados) {
  const opcoes = {
    pendente: {
      classe: 'pendente',
      label: 'Pendentes',
      status: 'pendente'
    },      
    prospect: {
      classe: 'proactive',
      label: 'Em negociação',
      status: 'prospect'
    },
    confirmado: {
      classe: 'active',
      label: 'Confirmado',
      status: 'ativo'
    },
    inativo: {
      classe: 'inactive',
      label: 'Inativos',
      status: 'inativo'
    }
  };
  return Object.keys(opcoes).map(function(k) {
    const opcao = opcoes[k];
    const total= dados.filter(function(d) {
      return d.status === opcao.status;
    }).length;
    return `<div class="col-lg-3 mb-lg-0 col-6 mb-3" style="cursor: pointer;"
    onclick="$('.item-user').hide(); $('.item-user.${opcao.classe}').show();">
    <div class="box-count ${opcao.classe}">
      <span class="qtd">${total}</span>
      <span class="info-text">${opcao.label}</span>
    </div>
  </div>`;
  }).join('');
}

function templateIndicatorsFin(dados) {
    const valor_item = dados.length ? dados.reduce(function(acc, item) {
        acc += (parseFloat(item.valor) * (item.tipo === "1" ? -1 : 1));
        return acc; 
    }, 0) : 0;     
    const linha = function(item) {
        return `<tr class="${item.tipo === "1" ? "pago" : ""}">
                    <td>${item.data_venda}</td>
                    <td>${item.razao_social}</td>
                    <td>${(item.tipo === "1" ? "-" : "") + numberFormat(item.valor, 1)}</td>
                </tr>`;
    };
    return `<div class="row justify-content-end">                     
        <div class="mr-2">
            <p class="m-0">Seu Saldo:</p>
            <h1 class="m-0">${numberFormat((valor_item > 0 ? valor_item : 0), 1)}</h1>
        </div>
    </div>                        
    <hr>
    <table class="table-valores-indicator">
        <thead>
            <tr>
                <th>DATA:</th>
                <th>NOME:</th>
                <th>VALOR:</th>
            </tr>                            
        </thead>
        <tbody>${dados.map(linha).join('')}</tbody>
    </table>`;
}

function templateTabIndicatorNoLogin() {
  return `<div class="box-no-login">
    <div class="container-icon">
      <i class="mdi mdi-account-multiple-check texto-secundaria"></i>
    </div>  
    <h4 class="text-no-login">Insira seus dados para visualizar suas indicações.</h4> 
  </div>
`;
}

function templateTabIndicator(leads) {
  if (leads.length) {
    const itemLead = function(lead) {
      return `
        <li class="item-user ${lead.status === 'ativo' ? 'active' 
        : (lead.status === 'inativo' ? 'inactive' 
        : (lead.status === 'prospect' ? 'proactive' : 'pendente'))}">
          <i class="mdi mdi-account"></i>
          <span class="name-user">${lead.razao_social}</span>
          <span class="status-user">
            <i class="mdi ${lead.status === 'ativo' ? 'mdi-check' 
            : (lead.status === 'inativo' ? 'mdi-close' : 'mdi-clock')}"></i>
          </span>
        </li>
      `;
    };
    return ` 
      <div class="box-list-users">
        <ul class="list-users">${leads.map(itemLead).join('')}</ul>
      </div>
    `;
  } 
  return `<div class="box-no-login">
  <div class="container-icon">
    <i class="mdi mdi-account-multiple-check texto-secundaria"></i>
  </div>  
  <h4 class="text-no-login">Nenhum indicado, aproveite e indique seus amigos agora!</h4> 
</div>`;
}

function templateTabCuponsNoLogin() {
  return `<div class="box-no-login">
    <div class="container-icon">
      <i class="mdi mdi-ticket texto-secundaria"></i>
    </div>  
    <h4 class="text-no-login">Insira seus dados para visualizar seus cupons.</h4> 
  </div>
  `;
}

function templateTabCupons(cupons) {
  if (cupons.length) {
    const itemCupom = function(cupom) {
      return `<li class="item-cupom">
        <div class="info-cupom">
          <i class="mdi mdi-ticket"></i>
          <span class="name-cupom">${cupom.cupom}</span>
        </div>
        <div class="info-origem">
          <i class="mdi mdi-account"></i>
          <div class="name-origem">${cupom.razao_social}</div>
        </div>
      </li>
      `;
    }
    return `<div class="box-list-users">
      <ul class="list-users">${cupons.map(itemCupom).join('')}</ul>
      </div>
    `;
  }
  return `
  <div class="box-no-login">
    <div class="container-icon">
      <i class="mdi mdi-ticket texto-secundaria"></i>
    </div>  
    <h4 class="text-no-login">Nenhum cupom encontrado!</h4> 
  </div>
  `;
}