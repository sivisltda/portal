import * as loading from './../core/loading.js';

import * as faqService from './../service/duvida.service.js';

const accordion = $('#accordion');
const boxLoader = $('.boxLoader');

$(document).ready(function() {
    loading.subscribe(function(numero) {
        !!numero ? boxLoader.show() : boxLoader.hide();
    });
    faqService.getListaFaq(function(data) {
        console.log(data);
        accordion.html(data.map(templateFaq).join(''));
    }, function(erro) {
        console.log(erro);
        swal('Erro', 'Ocorreu ao listar as faqs', 'erro');
    });

});


function templateFaq(item, index) {
    return `<div class="card mb-0">
    <div class="card-header" id="heading${item.id}">
        <h5 class="m-0">
            <a class="text-dark d-block pt-2 pb-2" data-toggle="collapse" href="#collapse${item.id}" aria-expanded="${index === 0 ? 'true' : 'false'}" aria-controls="collapse${item.id}">
                ${item.titulo} <span class="float-right"><i class="mdi mdi-chevron-down accordion-arrow"></i></span>
            </a>
        </h5>
    </div>
    <div id="collapse${item.id}" class="collapse${index === 0 ? ' show' : ''}" aria-labelledby="heading${item.id}" data-parent="#accordion">
        <div class="card-body">
           ${item.conteudo}
        </div>
    </div>
</div>`;
}

function templateEmpty() {
    return `Nenhum Faq Cadastrada`;
}