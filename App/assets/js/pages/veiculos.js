import { textToNumber, numberFormat  } from './../core/function.js';
import * as loading from './../core/loading.js';

import { getVeiculos } from './../service/admin.service.js';

const listVeiculos  = $('#veichles');
const boxLoader     = $('.boxLoader');

const tiposVeiculos = {
    1: 'mdi-car-sports',
    2: 'mdi-motorbike',
    3: 'mdi-truck'
};

let pagina = 1;

$(document).ready(function() {
    loading.subscribe(function(numero) {
        !!numero ? boxLoader.show() : boxLoader.hide();
    });

    getListaVeiculos();

    $(document).on('click', '.page-prev', function(e) {
        e.preventDefault();
        pagina-=1;
        getListaVeiculos();
    });

    $(document).on('click', '.page-next', function(e) {
        e.preventDefault();
        pagina+=1;
        getListaVeiculos();
    });

    $(document).on('click', '.page-number', function(e) {
        e.preventDefault();
        pagina = $(this).data('page');
        getListaVeiculos();
    });


});


function getListaVeiculos() {
    getVeiculos({
        iDisplayLength: 2,
        page: pagina
    },function(res) {
        listVeiculos.html(`<div class="row justify-content-center">${templateVeiculo(res)}</div>${paginacao(res.pagination)}`);
    }, function(erro) {
        console.log(erro);        
        swal('Erro', 'Ocorreu um erro a buscar a listagem de veículos', 'error');
    });
}


function templateVeiculo(data) {
   return data.aaData.map(function (veiculo) {
       veiculo.acessorios.unshift({ id_servico_acessorio: veiculo.produto_id, descricao: veiculo.produto,
           preco_venda: veiculo.valor_plano_item });
        return `
        <div class="col-xl-6">
            <div class="card cardCars" id="card${veiculo.id}">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-3">
                        <strong>${veiculo.modelo}</strong> - <span>${veiculo.ano_modelo}</span>
                        <i class="mdi ${tiposVeiculos[veiculo.tipo_veiculo]}" style="float:right; font-size: 20px;"></i>
                    </h4>
                   <hr/>
                   <div class="container-veiculos">
                       <div class="text-left">
                           ${listarDadosVeiculos(veiculo)}
                            <hr/>
                            <div class="container-cobertura">
                                <div class="header-cobertura bg-secundaria text-white">Coberturas</div>
                                <div class="body-cobertura">
                                    ${veiculo.produto_descricao ||''}
                                    ${templateCobertura(veiculo.servicos)}
                                </div>
                                <div class="footer-cobertura bg-secundaria text-white">
                                    <div class="nome-cobertura">${veiculo.produto}</div>
                                    <div class="preco-cobertura">${veiculo.valor_plano_item}</div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <div class="table-header"> <b>Adicionais</b> </div>
                                ${templateAcessorios(veiculo.acessorios)}
                            </div>
                       </div>
                       <div class="veiculo-acao">  
                            <div class="row">
                               <div class="col-sm-8">
                                   <div class="clearfix pt-3">
                                       <h6 class="text-muted">Observações:</h6>
                                       <small> 
                                            *A Taxa de adesão será paga somente no primeiro mês. <br>
                                            *A mensalidade será cobrada somente após a avaliação técnica e aprovação da ${data.config.nome.toUpperCase()}
                                       </small>
                                   </div>
                               </div>
                               <div class="col-sm-4">                                   
                                   <div class="clearfix"></div>
                               </div>
                            </div>
                       </div>
                   </div>
                </div>
            </div>
        </div>
        `;
    }).join('');
   /*
   button imprimir
   <div class="float-right d-print-none mt-3">
        <a href="#" class="btn btn-amd"><i class="mdi mdi-printer"></i> Imprimir Contrato </a>
    </div>
    */
}

function listarDadosVeiculos(data) {
    const mostrar = { 'placa': 'Placa', 'marca': 'Marca', 'modelo': 'Modelo', 'ano_modelo': 'Ano',
    'renavam': 'Renavam', 'valor': 'Valor FIPE', 'mes_referencia': 'Mês Referência', 'codigo_fipe': 'Código FIPE',
    'combustivel': 'Combustível', 'nome_cor': 'Cor', 'data_cadastro': 'Data de Cadastro'};
    return Object.keys(mostrar).map(function(e) {
        if(data.hasOwnProperty(e)) {
            let valor = data[e];
            return `<p class="text-muted"><strong>${mostrar[e]}:</strong><span class="ml-2">${valor.toUpperCase()}</span></p>`;
        }
        return '';
    }).join('');
}

function templateCobertura(items) {
    if (items.length) {
        return `<ul>
            ${items.map(function(item) {
                return `<li>${item.descricao}</li>`;
            }).join('')}
        </ul>`;
    }
    return '';
}

function templateAcessorios(acessorios) {
     if(acessorios.length > 0) {
         let total = 0;
         let html = `<table class="table" style="margin-bottom:0">
           <thead>
               <tr>
                 <th>Item</th>
                 <th class="text-right">Preço</th>
               </tr>
           </thead>
           <tbody>`;
         html+= acessorios.map(function(item) {
             if(textToNumber(item.preco_venda) > 0) {
                 total+= textToNumber(item.preco_venda);
                 return `
                     <tr>
                          <td>${item.descricao}</td>
                          <td class="text-right">${item.preco_venda}</td>
                      </tr>
                 `;
             }
             return '';
         }).join('');
         html+=` </tbody>
        </table>
        <div class="table-footer"> <b>Total:</b> <span class="float-right">&nbsp;${numberFormat(total, 1)}</span></div>`;
        return html;
     }
     return `<h4 class="my-3 text-center">Sem Proteção adicional</h4><div class="table-footer" style="height: 41px"></div>`;

}

function paginacao(pagination) {
    if(pagination.next || pagination.prev) {
        let template = `<ul class="pagination mt-3 float-right">
            <li class="page-item ${pagination.prev ? '': 'disabled'}"><a class="page-link page-prev" href="javascript:void(0)" tabindex="-1" aria-disabled="${pagination.prev}">Anterior</a></li>`;
        for(let i =1; i <= pagination.qtd_pages; i++) {
            template+=`<li class="page-item ${pagination.current === i ? 'active' : ''}"><a class="page-link page-number" href="javascript:void(0)" data-page="${i}">${i}</a></li>`;
        }
        template+=` <li class="page-item ${pagination.next ? '': 'disabled'}"><a class="page-link page-next" href="javascript:void(0)" tabindex="-1" aria-disabled="${pagination.next}">Próximo</a></li></ul>`;
        return template;
    }
    return '';
}