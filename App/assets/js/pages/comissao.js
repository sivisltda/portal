import { getConfig as configFunc } from './../config/config.js';
import {numberFormat, textToNumber, convertDataBr, formataNumero } from './../core/function.js';

const config = configFunc();

let tipoComissao = [];

const tblFuncionarios = $('#tbFuncionarios').dataTable({
    "iDisplayLength": 10,
    "aLengthMenu": [20, 30, 40, 50, 100],
    "bProcessing": true,
    "bServerSide": true,
    "bLengthChange": false,
    "sAjaxSource": finalFind(),
    "bFilter": false,
    "ordering": false,
    "bAutoWidth": false,
    "columnDefs": [
        { "responsivePriority": 1, "targets": 0 },
        { "responsivePriority": 2, "targets": 2 },
        { "responsivePriority": 3, "targets": -2 },
        { "responsivePriority": 4, "targets": -1 }
    ],
    "aoColumns": [
        {"className": "centerTable", "sWidth": '2%', render: function(data, type, row) {
            return ``;
        }},
        {"className": "centerTable", "sWidth": '3%', data: "parcela"},
        {"className": "centerTable", "sWidth": '30%',  render: function (data, type, row) {
            return nomeResumido(row.nome);
        }},
        {"className": "centerTable", "sWidth": '15%', data: "data_venda"},
        {"className": "centerTable", "sWidth": '15%', data: "produto"},
        {"className": "centerTable column-success", "sWidth": '15%', data: "preco_plano"},
        {"className": "centerTable column-warning", "sWidth": '15%', data: "preco_servico"},
        {"className": "centerTable column-info", "sWidth": '15%', data: "preco_produto"},
        {"className": "centerTable column-success", "sWidth": '25%', data: "comissao_plano_pri"},
        {"className": "centerTable column-success", "sWidth": '15%', data: "comissao_plano"},
        {"className": "centerTable column-warning", "sWidth": '15%', data: "comissao_servico"},
        {"className": "centerTable column-info", "sWidth": '15%', data: "comissao_produto"},
        {"className": "centerTable column-danger", "sWidth": '10%', data: "valor_comissao"},
        {"className": "centerTable", "sWidth": '15%', "render": function (data, type, row) {
            const colors = {aprovado: '#0acf97', reprovado: '#fa5c7c', aguarda: '#ffbc00', pendente: '#6c757d' };
            return `<a class="badge link-status" style="background-color: ${colors[row.status.toLowerCase()]}"
            data-toggle="modal" data-target="#modalDetailsComissao" data-item='${JSON.stringify(row)}'>${row.status}</a>`;
        }}
    ],
    "oLanguage": {
        'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"},
    "sPaginationType": "full_numbers",
    drawCallback: function (settings, data) {
        const valorReceber = settings.json.valoresReceber;
        $('#quantidade_total').html(settings.json.iTotalRecords);
        $('#valor_comissao').html('R$ '+numberFormat(parseFloat(settings.json.valoresTotal)));
        $('#valor_reprovado').html('R$ '+numberFormat(parseFloat(valorReceber.reprovado)));
        $('#nome-usuario').text(settings.json.usuario.razao_social);
        valoresReceber(valorReceber);
        porcentagemReceber(valorReceber);
        listaCounter(settings.json.comissoes);
        tabelaPlanos(settings.json.planos);
        $('.boxLoader').hide();
    }
});

$("#btnfind").click(function () {
    tblFuncionarios.fnReloadAjax(finalFind());
});

$(document).on('click', '#btnLink', function(event) {
    event.preventDefault();
    $('#txtLink').select();
    $(this).tooltip({
        placement: 'top',
        title: 'Copiado'
    });
    document.execCommand("copy");
});

$('#list-counter').on('click', '.counter', function(event) {
    event.preventDefault();
    const t = $(this);
    const id = t.data('id');
    if (id != 'comissao') {
        const index = tipoComissao.indexOf(id);
        if(index > -1) {
            tipoComissao.splice(index, 1);
            t.removeClass('active');
        } else {
            tipoComissao.push(id);
            t.addClass('active');
        }
        $('#btnfind').click();
    }
});

$('#modalDetailsComissao').on('show.bs.modal', function(e) {
    const button = $(e.relatedTarget);
    const item = button.data('item');
    const comissao = [{ label: 'Plano:', value: item.produto }, { label: 'Valor:', value: item.valor_comissao }, { label: 'Nome:', value: item.nome }, { label: 'Data de Venda:', value: item.data_venda },
                    { label: 'Status:', value: item.status }, { label: 'Data de Lançamento:', value: item.data_lanc },  { label: `Data de ${item.status}:`, value: item.data_aprov }];
    const template = comissao.map(function(c) {
        if(c.value) {
            return `<p><strong>${c.label}</strong> ${c.value}</p>`;
        }
        return '';
    }).join(''); 
    $('#detalhes').html(template);               
});

$('.auto-date').click(function(e) {
    $('#rel-date-from').val($(this).data('from'));
    $('#rel-date-to').val($(this).data('to'));
    $('#btnfind').click();
});

function finalFind() {
    const inicio = $("#rel-date-from").val();
    const fim    = $("#rel-date-to").val();
    $('.boxLoader').show();
    let sData = "";
    if (inicio !== "" && fim !== "") {
        sData = '&inicio=' + convertDataBr(inicio) + '&fim=' + convertDataBr(fim);
    }
    if (tipoComissao.length) {
        sData+= '&tipoComissao='+tipoComissao.join(',');
    }
    return config.api+'dashboard/funcionario?functionPage=GetComissaoFuncionario'+sData;
}

function listaCounter(items) {
    const isTodas = !tipoComissao.length;
    const template = items.map(function(item) {
        return `
            <a href="javascript:void(0)" class="counter ${item.id}${tipoComissao.includes(item.id) 
                || isTodas || item.id === 'comissao' ? ' active' : ''}" 
                data-id="${item.id}">
                <div class="descricao">${item.status}</div>
                <div class="qtd-counter">${item.quantidade}</div>
                <div class="valor-counter">${formataNumero(item.valor)}</div>
            </a> 
        `;
    }).join('');
    
    $('#list-counter').html(template);
}

function valoresReceber(valorReceber) {
    $('#recebido').html(numberFormat(parseFloat(valorReceber["recibido"]), 1));
    $('#receber').html(numberFormat(parseFloat(valorReceber["receber"]), 1));
}

function porcentagemReceber(per) {
    const cemPer = parseFloat(per["recibido"]) + parseFloat(per["receber"]);
    const per1 = parseFloat(per["recibido"]) * 100 / cemPer;
    const per2 = 100 - per1;
    $('#per-1').html(numberFormat(per1) + "%");
    $('#per-2').html(numberFormat(per2) + "%");
    document.getElementById("progressBar").style.width = per1 + "%";
}


function tabelaPlanos(planos) {
    let totalValor = 0;
    let totalQtd = 0;
    let comissaoPlano = 0;
    let comissaoPlanoPri = 0;
    let comissaoProduto = 0;
    let comissaoServico = 0;
    const trPlano = planos.map(function(plano) {
        totalValor+= textToNumber(plano.comissao);
        totalQtd+= textToNumber(plano.quantidade);
        comissaoPlanoPri+= textToNumber(plano.comissao_plano_pri);
        comissaoPlano+= textToNumber(plano.comissao_plano);
        comissaoProduto+= textToNumber(plano.comissao_produto);
        comissaoServico+= textToNumber(plano.comissao_servico);
        return `<tr>
            <td>${plano.descricao}</td>
            <td><span class="badge bg-secundaria text-white" id="ingressos-online-quant">${plano.quantidade}</span></td>
            <td>${plano.comissao_plano_pri}</td>
            <td>${plano.comissao_plano}</td>
            <td>${plano.comissao_produto}</td>
            <td>${plano.comissao_servico}</td>
            <td id="ingressos-online-vendas-total">${plano.comissao}</td>
        </tr>`;
    }).join('');
    $('#tblComissao tbody').html(trPlano);
    $('#tblComissao tfoot').html(`<tr>
        <td><strong>Total</strong></td>
        <td><span class="badge bg-secundaria text-white" id="ingressos-todos-quant">${totalQtd}</span></td>
        <td><strong>${numberFormat(comissaoPlanoPri, 1)}</strong></td>
        <td><strong>${numberFormat(comissaoPlano, 1)}</strong></td>
        <td><strong>${numberFormat(comissaoProduto, 1)}</strong></td>
        <td><strong>${numberFormat(comissaoServico, 1)}</strong></td>
        <td id="ingressos-todos-vendas-total" class="font-weight-bold">${numberFormat(totalValor, 1)}</td>
    </tr>`);
}


function nomeResumido(nome) {
    return nome.toUpperCase();
}