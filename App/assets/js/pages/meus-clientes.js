import { getConfig as configFunc } from './../config/config.js';
import {criarDataTable, convertDataBr, convertSerializeObject, errorCamposInvalidos, slug} from './../core/function.js';
import {validate, validate_email} from './../core/validator.js';
import {createInput} from './../core/form.js';

import * as loading from './../core/loading.js';
import * as funcionarioService from './../service/funcionario.service.js';

const config = configFunc();
const boxLoader = $('.boxLoader');
const txtBusca = $('#busca');
const txtFrom = $('#rel-date-from');
const txtTo = $('#rel-date-to');
const txtProcedencia = $('#procedencia');
const btnBusca = $('#btnBusca');
const tblProspects = $('#tblProspect');
const listCounter = $('#list-counter');
const modalAux = $('#modal-aux');

let status = [];

const valoresInput = {
    email: '',
};
const forms = {
    inputs: [
        {
            type: 'text', size: 'col-sm-9', id: 'razao_social', label: 'Nome:',
            name: 'razao_social', required: true, visibility: true,
            attributes: {
                class: 'form-control',
                placeholder: 'Seu nome'
            }
        }, {
            type: 'email', size: 'col-sm-9', id: 'email', label: 'E-mail:',
            name: 'email', required: true, visibility: true,
            attributes: {
                class: 'form-control',
                placeholder: 'nome@dominio.com'
            }
        }, {
            type: 'tel', size: 'col-sm-9', id: 'celular', label: 'Celular:',
            name: 'celular', required: true, visibility: true,
            attributes: {
                class: 'form-control',
                placeholder: '(xx) xxxxx-xxxx',
                "data-toggle": "input-mask",
                "data-mask-format": "(99) 99999-9999"
            }
        }
    ],
    rules: {
        razao_social: {
            rule: "required|min:2",
            message: {required: "Nome é um campo obrigatório", min: "mínimo :min caracteres"}
        }, email: {
            rule: "required|email",
            message: {
                required: "Email é um campo obrigatório",
                email: "Campo deve ser do tipo email"
            }
        }, celular: {
            rule: "required",
            message: {
                required: "Telefone é um campo obrigatório"
            }
        }
    }
};

const dadosLead = {
    dt_cadastro: 'Data de Cadastro',
    razao_social: 'Nome',
    email: 'E-mail',
    telefone: 'Celular',
    tipo: 'Tipo',
    nome_procedencia: 'Procedência',
    indicador_lead: 'Indicador',
    status: 'Status',
    assunto: ''
};

$(document).ready(function (e) {
    loading.subscribe(function (numero) {
        !!numero ? boxLoader.show() : boxLoader.hide();
    });
    txtProcedencia.select2();
    const columns = {
        "columnDefs": [
            {"responsivePriority": 1, "targets": 0},
            {"responsivePriority": 2, "targets": 2},
            {"responsivePriority": 3, "targets": -1},
            {"responsivePriority": 4, "targets": 1},
        ],
        "aoColumns": [
            {"className": "centerTable", "sWidth": '5%', render: function (data, type, row) {
                    return `<span class="bagde ${row.status.toLowerCase()}"></span>`;
                }},
            {"className": "centerTable", "sWidth": '15%', data: "dt_cadastro"},
            {"className": "centerTable", "sWidth": '20%', render: function (data, type, row) {
                    return `<span class="nome_reduzido">${row.razao_social}</span>`;
                }},
            {"className": "centerTable", "sWidth": '10%', data: "tipo"},
            {"className": "centerTable", "sWidth": '20%', data: "telefone"},
            {"className": "centerTable", "sWidth": '15%', data: "email"},
            {"className": "centerTable", "sWidth": '20%', data: "nome_procedencia"},
            {"className": "centerTable", "sWidth": '20%', data: "nome_indicador"},
            {"className": "centerTable", "sWidth": '15%', render: function (data, type, row) {
                    return `<span class="status ${slug(row.status)}">${row.status}</span>`;
                }},
            {"className": "centerTable", "sWidth": '15%', render: function (data, type, row) {
                    return `<div class="dropdown">
        <a class="btn btn-sm btn-secundaria dropdown-toggle" href="javascript:void(0)" 
          role="button" id="dropdown-${row.tipo}-${row.id}"
          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         Ação
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdown-${row.tipo}-${row.id}">
          <a class="dropdown-item" data-type="detail" data-item='${JSON.stringify(row)}' 
          data-toggle="modal" data-title="Detalhes" href="#modal-aux">Detalhes</a>
          ${row.tipo === 'lead' ? '<a class="dropdown-item"' + ' data-item=\'' + JSON.stringify(row) + '\''
                            + ' data-toggle="modal" data-type="covert" data-title="Coverter para Prospect" href="#modal-aux">Converte Prospect</a>' : ''}
          ${row.status !== 'Inativo' ? '<a class="dropdown-item btn-imprimir" data-id="' + row.id + '" ' +
                            ' href="javascript:void(0)">Imprimir Cotação</a>' : ''}
          ${row.status !== 'Inativo' ? '<a class="dropdown-item btn-inativar" data-tipo="' + row.tipo + '" data-nome="' + row.razao_social + '"'
                            + 'data-id="' + row.id + '" href="javascript:void(0)">Inativar</a>' : ''}
        </div>
      </div>`;
                }}
        ]
    };

    tblProspects.dataTable(criarDataTable(10, columns, finalFind(), function (settings, data) {
        boxLoader.hide();
        listaCounter(settings.json.status);
    }));

    modalAux.on('show.bs.modal', function (event) {
        const button = $(event.relatedTarget);
        const title = button.data('title');
        const type = button.data('type');
        const item = button.data('item');
        if (type === 'detail') {
            const html = Object.keys(dadosLead).map(function (key) {
                if (item[key]) {
                    return `<p class="mb-0">${dadosLead[key].length > 0 ? "<strong>" + dadosLead[key] + "</strong>: " : ""}${item[key]}</p>`;
                }
                return false;
            }).filter(Boolean).join('');
            const htmlDependente = montaListaDependente(item.dependentes);
            modalAux.find('.modal-title').text('Detalhes do ' + item.razao_social);
            modalAux.find('.modal-body').html(`<div class="mt-2">${html}${htmlDependente}</div>`);
            modalAux.find('.modal-footer').html(`<button type="button" class="btn btn-secundaria" data-dismiss="modal">Fechar</button>`);
        } else {
            const htmlForm = createInput(forms.inputs, 'row', 'col-sm-3');
            modalAux.find('.modal-title').text('Convertendo pra Prospect o(a) Lead ' + item.razao_social);
            modalAux.find('.modal-body').html(
            `<form id="form-prospect">${htmlForm}
                <input type="hidden" name="lead" value='${JSON.stringify(item)}'/>
            </form>`);
            modalAux.find('.modal-footer').html(`
        <button type="button" class="btn btn-secundaria" data-dismiss="modal">Fechar</button>
        <button id="btn-convert-prospect" type="button" class="btn btn-primaria">Salvar</button>
      `);
            const form = modalAux.find('#form-prospect');
            form.find('[name="razao_social"]').val(item.razao_social);
            form.find('[name="email"]').val(item.email);
            form.find('[name="celular"]').val(item.telefone);
        }
    });

    modalAux.on('focusin', '[data-toggle=input-mask]', function (e) {
        e.preventDefault();
        const t = $(this);
        t.mask(t.data('mask-format'));
    });

    modalAux.on('blur', '[name="email"]', function (e) {
        e.preventDefault();
        const t = $(this);
        const valor = t.val();
        console.log(valor);
        t.removeClass('is-invalid').text('');
        if (valor === "") {
            t.removeClass('is-valid').val('').addClass('is-invalid').focus()
                    .siblings('.invalid-feedback').text('E-mail obrigatório');
            return false;
        }
        if (!validate_email(valor)) {
            t.removeClass('is-valid').addClass('is-invalid').focus()
                    .siblings('.invalid-feedback').text('E-mail inválido');
            return false;
        }
        if (valoresInput.email !== valor) {
            valoresInput.email = valor;
            funcionarioService.verificaEmail(t.val(), function () {
                t.closest('.form-group').next().find(['name="celular"']).focus();
            }, function (erro) {
                if (erro.status === 400) {
                    t.removeClass('is-valid').val('').addClass('is-invalid').focus()
                            .siblings('.invalid-feedback').text('E-mail já utilizado');
                    valoresInput.email = '';
                    return false;
                } else {
                    swal('Erro', 'Ocorreu um erro ao validar o e-mail');
                }
            });
        }
    });

    modalAux.on('click', '#btn-convert-prospect', function (e) {
        e.preventDefault();
        const t = $(this);
        const form = modalAux.find('#form-prospect');
        const validations = validate(forms.rules, form);
        if (validations.length) {
            const errors = validations.reduce(function (acc, i) {
                acc[i.field] = i.message;
                return acc;
            }, {});
            errorCamposInvalidos(form, errors);
            swal('Atenção!', 'Informe os campos obrigatórios', 'warning');
            return false;
        }
        const dataForm = convertSerializeObject(form.serialize());
        const dataLead = JSON.parse(dataForm.lead);
        const data = {
            razao_social: dataForm.razao_social,
            email: dataForm.email,
            celular: dataForm.celular,
            procedencia: dataLead.id_procedencia,
            id_lead: dataLead.id,
            id_lead_indicador: dataLead.indicador_lead
        };
        funcionarioService.convertLeadProspect(data, function (idProspect) {
            modalAux.modal('hide');
            swal({
                type: 'question',
                title: 'Convertido com sucesso',
                html: `<p>O lead <strong>${data.razao_social}</strong> foi convertido com sucesso para Prospect!</p>
        <p>Você deseja continuar com seu processo de cotação agora?</p>`,
                showCancelButton: true,
                confirmButtonText: 'Sim', cancelButtonText: 'Não'
            }).then(function (res) {
                if (res.value) {
                    window.location.href = config.api
                            .replace('/api/', '/cliente/prospect/' + idProspect);
                } else {
                    tblProspects.fnReloadAjax(finalFind());
                }
            });

        }, function (erro) {
            console.log(erro);
            if (erro.status === 422) {
                const errors = erro.erro.responseJSON;
                errorCamposInvalidos(form, errors);
            }
            swal('Erro', 'Ocorreu um erro ao converter o Lead para Prospect', 'error');
        });
    });

    btnBusca.on('click', function (event) {
        event.preventDefault();
        tblProspects.fnReloadAjax(finalFind());
    });

    txtBusca.on('keypress', function (event) {
        if (event.keyCode === 13) {
            btnBusca.click();
        }
    });

    listCounter.on('click', '.counter', function (event) {
        event.preventDefault();
        const t = $(this);
        const id = t.data('id');
        if (id != 'todos') {
            const index = status.indexOf(id);
            if (index > -1) {
                status.splice(index, 1);
                t.removeClass('active');
            } else {
                status.push(id);
                t.addClass('active');
            }
            btnBusca.click();
        }
    });

    tblProspects.on('click', '.btn-inativar', function (e) {
        e.preventDefault();
        const t = $(this);
        const id = t.data('id');
        const nome = t.data('nome');
        const tipo = t.data('tipo');
        const descTipo = tipo === 'lead' ? 'Lead' : 'Prospect';
        console.log(t, id, nome, tipo);

        swal({
            type: 'question',
            title: 'Deseja inativar',
            html: `<p>Deseja inativar o ${descTipo} <strong>${nome}</strong>?</p>`,
            showCancelButton: true,
            confirmButtonText: 'Sim', cancelButtonText: 'Não'
        }).then(function (res) {
            if (res.value) {
                funcionarioService.inativarUsuario({id, tipo}, function () {
                    swal('Sucesso', descTipo + ' inativado com sucesso!', 'success').then(function () {
                        tblProspects.fnReloadAjax(finalFind());
                    });
                }, function (erro) {
                    console.log(erro);
                    swal('Erro', 'Ocorreu um erro ao inativar o ' + descTipo, 'error');
                });
            }
        });
    });

    tblProspects.on('click', '.btn-imprimir', function (e) {
        e.preventDefault();
        const t = $(this);
        const id = t.data('id');
        window.open(`${config.api}dashboard/pendencia?functionPage=imprimirLote&id=${id}`,'_blank');
    });

    $('.auto-date').click(function (event) {
        event.preventDefault();
        txtFrom.val($(this).data('from'));
        txtTo.val($(this).data('to'));
        btnBusca.click();
    });

});

function montaListaDependente(items) {
    let template = ``;
    if (items.length > 0) {
        template += `<table class="table table-striped dt-responsive nowrap w-100 dataTable no-footer dtr-inline collapsed">
        <thead class="bg-secundaria text-white">
            <tr role="row">
                <th class="sorting_disabled centerTable" rowspan="1" colspan="1" style="width: 20%;">ID</th>
                <th class="sorting_disabled centerTable" rowspan="1" colspan="1" style="width: 80%;">Dependente</th>
            </tr>
        </thead>`;
        template += items.map(function (item) {
            return `<tr><td>${item.id_fornecedores_despesas}</td><td>${item.razao_social}</td></tr>`;
        }).join('');
        template += `</table>`;
    }
    console.log(template);
    return template;
}

function listaCounter(items) {
    const isTodas = !status.length;
    const template = items.map(function (item) {
        return `
          <a href="javascript:void(0)" class="counter ${item.id}${status.includes(item.id)
                || isTodas || item.id === 'todos' ? ' active' : ''}" 
              data-id="${item.id}">
              <div class="descricao">${item.status}</div>
              <div class="qtd-counter">${item.qtd}</div>
          </a> 
      `;
    }).join('');

    listCounter.html(template);
}

function finalFind() {
    boxLoader.show();
    const inicio = txtFrom.val();
    const fim = txtTo.val();
    let sData = "";
    if (inicio !== "" && fim !== "") {
        sData = '&inicio=' + convertDataBr(inicio) + '&fim=' + convertDataBr(fim);
    }
    if (txtProcedencia.val()) {
        sData += '&procedencia=' + txtProcedencia.val();
    }
    if (status.length) {
        sData += '&status=' + status.join(',');
    }
    if (txtBusca.val()) {
        sData += '&busca=' + txtBusca.val();
    }
    return config.api + 'dashboard/funcionario?functionPage=getProspectLead' + sData;
}