import * as pendenciaService from "./../service/pendencia.service.js";
import * as loading from './../core/loading.js';
import { makeModal } from "./../shared/pendencia.js";
import { chamaSwalQueue } from "./../core/auth.js";
import { textToNumber, numberFormat, maskMoney, convertSerializeObject, convertDataBr } from './../core/function.js';
import {getConfig as configFunc} from './../config/config.js';

const listaPendencia = $('#lista-pendencia');
const dtInicio = $('#rel-date-from');
const dtFim = $('#rel-date-to');
const boxLoader = $('.boxLoader');
const autoDate = $('.auto-date');
const busca = $('#busca');
const btnAssinatura = $('#btnAssinatura');
const btnBusca = $('#btnBusca');
const modalDetails = $('#details');
const funcionario = $('#funcionario');
const nomeCliente = $('#nomeCliente');
const idCliente = $('#idCliente');

const config = configFunc();
const sourceListaClientes = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: `${config.api}dashboard/pendencia?functionPage=listaClientes`,
    remote: {
        url: `${config.api}dashboard/pendencia?functionPage=listaClientes&q=%QUERY`,
        wildcard: '%QUERY'
    }
});

$(document).ready(function () {
    loading.subscribe(function (numero) {
        !!numero ? boxLoader.show() : boxLoader.hide();
    });

    autoDate.click(function (e) {
        e.preventDefault();
        dtInicio.val($(this).data('from'));
        dtFim.val($(this).data('to'));
        initGetListaPendencias();
    });

    if (funcionario.length) {
        funcionario.select2();
        funcionario.on('change', function () {
            initGetListaPendencias();
        });
    }

    btnAssinatura.on('click', function (event) {
        event.preventDefault();
        assinarContratos();
    });
    
    btnBusca.on('click', function (event) {
        event.preventDefault();
        initGetListaPendencias();
    });

    modalDetails.on('show.bs.modal', function (event) {
        const button = $(event.relatedTarget);
        const modal = $(this);
        const item = button.data('item');
        const tipo = button.data('tipo');
        makeModal(modal, item, tipo);
    });
    
    nomeCliente.typeahead({
        classNames: {
          input: 'w-100'
        },
        hint: true,
        highlight: true,
        minLength: 1
    },  {
        name: 'cliente',
        display: 'razao_social',
        source: sourceListaClientes,
        templates: {
            header: '<h3 class="titulo-autocomplete">Clientes / Prospects</h3>',
            empty: [
                '<div class="empty-message">',
                'Nenhum cliente encontrado com esse nome',
                '</div>'
            ].join('\n'),
            limit: 10,
            suggestion: function(data) {
                return `<div class="box-suggestion"><strong>${data.razao_social}</strong>
                (${data.tipo === "P" ? "Prospect" : "Cliente"})</div>`
            }
        }
    });

    nomeCliente.bind('typeahead:select', function(e, suggestion) {
        e.preventDefault();
        idCliente.val(suggestion.id);
    });

    initGetListaPendencias();
});

function assinarContratos() {
    if ($.isNumeric(idCliente.val()) && nomeCliente.val().length > 0) {
        pendenciaService.getListaPendencias({
            inicio: convertDataBr(dtInicio.val()),
            fim: convertDataBr(dtFim.val()),
            busca: busca.val(),
            funcionario_id: funcionario.length ? funcionario.val() : null,
            cliente_id: nomeCliente.val().length > 0 ? idCliente.val() : null
        }, function (res) {
            if (res.length) {
                refreshTable(res);
                chamaSwalQueue(escolhendoPendencias(res), function (resp) {}); 
            }
        }, function (erro) {
            console.log(erro);
            swal('Erro', 'Ocorreu um erro ao buscar a lista de dependencias', 'error');
        });
    } else {
        swal('Erro', 'Selecione um cliente para efetuar essa operação!', 'error');
    }
}

function escolhendoPendencias(veiculos, opcoes = {}, callback = null) {
    return {
        title: 'Assinatura Agrupada',
        html: `<div>
                    <p>Selecione os veículo(s) para assinatura de contrato:</p>
                    <ul id="sel-veiculos">${templateItemPendencias('veiculo', veiculos, 'lista-veiculo')}</ul>
                </div>`,
        confirmButtonText: 'Assinar',
        cancelButtonText: `Sair`,
        showLoaderOnConfirm: true, ...opcoes,
        preConfirm: function () {
            return new Promise(function (resolve) {
                const checkVeiculo = $('#sel-veiculos').find('input[type="checkbox"]:checked').first();
                if (checkVeiculo.length) {
                    console.log(config);
                    window.open(`${config.urlErp}Modulos/Seguro/FormAssinatura.php?id=${$('#sel-veiculos').find('input[type="checkbox"]:checked').map(function() {
                        return this.value;
                    }).get().join(",")}&idContrato=${$("#idPortalConstrato").val()}&crt=${config.contrato}&idCliente=${$("#idCliente").val()}`,'_blank');
                    resolve(true);
                    callback && callback();
                } else {
                    throw new Error('Selecione um veículo');
                }
            }).catch(function (erro) {
                console.log(erro);
                Swal.showValidationError('Selecione um veículo');
                callback && callback();
            });
        }
    };
}

function templateItemPendencias(name, veiculos, parent) {
    return veiculos.map(function (p) {
        return `<li>
              <div class="row justify-content-between">
                  <div class="col-8 col-sm-9 text-left">
                      ${"<h6>" + (p.placa.length > 0 ? p.placa : "Zero Km") + " - " + p.veiculo + "</h6>"}
                  </div>
                  <div class="col-4 col-sm-3 action-service">
                      <input type="checkbox" id="${name}-${p.id_plano}" checked 
                      name="${name}[]" value="${p.id_plano}" data-type="multiple" data-parent="${parent}" data-switch="bool">
                      <label for="${name}-${p.id_plano}" data-on-label="sim" data-off-label="não"></label>
                  </div>
              </div>
          </li>`;
    }).join('');
}

function refreshTable(res) {
    listaPendencia.html(res.map(function (item) {
        return templateItemPendencia(item);
    }).join(''));            
    listaPendencia.find('.btn-finalizar').on('click', function (e) {
        e.preventDefault();
        const item = $(this).data('item');
        swal({
            title: 'Atenção',
            text: `Você deseja finalizar a pendência do(a) cliente ${item.razao_social}?`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não'
        }).then(function (res) {
            if (res.value) {
                pendenciaService.finalizarAdesao({id_plano: item.id_plano}, function () {
                    swal('Sucesso', 'Pendência finalizada com sucesso', 'success').then(function () {
                        initGetListaPendencias();
                    });
                }, function (erro) {
                    console.log(erro);
                    swal('Erro', 'Ocorreu um erro ao finalizar a adesão', 'error');
                });
            }
        });
    });
    listaPendencia.find('.btn-cancelar').on('click', function (e) {
        e.preventDefault();
        const item = $(this).data('item');
        swal({
            title: 'Atenção',
            text: `Confirma o cancelamento deste veículo?`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não'
        }).then(function (res) {
            if (res.value) {
                pendenciaService.cancelarAdesao({id_plano: item.id_plano}, function () {
                    swal('Sucesso', 'Pendência cancelada com sucesso', 'success').then(function () {
                        initGetListaPendencias();
                    });
                }, function (erro) {
                    console.log(erro);
                    swal('Erro', 'Ocorreu um erro ao cancelar', 'error');
                });
            }
        });
    });
}

function initGetListaPendencias() {
    pendenciaService.getListaPendencias({
        inicio: convertDataBr(dtInicio.val()),
        fim: convertDataBr(dtFim.val()),
        busca: busca.val(),
        funcionario_id: funcionario.length ? funcionario.val() : null,
        cliente_id: nomeCliente.val().length > 0 ? idCliente.val() : null
    }, function (res) {
        if (res.length) {                                               
            refreshTable(res);
        } else {
            listaPendencia.html(templateError());
        }
    }, function (erro) {
        console.log(erro);
        swal('Erro', 'Ocorreu um erro ao buscar a lista de dependencias', 'error');
    });
}

function templateItemPendencia(item) {
    const active = item.complete && item.contrato && item.vistoria && item.habilitacao && item.residencia;
    return `<div class="col-md-6 col-lg-4">
    <div class="card">
      <div class="card-body">
        <div class="box-header">
          <div class="car-placa">${item.placa}</div>
          <div class="car-ano">${item.ano_fabricacao} / ${item.ano_modelo.includes('32000') ? 'Zero km' : item.ano_modelo}</div>
        </div>
        <div class="box-info-cliente">
          <div class="name-cliente">${item.razao_social}</div>
          <div class="info-veiculo">${item.veiculo}</div>
        </div>
        <div class="box-steps-pendencias line">
          <ul>${listaStatus(statusObrigatorios(item), item)}</ul>
        </div>
        <div class="box-steps-pendencias line">
          <ul>${listaStatus(statusOpcionais(item), item)}</ul>
        </div>    
      </div>
      <div class="container row">
        <button class="btn btn-block btn-secundaria col-md-8 btn-finalizar" data-item='${JSON.stringify(item)}' ${!active ? 'disabled' : ''}>Finalizar Adesão</button>
        <button class="btn btn-link col-md-4 btn-cancelar" data-item='${JSON.stringify(item)}'>Cancelar</button>
      </div>
    </div>
  </div>`;
}

function statusObrigatorios(item) {
    return {
        complete: {
            text: 'Preenchimentos dos dados',
            modal: false
        },
        descontos: {
            text: 'Aprovação dos descontos',
            modal: true
        },
        contrato: {
            text: 'Assinatura do Contrato',
            modal: !!item.contrato_url.length
        },
        habilitacao: {
            text: 'Comprovante de Habilitação',
            modal: true
        },
        residencia: {
            text: 'Comprovante de Residência',
            modal: true
        },        
        vistoria: {
            text: 'Vistoria Finalizada',
            modal: true
        }
    };
}

function statusOpcionais(item) {
    return {
        documento: {
            text: 'Documento do Veículo',
            modal: true
        },         
        pagamento: {
            text: 'Comprovante de Pagamento',
            modal: true
        }
    };
}

function listaStatus(status, item) {
    return Object.keys(status).map(function (keyStatus) {
        const itemStatus = status[keyStatus];
        const isActive = item[keyStatus];
        return (isActive === 2 ? `` : `<li>
        <div class="name-pendencia">
            <span>${itemStatus.text}</span>
            <div class="line-reticencias"></div>
        </div>
        <div class="box-status-pendencia">
            <a class="link-icon-pendencia" ${itemStatus.modal ? `data-toggle="modal" href="#details" data-backdrop="static"
              data-item='${JSON.stringify(item)}' data-tipo="${keyStatus}"` : 'href="javascript:void(0)"'} >
              ${isActive ? '<i class="mdi mdi-checkbox-marked-circle-outline text-success"></i>' : '<i class="mdi mdi-alert text-warning"></i>'}
            </a>
        </div></li>`);
    }).join('');
}

function templateError() {
    return `<div class="alert alert-warning text-center m-auto" role="alert">
  <h5 class="alert-heading">Atenção</h5>
  <p class="text-center">Nenhuma pendência foi encontrada com as opções do filtro acima</p>
  </div>`;
}