import { getConfig as configFunc } from './../config/config.js';
import * as gestorService from './../service/gestor.service.js';
import {textToNumber, convertDataBr} from './../core/function.js';
import * as pagamentoService from './../service/pagamento.service.js';
import { templateCarteiras } from './../shared/carteira.js';

const boxLoader     = $('.boxLoader');
const tbClientes    = $('#tbClientes');
const dateFrom      = $('#rel-date-from');
const dateTo        = $('#rel-date-to');
const txtBusca      = $('#txtBusca');
const btnFind       = $('#btnfind');
const tipoPagamento = $('#tipo_pagamento');
const tipoPlano     = $('#tipo_plano');
const selConsultor  = $('#consultor');
const autoDate      = $('.auto-date');

const modalCliente  = $('#modalCliente');
const modalDetails  = $('#modalDetails');
const modalDependente = $('#modalDetailsDependentes');

const config = configFunc();
const colors = {ativo: '#0acf97', suspenso: '#39afd1', inativo: '#fa5c7c', prospect: '#ffbc00', novo: '#8e44ad', dependente: '#6c757d', emaberto: '#8b0000' };

$(document).ready(function() {
    $('.select2').select2();
    tbClientes.dataTable({
        "iDisplayLength": 15,
        "aLengthMenu": [20, 30, 40, 50, 100],
        "bProcessing": true,
        "bServerSide": true,
        "bLengthChange": false,
        "sAjaxSource": finalFind(),
        "bFilter": false,
        "ordering": false,
        "bAutoWidth": false,
        "aoColumns": [
            {"className": "centerTable", "sWidth": '5%', "render": function (data, type, row) {
                return `<span class="status" style="background-color: ${colors[row.status.toLowerCase()]} " data-toggle="tooltip" data-placement="right" title="${row.status}"></span>`;
            }},
            {"className": "centerTable", "sWidth": '5%', data: "id"},
            {"className": "centerTable", "sWidth": '10%', data: "nome"},
            {"className": "centerTable", "sWidth": '5%', data: "cnpj"},
            {"className": "centerTable", "sWidth": '10%', data: "email"},
            {"className": "centerTable", "sWidth": '10%', data: "nome_plano"},
            {"className": "centerTable", "sWidth": '10%', data: "situacao"},
            {"className": "centerTable", "sWidth": '15%', data: "nome_responsavel"},
            {"className": "centerTable", "sWidth": '15%', data: "nome_funcionario"},
            {"className": "centerTable", "sWidth": '5%', data: "dependentes"},
            {"className": "centerTable", "sWidth": '5%', data: "tipo_pagamento"},
            {"className": "centerTable", "sWidth": '10%', data: "cidade"},
            {"className": "centerTable", "sWidth": '10%', "render": function (data, type, row) {
                return `<button class="btn btn-secundaria btn-sm" data-toggle="modal" data-target="#modalCliente" data-id="${row.id}">detalhes</button>`;
            }}
        ],
        "oLanguage": {
            'oPaginate': {
                'sFirst': "Primeiro",
                'sLast': "Último",
                'sNext': "Próximo",
                'sPrevious': "Anterior"
            }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sSearch': "Pesquisar:",
            'sZeroRecords': "Não foi encontrado nenhum resultado"},
        "sPaginationType": "full_numbers",
        drawCallback: function (settings, data) {
            montarFiltroStatus(settings.json.status);
            boxLoader.hide();
        }
    });
    
    txtBusca.on('keypress', function(event) {
        if (event.keyCode === 13) {
            btnFind.click();
        }
    });
        
    autoDate.click(function(e) {
        dateFrom.val($(this).data('from'));
        dateTo.val($(this).data('to'));
        btnFind.click();
    });
    
    btnFind.click(function () {
        tbClientes.fnReloadAjax(finalFind());
    });
    
    modalCliente.on('show.bs.modal', function(e) { 
        const button = $(e.relatedTarget);
        const id = button.data('id');
        modalCliente.find('#headingMensalidade, #headingDependentes').hide();
        gestorService.getCliente(id, function(response){
            modalCliente.find('#nome-cliente').text(`${response.id} - ${response.nome}`);
            modalCliente.find('#info-pessoal').html(infoPessoalCliente(response));
            modalCliente.find('#info-endereco').html(infoEnderecoCliente(response));
            modalCliente.find('#plano').html(infoPlanoCliente(response));
            if(response.mensalidades.length) {
                modalCliente.find('#headingMensalidade').show();
                modalCliente.find('#table-mensalidade tbody').html(dadosMensalidadesCliente(response));
            }
            if(response.dependentes.length) {
                modalCliente.find('#headingDependentes').show();
                modalCliente.find('#table-dependentes tbody').html(dadosDependentesCliente(response.dependentes));
            }
           // console.log(tableMensaldiade);
        }, function(erro) {
            console.log(erro.responseText);
            swal('Erro', 'Ocorreu um erro ao carregar o cliente', 'error');
        });
    });
    
    modalDetails.on('hidden.bs.modal', function (e) {
        $('body').addClass('modal-open').css('padding-right','17px');
    });
    
    modalDetails.on('show.bs.modal', function(e) {
        const button = $(e.relatedTarget);
        const mensalidade = button.data('mensalidade');
        modalDetails.find('#nome-plano').text(button.data('plano'));
        templateMensalidades(mensalidade, button.data('plano'));
    });

    modalDependente.on('show.bs.modal', function(e) {
        const button = $(e.relatedTarget);
        const dependente = button.data('dependente');
        modalDependente.find('#nome-dependente').text(dependente.nome);
        modalDependente.find('#detalhe-dependente').html(infoDependenteCliente(dependente));
    });
    
    $(document).on('click','.btn-payment', function(e) {
        e.preventDefault();
        const t = $(this);
        e.preventDefault();
        const button = $(this);
        const id_mens = button.data('mensalidade');
        const id_parcela = button.data('parcela')
        const valor = button.data("valor");
        const cartao = button.data("cartao");
        const tipo = button.data('tipo');
        const usuario_id = button.data('usuario');
        swal({
            title: 'Deseja efetuar o pagamento da sua mensalidade?',
            text: "O valor de R$ "+valor+" será debitado do cartão "+cartao,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Sim, Pagar!",
            cancelButtonText: "Não, Cancelar!",
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#9E9E9E"
        }).then(function(result) {
            if(result.value) {
                processarCartao({id_mens, id_parcela, usuario_id}, tipo, function(res) {
                    console.log(res);
                    button.parent().prev().text('Pago');
                    button.remove().parent().text('----');
                }, function() {
                   
                });
            }
        });    
    });
    
    $(document).on('click', '.btn-billet', function(e) {
        e.preventDefault();
        const t = $(this);
        const id_mens = t.data('mensalidade');
        const id_parcela =  t.data('parcela');
        swal({
            title: 'Boleto',
            text: "Deseja gerar o boleto?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Sim, Gerar!",
            cancelButtonText: "Não, Cancelar!",
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#9E9E9E"
        }).then(function(resp) {
            if(resp.value) {
                const data = {
                    id_mens,
                    id_parcela,
                    usuario_id: t.data('usuario')
                };
                gerarBoleto(data);
            }
        });
    });
});        

function finalFind() {
    boxLoader.show();
    const checkBoxesStatus = [... document.querySelectorAll('[name="search_status[]"]:checked')];
        const status = checkBoxesStatus.map(c => c.value).join(',');
    let sData = "";
    if (dateFrom.val() !== "" && dateTo.val() !== "") {
        sData+= '&inicio=' + convertDataBr(dateFrom.val()) + '&fim=' + convertDataBr(dateTo.val());
    }
    if (status !== "") {
        sData+= '&status=' + status;
    }
    if(txtBusca.val() !== "") {
        sData+= '&txtBusca='+txtBusca.val();
    }
    if(tipoPagamento.val() !== "") {
        sData+= '&tipo_pagamento='+tipoPagamento.val();
    }
    if(tipoPlano.val() !== "") {
        sData+= '&tipo_plano='+tipoPlano.val();
    }
    if(selConsultor.val() !== "") {
        sData+= '&consultor='+selConsultor.val();
    }
    return gestorService.getUrlClientes()+sData;
}

function montarFiltroStatus(status) {
    const classes = ['success', 'info', 'emaberto', 'danger', 'warning', 'secondary'];
    const template = Object.keys(status).map(function(item, key) {
        return `
            <div class="col-6 col-md">
                <div class="checkbox-status">
                    <input type="checkbox" name="search_status[]" value="${item}" checked id="check_${item}">
                    <label class="${classes[key]}" for="check_${item}">
                        <div class="titulo">${item+'s'}</div>
                        <span class="quantidade" id="quant_${item}">${status[item]}</span>
                    </label>
                </div>
            </div>
        `;
    }).join('');
    $('#status_filtro').html(template);
}

function infoPessoalCliente(response) {
    const info = [
        {label: 'Id:', value: response.id}, {label: 'Tipo', value: response.tipo === 'P' ? 'Prospect' : (response.tipo === 'C' ? 'Cliente' : '')}, {label: 'Nome:', value: response.nome },
        {label: 'Tipo Júridico:', value: response.juridico_tipo === 'J' ? 'Jurídico' : 'Física'}, 
        {label: 'CPF/CNPJ:', value: response.cnpj},
        {label: 'I.E.:', value: response.inscricao_estadual || ''}, {label: 'CNAE:', value: response.cnae || ''}, 
        {label: response.juridico_tipo === 'J' ? 'Fundação.:' : 'Data de Nasc.:', value: response.data_nascimento},
        {label: 'Email:', value: response.email}, {label: 'Celular:', value: response.celular},  {label: 'Telefone:', value: response.telefone},
        {label: 'Sexo:', value: (response.sexo === 'M' ? 'Masculino' : ((response === 'F') ? 'Feminino' : '')) }, {label: 'Estado Civil:', value: response.estado_civil},
        {label: 'Data de Cadastro:', value: response.data_cadastro}
    ];
    return info.map(function(item) {
        if(item.value) {
            return `<p><strong>${item.label}</strong> ${item.value}</p>`;
        }
        return '';
    }).join('');

}

function infoEnderecoCliente(response) {
    if(!response.cep) {
        return `<h3 class="h4">Nenhum endereço registrado para esse usuário</h3>`;
    } else {
        const info = [
            {label: 'Cep:', value: response.cep}, {label: 'Endereço:', value: response.endereco}, {label: 'Número:', value: response.numero},
            {label: 'Complemento:', value: response.complemento}, {label: 'Bairro:', value: response.bairro}, {label: 'Cidade:', value: response.cidade},
            {label: 'UF:', value: response.estado}
        ];
        return info.map(function(item) {
            if(item.value) {
                return `<p><strong>${item.label}</strong> ${item.value}</p>`;
            }
            return '';
        }).join('');
    }
}

function infoPlanoCliente(response) {
    if(!response.id_plano) {
        return `<h3 class="h4">Nenhum plano registrado para esse usuário</h3>`;
    } else {
        const proximaMensalidade = response.mensalidades.length > 0 ? response.mensalidades[0].dt_inicio_mens : ''; 
        const info = [
            {label: 'Status:', value: response.situacao}, {label: 'Tipo Pagamento:', value: response.tipo_pagamento}, {label: 'Prox. Venc.:', value: proximaMensalidade},
            {label: 'Data de Contratação:', value: response.data_cadastro_plano}, {label: 'Titular:', value: response.nome_responsavel}, {label: 'Consultor:', value: response.nome_funcionario},
            {label: 'Dependentes', value: response.dependentes.length}
        ];
        return `<h3>${response.nome_plano}</h3>
        ${templateCarteiras(response.carteiras, true)}
        <div class="row">
            <div class="col-12">${
                info.map(function(item) {
                    if(item.value) {
                        return `<p><strong>${item.label}</strong> ${item.value}</p>`;
                    }
                    return '';
                }).join('')
            }</div>
        </div>`;
    }
}

function dadosMensalidadesCliente(response) {
    const mensalidades = response.mensalidades;
    const acao = function(mensalidade)  {
        if((['aberto', 'vencido', 'suspenso'].includes(mensalidade.status.toLowerCase()))) {
            if(mensalidade.tipo_pagamento === "DCC" && mensalidade.legenda_cartao) {
                return `<button class="btn btn-secundaria btn-block btn-sm btn-payment mb-1" 
                data-mensalidade="${mensalidade.id_mens}" data-parcela="${mensalidade.id_parcela}"  
                data-cartao="${mensalidade.legenda_cartao}" data-usuario="${response.id}"
                data-valor="${mensalidade.valor_mens}" data-tipo="dcc">Pagar</button>`;
            }else if(["BOLETO", "BOLETO RECORRENTE"].includes(mensalidade.tipo_pagamento)) {
                return `<button class="btn btn-secundaria btn-block btn-sm btn-billet mb-1" 
                data-mensalidade="${mensalidade.id_mens}" data-parcela="${mensalidade.id_parcela}" data-usuario="${response.id}"
                data-boleto="${mensalidade.url_boleto}">${mensalidade.url_boleto ? '2ª via' : 'boleto' }</button>`;
            }
        }            
        return '';
    };
    const detalhes = function(mensalidade) {
        return `<button class="btn btn-secondary btn-sm mr-lg-2 mb-1 btn-details"
        data-toggle="modal" data-target="#modalDetails" 
        data-mensalidade='${JSON.stringify(mensalidade)}'
        data-plano="${response.nome_plano}">detalhes</button>`;       
    }
    return mensalidades.map(function(mensalidade) {
        return `<tr>
            <td>${mensalidade.dt_inicio_mens} a ${mensalidade.dt_fim_mens}</td>
            <td>${mensalidade.valor_pago || mensalidade.valor_mens}</td>
            <td>${mensalidade.dt_inicio_mens}</td>
            <td>${mensalidade.dt_pagamento_mens || '---'}</td>
            <td>${mensalidade.status}</td>
            <td>${detalhes(mensalidade)} ${acao(mensalidade)}</td>
        </tr>`;
    }).join('');
}

function dadosDependentesCliente(dependentes) {
    return dependentes.map(function(dependente) {
        return `<tr>
            <td>${dependente.nome}</td>
            <td>${dependente.nome_parentesco || ''}</td>
            <td>${dependente.data_inclusao}</td>
            <td><button class="btn btn-sm btn-secundaria" data-toggle="modal"  data-target="#modalDetailsDependentes" data-dependente='${JSON.stringify(dependente)}'>detalhes</button></td>
        </tr>`;
    }).join('');
}


function infoDependenteCliente(response) {
    const info = [
        {label: 'ID:', value: response.id_dependente },{label: 'Nome:', value: response.nome}, {label: 'Email:', value: response.email}, {label: 'CPF:', value: response.cnpj},
        {label: 'Data de Nascimento:', value: response.data_nascimento}, {label: 'Sexo:', value: (response.sexo === 'M' ? 'Masculino' : ((response === 'F') ? 'Feminino' : ''))}, {label: 'Estado Civil:', value: response.estado_civil},
        {label: 'Celular:', value: response.celular},   {label: 'Grau Parentesco:', value: response.nome_parentesco}, {label: 'Data de Inclusão:', value: response.data_inclusao}, 
        {label: 'Data de Admissão:', value: response.pri_cnh}
    ];
    return info.map(function(item) {
        if(item.value) {
            return `<p><strong>${item.label}</strong> ${item.value}</p>`;
        }
        return '';
    }).join('');
}

function templateMensalidades(mensalidade, plano) {
    let info = '';
    if (mensalidade.tipo_pagamento === 'DCC') {
       info = `<p>Tipo: <strong>DCC</strong></p>
               <p>Débito no dia <strong>
               ${mensalidade.dt_pagamento_mens ? mensalidade.dt_pagamento_mens : mensalidade.dt_inicio_mens}
               </strong> no cartão: <strong>${mensalidade.legenda_cartao || 'sem cartão'}</strong></p>
               <p>Status: <strong>${mensalidade.status}</strong></p>`;
    } else {
        info = `<p>Tipo: <strong>${mensalidade.tipo_pagamento}</strong></p>
        <p>Boleto ${mensalidade.dt_pagamento_mens ? 'pago' : 'gerado'} no dia<strong>
        ${mensalidade.dt_pagamento_mens ? mensalidade.dt_pagamento_mens : mensalidade.dt_inicio_mens}
        <p>Status: <strong>${mensalidade.status}</strong></p>`;
    }
    let table = '';
    if (textToNumber(mensalidade.acrescimo)) {
        const valor = textToNumber(mensalidade.dependentes) * textToNumber(mensalidade.valor_unit);
        table = `<tr><td>${plano}</td><td>${mensalidade.dependentes}</td><td>${mensalidade.valor_unit}</td><td>${valor}</td></tr>`;
        if (Math.abs(mensalidade.valor_adesao.substr(3).replace(',','.')) > 0) {
            table+=`<tr><td>Taxa de Adesao</td><td>${mensalidade.novo_dependente}</td><td>${mensalidade.taxa_adesao}</td><td>${mensalidade.valor_adesao}</td></tr>`;
        }
    } else {
        table = `<tr><td>${plano}</td><td>1</td><td>${mensalidade.valor_unit}</td><td>${mensalidade.valor_unit}</td></tr>`;
        if (Math.abs(mensalidade.valor_adesao.substr(3).replace(',','.')) > 0) {
            table+=`<tr><td>Taxa de Adesao</td><td>1</td><td>${mensalidade.taxa_adesao}</td><td>${mensalidade.valor_adesao}</td></tr>`;
        }
    }
    $('#info-mensalidade').html(info);
    $('#table-details tbody').html(table);
    $('#table-details tfoot').html(`<tr><td colspan="3"><strong>Total</strong></td><td>${mensalidade.valor_pago}</td></tr>`);
}



function processarCartao(data, tipo, callback, errCallback) {
    boxLoader.show();
    pagamentoService.processarPagamento(data, function(res) {
        boxLoader.hide();
        swal('Sucesso!', 'Transação paga com sucesso', 'success').then(function(rep) {
            callback(res);
        });
    }, function(erro, mensagem) {
        boxLoader.hide();
        if (tipo == 'dcc') {
            swal('Erro!', 'Ocorreu um erro ao processar o pagamento', 'error');
        } else if(erro === 1) {
            swal({
                title: 'Não foi possivel efetuar a compra com o seu cartão',
                text: "Gostaria de alterar seu cartão?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Sim, vou mudar o cartão!',
                cancelButtonText: 'Não, tentarei mais tarde!',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#9E9E9E'
            }).then((result) => {
                if (result.value) {
                    errCallback();
                }
            });
        } else if(erro === 2) {
            swal({type: 'error', title: 'Erro', text: mensagem});
        }
    }, 1);
}

function gerarBoleto(data) {
    pagamentoService.processarPagamento(data, function(res) {
        window.open(res.boleto.url);
        swal('Sucesso!', 'Documento gerado com sucesso!', 'success');        
    }, function(erro, mensagem) {
        if (erro === 1) {
            swal({
                title: 'Erro',
                text: "Não foi possivel gerar o seu documento, tente novamente mais tarde",
                type: 'error'
            });
        } else if(erro === 2) {
            swal({type: 'error', title: 'Erro', text: mensagem });
        }
    }, 1);    
} 