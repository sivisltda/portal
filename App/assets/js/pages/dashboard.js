import { getConfig as configFunc } from './../config/config.js';

import * as dashboardService from './../service/dashboard.service.js';
import { templateCarteiras } from './../shared/carteira.js';
import * as loading from './../core/loading.js';
import { criarDataTable, textToNumber, numberFormat } from './../core/function.js';

const infoPessoal       = $('#info-pessoal');
const infoPlano         = $('#info-plano');
const tabelaMensalidade = $('#table-mensalidade');
const modalPlano        = $('#modalPlano');
const boxLoader         = $('.boxLoader');
const containerCarteira = $('#carteirinha'); 
const containerLink     = $('#links');
const config            = configFunc();

let plano               = null;
let tableMensalidade    = null;

$(document).ready(function() {
    loading.subscribe(function(numero) {
        !!numero ? boxLoader.show() : boxLoader.hide();
    });
    modalPlano.on('show.bs.modal', showModalPlanos);

    dashboardService.getInfoDashboard(function (data) {
        infoPessoal.html(infoPessoalCliente(data.usuario));
        if (data.hasOwnProperty('links')) {
            containerLink.html(`<h4>Links das Campanhas</h4><div class="row">
            ${templateLinkCampanha(data.links)}</div>`);
            containerLink.on('click', '.btnLinkCampanha', function(event) {
                event.preventDefault();
                $(this).closest('.input-group').find('.textCampoLink').select();
                $(this).tooltip({
                    placement: 'top',
                    title: 'Copiado'
                });
                document.execCommand("copy");
            });
        }  else {
            infoHeadLista(data.plano);
            if (data.hasOwnProperty('carteiras') && data.carteiras && data.carteiras.length) {
                containerCarteira.html(templateCarteiras(data.carteiras, false));
                containerCarteira.find('.btn-close-card').on('click', function(e) {
                    e.preventDefault();
                    $('.fundo-preto').removeClass('show-card');
                });
            
                containerCarteira.find('.flip-card').on('click', function(e) {
                    e.preventDefault();
                    const element = $(this).parent().find('.card-digital');
                    element.toggleClass('flip');
            
                });
            
                containerCarteira.find('.card-image').on('click', function(e) {
                    e.preventDefault();
                    const fundo = $(this).closest('.fundo-preto');
                    !fundo.hasClass('show-card') && fundo.addClass('show-card');
                    if($(this).hasClass('front')) {
                        $(this).parent().toggleClass('flip');
                    }else {
                        const element = $(this).parent().find('.card-image.back');
                        element.parent().toggleClass('flip');
                    }
                });
            }
        }
    }, function(erro) {
        console.log(erro);
        swal('Erro', 'Ocorreu um erro ao carregar as informações', 'error');
    });

    $(document).on('change','input[name="search_status[]"]', function(e) {
        e.preventDefault();
        tabelaMensalidade.fnReloadAjax(findMensalidade());
    });

});

function infoPessoalCliente(response) {
    const info = [
        {label: 'Nome:', value: response.razao_social },
        {label: response.juridico_tipo === 'F' ? 'CPF:': 'CNPJ:', value: response.cnpj},
        {label: response.juridico_tipo === 'F' ? 'Data de Nasc.:': 'Fundação.:', value: response.data_nascimento},
        {label: 'Email:', value: response.email}, {label: 'Celular:', value: response.celular},
        {label: 'Telefone:', value: response.telefone},
        {label: 'Sexo:', value: (response.sexo === 'M' ? 'Masculino' : ((response === 'F') ? 'Feminino' : '')) },
        {label: 'Estado Civil:', value: response.estado_civil},
        {label: 'Data de Cadastro:', value: response.data_cadastro}
    ];
    return info.map(function(item) {
        if(item.value) {
            return `<p><strong>${item.label}</strong> ${item.value}</p>`;
        }
        return '';
    }).join('');

}

function infoHeadLista(data) {
    if(data.tipo === 'multiplo') {
       templateClienteMultiplo(data);
    } else if(data.data) {
        templateClienteSingle(data);        
    } else {
        infoPlano.html(`<div class="card card-100">
        <div class="card-body">
            <h4 class="header-title mb-3">Sem Plano Ativo</h4>
            <hr/>
            <div class="text-center">
                <h5 class="h2">Você não possui nenhum plano Ativo no momento</h5>
                <p>Para contratar um, clique no botão abaixo para entrar no nosso site ou entre em contato com um dos nossos representantes.</p>
                <a href="${config.api.replace('Api/', 'loja/')+config.contrato}" target="__blank" class="btn btn-primary">Contratar um plano</a>
            </div>
        </div>
    </div>`);
        
    }
}

function showModalPlanos(e) {
    const button = $(e.relatedTarget);
    const valor = 'vencido,aberto';
    plano = button.data("plano");
    const modal = $(this);
    
    modal.find('.nome-plano').text(plano.nome);
    if(!tableMensalidade) {
        tableMensalidade = makeDataTableMensalidade(tabelaMensalidade, 5);
    }
    tableMensalidade.fnReloadAjax(findMensalidade());
}

function criaTabelaPlano(data) {
    let url = '';
    let columns = {};
    if(data.modelo === 'seguro') {
        columns = {
            "aoColumns": [
                {"className": "centerTable", "sWidth": '10%', data: "placa"},
                {"className": "centerTable", "sWidth": '5%', "render": function (data, type, row) {
                    return `${row.modelo} / ${row.ano_modelo}`;
                }},
                {"className": "centerTable", "sWidth": '10%', data: "data_cadastro"},
                {"className": "centerTable", "sWidth": '5%', "render": function (data, type, row) {
                    const valorProtecoes = row.acessorios.reduce(function(acc, item) {
                        return acc + textToNumber(item.preco_venda);
                    }, textToNumber(row.valor_plano_item));
                    return numberFormat(valorProtecoes, 1);
                }},
                {"className": "centerTable", "sWidth": '5%', "render": function (data, type, row) {
                    return row.link_contrato.length > 0 ? `<a target="_blank" rel="noopener noreferrer" title="${row.link_contrato.endsWith(".pdf") ? "Ver Contrato" : "Assinar Contrato"}" href="${row.link_contrato}">
                    <img src="${config.site}assets/img/${row.link_contrato.endsWith(".pdf") ? "assinado.png" : "assinar.png"}" alt="Contrato" class="img-assinatura"></a>` : ``;
                }}
            ]
        };
        url = config.api+'dashboard/dashboard?functionPage=GetVeiculos';
    }else {
        columns = {
            "aoColumns": [
                {"className": "centerTable", "sWidth": '5%', "render": function (data, type, row) {
                    return `<div class="input-group-prepend">
                        <button class="btn bg-primaria texto-secundaria dropdown-toggle p-0 mx-auto" 
                        style="width: 25px !important; height: 25px!important; " 
                        type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" data-toggle="modal" data-target="#modalPlano" data-backdrop="static" 
                            data-plano='${JSON.stringify(row)}' data-usuario="${row.favorecido}">Mensalidades</a>
                        </div>
                    </div>`;
                }},
                {"className": "centerTable", "sWidth": '10%', data: "nome"},
                {"className": "centerTable", "sWidth": '5%', "render": function (data, type, row) {
                    return `
                     <span class="notify-status ${row.status.replace(/( )+/, '').toLowerCase()}"></span>
                     <span class="status ${row.status.replace(/( )+/, '').toLowerCase()}">${row.status}</span>`;
                }},
                {"className": "centerTable", "sWidth": '10%', data: "dt_inicio"},
                {"className": "centerTable", "sWidth": '10%', data: "dt_fim"}
            ]
        };
        url = config.api+'dashboard/dashboard?functionPage=GetPlanos';
    }
    infoPlano.find('table#tblPlanos').dataTable(criarDataTable(5, columns, url, function() {        
    }));
}

function makeDataTableMensalidade(tabela, quantidade) {
    const columns = {
        "aoColumns": [
            {"sWidth": '15%', "render": function (data, type, row) {
                return `${row.dt_inicio_mens} a ${row.dt_fim_mens}`;
            }},
            {"className": "centerTable", "sWidth": '12%', "render": function (data, type, row) {
                return `<span class="status_mens ${row.status.toLowerCase()}">${row.status}</span>`;
            }},
            {"className": "centerTable", "sWidth": '12%', data: "valor_mens"},
            {"className": "centerTable", "sWidth": '12%', data: "valor_pago"},
            {"className": "centerTable", "sWidth": '12%', data: "dt_inicio_mens"},
            {"className": "centerTable", "sWidth": '12%', data: "dt_pagamento_mens"}
        ],
        "sPaginationType": "full_numbers",
        "initComplete": function (settings, data) {            
        }
    };
    return tabela.dataTable(criarDataTable(quantidade, columns, findMensalidade(),function (settings, data) {
        montarFiltroStatusMensalidade(settings.json.totais);        
    }));
}

function templateClienteMultiplo(data) {
    const head = data.modelo === 'seguro' ? 
    ['Placa', 'Modelo / Ano', 'Dt. Cadastro', 'Val.Prot.', 'Ass.'] : 
    ['Ação',  'Plano', 'Status', 'Inicio', 'Fim'];
    const thead = head.map(function(item) {
        return `<th>${item}</th>`;
    }).join('');
    const titulo = data.modelo === 'seguro' ? 'Veículos' : 'Planos';
    const templateCard =`<div class="card card-100"><div class="card-body card-dashboard"><h4 class="header-title mb-3">${titulo}</h4>
          <hr/><table id="tblPlanos" class="table table-striped dt-responsive nowrap w-100 dataTable no-footer dtr-inline">
          <thead class="bg-primaria texto-secundaria"><tr>${thead}</tr></thead><tbody></tbody></table></div></div>`;
    infoPlano.html(templateCard);
    criaTabelaPlano(data);
}

function templateClienteSingle(data) {
    const titulo = data.modelo === 'seguro' ? 'Veículo' : 'Plano';
    const dados = {'status': 'Status', 'dt_inicio': 'Data de Contratacao', 'dependentes': 'Dependentes' ,
        'prox_venc': 'Próximo Vencimento'};
    const infoDados = Object.keys(dados).map(function(item) {
        const valorDado = Array.isArray(data.data[item]) ? data.data[item].length : data.data[item];
        if (valorDado) {
            return `<p class="text-muted"><strong class="mr-2">${dados[item]}:</strong><span>${valorDado}</span></p>`;
        }
        return '';
    }).join('');
    const templateCard = `<div class="card card-100"><div class="card-body card-dashboard">
            <h4 class="header-title mb-3">${titulo}: ${data.data.nome}</h4><hr/><div class="row align-items-center">
            <div class="col-md-8 text-left">${infoDados}</div>
            <div class="col-md-4"><button class="btn bg-secundaria texto-secundaria"data-toggle="modal" 
            data-target="#modalPlano" data-backdrop="static" data-plano='${JSON.stringify(data.data)}' 
            data-usuario="${data.data.favorecido}">Mensalidades</button></div></div>
            </div></div>`;
    infoPlano.html(templateCard);
}

function findMensalidade() { 
    const status =  [... document.querySelectorAll('input[name="search_status[]"]:checked')].map(c => c.value).join(',');
    return config.api+'dashboard/dashboard?functionPage=GetMensalidades&plano_id='+plano.id+'&status='+status;
}

function montarFiltroStatusMensalidade(status) {
    const checkBoxesStatus = [... document.querySelectorAll('input[name="search_status[]"]')].filter(c => c.checked).map(c => c.value);
    const valor = checkBoxesStatus.length ? checkBoxesStatus : ['vencido', 'aberto', 'pago'];
    const template = Object.keys(status).map(function(item, key) {
        return `
            <div class="col-6 col-md">
                <div class="checkbox-status ${item}">
                    <input type="checkbox" name="search_status[]" value="${item}" ${valor.includes(item) ? 'checked' : ''}  id="check_${item}">
                    <label for="check_${item}">
                        <div class="titulo">${item+'s'}</div>
                        <span class="quantidade" id="quant_${item}">${status[item]}</span>
                    </label>
                </div>
            </div>
        `;
    }).join('');
    $('#status_filtro').html(template);
}

function templateLinkCampanha(dados) {
    return dados.map(function(item) {
        return `
            <div class="col-12 col-sm-6 col-xl-4 mb-2">
            <div class="form-group">
                <label for="link">${item.titulo}</label>
                <div class="input-group mb-3">
                    <input type="text" class="form-control textCampoLink" readonly
                    value="${item.url}">
                    <div class="input-group-append">
                        <button class="btn bg-secundaria text-white btnLinkCampanha" type="button">
                            <i class="mdi mdi-content-copy"></i> Copiar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        `;
    }).join('');
}