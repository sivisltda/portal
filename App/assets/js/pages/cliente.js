import carregaPortal from './../modules/load-module.js';
import * as loading from './../core/loading.js';
import * as funcionarioService from './../service/funcionario.service.js';

const app           = $('#app');
const boxLoader     = $('.boxLoader');
const body          = $('body');

$(document).ready(function(e) {
    loading.subscribe(function(numero) {
        !!numero ? boxLoader.show() : boxLoader.hide();
    });    
    $('.btn-cotacao').on('click', clickSessaoConsultor);
    $('#normal').click();    
    $(window).scroll(function(e) {
        e.preventDefault();
        const altura = $(window).scrollTop();
        if (altura > 150 && !body.hasClass('fixed-dashboard')) {
            body.addClass('fixed-dashboard');
        } else if (altura <=  150 && body.hasClass('fixed-dashboard')) {
            body.removeClass('fixed-dashboard');
        }
    });
});

function clickSessaoConsultor(e) {
    e.preventDefault();
    const iframe = $('iframe');
    const t = $(this);
    const tipo = t.data('tipo');
    const rapido = tipo === 'normal' ? 0 : 1;
    iframe.removeAttr('src');
    $('.btn-cotacao').removeClass('btn-secundaria').addClass('btn-primaria');
    t.addClass('btn-secundaria');
    funcionarioService.criarSessaoConsultor({ rapido }, function(){
        //iframe.attr('src', t.data('src'));
        carregaPortal(app, function(erro) {
            console.log(erro);
            swal('Erro', 'Ocorreu um erro ao carregar os dados do portal', 'error');
        });
    }, function (erro) {
        console.log(erro);
        swal('Erro', 'Ocorreu um erro ao criar a sessão do consultor logado', 'error');
    });
} 


