export function formDados(events, id) {
    return {
        type: 'column',
        events,
        inputs: [
            {
                type: 'text',  size: 'col-sm-8', id: 'cartao_numero'+id, label: 'Número do Cartão:',
                name: 'cartao_numero', required: true,
                attributes : {
                    class: 'form-control cartao_numero',
                    placeholder: 'Número do Cartão',
                    style: 'padding-left: 60px;'
                },
                additional: [
                    { type: 'prepend',  class: 'credcard-thumb' }
                ]
            }, {
                type: 'select',  size: 'col-sm-4', id: 'cartao_bandeira'+id, label: 'Bandeira:',
                name: 'cartao_bandeira', required: true,
                options: {
                    "0": "",
                    "1": "",
                    "2": "",
                    "3": "",
                    "4": "",
                    "5": "",
                    "6": "",
                    "7": "",
                    "8": "",
                    "9": "",
                    "10": "",
                    "11": ""
                },
                attributes : {
                    class: 'form-control'
                }                
            }, {                
                type: 'text',  size: 'col-sm-6', id: 'data_expiracao'+id, label: 'Data de Expiração:',
                name: 'data_expiracao', required: true,
                attributes : {
                    class: 'form-control data_expiracao',
                    placeholder: 'xx/xxxx',
                    "data-mask-format": "00/0000"
                }
            }, {
                type: 'text',  size: 'col-sm-6', id: 'cvv'+id, label: 'CVV:',
                name: 'cvv', required: true,
                attributes : {
                    class: 'form-control cvv',
                    placeholder: 'xxx',
                    "maxLenght": "4",
                    "minLenght": "3"
                }
            }, {
                type: 'text',  size: 'col-sm-12', id: 'nome_cartao'+id, label: 'Nome do Responsável do Cartão:',
                name: 'nome_cartao', required: true,
                attributes : {
                    class: 'form-control nome_cartao',
                    placeholder: 'Nome do Cartão'
                }
            }
        ],
        rules: {
            cartao_numero: {
                rule: 'required|creditCard',
                message: {
                    required: 'O número do cartão é obrigatório',
                    creditcard: 'O número do cartão é inválido'
                }
            },
            cartao_bandeira: {
                rule: 'required|number',
                message: {
                    required: 'A bandeira obrigatória'
                }
            },            
            data_expiracao: {
                rule: 'required|mesano',
                message: {
                    required: 'A data de expiração é obrigatório',
                    mesano: 'A data de expiração tem que conter o mês e o ano'
                }
            },
            cvv: {
                rule: 'required|max:4|min:3',
                message: {
                    required: 'O CVV é obrigatório',
                    number: 'O CVV deve ser um número',
                    min: 'O mínimo de caracteres é :min',
                    max: 'O mínimo de caracteres é :max'
                }
            },
            nome_cartao: {
                rule: 'required|min:2',
                message: {
                    required: 'O nome do cartão é um campo obrigatório',
                    min: 'O mínimo de caracteres é :min'
                }
            }
        }
    };
}

export function formatState(state) {
    if (!state.id) {
        return state.text;
    }
    var baseUrl = "./../../App/assets/img/cartao/";
    var $state = $('<span><img src="' + baseUrl + '/' + state.element.value.toLowerCase() + '.png" style="height: 25px;" /></span>');
    return $state;
}

export function actionState(card, flag) {
    $(card).change(function () {
        if ($(card).val() !== "") {
            $(card).validateCreditCard(function (result) {
                if (result.card_type !== null) {
                    $(flag).val(result.card_type.code).trigger("change");
                }
            });
        }
    });
}